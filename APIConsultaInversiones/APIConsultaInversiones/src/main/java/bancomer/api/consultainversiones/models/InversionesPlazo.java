package bancomer.api.consultainversiones.models;

/**
 * Created by miguelangel.aguilarc on 09/11/2015.
 */
public class InversionesPlazo {

    private String numeroContrato;

    private String numeroInversion;

    private String saldo;

    private String fechaVencimiento;

    private String tasa;

    private String gat;

    public InversionesPlazo() {
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getNumeroInversion() {
        return numeroInversion;
    }

    public void setNumeroInversion(String numeroInversion) {
        this.numeroInversion = numeroInversion;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getTasa() {
        return tasa;
    }

    public void setTasa(String tasa) {
        this.tasa = tasa;
    }

    public String getGat() {
        return gat;
    }

    public void setGat(String gat) {
        this.gat = gat;
    }
}
