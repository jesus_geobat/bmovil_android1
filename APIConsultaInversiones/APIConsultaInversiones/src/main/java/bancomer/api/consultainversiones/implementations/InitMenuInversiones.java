package bancomer.api.consultainversiones.implementations;

import android.content.Context;

import com.bancomer.base.callback.CallBackAPI;
import com.bancomer.base.callback.CallBackSession;

import java.util.ArrayList;

import bancomer.api.consultainversiones.gui.controllers.DetalleInversionViewController;
import bancomer.api.consultainversiones.gui.delegates.InversionDelegate;
import bancomer.api.consultainversiones.io.BanderasServer;
import bancomer.api.consultainversiones.models.InversionesPlazo;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

/**
 * Created by User1 on 09/12/2015.
 */
public class InitMenuInversiones {

    public static InitMenuInversiones initMenuInversiones;
    private Context context;
    private SuiteAppConsultaInversionesApi suiteAppConsultaInversionesApi;
    private CallBackSession callBackSession;
    private CallBackAPI callBackAPI;
    private String numeroCuenta;

    private InitMenuInversiones(BanderasServer banderasServer) {
        ServerCommons.ALLOW_LOG = banderasServer.getLogActivo();
        ServerCommons.DEVELOPMENT = banderasServer.isDesarrollo();
        ServerCommons.EMULATOR = banderasServer.getEmulador();
        ServerCommons.SIMULATION = banderasServer.getSimulacion();
    }

    public static InitMenuInversiones getInstance(Context context, BanderasServer bandera) {

        initMenuInversiones = new InitMenuInversiones(bandera);
        //}
        initMenuInversiones.setContext(context);
        return initMenuInversiones;
    }

    public void initializeDependenceApis() {

        suiteAppConsultaInversionesApi = new SuiteAppConsultaInversionesApi();
        suiteAppConsultaInversionesApi.onCreate(context);

    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setCallBackAPI(CallBackAPI callBackAPI) {
        this.callBackAPI = callBackAPI;
    }

    public void configurar(ArrayList <InversionesPlazo> inversion) {

        configurarConInversion(inversion);
    }

    public void configurarConInversion(ArrayList<InversionesPlazo> inversion) {
        initMenuInversiones.initializeDependenceApis();
        final BaseViewsController baseViewsController = SuiteAppConsultaInversionesApi
                .getInstance().getBmovilApplication()
                .getBmovilViewsController();
        final InversionDelegate delegate = new InversionDelegate();
        delegate.setListaInversionesPlazos(inversion);
        baseViewsController.addDelegateToHashMap(
                InversionDelegate.INVERSION_DELEGATE_ID, delegate);
    }

    public void returnToPrincipal() {
        callBackAPI.returnToPrincipal();
    }

    public void setCallBackSession(CallBackSession callBackSession) {
        this.callBackSession = callBackSession;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }
}
