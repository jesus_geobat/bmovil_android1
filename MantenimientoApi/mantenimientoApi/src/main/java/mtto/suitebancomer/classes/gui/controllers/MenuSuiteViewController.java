package mtto.suitebancomer.classes.gui.controllers;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.HashMap;
import java.util.Map;
import bancomer.api.common.commons.Constants;
import mtto.com.bancomer.mbanking.R;
import mtto.com.bancomer.mbanking.SuiteAppMtto;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.views.FlipAnimation;
import mtto.suitebancomer.classes.common.GuiTools;
import mtto.suitebancomer.classes.gui.delegates.MenuSuiteDelegate;
import mtto.tracking.TrackingHelper;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.ContratacionSofttokenViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

public class MenuSuiteViewController extends BaseViewController implements View.OnClickListener, DialogInterface.OnClickListener, AnimationListener {
	
	/**
	 * The layout where the options and the login view are supposed to appear
	 */
	private FrameLayout baseLayout;
	/**
	 * The layout containing the suite options
	 */
	private LinearLayout menuOptionsLayout;
	/**
	 * Reference to the first button of the option layout
	 */
	private ImageButton firstMenuOption;
	/**
	 * Reference to the second button of the option layout
	 */
	private ImageButton secondMenuOption;
	/**
	 * Referencia al bot��n cont��ctanos
	 */
	private ImageButton contactButton;
	/**
	 * Referencia texto versi��n
	 */
	private TextView txtVersion;

	private MenuSuiteDelegate delegate;
	
	private boolean isFlipPerforming;
	
	private boolean shouldHideLogin;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	public boolean comesFromNov = false;

	public void setComesFromNov(final Boolean b){
		comesFromNov = b;
	}

	public void setIsFlipPerforming(final Boolean b){
		isFlipPerforming = b;
	}

	/**
	 * Default constructor for this activity
	 */
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER, R.layout.mtto_layout_suite_menu_principal);
		//AMZ
				parentManager = SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController();
			
				TrackingHelper.trackState("menu suite", parentManager.estados);
			
		parentViewsController = SuiteAppMtto.getInstance().getSuiteViewsController();
		baseLayout = (FrameLayout)findViewById(R.id.suite_menu_options_layout);
		menuOptionsLayout = (LinearLayout)findViewById(R.id.suite_menu_options_view);
		firstMenuOption = (ImageButton)findViewById(R.id.suite_menu_option_first);
		firstMenuOption.setOnClickListener(this);
		secondMenuOption = (ImageButton)findViewById(R.id.suite_menu_option_second);
		secondMenuOption.setOnClickListener(this);
		contactButton = (ImageButton)findViewById(R.id.suite_contact_button);
		contactButton.setOnClickListener(this);
		txtVersion = (TextView)findViewById(R.id.textVersion);
		txtVersion.setText(SuiteAppMtto.appContext
				.getString(R.string.administrar_app_version)+" "
				+ Tools.getDoubleAmountFromServerString(Constants.APPLICATION_VERSION));
		
		scaleForCurrentScreen();
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		parentViewsController.setCurrentActivityApp(this);
		delegate = (MenuSuiteDelegate)parentViewsController.getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuSuiteDelegate();
			parentViewsController.addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, delegate);
		}
		delegate.setMenuSuiteViewController(this);
		if(null != contratacionSTViewController) {
            contratacionSTViewController.setVisibility(View.GONE);
        }
		if(null != menuOptionsLayout) {
            this.menuOptionsLayout.setVisibility(View.VISIBLE);
        }
		areButtonsDisabled = false;
		
		if(delegate.isbMovilSelected()) {
            delegate.bmovilSelected();
        }
		areButtonsDisabled = false;
		
		if(comesFromNov){
			this.menuOptionsLayout.setVisibility(View.GONE);
			comesFromNov = false;
		}
		delegate.cargaTelSeedKeystore();
	}

	@Override
	protected void onPause() {
		super.onPause();
		hideSoftKeyboard();
		
		if (SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().consumeAccionesDePausa()) {
			parentViewsController.getCurrentViewControllerApp().hideCurrentDialog();
		}
		parentViewsController.getCurrentViewControllerApp().hideCurrentDialog();
	}
	
	/**
	 * Method that listens whenever a view is touched (clicked)
	 */
	@Override
	public void onClick(final View v) {
		if(areButtonsDisabled) {
            return;
        }
		areButtonsDisabled = true;
		if (v.equals(firstMenuOption)) {
			delegate.bmovilSelected();
		} else if (v.equals(secondMenuOption)) {
			delegate.onBtnContinuarclick(v);
		} else if (v.equals(contactButton)) {
			//ARR Boton Contactanos
			final Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
			//ARR
			paso1OperacionMap.put("evento_paso1", "event46");
			paso1OperacionMap.put("&&products", "menu suite");
			paso1OperacionMap.put("eVar12", "paso1:cont��ctanos");

			TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
			((SuiteViewsController)parentViewsController).showContactanos();
		}
	}

	public void setButtonsDisabled(final boolean value) {
		this.areButtonsDisabled = value;
	}
	
	public void plegarOpcion() {
		if (isFlipPerforming) {
			return;
		}
		isFlipPerforming = true;
	}
	
	private void llamarLineaBancomer(final String numeroTel) {
		delegate.llamarLineaBancomer(numeroTel);
	}
	
	public FrameLayout getBaseLayout() {
		return baseLayout;
	}
	
	public boolean getShouldHideLogin() {
		return shouldHideLogin;
	}
	
	public void setShouldHideLogin(final boolean shouldHideLogin) {
		this.shouldHideLogin = shouldHideLogin;
	}

	@Override
	public void onClick(final DialogInterface dialog, final int which) {
		if (which == Dialog.BUTTON_POSITIVE) {
			llamarLineaBancomer(getString(R.string.menuSuite_callLocalNumber));
		} else if (which == Dialog.BUTTON_NEUTRAL) {
			llamarLineaBancomer(getString(R.string.menuSuite_callAwayNumber));
		}
		areButtonsDisabled = false;
	}

	/**
	 * Stores the session information after a login operation. If registering
	 * is taking place, it show the activation screen instead.
	 *
	 * @param response The server response to the login operation
	 */
    public void processNetworkResponse(final int operationId, final ServerResponse response) {
        //Empty method
    }
    
    public void restableceMenu() {
    	if(contratacionSTViewController != null) {
    		contratacionSTViewController.setVisibility(View.GONE);
    		baseLayout.removeView(contratacionSTViewController);
    		contratacionSTViewController = null;
    	}
    	menuOptionsLayout.setVisibility(View.VISIBLE);
    	
    }
    
    @Override
	public void goBack() {
    	delegate.onBackPressed();
    }
    
	@Override
	public void onAnimationEnd(final Animation animation) {
		final FlipAnimation flipAnimation = (FlipAnimation)animation;
		if (!flipAnimation.isForward()) {

		}
		isFlipPerforming = false;
		areButtonsDisabled = false;
	}

	@Override
	public void onAnimationRepeat(final Animation animation) {
		isFlipPerforming = true;
	}

	@Override
	public void onAnimationStart(final Animation animation) {
		isFlipPerforming = true;
	}

	/**
	 * Establece la l���gica a seguir para el redimensionamiento de la pantalla.
	 */
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(baseLayout);
		guiTools.scale(menuOptionsLayout);
		guiTools.scale(firstMenuOption);
		guiTools.scale(secondMenuOption);
		guiTools.scale(findViewById(R.id.suite_advertising_image));
		guiTools.scale(contactButton);
	}
	
	// #region Softtoken
	private void softtokenSelected() {
		delegate.softtokenSelected();
	}
	
	private ContratacionSofttokenViewController contratacionSTViewController;
	
	public void setContratacionSTViewController(final ContratacionSofttokenViewController contratacionSTViewController) {
		this.contratacionSTViewController = contratacionSTViewController;
	}
	
	public ContratacionSofttokenViewController getContratacionSTViewController() {
		return contratacionSTViewController;
	}
	
	public void plegarOpcionST() {
		if (isFlipPerforming) {
			return;
		}
		isFlipPerforming = true;
		final FlipAnimation flipAnimation = new FlipAnimation(menuOptionsLayout, contratacionSTViewController);
		flipAnimation.setAnimationListener(this);
	    if (menuOptionsLayout.getVisibility() == View.GONE) {
	        flipAnimation.reverse();
	    }
	    baseLayout.startAnimation(flipAnimation);
	}
	public void plegarOpcionAplicacionDesactivada() {
        //Empty method
	}
	
	private boolean areButtonsDisabled = false;
}

