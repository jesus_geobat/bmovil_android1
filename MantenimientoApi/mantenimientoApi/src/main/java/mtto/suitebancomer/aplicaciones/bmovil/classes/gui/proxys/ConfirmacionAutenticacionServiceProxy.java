/**
 * 
 */
package mtto.suitebancomer.aplicaciones.bmovil.classes.gui.proxys;

import android.util.Log;
import com.bancomer.base.SuiteApp;
import java.io.Serializable;
import java.util.ArrayList;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import mtto.com.bancomer.mbanking.R;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.to.ParamTo;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * @author lbermejo
 *
 */
public class ConfirmacionAutenticacionServiceProxy implements IConfirmacionAutenticacionServiceProxy, Serializable {
	
	/**
	 */
	private static final long serialVersionUID = 3925667201345924488L;
	private BaseDelegateCommons baseDelegateCommons;
	
	public ConfirmacionAutenticacionServiceProxy(final BaseDelegateCommons bdc) {
		this.baseDelegateCommons = bdc;
	}
	
	@Override
	public ArrayList<Object> getListaDatos() {
		if(Server.ALLOW_LOG) {
            Log.d(getClass().getName(), ">>proxy getListaDatos >> delegate");
        }
		final ConfirmacionAutenticacionDelegate delegate = (ConfirmacionAutenticacionDelegate)baseDelegateCommons;
		return delegate.consultaOperationsDelegate().getDatosTablaConfirmacion();
	}

	@Override
	public ConfirmacionViewTo showFields() {

		if(Server.ALLOW_LOG) {
            Log.d(getClass().getName(), ">>proxy showFields >> delegate");
        }
		final ConfirmacionViewTo to = new ConfirmacionViewTo();

		final ConfirmacionAutenticacionDelegate delegate = 
				(ConfirmacionAutenticacionDelegate)baseDelegateCommons;
		to.setShowContrasena(delegate.consultaDebePedirContrasena());
		to.setShowNip(delegate.consultaDebePedirNIP());
		to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
		to.setShowCvv(delegate.consultaDebePedirCVV());
		to.setShowTarjeta(delegate.mostrarCampoTarjeta() );
		to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
		to.setInstrumentoSeguridad(delegate.consultaTipoInstrumentoSeguridad());
		to.setTextoAyudaInsSeg(
				delegate.getTextoAyudaInstrumentoSeguridad(
						to.getInstrumentoSeguridad()));
		return to;
	}
	
	@Override
	public Integer getMessageAsmError(final TipoInstrumento tipoInstrumento) {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getMessageAsmError >> delegate");

		int idMsg = 0;

		switch (tipoInstrumento) {
			case OCRA:
				idMsg = R.string.confirmation_ocra;
				break;
			case DP270:
				idMsg = R.string.confirmation_dp270;
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					idMsg = R.string.confirmation_softtokenActivado;
				} else {
					idMsg = R.string.confirmation_softtokenDesactivado;
				}
				break;
			default:
				break;
		}

		return idMsg;
	}

	@Override
	public String loadOtpFromSofttoken(final TipoOtpAutenticacion tipoOtpAutenticacion) {

		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy loadOtpFromSofttoken >> delegate");
		final ConfirmacionAutenticacionDelegate delegate =
				(ConfirmacionAutenticacionDelegate)baseDelegateCommons;
		return delegate.loadOtpFromSofttoken(tipoOtpAutenticacion);
	}

	@Override
	public Integer consultaOperationsIdTextoEncabezado(){
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy consultaOperationsIdTextoEncabezado >> delegate");

		//getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()
		final ConfirmacionAutenticacionDelegate delegate = 
								(ConfirmacionAutenticacionDelegate)baseDelegateCommons;
		final int res = delegate.consultaOperationsDelegate().getTextoEncabezado();
		
		return res;
	}

	@Override
	public Boolean doOperation(final ParamTo to) {

		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy doOperation >> delegate");
		final ConfirmacionAutenticacionDelegate delegate =
				(ConfirmacionAutenticacionDelegate)baseDelegateCommons;
		final ConfirmacionViewTo params = (ConfirmacionViewTo)to;
		final ConfirmacionAutenticacionViewController caller = new ConfirmacionAutenticacionViewController();
		caller.setDelegate(delegate);

		try {
			delegate.getOperationDelegate().realizaOperacion(caller, params.getContrasena(),
					params.getNip(), params.getAsm(),
					params.getCvv(), params.getTarjeta());

			return Boolean.TRUE;
		}catch(Exception e){
			//TODO Log
			return Boolean.FALSE;
		}

	}

	@Override
	public BaseDelegateCommons getDelegate() {
		return baseDelegateCommons;
	}

}
