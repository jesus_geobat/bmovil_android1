package com.bancomer.base.callback;

import bancomer.api.common.commons.Constants;

/**
 * Created by evaltierrah on 18/08/2015.
 */
public interface CallBackSession {

    void userInteraction();

    void cierraSesionBackground(Boolean isChanging);

    void cierraSesion();

    void returnMenuPrincipal();

    void closeSession(String userName, String ium, String client);

    void setClientProfile(Constants.Perfil perfil);

}
