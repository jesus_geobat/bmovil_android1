package com.bancomer.base.callback;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

/**
 * Created by andres.vicentelinare on 10/11/2015.
 */
public interface CallBackAPI {

    void returnToPrincipal();

    void returnDataFromModule(String operation, ServerResponse response);

    void cierraSesion();

    void cierraSesionBackground(Boolean isChanging);

    void userInteraction();

    void returnMenuPrincipal();
}
