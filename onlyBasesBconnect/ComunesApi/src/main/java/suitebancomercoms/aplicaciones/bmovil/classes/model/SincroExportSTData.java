package suitebancomercoms.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class SincroExportSTData implements ParsingHandler {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String estado;
	private String indReac2x1;
	
	public SincroExportSTData() {
		super();
	}
	public SincroExportSTData(final String estado, final String indReac2x1) {
		super();
		this.estado = estado;
		this.indReac2x1 = indReac2x1;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(final String estado) {
		this.estado = estado;
	}
	public String getIndReac2x1() {
		return indReac2x1;
	}
	public void setIndReac2x1(final String indReac2x1) {
		this.indReac2x1 = indReac2x1;
	}
	
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		// Empty method
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		
		this.estado= parser.parseNextValue("estado");
		this.indReac2x1 = parser.parseNextValue("indReac2x1");
		
	}
}
