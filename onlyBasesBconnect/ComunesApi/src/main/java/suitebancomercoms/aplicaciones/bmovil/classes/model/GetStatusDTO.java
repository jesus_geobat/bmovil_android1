package suitebancomercoms.aplicaciones.bmovil.classes.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;

/**
 * Created by evaltierrah on 18/07/16.
 */
public class GetStatusDTO {

    /**
     * Guarda el codigo de la respuesta
     */
    private int code = 0;
    /**
     * Estado del servicio
     */
    private String state;
    /**
     * Fecha de contratacion
     */
    private String creationDate;
    /**
     * Perfil del cliente
     */
    private String profile;
    /**
     * Compania de celular
     */
    private String mobilePhoneCompany;
    /**
     * Indica si tiene las alertas activas
     *
     */
    private String alertIndicator;
    /**
     * Tipo de instrumento asociado al cliente
     */
    private String securityInstrument;
    /**
     * Estado del instrumento
     */
    private String securityInstrumentState;
    /**
     * Fecha del sistema
     */
    private String serverDate;
    /**
     * Indicador de cuenta digital
     */
    private String digitalAccount;
    /**
     * Mensaje de la respuesta
     */
    private String description;

    public void process(String parser) throws IOException, ParsingException {
        try {
            final JSONObject jsonObjectStat = new JSONObject(parser);
            if (jsonObjectStat.has("response")) {
                setCode(200);
                final JSONObject jsonObjectResponse = new JSONObject(jsonObjectStat.getString("response"));
                setCreationDate(getDataFromJson(jsonObjectResponse, "creationDate", null));
                setServerDate(getDataFromJson(jsonObjectResponse, "serverDate", null));
                setSecurityInstrument(getDataFromJson(jsonObjectResponse, "securityInstrument", null));
                setSecurityInstrumentState(getDataFromJson(jsonObjectResponse, "securityInstrumentState", null));
                setProfile(getDataFromJson(jsonObjectResponse, "profile", null));
                setMobilePhoneCompany(getDataFromJson(jsonObjectResponse, "mobilePhoneCompany", null));
                setAlertIndicator(getDataFromJson(jsonObjectResponse, "alertIndicator", null));
                setState(jsonObjectResponse.getString("state"));
                if (jsonObjectResponse.has("digitalAccount")) {
                    setDigitalAccount(jsonObjectResponse.getString("digitalAccount"));
                }
                if (jsonObjectStat.has("status")) {
                    final JSONObject jsonObjectCode = new JSONObject(jsonObjectStat.getString("status"));
                    setDescription(jsonObjectCode.getString("description"));
                }
            }
            if (jsonObjectStat.has("description")) {
                setCode(403);
                setDescription(jsonObjectStat.getString("description"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private String getDataFromJson(final JSONObject jsonObject, final String data,
                                   final String opt) throws JSONException {
        if (!jsonObject.isNull(data) && !jsonObject.getString(data).equalsIgnoreCase("")
                && !jsonObject.getString(data).equalsIgnoreCase("NO_VALUE")) {
            return jsonObject.getString(data);
        } else {
            return "NO_VALUE";
        }
    }

    /**
     * Gets the value of code and returns code
     *
     * @return Returns value of code
     */
    public int getCode() {
        return code;
    }

    /**
     * Sets the code
     * You can use getCode() to get the value of code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Gets the value of state and returns state
     *
     * @return Returns value of state
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the state
     * You can use getState() to get the value of state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Gets the value of creationDate and returns creationDate
     *
     * @return Returns value of creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the creationDate
     * You can use getCreationDate() to get the value of creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Gets the value of profile and returns profile
     *
     * @return Returns value of profile
     */
    public String getProfile() {
        return profile;
    }

    /**
     * Sets the profile
     * You can use getProfile() to get the value of profile
     */
    public void setProfile(String profile) {
        this.profile = profile;
    }

    /**
     * Gets the value of mobilePhoneCompany and returns mobilePhoneCompany
     *
     * @return Returns value of mobilePhoneCompany
     */
    public String getMobilePhoneCompany() {
        return mobilePhoneCompany;
    }

    /**
     * Sets the mobilePhoneCompany
     * You can use getMobilePhoneCompany() to get the value of mobilePhoneCompany
     */
    public void setMobilePhoneCompany(String mobilePhoneCompany) {
        this.mobilePhoneCompany = mobilePhoneCompany;
    }

    /**
     * Gets the value of alertIndicator and returns alertIndicator
     *
     * @return Returns value of alertIndicator
     */
    public String getAlertIndicator() {
        return alertIndicator;
    }

    /**
     * Sets the alertIndicator
     * You can use getAlertIndicator() to get the value of alertIndicator
     */
    public void setAlertIndicator(String alertIndicator) {
        this.alertIndicator = alertIndicator;
    }

    /**
     * Gets the value of securityInstrument and returns securityInstrument
     *
     * @return Returns value of securityInstrument
     */
    public String getSecurityInstrument() {
        return securityInstrument;
    }

    /**
     * Sets the securityInstrument
     * You can use getSecurityInstrument() to get the value of securityInstrument
     */
    public void setSecurityInstrument(String securityInstrument) {
        this.securityInstrument = securityInstrument;
    }

    /**
     * Gets the value of securityInstrumentState and returns securityInstrumentState
     *
     * @return Returns value of securityInstrumentState
     */
    public String getSecurityInstrumentState() {
        return securityInstrumentState;
    }

    /**
     * Sets the securityInstrumentState
     * You can use getSecurityInstrumentState() to get the value of securityInstrumentState
     */
    public void setSecurityInstrumentState(String securityInstrumentState) {
        this.securityInstrumentState = securityInstrumentState;
    }

    /**
     * Gets the value of serverDate and returns serverDate
     *
     * @return Returns value of serverDate
     */
    public String getServerDate() {
        return serverDate;
    }

    /**
     * Sets the serverDate
     * You can use getServerDate() to get the value of serverDate
     */
    public void setServerDate(String serverDate) {
        this.serverDate = serverDate;
    }

    /**
     * Gets the value of digitalAccount and returns digitalAccount
     *
     * @return Returns value of digitalAccount
     */
    public String getDigitalAccount() {
        return digitalAccount;
    }

    /**
     * Sets the digitalAccount
     * You can use getDigitalAccount() to get the value of digitalAccount
     */
    public void setDigitalAccount(String digitalAccount) {
        this.digitalAccount = digitalAccount;
    }

    /**
     * Gets the value of description and returns description
     *
     * @return Returns value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description
     * You can use getDescription() to get the value of description
     */
    public void setDescription(String description) {
        this.description = description;
    }

}
