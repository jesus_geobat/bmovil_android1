package suitebancomercoms.aplicaciones.bmovil.classes.constants;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;

/**
 * Created by evaltierrah on 19/07/16.
 */
public enum EstatusInstrumentoType {

    ACTIVE("A1"),
    DELIVERED_TO_CLIENT("AB"),
    LOST("AS"),
    STOLEN("AS"),
    BLOCKED_BY_CLIENT("AT"),
    BLOCKED_BY_BANK("AB"),
    NO_VALUE(Constants.EMPTY_STRING);

    String instrumentoEstatus;

    EstatusInstrumentoType(final String instrumentoEstatus) {
        this.instrumentoEstatus = instrumentoEstatus;
    }

    public String getInstrumentoEstatus() {
        return instrumentoEstatus;
    }

}
