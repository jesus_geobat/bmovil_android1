package suitebancomercoms.aplicaciones.bmovil.classes.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class AceptaOfertaEFI implements ParsingHandler{
	String tipoCuenta;
	String numeroTarjeta;
	String impDispuesto;
	String numeroCuenta;
	String plazoMeses;
	String nombreTitular;
	String fechaOperacion;
	String horaOperacion;
	String comisionEFIPorcentaje;
	String comisionEFIMonto;
	String pagoMensual;
	String fechaCat;
	String email;
	String Cat;
	String folioAST;
	String ofertaCruzada;
	Promociones promocion;
	Promociones[] promociones;
	String PM;
	String tasaMensual;
	
	
	
	public String getTipoTarjeta() {
		return tipoCuenta;
	}

	public void setTipoTarjeta(final String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	
	
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(final String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getImpDispuesto() {
		return impDispuesto;
	}

	public void setImpDispuesto(final String impDispuesto) {
		this.impDispuesto = impDispuesto;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(final String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getPlazoMeses() {
		return plazoMeses;
	}

	public void setPlazoMeses(final String plazoMeses) {
		this.plazoMeses = plazoMeses;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(final String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(final String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getHoraOperacion() {
		return horaOperacion;
	}

	public void setHoraOperacion(final String horaOperacion) {
		this.horaOperacion = horaOperacion;
	}

	public String getComisionEFIPorcentaje() {
		return comisionEFIPorcentaje;
	}

	public void setComisionEFIPorcentaje(final String comisionEFIPorcentaje) {
		this.comisionEFIPorcentaje = comisionEFIPorcentaje;
	}

	public String getComisionEFIMonto() {
		return comisionEFIMonto;
	}

	public void setComisionEFIMonto(final String comisionEFIMonto) {
		this.comisionEFIMonto = comisionEFIMonto;
	}

	public String getPagoMensual() {
		return pagoMensual;
	}

	public void setPagoMensual(final String pagoMensual) {
		this.pagoMensual = pagoMensual;
	}

	public String getFechaCat() {
		return fechaCat;
	}

	public void setFechaCat(final String fechaCat) {
		this.fechaCat = fechaCat;
	}

	public String getCat() {
		return Cat;
	}

	public void setCat(final String cat) {
		Cat = cat;
	}

	public String getFolioAST() {
		return folioAST;
	}

	public void setFolioAST(final String folioAST) {
		this.folioAST = folioAST;
	}

	public String getOfertaCruzada() {
		return ofertaCruzada;
	}

	public void setOfertaCruzada(final String ofertaCruzada) {
		this.ofertaCruzada = ofertaCruzada;
	}

	public Promociones getPromocion() {
		return promocion;
	}

	public void setPromocion(final Promociones promocion) {
		this.promocion = promocion;
	}

	public Promociones[] getPromociones() {
		if(Tools.isNull(promociones)) {
			return new Promociones[0];
		} else {
			return promociones.clone();
		}
	}

	public void setPromociones(final Promociones... promociones) {
		this.promociones = promociones;
	}
	
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}
	
	public String getPM() {
		return PM;
	}

	public void setPM(final String pM) {
		PM = pM;
	}
	
	public String getTasaMensual() {
		return tasaMensual;
	}

	public void setTasaMensual(final String tasaMensual) {
		this.tasaMensual = tasaMensual;
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		//tipoCuenta=parser.parseNextValue("tipoCuenta",false);
		numeroTarjeta=parser.parseNextValue("numeroTarjeta",false);
		impDispuesto=parser.parseNextValue("impDispuesto",false);
		numeroCuenta=parser.parseNextValue("numeroCuenta",false);
		plazoMeses=parser.parseNextValue("plazoMeses",false);
		nombreTitular=parser.parseNextValue("nombreTitular",false);
		fechaOperacion=parser.parseNextValue("fechaOperacion",false);
		comisionEFIPorcentaje=parser.parseNextValue("comisionEFIPorcentaje",false);
		comisionEFIMonto=parser.parseNextValue("comisionEFIMonto",false);
		email=parser.parseNextValue("email",false);
		pagoMensual=parser.parseNextValue("pagoMensual",false); 
		fechaCat=parser.parseNextValue("fechaCat",false); 
		Cat=parser.parseNextValue("Cat",false); 
		tasaMensual=parser.parseNextValue("tasaMensual",false); 
		folioAST=parser.parseNextValue("folioAST",false); 
		ofertaCruzada=parser.parseNextValue("ofertaCruzada",false);
		PM=parser.parseNextValue("PM");
		try {
			if(ofertaCruzada!=null){
				if(!ofertaCruzada.equals("NO")){
					final JSONObject jsonObject= new JSONObject(parser.parseNextValue("LO"));
					promocion= new Promociones();
					final String cveCamp=jsonObject.getString("cveCamp");
					promocion.setCveCamp(cveCamp);
					final String desOferta= jsonObject.getString("desOferta");
					promocion.setDesOferta(desOferta);
					final String monto=jsonObject.getString("monto");
					promocion.setMonto(monto);
				}
		}
			if(PM.equals("SI")){
			final JSONObject jsonObjectPromociones= new JSONObject(parser.parseNextValue("LP"));
			final JSONArray arrPromocionesJson =jsonObjectPromociones.getJSONArray("campanias");
			final ArrayList<Promociones> arrPromociones = new ArrayList<Promociones>();
			for (int i = 0; i < arrPromocionesJson.length(); i++) {
				final JSONObject jsonPromocion = (JSONObject) arrPromocionesJson.get(i);
				final Promociones promo = new Promociones();
				promo.setCveCamp(jsonPromocion.getString("cveCamp"));
				promo.setDesOferta(jsonPromocion.getString("desOferta"));
				promo.setMonto(jsonPromocion.getString("monto"));
				arrPromociones.add(promo);
			}			
			this.promociones = arrPromociones.toArray(new Promociones[arrPromociones.size()]);			
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			if(ServerCommons.ALLOW_LOG) e.printStackTrace();
		}
		
	}

}
