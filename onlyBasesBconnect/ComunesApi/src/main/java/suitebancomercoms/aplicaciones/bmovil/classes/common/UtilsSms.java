package suitebancomercoms.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by alanmichaelgonzalez on 18/08/16.
 */

public class UtilsSms {

    private static final String TAG = "UtilsSms";

    /**
     *
     * @param fechaHora en milisegundos ejm: System.currentTimeMillis()+"";
     * @param context
     * @param filtro una palabra o palabras que el sms debera contener
     * @return
     */
    public static ArrayList<String> fetchInbox(String fechaHora,Context context,String filtro) {
        ArrayList<String> sms = new ArrayList<String>();
        Uri uri = Uri.parse("content://sms/inbox");

        String address = "";
        if (ServerCommons.SIMULATION || ServerCommons.DEVELOPMENT) {
            address = "";
        }else {
            address = "address LIKE '%38240' OR address LIKE '%6110' OR address LIKE '%266845' OR address LIKE '%23636'";
        }
        Cursor cursor = context.getContentResolver().query(uri, new String[]{"_id", "address", "date", "body"}, address, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int index_Body = cursor.getColumnIndex("body");
            int index_Date = cursor.getColumnIndex("date");

            do {
                String strBody = cursor.getString(index_Body);

                Long dateSMSLong = null;
                Long dateSystemLong = null;

                try {
                    dateSMSLong = Long.parseLong(cursor.getString(index_Date));
                    dateSystemLong = Long.parseLong(fechaHora);
                }
                catch (Exception e) { }

                if (dateSMSLong == null || dateSystemLong == null) {
                    if (strBody != null && strBody.contains(filtro)) {
                        sms.add(strBody);
                       /* if (strBody.contains(":")) {
                            sms.add(strBody.substring(strBody.indexOf(":") + 1, strBody.indexOf(" ingresala")));
                        } else {
                            sms.add(strBody.substring(strBody.indexOf("es ") + 2, strBody.indexOf(" Dudas")));
                        }*/

                        break;
                    }
                }
                else {
                    try
                    {
                        Date dateSMS 	= new Date(dateSMSLong);
                        Date dateSystem = new Date(dateSystemLong);

                        if (strBody != null && strBody.contains(filtro)) {
                            if(dateSMS.after(dateSystem)) {
                                sms.add(strBody);
                                /*if (strBody.contains(":")) {
                                    sms.add(strBody.substring(strBody.indexOf(":") + 1, strBody.indexOf(" ingresala")));
                                } else {
                                    sms.add(strBody.substring(strBody.indexOf("es ") + 2, strBody.indexOf(" Dudas")));
                                }*/
                            }
                            else {
                                Log.e(TAG, "NO ES MAYOR LA FECGA DEL MENSAJE");
                            }

                            break;
                        }

                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } while (cursor.moveToNext());

            if (!cursor.isClosed()) {
                cursor.close();
            }
        }
        else {
            Log.e(TAG, "SMS not found :S !");
        }

        return sms;
    }

    /**
     *
     * @param fechaHora en milisegundos ejm: System.currentTimeMillis()+"";
     * @param context
     * @param filtro una palabra o palabras que el sms debera contener
     * @param delimitadores delimitador(palabra) inicial y final para encontrar la clave del mensaje
     * @return
     */
    public static ArrayList<String> fetchInbox(String fechaHora,Context context,String filtro, String[] delimitadores) {
        ArrayList<String> sms = new ArrayList<String>();
        Uri uri = Uri.parse("content://sms/inbox");

        String address = "";
        if (ServerCommons.SIMULATION || ServerCommons.DEVELOPMENT) {
            address = "";
        }else {
            address = "address LIKE '%38240' OR address LIKE '%6110' OR address LIKE '%266845' OR address LIKE '%23636'";
        }
        Cursor cursor = context.getContentResolver().query(uri, new String[]{"_id", "address", "date", "body"}, address, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int index_Body = cursor.getColumnIndex("body");
            int index_Date = cursor.getColumnIndex("date");

            do {
                String strBody = cursor.getString(index_Body);

                Long dateSMSLong = null;
                Long dateSystemLong = null;

                try {
                    dateSMSLong = Long.parseLong(cursor.getString(index_Date));
                    dateSystemLong = Long.parseLong(fechaHora);
                }
                catch (Exception e) { }

                if (dateSMSLong == null || dateSystemLong == null) {
                    if (strBody != null && strBody.contains(filtro)) {
                       if (strBody.contains(delimitadores[0]) && strBody.contains(delimitadores[1])) {
                           sms.add(strBody.substring(strBody.indexOf(delimitadores[0]) + delimitadores[0].length(), strBody.indexOf(" "+delimitadores[1])));
                       }
                        break;
                    }
                }
                else {
                    try
                    {
                        Date dateSMS 	= new Date(dateSMSLong);
                        Date dateSystem = new Date(dateSystemLong);

                        if (strBody != null && strBody.contains(filtro)) {
                            if(dateSMS.after(dateSystem)) {
                                if (strBody.contains(delimitadores[0]) && strBody.contains(delimitadores[1])) {
                                    sms.add(strBody.substring(strBody.indexOf(delimitadores[0]) + delimitadores[0].length(), strBody.indexOf(" "+delimitadores[1])));
                                }
                            }
                            else {
                                Log.e(TAG, "NO ES MAYOR LA FECGA DEL MENSAJE");
                            }

                            break;
                        }

                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } while (cursor.moveToNext());

            if (!cursor.isClosed()) {
                cursor.close();
            }
        }
        else {
            Log.e(TAG, "SMS not found :S !");
        }

        return sms;
    }

}
