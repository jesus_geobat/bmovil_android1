package suitebancomercoms.aplicaciones.bmovil.classes.constants;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;

/**
 * Created by evaltierrah on 19/07/16.
 */
public enum InstrumentoType {

    TAS_TS("TS"),
    TAS_DU("DU"),
    TASA_D2("D2"),
    TASA_D3("D3"),
    TASA_D4("D4"),
    TOKEN_UNIDENTIFIED("TK"),
    TOKEN_T1_GO3("T1"),
    TOKEN_T3_DP270("T3"),
    TOKEN_T5_EZIO("T5"),
    TOKEN_T6_OCRA("T6"),
    SOFTWARE_TOKEN("S1"),
    VIRTUAL_TOKEN("TV"),
    NO_VALUE(Constants.EMPTY_STRING);

    String instrumento;

    InstrumentoType(final String instrumento) {
        this.instrumento = instrumento;
    }

    public String getInstrumento() {
        return instrumento;
    }

}
