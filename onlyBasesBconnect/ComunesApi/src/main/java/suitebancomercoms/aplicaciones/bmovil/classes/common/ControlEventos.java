package suitebancomercoms.aplicaciones.bmovil.classes.common;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import java.util.List;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by evaltierrah on 05/09/2015.
 */
public final class ControlEventos {
    private static ControlEventos ourInstance = new ControlEventos();

    private static long milAnt;

    private static long milAct;

    private ControlEventos() {
        milAnt = System.currentTimeMillis();
    }

    public static ControlEventos getInstance() {
        return ourInstance;
    }

    public static boolean isScreenOn(final Activity activity) {
        milAct = System.currentTimeMillis();
        final PowerManager powerManager = (PowerManager) activity.getSystemService(Activity.POWER_SERVICE);
        final long current = milAct - milAnt;
        if(current < 1000 && !powerManager.isScreenOn()) {
            if(ServerCommons.ALLOW_LOG) {
                Log.d(ControlEventos.class.getName(), String.valueOf(current));
            }
            milAnt = System.currentTimeMillis();
            activity.finish();
            return true;
        }
        milAnt = System.currentTimeMillis();
        return powerManager.isScreenOn();
    }

    public static boolean isAppIsInBackground(final Context context) {
        boolean isInBackground = true;
        final ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            final List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (final ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (final String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            final List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            final ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

}
