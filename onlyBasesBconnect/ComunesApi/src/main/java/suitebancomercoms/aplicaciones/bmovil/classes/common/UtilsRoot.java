package suitebancomercoms.aplicaciones.bmovil.classes.common;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

/**
 * BMovil_v940
 * Created by terry0022 on 31/10/16 - 6:14 PM.
 */
public class UtilsRoot {

    private static final String TAG = "UtilRoot";

    /**
     * Saber si el dispositivo tiene usuario root
     * @return boolean true / false
     */
    public static boolean isDeviceRooted() {
        return checkRoot1() || checkRoot2() || checkRoot3() || checkRoot4();
    }

    /**
     * busca el tag test-keys
     * @return boolean true / false
     */
    private static boolean checkRoot1() {
        String buildTags = android.os.Build.TAGS;
        return buildTags != null && buildTags.contains("test-keys");
    }

    /**
     * busca aplicacion superuser, o modo su
     * @return boolean true / false
     */
    private static boolean checkRoot2() {
        String[] paths = { "/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }

    /**
     * busca comando su
     * @return boolean true / false
     */
    private static boolean checkRoot3() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[] { "/system/xbin/which", "su" });
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            return in.readLine() != null;
        } catch (Throwable t) {
            if (Server.ALLOW_LOG){
                Log.e(TAG, "checkRoot3: " + t.getMessage(), t.getCause());
            }
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

    /**
     * Intenta ejecutar comando su
     * @return boolean true / false
     */
    private static boolean checkRoot4(){
        Process process = null;
        try{
            process = Runtime.getRuntime().exec("su");
            return true;
        } catch (Exception e) {
            return false;
        } finally{
            if(process != null){
                try{
                    process.destroy();
                }catch (Exception e) {
                    if (Server.ALLOW_LOG){
                        Log.e(TAG, "checkRoot4: "+e.getMessage(), e.getCause());
                    }
                }
            }
        }
    }

}
