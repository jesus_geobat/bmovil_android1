package suitebancomercoms.aplicaciones.bmovil.classes.constants;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;

/**
 * Created by evaltierrah on 19/07/16.
 */
public enum ProfileType {

    BASIC_ALERTS(Constants.PROFILE_BASIC_00),
    BASIC(Constants.PROFILE_BASIC_01),
    LIMITED(Constants.PROFILE_RECORTADO_02),
    ADVANCED(Constants.PROFILE_ADVANCED_03),
    NO_VALUE(Constants.EMPTY_STRING);

    String perfil;

    ProfileType(final String perfil) {
        this.perfil = perfil;
    }

    public String getPerfil() {
        return perfil;
    }

}
