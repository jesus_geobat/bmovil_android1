package suitebancomercoms.aplicaciones.bmovil.classes.model;

/**
 * Created by joseangelgonzalezmagana on 08/08/16.
 */
public class Allowed {

    private String arquitectura;
    private String edocuenta;

    public String getEdocuenta() {
        return edocuenta;
    }

    public void setEdocuenta(String edocuenta) {
        this.edocuenta = edocuenta;
    }

    public String getArquitectura() {
        return arquitectura;
    }

    public void setArquitectura(String arquitectura) {
        this.arquitectura = arquitectura;
    }


}
