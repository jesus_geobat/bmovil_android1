package suitebancomercoms.classes.common;

import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.base.callback.CallBackSession;

/**
 * BMovil_v940
 * Created by terry0022 on 30/08/16 - 11:01 AM.
 */
public class FirmaAutografaCallback {
    private static FirmaAutografaCallback ourInstance = new FirmaAutografaCallback();
    private CallBackBConnect callBackBConnect;
    private CallBackSession callBackSession;
    public static FirmaAutografaCallback getInstance() {
        return ourInstance;
    }

    private FirmaAutografaCallback() {

    }

    public void setCallBackBConnect(CallBackBConnect CallBackSession){
        this.callBackBConnect=CallBackSession;
    }

    public CallBackBConnect getCallBackBConnect( ){
        return callBackBConnect;
    }

    public void setCallBackSession(CallBackSession callBackSession){
        this.callBackSession=callBackSession;
    }

    public CallBackSession getCallBackSession( ){
        return callBackSession;
    }


}
