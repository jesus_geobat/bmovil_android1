package suitebancomercoms.classes.gui.controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bancomer.base.R;

public class PopUpAvisoPrivacidadCommon extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_popup_aviso_privacidad);
        final String textURL="<font>BBVA Bancomer, S.A., Av. Paseo de la Reforma 510, Colonia Juárez, Delegación Cuauhtémoc, Código Postal 06600, Distrito Federal, recaba sus datos para verificar su identidad. El Aviso de Privacidad Integral actualizado está en cualquier sucursal y en </font><b><font color=#009EE5><a href=\"http://www.bancomer.com/personas/aviso-privacidad.jsp\">http://www.bancomer.com/personas/aviso-privacidad.jsp</a></font>";


        final TextView txtMessage = (TextView) findViewById(R.id.lblMessageAvisoPrivacidad);
        txtMessage.setText(Html.fromHtml(textURL));
        setTextViewHTML(txtMessage, textURL);
        /*txtMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTextViewHTML(txtMessage, textURL);
                Log.d("LINK CLICKEABLE", "http://www.bancomer.com/personas/aviso-privacidad.jsp");
            }
        });*/

        Button btnLate = (Button) findViewById(R.id.btnAceptarAvisoPrivacidad);
        btnLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent();
                i.putExtra("regresar", false);
                setResult(RESULT_OK, i);
                finish();
            }
        });



    }


    @Override
    protected void onResume() {
        super.onResume();
        //APICuentaDigital.appContext=this;
    }

    @Override
    public void onBackPressed() {
    }


    protected void setTextViewHTML(TextView text, String html)  {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);

        for(URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }

        text.setText(strBuilder);
        text.setMovementMethod(LinkMovementMethod.getInstance());
    }


    //Termina One click
    protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span)  {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);

        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                Intent intent = new Intent(PopUpAvisoPrivacidadCommon.this, WebViewController.class);
                intent.putExtra("url",span.getURL());
                startActivity(intent);
                finish();

            }
        };

        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

}
