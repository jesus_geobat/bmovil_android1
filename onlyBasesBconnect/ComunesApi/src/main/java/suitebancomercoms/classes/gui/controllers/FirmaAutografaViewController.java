package suitebancomercoms.classes.gui.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.base.R;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.FirmaAutografaCallback;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.delegates.FirmaDelegate;

/**
 * BMovil_v940
 * Created by terry0022 on 25/08/16 - 10:38 AM.
 */
public class FirmaAutografaViewController extends Activity implements View.OnClickListener{

    private TextView aceptar_terminos_label,aceptar_terminos_link,firmar_title;
    private CheckBox acepto_terminos_checkbox;
    private Button continuar;
    //    private ImageView ic_firma;
    public static final Integer SIGNATURE = 147;
    /* componente firma */
    FirmaDelegate firma = new FirmaDelegate(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_firma);

        aceptar_terminos_label = (TextView)findViewById(R.id.aceptar_terminos_label);
        aceptar_terminos_link = (TextView)findViewById(R.id.aceptar_terminos_link);
        firmar_title = (TextView)findViewById(R.id.firmar_title);
        acepto_terminos_checkbox = (CheckBox)findViewById(R.id.acepto_terminos_checkbox);
        continuar = (Button)findViewById(R.id.continuar);
        LinearLayout firmar_layout = (LinearLayout) findViewById(R.id.firmar_layout);

        firma.generateViewSignature(firmar_layout,SIGNATURE);

        scaleToScreenSize();
        firmar_title.setOnClickListener(this);
        continuar.setOnClickListener(this);

    }

    /**
     * Escala el layout al tamaño de la pantalla.
     */
    private void scaleToScreenSize() {
        final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(aceptar_terminos_link, true);
        guiTools.scale(aceptar_terminos_label, true);
        guiTools.scale(firmar_title, true);
        guiTools.scale(continuar, true);
    }

    @Override
    public void onClick(View v) {
        if (v == continuar){
            if (!acepto_terminos_checkbox.isChecked()){
                alertDialog("Error","Debes aceptar los terminos y condiciones","Aceptar");
                return;
            }
            FirmaAutografaCallback.getInstance().setCallBackSession(null);
            FirmaAutografaCallback.getInstance().setCallBackBConnect(null);
            setResult(RESULT_OK, new Intent());
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            FirmaAutografaCallback.getInstance().setCallBackSession(null);
            FirmaAutografaCallback.getInstance().setCallBackBConnect(null);
            setResult(RESULT_CANCELED, new Intent());
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == SIGNATURE){
                String base64 = firma.returnViewSignature(data.getExtras());
            }
        }
    }

    @Override
    public void onUserInteraction() {
        if(ServerCommons.ALLOW_LOG) {
            Log.i(getClass().getSimpleName(), "User Interacted firma Autografa");
        }
        if(FirmaAutografaCallback.getInstance().getCallBackSession() != null) {
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Envia peticion de reinicio a bmovil desde CallBackSession");
            }
            FirmaAutografaCallback.getInstance().getCallBackSession().userInteraction();
        }
        if( FirmaAutografaCallback.getInstance().getCallBackBConnect() != null) {
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Envia peticion de reinicio a bmovil desde CallBackBconnect");
            }
            FirmaAutografaCallback.getInstance().getCallBackBConnect().userInteraction();
        }
        super.onUserInteraction();
    }

    private void alertDialog(String title, String message, String okText ){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(okText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog mAlertDialog = alertDialogBuilder.create();
        mAlertDialog.show();
    }

}
