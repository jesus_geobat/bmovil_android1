package suitebancomercoms.classes.gui.delegates;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.FirmaAutografaViewController;

/**
 * BMovil_v940
 * Created by terry0022 on 29/08/16 - 10:29 AM.
 */
public class BaseDelegate extends BaseDelegateCommons {

    /**
            * Invoke a network operation, controlling that it is called after all previous
    * repaint events have been completed
    * @param operationId network operation identifier. See Server class.
            * @param params Hashtable with the parameters passed to the Server. See Server
    * class for parameter names.
            * @param caller the BaseScreen instance (that is, the screen), which requests the
    * network operation. Must be null if the caller is not a screen.
            */
    public void doNetworkOperation(final int operationId, final java.util.Hashtable<String,?> params, final FirmaAutografaViewController caller,final boolean callerHandlesError,final Class<?> ojbResponse) {
        //SuiteAppLogin.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params, caller, callerHandlesError,ojbResponse);
    }

    /**
     *
     * This function has to be overridden by child members, it implements
     * the processing for server response.
     *
     * @param operationId
     * @param response
     */
    public void analyzeResponse(final int operationId,final ServerResponse response) {}

    public void performAction(final Object obj) {}

    public long getDelegateIdentifier() { return 0; }

}
