package suitebancomercoms.classes.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bancomer.base.R;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;

public class WebViewController extends Activity {

    private String url = "https://www.bancomermovil.com/imgsp_mx_web/imgsp_mx_web/web/hsu/cd.html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_controller);

        final WebView mWebView = (WebView) findViewById(R.id.webview_object);
        mWebView.setWebViewClient(new WebViewClient());
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (null != bundle){
            url = bundle.getString(Constants.URL);
        }

        mWebView.loadUrl(url);
    }
}
