package suitebancomercoms.classes.gui.delegates;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bancomer.base.R;

import suitebancomercoms.classes.gui.controllers.FirmaViewController;

/**
 * BMovil_v940
 * Created by terry0022 on 01/09/16 - 3:51 PM.
 */
public class FirmaDelegate {

    private ImageView imageView;
    private Context context;
    private Fragment fragment;
    private android.support.v4.app.Fragment fragmentV4;

    /**
     * Constructor para la funcion de firma
     * @param context Contexto
     */
    public FirmaDelegate(Context context) {
        this.context = context;
    }

    /**
     * Constructor para la funcion de firma desde un fragment
     * @param context Contexto de la aplicación
     * @param fragment Fragment desde donde se manda llamar
     */
    public FirmaDelegate(Context context, Fragment fragment) {
        this.context = context;
        this.fragment = fragment;
    }

    /**
     * Constructor para la funcion de firma desde un fragmentV4
     * @param context Contexto de la aplicación
     * @param fragment Fragment desde donde se manda llamar
     */
    public FirmaDelegate(Context context, android.support.v4.app.Fragment fragment) {
        this.context = context;
        this.fragmentV4 = fragment;
    }

    /**
     * Generar componente que hace la peticion a firmar
     * @param linearLayout LinearLayout layout que se utilizará para colocar la imagen y el metodo que llamara a la firma
     * @param RESULT int codigo de peticion que al cual retornará el ActivityForResult
     */
    public void generateViewSignature(LinearLayout linearLayout, final int RESULT){
        ImageView imageView = new ImageView(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        imageView.setImageResource(R.drawable.ic_firma);
        imageView.setLayoutParams(params);
        imageView.setPadding(5, 0, 5, 0);
        linearLayout.setBackgroundResource(R.drawable.combo_firma);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(imageView);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragment != null)
                    fragment.startActivityForResult(new Intent(context, FirmaViewController.class), RESULT);
                else if (fragmentV4 != null)
                    fragmentV4.startActivityForResult(new Intent(context, FirmaViewController.class), RESULT);
                else
                    ((Activity) context).startActivityForResult(new Intent(context, FirmaViewController.class), RESULT);
            }
        });
        this.imageView = imageView;
    }

    /**
     * Regresa firma al componente
     * @param bundle Bundle Datos que retorna la firma.
     */
    public String returnViewSignature(Bundle bundle){
        String base = "";
        if (bundle != null){
            String path = bundle.getString(context.getString(R.string.path_key));
            base = bundle.getString(context.getString(R.string.base_key));
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            imageView.setImageBitmap(bitmap);
            return base;
        }
        return base;
    }

}
