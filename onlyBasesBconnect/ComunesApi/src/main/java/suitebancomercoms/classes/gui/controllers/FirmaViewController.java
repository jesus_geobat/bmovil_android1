package suitebancomercoms.classes.gui.controllers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bancomer.base.R;
import com.bancomer.base.SuiteApp;
import com.tutorialbbvasend.clases.TutorialBBVAView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import bancomer.api.common.commons.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.FirmaAutografaCallback;

/**
 * BMovil_v940
 * Created by terry0022 on 29/08/16 - 4:18 PM.
 */
public class FirmaViewController  extends Activity implements View.OnClickListener {

    private LinearLayout mContent,linear;
    private signature mSignature;
    private ImageButton mClear, mGetSign, mCancel;
    private ImageView mImage;
    public String base = "";
    private View mView;
    private File mypath;


    private ImageButton help_firma_autografa;
    private TutorialBBVAView tutorial;
    private RelativeLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_signature);

        mContent = (LinearLayout) findViewById(R.id.contenSignature);
        mSignature = new signature(this, null);
        mSignature.setBackgroundResource(R.drawable.combo_firma);
        mSignature.setVisibility(View.GONE);
        mImage = (ImageView)findViewById(R.id.image_signature);
        mImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mImage.setVisibility(View.GONE);
                mSignature.setVisibility(View.VISIBLE);
                mSignature.onTouchEvent(motionEvent);
                return true;
            }
        });
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mClear = (ImageButton)findViewById(R.id.button_clear_signature);
        mGetSign = (ImageButton)findViewById(R.id.button_save_signature);
        mCancel = (ImageButton)findViewById(R.id.button_cancel_signature);
        linear = (LinearLayout)findViewById(R.id.header_layout);
        assert mGetSign != null;
        mGetSign.setEnabled(false);
        mView = mContent;

        mClear.setOnClickListener(this);
        mGetSign.setOnClickListener(this);
        mCancel.setOnClickListener(this);

        rootLayout = (RelativeLayout) findViewById(R.id.master_layout);
        help_firma_autografa = (ImageButton) findViewById(R.id.ayuda_firma_autografa);

        tutorial = new TutorialBBVAView(this, rootLayout);

        help_firma_autografa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tutorial.initTutorial(
                        new int[]{R.id.contenSignature}, R.id.contenSignature,
                        "",
                        "Captura tu firma dentro del recuadro blanco.",
                        0, 0,
                        false, 4, true);
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == mClear){
            mSignature.clear();
        }else if(v == mGetSign){
            saveSignature();
        }else if(v == mCancel){
            //retorno con estado cancelado
            setResult(RESULT_CANCELED,new Intent());
            finish();
        }
    }

    /**
     * Guardar firma
     */
    private void saveSignature(){
        //se resetea el contorno de el canvas
        linear.setBackgroundResource(android.R.color.white);
        mSignature.setBackgroundResource(android.R.color.white);
        //se almacena el cache del dibujo
        mView.setDrawingCacheEnabled(true);
        //se almacena la imagen
        mSignature.save(mView);
        //creacion de un bundle para regresar variables
        Bundle b = new Bundle();
        //se envia el base64
        b.putString(getString(R.string.base_key),base);
        //se envia la direccion de la imagen en el dispositivo
        b.putString(getString(R.string.path_key), mypath.getAbsolutePath());
        //creacion de intent para colocarle el bundle
        Intent intent = new Intent();
        intent.putExtras(b);
        //retorno con estado ok
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Clase Signature
     * perimute guardar, limpiar la imagen en el canvas establecido
     */
    public class signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        /**
         * Constructor de la clase
         * @param context Context contexto de la aplicacion
         * @param attrs AttributeSet attributos extras
         */
        public signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        /**
         * Guardar
         * @param view View elemento view, que se almacenara en la memoria interna de telefono
         */
        public void save(View view){
            Bitmap bitmap = Bitmap.createBitmap (mContent.getWidth(), mContent.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            try{
                mypath = new File(Environment.getExternalStorageDirectory() + Constants.EMPTY_STRING,getString(R.string.image_name));
                FileOutputStream stream = new FileOutputStream(mypath);
                view.draw(canvas);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                base = generateB64(bitmap);
                stream.flush();
                stream.close();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * funcion para limpiar el canvas
         */
        public void clear() {
            path.reset();
            invalidate();
            mSignature.setVisibility(View.GONE);
            mImage.setVisibility(View.VISIBLE);
            mGetSign.setEnabled(false);
        }

        /**
         * Accion dibujar
         * @param canvas Canvas
         */
        @SuppressLint("ClickableViewAccessibility")
        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        /**
         *
         * @param event MotionEvent
         * @return Boolean true/false si existe movimiento registrado sobre el canvas
         */
        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);

            switch (event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;
                case MotionEvent.ACTION_MOVE:
                case MotionEvent.ACTION_UP:
                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;
                default:
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH), (int) (dirtyRect.top - HALF_STROKE_WIDTH), (int) (dirtyRect.right + HALF_STROKE_WIDTH), (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        /**
         * Movimiento del lapiz(dedo) al firma
         * @param historicalX float
         * @param historicalY float
         */
        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left)
                dirtyRect.left = historicalX;
            else if (historicalX > dirtyRect.right)
                dirtyRect.right = historicalX;
            if (historicalY < dirtyRect.top)
                dirtyRect.top = historicalY;
            else if (historicalY > dirtyRect.bottom)
                dirtyRect.bottom = historicalY;
        }

        /**
         * Limpiar linea
         * @param eventX float evento en eje x
         * @param eventY float evento en eje y
         */
        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }

    }

    /**
     * interaccion con el usuario
     */
    @Override
    public void onUserInteraction() {
        if(ServerCommons.ALLOW_LOG) {
            Log.i(getClass().getSimpleName(), "User Interacted firma Autografa");
        }
        if(FirmaAutografaCallback.getInstance().getCallBackSession() != null) {
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Envia peticion de reinicio a bmovil desde CallBackSession");
            }
            FirmaAutografaCallback.getInstance().getCallBackSession().userInteraction();
        }
        if( FirmaAutografaCallback.getInstance().getCallBackBConnect() != null) {
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Envia peticion de reinicio a bmovil desde CallBackBconnect");
            }
            FirmaAutografaCallback.getInstance().getCallBackBConnect().userInteraction();
        }
        super.onUserInteraction();
    }

    /**
     * Convertir Bitmap a Base64
     * @param bmap Bitmap imagen en formato para transformar a base64
     * @return String texto base64 de la imagen
     */
    private String generateB64(Bitmap bmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] byteFormat = stream.toByteArray();
        return Base64.encodeToString(byteFormat, Base64.NO_WRAP);
    }

}
