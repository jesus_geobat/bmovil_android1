package com.bancomer.apicheckupfinanciero.controller;

import android.app.Activity;
import android.content.Intent;



public class ActivityController {

	private Activity currentActivity;

	public Activity getCurrentActivity() {
		return currentActivity;
	}

	void setCurrentActivity(Activity currentActivity) {
		this.currentActivity = currentActivity;
	}
	
	public void showScreen(Class<?> activity) {
        //currentActivity.setActivityChanging(true);
		Intent intent = new Intent(currentActivity, activity);
		currentActivity.startActivity(intent);
	}
}
