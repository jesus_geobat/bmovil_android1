package com.bancomer.apicheckupfinanciero.gui.views.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.DepositExpensesFragment;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.DepositsFragment;
import com.bancomer.apicheckupfinanciero.models.Account;
import com.bancomer.apicheckupfinanciero.models.AccountCUF;
import com.bancomer.apicheckupfinanciero.utils.CustomerTextView;
import com.bancomer.apicheckupfinanciero.utils.Tools;

import java.util.ArrayList;

/**
 * Created by DMEJIA on 09/06/16.
 */
public class ExpandableListViewDepositsAdapter extends BaseExpandableListAdapter{

    private Fragment fragment;
    private CustomerTextView txtName;
    private CustomerTextView txtAmount;
    private ImageView imgArrow;

    private ExpandableListView listView;
    private int aux;

    private boolean flag;
    private boolean first;

    private ArrayList<AccountCUF> data;

    private RelativeLayout.LayoutParams params;

    public ExpandableListViewDepositsAdapter(Fragment fragment, ArrayList<AccountCUF> data){
        this.fragment = fragment;
        this.data = data;
        aux = -1;
        flag = true;
    }


    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if(data.size() > 0) {
            if (data.get(groupPosition) == null) {
                return 0;
            } else
                return data.get(groupPosition).getDescription().size();
        }else
            return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return data.get(groupPosition).getDescription().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) fragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_goup_item, null);
            first = true;
        }
        txtAmount = (CustomerTextView) convertView.findViewById(R.id.txtAmount);
        txtName = (CustomerTextView) convertView.findViewById(R.id.txtName);
        imgArrow = (ImageView) convertView.findViewById(R.id.imgArrow);

        if(first) {
            first = false;
        }
        if(isExpanded) {
            imgArrow.setImageDrawable(fragment.getActivity().getResources().getDrawable(R.drawable.arrow_up_cuf));
            txtAmount.setTextColor(fragment.getActivity().getResources().getColor(R.color.colorBlue1));
            txtName.setTextColor(fragment.getActivity().getResources().getColor(R.color.colorBlue1));
        }else{
            imgArrow.setImageDrawable(fragment.getActivity().getResources().getDrawable(R.drawable.flechita));
            txtAmount.setTextColor(fragment.getActivity().getResources().getColor(R.color.colorBlue2));
            txtName.setTextColor(fragment.getActivity().getResources().getColor(R.color.colorBlue2));
        }

        txtAmount.setText(Tools.decimalFormat(String.valueOf(data.get(groupPosition).getSaldo())));
        txtName.setText(Tools.getFormatAccount(data.get(groupPosition).getCuenta(), data.get(groupPosition).getTipo()));


        return convertView;
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        LayoutInflater inflater = fragment.getActivity().getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_goup_item, null);
        }

        RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.rlGroupItem);
        txtAmount = (CustomerTextView) convertView.findViewById(R.id.txtAmount);
        txtName = (CustomerTextView) convertView.findViewById(R.id.txtName);
        imgArrow = (ImageView) convertView.findViewById(R.id.imgArrow);

        txtAmount.setText(data.get(groupPosition).getDescription().get(childPosition).getAmount());
        txtName.setText(data.get(groupPosition).getDescription().get(childPosition).getName());

        imgArrow.setColorFilter(Color.WHITE);
        rl.setBackgroundColor(Color.WHITE);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        if(flag) {
            if (groupPosition != aux) {
                ((DepositExpensesFragment) fragment).getExpandableListView().collapseGroup(aux);
            }
            if((groupPosition >0) || (groupPosition == 0 && getGroupCount()==1)) {
                ((DepositExpensesFragment) fragment).setSelectedPie(groupPosition);
            }
        }
        aux = groupPosition;
        super.onGroupExpanded(groupPosition);
        ((DepositExpensesFragment)fragment).fullScroll();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        if(flag) {
            if((aux >0) || (aux == 0 && getGroupCount()==1)) {
                ((DepositExpensesFragment) fragment).setSelectedPie(groupPosition);
            }
        }
        super.onGroupCollapsed(groupPosition);
    }

    public void setData(ArrayList<AccountCUF> data) {
        this.data = data;
    }

    public void setFlag(boolean flag){
        this.flag = flag;
    }

    public boolean getFlag(){
        return flag;
    }
}
