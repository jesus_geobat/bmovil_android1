package com.bancomer.apicheckupfinanciero.utils.Tutorial;

/**
 * Created by DMEJIA on 06/04/16.
 */
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AnimationSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;

public class TutorialDelegate {
    private View pantallaPrincipal;
    private RelativeLayout fondoTutorial;
    private FondoTutorial fondoAnimado;
    private String[] textosDescriptivos;
    private int[] idViews;
    private int totalNumPasos;
    private int pasoActual;
    private AnimationSet animInOut;
    private TextView txtDescripcionPaso;
    private ImageView imgFlecha;
    private ImageView imgMano;
    private int anchoFlecha;
    private int altoFlecha;
    private int maxAncho;
    private int alturaBar;

    public TutorialDelegate(View pantallaPrincipal, String[] textosDescriptivos, int[] idViews) {
        this.pantallaPrincipal = pantallaPrincipal;
        this.textosDescriptivos = textosDescriptivos;
        this.idViews = idViews;
        this.totalNumPasos = textosDescriptivos.length - 1;
        this.pasoActual = 0;
       /* this.animInOut = new AnimationSet(false);
        AlphaAnimation fadeIn = new AlphaAnimation(0.0F, 1.0F);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(1500L);
        AlphaAnimation fadeOut = new AlphaAnimation(1.0F, 0.0F);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setStartOffset(4000L);
        fadeOut.setDuration(1500L);
        fadeOut.setAnimationListener(new AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                if(TutorialDelegate.this.pasoActual < TutorialDelegate.this.totalNumPasos) {
                    TutorialDelegate.this.imgFlecha.setVisibility(View.INVISIBLE);
                    TutorialDelegate.this.txtDescripcionPaso.setVisibility(View.INVISIBLE);
                    TutorialDelegate.this.pasoSiguiente(TutorialDelegate.this.pasoActual + 1);
                } else {
                    TutorialDelegate.this.finalizaTutorial();
                }

            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        this.animInOut.addAnimation(fadeIn);
        this.animInOut.addAnimation(fadeOut);*/
    }

    public void iniciaTuto(View[] view, RelativeLayout fondoTutorial, FondoTutorial fondoAnimado) {
        this.fondoTutorial = fondoTutorial;
        this.fondoAnimado = fondoAnimado;
        fondoTutorial.setVisibility(View.VISIBLE);
        this.txtDescripcionPaso = (TextView)view[0];
        this.imgFlecha = (ImageView)view[1];
        this.imgMano = (ImageView)view[2];
        this.anchoFlecha = this.imgFlecha.getWidth();
        this.altoFlecha = this.imgFlecha.getHeight();
        this.maxAncho = this.pantallaPrincipal.getWidth() - this.anchoFlecha;
        int[] coordsMain = new int[2];
        this.pantallaPrincipal.getLocationOnScreen(coordsMain);
        this.alturaBar = coordsMain[1];
        this.pasoSiguiente(0);
    }

    public void pasoSiguiente(int paso) {
        this.pasoActual = paso;
        this.txtDescripcionPaso.setText(this.textosDescriptivos[paso]);
        View vistaObjetivo = null;
        boolean esVistaEspecial = this.idViews[paso] == -1;
        if(!esVistaEspecial) {
            vistaObjetivo = this.pantallaPrincipal.findViewById(this.idViews[paso]);
        }

        if(vistaObjetivo != null) {
            int[] coordenadas = new int[2];
            vistaObjetivo.getLocationInWindow(coordenadas);
            android.util.Log.w("medidas", "imagen tuto:" + coordenadas[0]);
            android.util.Log.w("medidas", "imagen tuto:" + coordenadas[1]);
            this.fondoAnimado.changeArea(0, coordenadas[0], coordenadas[1] - this.alturaBar, coordenadas[0] + vistaObjetivo.getWidth(), coordenadas[1] + vistaObjetivo.getHeight() - this.alturaBar);
            int[] coordFlecha;
            if(this.estaArriba(coordenadas[1])) {
                this.imgFlecha.setImageResource(R.drawable.arriba);
                coordFlecha = this.getPosFlecha(coordenadas, vistaObjetivo, true);
                this.setMarginsText(this.txtDescripcionPaso, this.getLeftTxt(coordFlecha[0]), coordFlecha[1] + this.imgFlecha.getHeight(), true);
            } else {
                this.imgFlecha.setImageResource(R.drawable.abajo);
                coordFlecha = this.getPosFlecha(coordenadas, vistaObjetivo, false);
                this.setMarginsText(this.txtDescripcionPaso, this.getLeftTxt(coordFlecha[0]), coordFlecha[1], false);
            }

            setMargins(this.imgFlecha, coordFlecha[0], coordFlecha[1], 0);
            this.imgFlecha.setVisibility(View.VISIBLE);
           // this.imgFlecha.startAnimation(this.animInOut);
        } else if(esVistaEspecial) {
            this.fondoAnimado.changeArea(0, this.pantallaPrincipal.getWidth() - 180, 0, 80, this.pantallaPrincipal.getWidth());
            this.imgFlecha.setImageResource(R.drawable.arriba);
            setMargins(this.txtDescripcionPaso, this.pantallaPrincipal.getWidth() - this.anchoFlecha, 80, 0);
            this.imgFlecha.setVisibility(View.VISIBLE);
           // this.imgFlecha.startAnimation(this.animInOut);
        }

        this.txtDescripcionPaso.setVisibility(View.VISIBLE);
        //this.txtDescripcionPaso.startAnimation(this.animInOut);
    }

    public void nextStep() {
        if(this.pasoActual < this.totalNumPasos) {
            this.imgFlecha.clearAnimation();
            this.txtDescripcionPaso.clearAnimation();
            this.imgFlecha.setVisibility(View.INVISIBLE);
            this.txtDescripcionPaso.setVisibility(View.INVISIBLE);
            this.pasoSiguiente(this.pasoActual + 1);
        } else {
            this.finalizaTutorial();
        }

    }

    private void finalizaTutorial() {
        this.fondoAnimado.finalizaAnim();
        this.txtDescripcionPaso.setVisibility(View.INVISIBLE);
        this.imgFlecha.setVisibility(View.INVISIBLE);
        this.imgMano.setVisibility(View.INVISIBLE);
        this.fondoTutorial.setVisibility(View.INVISIBLE);
        this.pasoActual = 0;
    }

    private int[] getPosFlecha(int[] coordVista, View vista, boolean estaArriba) {
        int[] coord = new int[2];
        if(coordVista[0] > this.maxAncho) {
            coord[0] = this.maxAncho;
        } else if(vista.getWidth() > this.anchoFlecha) {
            coord[0] = coordVista[0] + (vista.getWidth() - this.anchoFlecha) / 2;
        } else {
            coord[0] = coordVista[0];
        }

        if(estaArriba) {
            coord[1] = coordVista[1] + vista.getHeight() - this.alturaBar;
        } else {
            coord[1] = coordVista[1] - this.altoFlecha - this.alturaBar;
        }

        return coord;
    }

    private static void setMargins(View v, int l, int t, int b) {
        if(v.getLayoutParams() instanceof MarginLayoutParams) {
            MarginLayoutParams p = (MarginLayoutParams)v.getLayoutParams();
            p.setMargins(l, t, 0, b);
            v.requestLayout();
        }

    }

    private void setMarginsText(View v, int l, int t, boolean estaArriba) {
        LayoutParams layout = (LayoutParams)v.getLayoutParams();
        if(estaArriba) {
            layout.addRule(2, 0);
            setMargins(v, l, t, 0);
        } else {
            layout.addRule(2, this.imgFlecha.getId());
            setMargins(v, l, 0, 0 - t);
        }

    }

    private int getLeftTxt(int marginLeft) {
        int margenMax = (int)((double)this.pantallaPrincipal.getWidth() * 0.6D);
        return marginLeft < 20?0:(marginLeft > margenMax?margenMax:marginLeft - 20);
    }

    private boolean estaArriba(int altura) {
        return this.pantallaPrincipal.getHeight() / 2 > altura;
    }
}

