package com.bancomer.apicheckupfinanciero.models;

import java.io.Serializable;

/**
 * Created by DMEJIA on 17/06/16.
 */
public class ProductBase implements Serializable {
    private String errorInfo;

    public ProductBase(){
        errorInfo = "";
    }

    public String getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo) {
        this.errorInfo = errorInfo;
    }
}
