package com.bancomer.apicheckupfinanciero.gui.Delegates;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.controller.InitCheckUp;
import com.bancomer.apicheckupfinanciero.gui.views.activities.MainActivity;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.DepositExpensesFragment;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.TotalPaymentsFragment;
import com.bancomer.apicheckupfinanciero.io.Server;
import com.bancomer.apicheckupfinanciero.models.CheckUp;
import com.bancomer.apicheckupfinanciero.models.CheckUpValues;
import com.bancomer.apicheckupfinanciero.models.Payments;
import com.bancomer.apicheckupfinanciero.persistence.BBVACheckUpSharedPreferences;
import com.bancomer.apicheckupfinanciero.utils.CustomerTextView;
import com.bancomer.apicheckupfinanciero.utils.Tools;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;

public class TotalPaymentsDelegate extends BaseDelegateImp {

    private TotalPaymentsFragment fragment;
    private RelativeLayout rlMessageTip;
    private MainActivity mainActivity;
    private View rootView;

    private int radious;
    private int[] initRadious;

    private double total;

    private boolean disponible;
    private boolean showMessageTuto;

    private BBVACheckUpSharedPreferences preferences;


    private DisplayMetrics metrics;

    public TotalPaymentsDelegate(TotalPaymentsFragment fragment, View view, MainActivity mainActivity) {
        this.fragment = fragment;
        initRadious = new int[2];
        initRadious[1] = 0;
        disponible = false;
        rootView = view;
        this.mainActivity = mainActivity;
        metrics = mainActivity.getResources().getDisplayMetrics();
        preferences = BBVACheckUpSharedPreferences.getTheInstance();

    }


    public PieChart configurePieChart(PieChart pieChart, Payments payments) {
        // if(count < fragment.getNumHalos()) {
        String cant;
        double amount = payments.getAmount();
        int color = getColor(payments.getClve());
        pieChart.setRotationEnabled(false);
        try {
            if (!InitCheckUp.getInstance().getPreferences().getBooleanData(ConstantsCUF.SHOW_TUTORIAL))
                pieChart.animateXY(ConstantsCUF._VALUE_XY, ConstantsCUF._VALUE_XY);
        }catch (NullPointerException e){}

        pieChart.setRotationAngle(270);
        pieChart.setDescriptionColor(color);
        pieChart.setHoleRadius(radious);
        pieChart.setRadius(radious);

        int border = (int) Tools.get_border();
        pieChart.setBorderHeigth(fragment.getNumHalos() == 5 ? border - 2 : border);
        pieChart.setNoDataTextDescription(" " + getMessage(payments.getClve()));
        ArrayList<Entry> valsY = new ArrayList<Entry>();

        ArrayList<Integer> colors = new ArrayList<Integer>();

        double percentage = calculatePercentage(amount>0? amount:0);

        //TODO Se valida si se muestran los dos colores en el halo de "Saldo Inicial + Depósitos
        if (payments.getClve().equals(ConstantsCUF.TAG_DEPOSITO)) {

            double saldoI = fragment.isShowCredit() == true ? ConstantsCUF._VALUE_ZERO : fragment.getSaldoInicial();
            if (Server.ALLOW_LOG) {
                android.util.Log.w(getClass().getSimpleName(), String.format("saldoInicial: %s depósitos: %s", saldoI, amount));
            }
            if (saldoI > ConstantsCUF._VALUE_ZERO) {
                valsY.add(new Entry((int) saldoI, ConstantsCUF._VALUE_ZERO));
                colors.add(color);
            } else {
                valsY.add(new Entry( (int)amount, ConstantsCUF._VALUE_ZERO));
                colors.add(fragment.getResources().getColor(R.color.colorSaldo));
            }

            if (saldoI > ConstantsCUF._VALUE_ZERO && amount > ConstantsCUF._VALUE_ZERO) {
                valsY.add(new Entry((int) amount, ConstantsCUF._VALUE_ONE));
                colors.add(fragment.getResources().getColor(R.color.colorSaldo));
            }

            cant = Tools.addDecimals(Math.floor(amount + saldoI));
            pieChart.setDescription((amount + saldoI) > 0 ? cant : ConstantsCUF.EMPTY);
            percentage = calculatePercentage(amount + saldoI);
        } else {
            cant = Tools.addDecimals(Math.floor(amount));
            pieChart.setDescription(amount > 0 ? cant : payments.getClve().equals(ConstantsCUF.TAG_AHORRO)? cant:ConstantsCUF.EMPTY);
            valsY.add(new Entry((float) percentage, ConstantsCUF._VALUE_ZERO));
        }

        pieChart.setxAumount(Tools.getxAmount());
        pieChart.setxConcep(Tools.getxConcep());
        pieChart.setTextSize(Tools.getSizeText());

        PieDataSet set1 = new PieDataSet(valsY, ConstantsCUF.EMPTY);
        if (payments.getClve().equals(ConstantsCUF.TAG_DEPOSITO)) {
            set1.setColors(colors);
        } else
            set1.setColor(color);

        PieData data = new PieData();
        data.addDataSet(set1);


        //pieChart.setMaxAngle(270);
        if (payments.getClve().equals(ConstantsCUF.TAG_AHORRO)) {
            initRadious[1] = radious;
            disponible = true;
        }
        radious = radious - (fragment.getNumHalos() == 5 ? 12 : 15);
        pieChart.setVisibility(View.VISIBLE);
        //   count++;
        // }
        pieChart.setMaxAngle((float) calculateAngle(percentage));
        pieChart.setData(data);
        pieChart.highlightValues(null);

        pieChart.invalidate();

        android.util.Log.i(getClass().getSimpleName(), "fragment heigthHalos: "+ fragment.getHeigthHalos());
        if (fragment.getHeigthHalos() > ConstantsCUF._VALUE_ZERO) {
            RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) pieChart.getLayoutParams();
            p.height = (int) fragment.getHeigthHalos();
            pieChart.setLayoutParams(p);
        }
        return pieChart;
    }

    private int[] calculatePercentageSyD(double depositos) {
        int arrPercentage[] = new int[2];

        double saldoI = fragment.getSaldoInicial();
        double total = saldoI + depositos;

        arrPercentage[0] = (int) (saldoI * ConstantsCUF.PERCENTAGE_100 / total);
        arrPercentage[1] = (int) (depositos * ConstantsCUF.PERCENTAGE_100 / total);
        return arrPercentage;
    }


    public void setRadious() {
        switch (fragment.getNumHalos()) {
            case ConstantsCUF._VALUE_ONE:
                radious = ConstantsCUF.PERCENTAGE_50;//Tools.isLow() ? ConstantsCUF.PERCENTAGE_71:ConstantsCUF.PERCENTAGE_50;
                break;
            case ConstantsCUF._VALUE_TWO:
                radious = ConstantsCUF.PERCENTAGE_65;//Tools.isLow() ? ConstantsCUF.PERCENTAGE_84:ConstantsCUF.PERCENTAGE_65;
                break;
            case ConstantsCUF._VALUE_THREE:
                radious = ConstantsCUF.PERCENTAGE_80;//Tools.isLow() ? ConstantsCUF.PERCENTAGE_97:ConstantsCUF.PERCENTAGE_80;
                break;
            case ConstantsCUF._VALUE_FOUR:
                radious = ConstantsCUF.PERCENTAGE_90;//Tools.isLow() ? ConstantsCUF.PERCENTAGE_110:ConstantsCUF.PERCENTAGE_95;
                break;
            case ConstantsCUF._VALUE_FIVE:
                radious = ConstantsCUF.PERCENTAGE_90;//Tools.isLow() ? ConstantsCUF.PERCENTAGE_110:ConstantsCUF.PERCENTAGE_95;
                break;
        }
        initRadious[0] = radious;
    }

    public int[] getInitRadious() {
        return initRadious;
    }

    private double calculateAngle(double percentage) {
        return (percentage * ConstantsCUF.GRADES_360) / ConstantsCUF.PERCENTAGE_100;
    }

    private double calculatePercentage(double value) {
        return (value * ConstantsCUF.PERCENTAGE_100 / total);
    }

    public void calculateMax(double a, double b, double c, double d, double i) {
        double max;
        if (a > b) {
            max = a;
        } else
            max = b;

        if (c > max)
            max = c;
        else if (d > max)
            max = d;

        if (i > max)
            max = i;

        total = max * ConstantsCUF.PERCENTAGE_100 / ConstantsCUF.PERCENTAGE_75;

    }

    private int getColor(String op) {
        if (op.equals(ConstantsCUF.TAG_AHORRO))
            return Tools.getIntColor(fragment.getStatus(op), mainActivity);
            //return fragment.getResources().getColor(R.color.colorAhorros);//Color.rgb(137, 209, 243);
        else if (op.equals(ConstantsCUF.TAG_PAGO_CREDITO)) {
            String salud = fragment.getStatus(op);
            if (salud.equalsIgnoreCase(ConstantsCUF._MALESTARES) || salud.equalsIgnoreCase(ConstantsCUF._CRITICO))
                return Tools.getIntColor(salud, mainActivity);
            else
                return fragment.getResources().getColor(R.color.colorCreditos);//Color.rgb(82, 188, 236);
        }else if (op.equals(ConstantsCUF.TAG_GASTO)) {
            String salud = fragment.getStatus(op);
            if (salud.equalsIgnoreCase(ConstantsCUF._MALESTARES) || salud.equalsIgnoreCase(ConstantsCUF._CRITICO))
                return Tools.getIntColor(salud, mainActivity);
            else
                return fragment.getResources().getColor(R.color.colorGastos);//Color.rgb(0, 158, 229);
        }else if (op.equals(ConstantsCUF.TAG_DEPOSITO))
            return fragment.getResources().getColor(R.color.colorDepositos);//Color.rgb(0, 125, 204);
        else
            return fragment.getResources().getColor(R.color.statusExcelente);

    }

    private String getMessage(String op) {
        if (op.equals(ConstantsCUF.TAG_AHORRO))
            return ConstantsCUF.AHORRO;
        else if (op.equals(ConstantsCUF.TAG_PAGO_CREDITO))
            return ConstantsCUF.PAGO_CREDITO_MES;
        else if (op.equals(ConstantsCUF.TAG_GASTO))
            return ConstantsCUF.GASTO;
        else if (op.equals(ConstantsCUF.TAG_INVERSION))
            return ConstantsCUF.INVERSION;
        else if (op.equals(ConstantsCUF.TAG_DEPOSITO))
            return fragment.isShowCredit() == true ? ConstantsCUF.DEPOSITO : "Saldo Inicial &";
        else
            return ConstantsCUF.EMPTY;

    }

    public ArrayList<Payments> orderLoands() {
        ArrayList<Payments> payments = fragment.getArrayLoands();

        Comparator<Payments> comparator = new Comparator<Payments>() {
            @Override
            public int compare(Payments lhs, Payments rhs) {

                return lhs.getAmount().compareTo(rhs.getAmount());
            }
        };
        Collections.sort(payments, comparator);
        return payments;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void realizaOperacion() {
        String numeroCelular = InitCheckUp.getInstance().getSession().getUsername();
        Map<String, String> paramsUrl = new HashMap<String, String>();
        paramsUrl.put("{cellphoneNumber}", numeroCelular);
        //paramTable.put(ApiConstants.USR_STRING, InitCheckUp.getInstance().getSession().getUsername());
        //paramTable.put(ConstantsCUF.TAG_MES, opId == Server.CONSULTA_ESTADO_FIANCIERO_MA ?"MA": "MIA");
        if(Server.ALLOW_LOG)
            android.util.Log.w("paramsUrl", numeroCelular);
        doNetworkOperation(Server.CONSULTA_ESTADO_FIANCIERO_MIA, paramsUrl, this);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse serverResponse) {
        if (operationId == Server.CONSULTA_ESTADO_FIANCIERO_MIA) {
            if (serverResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                CheckUp cltaEdoFinanciero = (CheckUp) serverResponse.getResponse();

                String deposito = cltaEdoFinanciero.getDeposito();
                String gasto = cltaEdoFinanciero.getGasto().replace("-", ConstantsCUF.EMPTY);
                gasto = Double.parseDouble(gasto) < 0 ? String.valueOf(Double.parseDouble(gasto) * -1) : gasto;
                String ahorro = cltaEdoFinanciero.getAvailable();
                String inversion = cltaEdoFinanciero.getInversion();
                String saldoI = cltaEdoFinanciero.getBalanceI();
                saldoI = saldoI.contains("-") ? ConstantsCUF.ZERO : saldoI;
                // ahorro = cltaEdoFinanciero.getAhorro();
                String pagoCreditos = cltaEdoFinanciero.getPagoCreditos();
                Tools.selectedMonth = ConstantsCUF.TAG_LAST_MONTH;
                fragment.selectMIA(deposito, gasto, ahorro, pagoCreditos, inversion, saldoI, cltaEdoFinanciero, cltaEdoFinanciero.isPagaCreditos());
                ((DepositExpensesFragment) (mainActivity.getAdapter().getItem(1))).setDataCheckUPMIA(cltaEdoFinanciero);
            } else {
                Tools.selectedMonth = ConstantsCUF.TAG_CURRENT_MONTH;
                Tools.showErrorMessage(serverResponse.getMessageText(),
                        InitCheckUp.getInstance().getActivityController().getCurrentActivity(),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((MainActivity) fragment.getActivity()).setCurrentItem(0);
                            }
                        });
            }
        }
    }

    @Override
    public void doNetworkOperation(int i, Hashtable<String, ?> hashtable, BaseViewController baseViewController) {
        super.doNetworkOperation(i, hashtable, baseViewController);
    }

    public void doNetworkOperation(int i, Map<String, String>  hashtable, BaseDelegate baseViewController) {
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put(ApiConstants.IUM_STRING, InitCheckUp.getInstance().getSession().getIum());
        InitCheckUp.getInstance().getBaseSubapplication().invokeNetworkOperation(i, hashtable, headers, baseViewController, new CheckUp(), "Operación en curso", "Conectando con servidor remoto", false);

    }

    public int getRadious() {
        return radious;
    }

    public void showTip(String message, final String message2,final int tip) {
        boolean showMessage = showMessage(tip);
        if (Server.ALLOW_LOG)
            android.util.Log.i(getClass().getSimpleName(), String.format("showTip  mensaje: %s num tip: %d", message, tip));
        if (!message.equalsIgnoreCase(ConstantsCUF.EMPTY) && (showMessage || showMessageTuto)) {
            rlMessageTip = (RelativeLayout) rootView.findViewById(R.id.rlMessageTip);
            final CustomerTextView textView = (CustomerTextView) rootView.findViewById(R.id.txtMessageTip);
            final ImageView txtOk = (ImageView) rootView.findViewById(R.id.imgCloseTip);


            textView.setTextColor(rootView.getResources().getColor(R.color.colorTutorial));
            textView.setText(Html.fromHtml(message), TextView.BufferType.SPANNABLE);

            textView.setTypeface(Typeface.createFromAsset(fragment.getActivity().getAssets(), "stag_sans_book.ttf"));
            txtOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        closeTip(tip);
                        rlMessageTip.setVisibility(View.GONE);
                        fragment.changeNumTip();

                }
            });
            //calculatePsitionX(txtOk);
            calculatePositionMessage(rlMessageTip, textView);
            fragment.dimensPie();
            rlMessageTip.setVisibility(View.VISIBLE);
            //fragment.centerGraphic();
        } else {
            if(rlMessageTip != null){
                rlMessageTip.setVisibility(View.GONE);
            }
            //fragment.centerGraphic();
        }
        showMessageTuto = false;
        //fragment.setVisibilityBtnCoach();
    }

    private boolean showMessage(int tip) {
        BBVACheckUpSharedPreferences preferences = BBVACheckUpSharedPreferences.getTheInstance();
        boolean isShow = false;
        switch (tip) {
            case ConstantsCUF._VALUE_ONE:
                isShow = preferences.getBooleanData(ConstantsCUF.SHOW_TIPMA);
                break;
            case ConstantsCUF._VALUE_TWO:
                isShow = preferences.getBooleanData(ConstantsCUF.SHOW_TIPMIA);
                break;
            case ConstantsCUF._VALUE_THREE:
                isShow = preferences.getBooleanData(ConstantsCUF.SHOW_TIPSCMIA);
                break;
        }
        if(Server.ALLOW_LOG)
            android.util.Log.i(getClass().getSimpleName(), String.format("tip: %d debe verse: %s", tip, isShow));
        return isShow;
    }

    private void closeTip(int tip) {
        if(Server.ALLOW_LOG)
            android.util.Log.e(getClass().getSimpleName(), "Tip a cerrar: " + tip);
        switch (tip) {
            case ConstantsCUF._VALUE_ONE:
                preferences.setBooleanData(ConstantsCUF.SHOW_TIPMA, false);
                break;
            case ConstantsCUF._VALUE_TWO:
                preferences.setBooleanData(ConstantsCUF.SHOW_TIPMIA, false);
                break;
            case ConstantsCUF._VALUE_THREE:
                preferences.setBooleanData(ConstantsCUF.SHOW_TIPSCMIA, false);
                break;
        }
    }


    private void calculatePositionMessage(final RelativeLayout rlTip, final TextView txt) {
        rlTip.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) rlTip.getLayoutParams();
                float circumference = Math.min(fragment.getHeigthHalos(), metrics.widthPixels);
                float margin = circumference == metrics.widthPixels ? fragment.getHeigthHalos() - metrics.widthPixels : ConstantsCUF._VALUE_ZERO;

                float positionHalo = ((circumference / ConstantsCUF._VALUE_TWO) * initRadious[ConstantsCUF._VALUE_ZERO]) / ConstantsCUF.PERCENTAGE_100;

                float m = 1 * metrics.scaledDensity;
                float marginTop = (fragment.getHeigthHalos() / 2) + positionHalo + (fragment.isShowCredit() == true ? 0 : Tools.heigthText - 10) + m;
                float space = calculateSpaceGraphic(marginTop);
                float h = Math.min(rlTip.getHeight(), space);

                if (space < rlTip.getHeight()) {
                    p.setMargins(0, (int) marginTop, 0, 0);
                    rlTip.setLayoutParams(p);

                    //se pone margen con la grafica de 10sp y hay scroll
                } else {
                    float top = space - rlTip.getHeight();
                    //marginTop -= top;
                    p.setMargins(0, (int) marginTop, 0, 0);

                    rlTip.setLayoutParams(p);
                    //se pone margen con la grafica de 10 sp
                }

                p.setMargins(0, (int) marginTop, 0, 0);
                rlTip.setLayoutParams(p);

                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                    rlTip.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    rlTip.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
    }

    private float calculateSpaceGraphic(float margin) {
        float totalSpace = fragment.getHeigthHalos()
                - (margin
                + mainActivity.getHeigthCirclePager())
                + 140 * metrics.scaledDensity;
        return totalSpace;
    }

    private void calculatePsitionX(final ImageView img) {
        img.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                float imgW = img.getWidth();
                float marginTxt = 10 * metrics.scaledDensity;

                float margin = marginTxt - (imgW / 3);

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) img.getLayoutParams();

                params.setMargins(0, (int) margin, (int) margin, 0);

                img.setLayoutParams(params);

                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                    img.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    img.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });

    }

    public int changeVisibilityTipFromTurotial() {
        if (rlMessageTip == null) {
            showMessageTuto = true;
            return ConstantsCUF._VALUE_ONE;
        } else if (!rlMessageTip.isShown()){
            showMessageTuto = false;
            return ConstantsCUF._VALUE_TWO;
        }else{
            showMessageTuto = false;
            return ConstantsCUF._VALUE_THREE;
        }
    }

    public RelativeLayout getRlMessageTip(){
        return rlMessageTip;
    }

    public CheckUpValues copyData(String status,String statusSC, String statusGD,double d, double g, double pc, double a, double s, double i,boolean payCredits){

        CheckUpValues values = new CheckUpValues();
        values.setStatus(status);
        values.setStatusSC(statusSC);
        values.setStatusDG(statusGD);
        values.setDeposito(d);
        values.setGasto(g);
        values.setPc(pc);
        values.setDisponible(a);
        values.setSaldo(s);
        values.setPayCredits(payCredits);


        if(Server.ALLOW_LOG)
            android.util.Log.i(getClass().getSimpleName(), String.format("getDataFromBundle() \n saldo inicial: %s\n depósitos: %s\n disponible: %s\n inversiones:  %s\n pado de créditos: %s \n" +
                    "Gasto: %s\""  , s, d, a, i, pc, g));

        return values;
    }

}
