package com.bancomer.apicheckupfinanciero.utils.LineTime;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.github.mikephil.charting.utils.Utils;

/**
 * Created by DMEJIA on 02/05/16.
 */
public class LineTime extends View{

    private float totalDays;
    private float paymentDate;

    private float width;
    private float height;

    private float strokeWidth;
    private float strokeWidthIndicator;


    private String amount;
    private String date;

    private Paint line;
    private Paint indicator;
    private Paint fecha;
    private Paint monto;

    private Rect bounds;



    public LineTime(Context context) {
        this(context,null);
    }

    public LineTime(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void init(){
        strokeWidth = Utils.convertDpToPixel(10f);
        strokeWidthIndicator = Utils.convertDpToPixel(3f);
        line = new Paint(Paint.DITHER_FLAG);
        line.setStyle(Paint.Style.STROKE);
        line.setStrokeCap(Paint.Cap.ROUND);
        line.setStrokeWidth(strokeWidth);
        line.setColor(Color.GREEN);

        indicator = new Paint(Paint.DITHER_FLAG);
        indicator.setStyle(Paint.Style.STROKE);
        indicator.setStrokeCap(Paint.Cap.ROUND);
        indicator.setStrokeWidth(strokeWidthIndicator);
        indicator.setColor(Color.GREEN);

        fecha  = new Paint(Paint.ANTI_ALIAS_FLAG);
        fecha.setColor(Color.GRAY);
        fecha.setTextAlign(Paint.Align.CENTER);
        fecha.setTextSize(Utils.convertDpToPixel(10));

        monto  = new Paint(Paint.ANTI_ALIAS_FLAG);
        monto.setColor(Color.BLACK);
        monto.setTextAlign(Paint.Align.CENTER);
        monto.setTextSize(Utils.convertDpToPixel(10));



        bounds = new Rect();
        amount = "$10,000";
        fecha.getTextBounds(amount, ConstantsCUF._VALUE_ZERO, amount.length(), bounds);

    }

    public void setAmount(String amount, String date){
        this.amount = amount;
        this.date = date;
    }

    public void setDays(float totalDays, float paymentDate){
        this.totalDays = totalDays;
        this.paymentDate = paymentDate;

        paymentDate = (getWidth()/totalDays)*paymentDate;

    }

    public void setDimens(float width, float height){
        this.width = width;
        this.height = height;
    }
    public void setColor(int color){
        line.setColor(color);
        indicator.setColor(color);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        android.util.Log.e("dkmg", "width: " + getWidth());
        android.util.Log.e("dkmg", "height: " + getHeight());
        android.util.Log.e("d_medidas", "width2: ** " + width);
        android.util.Log.e("d_medidas", "height2: ** " + height);
        paymentDate = (getWidth()/31)*31;
        canvas.drawLine(strokeWidth, getHeight() / 2, getWidth() - strokeWidth, getHeight() / 2, line);
        canvas.drawLine(paymentDate, getHeight() / 2 + (strokeWidth/4)*3, paymentDate, getHeight() / 2 - (strokeWidth/4*3), indicator);
        canvas.drawText("14 Mar", paymentDate, getHeight() / 2 + strokeWidth*2 , fecha);
        voidToolTip(canvas);
        canvas.drawText(amount, paymentDate, getHeight() / 2 - strokeWidth - bounds.height() , monto);

    }


    private void voidToolTip(Canvas canvas){


        Bitmap toolTip = BitmapFactory.decodeResource(getContext().getResources(),
                R.drawable.tooltip);

        toolTip = Bitmap.createScaledBitmap(toolTip, bounds.width()+(int)Utils.convertDpToPixel(12),(int)(bounds.height()*2.5), false);

        canvas.drawBitmap(toolTip,
                paymentDate- bounds.width()/2 - Utils.convertDpToPixel(6),
                (getHeight() / 2) - strokeWidth -(int)(bounds.height()*1.25) - bounds.height(),
                monto);
    }
}
