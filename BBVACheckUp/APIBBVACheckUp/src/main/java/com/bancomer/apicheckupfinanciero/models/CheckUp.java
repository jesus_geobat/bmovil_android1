package com.bancomer.apicheckupfinanciero.models;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.io.ParserJSONImpl;
import com.bancomer.apicheckupfinanciero.utils.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by DMEJIA on 20/05/16.
 */
public class CheckUp implements ParsingHandler {

    private String available;
    private String month;
    private String status;
    private String inversion;
    private String balanceT;
    private String balanceI;
    private String statusCredit;
    private String statusExpDep;
    private String tipoSaludCred;

    private boolean pagaCreditos;

    private Halo spend;
    private Halo deposit;
    private Halo creditPayment;

    private ArrayList<AccountCUF> arrayAccount;

    public CheckUp(){
        available = ConstantsCUF.ZERO;
        month = ConstantsCUF.EMPTY;
        status = ConstantsCUF.EMPTY;
        tipoSaludCred = ConstantsCUF.EMPTY;
        inversion = ConstantsCUF.ZERO;
        balanceI = ConstantsCUF.ZERO;
        balanceT = ConstantsCUF.ZERO;
        statusExpDep = ConstantsCUF.ZERO;
        statusCredit = ConstantsCUF.ZERO;
        pagaCreditos = false;

        spend = new Halo();
        deposit = new Halo();
        creditPayment = new Halo();

        arrayAccount = null;
    }

    public String getTipoSaludCred() {
        return tipoSaludCred;
    }

    public String getInversion() {
        return inversion;
    }

    public String getBalanceT() {
        return balanceT;
    }

    public String getBalanceI() {
        return balanceI;
    }

    public String getStatusCredit() {
        return statusCredit;
    }

    public String getStatusExpDep() {
        return statusExpDep;
    }

    public ArrayList<AccountCUF> getArrayAccount() {
        return arrayAccount;
    }

    public String getAvailable() {
        return available;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status){
        this.status  = status;
    }

    public String getPagoCreditos() {
        return creditPayment.getTotal();
    }

    public String getDeposito() {
        return deposit.getTotal();
    }

    public String getGasto() {
        return spend.getTotal();
    }

    public String getLastMonth(){
        return month;
    }

    public boolean isPagaCreditos() {
        return pagaCreditos;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parserJSON) throws IOException, ParsingException {
        if(parserJSON.hasValue(ConstantsCUF.TAG_RESPONSE)) {
            JSONObject object = parserJSON.parserNextObject(ConstantsCUF.TAG_RESPONSE, false);
            parserJSON = new ParserJSONImpl(object.toString());

            if(parserJSON.hasValue(ConstantsCUF.TAG_DISPONIBLE))
                available = parserJSON.parseNextValue(ConstantsCUF.TAG_DISPONIBLE, false);

            if(parserJSON.hasValue(ConstantsCUF.TAG_PAGA_CREDITOS)) {
                pagaCreditos = (parserJSON.parseNextValue(ConstantsCUF.TAG_PAGA_CREDITOS, false)).equalsIgnoreCase("true")? true:false;
                android.util.Log.v(getClass().getSimpleName(), String.format("process () TAG_PAGA_CREDITOS: %s", pagaCreditos));
            }
            if(parserJSON.hasValue(ConstantsCUF.TAG_MES))
                month = parserJSON.parseNextValue(ConstantsCUF.TAG_MES, false);

            if(parserJSON.hasValue(ConstantsCUF.TAG_SALUD_CREDITICIA))
                statusCredit = parserJSON.parseNextValue(ConstantsCUF.TAG_SALUD_CREDITICIA, false);

            if(parserJSON.hasValue(ConstantsCUF.TAG_SALDO_TOTAL))
                balanceT = parserJSON.parseNextValue(ConstantsCUF.TAG_SALDO_TOTAL, false);

            if(parserJSON.hasValue(ConstantsCUF.TAG_SALUD_DEPOSIVOSGASTOS))
                statusExpDep = parserJSON.parseNextValue(ConstantsCUF.TAG_SALUD_DEPOSIVOSGASTOS, false);

            if(parserJSON.hasValue(ConstantsCUF.TAG_SALDO_INICIAL))
                balanceI = parserJSON.parseNextValue(ConstantsCUF.TAG_SALDO_INICIAL, false);

            JSONObject jsonGasto = null;
            JSONObject jsonDeposito = null;
            JSONObject jsonCreditos = null;
            JSONArray jArrayCuentas = null;
            JSONObject jsonInversion;

            if(parserJSON.hasValue(ConstantsCUF.TAG_GASTO))
                jsonGasto = parserJSON.parserNextObject(ConstantsCUF.TAG_GASTO,false);

            if(parserJSON.hasValue(ConstantsCUF.TAG_DEPOSITO)) {
                jsonDeposito = parserJSON.parserNextObject(ConstantsCUF.TAG_DEPOSITO, false);
            }
            if(parserJSON.hasValue(ConstantsCUF.TAG_TIPO_SALUD_CREDITICIA)){
                tipoSaludCred = parserJSON.parseNextValue(ConstantsCUF.TAG_TIPO_SALUD_CREDITICIA,false);
            }

            if(parserJSON.hasValue(ConstantsCUF.TAG_PAGO_CREDITO))
                jsonCreditos = parserJSON.parserNextObject(ConstantsCUF.TAG_PAGO_CREDITO,false);

            if(parserJSON.hasValue(ConstantsCUF.TAG_CUENTAS))
                jArrayCuentas = parserJSON.parseNextValueWithArray(ConstantsCUF.TAG_CUENTAS, false);

            if(parserJSON.hasValue(ConstantsCUF.TAG_INVERSION)) {
                jsonInversion = parserJSON.parserNextObject(ConstantsCUF.TAG_INVERSION, false);

                parserJSON = new ParserJSONImpl(jsonInversion.toString());
                if(parserJSON.hasValue(ConstantsCUF.TAG_TOTAL))
                    inversion = (parserJSON.parseNextValue(ConstantsCUF.TAG_TOTAL)).replace("-","");

            }

            if(jsonGasto != null){
                parserJSON = new ParserJSONImpl(jsonGasto.toString());

                if(parserJSON.hasValue(ConstantsCUF.TAG_TOTAL))
                    spend.setTotal(parserJSON.parseNextValue(ConstantsCUF.TAG_TOTAL));

                if(parserJSON.hasValue(ConstantsCUF.TAG_DETALLES)) {
                    JSONArray arrayDetailSpend = parserJSON.parseNextValueWithArray(ConstantsCUF.TAG_DETALLES);
                    spend.setDetailsList(getDetails(arrayDetailSpend));
                }
            }

            if(jsonDeposito != null){
                parserJSON = new ParserJSONImpl(jsonDeposito.toString());
                if(parserJSON.hasValue(ConstantsCUF.TAG_TOTAL)) {
                    deposit.setTotal(parserJSON.parseNextValue(ConstantsCUF.TAG_TOTAL));
                }

                if(parserJSON.hasValue(ConstantsCUF.TAG_DETALLES)) {
                    JSONArray arrayDetailSpend = parserJSON.parseNextValueWithArray(ConstantsCUF.TAG_DETALLES);
                    deposit.setDetailsList(getDetails(arrayDetailSpend));
                }
            }

            if(jsonCreditos != null){
                parserJSON = new ParserJSONImpl(jsonCreditos.toString());

                if(parserJSON.hasValue(ConstantsCUF.TAG_TOTAL))
                    creditPayment.setTotal(parserJSON.parseNextValue(ConstantsCUF.TAG_TOTAL));


                if(parserJSON.hasValue(ConstantsCUF.TAG_TARCREDITO)) {
                    JSONArray arrayDetailSpend = parserJSON.parseNextValueWithArray(ConstantsCUF.TAG_TARCREDITO);
                    creditPayment.setTarjetaCredito(getDetails(arrayDetailSpend));
                }

                if(parserJSON.hasValue(ConstantsCUF.TAG_CREDITOAUTO)) {
                    JSONArray arrayDetailSpend = parserJSON.parseNextValueWithArray(ConstantsCUF.TAG_CREDITOAUTO);
                    creditPayment.setCreditoAuto(getDetails(arrayDetailSpend));
                }

                if(parserJSON.hasValue(ConstantsCUF.TAG_HIPOTECARIO)) {
                    JSONArray arrayDetailSpend = parserJSON.parseNextValueWithArray(ConstantsCUF.TAG_HIPOTECARIO);
                    creditPayment.setHipotecario(getDetails(arrayDetailSpend));
                }

                if(parserJSON.hasValue(ConstantsCUF.TAG_PRESPERSONAL)) {
                    JSONArray arrayDetailSpend = parserJSON.parseNextValueWithArray(ConstantsCUF.TAG_PRESPERSONAL);
                    creditPayment.setPrestamoPersonal(getDetails(arrayDetailSpend));
                }
            }

            if(jArrayCuentas != null){
                arrayAccount = new ArrayList<AccountCUF>();
                for (int i = 0; i < jArrayCuentas.length(); i++){
                    try {
                        AccountCUF accountCUF = new AccountCUF();
                        JSONObject jsonAccount = (JSONObject) jArrayCuentas.get(i);
                        if(jsonAccount.has(ConstantsCUF.TAG_TIPO)) {
                            accountCUF.setTipo(jsonAccount.getString(ConstantsCUF.TAG_TIPO));
                        }

                        if(jsonAccount.has(ConstantsCUF.TAG_DEPOSITO)) {
                            Account account = new Account();
                            account.setName("Saldo Inicial + Depósitos");
                            double amount = 0;
                            if(jsonAccount.has(ConstantsCUF.TAG_SALDO_INICIAL)) {
                                amount = jsonAccount.getDouble(ConstantsCUF.TAG_SALDO_INICIAL);
                                amount = amount > ConstantsCUF._VALUE_ZERO ? amount : ConstantsCUF._VALUE_ZERO;
                            }
                            account.setAmount(Tools.decimalFormat(String.valueOf(jsonAccount.getDouble(ConstantsCUF.TAG_DEPOSITO)+amount)));
                            accountCUF.getDescription().add(ConstantsCUF._VALUE_ZERO, account);
                        }

                        if(jsonAccount.has(ConstantsCUF.TAG_GASTO)) {
                            Account account = new Account();
                            account.setName("Gastos");
                            account.setAmount(Tools.decimalFormat(String.valueOf(jsonAccount.getDouble(ConstantsCUF.TAG_GASTO)).replace("-","")));
                            accountCUF.getDescription().add(ConstantsCUF._VALUE_ONE, account);
                        }

                        if(pagaCreditos) {
                            if (jsonAccount.has(ConstantsCUF.TAG_PAGO_CREDITOS)) {
                                Account account = new Account();
                                account.setName("Pago de créditos");
                                account.setAmount(Tools.decimalFormat(String.valueOf(jsonAccount.getDouble(ConstantsCUF.TAG_PAGO_CREDITOS))));
                                accountCUF.getDescription().add(ConstantsCUF._VALUE_TWO, account);
                            }
                        }

                        if(jsonAccount.has(ConstantsCUF.TAG_CUENTA)) {
                            accountCUF.setCuenta(jsonAccount.getString(ConstantsCUF.TAG_CUENTA));
                        }

                        if(jsonAccount.has(ConstantsCUF.TAG_SALDO)) {
                            accountCUF.setSaldo((jsonAccount.getDouble(ConstantsCUF.TAG_SALDO)));
                        }

                        arrayAccount.add(accountCUF);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }



            }
        }
    }

    private ArrayList<Details> getDetails(JSONArray jsonArray){
        ArrayList<Details> arrayDetails = new ArrayList<Details>();

        for (int i = 0; i < jsonArray.length(); i++) {
            Details details = new Details();
            try {
                details.setAmount(jsonArray.getJSONObject(i).getString(ConstantsCUF.TAG_MONTO));
                details.setDate(jsonArray.getJSONObject(i).getString(ConstantsCUF.TAG_FECHA));
                details.setDescription(jsonArray.getJSONObject(i).getString(ConstantsCUF.TAG_DESCRIPCION));
                details.setProduct(jsonArray.getJSONObject(i).getString(ConstantsCUF.TAG_PRODUCTO));
                arrayDetails.add(details);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return arrayDetails;
    }
}
