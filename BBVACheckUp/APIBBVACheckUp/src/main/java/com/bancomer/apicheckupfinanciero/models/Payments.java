package com.bancomer.apicheckupfinanciero.models;

/**
 * Created by DMEJIA on 22/02/16.
 */
public class Payments {
    Double amount;
    String clve;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getClve() {
        return clve;
    }

    public void setClve(String clve) {
        this.clve = clve;
    }


}
