package com.bancomer.apicheckupfinanciero.gui.views.fragments;

import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;

/**
 * Created by DMEJIA on 08/08/16.
 */
public class DialogMessagePG extends DialogFragment{

    private View rootView;

    private TextView txt1;
    private TextView txt2;
    private TextView txtOK;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_detal_credit, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().setCancelable(false);
        init();
        return rootView;
    }

    public void init(){
        findViews();
        setText();
    }

    private void findViews(){
        txt1 = (TextView)rootView.findViewById(R.id.txt1);
        txt2 = (TextView)rootView.findViewById(R.id.txt2);
        txtOK = (TextView)rootView.findViewById(R.id.txtOK);

        txtOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void setText(){
        String message1 = "¿Quieres conocer la <font color='#3EB6BB'>Salud</font> de tus <font color='#3EB6BB'>Finanzas Personales</font>?";
        String message2 = "Cámbiate a <font color='#3EB6BB'>Nómina Bancomer</font> y descubre una nueva forma para administrarlas a tu favor.";

        txt1.setText(Html.fromHtml(message1), TextView.BufferType.SPANNABLE);
        txt2.setText(Html.fromHtml(message2), TextView.BufferType.SPANNABLE);
    }
}
