package com.bancomer.apicheckupfinanciero.gui.views.fragments;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.gui.Delegates.CreditPaymentsDelegate;
import com.bancomer.apicheckupfinanciero.gui.Delegates.MainDelegate;
import com.bancomer.apicheckupfinanciero.gui.views.activities.MainActivity;
import com.bancomer.apicheckupfinanciero.gui.views.adapters.CreditsAdapter;
import com.bancomer.apicheckupfinanciero.models.PagoCreditos;
import com.bancomer.apicheckupfinanciero.utils.CustomerTextView;
import com.bancomer.apicheckupfinanciero.utils.LineTime.LineTime;
import com.bancomer.apicheckupfinanciero.utils.Tools;

/**
 * Created by DMEJIA on 10/02/16.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CreditPaymentsFragment extends Fragment implements View.OnClickListener{

    private View rootView;
    private View v;

    private View tabIndicatorMA;
    private View tabIndicatorMIA;
    private CustomerTextView txtLastMonth;
    private CustomerTextView txtCurrentMont;
    private RelativeLayout rl_TabMIA;
    private RelativeLayout rl_TabMA;

    private ListView lsCredits;


    private boolean showDetail;
    private boolean remove;

    private CreditPaymentsDelegate delegate;
    private Bundle bundle;

  /*  @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = this.getArguments();
    }*/


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_payment_credits, container, false);
        init();
        return rootView;
    }

    private void init(){
        delegate = new CreditPaymentsDelegate(this);
        //getDataFromBundle();
        findViews();
        showDetail = true;
    }

    /*private void getDataFromBundle(){
        pagoCreditos = (PagoCreditos) bundle.getSerializable(ConstantsCUF.TAG_OBJ_PAGO_CREDITOS);
        android.util.Log.e("dorina", "pagoCreditos arrar " + pagoCreditos.getCreditCardLists().size());
    }*/

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void findViews(){
        lsCredits = (ListView) rootView.findViewById(R.id.lsCredits);
        txtCurrentMont = (CustomerTextView) rootView.findViewById(R.id.txtCurrentMonthTabDYG);
        txtLastMonth = (CustomerTextView) rootView.findViewById(R.id.txtLastMonthTabDYG);

        rl_TabMA = (RelativeLayout) rootView.findViewById(R.id.rlTabMADYG);
        rl_TabMIA = (RelativeLayout) rootView.findViewById(R.id.rlTabMIADYG);

        txtLastMonth.setText(Tools.lastMonth);
        txtCurrentMont.setText(Tools.currentMonth);

        tabIndicatorMA = rootView.findViewById(R.id.tabIndicatorMADYG);
        tabIndicatorMIA = rootView.findViewById(R.id.tabIndicatorMIADYG);

        rl_TabMIA.setOnClickListener(this);
        rl_TabMA.setOnClickListener(this);
    }

    public void inflateList(MainDelegate mainDelegate){
        final CreditsAdapter adapter = new CreditsAdapter(getActivity(), mainDelegate.getObjPagoCreditos().getCreditCardLists(),delegate,this);
        lsCredits.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        float n = 3 * getActivity().getResources().getDisplayMetrics().scaledDensity;
        FragmentManager fm = getActivity().getSupportFragmentManager();
        DetailCredtPaymentsFragment dialog = null;

        if(showDetail) {
            showDetail = false;
            /*if (v == imgLoands)
                dialog = new DetailCredtPaymentsFragment(txtAmount.getText().toString(),
                        txtDate.getText().toString(), ConstantsCUF.EXCELENTE,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_auto_ver), date[0], n, false, this);
            else if (v == imgLoands2)
                dialog = new DetailCredtPaymentsFragment(txtAmount2.getText().toString(),
                        txtDate2.getText().toString(), ConstantsCUF.SALUDABLE,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_hipo_ver),
                        date[1], n, false, this);
            else if (v == imgLoands3)
                dialog = new DetailCredtPaymentsFragment(txtAmount3.getText().toString(),
                        txtDate3.getText().toString(), ConstantsCUF.CRITICO,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_nomi_ver),
                        date[2], n, false, this);
            else if (v == imgLoands4)
                dialog = new DetailCredtPaymentsFragment(txtAmount4.getText().toString(),
                        txtDate4.getText().toString(), ConstantsCUF.MALESTARES,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_pers_ver),
                        date[3], n, false, this);
            else if (v == imgLoands5)
                dialog = new DetailCredtPaymentsFragment(txtAmount5.getText().toString(),
                        txtDate5.getText().toString(), ConstantsCUF.EXCELENTE,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_tarcre_ver),
                        date[4], n, false, this);
            if (v == imgLoands6)
                dialog = new DetailCredtPaymentsFragment(txtAmount6.getText().toString(),
                        txtDate6.getText().toString(), ConstantsCUF.EXCELENTE,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_compra_ver),
                        date[5], n, false, this);*/
            //dialog.show(fm, "Ticket");
        }
    }


    private void setColor(ImageView ico, ImageView line, ImageView indicador){
        ico.clearColorFilter();
        line.clearColorFilter();
        indicador.clearColorFilter();
        //img.setColorFilter(getIntColor(arrayStatus[count]));
    }

    public void setShowDetail(boolean showDetail){
        this.showDetail = showDetail;
    }

    public void getData(){
        delegate.realizaOperacion();
    }
}
