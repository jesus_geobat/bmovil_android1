package com.bancomer.apicheckupfinanciero.gui.Delegates;

import com.bancomer.apicheckupfinanciero.io.ServerResponseImpl;

import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by DMEJIA on 18/02/16.
 */
public class BaseDelegateImp implements bancomer.api.common.gui.delegates.BaseDelegate {

    public void analyzeResponse(String operationId, ServerResponseImpl response) {
        // TODO Auto-generated method stub
    }

    public void doNetworkOperation(int i, Hashtable<String, ?> hashtable, bancomer.api.common.gui.delegates.BaseDelegate baseViewController) {

    }
    @Override
    public void doNetworkOperation(int i, Hashtable<String, ?> hashtable, BaseViewController baseViewController) {

    }
    @Override
    public void analyzeResponse(int i, ServerResponse serverResponse) {

    }

    @Override
    public void performAction(Object o) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
