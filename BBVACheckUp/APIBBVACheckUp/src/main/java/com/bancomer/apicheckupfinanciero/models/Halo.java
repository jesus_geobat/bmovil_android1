package com.bancomer.apicheckupfinanciero.models;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DMEJIA on 20/05/16.
 */
public class Halo implements Serializable {

    private String total;
    private ArrayList<Details> detailsList;
    private ArrayList<Details> tarjetaCredito;
    private ArrayList<Details> creditoAuto;
    private ArrayList<Details> prestamoPersonal;
    private ArrayList<Details> hipotecario;

    public Halo(){
        total = ConstantsCUF.ZERO;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public ArrayList<Details> getDetailsList() {
        return detailsList;
    }

    public void setDetailsList(ArrayList<Details> detailsList) {
        this.detailsList = detailsList;
    }

    public ArrayList<Details> getTarjetaCredito() {
        return tarjetaCredito;
    }

    public void setTarjetaCredito(ArrayList<Details> tarjetaCredito) {
        this.tarjetaCredito = tarjetaCredito;
    }

    public ArrayList<Details> getCreditoAuto() {
        return creditoAuto;
    }

    public void setCreditoAuto(ArrayList<Details> creditoAuto) {
        this.creditoAuto = creditoAuto;
    }

    public ArrayList<Details> getPrestamoPersonal() {
        return prestamoPersonal;
    }

    public void setPrestamoPersonal(ArrayList<Details> prestamoPersonal) {
        this.prestamoPersonal = prestamoPersonal;
    }

    public ArrayList<Details> getHipotecario() {
        return hipotecario;
    }

    public void setHipotecario(ArrayList<Details> hipotecario) {
        this.hipotecario = hipotecario;
    }
}
