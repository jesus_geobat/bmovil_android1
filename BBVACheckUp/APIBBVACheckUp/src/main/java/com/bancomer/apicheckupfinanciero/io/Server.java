package com.bancomer.apicheckupfinanciero.io;

import com.bancomer.apicheckupfinanciero.controller.InitCheckUp;
import com.bancomer.apicheckupfinanciero.models.CheckUp;
import com.bancomer.apicheckupfinanciero.models.CreditCardList;
import com.bancomer.apicheckupfinanciero.models.PagoCreditos;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import bancomer.api.common.commons.NameValuePair;
import bancomer.api.common.io.ParsingException;

/**
 * Created by DMEJIA on 18/02/16.
 */
public class Server {

    /**
     * Defines if the application show logs
     */
    public static boolean ALLOW_LOG = true;

    /**
     * Defines if the application is running on the emulator or in the device.
     */
    public static boolean EMULATOR = false;

    /**
     * Indicates simulation usage.
     */
    public static boolean SIMULATION = true;

    /**
     * Indicates if the base URL points to development server or production
     */
    public static boolean DEVELOPMENT = true;

    /**
     * Indicates simulation usage.
     */
    public static final long SIMULATION_RESPONSE_TIME = 15;

	/*
		Verifica si ilc se lanza ILC desde bbvaCredit
	 */

    public static boolean isILCFromCredit;
    /**
     * Bancomer Infrastructure.
     */
    public static final int BANCOMER = 0;

    public static int PROVIDER = BANCOMER; // TODO remove for TEST

    // ///////////////////////////////////////////////////////////////////////////
    // Operations identifiers //
    // ///////////////////////////////////////////////////////////////////////////

    /** ILC **/

    public static final int CONSULTA_ESTADO_FIANCIERO_MA=203;
    public static final int LOGIN=2;
    public static final int CONSULTA_DETALLE_PRESTAMOS=3;
    public static final int CONSULTA_DETALLE_CREDITOS=4;
    public static final int CONSULTA_ESTADO_FIANCIERO_MIA=202;


    //one CLick
    /**
     * Operation code parameter for new ops.
     */
    public static final String JSON_OPERATION_CODE_VALUE_CHECKUP= "consultaCheckUp";
    /**
     *
     */

    public static final String JSON_OPERATION_CODE_VALUE_LOGIN= "login";

    public enum isJsonValueCode{
        CUF, LOGIN , DETALLE_TDC,NONE;
    }

    public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
    //Termina One CLick


    /**
     * Operation codes.
     */
    public static final String[] OPERATION_CODES = { "00","consultaCheckUp", "login", "consultaDetallePrestamos", "getCreditsDetail","consultaCheckUpMIA"};


    /**
     * Base URL for providers.
     */
    public static String[] BASE_URL;

    /**
     * Path table for provider/operation URLs.
     */
    public static String[][] OPERATIONS_PATH;

    /**
     * Operation URLs map.
     */
    public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

    /**
     * Operation providers map.
     */
    public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

    /**
     * Operation data parameter.
     */
    public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

    /**
     * Operation code parameter.
     */
    public static final String OPERATION_CODE_PARAMETER = "OPERACION";


    /**
     * Operation data parameter.
     */
    public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

    /**
     * Operation data parameter.
     */
    public static final String OPERATION_LOCALE_VALUE = "es_ES";


    private ClienteHttp clienteHttp = null;


    public Server() {
        clienteHttp = new ClienteHttp();
    }

    public ClienteHttp getCliente(){
        return clienteHttp;
    }

    //static {
    public static void setEnviroment() {
        android.util.Log.e("bandera", "DEVE: "+ DEVELOPMENT +" SIM: "+ SIMULATION);
        if (DEVELOPMENT) {
            setupDevelopmentServer();
        } else {
            setupProductionServer();
        }

        setupOperations(PROVIDER);
    }

    /**
     * Setup production server paths.
     */
    private static void setupProductionServer() {
        BASE_URL = new String[] { "https://www.bancomermovil.com",
                // "https://172.17.100.173",
                EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Produccion"
                        : "http://189.254.77.54:8080/servermobile/Servidor/Produccion" };
        OPERATIONS_PATH = new String[][] {
                new String[] { "",
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaCheckUp/MA/", // consultaEdoFinanciero
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/authentication/V01/login", // Login
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaDetallePrestamos",
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/getCreditsDetail",
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaCheckUp/MIA/" // consultaEdoFinanciero

                },
                new String[] { "",
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaCheckUp/MA/", // consultaEdoFinanciero
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/authentication/V01/login", // Login
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaDetallePrestamos",
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/getCreditsDetail",
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaCheckUp/MIA/" // consultaEdoFinanciero

                } };
    }

    /**
     * Setup production server paths.
     */
    private static void setupDevelopmentServer() {
        BASE_URL = new String[] {
                "https://www.bancomermovil.net:11443",
                EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Desarrollo"
                        : "http://189.254.77.54:8080/servermobile/Servidor/Desarrollo" };
        OPERATIONS_PATH = new String[][] {
                new String[] { "",
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaCheckUp/MA/", // consultaEdoFinanciero
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/authentication/V01/login", // Login
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaDetallePrestamos",
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/getCreditsDetail",
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaCheckUp/MIA/"
                },
                new String[] { "",
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaCheckUp/MA/", // consultaEdoFinanciero
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/authentication/V01/login", // Login
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaDetallePrestamos",
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/getCreditsDetail",
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/checkupfinanciero/V01/consultaCheckUp/MIA/"
                } };
    }

    private static void setupOperations(int provider) {

        setupOperation(CONSULTA_ESTADO_FIANCIERO_MA, provider);
        setupOperation(LOGIN, provider);
        setupOperation(CONSULTA_DETALLE_CREDITOS, provider);
        setupOperation(CONSULTA_ESTADO_FIANCIERO_MIA, provider);
    }

    /**
     * Setup operation for a provider.
     * @param operation the operation
     * @param provider the provider
     */
    private static void setupOperation(int operation, int provider) {
        String code = OPERATION_CODES[operation];
        OPERATIONS_MAP.put(
                code,
                new StringBuffer(BASE_URL[provider]).append(
                        OPERATIONS_PATH[provider][operation]).toString());
        OPERATIONS_PROVIDER.put(code, new Integer(provider));
    }

    public static String getOperationUrl(String operation) {
        String url = (String) OPERATIONS_MAP.get(operation);
        return url;
    }

    /**
     * Cancel the ongoing network operation.
     */
    public void cancelNetworkOperations() {
        clienteHttp.abort();
    }

    public ServerResponseImpl doNetworkOperation(int operationId, Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException, JSONException {
        ServerResponseImpl response = null;
        long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
        Integer provider = (Integer) OPERATIONS_PROVIDER.get(OPERATION_CODES[operationId]);
        if (provider != null) {
            PROVIDER = provider.intValue();
        }

        switch (operationId) {
            case CONSULTA_ESTADO_FIANCIERO_MA:
                response = consultaEdoFianciero(params, true);
                break;

            case LOGIN:
                response = consultaLogin(params);
                break;

            case CONSULTA_DETALLE_CREDITOS:
                response = consultaDetalleTDC(params);
                break;
            case CONSULTA_ESTADO_FIANCIERO_MIA:
                response = consultaEdoFianciero(params, false);
                break;
        }

        if (sleep > 0) {
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
            }
        }
        return response;
    }

    private ServerResponseImpl consultaEdoFianciero(Hashtable<String, ?> params,boolean MA)
            throws IOException, ParsingException, URISyntaxException, JSONException {
        CheckUp cltaEdoFinanciero = new CheckUp();
        //CltaEdoFinanciero cltaEdoFinanciero = new CltaEdoFinanciero();
        ServerResponseImpl response = new ServerResponseImpl(cltaEdoFinanciero);
        isjsonvalueCode=isJsonValueCode.CUF;

        NameValuePair[] parameters = new NameValuePair[0];

        if (SIMULATION) {
            String mensaje = MA == true ? TramasSimuacion.MIA_5518561555: TramasSimuacion.MIA_5518561555;
            this.clienteHttp.simulateOperation(OPERATION_CODES[ MA== true ? CONSULTA_ESTADO_FIANCIERO_MA: CONSULTA_ESTADO_FIANCIERO_MIA],parameters,response,mensaje, true);
        }else{
            clienteHttp.invokeOperation(OPERATION_CODES[MA== true ? CONSULTA_ESTADO_FIANCIERO_MA: CONSULTA_ESTADO_FIANCIERO_MIA], null, response);

        }

        return response;
    }

    private ServerResponseImpl consultaLogin(Hashtable<String, ?> params)
            throws IOException, ParsingException, URISyntaxException, JSONException {
        ServerResponseImpl response = new ServerResponseImpl();
        isjsonvalueCode=isJsonValueCode.LOGIN;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("user", InitCheckUp.getInstance().getSession().getUsername());
        jsonObject.put("password", InitCheckUp.getInstance().getPassword());
        jsonObject.put("sc", "MB");
        jsonObject.put("ium", InitCheckUp.getInstance().getSession().getIum());

        if (SIMULATION) {
            String mensaje = TramasSimuacion.LOGIN_OK;
            this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_ESTADO_FIANCIERO_MA],null,response,mensaje, true);
        }else{
            clienteHttp.invokeOperation(OPERATION_CODES[LOGIN], jsonObject, response);
        }

        return response;
    }

    private ServerResponseImpl consultaDetalleTDC(Hashtable<String, ?> params)
            throws IOException, ParsingException, URISyntaxException, JSONException {
        PagoCreditos pagoCreditos = new PagoCreditos();
        ServerResponseImpl response = new ServerResponseImpl(pagoCreditos);
        isjsonvalueCode = isJsonValueCode.DETALLE_TDC;
        NameValuePair[] parameters = new NameValuePair[0];
        String ho = "\'h";

        if (SIMULATION) {
            String mensaje = TramasSimuacion.credits_5550505001;
            this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DETALLE_CREDITOS],parameters,response,mensaje, true);
        }else{
            clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DETALLE_CREDITOS], null, response);
        }

        return response;
    }
}
