package com.bancomer.apicheckupfinanciero.io;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.AbstractSuitePowerManager;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.controller.InitCheckUp;
import com.bancomer.apicheckupfinanciero.models.CheckUp;
import com.bancomer.apicheckupfinanciero.utils.Tools;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Objects;

import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.MethodType;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;


public abstract class BaseSubapplication {
	// #region Variables.
	/**
	 * Determines if this activity is in process of changing screen.
	 */
	protected boolean changingActivity = false;

	protected boolean applicationLogged = false;

	protected boolean applicationInBackground = false;


	/**
	 * Reference to the main application.
	 */
	protected Context suiteApp = null;

	/**
	 * Referencia al controlador de vistas.
	 */
	protected BaseViewsController viewsController;

	/**
	 * Reference to a pending network operation, in case that an
	 * operation launched a session expired error. This operation
	 * will be relaunched after a successful login
	 */
	protected NetworkOperation pendingOperation = null;

	/**
	 * Reference to server communication handler.
	 */
	protected Server server;

	/**
	 * Pending network operations.
	 */
	protected Hashtable<Integer, NetworkOperation> networkOperationsTable = null;

	protected boolean isHomeKeyPressed = false;
	// #endregion

	// #region Getters y Setters.

	/**
	 * @return the changingActivity
	 */
	public boolean isChangingActivity() {
		return changingActivity;
	}

	private boolean isHomeKeyPressed() {
		return isHomeKeyPressed;
	}

	protected void setHomeKeyPressed(boolean isHomeKeyPressed) {
		this.isHomeKeyPressed = isHomeKeyPressed;
	}

	/**
	 * @param changingActivity the changingActivity to set
	 */
	public void setChangingActivity(boolean changingActivity) {
		this.changingActivity = changingActivity;
	}

	/**
	 * @return the applicationLogged
	 */
	public boolean isApplicationLogged() {
		return applicationLogged;
	}

	/**
	 * @param applicationLogged the applicationLogged to set
	 */
	public void setApplicationLogged(boolean applicationLogged) {
		this.applicationLogged = applicationLogged;
	}

	/**
	 * @return the applicationInBackground
	 */
	public boolean isApplicationInBackground() {
		return applicationInBackground;
	}

	/**
	 * @param applicationInBackground the applicationInBackground to set
	 */
	public void setApplicationInBackground(boolean applicationInBackground) {
		this.applicationInBackground = applicationInBackground;
	}

	/**
	 * @return the suiteApp
	 */
	public Context getSuiteApp() {
		return suiteApp;
	}



	/**
	 * @return the pendingOperation
	 */
	public NetworkOperation getPendingOperation() {
		return pendingOperation;
	}

	/**
	 * @param pendingOperation the pendingOperation to set
	 */
	public void setPendingOperation(NetworkOperation pendingOperation) {
		this.pendingOperation = pendingOperation;
	}

	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(Server server) {
		this.server = server;
	}

	/**
	 * @return the networkOperationsTable
	 */
	public Hashtable<Integer, NetworkOperation> getNetworkOperationsTable() {
		return networkOperationsTable;
	}

	/**
	 * @param networkOperationsTable the networkOperationsTable to set
	 */
	public void setNetworkOperationsTable(
			Hashtable<Integer, NetworkOperation> networkOperationsTable) {
		this.networkOperationsTable = networkOperationsTable;
	}

	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	public void invokeNetworkOperation(final int operationId, final Map<String, String>  params,
									   final Map<String, String> header,
									   final BaseDelegate caller,
									   final ParsingHandler handler,
									   String progressLabel,
									   String progressMessage,
									   final boolean callerHandlesError) {
		final Integer opId = Integer.valueOf(operationId);

		if (isNotAlreadyCalling(opId)) {

			try {

				NetworkOperation operation = new NetworkOperation(operationId, params, caller);
				this.networkOperationsTable.put(opId, operation);
				this.pendingOperation = operation;

//				//TODO quitar el numero de operacion en hardcode.
//				if (caller != null && 11 != operationId)
//				    caller.muestraIndicadorActividad(progressLabel, progressMessage);

				if (caller != null) {
					if(AbstractSuitePowerManager.getSuitePowerManager().isScreenOn()) {
						Tools.muestraIndicadorActividad(progressLabel, progressMessage,InitCheckUp.getInstance().getActivityController().getCurrentActivity());
					} else {
						Log.d(this.getClass().getSimpleName(), "La aplicación estaba bloqueada.");
					}
				}

				Thread thread = new Thread(new Runnable() {
					public void run() {
						try {
							ServerResponseImpl response = null;
							//TODO sin apiserver  !Server.SIMULATION  con apiServer Server.SIMULATION
							/*if(!Server.SIMULATION ) //|| opId.intValue() == Server.CONSULTA_ESTADO_FIANCIERO_MA || opId.intValue() == Server.CONSULTA_ESTADO_FIANCIERO_MIA)
								response = server.doNetworkOperation(opId.intValue(), params);
							else{*/
							ParametersTO parametersTO = new ParametersTO();
							parametersTO.setParameters(new Hashtable<String,String>());
							parametersTO.setMethodType(MethodType.GET);
							parametersTO.setParamsUrl(params);
							parametersTO.setOperationId(opId.intValue());
							parametersTO.setHeaders(header);
							parametersTO.setForceArq(true);
							parametersTO.setAddCadena(false);
							parametersTO.setJson(true);
							parametersTO.setSimulation(Server.SIMULATION);
							parametersTO.setIsJsonValue(ApiConstants.isJsonValueCode.NONE);
							parametersTO.setDevelopment(Server.DEVELOPMENT);
							/*ParametersTO parametersTO = new ParametersTO();
							parametersTO.setParameters(params);
							parametersTO.setHeaders(header);
							parametersTO.setMethodType(MethodType.GET);
							parametersTO.setOperationId(opId);
							parametersTO.setAddCadena(false);
							parametersTO.setForceArq(true);
							parametersTO.setJson(true);
							parametersTO.setParamsUrl(new HashMap<String, String>());
							parametersTO.setSimulation(Server.SIMULATION);
							parametersTO.setIsJsonValue(ApiConstants.isJsonValueCode.NONE);
							parametersTO.setDevelopment(Server.DEVELOPMENT);*/

							IResponseService resultado = new ResponseServiceImpl();
							ICommService serverproxy = new CommServiceProxy(suiteApp);
							resultado = serverproxy.request(parametersTO,new Object().getClass());

							if(opId.intValue() == Server.CONSULTA_ESTADO_FIANCIERO_MA || opId.intValue() == Server.CONSULTA_ESTADO_FIANCIERO_MIA){
								if(Server.ALLOW_LOG)
									android.util.Log.e(getClass().getSimpleName(), "resultado: " + resultado.getResponseString());
								resultado.setObjResponse(new CheckUp());

								response = parserConnection(resultado,handler);
							}
							//}
							Log.e("After doNetworkOperation","ok");
							returnFromNetworkOperation(opId, response, caller, null, callerHandlesError);

						} catch (Throwable t) {
							returnFromNetworkOperation(opId, null, caller, t, callerHandlesError);
						}
					}

				});

				// Starts the worker thread to perform network operation
				thread.start();

			} catch (Throwable th) {
				Log.d("gets on ", "catch invoke");
				returnFromNetworkOperation(opId, null, caller, th, callerHandlesError);
			}

		} else {
			// unable to process request, show error and return
			return;
		}

	}

	public ServerResponseImpl parserConnection(IResponseService resultado,
											   ParsingHandler handler) throws Exception {
		ServerResponseImpl response=null;
		ParsingHandler handlerP=null;
		if (ApiConstants.OPERATION_SUCCESSFUL == resultado.getStatus()) {
			if (handler != null) {
				ParserJSON parser = new ParserJSONImpl(
						resultado.getResponseString());
				handler.process(parser);
			}
			response = new ServerResponseImpl(resultado.getStatus(),
					resultado.getMessageCode(), resultado.getMessageText(), handler);
		}else{
			boolean isJsonResponse = false;
			try {
				JSONObject jsonObject =new JSONObject(resultado.getResponseString());
				isJsonResponse = true;
			}
			catch (JSONException e){
			}
			if (ApiConstants.OPERATION_ERROR != resultado.getStatus() && isJsonResponse) {
				ServerResponseImpl res = new ServerResponseImpl(handler);

				try {

					parseJSON(resultado.getResponseString(), res);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParsingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				response = res;
			}else {
				response = new ServerResponseImpl(resultado.getStatus(),
						resultado.getMessageCode(), resultado.getMessageText(), handler);
			}
		}
		return response;
	}
	/**
	 * Called internally after a network operation has returned. It ends the progress
	 * popup and invokes method to analyze the result
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message from the server or method should handle in the default
	 * implementation
	 */
	protected void returnFromNetworkOperation(final Integer operationId,
											  final ServerResponseImpl response,
											  final BaseDelegate caller,
											  final Throwable throwable,
											  final boolean callerHandlesError) {

		Log.e("gets on Return","ok");

		if (caller != null) {
			//if(!(operationId.equals(Server.MOVEMENTS_OPERATION))){
			Tools.ocultaIndicadorActividad();
			//}

			InitCheckUp.getInstance().getActivityController().getCurrentActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						Log.d("Caller != null", "ok");
						analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);
					} catch (Throwable t) {
						Log.d(this.getClass().getSimpleName(), "Exception on analyzeNetworkResponse.", t);
						if (caller != null) {
							String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
							Log.d("Exception on analyzeNetworkResponse.", message);
							//InitILC.getInstance().getBaseDelegate().showErrorMessage(caller.getActivity(), message);
							reiniciar();
						}
					}
				}
			});

		} else { //TODO session timer expired, go to login screen
			Thread thread = new Thread(new Runnable() {
				public void run() {
					try {
						Log.d("Caller == null", "ok");
						analyzeNetworkResponse(operationId, response, throwable, null, callerHandlesError);
					} catch (Throwable t) {
						String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
						Log.d(this.getClass().getSimpleName() + " :: returnFromNetworkOperation", message);
					}
				}
			});
			thread.run();
		}
	}

	// TODO revisar en hijos.
	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void analyzeNetworkResponse(Integer operationId,
										  ServerResponseImpl response,
										  Throwable throwable,
										  final BaseDelegate caller,
										  boolean callerHandlesError) {
		// get the caller           
		NetworkOperation operation = this.networkOperationsTable.get(operationId);
		if (operation != null) {
			// remove the operation id from the table
			this.networkOperationsTable.remove(operationId);
			if (operation.isActive()) {
				if (response != null) {

					int resultCode = response.getStatus();
					android.util.Log.e("resultcodeee", "code:---" + resultCode);
					switch (resultCode) {
						case ServerResponseImpl.OPERATION_SUCCESSFUL:
						case ServerResponseImpl.OPERATION_WARNING:
							android.util.Log.i("es nulo??", "caller: " + caller);
							caller.analyzeResponse(operationId.intValue(), response);

							break;
						case ServerResponseImpl.OPERATION_ERROR:
							this.pendingOperation = null;
							String errorCode = response.getMessageCode();
							int operationError = operationId.intValue();

//
							if (callerHandlesError) {
								caller.analyzeResponse(operationId.intValue(), response);
							} else {
								caller.analyzeResponse(operationId.intValue(), response);
							}
							break;
						default:
							Tools.ocultaIndicadorActividad();
							response = unexpectedErrorResponse(InitCheckUp.getInstance().getActivityController().getCurrentActivity().getString(R.string.error_format));
							caller.analyzeResponse(operationId.intValue(), response);
							//reiniciar();

							break;
					}
				} else {
					if (throwable != null) {
						Tools.ocultaIndicadorActividad();
						//InitILC.getInstance().getBaseDelegate().showErrorMessage(caller.getActivity(), getErrorMessage(throwable));

						response = unexpectedErrorResponse(getErrorMessage(throwable));
						caller.analyzeResponse(operationId.intValue(), response);
						//reiniciar();
					} else {
						Tools.ocultaIndicadorActividad();
						//InitILC.getInstance().getBaseDelegate().showErrorMessage(caller.getActivity(), R.string.error_communications);

						response = unexpectedErrorResponse(InitCheckUp.getInstance().getActivityController().getCurrentActivity().getString(R.string.error_communications));
						caller.analyzeResponse(operationId.intValue(), response);
						//reiniciar();
					}
				}
			}
		} else {
//			InitConsultaOtrosCreditos.getInstance().getBaseDelegate().ocultaIndicadorActividad();
//			InitConsultaOtrosCreditos.getInstance().getBaseDelegate().showErrorMessage(caller.getActivity(),R.string.error_unexpected);

			response = unexpectedErrorResponse(InitCheckUp.getInstance().getActivityController().getCurrentActivity().getString(R.string.error_unexpected));
			caller.analyzeResponse(operationId.intValue(), response);
			//reiniciar();
		}
	}

	private void reiniciar(){
		InitCheckUp.getInstance().getObj().habilitarVista();
		InitCheckUp.getInstance().getObj().exitSaludFinanciera();
		InitCheckUp.resetInstace();
	}

	private ServerResponseImpl unexpectedErrorResponse(String mensaje){
		String msg = "{\"status\":\"ERROR\",\"codigoMensaje\":\"\",\"description\":\""+mensaje+"\"}";

		ServerResponseImpl res = new ServerResponseImpl();

		try {

			parseJSON(msg, res);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParsingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return res;
	}

	private void parseJSON(String response, ParsingHandler handler) throws IOException, ParsingException {

		Reader reader = null;
		try {
//            reader = new StringReader(response);
			ParserJSON parser = new ParserJSONImpl(response);
			handler.process(parser);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (Throwable ignored) {
				}
			}
		}
	}





	/**
	 * Cancel any ongoing network operation, and clears the network
	 * operation table to prevent any other network operation from
	 * executing
	 */
	protected void cancelOngoingNetworkOperations() {
		this.networkOperationsTable.clear();
		this.pendingOperation = null;
		this.server.cancelNetworkOperations();
	}

	/**
	 * Determines if the requested operation is not being called already
	 * @param opId the operation id
	 * @return true if the operation isn't already waiting for a response
	 */
	private boolean isNotAlreadyCalling(int opId){
		return !this.networkOperationsTable.containsKey(opId);
	}
	// #endregion

	// #region Otros métodos.
	// TODO revisar en hijos.
	public void cierraAplicacion() {
		viewsController.cierraViewsController();
		viewsController = null;
		server.cancelNetworkOperations();
		server = null;
		applicationLogged = false;
	}

	/**
	 * Shows the Main menu screen, removing any activity on top
	 */
	public void requestMenu(Context activityContext){
		changingActivity = true;
		viewsController.showMenuInicial();
	}

	/**
	 * Get the operation error message
	 * @param throwable the throwable
	 * @return the operation error message 
	 */
	protected String getErrorMessage(Throwable throwable) {
		StringBuffer sb = new StringBuffer();
		if (throwable != null) {
			if (throwable instanceof NumberFormatException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_format));
			} else if (throwable instanceof ParsingException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_format));
			} else if (throwable instanceof IOException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_communications));
			} else {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_communications));
			}
		}
		return sb.toString();
	}
	// #endregion

	// TODO revisar en hijos.
	public BaseSubapplication(Context suiteApp) {
		this.suiteApp = suiteApp;
		server = new Server();
		networkOperationsTable = new Hashtable<Integer, NetworkOperation>();
		viewsController = null;
		applicationLogged = false;
	}
}
