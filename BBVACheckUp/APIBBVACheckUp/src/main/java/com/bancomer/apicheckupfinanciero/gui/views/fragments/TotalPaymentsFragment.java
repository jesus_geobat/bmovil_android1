package com.bancomer.apicheckupfinanciero.gui.views.fragments;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.controller.InitCheckUp;
import com.bancomer.apicheckupfinanciero.gui.Delegates.TotalPaymentsDelegate;
import com.bancomer.apicheckupfinanciero.gui.views.activities.MainActivity;
import com.bancomer.apicheckupfinanciero.io.Server;
import com.bancomer.apicheckupfinanciero.models.CheckUp;
import com.bancomer.apicheckupfinanciero.models.CheckUpValues;
import com.bancomer.apicheckupfinanciero.models.Payments;
import com.bancomer.apicheckupfinanciero.persistence.BBVACheckUpSharedPreferences;
import com.bancomer.apicheckupfinanciero.utils.Blur.Blurry;
import com.bancomer.apicheckupfinanciero.utils.Tools;
import com.github.mikephil.charting.charts.PieChart;

import java.io.Serializable;
import java.util.ArrayList;

public class TotalPaymentsFragment extends Fragment implements OnClickListener, Serializable{

    private TotalPaymentsDelegate delegate;
    private MainActivity activity;
    private CheckUp checkUpMA;
    private CheckUp checkUpMIA;
    private CheckUpValues valuesMA;
    private CheckUpValues valuesMIA;

    private View rootView;
    private View tabIndicatorMA;
    private View tabIndicatorMIA;

    private PieChart pieChart1;
    private PieChart pieChart2;
    private PieChart pieChart3;
    private PieChart pieChart4;
    private PieChart pieChart5;

    private TextView txtLastMonthTab;
    private TextView txtCurrentMonthTab;
    private TextView txtMonth;
    private TextView txtStatus;
    private TextView lblDisponible;
    private TextView txtDisponible;


    private RelativeLayout rlGraphics;
    private RelativeLayout rlGraphics2;
    private RelativeLayout rlTabMIA;
    private RelativeLayout rlTabMA;
    private RelativeLayout rlTABS;

    private ImageView imgHeart;
    private ImageView imgBtnCoach;

    private ArrayList<Payments> arrayLoands;

    private double deposito;
    private double gasto;
    private double ahorro;
    private double pagoCreditos;
    private double inversion;
    private double saldoInicial;
    private double depositoMIA;
    private double gastoMIA;
    private double ahorroMIA;
    private double pagoCreditosMIA;
    private double inversionMIA;
    private double saldoInicialMIA;

    private boolean pagaCreditos;
    private boolean healthEquals;
    private boolean secondMessage;
    private int topCenterGraphic;

    private int heigthHalos;
    private int countTips;
    private Bundle bundle;

    private boolean showCredit;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = this.getArguments();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView == null) {
            rootView = inflater.inflate(R.layout.layout_total_payments, container, false);
            init();
        }
        return rootView;
    }

    /**
     * TODO valida si se encuentra activa la vista de salud crediticia
     * @return
     */
    public boolean isShowCredit() {
        return showCredit;
    }

    public int getNumHalos() {
        return arrayLoands.size();
    }

    public ArrayList<Payments> getArrayLoands() {
        return arrayLoands;
    }


    private void init(){
        initObjet();
        getDataFromBundle();
        findViewById();
        setDataLoands(ahorro, pagoCreditos, gasto, deposito, inversion);
        delegate.calculateMax(gasto, ahorro, deposito + saldoInicial, pagoCreditos, inversion);
        delegate.setRadious();
        showCreditHealth(checkUpMA.getStatusExpDep());
        fillGraphics();
        Tools.numTips = 1;
    }

    private void initObjet(){

        activity = ((MainActivity)getActivity());

        delegate = new TotalPaymentsDelegate(this, rootView, activity);
        arrayLoands = new ArrayList<Payments>();
    }

    private void getDataFromBundle(){

        checkUpMA = (CheckUp) bundle.getSerializable(ConstantsCUF.TAG_OBJ_TOTAL_PAYMENTS);

        String depositoStr = (checkUpMA.getDeposito());
        String gastoStr = (checkUpMA.getGasto().replace("-", ConstantsCUF.EMPTY));
        String ahorroStr = (checkUpMA.getBalanceT());
        String pagoCreditosStr = (checkUpMA.getPagoCreditos());
        String inversionStr = (checkUpMA.getInversion());
        String saldoInicialStr = (checkUpMA.getBalanceI());

        deposito = Double.parseDouble(depositoStr);

        gasto = Double.parseDouble(gastoStr);
        ahorro = Double.parseDouble(ahorroStr);
        pagoCreditos = Double.parseDouble(pagoCreditosStr);
        inversion = Double.parseDouble(inversionStr);
        saldoInicial = Double.parseDouble(saldoInicialStr);
        saldoInicial = saldoInicial > ConstantsCUF._VALUE_ZERO ? saldoInicial: ConstantsCUF._VALUE_ZERO;
        pagaCreditos = checkUpMA.isPagaCreditos();
        //txtDisponible.setText(Tools.decimalFormat(String.valueOf(ahorro)));
        //txtDisponible.setTextColor(Tools.getIntColor(checkUpMA.getStatusExpDep(), getActivity()));

        valuesMA = delegate.copyData(checkUpMA.getStatusExpDep(),ConstantsCUF.EMPTY,ConstantsCUF.EMPTY, deposito, gasto, pagoCreditos, ahorro, saldoInicial, inversion,pagaCreditos);

    }

    private void setDataLoands(double a, double pc, double g, double d, double i){
        boolean iscurrentMonth = Tools.selectedMonth.equals(ConstantsCUF.TAG_CURRENT_MONTH);
        ArrayList<String> arrAmount = new ArrayList<String>();
        arrayLoands = new ArrayList<Payments>();
        Payments payments = new Payments();
        //if(a > ConstantsCUF._VALUE_ZERO) {
            payments.setAmount(a);//> ConstantsCUF._VALUE_ZERO ? a : ConstantsCUF._VALUE_ZERO);
            payments.setClve(ConstantsCUF.TAG_AHORRO);
            arrayLoands.add(payments);
            arrAmount.add(Tools.addDecimals(Math.floor(a)));
        //}

        if(i > ConstantsCUF._VALUE_ZERO) {
            payments = new Payments();
            payments.setAmount(i);
            payments.setClve(ConstantsCUF.TAG_INVERSION);
            arrayLoands.add(payments);
            arrAmount.add(Tools.addDecimals(Math.floor(i)));
        }

        if(pc > ConstantsCUF._VALUE_ZERO) {
            payments = new Payments();
            payments.setAmount(pc);
            payments.setClve(ConstantsCUF.TAG_PAGO_CREDITO);
            arrayLoands.add(payments);
            arrAmount.add(Tools.addDecimals(Math.floor(pc)));
        }

        if(g > ConstantsCUF._VALUE_ZERO) {
            payments = new Payments();
            payments.setAmount(g);
            payments.setClve(ConstantsCUF.TAG_GASTO);
            arrayLoands.add(payments);
            arrAmount.add(Tools.addDecimals(Math.floor(g)));
        }

        double saldo = iscurrentMonth == true ? saldoInicial : saldoInicialMIA;

        if(d > ConstantsCUF._VALUE_ZERO || saldo > ConstantsCUF._VALUE_ZERO) {
            payments = new Payments();
            payments.setAmount(d);
            payments.setClve(ConstantsCUF.TAG_DEPOSITO);
            arrayLoands.add(payments);
            arrAmount.add(Tools.addDecimals(Math.floor(d + saldo)));
        }
        Tools.resizeText(Tools.maxString(arrAmount, getActivity()), getActivity());

    }

    private void findViewById(){

        imgHeart = (ImageView) rootView.findViewById(R.id.imgHeart);
            imgBtnCoach = (ImageView) rootView.findViewById(R.id.imgBtnCoach);
            imgBtnCoach.setOnClickListener(this);


        rlGraphics = (RelativeLayout)rootView.findViewById(R.id.graphics);
        rlGraphics2 = (RelativeLayout)rootView.findViewById(R.id.graphics2);
        rlTABS = (RelativeLayout)rootView.findViewById(R.id.tlTabs);

        pieChart1 = (PieChart) rootView.findViewById(R.id.pieChart1);
        pieChart2 = (PieChart) rootView.findViewById(R.id.pieChart2);
        pieChart3 = (PieChart) rootView.findViewById(R.id.pieChart3);
        pieChart4 = (PieChart) rootView.findViewById(R.id.pieChart4);
        pieChart5 = (PieChart) rootView.findViewById(R.id.pieChart5);

        txtMonth = (TextView) rootView.findViewById(R.id.txtMonth) ;
        txtMonth.setText("Tu salud es:");
        txtStatus = (TextView) rootView.findViewById(R.id.txtStatus);

        lblDisponible = (TextView) rootView.findViewById(R.id.txtDisponible);
        lblDisponible.setText("Tu disponible es:");
        txtDisponible = (TextView)rootView.findViewById(R.id.txtStatusDisponible);
        txtCurrentMonthTab = (TextView) rootView.findViewById(R.id.txtCurrentMonthTab);
        txtLastMonthTab = (TextView) rootView.findViewById(R.id.txtLastMonthTab);

        txtCurrentMonthTab.setText("Hoy");//Tools.currentMonth);
        txtLastMonthTab.setText("Mes anterior");

        tabIndicatorMIA = rootView.findViewById(R.id.tabIndicatorMIA);
        tabIndicatorMA = rootView.findViewById(R.id.tabIndicatorMA);

        rlTabMA = (RelativeLayout) rootView.findViewById(R.id.rlTab2);
        rlTabMA.setOnClickListener(this);
        rlTabMIA = (RelativeLayout) rootView.findViewById(R.id.rlTab1);
        rlTabMIA.setOnClickListener(this);

    }

    public float getHeigthHalos() {
        return heigthHalos;
    }

    private void fillGraphics(){
        if(heigthHalos > 0){
            RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) rlGraphics.getLayoutParams();
            p.height = heigthHalos;
            rlGraphics.setLayoutParams(p);
        }
        ArrayList<Payments> arrarOrderly = arrayLoands;//delegate.orderLoands();
        if(arrarOrderly.size() == 5) {
            pieChart1 = delegate.configurePieChart(pieChart1, arrarOrderly.get((ConstantsCUF._VALUE_FOUR)));
            pieChart2 = delegate.configurePieChart(pieChart2, arrarOrderly.get((ConstantsCUF._VALUE_THREE)));
            pieChart3 = delegate.configurePieChart(pieChart3, arrarOrderly.get((ConstantsCUF._VALUE_TWO)));
            pieChart4 = delegate.configurePieChart(pieChart4, arrarOrderly.get((ConstantsCUF._VALUE_ONE)));
            pieChart5 = delegate.configurePieChart(pieChart5, arrarOrderly.get((ConstantsCUF._VALUE_ZERO)));
        }else if(arrarOrderly.size() == 4) {
            pieChart2 = delegate.configurePieChart(pieChart2, arrarOrderly.get((ConstantsCUF._VALUE_THREE)));
            pieChart3 = delegate.configurePieChart(pieChart3, arrarOrderly.get((ConstantsCUF._VALUE_TWO)));
            pieChart4 = delegate.configurePieChart(pieChart4, arrarOrderly.get((ConstantsCUF._VALUE_ONE)));
            pieChart5 = delegate.configurePieChart(pieChart5, arrarOrderly.get((ConstantsCUF._VALUE_ZERO)));
        }else if(arrarOrderly.size() == 3){
            pieChart3 = delegate.configurePieChart(pieChart3, arrarOrderly.get((ConstantsCUF._VALUE_TWO)));
            pieChart4 = delegate.configurePieChart(pieChart4, arrarOrderly.get((ConstantsCUF._VALUE_ONE)));
            pieChart5 = delegate.configurePieChart(pieChart5, arrarOrderly.get((ConstantsCUF._VALUE_ZERO)));
        }else if(arrarOrderly.size() == 2){
            pieChart4 = delegate.configurePieChart(pieChart4, arrarOrderly.get((ConstantsCUF._VALUE_ONE)));
            pieChart5 = delegate.configurePieChart(pieChart5, arrarOrderly.get((ConstantsCUF._VALUE_ZERO)));
        }else if(arrarOrderly.size() == 1){
            pieChart5 = delegate.configurePieChart(pieChart5, arrarOrderly.get((ConstantsCUF._VALUE_ZERO)));        }
    }

    private void showCreditHealth(String status){
//depositos todas las entradas de un cliente
        //gastos todas las salidas del cliente
        //ahorro
        double sum = pagoCreditos;
        double percentage = (sum/deposito)*ConstantsCUF.PERCENTAGE_100;
        String message = ConstantsCUF.EMPTY;
        if(status.equalsIgnoreCase(ConstantsCUF.EXCELENTE)){//(percentage <= ConstantsCUF.PERCENTAGE_GREAT){
            txtStatus.setText(ConstantsCUF.EXCELENTE);
            txtStatus.setTextColor(getResources().getColor(R.color.statusExcelente));
            imgHeart.setImageResource(R.drawable.corazon_verde);

        }else if(status.equalsIgnoreCase(ConstantsCUF.SALUDABLE)){//(percentage > ConstantsCUF.PERCENTAGE_GREAT && percentage <= ConstantsCUF.PERCENTAGE_GOOD){
            txtStatus.setText(ConstantsCUF.SALUDABLE);
            imgHeart.setImageResource(R.drawable.corazon_turquesa);
            txtStatus.setTextColor(getResources().getColor(R.color.statusSaludable));

        }else if(status.equalsIgnoreCase(ConstantsCUF._MALESTARES)){//if(percentage > ConstantsCUF.PERCENTAGE_GOOD && percentage <= ConstantsCUF.PERCENTAGE_MALAISE){
            txtStatus.setText(ConstantsCUF.MALESTARES);
            imgHeart.setImageResource(R.drawable.corazon_naranja);
            txtStatus.setTextColor(getResources().getColor(R.color.statusMalestares));
        }else if(status.equalsIgnoreCase(ConstantsCUF._CRITICO)){//if(percentage > ConstantsCUF.PERCENTAGE_MALAISE || sum > deposito){
            txtStatus.setText(ConstantsCUF.CRITICO);
            imgHeart.setImageResource(R.drawable.corazon_magenta);
            txtStatus.setTextColor(getResources().getColor(R.color.statusCritico));
        }
        //if(!status.equalsIgnoreCase(ConstantsCUF.EMPTY)) {
            txtDisponible.setText(Tools.decimalFormat(String.valueOf(Tools.selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? ahorro : ahorroMIA)));
            txtDisponible.setTextColor(Tools.getIntColor(Tools.selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? checkUpMA.getStatusExpDep() : checkUpMIA.getStatusExpDep(), getActivity()));
        //}
        //TODO se agregó cuando en el front se determinaba la salud
        /*else{
            int img =Tools.getImageHeart(tabIndicatorMA.isShown() == true ? deposito : depositoMIA,
                    tabIndicatorMA.isShown() == true ? gasto : gastoMIA,
                    tabIndicatorMA.isShown() == true ? pagoCreditos : pagoCreditosMIA,
                    tabIndicatorMA.isShown() == true ? ahorro : ahorroMIA,
                    tabIndicatorMA.isShown() == true ? saldoInicial : saldoInicialMIA);
            imgHeart.setImageResource(img);
            if(img == R.drawable.corazon_excelente_cuf) {
                txtStatus.setText(ConstantsCUF.EXCELENTE);
                txtStatus.setTextColor(getResources().getColor(R.color.statusExcelente));
            }else if(img == R.drawable.corazon_saludable_cuf) {
                txtStatus.setText(ConstantsCUF.SALUDABLE);
                txtStatus.setTextColor(getResources().getColor(R.color.statusSaludable));
            }else if(img == R.drawable.corazon_malestares_cuf) {
                txtStatus.setText(ConstantsCUF.MALESTARES);
                txtStatus.setTextColor(getResources().getColor(R.color.statusMalestares));
            }else if(img == R.drawable.corazon_critico_cuf) {
                txtStatus.setTextColor(getResources().getColor(R.color.statusCritico));
                txtStatus.setText(ConstantsCUF.CRITICO);
            }
        }*/
        activity.getDelegate().setStatus(txtStatus.getText().toString());
    }

    private String selectMessageFromCreditHealth(){
        String status = checkUpMIA.getStatusCredit();
        String message = ConstantsCUF.EMPTY;
        String amount;
        if(ConstantsCUF.EXCELENTE.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && ConstantsCUF.EXCELENTE.equalsIgnoreCase(status)){
            message = getResources().getString(R.string.caso_excVSexc_pc_tdc);
        }else if(ConstantsCUF.EXCELENTE.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && ConstantsCUF.SALUDABLE.equalsIgnoreCase(status)){
            message = getResources().getString(R.string.caso_excVSsble_pc_tdc);
        }else if(ConstantsCUF.EXCELENTE.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && ConstantsCUF._MALESTARES.equalsIgnoreCase(status)){
            message = getResources().getString(R.string.caso_excVSmlres_pc_tdc);
        }else if(ConstantsCUF.EXCELENTE.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && ConstantsCUF._CRITICO.equalsIgnoreCase(status)){
            message = getResources().getString(R.string.caso_excVScritico_pc_tdc);
        }else if(ConstantsCUF.SALUDABLE.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && ConstantsCUF.EXCELENTE.equalsIgnoreCase(status)){
            message = getResources().getString(R.string.caso_sbleVSexc_pc_tdc);
        }else if(ConstantsCUF.SALUDABLE.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && ConstantsCUF.SALUDABLE.equalsIgnoreCase(status)){
            message = getResources().getString(R.string.caso_sbleVSsble_pc_tdc);
        }else if(ConstantsCUF.SALUDABLE.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && ConstantsCUF._MALESTARES.equalsIgnoreCase(status)){
            message = getResources().getString(R.string.caso_sbleVSmlres_pc_tdc);
        }else if(ConstantsCUF.SALUDABLE.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && ConstantsCUF._CRITICO.equalsIgnoreCase(status)){
            message = getResources().getString(R.string.caso_sbleVScritico_pc_tdc);
        }else if(ConstantsCUF._MALESTARES.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && !ConstantsCUF._CRITICO.equalsIgnoreCase(status)){
            amount = Tools.decimalFormat(String.valueOf((gastoMIA + pagoCreditosMIA) - (saldoInicialMIA + depositoMIA)));
            message = getResources().getString(R.string.caso_mlresVSexc_pc_tdc).replace(getResources().getString(R.string.amount),amount);
        }else if(ConstantsCUF._MALESTARES.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && ConstantsCUF._CRITICO.equalsIgnoreCase(status)){
            amount = Tools.decimalFormat(String.valueOf((gastoMIA + pagoCreditosMIA) - (saldoInicialMIA + depositoMIA)));
            message = getResources().getString(R.string.caso_mlresVScritico_pc_tdc).replace(getResources().getString(R.string.amount),amount);
        }else if(ConstantsCUF._CRITICO.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && !ConstantsCUF._CRITICO.equalsIgnoreCase(status)){
            message = getResources().getString(R.string.caso_criticoVSecx_pc_tdc);
        }else if(ConstantsCUF._CRITICO.equalsIgnoreCase(checkUpMA.getStatusExpDep()) && ConstantsCUF._CRITICO.equalsIgnoreCase(status)){
            message = getResources().getString(R.string.caso_criticoVScritico_pc_tdc);
        }
        if(pagaCreditos && pagoCreditosMIA == 0)
            message = getResources().getString(R.string.caso_critico_mia_sc_impago);

        return message;
    }

    @Override
    public void onClick(View v) {
        if(v == imgBtnCoach){
            endAnimation();
            showTipFromTutorial(View.VISIBLE);
            startTutorial();
        }else if(v == rlTabMA){
            if(!Tools.selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH)) {
                Tools.selectedMonth = ConstantsCUF.TAG_CURRENT_MONTH;
                changeSelected();
            }
        }else if(v == rlTabMIA) {
            if(Tools.selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH)) {
                if(!Tools.isDataMIA)
                    checkUPMIA();
                else {
                    Tools.selectedMonth = ConstantsCUF.TAG_LAST_MONTH;
                    changeSelected();
                }
            }
        }else{
            activity.setActivityChanging(true);
            InitCheckUp.getInstance().getObj().showView(v.getId());
        }
    }
    public void setVisibilityTip(int visible){
        delegate.getRlMessageTip().setVisibility(visible);
    }

    public void startTutorial(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                activity.configureMessageWelcomeFromTutorial();
                delegate.getRlMessageTip().setVisibility(View.GONE);
                View[] view;
                view = new View[]{imgHeart, rlTABS,
                        rlGraphics2,
                        pieChart5,
                        delegate.getRlMessageTip(),
                        activity.getImgFoco(),
                        activity.getCirclePageIndicator(),
                        activity.getLnHeart(),
                        activity.getRlMessage()};
                activity.getImgFoco().setVisibility(View.GONE);

                Blurry.with(getActivity())
                        .radius(10)
                        .sampling(2)
                        .async()
                        .animate(0)
                        .onto(activity.getLnHeader(), activity.getLnWelcomTutorial(), topCenterGraphic, activity.getRlMain(), view,
                                activity.getHeigthHeader(),
                                txtMonth.getHeight() + rlTabMA.getHeight() + txtDisponible.getHeight(), delegate.getInitRadious(), isShowTip(), TotalPaymentsFragment.this);
                BBVACheckUpSharedPreferences.getTheInstance().setBooleanData(ConstantsCUF.SHOW_TUTORIAL, false);
            }

        }, 200);
    }

    public void visibleFoto(){
        activity.getImgFoco().setVisibility(View.VISIBLE);

    }
    /**
     * Se cuentan los tips en el caso de que la salud sea critica
     * @param status
     */
    private int countTip(String status){
        int count = 1;
        if(checkUpMIA.getStatusCredit().equalsIgnoreCase(status)){
            BBVACheckUpSharedPreferences.getTheInstance().setStringData(ConstantsCUF.HEALTH_CREDIT,status);
            if(checkUpMIA.getStatusExpDep().equalsIgnoreCase(ConstantsCUF._MALESTARES) ||
                    checkUpMIA.getStatusExpDep().equalsIgnoreCase(ConstantsCUF._CRITICO)) {
                count++;
            }
        }else {
            BBVACheckUpSharedPreferences.getTheInstance().setStringData(ConstantsCUF.HEALTH_EXPENSES,status);
            if(checkUpMIA.getStatusCredit().equalsIgnoreCase(ConstantsCUF._MALESTARES)){
                count++;
            }
        }
        return count;
    }

    private String  seleccionarSalud(){
        String status;
        if(pagaCreditos) {
            if (checkUpMIA.getStatusExpDep().equalsIgnoreCase(checkUpMIA.getStatusCredit())) {
                status = checkUpMIA.getStatusExpDep();
                healthEquals = true;
            } else if (checkUpMIA.getStatusCredit().equalsIgnoreCase(ConstantsCUF._CRITICO))
                status = ConstantsCUF._CRITICO;
            else if (checkUpMIA.getStatusExpDep().equalsIgnoreCase(ConstantsCUF._CRITICO)) {
                status = ConstantsCUF._CRITICO;
            } else if (checkUpMIA.getStatusCredit().equalsIgnoreCase(ConstantsCUF._MALESTARES))
                status = ConstantsCUF._MALESTARES;
            else if (checkUpMIA.getStatusExpDep().equalsIgnoreCase(ConstantsCUF._MALESTARES)) {
                status = ConstantsCUF._MALESTARES;
            } else if (checkUpMIA.getStatusCredit().equalsIgnoreCase(ConstantsCUF.SALUDABLE))
                status = ConstantsCUF.SALUDABLE;
            else {
                status = checkUpMIA.getStatusExpDep();
            }
        }else
            status = checkUpMIA.getStatusExpDep();
        return  status;
    }

    public String getStatus(String op){
        String status;
        if( tabIndicatorMA.isShown() || checkUpMIA == null){
            status = checkUpMA.getStatusExpDep();
        }else{
            if(op.equalsIgnoreCase(ConstantsCUF.TAG_AHORRO)){
                status = checkUpMIA.getStatusExpDep();
            }else if(op.equalsIgnoreCase(ConstantsCUF.TAG_PAGO_CREDITO))
                status = checkUpMIA.getStatusCredit();
            else{
                status = checkUpMIA.getStatusExpDep();
            }
        }
        return status;
    }

    public void calculateSpace(int total){
        if(txtMonth != null && heigthHalos == 0){
            heigthHalos = (int) (total - txtMonth.getHeight() - txtDisponible.getHeight() - 140*getActivity().getResources().getDisplayMetrics().scaledDensity);

            if(BBVACheckUpSharedPreferences.getTheInstance().getStringData(ConstantsCUF.HEALTH).equalsIgnoreCase(ConstantsCUF.EMPTY))
                BBVACheckUpSharedPreferences.getTheInstance().setStringData(ConstantsCUF.HEALTH, checkUpMA.getStatusExpDep());
            else if(!BBVACheckUpSharedPreferences.getTheInstance().getStringData(ConstantsCUF.HEALTH).equalsIgnoreCase(checkUpMA.getStatusExpDep())) {
                BBVACheckUpSharedPreferences.getTheInstance().setBooleanData(ConstantsCUF.SHOW_TIPMA, true);
                BBVACheckUpSharedPreferences.getTheInstance().setStringData(ConstantsCUF.HEALTH, checkUpMA.getStatusExpDep());
            }


            showTip(ConstantsCUF._VALUE_ONE);
            showCreditHealth(ConstantsCUF.EMPTY);
            RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) rlGraphics.getLayoutParams();

            if(heigthHalos <= 150) {
                heigthHalos = 150;
                dimensPie();
            }

            android.util.Log.e(getClass().getSimpleName(), "tamaño de la grafica: " + heigthHalos);
            p.height = heigthHalos;
            /*if(delegate.getRlMessageTip() == null || !delegate.getRlMessageTip().isShown()){
                p.height = heigthHalos + (int) (140 * getActivity().getResources().getDisplayMetrics().scaledDensity) -(int)activity.getHeigthCirclePager()- rlTABS.getHeight();
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rlGraphics2.getLayoutParams();
                params.topMargin = ((p.height-heigthHalos)/2);
                params.height = heigthHalos;
                topCenterGraphic = params.topMargin ;
                rlGraphics2.setLayoutParams(params);
            }*/
            rlGraphics.setLayoutParams(p);

            int min = Math.min(heigthHalos, getResources().getDisplayMetrics().widthPixels);
            float space = min/2 *delegate.getRadious()/100;
            space -= 4;
            android.util.Log.e(getClass().getSimpleName(), "space" + space);
            android.util.Log.e(getClass().getSimpleName(), "image width" + imgHeart.getMeasuredWidth());
            android.util.Log.e(getClass().getSimpleName(), "grosos: "+ (Tools.convertDpToPixel(getActivity(),Tools.get_border()))/2);
            float spaceTotal = ((space) *2 - (Tools.convertDpToPixel(getActivity(),Tools.get_border())));
            if(imgHeart.getMeasuredWidth() > spaceTotal){
                imgHeart.getLayoutParams().height = (int)spaceTotal;
                imgHeart.getLayoutParams().width = (int)spaceTotal;
                imgHeart.requestLayout();
            }

        }
    }

    /*public void centerGraphic(){
        RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) rlGraphics.getLayoutParams();
        if(heigthHalos == p.height && delegate.getRlMessageTip()!= null && isShowTip() == View.GONE) {
            p.height = heigthHalos + (int) (140 * getActivity().getResources().getDisplayMetrics().scaledDensity) -(int)activity.getHeigthCirclePager()- rlTABS.getHeight();
        }else {
            p.height = heigthHalos;
        }

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rlGraphics2.getLayoutParams();
        params.topMargin =  ((heigthHalos == p.height ? 0:(p.height-heigthHalos)/2));
        params.height = heigthHalos;
        topCenterGraphic = params.topMargin ;

        rlGraphics2.setLayoutParams(params);

        rlGraphics.setLayoutParams(p);
    }*/

    private int isShowTip(){
       return Tools.selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH)?
               BBVACheckUpSharedPreferences.getTheInstance().getBooleanData(ConstantsCUF.SHOW_TIPMA) == true ?View.VISIBLE : View.GONE:
               BBVACheckUpSharedPreferences.getTheInstance().getBooleanData(ConstantsCUF.SHOW_TIPMIA) == true ?View.VISIBLE : View.GONE;
    }
    public void  showTipFromTutorial(int visibleTip){
        if(visibleTip == View.VISIBLE) {
            int op = delegate.changeVisibilityTipFromTurotial();
            if (op == ConstantsCUF._VALUE_ONE)
                showTip(ConstantsCUF._VALUE_ONE);
            else if(op == ConstantsCUF._VALUE_TWO){
                delegate.getRlMessageTip().setVisibility(View.VISIBLE);
            }
        }else if(visibleTip == View.GONE){
            delegate.getRlMessageTip().setVisibility(View.GONE);
        }
    }

    public void checkUPMIA(){
        delegate.realizaOperacion();
    }

    public void changeSelected(){
        boolean isCurrentMonth = Tools.selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? true : false;

            tabIndicatorMIA.setVisibility(isCurrentMonth == true ? View.GONE: View.VISIBLE);
            tabIndicatorMA.setVisibility(isCurrentMonth == true ?View.VISIBLE : View.GONE);
            rlTabMIA.setBackgroundColor(isCurrentMonth == true ? Color.TRANSPARENT : getResources().getColor(R.color.colorTabSelected));
            rlTabMA.setBackgroundColor(isCurrentMonth == true ? getResources().getColor(R.color.colorTabSelected) : Color.TRANSPARENT);
            txtLastMonthTab.setTextColor(isCurrentMonth == true ? getResources().getColor(R.color.colorTextTabNoSelected) : getResources().getColor(R.color.colorTextTabSelected));
            txtCurrentMonthTab.setTextColor(isCurrentMonth == true ? getResources().getColor(R.color.colorTextTabSelected) : getResources().getColor(R.color.colorTextTabNoSelected));
            reloadGraphic(isCurrentMonth);
    }

    /**
     * llena los datos del mes anterior
     * @param deposito
     * @param gasto
     * @param ahorro
     * @param pagoCreditos
     * @param inv
     * @param saldoI
     * @param checkUpMIA
     * @param pagaCreditos
     */
    public void selectMIA(String deposito, String gasto, String ahorro, String pagoCreditos, String inv, String saldoI, CheckUp checkUpMIA, boolean pagaCreditos){
        this.checkUpMIA = checkUpMIA;
        checkUpMIA.setStatus(seleccionarSalud());
        depositoMIA = Double.parseDouble(deposito);
        gastoMIA = Double.parseDouble(gasto);
        ahorroMIA = Double.parseDouble(ahorro);
        pagoCreditosMIA = Double.parseDouble(pagoCreditos);
        inversionMIA = Double.parseDouble(inv);
        saldoInicialMIA = Double.parseDouble(saldoI);

        valuesMIA = delegate.copyData(checkUpMIA.getStatus(),checkUpMIA.getStatusCredit(),checkUpMIA.getStatusExpDep(),depositoMIA, gastoMIA, pagoCreditosMIA, ahorroMIA, saldoInicialMIA, inversionMIA,pagaCreditos);

        Tools.isDataMIA = true;
        changeSelected();
        //setVisibilityBtnCoach();
    }

    /**
     * Recarga los valores de la grafica dependiendo el mes seleccionado
     */
    private void reloadGraphic(boolean isCurrent){
        if(delegate.getRlMessageTip() != null)
            delegate.getRlMessageTip().setVisibility(View.GONE);
            showCredit = false;
            txtMonth.setText(isCurrent == true ? "Tu salud es: " : "Tu salud fue: ");
            lblDisponible.setText(isCurrent == true ? "Tu disponible es: " : "Tu disponible fue: ");
            showCreditHealth(isCurrent == true ? checkUpMA.getStatusExpDep() : checkUpMIA.getStatus());
            setDataLoands(isCurrent == true ? ahorro : ahorroMIA,
                    isCurrent == true ? pagoCreditos : pagoCreditosMIA,
                    isCurrent == true ? gasto : gastoMIA,
                    isCurrent == true ? deposito : depositoMIA,
                    isCurrent == true ? inversion : inversionMIA);
            clearGrapics();
            delegate.calculateMax(isCurrent == true ? gasto : gastoMIA,
                    isCurrent == true ? ahorro : ahorroMIA,
                    isCurrent == true ? deposito + saldoInicial : depositoMIA + saldoInicialMIA,
                    isCurrent == true ? pagoCreditos : pagoCreditosMIA,
                    isCurrent == true ? inversion : inversionMIA);
            delegate.setRadious();

        Tools.numTips = ConstantsCUF._VALUE_ONE;//isCurrent == true ? ConstantsCUF._VALUE_ONE : countTip(checkUpMIA.getStatus());
        countTips = Tools.numTips-1;
        showTip(Tools.numTips);
            fillGraphics();
            //centerGraphic();
    }

    /**
     * Manda a llamar al método para mostrar el tip.
     */
    private void showTip(int tip) {
        if (tip == ConstantsCUF._VALUE_ONE) {

            String mensaje = Tools.selectMessageToDisplayWithStatus(getActivity(),
                    tabIndicatorMA.isShown() == true ? valuesMA : valuesMIA);
            if (mensaje.equalsIgnoreCase(ConstantsCUF.EMPTY))
                mensaje = selectMessageFromCreditHealth();
            delegate.showTip(mensaje, ConstantsCUF.EMPTY,
                    tabIndicatorMA.isShown() == true ? ConstantsCUF._VALUE_ONE : ConstantsCUF._VALUE_TWO);
        } else if (tip == ConstantsCUF._VALUE_TWO) {
            if (valuesMIA.getStatus().equalsIgnoreCase(valuesMIA.getStatusSC())) {
                delegate.showTip(selectMessageFromCreditHealth(), Tools.selectMessageToDisplayWithStatus(getActivity(), valuesMIA), ConstantsCUF._VALUE_TWO);
            } else {
                delegate.showTip(Tools.selectMessageToDisplayWithStatus(getActivity(), valuesMIA), selectMessageFromCreditHealth(), ConstantsCUF._VALUE_TWO);
            }
        } else if(tip == -100){
            if (valuesMIA.getStatus().equalsIgnoreCase(valuesMIA.getStatusSC())) {
                delegate.showTip(Tools.selectMessageToDisplayWithStatus(getActivity(), valuesMIA),ConstantsCUF.EMPTY, ConstantsCUF._VALUE_TWO);
            } else {
                delegate.showTip(selectMessageFromCreditHealth(), ConstantsCUF.EMPTY, ConstantsCUF._VALUE_TWO);
            }
        }else if(delegate.getRlMessageTip() != null)
            delegate.getRlMessageTip().setVisibility(View.VISIBLE);

        if(Tools.numTips == ConstantsCUF._VALUE_TWO) {
            if ((delegate.getRlMessageTip() == null || !delegate.getRlMessageTip().isShown()))
                activity.changeNumTip(View.VISIBLE, String.valueOf(ConstantsCUF._VALUE_TWO));
            else if (!activity.getNumberTip().equalsIgnoreCase(String.valueOf(ConstantsCUF._VALUE_ONE))) {
                activity.changeNumTip(View.VISIBLE, String.valueOf(ConstantsCUF._VALUE_ONE));
            } else{
                    activity.changeNumTip(View.GONE, String.valueOf(ConstantsCUF._VALUE_TWO));
            }
        }else if(Tools.numTips == ConstantsCUF._VALUE_ONE){
            if ((delegate.getRlMessageTip() == null || !delegate.getRlMessageTip().isShown()))
                activity.changeNumTip(View.VISIBLE, String.valueOf(ConstantsCUF._VALUE_ONE));
            else if (delegate.getRlMessageTip().isShown()) {
                activity.changeNumTip(View.GONE, ConstantsCUF.EMPTY);
            }
        }

    }

    /**
     * Cambia el estado del tip mostrado de acuerdo a la vista en que se seleccioné el foco
     * y lo almacena.
     */
    public void onOfTip(){
        if(Server.ALLOW_LOG)
            android.util.Log.i(getClass().getSimpleName(), String.format("onOfTip \n tabIndicatorMA.isShown() %s\n tabIndicatorMIA.isShown() %s\n showCredi %s", tabIndicatorMA.isShown(), tabIndicatorMIA.isShown(), showCredit));


        boolean showTip = false;
        android.util.Log.v(getClass().getSimpleName(), "count tips: " + countTips);

        if(!tabIndicatorMA.isShown() && delegate.getRlMessageTip() != null && !delegate.getRlMessageTip().isShown())
            countTips = 1;

        if(Tools.numTips == ConstantsCUF._VALUE_ONE) {
            showTip = BBVACheckUpSharedPreferences.getTheInstance().getBooleanData(tabIndicatorMA.isShown() == true ? ConstantsCUF.SHOW_TIPMA : ConstantsCUF.SHOW_TIPMIA) == true ? false : true;
            BBVACheckUpSharedPreferences.getTheInstance().setBooleanData(tabIndicatorMA.isShown() == true ? ConstantsCUF.SHOW_TIPMA : ConstantsCUF.SHOW_TIPMIA, showTip);
            showTip(Tools.numTips);
        }else if(countTips == Tools.numTips-1 && delegate.getRlMessageTip() != null && delegate.getRlMessageTip().isShown()){
            showTip = true;
            secondMessage = false;
            countTips--;
            BBVACheckUpSharedPreferences.getTheInstance().setBooleanData(ConstantsCUF.SHOW_TIPMIA, showTip);
            showTip(-100);
        }else if(countTips == Tools.numTips-1){
            showTip = true;
            secondMessage = true;
            countTips--;
            BBVACheckUpSharedPreferences.getTheInstance().setBooleanData(ConstantsCUF.SHOW_TIPMIA, showTip);
            showTip(Tools.numTips);
        }else if(countTips == Tools.numTips-2){

            BBVACheckUpSharedPreferences.getTheInstance().setBooleanData(ConstantsCUF.SHOW_TIPMIA, secondMessage);
            showTip(-100);
            countTips--;
        }else{
            countTips = Tools.numTips-1;
            BBVACheckUpSharedPreferences.getTheInstance().setBooleanData(ConstantsCUF.SHOW_TIPMIA, false);
            showTip(-100);
        }
    }


    public double getSaldoInicial() {
        return  Tools.selectedMonth.equals(ConstantsCUF.TAG_CURRENT_MONTH)?(saldoInicial):(saldoInicialMIA);

    }

    private void clearGrapics() {
        Payments payments = new Payments();
        payments.setAmount(0.0);
        payments.setClve("");

        pieChart1 = delegate.configurePieChart(pieChart1,payments);
        pieChart2 = delegate.configurePieChart(pieChart2,payments);
        pieChart3 = delegate.configurePieChart(pieChart3,payments);
        pieChart4 = delegate.configurePieChart(pieChart4,payments);
        pieChart5 = delegate.configurePieChart(pieChart5,payments);

    }

    public void dimensPie(){
        if(getHeigthHalos()>ConstantsCUF._VALUE_ZERO) {
            RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) pieChart1.getLayoutParams();
            p.height = (int)getHeigthHalos();
            pieChart1.setLayoutParams(p);
            pieChart2.setLayoutParams(p);
            pieChart3.setLayoutParams(p);
            pieChart4.setLayoutParams(p);
            pieChart5.setLayoutParams(p);
        }
    }

    private void endAnimation(){
        pieChart1.endAnimationXY();
        pieChart2.endAnimationXY();
        pieChart3.endAnimationXY();
        pieChart4.endAnimationXY();
        pieChart5.endAnimationXY();
    }

    public void setSecondMessage(boolean secondMessage) {
        this.secondMessage = secondMessage;
    }

    public void changeNumTip(){
        activity.changeNumTip(Tools.numTips == 1 ?View.VISIBLE : View.GONE,String.valueOf(ConstantsCUF._VALUE_ONE));
        //activity.changeNumTip(Tools.numTips == 2 ?View.VISIBLE : View.GONE,String.valueOf(ConstantsCUF._VALUE_TWO));
    }
    public void setCountTips(int countTips) {
        this.countTips = countTips;
    }
}
