package com.bancomer.apicheckupfinanciero.gui.Delegates;

import android.graphics.Typeface;
import android.os.Build;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.controller.InitCheckUp;
import com.bancomer.apicheckupfinanciero.gui.views.activities.BaseActivity;
import com.bancomer.apicheckupfinanciero.gui.views.activities.MainActivity;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.CreditPaymentsFragment;
import com.bancomer.apicheckupfinanciero.io.Server;
import com.bancomer.apicheckupfinanciero.models.CheckUp;
import com.bancomer.apicheckupfinanciero.models.PagoCreditos;
import com.bancomer.apicheckupfinanciero.utils.CustomerTextView;
import com.bancomer.apicheckupfinanciero.utils.ScaleTool;
import com.bancomer.apicheckupfinanciero.utils.Tools;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import bancomer.api.common.commons.ServerConstants;
import bancomer.api.common.io.ServerResponse;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;

/**
 * Created by DMEJIA on 21/02/16.
 */
public class MainDelegate extends BaseDelegateImp {
    public static final long MAIN_CONTROLLER = 201602214566765659L;

    private BaseActivity activity;
    private MainActivity mainActivity;


    private String deposito;
    private String gasto;
    private String ahorro;
    private String pagoCreditos;
    private String lastMonth;

    private String status;

    private CheckUp checkUp;

    private PagoCreditos objPagoCreditos;

    public MainDelegate() {
    }

    public void setActivity(BaseActivity activity){
        this.activity = activity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void realizaOperacion(int opId, BaseActivity activity){
        this.activity = activity;
        String numeroCelular = InitCheckUp.getInstance().getSession().getUsername();
        Map<String, String> paramsUrl = new HashMap<String, String>();
        paramsUrl.put("{cellphoneNumber}", numeroCelular);
        //paramTable.put(ApiConstants.USR_STRING, InitCheckUp.getInstance().getSession().getUsername());
        //paramTable.put(ConstantsCUF.TAG_MES, opId == Server.CONSULTA_ESTADO_FIANCIERO_MA ?"MA": "MIA");
        if(Server.ALLOW_LOG)
            android.util.Log.w("paramsUrl", numeroCelular);
        doNetworkOperation(opId, paramsUrl, activity.getDelegate());
    }


    @Override
    public void analyzeResponse(int i, ServerResponse serverResponse) {
        android.util.Log.e("dora", "regresa a analizar la respuesta =D");
        if(i == Server.CONSULTA_ESTADO_FIANCIERO_MA) {
            if(serverResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {

                CheckUp cltaEdoFinanciero = (CheckUp) serverResponse.getResponse();
                //CltaEdoFinanciero cltaEdoFinanciero = (CltaEdoFinanciero) serverResponse.getResponse();
                deposito = cltaEdoFinanciero.getDeposito();
                gasto = cltaEdoFinanciero.getGasto();
                gasto = Double.parseDouble(gasto) < 0?String.valueOf(Double.parseDouble(gasto) * -1):gasto;
                ahorro = cltaEdoFinanciero.getBalanceT();
               // ahorro = cltaEdoFinanciero.getAhorro();
                pagoCreditos = cltaEdoFinanciero.getPagoCreditos();
                lastMonth = cltaEdoFinanciero.getLastMonth();
                checkUp = cltaEdoFinanciero;

                android.util.Log.e("CHECKUP::", "");
                Tools.currentMonth = lastMonth;
                Tools.lastMonth = Tools.getCurrentMont(lastMonth);

                    InitCheckUp.getInstance().getActivityController
                            ().showScreen(MainActivity.class);

            }else{

                if(serverResponse.getMessageCode().equalsIgnoreCase("BNE1012")){
                    Tools.showErrorNomina(InitCheckUp.getInstance().getActivityController().getCurrentActivity());
                }else
                    Tools.showErrorMessage(serverResponse.getMessageText(), InitCheckUp.getInstance().getActivityController().getCurrentActivity(),null);

                InitCheckUp.getInstance().getObj().habilitarVista();
                    InitCheckUp.getInstance().getObj().exitSaludFinanciera();
                    InitCheckUp.resetInstace();
            }
        }else if(i == Server.LOGIN){
            if(serverResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                InitCheckUp.getInstance().getBaseSubapplication().getServer().cancelNetworkOperations();
                String numeroCelular = InitCheckUp.getInstance().getSession().getUsername();
                Map<String, String> paramsUrl = new HashMap<String, String>();
                paramsUrl.put("{cellphoneNumber}", numeroCelular);
               doNetworkOperation(Server.CONSULTA_ESTADO_FIANCIERO_MA, paramsUrl, activity.getDelegate());
            }else{
                if(serverResponse.getMessageCode().equalsIgnoreCase("BNE1012")){
                    Tools.showErrorNomina(InitCheckUp.getInstance().getActivityController().getCurrentActivity());
                }else
                    Tools.showErrorMessage(serverResponse.getMessageText(), InitCheckUp.getInstance().getActivityController().getCurrentActivity(),null);

                InitCheckUp.getInstance().getObj().habilitarVista();
                InitCheckUp.getInstance().getObj().exitSaludFinanciera();
                InitCheckUp.resetInstace();
            }

        }else if(i == Server.CONSULTA_DETALLE_CREDITOS){
            if(serverResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                InitCheckUp.getInstance().getActivityController
                        ().showScreen(MainActivity.class);

                PagoCreditos pagoCreditos = (PagoCreditos)serverResponse.getResponse();
                this.objPagoCreditos = pagoCreditos;

            }else{
                Tools.showErrorMessage(serverResponse.getMessageText(), InitCheckUp.getInstance().getActivityController().getCurrentActivity(),null);

                InitCheckUp.getInstance().getObj().habilitarVista();
                InitCheckUp.getInstance().getObj().exitSaludFinanciera();
                InitCheckUp.resetInstace();
            }
        }
    }
    public void doNetworkOperation(int operationId, Map<String, String> params, bancomer.api.common.gui.delegates.BaseDelegate caller) {
        final Map<String, String> headers = new HashMap<String, String>();
        if(Server.ALLOW_LOG)
            android.util.Log.i(":::IUM:::", "IUM: "+ InitCheckUp.getInstance().getSession().getIum());
        headers.put(ApiConstants.IUM_STRING, InitCheckUp.getInstance().getSession().getIum());
        InitCheckUp.getInstance().getBaseSubapplication().invokeNetworkOperation(operationId, params, headers,caller, new CheckUp(),"Operación en curso", "Conectando con servidor remoto", false);

    }

    public void loadData(){
        /*if(Double.parseDouble(ahorro) == ConstantsCUF._VALUE_ZERO || Double.parseDouble(ahorro) < ConstantsCUF._VALUE_ZERO) {
            showTip = true;
        }*/
        mainActivity.createViewPager(checkUp);//(deposito, gasto, ahorro, pagoCreditos, lastMonth);
    }

    /*public void hideView(){
        mainActivity.fullScreen();
    }*/


    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }



    public PagoCreditos getObjPagoCreditos() {
        return objPagoCreditos;
    }

    public void setObjPagoCreditos(PagoCreditos objPagoCreditos) {
        this.objPagoCreditos = objPagoCreditos;
    }
}
