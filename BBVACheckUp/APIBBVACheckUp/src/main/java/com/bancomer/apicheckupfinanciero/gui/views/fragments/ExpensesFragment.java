package com.bancomer.apicheckupfinanciero.gui.views.fragments;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.utils.CustomerTextView;

/**
 * Created by DMEJIA on 10/02/16.
 */
public class ExpensesFragment extends Fragment implements View.OnLayoutChangeListener,View.OnClickListener{

    private View rootView;
    private View v;

    private ImageView line1;
    private ImageView line2;
    private ImageView line3;
    private ImageView line4;
    private ImageView line5;
    private ImageView line6;

    private ImageView indicator1;
    private ImageView indicator2;
    private ImageView indicator3;
    private ImageView indicator4;
    private ImageView indicator5;
    private ImageView indicator6;

    private ImageView toolTip1;
    private ImageView toolTip2;
    private ImageView toolTip3;
    private ImageView toolTip4;
    private ImageView toolTip5;
    private ImageView toolTip6;

    private ImageView imgLoands;
    private ImageView imgLoands2;
    private ImageView imgLoands3;
    private ImageView imgLoands4;
    private ImageView imgLoands5;
    private ImageView imgLoands6;

    private CustomerTextView txtAmount;
    private CustomerTextView txtAmount2;
    private CustomerTextView txtAmount3;
    private CustomerTextView txtAmount4;
    private CustomerTextView txtAmount5;
    private CustomerTextView txtAmount6;

    private CustomerTextView txtDate;
    private CustomerTextView txtDate2;
    private CustomerTextView txtDate3;
    private CustomerTextView txtDate4;
    private CustomerTextView txtDate5;
    private CustomerTextView txtDate6;

    private float left;

    private int[] date;
    private int count;

    private boolean showDetail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_expenses, container, false);
        init();
        return rootView;
    }

    private void init(){
        findView();
        date = new int[]{14,7,18,30,5,1};
        count = 0;
        showDetail = true;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void findView(){
        txtAmount = (CustomerTextView) rootView.findViewById(R.id.txt_amount);
        txtAmount2 = (CustomerTextView) rootView.findViewById(R.id.txt_amount2);
        txtAmount3 = (CustomerTextView) rootView.findViewById(R.id.txt_amount3);
        txtAmount4 = (CustomerTextView) rootView.findViewById(R.id.txt_amount4);
        txtAmount5 = (CustomerTextView) rootView.findViewById(R.id.txt_amount5);
        txtAmount6 = (CustomerTextView) rootView.findViewById(R.id.txt_amount6);

        txtDate = (CustomerTextView) rootView.findViewById(R.id.txt_date);
        txtDate.setVisibility(View.GONE);
        txtDate2 = (CustomerTextView) rootView.findViewById(R.id.txt_date2);
        txtDate2.setVisibility(View.GONE);
        txtDate3 = (CustomerTextView) rootView.findViewById(R.id.txt_date3);
        txtDate3.setVisibility(View.GONE);
        txtDate4 = (CustomerTextView) rootView.findViewById(R.id.txt_date4);
        txtDate4.setVisibility(View.GONE);
        txtDate5 = (CustomerTextView) rootView.findViewById(R.id.txt_date5);
        txtDate5.setVisibility(View.GONE);
        txtDate6 = (CustomerTextView) rootView.findViewById(R.id.txt_date6);
        txtDate6.setVisibility(View.GONE);

        indicator1 = (ImageView) rootView.findViewById(R.id.indicator);
        indicator2 = (ImageView) rootView.findViewById(R.id.indicator2);
        indicator3 = (ImageView) rootView.findViewById(R.id.indicator3);
        indicator4 = (ImageView) rootView.findViewById(R.id.indicator4);
        indicator5 = (ImageView) rootView.findViewById(R.id.indicator5);
        indicator6 = (ImageView) rootView.findViewById(R.id.indicator6);

        toolTip1 = (ImageView) rootView.findViewById(R.id.img_tool_tip);
        toolTip2 = (ImageView) rootView.findViewById(R.id.img_tool_tip2);
        toolTip3 = (ImageView) rootView.findViewById(R.id.img_tool_tip3);
        toolTip4 = (ImageView) rootView.findViewById(R.id.img_tool_tip4);
        toolTip5 = (ImageView) rootView.findViewById(R.id.img_tool_tip5);
        toolTip6 = (ImageView) rootView.findViewById(R.id.img_tool_tip6);

        line2 = (ImageView) rootView.findViewById(R.id.lineTime2);
        line3 = (ImageView) rootView.findViewById(R.id.lineTime3);
        line4 = (ImageView) rootView.findViewById(R.id.lineTime4);
        line5 = (ImageView) rootView.findViewById(R.id.lineTime5);
        line6 = (ImageView) rootView.findViewById(R.id.lineTime6);

        imgLoands = (ImageView) rootView.findViewById(R.id.imgLoands);
        imgLoands.setOnClickListener(this);

        imgLoands2 = (ImageView) rootView.findViewById(R.id.imgLoands2);
        imgLoands2.setOnClickListener(this);

        imgLoands3 = (ImageView) rootView.findViewById(R.id.imgLoands3);
        imgLoands3.setOnClickListener(this);

        imgLoands4 = (ImageView) rootView.findViewById(R.id.imgLoands4);
        imgLoands4.setOnClickListener(this);

        imgLoands5 = (ImageView) rootView.findViewById(R.id.imgLoands5);
        imgLoands5.setOnClickListener(this);

        imgLoands6 = (ImageView) rootView.findViewById(R.id.imgLoands6);
        imgLoands6.setOnClickListener(this);

        line1 = (ImageView) rootView.findViewById(R.id.lineTime);
        line1.addOnLayoutChangeListener((View.OnLayoutChangeListener) this);
    }

    @Override
    public void onClick(View v) {
        int n = 30;
        FragmentManager fm = getActivity().getSupportFragmentManager();
        DetailCredtPaymentsFragment dialog = null;

        android.util.Log.e("o_o", "tags:  " + v.getTag() +  " id de la imagen auto " + R.drawable.ico_pop_auto_ver) ;

            if (v == imgLoands)
                dialog = new DetailCredtPaymentsFragment(txtAmount.getText().toString(),
                        "", ConstantsCUF.EXCELENTE,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_auto_ver), date[0], n, true, this, null);
            else if (v == imgLoands2)
                dialog = new DetailCredtPaymentsFragment(txtAmount2.getText().toString(),
                        "", ConstantsCUF.SALUDABLE,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_hipo_ver),
                        date[1], n, true, this, null);
            else if (v == imgLoands3)
                dialog = new DetailCredtPaymentsFragment(txtAmount3.getText().toString(),
                        "", ConstantsCUF.CRITICO,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_nomi_ver),
                        date[2], n, true, this, null);
            else if (v == imgLoands4)
                dialog = new DetailCredtPaymentsFragment(txtAmount4.getText().toString(),
                        "", ConstantsCUF.MALESTARES,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_pers_ver),
                        date[3], n, true, this, null);
            else if (v == imgLoands5)
                dialog = new DetailCredtPaymentsFragment(txtAmount5.getText().toString(),
                        "", ConstantsCUF.EXCELENTE,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_tarcre_ver),
                        date[4], n, true, this, null);
            else if (v == imgLoands6)
                dialog = new DetailCredtPaymentsFragment(txtAmount6.getText().toString(),
                        "", ConstantsCUF.EXCELENTE,
                        getActivity().getResources().getDrawable(R.drawable.ico_pop_compra_ver),
                        date[5], n, true, this, null);
            dialog.show(fm, "Ticket");

    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        this.left = left;
        this.v = v;
        positionViews(txtAmount,indicator1, toolTip1);
        positionViews(txtAmount2, indicator2, toolTip2);
        positionViews(txtAmount3, indicator3, toolTip3);
        positionViews(txtAmount4, indicator4, toolTip4);
        positionViews(txtAmount5, indicator5, toolTip5);
        positionViews(txtAmount6, indicator6, toolTip6);

        line1.removeOnLayoutChangeListener(this);
    }

    private void positionViews(final TextView txtAmount, final ImageView indicator, final ImageView toolTip){
        txtAmount.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams)indicator.getLayoutParams();
                float margint =  (5 * getResources().getDisplayMetrics().scaledDensity);
                p.leftMargin = (int) (left + ((v.getWidth()- margint*2)/30*date[count])+ margint);
                count++;
                indicator.setLayoutParams(p);


                final int w = (int) txtAmount.getWidth();

                if(w>0) {

                    RelativeLayout.LayoutParams t = (RelativeLayout.LayoutParams)toolTip.getLayoutParams();
                    t.leftMargin = (int)((p.leftMargin - (w/2)));
                    t.topMargin = indicator.getTop() -(int)(txtAmount.getHeight());
                    t.width = w;
                    t.height = (int) (txtAmount.getMeasuredHeight());

                    toolTip.setLayoutParams(t);

                    RelativeLayout.LayoutParams a = (RelativeLayout.LayoutParams) txtAmount.getLayoutParams();
                    a.leftMargin = p.leftMargin  -   w / 2;
                    a.topMargin = (int) (t.topMargin*.75);

                    txtAmount.setLayoutParams(a);

                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                        txtAmount.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        txtAmount.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }

                }



            }
        });
    }

    public void setShowDetail(boolean showDetail){
        this.showDetail = showDetail;
    }
}
