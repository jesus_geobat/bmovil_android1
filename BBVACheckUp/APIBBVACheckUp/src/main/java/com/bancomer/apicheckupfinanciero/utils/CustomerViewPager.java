package com.bancomer.apicheckupfinanciero.utils;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by DMEJIA on 07/07/16.
 */
public class CustomerViewPager extends ViewPager {

    public CustomerViewPager(Context     context) {
        super(context);
    }

    public CustomerViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }

    @Override
    public void setPageTransformer(boolean reverseDrawingOrder, PageTransformer transformer) {
        super.setPageTransformer(false, transformer);
    }

    @Override
    protected void setStaticTransformationsEnabled(boolean enabled) {
        super.setStaticTransformationsEnabled(enabled);
    }
}