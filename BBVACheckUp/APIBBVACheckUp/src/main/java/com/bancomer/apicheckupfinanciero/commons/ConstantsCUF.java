package com.bancomer.apicheckupfinanciero.commons;

/**
 * Created by DMEJIA on 09/02/16.
 */
public class ConstantsCUF {

    public static final String SHARED_NAME = "BBVACheckUp";

    public static final int _BORDER_16 = 16;
    public static final int _BORDER_10 = 10;

    public static final int PERCENTAGE_110 = 110;
    public static final int PERCENTAGE_100 = 100;
    public static final int PERCENTAGE_97 = 97;
    public static final int PERCENTAGE_95 = 95;
    public static final int PERCENTAGE_91 = 91;
    public static final int PERCENTAGE_90 = 90;
    public static final int PERCENTAGE_85 = 85;
    public static final int PERCENTAGE_84 = 84;
    public static final int PERCENTAGE_80 = 80;
    public static final int PERCENTAGE_75 = 75;
    public static final int PERCENTAGE_71 = 71;
    public static final int PERCENTAGE_70 = 70;
    public static final int PERCENTAGE_65 = 65;
    public static final int PERCENTAGE_60 = 60;
    public static final int PERCENTAGE_55 = 55;
    public static final int PERCENTAGE_50 = 50;
    public static final int PERCENTAGE_45 = 45;
    public static final int PERCENTAGE_40 = 40;
    public static final int GRADES_360 = 360;

    public static final int _VALUE_XY = 1000;

    // salud crediticia
    public static final double PERCENTAGE_GREAT = 30;
    public static final double PERCENTAGE_GOOD = 50;
    public static final double PERCENTAGE_MALAISE = 70;
    public static final double PERCENTAGE_CRITICAL = 100;

    public static final String MA = "MA";
    public static final String MIA = "MIA";

    public static final String EXCELENTE = "Excelente";
    public static final String SALUDABLE = "Saludable";
    public static final String _MALESTARES = "Malestares";
    public static final String _CRITICO = "Critico";
    public static final String MALESTARES = "Con malestares";
    public static final String CRITICO = "Crítica";
    public static final String ALFA = "Alfa";
    public static final String OMEGA = "Omega";

    public static final int TOTAL_HALOS = 5;

    public static final String EMPTY = "";
    public static final String ZERO = "0";

    /** Claves Creditos **/
    public static final String INCREMENTO_LINEA_CREDITO = "0ILC";
    public static final String TARJETA_CREDITO = "0TDC";
    public static final String CREDITO_HIPOTECARIO = "0HIP";
    //public static final String CREDITO_HIPOTECARIO = "HIPO"; 160108
    public static final String CREDITO_AUTO = "AUTO";
    public static final String PRESTAMO_PERSONAL_INMEDIATO = "0PPI";
    public static final String CREDITO_NOMINA = "0NOM";


    /*
        Tags de consulta estado financiero
     */
    public static final String TAG_GASTO = "gasto";
    public static final String TAG_AHORRO = "ahorro";
    public static final String TAG_DISPONIBLE = "disponible";
    public static final String TAG_DEPOSITO = "deposito";
    public static final String TAG_INVERSION = "inversion";
    public static final String TAG_PAGO_CREDITO = "pagoCredito";
    public static final String TAG_PAGO_CREDITOS = "pagoCreditos";
    //public static final String TAG_LAST_MONTH = "mesInmediatoAnterior";
    public static final String TAG_NUM_HALOS = "cant";

    public static final String TAG_OBJ_PAGO_CREDITOS = "objPagoCreditos";
    public static final String TAG_OBJ_TOTAL_PAYMENTS = "objTotalPaymentsFragment";

    public static final String TAG_MES = "mes";
    public static final String TAG_SALUD = "salud";
    public static final String TAG_TOTAL = "total";
    public static final String TAG_CUENTAS = "cuentas";
    public static final String TAG_CUENTA = "cuenta";
    public static final String TAG_TIPO = "tipo";
    public static final String TAG_SALDO = "saldo";
    public static final String TAG_SALDO_TOTAL = "saldoTotal";
    public static final String TAG_SALUD_CREDITICIA = "saludCrediticia";
    public static final String TAG_SALUD_DEPOSIVOSGASTOS = "saludDepositosGastos";
    public static final String TAG_SALDO_INICIAL = "saldoInicial";
    public static final String TAG_PAGA_CREDITOS = "pagaCreditos";
    public static final String TAG_TIPO_SALUD_CREDITICIA = "tipoSaludCrediticia";

    public static final String TAG_DETALLES = "detalles";
    public static final String TAG_DESCRIPCION = "descripcion";
    public static final String TAG_FECHA = "fecha";
    public static final String TAG_MONTO = "monto";
    public static final String TAG_PRODUCTO = "producto";

    public static final String TAG_TARCREDITO = "tarjetaCredito";
    public static final String TAG_CREDITOAUTO = "creditoAuto";
    public static final String TAG_PRESPERSONAL= "prestamoPersonal";
    public static final String TAG_HIPOTECARIO = "hipotecario";

    public static final String TAG_RESPONSE = "response";
    public static final String TAG_AMOUNT= "amount";
    public static final String TAG_CURRENCY = "currency ";

    public static final String TAG_CREDIT_CARD = "creditCards";
    public static final String TAG_CREDIT_CARD_LIST = "creditCardList";
    public static final String TAG_PRODUCT_BASE = "productBase";
    public static final String TAG_ERROR_INFO = "errorInfo";
    public static final String TAG_CARD_NUMBER = "cardNumber";
    public static final String TAG_MINIMUM_PAYMENT = "minimumPayment";
    public static final String TAG_OUTSTANDING_DEBT = "outstandingDebt";
    public static final String TAG_PAYMENT_WHITH_OUT_INTEREST = "paymentWithoutInterest";
    public static final String TAG_END_DATE = "endDate";
    public static final String TAG_PAYMENT_DATE = "paymentDate";
    public static final String TAG_PREVIOUS_PERIODS ="previousPeriods";


    public static final String GASTO = "Gastos";
    public static final String AHORRO = "Disponible";
    public static final String DEPOSITO = "Deposítos";
    public static final String INVERSION = "Inversiones";
    public static final String PAGO_CREDITO_MES = "Pago Créditos";

    public static final String SHOW_TUTORIAL = "tutorial";
    public static final String SHOW_TIPMA = "TIP_MA";
    public static final String SHOW_TIPSCMIA = "TIP_SC_MIA";
    public static final String HEALTH_CREDIT ="salud crediticia";
    public static final String HEALTH_EXPENSES = "salud gastos";
    public static final String SHOW_TIPMIA = "TIP_MIA";
    public static final String HEALTH = "Health";

    public static final String CONSULTA = "consulta";
    public static final int _VALUE_ZERO = 0;
    public static final int _VALUE_ONE = 1;
    public static final int _VALUE_TWO = 2;
    public static final int _VALUE_THREE = 3;
    public static final int _VALUE_FOUR = 4;
    public static final int _VALUE_FIVE = 5;
    public static final int _VALUE_SIX = 6;

    public static final String JANUARY = "Enero";
    public static final String FEBRUARY = "Febrero";
    public static final String MARCH = "Marzo";
    public static final String APRIL = "Abril";
    public static final String MAY = "Mayo";
    public static final String JUNE = "Junio";
    public static final String JULY = "Julio";
    public static final String AUGUST = "Agosto";
    public static final String SEPTEMBER = "Septiembre";
    public static final String OCTOBER = "Octubre";
    public static final String NOVENBER = "Noviembre";
    public static final String DECEMBER = "Diciembre";

    public static final String TAG_CURRENT_MONTH = "currente month";
    public static final String TAG_LAST_MONTH = "last month";

}
