package com.bancomer.apicheckupfinanciero.models;

/**
 * Created by DMEJIA on 17/02/16.
 */
public class Loands {
    String amount;
    String clve;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getClve() {
        return clve;
    }

    public void setClve(String clve) {
        this.clve = clve;
    }
}
