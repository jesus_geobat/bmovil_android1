package com.bancomer.apicheckupfinanciero.gui.views.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.gui.Delegates.DepositsExpensesDelegate;
import com.bancomer.apicheckupfinanciero.gui.views.activities.MainActivity;
import com.bancomer.apicheckupfinanciero.gui.views.adapters.ExpandableListViewDepositsAdapter;
import com.bancomer.apicheckupfinanciero.models.AccountCUF;
import com.bancomer.apicheckupfinanciero.models.CheckUp;
import com.bancomer.apicheckupfinanciero.utils.CustomExpandableHeightListView;
import com.bancomer.apicheckupfinanciero.utils.CustomerTextView;
import com.bancomer.apicheckupfinanciero.utils.Tools;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;

import javax.crypto.Cipher;

/**
 * Created by DMEJIA on 20/06/16.
 */
public class DepositExpensesFragment extends Fragment implements OnChartValueSelectedListener, View.OnClickListener {

    private DepositsExpensesDelegate delegate;
    private CheckUp checkUpMA;
    private CheckUp checkUpMIA;

    private Bundle bundle;

    private View rootView;
    private View tabIndicatorMA;
    private View tabIndicatorMIA;
    private ScrollView scrollView;

    private CustomerTextView txtLastMonth;
    private CustomerTextView txtCurrentMont;


    private RelativeLayout rl_TabMIA;
    private RelativeLayout rl_TabMA;

    private CustomExpandableHeightListView expandableListView;
    private PieChart graphicPie;
    private Typeface tf;
    private int aux;
    private double totalBarWidth;
    private ExpandableListViewDepositsAdapter adapter;

    private boolean selectedList;
    private boolean clickCenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = this.getArguments();
        checkUpMA = (CheckUp) bundle.getSerializable(ConstantsCUF.TAG_OBJ_TOTAL_PAYMENTS);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView == null) {
            rootView = inflater.inflate(R.layout.layout_deposits, container, false);
            init();
        }
        return rootView;
    }



    /*public void changeSelected(){

        if(Tools.selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH)){
            if(tabIndicatorMA != null && !tabIndicatorMA.isShown()){
                changeSelected(true);
            }
        }else {
            if(tabIndicatorMIA != null && !tabIndicatorMIA.isShown()){
                changeSelected(false);
            }
        }
    }*/

    public void changeSelected(){
        boolean isCurrentMonth = Tools.selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH)? true : false;
        expandableListView.collapseGroup(aux);
        aux = -1;

        tabIndicatorMA.setVisibility(isCurrentMonth == true? View.VISIBLE:View.GONE);
        tabIndicatorMIA.setVisibility(isCurrentMonth == true ? View.GONE : View.VISIBLE);
        rl_TabMIA.setBackgroundColor(isCurrentMonth == true ? Color.TRANSPARENT : getResources().getColor(R.color.colorTabSelected));
        rl_TabMA.setBackgroundColor(isCurrentMonth == true ? getResources().getColor(R.color.colorTabSelected) : Color.TRANSPARENT);
        txtCurrentMont.setTextColor(isCurrentMonth == true ? getResources().getColor(R.color.colorTextTabSelected) : getResources().getColor(R.color.colorTextTabNoSelected));
        txtLastMonth.setTextColor(isCurrentMonth == true ? getResources().getColor(R.color.colorTextTabNoSelected) : getResources().getColor(R.color.colorTextTabSelected));
        CheckUp checkUp = isCurrentMonth == true?checkUpMA:checkUpMIA;
        ArrayList<AccountCUF> arr = checkUp.getArrayAccount();
        setData(arr);
        arr = arr == null? new ArrayList<AccountCUF>():arr;
        updateListView(delegate.plusTotalAccount(checkUp));

        selectedList = false;
        if(arr != null && arr.size() >0) {
            expandableListView.expandGroup(0);
            aux = 0;
        }
    }

    private void updateListView(ArrayList<AccountCUF> accountCUFs){
        adapter.setData(accountCUFs);
        adapter.notifyDataSetChanged();
    }

    private void init(){
        delegate = new DepositsExpensesDelegate(this);
        findViews();
        createGraphic();
        createExpandableListView();
        initialize();
        scaleView();
    }


    private void scaleView(){
        ((MainActivity)getActivity()).getScaleTool().scaleHeight(graphicPie, 700);
    }

    private void initialize(){
        aux = 0;
        clickCenter = false;
        totalBarWidth = delegate.getBarWidth();
        txtCurrentMont.setText("Hoy");//Tools.currentMonth);
        txtLastMonth.setText("Mes anterior");
        /*txtLastMonth.setText(Tools.lastMonth);
        txtCurrentMont.setText(Tools.currentMonth);*/

    }
    private void findViews(){
        graphicPie = (PieChart) rootView.findViewById(R.id.graphicPie);
        expandableListView = (CustomExpandableHeightListView) rootView.findViewById(R.id.expLsAccount);
        txtCurrentMont = (CustomerTextView) rootView.findViewById(R.id.txtCurrentMonthTabDYG);
        txtLastMonth = (CustomerTextView) rootView.findViewById(R.id.txtLastMonthTabDYG);
        scrollView = (ScrollView) rootView.findViewById(R.id.scrollDeposit);

        rl_TabMA = (RelativeLayout) rootView.findViewById(R.id.rlTabMADYG);
        rl_TabMIA = (RelativeLayout) rootView.findViewById(R.id.rlTabMIADYG);

        tabIndicatorMA = rootView.findViewById(R.id.tabIndicatorMADYG);
        tabIndicatorMIA = rootView.findViewById(R.id.tabIndicatorMIADYG);

        rl_TabMIA.setOnClickListener(this);
        rl_TabMA.setOnClickListener(this);
    }



    private void createGraphic(){
        //graphicPie.setBridgeCUF(this);
        graphicPie.setUsePercentValues(true);
        graphicPie.setDescription("");
        graphicPie.setExtraOffsets(25, 5, 5, 5);
        graphicPie.setPieCircle(true);

        tf = Typeface.createFromAsset(getActivity().getAssets(), "sans_semibold.ttf");

        graphicPie.setCenterTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "stag_sans_book.ttf"));
        graphicPie.setDrawCenterText(true);

        graphicPie.setDragDecelerationFrictionCoef(0.95f);

        graphicPie.setDrawHoleEnabled(true);
        graphicPie.setHoleColor(Color.WHITE);

        graphicPie.setTransparentCircleColor(Color.WHITE);
        graphicPie.setTransparentCircleAlpha(110);
        graphicPie.setRotationEnabled(false);
        graphicPie.setHoleRadius(70f);
        graphicPie.setRotationAngle(-90);
        graphicPie.setTransparentCircleRadius(61f);
        graphicPie.setOnChartValueSelectedListener(this);

    }

    public void setData(ArrayList<AccountCUF> arr){

        String amount = Tools.selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? checkUpMA.getBalanceT() : checkUpMIA.getBalanceT();
        graphicPie = delegate.setData(arr, graphicPie, Double.parseDouble(amount));

        graphicPie.setCenterText(delegate.generateCenterSpannableText(Tools.decimalFormat(amount), arr.size()));

        graphicPie.highlightValues(null);

        graphicPie.invalidate();

        Legend l = graphicPie.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        l.setEnabled(false);
    }

    private void createExpandableListView(){
        adapter = new ExpandableListViewDepositsAdapter(this, checkUpMA.getArrayAccount());
        expandableListView.setAdapter(adapter);
        expandableListView.setExpanded(true);

    }


    public ExpandableListView getExpandableListView(){

        return expandableListView;
    }

    public void setSelectedPie(int groupPosition){
        selectedList = true;
        aux = groupPosition;

        graphicPie.setIndex(groupPosition==0?groupPosition:(groupPosition-1));
        graphicPie.drawSelected1();
    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        if(e != null) {
            if(!selectedList) {
                int index = e.getXIndex();
                android.util.Log.i("onValueSelected","aux: " + aux + " index: " +index);

                ((ExpandableListViewDepositsAdapter) expandableListView.getExpandableListAdapter()).setFlag(false);
                int size = adapter.getGroupCount() ;

                if (index < size) {
                    index = size == ConstantsCUF._VALUE_ONE ? index : (index+1);

                    if (!expandableListView.isGroupExpanded(index) ) {

                        if (aux != -1)
                            expandableListView.collapseGroup(aux);
                        expandableListView.expandGroup(index);
                        aux = index;
                    } else {
                        expandableListView.collapseGroup(index);
                        aux = -1;
                    }
                } else {
                    expandableListView.collapseGroup(aux);
                    aux = -1;
                }
                ((ExpandableListViewDepositsAdapter) expandableListView.getExpandableListAdapter()).setFlag(true);
            }
        }
        selectedList = false;
    }

    @Override
    public void onNothingSelected(boolean flag) {
        android.util.Log.i("onValueSelected","aux: " + aux + " flag: " +flag);
        if((flag)) {
                if (expandableListView.isGroupExpanded(0)) {
                    ((ExpandableListViewDepositsAdapter) expandableListView.getExpandableListAdapter()).setFlag(false);
                    expandableListView.collapseGroup(0);
                    ((ExpandableListViewDepositsAdapter) expandableListView.getExpandableListAdapter()).setFlag(true);

                    aux = -1;
                } else {
                    if (aux > 0) {
                        ((ExpandableListViewDepositsAdapter) expandableListView.getExpandableListAdapter()).setFlag(false);
                        expandableListView.collapseGroup(aux);
                        ((ExpandableListViewDepositsAdapter) expandableListView.getExpandableListAdapter()).setFlag(true);
                        graphicPie.setIndex(-1);
                        graphicPie.drawSelected1();
                    }else if(adapter.getGroupCount()==ConstantsCUF._VALUE_ONE){
                        graphicPie.setIndex(0);
                        graphicPie.drawSelected1();
                    }
                    ((ExpandableListViewDepositsAdapter) expandableListView.getExpandableListAdapter()).setFlag(false);

                    expandableListView.expandGroup(0);
                    ((ExpandableListViewDepositsAdapter) expandableListView.getExpandableListAdapter()).setFlag(true);
                    aux = 0;
                }
        }else if(aux > -1 ){

            if(expandableListView.isGroupExpanded(0) && adapter.getGroupCount()>ConstantsCUF._VALUE_ONE){
               aux = 0;
            }else {
                expandableListView.collapseGroup(aux);
                aux = -1;
            }

        }else if(aux <-1 && !flag){

        }
        selectedList = false;
    }

    public void fullScroll(){
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v == rl_TabMA) {
            Tools.selectedMonth = ConstantsCUF.TAG_CURRENT_MONTH;
            changeSelected();
        }else if(v == rl_TabMIA){
            if(!Tools.isDataMIA)
                ((TotalPaymentsFragment)(((MainActivity)getActivity()).getAdapter().getItem(0))).checkUPMIA();
            else {
                Tools.selectedMonth = ConstantsCUF.TAG_LAST_MONTH;
                changeSelected();
            }
        }

    }

    public void setDataCheckUPMIA(CheckUp checkUpMIA){
        Tools.selectedMonth = ConstantsCUF.TAG_LAST_MONTH;
        this.checkUpMIA = checkUpMIA;
        changeSelected();
    }
}
