package com.bancomer.apicheckupfinanciero.models;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.io.ParserJSONImpl;
import com.bancomer.apicheckupfinanciero.utils.Tools;

import org.json.JSONObject;

import java.io.IOException;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by DMEJIA on 18/02/16.
 */
public class CltaEdoFinanciero implements ParsingHandler{

    private Monto oAhorro;
    private Monto oPagoCredito;
    private Monto oGasto;
    private Monto oDeposito;
    private String lastMonth;
    private String currentMonth;

    public CltaEdoFinanciero(){

        oAhorro = new Monto();
        oPagoCredito = new Monto();
        oGasto = new Monto();
        oDeposito = new Monto();
        lastMonth = "";
        currentMonth = "";
    }

    public String getDeposito() {
        return oDeposito.getAmount();
    }


    public String getPagoCreditos() {
        return oPagoCredito.getAmount();
    }


    public String getGasto() {
        return oGasto.getAmount();
    }


    public String getAhorro() {
        return oAhorro.getAmount();
    }

    public String getLastMonth(){
        return lastMonth;
    }

    public String getCurrentMonth(){
        return currentMonth;
    }


    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parserJSON) throws IOException, ParsingException {


        if(parserJSON.hasValue(ConstantsCUF.TAG_RESPONSE)){
            JSONObject object = parserJSON.parserNextObject(ConstantsCUF.TAG_RESPONSE,false);
            parserJSON = new ParserJSONImpl(object.toString());
            if(parserJSON.hasValue(ConstantsCUF.TAG_DEPOSITO)){
                JSONObject deposito = parserJSON.parserNextObject(ConstantsCUF.TAG_DEPOSITO, false);
                ParserJSON parse = new ParserJSONImpl(deposito.toString());
                if(parse.hasValue(ConstantsCUF.TAG_MONTO)) {
                    android.util.Log.e("dora", "si lo tiene a parsear");
                    JSONObject monto = parse.parserNextObject(ConstantsCUF.TAG_MONTO, false);
                    android.util.Log.e("dora","monto: "+ monto.toString());
                    ParserJSON parseMonto = new ParserJSONImpl(monto.toString());
                    if (parseMonto.hasValue(ConstantsCUF.TAG_AMOUNT))
                        oDeposito.setAmount((parseMonto.parseNextValue(ConstantsCUF.TAG_AMOUNT, false)));
                }
            }

            if(parserJSON.hasValue(ConstantsCUF.TAG_GASTO)){
                JSONObject deposito = parserJSON.parserNextObject(ConstantsCUF.TAG_GASTO, false);
                ParserJSON parse = new ParserJSONImpl(deposito.toString());
                if(parse.hasValue(ConstantsCUF.TAG_MONTO)) {
                    JSONObject monto = parse.parserNextObject(ConstantsCUF.TAG_MONTO, false);
                    ParserJSON parseMonto = new ParserJSONImpl(monto.toString());
                    if (parseMonto.hasValue(ConstantsCUF.TAG_AMOUNT))
                        oGasto.setAmount((parseMonto.parseNextValue(ConstantsCUF.TAG_AMOUNT, false)));
                }
            }

            if(parserJSON.hasValue(ConstantsCUF.TAG_PAGO_CREDITO)){
                JSONObject deposito = parserJSON.parserNextObject(ConstantsCUF.TAG_PAGO_CREDITO, false);
                ParserJSON parse = new ParserJSONImpl(deposito.toString());
                if(parse.hasValue(ConstantsCUF.TAG_MONTO)) {
                    JSONObject monto = parse.parserNextObject(ConstantsCUF.TAG_MONTO, false);
                    ParserJSON parseMonto = new ParserJSONImpl(monto.toString());
                    if (parseMonto.hasValue(ConstantsCUF.TAG_AMOUNT))
                        oPagoCredito.setAmount((parseMonto.parseNextValue(ConstantsCUF.TAG_AMOUNT, false)));
                }
            }

            if(parserJSON.hasValue(ConstantsCUF.TAG_AHORRO)){
                JSONObject deposito = parserJSON.parserNextObject(ConstantsCUF.TAG_AHORRO, false);
                ParserJSON parse = new ParserJSONImpl(deposito.toString());
                if(parse.hasValue(ConstantsCUF.TAG_MONTO)) {
                    JSONObject monto = parse.parserNextObject(ConstantsCUF.TAG_MONTO, false);
                    ParserJSON parseMonto = new ParserJSONImpl(monto.toString());
                    if (parseMonto.hasValue(ConstantsCUF.TAG_AMOUNT))
                        oAhorro.setAmount((parseMonto.parseNextValue(ConstantsCUF.TAG_AMOUNT, false)));
                }
            }


            /*if(parserJSON.hasValue(ConstantsCUF.TAG_LAST_MONTH)){
                lastMonth = parserJSON.parseNextValue(ConstantsCUF.TAG_LAST_MONTH);
                currentMonth = Tools.getCurrentMont(lastMonth);
            }*/
        }

    }
}
