package com.bancomer.apicheckupfinanciero.controller;

import android.app.Activity;
import android.content.Context;

import com.bancomer.apicheckupfinanciero.io.BaseSubapplication;

/**
 * Created by DMEJIA on 21/02/16.
 */
public class BaseSubApplicationImp extends BaseSubapplication {

    public BaseSubApplicationImp(Context suiteApp) {
        super(suiteApp);
    }
}
