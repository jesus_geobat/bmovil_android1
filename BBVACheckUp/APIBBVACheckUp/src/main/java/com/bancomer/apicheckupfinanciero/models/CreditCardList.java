package com.bancomer.apicheckupfinanciero.models;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.io.ParserJSONImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by DMEJIA on 17/06/16.
 */
public class CreditCardList implements Serializable{
    private ProductBase productBase;
    private String cardNumber;
    private String amountMinimumPayment;
    private String amountOutstandingDebt;
    private String amountPaymentWithoutInterest;
    private ArrayList<PreviousPeriods> previousPeriodsArrayList;
    private int numDays;

    public CreditCardList(){
        productBase = new ProductBase();
        cardNumber = ConstantsCUF.EMPTY;
        amountMinimumPayment = ConstantsCUF.EMPTY;
        amountOutstandingDebt = ConstantsCUF.EMPTY;
        amountPaymentWithoutInterest = ConstantsCUF.EMPTY;
        previousPeriodsArrayList = new ArrayList<PreviousPeriods>();
        numDays = ConstantsCUF._VALUE_ZERO;
    }

    public ProductBase getProductBase() {
        return productBase;
    }

    public void setProductBase(ProductBase productBase) {
        this.productBase = productBase;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAmountMinimumPayment() {
        return amountMinimumPayment;
    }

    public void setAmountMinimumPayment(String amountMinimumPayment) {
        this.amountMinimumPayment = amountMinimumPayment;
    }

    public String getAmountOutstandingDebt() {
        return amountOutstandingDebt;
    }

    public void setAmountOutstandingDebt(String amountOutstandingDebt) {
        this.amountOutstandingDebt = amountOutstandingDebt;
    }

    public String getAmountPaymentWithoutInterest() {
        return amountPaymentWithoutInterest;
    }

    public void setAmountPaymentWithoutInterest(String amountPaymentWithoutInterest) {
        this.amountPaymentWithoutInterest = amountPaymentWithoutInterest;
    }

    public ArrayList<PreviousPeriods> getPreviousPeriodsArrayList() {
        return previousPeriodsArrayList;
    }

    public void setPreviousPeriodsArrayList(ArrayList<PreviousPeriods> previousPeriodsArrayList) {
        this.previousPeriodsArrayList = previousPeriodsArrayList;
    }

    public int getNumDays() {
        return numDays;
    }

    public void setNumDays(int numDays) {
        this.numDays = numDays;
    }
}
