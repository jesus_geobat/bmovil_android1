package com.bancomer.apicheckupfinanciero.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by DMEJIA on 10/05/16.
 */
public class CustomerTextView extends TextView{
    public CustomerTextView(Context context) {
        this(context, null);
    }

    public CustomerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), "stag_sans_book.ttf"));
    }
}
