package com.bancomer.apicheckupfinanciero.controller;

/**
 * Created by DMEJIA on 17/02/16.
 */
public class ControllerViewCredit{

    public static boolean habilitarVista = false;
    public static boolean portabilidad = false;

    public void exitCheckUp(){}

    public void exitSaludFinanciera(){}

    public void showView(int i){}

    public void habilitarVista(){
       habilitarVista = true;
    }

    public boolean isConsulta(){
        return true;
    }

    public void setConsulta(boolean value){
    }

}
