package com.bancomer.apicheckupfinanciero.models;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;

/**
 * Created by DMEJIA on 14/03/16.
 */
public class Monto {

    private Currency currency;
    private String amount;

    public Monto(){
        amount = ConstantsCUF.ZERO;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
