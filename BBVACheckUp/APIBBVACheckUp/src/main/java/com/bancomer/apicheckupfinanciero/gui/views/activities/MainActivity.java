package com.bancomer.apicheckupfinanciero.gui.views.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.controller.InitCheckUp;
import com.bancomer.apicheckupfinanciero.gui.Delegates.MainDelegate;
import com.bancomer.apicheckupfinanciero.gui.views.adapters.TotalPaymentsAdapter;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.CreditPaymentsFragment;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.DepositExpensesFragment;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.TotalPaymentsFragment;
import com.bancomer.apicheckupfinanciero.models.CheckUp;
import com.bancomer.apicheckupfinanciero.utils.Blur.Blurry;
import com.bancomer.apicheckupfinanciero.utils.CustomViewPager;
import com.bancomer.apicheckupfinanciero.utils.ScaleTool;
import com.bancomer.apicheckupfinanciero.utils.Tools;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.timer.TimerController;


/**
 * Created by DMEJIA on 10/02/16.
 */
public class MainActivity extends BaseActivity implements View.OnClickListener{

    private CirclePageIndicator circlePageIndicator;
    private CustomViewPager viewPager;
    private RelativeLayout rlMain;
    private RelativeLayout rlMessage;
    private RelativeLayout rlFoco;
    private TotalPaymentsAdapter adapter;

    private LinearLayout lnHeader;
    private LinearLayout lnHeart;
    private LinearLayout lnWelcomTutorial;

    private ImageView imgLogo;
    private ImageView imgFoco;

    private TextView txtNumTip;

    private MainDelegate delegate;
    private ScaleTool scaleTool;

    private int heigthHeader;
    private int heigthMain;
    private float heigthCirclePager;

    private boolean show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_main_activity);
        InitCheckUp.getInstance().setContext(this);
        InitCheckUp.getInstance().setCurrentActivity(this);
        setActivityChanging(false);

        init();
    }

    private void init(){
        findViewById();
        loadDataHalos();
        scaleView();
        show = true;
    }

    private void loadDataHalos(){
        delegate = (MainDelegate) InitCheckUp.getInstance().getParentManager().getBaseDelegateForKey(MainDelegate.MAIN_CONTROLLER);
        delegate.setMainActivity(this);
        delegate.loadData();
    }


    private void findViewById(){
        circlePageIndicator = (CirclePageIndicator) findViewById(R.id.circlePagerIndicatorCUF1);
        circlePageIndicator.setFillColor(getResources().getColor(R.color.colorFill));
        circlePageIndicator.setPageColor(getResources().getColor(R.color.colorPage));

        lnHeader = (LinearLayout) findViewById(R.id.lnheader);
        lnHeart = (LinearLayout) findViewById(R.id.lnHeart);
        lnWelcomTutorial = (LinearLayout) findViewById(R.id.welcomeTutorial);

        rlMessage = (RelativeLayout) findViewById(R.id.rlMessage);
        rlMain = (RelativeLayout)findViewById(R.id.rlMain);
        rlFoco = (RelativeLayout)findViewById(R.id.rlFoco);

        txtNumTip = (TextView) findViewById(R.id.txtNumTip);

        viewPager = (CustomViewPager) findViewById(R.id.viewpager);

        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        imgFoco = (ImageView) findViewById(R.id.imgFoco);
        imgFoco.setOnClickListener(this);

    }

    public void createViewPager(CheckUp checkUpMA){//(String deposito, String gasto, String ahorro, String pagoCreditos, String lastMonth){
        final TotalPaymentsAdapter adapter = new TotalPaymentsAdapter(getSupportFragmentManager(), checkUpMA);//(getSupportFragmentManager(), deposito, gasto, ahorro, pagoCreditos, lastMonth,delegate);
        this.adapter = adapter;
        viewPager.setAdapter(adapter);
        circlePageIndicator.setRadius(Tools.convertDpToPixel(this, getResources().getInteger(R.integer.radious_indicator)));
        circlePageIndicator.setViewPager(viewPager);
        circlePageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setLogo(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v == imgFoco){
            ((TotalPaymentsFragment)adapter.getItem(0)).onOfTip();
        }else {
            setActivityChanging(true);
            InitCheckUp.getInstance().getObj().showView(v.getId());
        }
    }

    @Override
    public void onBackPressed() {
        /*if(lnWelcomTutorial.isShown()){
            Blurry.delete(rlMain);
            lnWelcomTutorial.setVisibility(View.GONE);
            lnHeart.setVisibility(View.GONE);
            rlMessage.setVisibility(View.GONE);
            //((TotalPaymentsFragment)adapter.getItem(0)).centerGraphic();
            ((TotalPaymentsFragment)adapter.getItem(0)).showTipFromTutorial(-100);
        }else if(!Blurry.isShowTutorial(rlMain)){*/
            InitCheckUp.getInstance().getObj().exitCheckUp();
            InitCheckUp.resetInstace();
            super.onBackPressed();
        //}
    }

    int viewH;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        heigthHeader = lnHeader.getHeight();
        viewH = viewPager.getHeight();
        heigthMain = rlMain.getHeight();
        heigthCirclePager = circlePageIndicator.getMeasuredHeight();
        ((TotalPaymentsFragment)adapter.getItem(0)).calculateSpace(heigthMain - heigthHeader);
        initTutorial();
    }



    private void initTutorial(){
        /*Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {*/
                if(show) {
                    if(InitCheckUp.getInstance().getPreferences().getBooleanData(ConstantsCUF.SHOW_TUTORIAL))
                        ((TotalPaymentsFragment)adapter.getItem(0)).startTutorial();
                    show = false;
                }
            /*}
        }, 200);*/
    }

    public ScaleTool getScaleTool(){
        return scaleTool;
    }

    private void scaleView(){
        scaleTool = new ScaleTool(this);
        GuiTools  guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(txtNumTip,true);

        locateNumTip();
        Tools.resize(this);
    }

    private void locateNumTip(){
        int h = imgFoco.getDrawable().getIntrinsicHeight();
        int w = imgFoco.getDrawable().getIntrinsicWidth() / 2;
        h /= 3;

        RelativeLayout.LayoutParams rl = (RelativeLayout.LayoutParams) txtNumTip.getLayoutParams();
        rl.topMargin = h * 2;
        rl.leftMargin = w;
        txtNumTip.setLayoutParams(rl);
    }
    public void setLogo(int page){
        switch (page){
            case 0:
                //if(InitCheckUp.getInstance().getObj().isConsulta())
                imgLogo.setImageResource(R.drawable.bbva_checkup);
                imgFoco.setVisibility(View.VISIBLE);
                ((TotalPaymentsFragment)adapter.getItem(page)).changeSelected();
                //else
                //    imgLogo.setImageResource(R.drawable.logo_prestamos_disponibles_cuf);
                break;
            case 1:
                imgFoco.setVisibility(View.GONE);
                imgLogo.setImageResource(R.drawable.gastos_y_depositos);
                ((DepositExpensesFragment)adapter.getItem(page)).changeSelected();
                changeNumTip(View.GONE,ConstantsCUF.EMPTY);
                break;
            /*case 2:
                imgLogo.setImageResource(R.drawable.reprticion_de_gastos);
                break;*/
            case 2:
                imgLogo.setImageResource(R.drawable.pago_de_creditos);
                if(delegate.getObjPagoCreditos() == null)
                    ((CreditPaymentsFragment)adapter.getItem(page)).getData();
                else if(delegate.getObjPagoCreditos().getCreditCardLists().size() > ConstantsCUF._VALUE_ZERO)
                    ((CreditPaymentsFragment)adapter.getItem(page)).inflateList(delegate);
                break;
            case 4:
                //imgLogo.setImageResource(R.drawable.logo_cuf);
                break;
        }
    }

    public void configureMessageWelcomeFromTutorial(){
        TextView txtWelcome  = (TextView)lnWelcomTutorial.findViewById(R.id.txtWelcomeTuto);
        txtWelcome.setText(Html.fromHtml(getResources().getString(R.string.welcome_tutorial)), TextView.BufferType.SPANNABLE);
        PieChart pieCharts [] = new PieChart[]{(PieChart)lnWelcomTutorial.findViewById(R.id.pieChart1), (PieChart)lnWelcomTutorial.findViewById(R.id.pieChart2),(PieChart)lnWelcomTutorial.findViewById(R.id.pieChart3),(PieChart)lnWelcomTutorial.findViewById(R.id.pieChart4)};
        ImageView img = (ImageView)lnWelcomTutorial.findViewById(R.id.imgHeart);
        int radious = ConstantsCUF.PERCENTAGE_90;
        int color = 0;
        int color2 = getResources().getColor(R.color.colorDepositos);
        String title = ConstantsCUF.EMPTY;
        int value = 1;
        int angle = 0;
        float heigthHalos = ((TotalPaymentsFragment)adapter.getItem(0)).getHeigthHalos();
        for (int i = 0; i < pieCharts.length; i++){
            ArrayList<Integer> colors = new ArrayList<Integer>();
            ArrayList<Entry> valsY = new ArrayList<Entry>();

            switch (i){
                case 0:
                    color = getResources().getColor(R.color.colorSaldo);
                    title = "Saldo Inicial &";
                    angle = 270;
                    break;
                case 1:
                    color = getResources().getColor(R.color.colorGastos);
                    title = ConstantsCUF.GASTO;
                    angle = 150;
                    break;
                case 2:
                    color = getResources().getColor(R.color.colorCreditos);
                    title = ConstantsCUF.PAGO_CREDITO_MES;
                    angle = 100;
                    break;
                case 3:
                    color = getResources().getColor(R.color.statusExcelente);
                    title = ConstantsCUF.AHORRO;
                    angle = 75;
                    break;
            }
            pieCharts[i].setRotationAngle(270);
            pieCharts[i].setDescriptionColor(Color.rgb(128, 128, 128));
            pieCharts[i].setHoleRadius(radious);
            pieCharts[i].setRadius(radious);
            pieCharts[i].setRotationEnabled(false);
            pieCharts[i].setBorderHeigth((int) (Tools.get_border() - 2));
            pieCharts[i].setNoDataTextDescription(title);
            pieCharts[i].setxAumount(Tools.getxAmount());
            pieCharts[i].setxConcep(Tools.getxConcep());
            pieCharts[i].setTextSize(Tools.getSizeText());
            PieDataSet set1 = new PieDataSet(valsY, ConstantsCUF.EMPTY);

            if(i == 0){
                colors.add(color2);
                colors.add(color);
                set1.setColors(colors);
                valsY.add(new Entry(75, ConstantsCUF._VALUE_ZERO));
                valsY.add(new Entry(25, ConstantsCUF._VALUE_ONE));

            }else {
                set1.setColor(color);
                valsY.add(new Entry(value, ConstantsCUF._VALUE_ZERO));
            }


            PieData data = new PieData();
            data.addDataSet(set1);
            pieCharts[i].setMaxAngle(angle);
            pieCharts[i].setData(data);
            pieCharts[i].highlightValues(null);

            pieCharts[i].invalidate();

            if(i == pieCharts.length-1) {
                int min = Math.min((int)heigthHalos, getResources().getDisplayMetrics().widthPixels);
                float space = (min / 2) * radious / 100;
                space -= 4;
                float spaceTotal = (space-(Tools.convertDpToPixel(this, Tools.get_border())))  * 2;
                if (img.getDrawable().getIntrinsicWidth() > spaceTotal) {
                    img.getLayoutParams().height = (int) spaceTotal;
                    img.getLayoutParams().width = (int) spaceTotal;
                    img.requestLayout();

                }else if (getResources().getDisplayMetrics().widthPixels == 240){
                    img.getLayoutParams().height = (int) 30;
                    img.getLayoutParams().width = (int) 40;
                    img.requestLayout();

                }
            }
            radious -=12;
        }

    }

    @Override
    public void onUserInteraction() {
        TimerController.getInstance().resetTimer();
        super.onUserInteraction();
    }

    public RelativeLayout getImgFoco(){
        return rlFoco;
    }

    public RelativeLayout getRlMain(){
        return rlMain;
    }

    public CirclePageIndicator getCirclePageIndicator(){
        return circlePageIndicator;
    }

    public int getHeigthHeader(){
        return heigthHeader;
    }

    public RelativeLayout getRlMessage(){
        return rlMessage;
    }

    public LinearLayout getLnHeart(){
        return lnHeart;
    }

    public MainDelegate getDelegate(){
        return delegate;
    }

    public LinearLayout getLnHeader(){
        return lnHeader;
    }

    public void setCurrentItem(int position){
        viewPager.setCurrentItem(position);
    }

    public float getHeigthCirclePager() {
        return heigthCirclePager;
    }

    public TotalPaymentsAdapter getAdapter() {
        return adapter;
    }

    public LinearLayout getLnWelcomTutorial(){
        return lnWelcomTutorial;
    }

    public void changeNumTip(int visible, String num){
        txtNumTip.setText(num);
        txtNumTip.setVisibility(visible);
    }

    public String getNumberTip(){
        return  txtNumTip.getText().toString().trim();
    }
}
