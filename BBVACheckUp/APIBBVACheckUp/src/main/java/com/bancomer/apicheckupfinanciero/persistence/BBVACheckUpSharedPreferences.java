package com.bancomer.apicheckupfinanciero.persistence;

import android.content.Context;
import android.content.SharedPreferences;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.controller.InitCheckUp;

/**
 * Created by DMEJIA on 24/08/16.
 */
public class BBVACheckUpSharedPreferences {

    private SharedPreferences sharedPreferences = null;

    private static BBVACheckUpSharedPreferences theInstance = null;

    private BBVACheckUpSharedPreferences(){
        sharedPreferences = InitCheckUp.appContext.getSharedPreferences(ConstantsCUF.SHARED_NAME, Context.MODE_PRIVATE);
    }

    private synchronized static void createInstance(){
        if(theInstance == null)
            theInstance = new BBVACheckUpSharedPreferences();
    }

    public static BBVACheckUpSharedPreferences getTheInstance(){
        if(theInstance == null)
            createInstance();
        return theInstance;
    }

    public boolean contains(String value){
        boolean contain = false;
        try {
            contain = sharedPreferences.contains(value);
        } catch (Exception e) {
        }
        return contain;
    }

    public void setBooleanData(String value, Boolean data){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(value, data);
        editor.commit();
    }

    public boolean getBooleanData(String value){
        boolean resp = false;
        try {
            resp = sharedPreferences.getBoolean(value, false);
        } catch (Exception e) {}

        return resp;
    }

    public void setStringData(String value, String data){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(value, data);
        editor.commit();
    }

    public String getStringData(String value){
        String resp = ConstantsCUF.EMPTY;
        try {
            resp = sharedPreferences.getString(value, ConstantsCUF.EMPTY);
        } catch (Exception e) {
        }

        return resp;
    }
}
