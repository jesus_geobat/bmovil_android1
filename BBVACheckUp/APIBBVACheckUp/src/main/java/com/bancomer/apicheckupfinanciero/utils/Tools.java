package com.bancomer.apicheckupfinanciero.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.controller.ControllerViewCredit;
import com.bancomer.apicheckupfinanciero.io.Server;
import com.bancomer.apicheckupfinanciero.models.CheckUpValues;
import com.github.mikephil.charting.utils.Utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import bancomer.api.common.commons.Constants;

/**
 * Created by DMEJIA on 15/02/16.
 */
public class Tools {


    private static float sizeText;
    private static float xAmount;
    private static float xConcep;
    private static float _border;
    private static boolean isSlow;
    private static int radious;

    public static String lastMonth = "";
    public static String currentMonth = "";
    public static String selectedMonth = "";

    public static boolean isDataMIA = false;
    public static float heigthText = 0;
    public static boolean hasTDC = false;
    public static boolean hasTD = false;
    public static int numTips = 1;

    /**
     * A progress dialog for long waiting processes.
     */
    private static ProgressDialog mProgressDialog;

    public static String addDecimals(int value){
        return "$" + formatPositiveIntegerAmount(String.valueOf(value));
    }

    public static String addDecimals(double value){
        String amount = String.valueOf(value);
        try {
            if (amount.contains("E")) {
                String aux = amount.substring(amount.indexOf(".") + 1, amount.indexOf("E"));
                int potencia = Integer.parseInt(amount.substring(amount.indexOf("E") + 1));
                amount = amount.replace(".", ConstantsCUF.EMPTY);
                amount = amount.substring(ConstantsCUF._VALUE_ZERO, amount.indexOf("E"));
                for (int i = aux.length(); i < potencia; i++) {
                    amount += ConstantsCUF.ZERO;
                }
            }else{
                if(amount.contains("."))
                    amount = amount.substring(ConstantsCUF._VALUE_ZERO, amount.indexOf("."));

                if(value < ConstantsCUF._VALUE_ZERO) {
                    return formatAmount(amount, value < 0 ? true : false);
                }

            }
        }catch (Exception e){}
        return "$" + formatPositiveIntegerAmount(amount);
    }

    public static String decimalFormat(String value){
        double d = Double.parseDouble(value);
        DecimalFormat df = new DecimalFormat("#0.00");
        value = df.format(d).replace(",", ".");
        return formatAmount(value, d < 0 ? true : false);
    }

    private static String formatPositiveIntegerAmount(String amount) {

        if(!amount.contains("E")) {

            StringBuffer result = new StringBuffer();
            if (amount != null) {
                int size = amount.length();
                int remaining = size % 3;
                if (remaining == 0) {
                    remaining = 3;
                }
                int start = 0;
                for (int end = remaining; end <= size; end += 3) {
                    result.append(amount.substring(start, end));
                    if (end < size) {
                        result.append(',');
                    }
                    start = end;
                }
            }
            return result.toString();
        }else
            return amount;
    }

    public static String hideAccountNumber(String account) {
        String result = "";
        if(account != null && account.length() > 5) {
            StringBuffer sb = new StringBuffer("*");
            sb.append(account.substring(account.length() - 5));
            result = sb.toString();
        } else if(account.length() == 5) {
            result = account;
        }

        return result;
    }

    public static String getFormatAccount(String account, String type){
        String typeName = getNombreTipoCuenta(type);
        typeName = typeName.equalsIgnoreCase(type)?type:(typeName + " " + hideAccountNumber(account));
        return typeName;
    }

    public static String getNombreTipoCuenta(String type) {
        String typeName = "";
        if("CH".equals(type)) {
            typeName = "Cuenta";
        } else if("TC".equals(type)) {
            typeName = "T. de crédito";
        } else if("LI".equals(type)) {
            typeName = "Libretón";
        } else if("AH".equals(type)) {
            typeName = "Cuenta";
        } else if("CE".equals(type)) {
            typeName = "Cuenta Express";
        } else if("CL".equals(type)) {
            typeName = "Cuenta Clabe";
        } else if("TP".equals(type)) {
            typeName = "T. Prepago";
        } else if("PT".equals(type)) {
            typeName = "Contrato";
        }else
            typeName = type;

        return typeName;
    }

    public static String formatAmount(String amount, boolean negative){
        StringBuffer result = new StringBuffer();
        if(amount != null) {
            result.append('$');
            if (negative) {
                if (amount.startsWith("-")) {
                    amount = amount.substring(1);
                }

                result.append('-');
            }
            String cadena[] = amount.split("\\.");

            String intPart = cadena[0];

            result.append(formatPositiveIntegerAmount(intPart));

            if (cadena.length == ConstantsCUF._VALUE_TWO) {
                String decPart = cadena[1];
                result.append('.');
                result.append(decPart);
            }
        }
        return result.toString();
    }
    public static int getCreditIcon(String code){

        if(code.equals(ConstantsCUF.INCREMENTO_LINEA_CREDITO)){
            return R.drawable.detalleagregartara_cuf;
        }else if(code.equals(ConstantsCUF.TARJETA_CREDITO)){
            return R.drawable.detalletdca_cuf;
        }else if(code.equals(ConstantsCUF.CREDITO_HIPOTECARIO)){
            return R.drawable.detallehipotecarioa_cuf;
        }else if(code.equals(ConstantsCUF.CREDITO_AUTO)){
            return R.drawable.detalleautoa_cuf;
        }else if(code.equals(ConstantsCUF.PRESTAMO_PERSONAL_INMEDIATO)){
            return  R.drawable.detallepersonala_cuf;
        }else if(code.equals(ConstantsCUF.CREDITO_NOMINA)){
            return R.drawable.detallenominaa_cuf;
        }
        return 0;
    }

    public static boolean isEmptyOrNull(String text){
        return text==null || text.trim().length() == 0;
    }

    public static void calculateSizeMaxTex(Context context){
        String text = "_PAGO CRÉDITOS_";
        Paint paint = new Paint();
        paint.setTypeface(Typeface.createFromAsset(context.getAssets(), "sans_semibold.ttf"));
        paint.setTextSize(convertDpToPixel(context, sizeText));

        for (int i = (int)sizeText; i >= 9; i--){
            if(paint.measureText(text) >= xAmount) {
                sizeText--;
                paint.setTextSize(convertDpToPixel(context, sizeText));
            }
        }

    }

    public static void resize(Context context) {
        String text1 = "__$9,900,000";
        String text2 = "_PAGO CRÉDITOS_";

        Resources res = context.getResources();
        DisplayMetrics mMetrics = res.getDisplayMetrics();

        if(mMetrics == null){
            sizeText = context.getResources().getInteger(R.integer._text_size_small);
            xAmount = context.getResources().getInteger(R.integer._positionAmount);
            xConcep = context.getResources().getInteger(R.integer._positionConcep);
            return;
        }else {

            //if(mMetrics.widthPixels >= 1440)
                radious = 95;
           // else
           //     radious = 100;
            if (mMetrics.scaledDensity > 3.1)
                _border = 18;
            else
                _border = 16;

            sizeText = 12;

                Paint paint = new Paint();
                paint.setTypeface(Typeface.createFromAsset(context.getAssets(), "sans_semibold.ttf"));
                paint.setTextSize(convertDpToPixel(context, sizeText));
                Rect bounds = new Rect();
                Rect bounds2 = new Rect();
                paint.getTextBounds(text1, ConstantsCUF._VALUE_ZERO, text1.length(), bounds);
                paint.getTextBounds(text2, ConstantsCUF._VALUE_ZERO, text2.length(), bounds2);

                int sum = bounds.width() + bounds2.width() ;
                heigthText = bounds.height();
                float rest = (mMetrics.widthPixels / 2) - sum;
            if(Server.ALLOW_LOG){
                android.util.Log.v("Tools", "Tamaño texto");
            }

                if (rest >= ConstantsCUF._VALUE_ZERO) {
                    if (mMetrics.scaledDensity > 3) {
                        xAmount = bounds.width() + 15;
                        xConcep = xAmount  ;
                    } else {
                        xAmount = bounds.width();
                        xConcep = xAmount;
                    }
                } else {
                    sizeText = context.getResources().getInteger(R.integer._text_size_small);
                    xAmount = Tools.convertDpToPixel(context, context.getResources().getInteger(R.integer._positionAmount));
                    xConcep = Tools.convertDpToPixel(context, context.getResources().getInteger(R.integer._positionConcep));
                }

                if (mMetrics.heightPixels <= 500) {
                    radious = 110;
                    isSlow = true;
                    //TODO se cambia el valor debido a que no hay prestamos_border = 10;
                    _border = 14;
                }else if(mMetrics.heightPixels <= 320){
                    radious = 90;
                    isSlow = true;
                    _border = 10;
                }



        }
    }

    public static void resizeText (String amount, Context context){
        Paint paint = new Paint();
        paint.setTypeface(Typeface.createFromAsset(context.getAssets(), "stag_sans_book.ttf"));
        paint.setTextSize(convertDpToPixel(context, sizeText));
        xAmount = (context.getResources().getDisplayMetrics().widthPixels/2) - (_border*.67f);
        float txtwidth = (Utils.convertDpToPixel(12) + paint.measureText("Saldo Inicial & " + amount));
        for (int i = (int)sizeText; i >= 9; i--){
            if(txtwidth > xAmount) {
                sizeText--;
                paint.setTextSize(convertDpToPixel(context, sizeText));
                txtwidth = (Utils.convertDpToPixel(12) + paint.measureText("Saldo Inicial & " + amount));
            }else {
                xAmount = (context.getResources().getDisplayMetrics().widthPixels/2) - txtwidth - (_border*.67f);
                return;
            }
        }
    }

    public static String maxString(ArrayList<String> amount, Context context){
        String max = "";
        Paint paint = new Paint();
        paint.setTypeface(Typeface.createFromAsset(context.getAssets(), "stag_sans_book.ttf"));
        paint.setTextSize(convertDpToPixel(context, sizeText));

        for(String text : amount){
            if(paint.measureText(text) > paint.measureText(max))
                max = text;
        }

        return max;
    }

    public static float convertDpToPixel(Context context, float dp) {
        Resources res = context.getResources();
        DisplayMetrics mMetrics = res.getDisplayMetrics();

        if (mMetrics == null) {

            return dp;
        }

        DisplayMetrics metrics = mMetrics;
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static float getSizeText() {
        return sizeText;
    }

    public static float getxAmount() {
        return xAmount;
    }

    public static float getxConcep() {
        return xConcep;
    }

    public static boolean isLow() {
        return isSlow;
    }

    public static float get_border() {
        return _border;
    }

    public static int getRadious() {
        return radious;
    }

    public static int getIntColor(String status, Context context){
        int color = 0;
        if(Server.ALLOW_LOG)
            android.util.Log.v("Tools", "status: "+status);
        if(ConstantsCUF.EXCELENTE.equalsIgnoreCase(status))
            color = context.getResources().getColor(R.color.statusExcelente);
        else if(ConstantsCUF.SALUDABLE.equalsIgnoreCase(status))
            color = context.getResources().getColor(R.color.statusSaludable);
        else if(ConstantsCUF._MALESTARES.equalsIgnoreCase(status))
            color = context.getResources().getColor(R.color.statusMalestares);
        else if(ConstantsCUF._CRITICO.equalsIgnoreCase(status))
            color = context.getResources().getColor(R.color.statusCritico);

        return color;
    }

    public static void muestraIndicadorActividad(String strTitle, String strMessage, Context context) {
        if(mProgressDialog != null){
            ocultaIndicadorActividad();
        }
        mProgressDialog = ProgressDialog.show(context, strTitle,
                strMessage, true);
        mProgressDialog.setCancelable(false);
    }

    /**
     * Hides the progress dialog.
     */
    public static void ocultaIndicadorActividad() {
        if(mProgressDialog != null){
            mProgressDialog.dismiss();
        }
    }

    public static void showErrorMessage(String errorMessage, Context context, DialogInterface.OnClickListener onClickListener){
        if(errorMessage.length() > 0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setTitle(R.string.label_error);
            alertDialogBuilder.setMessage(errorMessage);
            alertDialogBuilder.setPositiveButton(R.string.label_ok, onClickListener);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.show();
        }
    }

    public static void showErrorNomina(Activity activity){

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_message_pg);

        TextView txt1 = (TextView)dialog.findViewById(R.id.txt1);
        TextView txt2 = (TextView)dialog.findViewById(R.id.txt2);
        TextView txtOK = (TextView)dialog.findViewById(R.id.txtOK);
        TextView txtCancel = (TextView)dialog.findViewById(R.id.txtCancel);

        txt1.setTypeface(Typeface.createFromAsset(activity.getAssets(), "stag_sans_light.ttf"));
        txt2.setTypeface(Typeface.createFromAsset(activity.getAssets(), "stag_sans_book.ttf"));
        txtOK.setTypeface(Typeface.createFromAsset(activity.getAssets(), "stag_sans_book.ttf"));
        txtCancel.setTypeface(Typeface.createFromAsset(activity.getAssets(), "stag_sans_book.ttf"));

        String message1 = "Para administrar mejor tus cuentas, cambiate a";
        String message2 = "<font color='#3EB6BB'><b>Nómina BBVA Bancomer</b></font>";

        txt1.setText(Html.fromHtml(message1), TextView.BufferType.SPANNABLE);
        txt2.setText(Html.fromHtml(message2), TextView.BufferType.SPANNABLE);

        txtOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ControllerViewCredit.portabilidad = true;
            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public static String getCurrentMont(String lastMonth){
        if(lastMonth.equalsIgnoreCase(ConstantsCUF.MARCH)){
            lastMonth = ConstantsCUF.FEBRUARY;
        }else if(lastMonth.equalsIgnoreCase(ConstantsCUF.APRIL)){
            lastMonth = ConstantsCUF.MARCH;
        }else if(lastMonth.equalsIgnoreCase(ConstantsCUF.MAY)){
            lastMonth = ConstantsCUF.APRIL;
        }else if(lastMonth.equalsIgnoreCase(ConstantsCUF.JUNE)){
            lastMonth = ConstantsCUF.MAY;
        }else if(lastMonth.equalsIgnoreCase(ConstantsCUF.JULY)){
            lastMonth = ConstantsCUF.JUNE;
        }else if(lastMonth.equalsIgnoreCase(ConstantsCUF.AUGUST)){
            lastMonth = ConstantsCUF.JULY;
        }else if(lastMonth.equalsIgnoreCase(ConstantsCUF.SEPTEMBER)){
            lastMonth = ConstantsCUF.AUGUST;
        }else if(lastMonth.equalsIgnoreCase(ConstantsCUF.OCTOBER)){
            lastMonth = ConstantsCUF.SEPTEMBER;
        }else if(lastMonth.equalsIgnoreCase(ConstantsCUF.NOVENBER)){
            lastMonth = ConstantsCUF.OCTOBER;
        }else if(lastMonth.equalsIgnoreCase(ConstantsCUF.DECEMBER)){
            lastMonth = ConstantsCUF.NOVENBER;
        }else if(lastMonth.equalsIgnoreCase(ConstantsCUF.JANUARY)){
            lastMonth = ConstantsCUF.DECEMBER;
        }else if(lastMonth.equalsIgnoreCase(ConstantsCUF.FEBRUARY)){
            lastMonth = ConstantsCUF.JANUARY;
        }
        return lastMonth;
    }

    public static String formatDate(String dateString){
        dateString = dateString.substring(0,10);
        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateformat = new SimpleDateFormat("dd MMM");
        String datetime = dateformat.format(date);
        System.out.println("Current Date Time : " + datetime);
        datetime = datetime.substring(0, 6);
        String replace = datetime.substring(2, 4);
        datetime = datetime.replace(replace,replace.toUpperCase());
        return datetime;
    }

    public static int getDay(String date){
        int day = Integer.parseInt(date.substring(0, 2));
        return day;
    }

    public static int totalDays(String date){
        int year = Integer.parseInt(date.substring(0,4));
        int m = Integer.parseInt(date.substring(5,7));
        if(m == 1 || m == 3 || m == 7 || m == 8 || m == 10 || m == 12)
            m = 31;
        else if(m ==2)
            m = new GregorianCalendar().isLeapYear(year)? 29:28;
        else{
            m = 30;
        }
        return m;
    }

    public static String selectMessageToDisplay(double d, double g, double pc, double a, double s, Context context){
        String tip = ConstantsCUF.EMPTY;

        //TODO CASO EXCELENTE: S+D>G+PC ó S+D>G (Cuando no hay créditos contratados)
        //Mensaje cuando hay Gastos y Pago de créditos
        if(pc > ConstantsCUF._VALUE_ZERO && (d + s) > (g + pc)){
            String amount = addDecimals((int)a);
            String messageLast = context.getResources().getString(R.string.caso_excelente_mia).replace(context.getResources().getString(R.string.amount), amount);
            String messageCurrent = messageLast.replace("alcanzaron","alcanzan");
            tip =selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? messageCurrent : messageLast;

            //Mensaje cuando solamente hay Gastos y no hay Pago de créditos:
        }else if((d + s) > g){
            String amount = addDecimals((int)a);
            String messageLast = context.getResources().getString(R.string.caso_excelente_mia).replace(context.getResources().getString(R.string.amount), amount);
            messageLast = messageLast.replace(" y pagos de créditos",ConstantsCUF.EMPTY);
            String messageCurrent = messageLast.replace("alcanzaron","alcanzan");
            tip =selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? messageCurrent:messageLast;
        }

        //TODO CASO SALUDABLE: S+D=G+PC ó S+D=G (Cuando no hay créditos contratados)
        //Mensaje cuando hay Gastos y Pago de créditos:
        else if(pc > ConstantsCUF._VALUE_ZERO && (d + s) == (g + pc)){
            String messageLast = context.getResources().getString(R.string.caso_saludable_mia);
            String messageCurrent = context.getResources().getString(R.string.caso_saludable_ma);
            tip =selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? messageCurrent:messageLast;

            //Mensaje cuando solamente hay Gastos y no hay Pago de créditos:
        }else if((d + s) == g){
            String messageLast = context.getResources().getString(R.string.caso_saludable_mia).replace(" y pagos de créditos", ConstantsCUF.EMPTY);;
            String messageCurrent = context.getResources().getString(R.string.caso_saludable_ma).replace(" y pagos de créditos", ConstantsCUF.EMPTY);;
            tip =selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? messageCurrent:messageLast;

        }

        //TODO CASO CON MALESTARES: S+D<G+PC

        else if(pc > ConstantsCUF._VALUE_ZERO && (d + s) < (g + pc)){
            String amount = addDecimals((int)((g + pc) - (s+d)));
            String messageLast = context.getResources().getString(R.string.caso_malestares_mia).replace(context.getResources().getString(R.string.amount), amount);
            String messageCurrent = context.getResources().getString(R.string.caso_malestares_ma).replace(context.getResources().getString(R.string.amount), amount);
            tip =selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? messageCurrent:messageLast;

        }
        //TODO CASO CRÍTICO: S+D<G
        else if((d + s) < g){
            String amount = addDecimals((int)(g - (s + d)));
            String messageLast = context.getResources().getString(R.string.caso_critico_mia).replace(context.getResources().getString(R.string.amount), amount);
            String messageCurrent = context.getResources().getString(R.string.caso_critico_ma).replace(context.getResources().getString(R.string.amount), amount);
            tip =selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? messageCurrent:messageLast;
        }

        return tip;
    }

    public static String selectMessageToDisplayWithStatus(Context context, CheckUpValues values){
        String tip = ConstantsCUF.EMPTY;

        if(Server.ALLOW_LOG)
            android.util.Log.e("Tools", String.format("selectMessageToDisplayWithStatus d +s : %s g: %s pc: %s a: %s", values.getDeposito() + values.getSaldo(), values.getGasto(), values.getPc(), values.getDisponible()));
        if((!values.isPayCredits() && !selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH)) || selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH)) {

            //TODO CASO EXCELENTE: S+D>G+PC ó S+D>G (Cuando no hay créditos contratados)
            //Mensaje cuando hay Gastos y Pago de créditos
            if (values.isPayCredits() && values.getStatus().equalsIgnoreCase(ConstantsCUF.EXCELENTE)) {
                String amount = decimalFormat(String.valueOf(values.getDisponible()));
                String messageLast = context.getResources().getString(R.string.caso_excelente_mia_nvo).replace(context.getResources().getString(R.string.amount), amount);
                String message = context.getResources().getString(R.string.caso_excelente_mia).replace(context.getResources().getString(R.string.amount), amount);
                String messageCurrent = message;
                tip = selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? messageCurrent : messageLast;

                //Mensaje cuando solamente hay Gastos y no hay Pago de créditos:
            } else if (values.getStatus().equalsIgnoreCase(ConstantsCUF.EXCELENTE)) {
                String amount = decimalFormat(String.valueOf(values.getDisponible()));
                String messageLast = context.getResources().getString(R.string.caso_excelente_mia_nvo).replace(context.getResources().getString(R.string.amount), amount);
                String message = context.getResources().getString(R.string.caso_excelente_mia).replace(context.getResources().getString(R.string.amount), amount);
                messageLast = messageLast.replace(" y pagos de créditos", ConstantsCUF.EMPTY);
                message = message.replace(" y pagos de créditos", ConstantsCUF.EMPTY);
                String messageCurrent = message;
                tip = selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? messageCurrent : messageLast;
            }

            //TODO CASO SALUDABLE: S+D=G+PC ó S+D=G (Cuando no hay créditos contratados)
            //Mensaje cuando hay Gastos y Pago de créditos:
            else if (values.isPayCredits() && values.getStatus().equalsIgnoreCase(ConstantsCUF.SALUDABLE)) {
                String messageLast = context.getResources().getString(R.string.caso_saludable_mia);
                String messageCurrent = context.getResources().getString(R.string.caso_saludable_ma);
                tip = selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? messageCurrent : messageLast;

                //Mensaje cuando solamente hay Gastos y no hay Pago de créditos:
            } else if (values.getStatus().equalsIgnoreCase(ConstantsCUF.SALUDABLE)) {
                String messageLast = context.getResources().getString(R.string.caso_saludable_mia).replace(" y pagos de créditos", ConstantsCUF.EMPTY);
                String messageCurrent = context.getResources().getString(R.string.caso_saludable_ma).replace(" y pagos de créditos", ConstantsCUF.EMPTY);
                tip = selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? messageCurrent : messageLast;

            }

            //TODO CASO CON MALESTARES: S+D<G+PC

            else if (values.isPayCredits() && values.getStatus().equalsIgnoreCase(ConstantsCUF.MALESTARES)) {
                String amount =decimalFormat(String.valueOf((values.getDeposito() + values.getPc()) - (values.getSaldo() + values.getDeposito())));
                String messageLast = context.getResources().getString(R.string.caso_malestares_mia).replace(context.getResources().getString(R.string.amount), amount);
                String messageCurrent = context.getResources().getString(R.string.caso_malestares_ma).replace(context.getResources().getString(R.string.amount), amount);
                tip = selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ? messageCurrent : messageLast;

            }
            //TODO CASO CRÍTICO: S+D<G
            else if (values.getStatus().equalsIgnoreCase(ConstantsCUF._CRITICO)) {
                String amount = decimalFormat(String.valueOf(values.getGasto() - (values.getSaldo() + values.getDeposito()))).replace("-", "");
                String messageLast = context.getResources().getString(R.string.caso_critico_mia).replace(context.getResources().getString(R.string.amount), amount);
                String messageCurrent = context.getResources().getString(R.string.caso_critico_ma).replace(context.getResources().getString(R.string.amount), amount);
                tip = selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH) ?
                        messageCurrent : messageLast;
            }
        }
        return tip;
    }

    /**
     * @param d depositos
     * @param g gastos
     * @param pc pago de créditos
     * @param a es el disponible
     * @param s saldo inicial
     * @return el color del corazón de la salud.
     */
    public static int getImageHeart(double d, double g, double pc, double a, double s){
        int image = ConstantsCUF._VALUE_ZERO;

        //TODO CASO EXCELENTE: S+D>G+PC ó S+D>G (Cuando no hay créditos contratados)
        //Mensaje cuando hay Gastos y Pago de créditos
        if(pc > ConstantsCUF._VALUE_ZERO && (d + s) > (g + pc)){
            image = R.drawable.corazon_excelente_cuf;
            //Mensaje cuando solamente hay Gastos y no hay Pago de créditos:
        }else if((d + s) > g){
            image = R.drawable.corazon_excelente_cuf;
        }

        //TODO CASO SALUDABLE: S+D=G+PC ó S+D=G (Cuando no hay créditos contratados)
        //Mensaje cuando hay Gastos y Pago de créditos:
        else if(pc > ConstantsCUF._VALUE_ZERO && (d + s) == (g + pc)){
            image = R.drawable.corazon_saludable_cuf;
            //Mensaje cuando solamente hay Gastos y no hay Pago de créditos:
        }else if((d + s) == g){
            image = R.drawable.corazon_saludable_cuf;
        }

        //TODO CASO CON MALESTARES: S+D<G+PC

        else if(pc > ConstantsCUF._VALUE_ZERO && (d + s) < (g + pc)){
            image = R.drawable.corazon_malestares_cuf;
        }
        //TODO CASO CRÍTICO: S+D<G
        else if((d + s) < g){
            image = R.drawable.corazon_critico_cuf;
        }

        return image;
    }
}
