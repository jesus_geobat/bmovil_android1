package com.bancomer.apicheckupfinanciero.models;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by DMEJIA on 03/08/16.
 */
public class AccountCUF implements Serializable{
    private String tipo;
    private String cuenta;

    private double saldo;

    private ArrayList<Account> description;


    public AccountCUF(){
        description = new ArrayList<Account>();
        tipo = ConstantsCUF.EMPTY;
        cuenta = ConstantsCUF.EMPTY;
        saldo = ConstantsCUF._VALUE_ZERO;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public ArrayList<Account> getDescription() {
        return description;
    }

    public void setDescription(ArrayList<Account> description) {
        this.description = description;
    }
}
