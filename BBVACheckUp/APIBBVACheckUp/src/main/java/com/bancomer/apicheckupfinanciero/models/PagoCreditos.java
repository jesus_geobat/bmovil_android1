package com.bancomer.apicheckupfinanciero.models;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.io.ParserJSONImpl;
import com.bancomer.apicheckupfinanciero.utils.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by DMEJIA on 22/06/16.
 */
public class PagoCreditos implements ParsingHandler{

    private ArrayList<CreditCardList> creditCardLists;

    public PagoCreditos(){
        creditCardLists = new ArrayList<CreditCardList>();
    }

    public ArrayList<CreditCardList> getCreditCardLists() {
        return creditCardLists;
    }

    public void setCreditCardLists(ArrayList<CreditCardList> creditCardLists) {
        this.creditCardLists = creditCardLists;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parserJSON) throws IOException, ParsingException {
        if(parserJSON.hasValue(ConstantsCUF.TAG_RESPONSE)) {
            JSONObject response = parserJSON.parserNextObject(ConstantsCUF.TAG_RESPONSE,false);
            parserJSON = new ParserJSONImpl(response.toString());

            if(parserJSON.hasValue(ConstantsCUF.TAG_CREDIT_CARD)) {
                JSONObject objectTDC = parserJSON.parserNextObject(ConstantsCUF.TAG_CREDIT_CARD);
                ParserJSON parserJSONTDC = new ParserJSONImpl(objectTDC.toString());

                if (parserJSONTDC.hasValue(ConstantsCUF.TAG_CREDIT_CARD_LIST)) {
                    JSONArray array = parserJSONTDC.parseNextValueWithArray(ConstantsCUF.TAG_CREDIT_CARD_LIST, false);

                    try {
                        if (array.length() > 0) {
                            for (int i = 0; i < array.length(); i++) {
                                parserJSONTDC = new ParserJSONImpl(array.get(i).toString());
                                CreditCardList creditCard = new CreditCardList();
                                if (parserJSONTDC.hasValue(ConstantsCUF.TAG_PRODUCT_BASE)) {
                                    JSONObject objProductBase = parserJSONTDC.parserNextObject(ConstantsCUF.TAG_PRODUCT_BASE, false);
                                    if (objProductBase.has(ConstantsCUF.TAG_ERROR_INFO)) {
                                        creditCard.getProductBase().setErrorInfo(objProductBase.getString(ConstantsCUF.TAG_ERROR_INFO));
                                    }
                                }

                                if (parserJSONTDC.hasValue(ConstantsCUF.TAG_CARD_NUMBER)) {
                                    creditCard.setCardNumber(parserJSONTDC.parseNextValue(ConstantsCUF.TAG_CARD_NUMBER, false));
                                }

                                if (parserJSONTDC.hasValue(ConstantsCUF.TAG_MINIMUM_PAYMENT)) {
                                    JSONObject objMinPayment = parserJSONTDC.parserNextObject(ConstantsCUF.TAG_MINIMUM_PAYMENT, false);
                                    if (objMinPayment.has(ConstantsCUF.TAG_AMOUNT)) {
                                        creditCard.setAmountMinimumPayment(objMinPayment.getString(ConstantsCUF.TAG_AMOUNT));
                                    }
                                }

                                if (parserJSONTDC.hasValue(ConstantsCUF.TAG_OUTSTANDING_DEBT)) {
                                    JSONObject objOutDebt = parserJSONTDC.parserNextObject(ConstantsCUF.TAG_OUTSTANDING_DEBT, false);
                                    if (objOutDebt.has(ConstantsCUF.TAG_AMOUNT)) {
                                        creditCard.setAmountOutstandingDebt(objOutDebt.getString(ConstantsCUF.TAG_AMOUNT));
                                    }
                                }

                                if (parserJSONTDC.hasValue(ConstantsCUF.TAG_PAYMENT_WHITH_OUT_INTEREST)) {
                                    JSONObject objPayOut = parserJSONTDC.parserNextObject(ConstantsCUF.TAG_MINIMUM_PAYMENT, false);
                                    if (objPayOut.has(ConstantsCUF.TAG_AMOUNT)) {
                                        creditCard.setAmountPaymentWithoutInterest(objPayOut.getString(ConstantsCUF.TAG_AMOUNT));
                                    }
                                }

                                if (parserJSONTDC.hasValue(ConstantsCUF.TAG_PREVIOUS_PERIODS)) {
                                    JSONArray arrayPreviousPeriods = parserJSONTDC.parseNextValueWithArray(ConstantsCUF.TAG_PREVIOUS_PERIODS, false);
                                    if (arrayPreviousPeriods.length() > ConstantsCUF._VALUE_ZERO) {

                                        for (int j = 0; j < arrayPreviousPeriods.length(); j++) {
                                            PreviousPeriods previousPeriods = new PreviousPeriods();
                                            JSONObject jsonPreviousPeriods = arrayPreviousPeriods.getJSONObject(j);
                                            if (jsonPreviousPeriods.has(ConstantsCUF.TAG_MINIMUM_PAYMENT)) {
                                                JSONObject objMinPayment = jsonPreviousPeriods.getJSONObject(ConstantsCUF.TAG_MINIMUM_PAYMENT);
                                                if (objMinPayment.has(ConstantsCUF.TAG_AMOUNT)) {
                                                    previousPeriods.setAmountMinimumPayment(Tools.decimalFormat(objMinPayment.getString(ConstantsCUF.TAG_AMOUNT)));
                                                }
                                            }

                                            if (jsonPreviousPeriods.has(ConstantsCUF.TAG_OUTSTANDING_DEBT)) {
                                                JSONObject objOutDebt = jsonPreviousPeriods.getJSONObject(ConstantsCUF.TAG_OUTSTANDING_DEBT);
                                                if (objOutDebt.has(ConstantsCUF.TAG_AMOUNT)) {
                                                    previousPeriods.setAmountOutstandingDebt(Tools.decimalFormat(objOutDebt.getString(ConstantsCUF.TAG_AMOUNT)));
                                                }
                                            }

                                            if (jsonPreviousPeriods.has(ConstantsCUF.TAG_PAYMENT_WHITH_OUT_INTEREST)) {
                                                JSONObject objPayOut = jsonPreviousPeriods.getJSONObject(ConstantsCUF.TAG_MINIMUM_PAYMENT);
                                                if (objPayOut.has(ConstantsCUF.TAG_AMOUNT)) {
                                                    previousPeriods.setAmountPaymentWithoutInterest(Tools.decimalFormat(objPayOut.getString(ConstantsCUF.TAG_AMOUNT)));
                                                }
                                            }

                                            if (jsonPreviousPeriods.has(ConstantsCUF.TAG_END_DATE)) {
                                                previousPeriods.setEndDate(Tools.formatDate(jsonPreviousPeriods.getString(ConstantsCUF.TAG_END_DATE)));
                                            }

                                            if (jsonPreviousPeriods.has(ConstantsCUF.TAG_PAYMENT_DATE)) {
                                                String date = jsonPreviousPeriods.getString(ConstantsCUF.TAG_PAYMENT_DATE);
                                                previousPeriods.setPaymentDate(Tools.formatDate(date));
                                                creditCard.setNumDays(Tools.totalDays(date));
                                            }
                                            creditCard.getPreviousPeriodsArrayList().add(previousPeriods);
                                        }
                                    }
                                }

                                creditCardLists.add(creditCard);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }
}
