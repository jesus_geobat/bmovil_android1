package com.bancomer.apicheckupfinanciero.models;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;

/**
 * Created by DMEJIA on 17/10/16.
 */
public class CheckUpValues {

    private String status;
    private String statusSC;
    private String statusDG;

    private String typeHealth;
    private double deposito;
    private double gasto;
    private double pc;
    private double disponible;
    private double saldo;
    private boolean payCredits;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getDeposito() {
        return deposito;
    }

    public void setDeposito(double deposito) {
        this.deposito = deposito;
    }

    public double getGasto() {
        return gasto;
    }

    public void setGasto(double gasto) {
        this.gasto = gasto;
    }

    public double getPc() {
        return pc;
    }

    public void setPc(double pc) {
        this.pc = pc;
    }

    public double getDisponible() {
        return disponible;
    }

    public void setDisponible(double disponible) {
        this.disponible = disponible;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public boolean isPayCredits() {
        return payCredits;
    }

    public void setPayCredits(boolean payCredits) {
        this.payCredits = payCredits;
    }

    public String getStatusSC() {
        return statusSC;
    }

    public void setStatusSC(String statusSC) {
        this.statusSC = statusSC;
    }

    public String getStatusDG() {
        return statusDG;
    }

    public void setStatusDG(String statusDG) {
        this.statusDG = statusDG;
    }

    public String getTypeHealth() {
        return typeHealth;
    }

    public void setTypeHealth(String typeHealth) {
        this.typeHealth = typeHealth;
    }
}
