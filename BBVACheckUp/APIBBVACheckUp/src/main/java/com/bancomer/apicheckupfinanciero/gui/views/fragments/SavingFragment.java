package com.bancomer.apicheckupfinanciero.gui.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bancomer.apicheckupfinanciero.R;

/**
 * Created by DMEJIA on 10/02/16.
 */
public class SavingFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_saving, container, false);
        return rootView;
    }
}
