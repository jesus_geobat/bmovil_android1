package com.bancomer.apicheckupfinanciero.gui.views.adapters;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.CreditPaymentsFragment;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.DepositExpensesFragment;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.ExpensesFragment;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.SavingFragment;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.TotalPaymentsFragment;
import com.bancomer.apicheckupfinanciero.models.CheckUp;


/**
 * Created by DMEJIA on 10/02/16.
 */
public class TotalPaymentsAdapter extends FragmentPagerAdapter {

    private CheckUp checkUpMA;

    private TotalPaymentsFragment totalPaymentsFragment;
    private CreditPaymentsFragment creditPaymentsFragment;
    private DepositExpensesFragment depositExpensesFragment;

    private Fragment fragment = null;


    public TotalPaymentsAdapter(FragmentManager fm, CheckUp checkUpMA){
        super(fm);
        this.checkUpMA = checkUpMA;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case ConstantsCUF._VALUE_ZERO:
                fragment = loadViewHalos();
                break;
            case ConstantsCUF._VALUE_ONE:

                return loadDataMA();
            case ConstantsCUF._VALUE_TWO:
                fragment = loadDataCredits();
                //fragment = new ExpensesFragment();
                break;
            case ConstantsCUF._VALUE_THREE:
                fragment = loadDataCredits();
                break;
            case ConstantsCUF._VALUE_FOUR:
                fragment = new SavingFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        super.restoreState(state, loader);
    }

    private TotalPaymentsFragment loadViewHalos(){

        Bundle bundle = new Bundle();
        bundle.putSerializable(ConstantsCUF.TAG_OBJ_TOTAL_PAYMENTS, checkUpMA);

        if(totalPaymentsFragment == null) {
            totalPaymentsFragment = new TotalPaymentsFragment();
            totalPaymentsFragment.setArguments(bundle);
        }

        return  totalPaymentsFragment;
    }

    private DepositExpensesFragment loadDataMA(){
        Bundle bundle = new Bundle();
        bundle.putSerializable(ConstantsCUF.TAG_OBJ_TOTAL_PAYMENTS, checkUpMA);

        if(depositExpensesFragment == null){
            depositExpensesFragment = new DepositExpensesFragment();
            depositExpensesFragment.setArguments(bundle);
        }

        return depositExpensesFragment;
    }

    private CreditPaymentsFragment loadDataCredits(){

        if(creditPaymentsFragment == null)
            creditPaymentsFragment = new CreditPaymentsFragment();
        /*Bundle bundle = new Bundle();
        bundle.putSerializable(ConstantsCUF.TAG_OBJ_PAGO_CREDITOS,mainDelegate.getObjPagoCreditos());
        creditPaymentsFragment.setArguments(bundle);*/

        return creditPaymentsFragment;
    }
}
