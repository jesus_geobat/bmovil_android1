/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package com.bancomer.apicheckupfinanciero.io;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;

import com.bancomer.apicheckupfinanciero.controller.InitCheckUp;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;

import bancomer.api.common.commons.NameValuePair;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
//import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;


/**
 * TODO: COMENTARIO DE LA CLASE
 *
 * @author BBVA Bancomer, DyD.
 */

public class ClienteHttp {
	
    /**
     * The field separator in the response.
     */
    private static final String PARAMETER_SEPARATOR = "*";
    
    /**
     * Se utiliza un httpclient de Apache Jakarta
     */
    DefaultHttpClient client;
    
    //protected Context mContext;

    /**
     * Cookie from server.
     */
    boolean reiniciar;

    private static StringBuffer oP = new StringBuffer();

    public boolean reiniciar(){
        return false;
    }

    public DefaultHttpClient getClient() {
		return client;
	}

	public void setClient(DefaultHttpClient client) {
		this.client = client;
	}
    org.apache.http.client.CookieStore cookieStore;
	/**
     * Constructor.
     */
    public ClienteHttp() {
    	createClient();
    }
    
    /**
     * Creates a new instance for the client.
     */
    private void createClient(){

        if(InitCheckUp.getClient() == null)
            client = new DefaultHttpClient();
        else {
            client = InitCheckUp.getClient();
        }
        HttpParams params = new BasicHttpParams();
        // Establece el timeout de la conexión y del socket a 30 segundos
        HttpConnectionParams.setConnectionTimeout(params, 900000000);
        HttpConnectionParams.setSoTimeout(params, 900000000);
        client.setParams(params);

        //Se debe descomentar cuando solo es 1 login
       cookieStore = client.getCookieStore();

        if(Server.ALLOW_LOG)
            android.util.Log.w(getClass().getSimpleName(), "createClient() ¿cookies?" + cookieStore);

    }

    public void invokeOperation(String operation, Object parameters,
                                ParsingHandler handler) throws IOException, ParsingException, JSONException, URISyntaxException {
        invokeOperation(operation, parameters, handler, false);
    }

    public void invokeOperation(String operation, Object parameters, ParsingHandler handler, boolean clearCookies)
    		throws IOException, ParsingException, URISyntaxException, JSONException {


        if(Server.ALLOW_LOG){
            Log.e("invokeOperation", "...");
            Log.e("operation", operation);
        }

        String url = getUrl(operation);


        if(Server.ALLOW_LOG) Log.e("url if", url);
        //JQH retrieve(url, method, type, contents, handler);
        if(parameters!= null)
            retrievePOST(url, parameters.toString(), null, null, handler);
        else
            retrieveGET(url, operation, handler);

    }


    private String getURLOperacionBan(String opCodeValue){
        StringBuffer buffer = new StringBuffer();

        buffer.append(Server.OPERATION_CODE_PARAMETER);
        buffer.append('=');
        buffer.append(opCodeValue);

        buffer.append('&');

        buffer.append(Server.OPERATION_LOCALE_PARAMETER);
        buffer.append('=');
        buffer.append(Server.OPERATION_LOCALE_VALUE);

        buffer.append('&');

        buffer.append(Server.OPERATION_DATA_PARAMETER);
        buffer.append('=');

        return buffer.toString();
    }


    public static String getParameterString(String operation,
                                            NameValuePair[] parameters) {

        StringBuffer sb = new StringBuffer("OP").append(operation);
        oP = new StringBuffer("OP").append(operation);
        System.out.println("Operacion=" + oP);
        if (parameters != null) {
            NameValuePair parameter;
            int size = parameters.length;
            for (int i = 0; i < size; i++) {
                parameter = parameters[i];
                String name = parameter.getName();
                String value = parameter.getValue();
                sb.append("*");
                sb.append(name);
                sb.append(value);
            }
        }

        String params = sb.toString();
        params = encode(params);

        return params;

    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    private void retrieveGET(String url,String op,
                              ParsingHandler handler) throws IOException, URISyntaxException, ParsingException, JSONException {


        if(op.equals(Server.JSON_OPERATION_CODE_VALUE_CHECKUP) || op.equals("consultaCheckUpMIA"))
            url = url + InitCheckUp.getInstance().getSession().getUsername();

        if(Server.ALLOW_LOG)
            Log.d("URL", url);
        BufferedReader in = null;
        String result = null;

        try {

            if(Server.ALLOW_LOG)
                android.util.Log.w(getClass().getSimpleName(), "retrieveGET() ¿cookies?"+cookieStore);
            client = CustomHttpClient.getHttpClient();
            client.setCookieStore(cookieStore);

            HttpGet request = new HttpGet();

            request.setURI(new URI(url));
            request.addHeader("Lan", "android");
            request.addHeader("ium", InitCheckUp.getInstance().getSession().getIum());
            HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, "OK");

            client.getConnectionManager().closeExpiredConnections();
           /* HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
            SchemeRegistry registry = new SchemeRegistry();
            SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
            socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
            registry.register(new Scheme("https", socketFactory, 443));

            HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);*/
            response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "ISO-8859-1"));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }

            in.close();
            result = sb.toString(); //respuesta
            Log.d("get", result);

        } catch (Exception e) {
            if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Ha ocurrido una excepción en la respuesta.", e);
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (result.length() > Tools.TAM_FRAGMENTO_TEXTO) {
            if(Server.ALLOW_LOG) Log.e("RESULT", "result: " + result);
        } else {
            if(Server.ALLOW_LOG) Log.e("RESULT", "result: " + result);
        }

        read(result, handler);

    }

    private void retrievePOST(String url,String params, String type, byte[] body,
                          ParsingHandler handler) throws IOException, URISyntaxException, ParsingException, JSONException {

        BufferedReader in = null;
        String result = null;

        try {

            HttpPost request = new HttpPost();

            request.setURI(new URI(url) );
            request.addHeader("Lan", "android");
            StringEntity entity = new StringEntity(params, HTTP.UTF_8);
            entity.setContentType("application/json");
            request.setEntity(entity);

            HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, "OK");

            client.getConnectionManager().closeExpiredConnections();
            response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "ISO-8859-1"));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }

            in.close();
            result = sb.toString(); //respuesta
            Log.d("POST", result.substring(result.length()));

        } catch (Exception e) {
            if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Ha ocurrido una excepción en la respuesta.", e);
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (result.length() > Tools.TAM_FRAGMENTO_TEXTO) {
            if(Server.ALLOW_LOG) Log.e("RESULT", "result: " + result);
        } else {
            if(Server.ALLOW_LOG) Log.e("RESULT", "result: " + result);
        }

        read(result, handler);

    }

    private static void read(String response,
                             ParsingHandler handler) throws IOException, ParsingException, JSONException {
        if (isJSONValid(response)) {
            parseJSON(response, handler);
        } else {
            parse(response, handler);
        }
    }


    private static boolean isJSONValid(String test) {
        boolean valid = false;
        try {
            new JSONObject(test);
            valid = true;
        } catch (JSONException ex) {
            valid = false;
        }

        return valid;
    }

    /**
     * Get the URL to invoke the server.
     * @param operation operation code
     * @return a string with the response
     */
    public static String getUrl(String operation) {
        return Server.getOperationUrl(operation);
    }

    /**
     * Get the URL to invoke the server.
     * @param operation operation code
     * @param parameters parameters to pass to the server
     * @return a string with the response
     */
   /* public static String getParameterString(String operation, NameValuePair[] parameters) {

        StringBuffer sb = new StringBuffer("OP").append(operation);
        if (parameters != null) {
            NameValuePair parameter;
            int size = parameters.length;
            for (int i = 0; i < size; i++) {
                parameter = parameters[i];
                String name = parameter.getName();
                String value = parameter.getValue();
                sb.append(PARAMETER_SEPARATOR);
                //if(Server.ALLOW_LOG) Log.e("Name",name);
                //if(Server.ALLOW_LOG) Log.e("Value",value); 
                sb.append(name);
                sb.append(value);
            }
        }
        
        String params = sb.toString();
        //params = encode(params);

        sb.setLength(0);
        
        sb.append(Server.OPERATION_CODE_PARAMETER);
        sb.append('=');
        if(Server.OPERATION_CODES[Server.OP_CONSULTA_TDC].equals(operation)){
        	sb.append(Server.OPERATION_CODE_VALUE_BMOVIL_MEJORADO);
        	
        }else if(Server.OPERATION_CODES[Server.OP_SOLICITAR_ALERTAS].equals(operation)){
        	sb.append(Server.OPERATION_CODE_VALUE_RECORTADO);
        }else{
            sb.append(Server.OPERATION_CODE_VALUE);

        }

        sb.append('&');

        sb.append(Server.OPERATION_LOCALE_PARAMETER);
        sb.append('=');
        sb.append(Server.OPERATION_LOCALE_VALUE);

        sb.append('&');

        sb.append(Server.OPERATION_DATA_PARAMETER);
        sb.append('=');
        sb.append(params);

        return sb.toString();

    }*/
    
    public static String getParameterStringWithJSON(String operation, NameValuePair[] parameters) {
    	
    	if(Server.ALLOW_LOG) Log.e("Entra", "aqui con jsOn");
    	
    	//Creamos un objeto de tipo JSON
    	JSONObject parametros = new JSONObject();

    	try {
			parametros.put("operacion",operation);
		} catch (JSONException e1) {
			if(Server.ALLOW_LOG) e1.printStackTrace();
		}
        if (parameters != null) {
            NameValuePair parameter;
            int size = parameters.length;
            for (int i = 0; i < size; i++) {
                parameter = parameters[i];
		        try 
		        {
		        	//Se almacenan los parámetros en el objeto 
		        	parametros.put(parameter.getName(), parameter.getValue());
		        	//if(Server.ALLOW_LOG) Log.e("Name",parameter.getName());
		        	//if(Server.ALLOW_LOG) Log.e("Value",parameter.getValue());
		        }
		        catch (JSONException e)
		        {
		        	if(Server.ALLOW_LOG) e.printStackTrace();
		        }
            }
        }
        
        String params = parametros.toString().replaceAll("\\\\", "");
        //params = encode(params);

        StringBuffer sb = new StringBuffer();

        sb.append(Server.OPERATION_CODE_PARAMETER);
        sb.append('=');

        if(Server.isjsonvalueCode.name()=="CUF")
            sb.append(Server.JSON_OPERATION_CODE_VALUE_CHECKUP);
        else{
            sb.append(Server.JSON_OPERATION_CODE_VALUE_LOGIN);
        }

        sb.append('&');

        sb.append(Server.OPERATION_LOCALE_PARAMETER);
        sb.append('=');
        sb.append(Server.OPERATION_LOCALE_VALUE);

        sb.append('&');

        sb.append(Server.OPERATION_DATA_PARAMETER);
        sb.append('=');
        sb.append(params);


        return sb.toString();

    }

    
    /**
     * URL encoding.
     * @param s the input string
     * @return the encodec string
     */
    public static String encode(String s) {
    	String encoded = s;
        if (s != null) {
            try {
                StringBuffer sb = new StringBuffer(s.length() * 3);
                char c;
                int size = s.length();
                for (int i = 0; i < size; i++) {
                    c = s.charAt(i);
                    if (c == '&') {
                        sb.append("%26");
                    } else if (c == ' ') {
                        sb.append('+');
                    } else if (c == '/'){
                    	sb.append("%2F");
                    } else if ((c >= ',' && c <= ';')
                    		|| (c >= 'A' && c <= 'Z')
                    		|| (c >= 'a' && c <= 'z')
                    		|| c == '_' || c == '?' || c == '*') {
                        sb.append(c);
                    } else {
                        sb.append('%');
                        if (c > 15) {
                            sb.append(Integer.toHexString((int) c));
                        } else {
                            sb.append("0" + Integer.toHexString((int) c));
                        }
                    }
                }
                encoded = sb.toString();
            } catch (Exception ex) {
                encoded = null;
            }
        }
        return (encoded);
    }
    
    /**
     * Read and parse the response.
     * @param response the response from the server
     * @param handler instance capable of parsing the result
     * @throws IOException
     * @throws ParsingException
     */
    private static void parse(String response, ParsingHandler handler) throws IOException, ParsingException {

        Reader reader = null;
        try {
            reader = new StringReader(response);
            Parser parser = new ParserImpl(reader);
            handler.process(parser);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Throwable ignored) {
                }
            }
        }
    }
    
    private static void parseJSON(String response, ParsingHandler handler) throws IOException, ParsingException {

        Reader reader = null;
        try {
            ParserJSON parser = new ParserJSONImpl(response);
            handler.process(parser);
        }
        catch (Exception e)
        {
            Log.w("Fail from ClienteHtpp", e.getLocalizedMessage());
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Throwable ignored) {
                }
            }
        }
    }
    
    
    /**
     * Gets and creates an image from the server, 
     * located in a specific URL
     * 
     * @param url the server image
     * @throws IOException
     */
    public Bitmap getImageFromServer(String url) throws IOException {
    	
    	HttpPost mHttppost = new HttpPost(url);
    	HttpResponse mHttpResponse = client.execute(mHttppost);
    	Bitmap image = null;
    	
    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
    	  HttpEntity entity = mHttpResponse.getEntity();
    	    if ( entity != null) {
    	    	 byte[] buffer = EntityUtils.toByteArray(entity);
    	    	 image = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
    	    }
    	}
    	
        return image;
    }

    /**
     * Closes all current connections
     */

    public synchronized void abort(){
    	synchronized (client) {
            if(Server.ALLOW_LOG)
                android.util.Log.w(getClass().getSimpleName(), String.format("abort() ¿cookies?%s", client.getCookieStore()));
            cookieStore= client.getCookieStore();
    		client.getConnectionManager().shutdown();
        	createClient();
		}
    }
    
    /**
     * Simula invocación de operación de Red.
     * @param operation operation code
     * @param parameters parameters
     * @param handler instance capable of parsing the result
     * @param response respuesta simulada
     * @param isJSON define si la respuesta debe ocupar un parser de JSON.
     * @throws IOException
     * @throws ParsingException
     * @throws URISyntaxException
     */
    public void simulateOperation(String operation, NameValuePair[] parameters,
    		ParsingHandler handler, String response, boolean isJSON) throws IOException,
            ParsingException, URISyntaxException {
        Log.e("REQUEST", "operation: " + operation);
        String url = getUrl(operation);
        Log.e("REQUEST", "url: " + url);
        String params = getParameterStringWithJSON(operation, parameters);
        char separator = ((url.indexOf('?') == -1) ? '?' : '&');
        url = new StringBuffer(url).append(separator).append(params).toString();
        if(Server.ALLOW_LOG) Log.e("REQUEST", "url: " + url);
        
        if (response.length() > Tools.TAM_FRAGMENTO_TEXTO) {
        	if(Server.ALLOW_LOG) Log.e("RESULT", "result: " + response);
    	} else {
    		if(Server.ALLOW_LOG) Log.e("RESULT", "result: " + response);
    	}
        Log.e("RESULT", "result: " + response);
        if(isJSON)
        	parseJSON(response, handler);
        else
        	parse(response, handler);
    }


}
