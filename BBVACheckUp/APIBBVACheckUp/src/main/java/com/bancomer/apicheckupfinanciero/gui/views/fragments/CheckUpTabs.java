package com.bancomer.apicheckupfinanciero.gui.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.gui.views.adapters.CheckUpTabsAdapter;
import com.bancomer.apicheckupfinanciero.utils.CustomerViewPager;
import com.bancomer.apicheckupfinanciero.utils.Tabs.SlidingTabLayout;

/**
 * Created by DMEJIA on 07/07/16.
 */
public class CheckUpTabs extends Fragment {

    private SlidingTabLayout mSlidingTabLayout;
    private CustomerViewPager viewPager;

    private View rootView;
    private Bundle bundle;
    private TotalPaymentsFragment totalPaymentsFragment;

      @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = this.getArguments();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_check_up_tabs, container, false);
        android.util.Log.e("dora_x", "oncreate : " );
        init();
        return rootView;
    }

    public void init() {
        getDataFromBundle();
        generateTabs();
    }

    private void generateTabs(){
        String[] tabs = { "Junio","Julio"};
        viewPager = (CustomerViewPager) rootView.findViewById(R.id.viewpagerTabs);
        CheckUpTabsAdapter adapter =new CheckUpTabsAdapter(getActivity().getSupportFragmentManager(), tabs, totalPaymentsFragment);
        viewPager.setAdapter(adapter);

        mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(viewPager, getActivity().getWindowManager());
        mSlidingTabLayout.setSelectedIndicatorColors(0xFF3484D5);
        mSlidingTabLayout.setDividerColors(0xFFD4D4D4);
        mSlidingTabLayout.setView(1);
        android.util.Log.e("dora_x", "generateTabs : " );
    }

    private void getDataFromBundle(){
        totalPaymentsFragment = (TotalPaymentsFragment) bundle.getSerializable(ConstantsCUF.TAG_OBJ_TOTAL_PAYMENTS);
    }

}
