package com.bancomer.apicheckupfinanciero.utils.Tutorial;

/**
 * Created by DMEJIA on 06/04/16.
 */
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;

public class TutorialBBVAView {
    public TextView txtDescripcionPaso;
    public ImageView imgFlecha;
    public ImageView imgMano;
    private TutorialDelegate tutorialDelegate;
    private RelativeLayout fondoTutorial;
    private FondoTutorial fondoAnimado;
    private ViewGroup pantallaPrincipal;
    private Context context;
    public static final int MENU_CONTEXTUAL = -1;

    public TutorialBBVAView(Context context, ViewGroup pantallaPrincipal) {
        this.pantallaPrincipal = pantallaPrincipal;
        this.context = context;
        LayoutInflater inflater = (LayoutInflater)context.getSystemService("layout_inflater");
        this.fondoTutorial = (RelativeLayout)inflater.inflate(R.layout.layout_tutorial_bbva, (ViewGroup)null);
        pantallaPrincipal.addView(this.fondoTutorial);
        this.txtDescripcionPaso = (TextView)this.fondoTutorial.findViewById(R.id.txt_descripcion_tutorial);
        this.imgFlecha = (ImageView)this.fondoTutorial.findViewById(R.id.img_flecha_ind);
        this.imgMano = (ImageView)this.fondoTutorial.findViewById(R.id.img_hand_tuto);
        this.fondoAnimado = (FondoTutorial)this.fondoTutorial.findViewById(R.id.fondo_animacion);
    }

    public void iniValores(String[] textosDescriptivos, int[] idViews) {
        Typeface type = Typeface.createFromAsset(this.context.getAssets(), "sans_semibold.ttf");
        this.txtDescripcionPaso.setTypeface(type);
        this.tutorialDelegate = new TutorialDelegate(this.pantallaPrincipal, textosDescriptivos, idViews);
        this.fondoTutorial.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                TutorialBBVAView.this.tutorialDelegate.nextStep();
            }
        });
    }

    public void iniciarTutorial() {
        this.tutorialDelegate.iniciaTuto(new View[]{this.txtDescripcionPaso, this.imgFlecha, this.imgMano}, this.fondoTutorial, this.fondoAnimado);
    }

    public void setColor(int color) {
        this.txtDescripcionPaso.setTextColor(this.context.getResources().getColor(color));
    }

    public void setTextSize(int size) {
        this.txtDescripcionPaso.setTextSize(1, (float)size);
    }

    public void onStartStep(int step) {
    }

    public void onStopStep(int step) {
    }
}

