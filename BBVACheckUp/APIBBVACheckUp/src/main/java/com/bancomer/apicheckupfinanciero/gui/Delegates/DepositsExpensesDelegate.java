package com.bancomer.apicheckupfinanciero.gui.Delegates;

import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RelativeLayout;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.DepositExpensesFragment;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.DepositsFragment;
import com.bancomer.apicheckupfinanciero.io.Server;
import com.bancomer.apicheckupfinanciero.models.Account;
import com.bancomer.apicheckupfinanciero.models.AccountCUF;
import com.bancomer.apicheckupfinanciero.models.CheckUp;
import com.bancomer.apicheckupfinanciero.utils.Tools;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;

/**
 * Created by DMEJIA on 20/06/16.
 */
public class DepositsExpensesDelegate {
    private DepositExpensesFragment fragment;
    private int size;

    public DepositsExpensesDelegate(DepositExpensesFragment depositsFragment){
        fragment = depositsFragment;
    }


    private int[] calculateGrades(ArrayList<AccountCUF> arrAccount, double totalAmount){
        int count = 0;
        int position = 0;
        float grade = 360F;
        int grades [] = new int[arrAccount.size()];
        if(Math.abs(totalAmount) > ConstantsCUF._VALUE_ZERO) {
            for (AccountCUF ac :
                    arrAccount) {
                double angle = calcAngle(Math.abs(ac.getSaldo()), totalAmount, grade);
                count += angle < 10 ? 1 : 0;
                grades[position] = (int)angle;
                position ++;

                if(Server.ALLOW_LOG)
                    android.util.Log.w(getClass().getSimpleName(), "calculateGrades  value: " + ac.getSaldo());
            }

            position = ConstantsCUF._VALUE_ZERO;
            grade -= (count * 10);
            for (AccountCUF ac :
                    arrAccount) {

                double g = grades[position] < 10 ? 10:calcAngle(Math.abs(ac.getSaldo()), totalAmount, grade);
                grades[position] =(int)g;
                position++;
            }
        }else{
            double g = grade/arrAccount.size();
            for (int ac :
                    grades) {
                grades[position] = (int)g;
                position++;
            }
        }


        return grades;
    }

    private double calcAngle(double value, double yValueSum, float maxAngle) {
        return value / yValueSum * maxAngle;
    }



    public SpannableString generateCenterSpannableText(String amount, int size) {
        String text = (Tools.selectedMonth.equalsIgnoreCase(ConstantsCUF.TAG_CURRENT_MONTH)?fragment.getResources().getString(R.string.saldo_disponible_cuentas):fragment.getResources().getString(R.string.saldo_disponible_cierre));
        text = text.equalsIgnoreCase(fragment.getResources().getString(R.string.saldo_disponible_cuentas))&& size == ConstantsCUF._VALUE_ONE ? fragment.getResources().getString(R.string.saldo_disponible_cuenta):text;
        SpannableString s = new SpannableString(amount+"\n"+text);
        s.setSpan(new RelativeSizeSpan(1.3f), 0, amount.length(), 0);
        s.setSpan(new ForegroundColorSpan(fragment.getActivity().getResources().getColor(R.color.colorLetters)), 0, amount.length(),0);
        s.setSpan(new RelativeSizeSpan(1f), amount.length(), s.length() , 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), amount.length(), s.length(), 0);
        s.setSpan(new ForegroundColorSpan(fragment.getActivity().getResources().getColor(R.color.colorDepositSpend)), amount.length(), s.length(), 0);
        return s;
    }

    public PieChart setData(ArrayList<AccountCUF> arrAcc, PieChart graphicPie, double totalAmount) {


        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Integer> colors = new ArrayList<Integer>();

        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.

        int arrAccount[] = calculateGrades(arrAcc,totalAmount);
        int count = 0;
        for(int i = 0; i < arrAccount.length; i++){
            int saldo = arrAccount[i];
            android.util.Log.d("saldo_cuenta", "saldo: "+saldo);
            yVals1.add(new Entry(saldo, i));
            xVals.add(ConstantsCUF.EMPTY);
            if(i >= 4){
                android.util.Log.i("Do_ra", "count: "+ count);
                android.util.Log.i("Do_ra", "i: "+ i);
                count = count <= 2 ? count +1 :1;
                android.util.Log.i("Do_ra", "i: "+ i);
                android.util.Log.i("Do_ra", "count: "+ count);

                colors.add(getColor(count));
                android.util.Log.e("Do_ra", "getColor(count): "+ getColor(count));

            }else{
                android.util.Log.i("Do_ra", "else i: "+ i);
                colors.add(getColor(i));
                android.util.Log.e("Do_ra", "getColor(i): "+ getColor(i));

            }
        }


        PieDataSet dataSet = new PieDataSet(yVals1, "Election Results");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(10f);

        // add a lot of colors





        dataSet.setColors(colors);

        PieData data = new PieData( );
        data.addDataSet(dataSet);
        graphicPie.setData(data);

        return graphicPie;
    }

    private int getColor(int color){
        switch (color){
            case ConstantsCUF._VALUE_ZERO:
                color = fragment.getActivity().getResources().getColor(R.color.colorBlue1);
                break;
            case ConstantsCUF._VALUE_ONE:
                color = fragment.getActivity().getResources().getColor(R.color.colorBlue2);
                break;
            case ConstantsCUF._VALUE_TWO:
                color = fragment.getActivity().getResources().getColor(R.color.colorBlue3);
                break;
            case ConstantsCUF._VALUE_THREE:
                color = fragment.getActivity().getResources().getColor(R.color.colorBlue4);
                break;

        }
        return color;
    }

    public int getSize() {
        return size-1;
    }

    public double getBarWidth(){
        DisplayMetrics metrics = fragment.getActivity().getResources().getDisplayMetrics();
        double w = metrics.widthPixels -(metrics.scaledDensity*10);
        return w;
    }

    public View progress(View v, double t, double amount){

        double p = amount * ConstantsCUF.PERCENTAGE_100 /t;

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) v.getLayoutParams();

        params.width = (int)p;

        v.setLayoutParams(params);

        return v;
    }

    public ArrayList<AccountCUF> plusTotalAccount(CheckUp checkUp){
        ArrayList<AccountCUF> arrCopy = (ArrayList<AccountCUF>) checkUp.getArrayAccount().clone();
        if(arrCopy != null && arrCopy.size()> ConstantsCUF._VALUE_ONE){
            AccountCUF account = new AccountCUF();
            account.setCuenta("");
            account.setSaldo(Double.parseDouble(checkUp.getBalanceT()));
            account.setTipo("Saldo disponible en mis cuentas");

            ArrayList<Account> acDescription = new ArrayList<Account>();
            Account accountD = new Account();
            accountD.setName("Saldo Inicial + Depósitos");
            double saldoInicial = Double.parseDouble(checkUp.getBalanceI());
            saldoInicial = saldoInicial > 0 ? saldoInicial: 0;
            accountD.setAmount(Tools.decimalFormat(String.valueOf(Double.parseDouble(checkUp.getDeposito()) + saldoInicial)));

            acDescription.add(accountD);

            accountD = new Account();
            accountD.setName("Gastos");
            accountD.setAmount(Tools.decimalFormat(String.valueOf(Double.parseDouble(checkUp.getGasto()) < 0?String.valueOf(Double.parseDouble(checkUp.getGasto()) * -1):checkUp.getGasto())));
            acDescription.add(accountD);

            if(checkUp.isPagaCreditos()) {
                accountD = new Account();
                accountD.setName("Pago de créditos");
                accountD.setAmount(Tools.decimalFormat(String.valueOf(checkUp.getPagoCreditos())));
                acDescription.add(accountD);
            }

            account.setDescription(acDescription);
            arrCopy.add(ConstantsCUF._VALUE_ZERO, account);
        }
        return arrCopy;
    }
}
