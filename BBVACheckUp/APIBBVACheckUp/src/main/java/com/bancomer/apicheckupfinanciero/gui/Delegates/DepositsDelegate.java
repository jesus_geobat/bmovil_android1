package com.bancomer.apicheckupfinanciero.gui.Delegates;

import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.DepositsFragment;
import com.bancomer.apicheckupfinanciero.models.Account;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;

/**
 * Created by DMEJIA on 09/06/16.
 */
public class DepositsDelegate {

    private DepositsFragment fragment;
    private int size;

    public DepositsDelegate(DepositsFragment depositsFragment){
        fragment = depositsFragment;
    }


    public SpannableString generateCenterSpannableText() {

        String amount = "$100,000";
        String text = "Saldo actual en mis cuentas";
        SpannableString s = new SpannableString(amount+"\n"+text);
        s.setSpan(new RelativeSizeSpan(1.5f), 0, amount.length(), 0);
        s.setSpan(new ForegroundColorSpan(fragment.getActivity().getResources().getColor(R.color.colorLetters)), 0, amount.length(),0);
        s.setSpan(new RelativeSizeSpan(1f), amount.length(), s.length() , 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), amount.length(), s.length(), 0);
        s.setSpan(new ForegroundColorSpan(fragment.getActivity().getResources().getColor(R.color.colorDepositSpend)), amount.length(), s.length(), 0);
        return s;
    }

    public PieChart setData(int count, float range, PieChart graphicPie, Typeface tf) {

        float mult = range;

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.

        yVals1.add(new Entry(25,ConstantsCUF._VALUE_ZERO));
        yVals1.add(new Entry(50,ConstantsCUF._VALUE_ONE));
        yVals1.add(new Entry(25,ConstantsCUF._VALUE_TWO));

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add(ConstantsCUF.EMPTY);
        xVals.add(ConstantsCUF.EMPTY);
        xVals.add(ConstantsCUF.EMPTY);

        PieDataSet dataSet = new PieDataSet(yVals1, "Election Results");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(10f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        colors.add(fragment.getActivity().getResources().getColor(R.color.colorBlue2));
        colors.add(fragment.getActivity().getResources().getColor(R.color.colorBlue3));
        colors.add(fragment.getActivity().getResources().getColor(R.color.colorBlue1));

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

/*
        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        // dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);*/

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(tf);
        graphicPie.setData(data);

        // undo all highlights
        graphicPie.highlightValues(null);

        graphicPie.invalidate();

        return graphicPie;
    }


    public int getSize() {
        return size-1;
    }
}
