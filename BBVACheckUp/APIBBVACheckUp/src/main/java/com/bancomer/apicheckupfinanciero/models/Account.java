package com.bancomer.apicheckupfinanciero.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by DMEJIA on 09/06/16.
 */
public class Account implements Serializable {

    private String name;
    private String amount;
    private int color;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
