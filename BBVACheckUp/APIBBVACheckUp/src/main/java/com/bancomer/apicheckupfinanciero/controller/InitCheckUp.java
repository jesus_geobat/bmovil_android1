package com.bancomer.apicheckupfinanciero.controller;

import android.app.Activity;
import android.content.Context;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.commons.SuiteViewReleaser;
import com.bancomer.apicheckupfinanciero.persistence.BBVACheckUpSharedPreferences;
import com.bancomer.apicheckupfinanciero.utils.Tools;
import com.bancomer.apicheckupfinanciero.gui.Delegates.MainDelegate;
import com.bancomer.apicheckupfinanciero.gui.views.activities.BaseActivity;
import com.bancomer.apicheckupfinanciero.io.BaseSubapplication;
import com.bancomer.apicheckupfinanciero.io.Server;

import org.apache.http.impl.client.DefaultHttpClient;
import java.util.Hashtable;

import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.BaseViewsControllerImpl;
import bancomer.api.common.model.HostAccounts;

/**
 * Created by DMEJIA on 16/02/16.
 */
public class InitCheckUp {

    private static InitCheckUp theInstance = null;
    private Context context;
    public static Context appContext;
    private static DefaultHttpClient client;

    private static BaseViewsController parentManager;
    private ControllerViewCredit obj;
    private ActivityController activityController;
    private BaseSubapplication baseSubapplication;
    private BaseActivity baseActivity ;
    private ICommonSession session;

    private String password;

    private BBVACheckUpSharedPreferences preferences;

    public static InitCheckUp getInstance(){
        if(theInstance == null)
            theInstance = new InitCheckUp();

        return theInstance;
    }

    public static InitCheckUp getInstance(Activity activity, Context context,DefaultHttpClient client , ICommonSession session, boolean dev, boolean sim, boolean emulator, boolean log){
        if(theInstance == null)
            theInstance = new InitCheckUp(activity, context,client, session,dev,sim, emulator, log);
        else
            parentManager.setCurrentActivityApp(activity);

        return theInstance;
    }

    public InitCheckUp(Activity activity, Context context,DefaultHttpClient client, ICommonSession session, boolean dev, boolean sim, boolean emulator, boolean log){
        setServerParams(dev, sim, emulator, log);
        //setEnviroment();
        this.client = client;
        appContext = context;
        parentManager = new BaseViewsControllerImpl();
        parentManager.setCurrentActivityApp(activity);
        activityController = new ActivityController();
        activityController.setCurrentActivity(activity);
        baseActivity = new BaseActivity();
        baseSubapplication = new BaseSubApplicationImp(context);
        this.obj = obj == null ? new ControllerViewCredit():obj;
        this.session = session;
        this.preferences = BBVACheckUpSharedPreferences.getTheInstance();

        if(Server.ALLOW_LOG)
            android.util.Log.w(getClass().getName(), "InitCheckUP ¿cliente?"+ this.client);
    }

    private void setServerParams(boolean dev, boolean sim, boolean emulator, boolean log)
    {

        if(log)
            android.util.Log.e("BAnderas TODAS", " ****"+ dev + " s "+ sim + " e "+ emulator +" l "+log);
        Server.DEVELOPMENT = dev;
        Server.SIMULATION = sim;
        Server.EMULATOR = emulator;
        Server.ALLOW_LOG = log;
    }

    public InitCheckUp(){}

    public static BaseViewsController getParentManager() {
        return parentManager;
    }

    public ControllerViewCredit getObj(){
        return obj;
    }

    public ActivityController getActivityController(){
        return activityController;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static DefaultHttpClient getClient() {
        return client;
    }

    public BaseActivity getBaseActivity() {
        return baseActivity;
    }

    public void setBaseActivity(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public BaseSubapplication getBaseSubapplication() {
        return baseSubapplication;
    }

    public void starAPIChecUp(){
        doCheckUp();
    }

    public void doCheckUp(){

           if(!preferences.contains(ConstantsCUF.SHOW_TUTORIAL)){
               preferences.setBooleanData(ConstantsCUF.SHOW_TUTORIAL,true);
               preferences.setBooleanData(ConstantsCUF.SHOW_TIPMA, true);
               preferences.setBooleanData(ConstantsCUF.SHOW_TIPMIA, true);
               preferences.setBooleanData(ConstantsCUF.SHOW_TIPSCMIA, true);
               preferences.setStringData(ConstantsCUF.HEALTH, ConstantsCUF.EMPTY);
           }


        preferences.setStringData(ConstantsCUF.HEALTH_EXPENSES, ConstantsCUF.EMPTY);
        preferences.setStringData(ConstantsCUF.HEALTH_CREDIT, ConstantsCUF.EMPTY);
        MainDelegate delegate = (MainDelegate) parentManager.getBaseDelegateForKey(MainDelegate.MAIN_CONTROLLER);

        if(delegate != null){
            parentManager.removeDelegateFromHashMap(MainDelegate.MAIN_CONTROLLER);
        }
        delegate = new MainDelegate();
        parentManager.addDelegateToHashMap(MainDelegate.MAIN_CONTROLLER, delegate);

        baseActivity.setActivity(getBaseActivity());
        baseActivity.setController(delegate);
        setBaseActivity(baseActivity);
        delegate.setActivity(getBaseActivity());
        //controller.realizaOperacion(Server.LOGIN, getBaseActivity());
        /*if(Server.SIMULATION)
            delegate.realizaOperacion(Server.LOGIN, getBaseActivity());
        else*/
            delegate.realizaOperacion(Server.CONSULTA_ESTADO_FIANCIERO_MA, getBaseActivity());
        Tools.selectedMonth = ConstantsCUF.TAG_CURRENT_MONTH;
        Tools.isDataMIA = false;
        //delegate.realizaOperacion(Server.CONSULTA_ESTADO_FIANCIERO, getBaseActivity());
        //}
    }


    public void setCurrentActivity(Activity currentActivity) {
        //screenStack.add(currentActivity);
        activityController.setCurrentActivity(currentActivity);
    }

    public static void resetInstace(){theInstance = null;}

    public ICommonSession getSession(){
        return session;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void hasTDC(HostAccounts[] accounts){
        for (HostAccounts hostAccounts:
             accounts) {
            if(hostAccounts.getType().equalsIgnoreCase("TC")){
                Tools.hasTDC = true;
                if(Server.ALLOW_LOG)
                    android.util.Log.w(getClass().getSimpleName(), "Tiene TDC");
                return;
            }
        }
    }
    public static void setAppContext(Context appContext) {
        InitCheckUp.appContext = appContext;
    }

    public BBVACheckUpSharedPreferences getPreferences() {
        return preferences;
    }
}
