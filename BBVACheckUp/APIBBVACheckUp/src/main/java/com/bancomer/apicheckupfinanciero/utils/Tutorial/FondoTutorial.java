package com.bancomer.apicheckupfinanciero.utils.Tutorial;

/**
 * Created by DMEJIA on 06/04/16.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.bancomer.apicheckupfinanciero.R;


public class FondoTutorial extends View {
    private Paint p = new Paint();
    private Paint transparentPaint = new Paint();
    private Paint marcoT = new Paint();
    private Paint paint = new Paint(0);
    private Paint texto;

    private Bitmap mBitmapToBlur, mBlurredBitmap;
    private Canvas blurCanvas;

    private int addX1 = 0;
    private int addX2 = 0;
    private int addY1 = 0;
    private int addY2 = 0;
    private static final int pasos = 15;
    private int contadorT = 1;
    private int x1;
    private int x2;
    private int y1;
    private int y2;
    private int xa1;
    private int xa2;
    private int ya1;
    private int ya2;
    private int dx1;
    private int dx2;
    private int dy1;
    private int dy2;
    private int alfa;

    public FondoTutorial(Context context) {
        this(context, null);
    }

    public FondoTutorial(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init();
    }

    private void init() {
        //setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        this.alfa = 255;
        this.xa1 = 0;
        this.xa2 = 0;
        this.ya1 = 0;
        this.ya2 = 0;
        this.dx1 = 0;
        this.dx2 = 0;
        this.dy1 = 0;
        this.dy2 = 0;
        this.contadorT = 15;
       /* paint.setXfermode(new PorterDuffXfermode(Mode.DST_ATOP));
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        //paint.setMaskFilter(new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL));
        //p.setColor(Color.TRANSPARENT);
        transparentPaint.setXfermode(new PorterDuffXfermode(Mode.DST_ATOP));
        transparentPaint.setColor(Color.TRANSPARENT);
        transparentPaint.setAntiAlias(true);
        marcoT.setStyle(Style.STROKE);
        marcoT.setColor(Color.BLUE);
        marcoT.setAntiAlias(true);*/

        paint.setColor(this.getResources().getColor(R.color.color_blanco_transparente));

        //paint.setMaskFilter(new BlurMaskFilter(20, BlurMaskFilter.Blur.NORMAL));
        transparentPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_ATOP));
        transparentPaint.setColor(Color.TRANSPARENT);
        transparentPaint.setAntiAlias(true);
        marcoT.setStyle(Paint.Style.STROKE);
        marcoT.setColor(this.getResources().getColor(R.color.color_blanco_transparente));
        marcoT.setAntiAlias(true);
        texto = new Paint();

    }
boolean f = true;
    @Override
    protected void onDraw(Canvas canvas) {
        if(this.getWidth() != 0 && this.getHeight() != 0) {
            int cocienteIntX1 = this.contadorT * this.dx1 / 15;
            int cocienteIntX2 = this.contadorT * this.dx2 / 15;
            int cocienteIntY1 = this.contadorT * this.dy1 / 15;
            int cocienteIntY2 = this.contadorT * this.dy2 / 15;
            this.addX1 = Math.abs(this.addX1) < Math.abs(cocienteIntX1)?cocienteIntX1:this.addX1;
            this.addX2 = Math.abs(this.addX2) < Math.abs(cocienteIntX2)?cocienteIntX2:this.addX2;
            this.addY1 = Math.abs(this.addY1) < Math.abs(cocienteIntY1)?cocienteIntY1:this.addY1;
            this.addY2 = Math.abs(this.addY2) < Math.abs(cocienteIntY2)?cocienteIntY2:this.addY2;

           //utilizando blur
           /* if(preparate()) {
                if (blurCanvas == null) {
                    if (mBlurredBitmap == null) {
                        mBitmapToBlur = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(),
                                Bitmap.Config.ARGB_8888);
                        if (mBitmapToBlur == null) {
                            System.out.print("primer blur nulo");
                        }

                        mBlurredBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(),
                                Bitmap.Config.ARGB_8888);
                        if (mBlurredBitmap == null) {
                            System.out.print("segundo blur nulo");
                        }
                    }
                    blurCanvas = new Canvas(mBitmapToBlur);

                }

                mBitmapToBlur.eraseColor(Color.TRANSPARENT);
            }*/
           Bitmap bitmap =
                   Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
           // bitmap.eraseColor(this.getResources().getDrawable(R.drawable.blur));
            Blur blur = new Blur();
            bitmap = blur.fastblur(getContext(),bitmap,10);

            Canvas temp = new Canvas(bitmap);

            temp.drawRect(0.0F, 0.0F, (float) temp.getWidth(), (float) temp.getHeight(), paint);
            transparentPaint.setAlpha(this.alfa);
            RectF r = new RectF((float)(this.xa1 + this.addX1 - 5), (float)(this.ya1 + this.addY1 - 5), (float)(this.xa2 + this.addX2 + 5), (float)(this.ya2 + this.addY2 + 5));
            temp.drawRoundRect(r, 10.0F, 10.0F, transparentPaint);
            temp.drawRoundRect(r, 10.0F, 10.0F, marcoT);
            canvas.drawBitmap(bitmap, 0.0F, 0.0F, p);

              //canvas.drawColor(this.getResources().getColor(R.color));
            //TODO dibujando pero no lgrado aceleracion
/*            Bitmap bitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);

            Canvas temp = new Canvas(bitmap);
            temp.drawRect(0.0F, 0.0F, (float) temp.getWidth(), (float) temp.getHeight(), paint);
            //canvas.drawPaint(paint);
            RectF r = new RectF((float)(this.xa1 + this.addX1 - 5), (float)(this.ya1 + this.addY1 - 5), (float)(this.xa2 + this.addX2 + 5), (float)(this.ya2 + this.addY2 + 5));
            temp.drawRoundRect(r, 10.0F, 10.0F, transparentPaint);
            temp.drawRoundRect(r, 10.0F, 10.0F, marcoT);
            canvas.drawText("hello", 0, 50, texto);
            RectF rectF = new RectF();
            rectF.set(0.0F, 0.0F, (float) temp.getWidth(), (float) temp.getHeight());
            //canvas.drawRect(rectF,paint);
            canvas.drawBitmap(bitmap, 0.0F, 0.0F, p);

*/

            if(this.contadorT < 15) {
                ++this.contadorT;
                this.invalidate();
            }

        }
    }

    private boolean preparate(){
        return true;
    }

    private  Bitmap drawViewToBitmap(View view, int color) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(color);
        view.draw(canvas);
        return returnedBitmap;
    }

    public void changeArea(int a, int x1, int y1, int x2, int y2) {
        marcoT.setStrokeWidth(3.0F);
        this.alfa = a;
        this.dx1 = x1 - this.x1;
        this.dx2 = x2 - this.x2;
        this.dy1 = y1 - this.y1;
        this.dy2 = y2 - this.y2;
        this.addX1 = 0;
        this.addX2 = 0;
        this.addY1 = 0;
        this.addY2 = 0;
        this.xa1 = this.x1;
        this.xa2 = this.x2;
        this.ya1 = this.y1;
        this.ya2 = this.y2;
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
        this.contadorT = 1;
        this.invalidate();
    }

    public void finalizaAnim() {
        this.alfa = 255;
        marcoT.setStrokeWidth(0.0F);
        this.x1 = 0;
        this.y1 = 0;
        this.x2 = 0;
        this.y2 = 0;
        this.xa1 = 0;
        this.xa2 = 0;
        this.ya1 = 0;
        this.ya2 = 0;
        this.dx1 = 0;
        this.dx2 = 0;
        this.dy1 = 0;
        this.dy2 = 0;
        this.contadorT = 15;
    }
}
