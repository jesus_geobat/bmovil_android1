package com.bancomer.apicheckupfinanciero.gui.views.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.SavingFragment;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.TotalPaymentsFragment;

/**
 * Created by DMEJIA on 07/07/16.
 */
public class CheckUpTabsAdapter extends FragmentStatePagerAdapter {

    private String[] tabsTitles;
    private TotalPaymentsFragment fragment;

    public CheckUpTabsAdapter(FragmentManager fm, String[] tabsTitles, TotalPaymentsFragment fragment) {
        super(fm);
        this.tabsTitles = tabsTitles;
        this.fragment = fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabsTitles[position];
    }

    @Override
    public Fragment getItem(int index) {
        android.util.Log.e("dora_x", "getItem : "  + index);
        if(index == 0)
        return loadViewHalos();
        else
            return  new SavingFragment();
    }

    @Override
    public int getCount() {
        return tabsTitles.length;
    }


    private TotalPaymentsFragment loadViewHalos(){

        Bundle bundle = new Bundle(ConstantsCUF.TOTAL_HALOS);
        bundle.putDouble(ConstantsCUF.TAG_GASTO, Double.parseDouble("50"));
        bundle.putDouble(ConstantsCUF.TAG_AHORRO, Double.parseDouble("100"));
        bundle.putDouble(ConstantsCUF.TAG_DEPOSITO, Double.parseDouble("500"));
        bundle.putDouble(ConstantsCUF.TAG_PAGO_CREDITO, Double.parseDouble("50"));
        //bundle.putString(ConstantsCUF.TAG_LAST_MONTH, "Junio");
        bundle.putInt(ConstantsCUF.TAG_NUM_HALOS, 4);

        fragment = new TotalPaymentsFragment();
        fragment.setArguments(bundle);

        return  fragment;
    }
}