package com.bancomer.apicheckupfinanciero.models;

import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;

import java.io.Serializable;

/**
 * Created by DMEJIA on 20/05/16.
 */
public class Details implements Serializable {

    private String description;
    private String date;
    private String amount;
    private String product;

    public Details(){
        description = ConstantsCUF.EMPTY;
        date = ConstantsCUF.EMPTY;
        amount = ConstantsCUF.EMPTY;
        product = ConstantsCUF.EMPTY;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}
