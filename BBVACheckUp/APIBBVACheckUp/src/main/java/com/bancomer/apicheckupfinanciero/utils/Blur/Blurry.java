package com.bancomer.apicheckupfinanciero.utils.Blur;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.gui.views.activities.MainActivity;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.TotalPaymentsFragment;
import com.bancomer.apicheckupfinanciero.utils.ScaleTool;
import com.bancomer.apicheckupfinanciero.utils.Tools;

/**
 * Copyright (C) 2015 Wasabeef
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class Blurry {

  private static final String TAG = Blurry.class.getSimpleName();

  public static Composer with(Context context) {
    return new Composer(context);
  }

  public static void delete(ViewGroup target) {
    View view = target.findViewWithTag(TAG);
    if (view != null) {
      android.util.Log.e("Blurry", "remueve la vista");
      View v = target.findViewWithTag("d");
      target.removeView(view);
      target.removeView(v);
    }
  }

  public static boolean isShowTutorial(ViewGroup target){
    View view = target.findViewWithTag(TAG);
    if (view != null) {
      return true;
    }else
      return false;
  }

  public static class Composer {

    private View rlMessage;
    private View lnHeart;
    private View header;
    private LinearLayout welcome;
    private View blurredView;
    private View vista;
    private ViewGroup target;
    private View arrView[];
    private View currentView;
    private View message;
    private View close;
    private Context context;
    private BlurFactor factor;
    private boolean async;
    private boolean animate;
    private int isShowTip;
    private TotalPaymentsFragment fragment;

    private int duration = 300;
    private ImageComposer.ImageComposerListener listener;
    private int n;
    private int i;
    private int j;
    private int widthText;
    private int indicatorBottom;
    int min;
    int margen ;
    private int radious[];

    private BlurTask task;
    private ScaleTool tool;

    public Composer(Context context) {
      this.context = context;
      blurredView = new View(context);
      blurredView.setTag(TAG);
      vista = new View(context);
      vista.setTag("d");
      factor = new BlurFactor();
      message = new TextView(context);
      close = new ImageView(context);
      //messages = {"Hugo, en esta sección encontrarás tips que te ayudarán a mejorar o mantener tu salud financiera","Aquí encontrarás el estado de tu salud financiera pintado el corazón de acuerdo a el","Aquí encontrarás de totdas tus cuentas en un mismo lugar, tus depósitos, gastos y pagos de créditos",};
    }

    public Composer radius(int radius) {
      factor.radius = radius;
      return this;
    }

    public Composer sampling(int sampling) {
      factor.sampling = sampling;
      return this;
    }

    public Composer color(int color) {
      factor.color = color;
      return this;
    }

    public Composer async() {
      async = true;
      return this;
    }

    public Composer async(ImageComposer.ImageComposerListener listener) {
      async = true;
      this.listener = listener;
      return this;
    }

    public Composer animate() {
      animate = true;
      return this;
    }

    public Composer animate(int duration) {
      animate = true;
      this.duration = duration;
      return this;
    }

    public ImageComposer capture(View capture) {
      return new ImageComposer(context, capture, factor, async, listener);
    }
    int topDisponible;
    public void onto(LinearLayout header,LinearLayout welcome,int top,final ViewGroup target, View []view, int i, int j, int[] radious, int isShowTip, TotalPaymentsFragment fragment) {
      this.welcome = welcome;
      this.header = header;
      topDisponible = top;
      factor.width = target.getMeasuredWidth();
      factor.height = target.getMeasuredHeight();
      this.target = target;
      factor.w = view[0].getMeasuredWidth();
      factor.h = view[0].getMeasuredHeight();
      arrView = view;
      this.radious = radious;
      currentView = view[0];
      n = 0;
      rlMessage = view[arrView.length-1];
      lnHeart = view[arrView.length-2];
      this.isShowTip = isShowTip;
      this.i = i;
      this.j = j;
      tool = new ScaleTool(context);
      fragment.dimensPie();
      indicatorBottom = factor.height -(arrView[2].getMeasuredHeight()+i+j);
      this.fragment = fragment;
      if (async) {
        task = new BlurTask(target,view[0] ,factor, new BlurTask.Callback() {
          @Override public void done(BitmapDrawable drawable, BitmapDrawable drawable2) {
            addView(target, drawable, drawable2);
          }
        });
        task.execute();
      } else {
        Drawable drawable = new BitmapDrawable(context.getResources(), Blur.of(target, factor));
        addView(target, drawable, drawable);

      }
    }

    private void next(){

      if(n < arrView.length-3) {
        //agregado para eliminar
          target.removeView(vista);
        factor.w = arrView[n].getMeasuredWidth();
        factor.h = arrView[n].getMeasuredHeight();

        currentView = arrView[n];
        View view = currentView;
        view.setDrawingCacheEnabled(true);
        Resources res = view.getResources();
        view.destroyDrawingCache();
        Bitmap bitmap = view.getDrawingCache();
        BitmapDrawable bitmapDrawable =
                new BitmapDrawable(res, bitmap);
        addView(target, bitmapDrawable);
      }else{
        fragment.showTipFromTutorial(isShowTip);
        //fragment.centerGraphic();
        target.removeView(blurredView);
        target.removeView(vista);
        rlMessage.setVisibility(View.GONE);
        fragment.visibleFoto();
      }
      /*if(n == 0 )
        n += 2;
      else
        n++;

      if(n ==3) {
        if(!disponible) {
          n ++;
        }
      }*/
      n++;

    }

    private void addView(ViewGroup target, Drawable drawable, Drawable drawable2) {
      Helper.setBackground(blurredView, drawable);
      target.addView(blurredView);
      TextView ok = (TextView)welcome.findViewById(R.id.txtOK);
      ok.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          next();
        }
      });
      blurredView.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
          next();
        }
      });
      fragment.showTipFromTutorial(View.VISIBLE);
      showMesasge();
    }
    private void addView(ViewGroup target, Drawable drawable) {
      Helper.setBackground(vista, drawable);
      RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(factor.w, factor.h);

      if(n == 0 || n ==4) {
        p.setMargins((int) currentView.getLeft(), (int) currentView.getTop() +  i+j, 0, 0);
      }else if(n == 1 ) {
        p.setMargins((int)currentView.getLeft(), (int)currentView.getTop() +i, 0, 0);
      }else if(n==5){
        p.setMargins((int) currentView.getLeft(), (int)currentView.getTop(), 0, 0);
      }else if(n == 3){
        p.setMargins((int) currentView.getLeft(), (int)currentView.getTop() + i +j + topDisponible, 0, 0);
      }else{
        p.setMargins((int) currentView.getLeft(), (int)currentView.getTop() + i +j, 0, 0);
      }

      vista.setLayoutParams(p);
      target.addView(vista);
      showMessage(n);
    }

    private void showMesasge(){

      widthText = (int)(700 * ScaleTool.SCALE_FACTOR);
      message = target.findViewById(R.id.txtMessageTuto);
      View txtOk = target.findViewById(R.id.txtOk);


      String styledText = "¡Bienvenido! conoce la  "+"<font color='#009ee5'>SALUD</font> de tus finanzas y cómo administrarlas a tu favor";

      ((TextView) message).setTextColor(context.getResources().getColor(R.color.colorTutorial));
      ((TextView) message).setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);
      ((TextView) message).setTypeface(Typeface.createFromAsset(this.context.getAssets(), "stag_sans_book.ttf"));

      //message.setPadding((int) (ScaleTool.SCALE_FACTOR * 18), (int) (ScaleTool.SCALE_FACTOR * 50), (int) (ScaleTool.SCALE_FACTOR * 18), (int) (ScaleTool.SCALE_FACTOR * 18));
      //scaleView();
      message.measure(View.MeasureSpec.UNSPECIFIED,
              View.MeasureSpec.UNSPECIFIED);


      LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtOk.getLayoutParams();
      params.height = (int) (((140*ScaleTool.SCALE_FACTOR))- (10*context.getResources().getDisplayMetrics().scaledDensity)*2);
      params.gravity = Gravity.CENTER;
      txtOk.setLayoutParams(params);
      txtOk.requestLayout();

      RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(widthText, ViewGroup.LayoutParams.WRAP_CONTENT);
      p.setMargins((int) (ScaleTool.SCALE_FACTOR * 10), factor.h + i + (int) (ScaleTool.SCALE_FACTOR * 5), (int) (ScaleTool.SCALE_FACTOR * 10), 0);
      rlMessage.setLayoutParams(p);

      rlMessage.setVisibility(View.VISIBLE);
      target.removeView(rlMessage);
      target.addView(rlMessage);
      welcome.setVisibility(View.VISIBLE);
      target.removeView(header);
      target.addView(header);
      target.removeView(welcome);
      target.addView(welcome);


    }
    RelativeLayout.LayoutParams aux = null;
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void showMessage(int n){
      //target.removeView(message);
      //target.removeView(close);
      welcome.setVisibility(View.GONE);
      target.removeView(rlMessage);
      String message_ = "";
      int y = currentView.getTop();
      switch (n){
        case 0:
          target.removeView(lnHeart);
          target.addView(lnHeart);
          lnHeart.setVisibility(View.VISIBLE);
          y += i+j + currentView.getHeight();
          message_ ="El <font color='#009ee5'>CORAZÓN</font> cambia con la salud de tus finanzas" ;
          break;
        case 1:
          lnHeart.setVisibility(View.GONE);
          y = factor.h + (int) (ScaleTool.SCALE_FACTOR * 5) + i;
          message_="Revisa tus finanzas del <font color='#009ee5'>MES ANTERIOR</font> y al <font color='#009ee5'>DÍA DE HOY</font>";
          //message_ ="Al dar clic sobre el "+"<font color='#009ee5'>FOCO</font>, encontrarás tips que te\n ayudarán a mejorar o mantener tus finanzas\n saludables";
          break;
        case 2:
          lnHeart.setVisibility(View.GONE);
          y = factor.h +indicatorBottom;

          if(factor.h > factor.w) {
            margen = (factor.h - factor.w) / 2;
            min = factor.w;
          }else {
            min = factor.h;
          }
          y -=  margen + (min/2*(100-radious[0]))/100 ;
          message_ ="Los <font color='#009ee5'>AROS</font>, indican cómo se mueve tu dinero";
          break;
        case 3:
          //y = currentView.getHeight()/2 + indicatorBottom + (min/2)*(radious[1])/100 ;
          y = currentView.getHeight()/2 + indicatorBottom -(min/2)*(radious[1])/100 - rlMessage.getMeasuredHeight()- topDisponible;
          //y -= (min/2)*(radious[1])/100  + message.getMeasuredHeight()+ (Tools.get_border()+1)*context.getResources().getDisplayMetrics().density;
          message_ ="Así como el dinero <font color='#009ee5'>DISPONIBLE</font> de tus cuentas en <font color='#009ee5'>BBVA Bancomer</font>" ;
         // yTop = factor.h/2 + (i+j)+ ((min)/2*radious[1])/100;
          break;
        case 4:
          message_ ="¡Descubre cómo mejorar o mantener sanas tus finanzas! ";
          y = (factor.height-i-j)/2 - rlMessage.getHeight();
          //y= arrView[2].getHeight()*
          android.util.Log.i("x_xx", "factor.height " + factor.height + " indicator "+indicatorBottom + "  yy: "+y);

         // y = factor.h; //cuando indicaba pageindicator
          break;
        case 5:
          y =factor.height- i+ (int) (ScaleTool.SCALE_FACTOR * 5)-rlMessage.getMeasuredHeight();
          message_="Prende y apaga los tips con este <font color='#009ee5'>BOTÓN</font>";
          break;
      }
      android.util.Log.i("x_xx", "y: " + y + " n = " + n);
      RelativeLayout.LayoutParams p;
      if(n > 3){
        p = aux;
      }else
      p = (RelativeLayout.LayoutParams)rlMessage.getLayoutParams();

      if(n== 0){
        aux = (RelativeLayout.LayoutParams)rlMessage.getLayoutParams();
      }
      if(n== 2 || n==3) {
        p.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
      }else if(n > 3){
        p = aux;
      }
      p.addRule(RelativeLayout.CENTER_HORIZONTAL);
      if(n>=2){
        p.setMargins(0, 0, 0, n==2?(y +(int)(ScaleTool.SCALE_FACTOR*20)):y);
      }
      else{
        p.setMargins(0,y+ (int)(ScaleTool.SCALE_FACTOR*15),0,0);
      }
      /*if(n==1  || n == 0 || n ==4) {

        if(n==4)
          p.setMargins(0,0,0,y- (int)(ScaleTool.SCALE_FACTOR*15));
        else
        p.setMargins(0,y+ (int)(ScaleTool.SCALE_FACTOR*15),0,0);
      }else if(n == 3)
        p.setMargins(0, 0, 0, y);
      else
        p.setMargins(0, 0, 0, y +(int)(ScaleTool.SCALE_FACTOR*15));*/

        rlMessage.setLayoutParams(p);
      //message.setLayoutParams(p);
      ((TextView) message).setTextColor(context.getResources().getColor(R.color.colorTutorial));
      ((TextView) message).setText(Html.fromHtml(message_), TextView.BufferType.SPANNABLE);
      ((TextView) message).setTypeface(Typeface.createFromAsset(this.context.getAssets(), "stag_sans_book.ttf"));


      target.addView(rlMessage);
    }
  }



  public static class ImageComposer {

    private Context context;
    private View capture;
    private BlurFactor factor;
    private boolean async;
    private ImageComposerListener listener;

    public ImageComposer(Context context, View capture, BlurFactor factor, boolean async,
        ImageComposerListener listener) {
      this.context = context;
      this.capture = capture;
      this.factor = factor;
      this.async = async;
      this.listener = listener;
    }

    public void into(final ImageView target) {
      factor.width = capture.getMeasuredWidth();
      factor.height = capture.getMeasuredHeight();
      View v = null;
      if (async) {
        BlurTask task = new BlurTask(capture, v,factor, new BlurTask.Callback() {
          @Override public void done(BitmapDrawable drawable, BitmapDrawable d2) {
            if (listener == null) {
              target.setImageDrawable(drawable);
            } else {
              listener.onImageReady(drawable);
            }
          }
        });
        task.execute();
      } else {
        Drawable drawable = new BitmapDrawable(context.getResources(), Blur.of(capture, factor));
        target.setImageDrawable(drawable);
      }
    }

    public interface ImageComposerListener {
      void onImageReady(BitmapDrawable drawable);
    }
  }
}
