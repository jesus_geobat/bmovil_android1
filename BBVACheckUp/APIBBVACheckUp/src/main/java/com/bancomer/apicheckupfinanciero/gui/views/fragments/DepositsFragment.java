package com.bancomer.apicheckupfinanciero.gui.views.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.gui.Delegates.DepositsDelegate;
import com.bancomer.apicheckupfinanciero.gui.views.adapters.ExpandableListViewDepositsAdapter;
import com.bancomer.apicheckupfinanciero.utils.CustomExpandableHeightListView;
import com.bancomer.apicheckupfinanciero.utils.CustomerTextView;
import com.bancomer.apicheckupfinanciero.utils.Tools;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

/**
 * Created by DMEJIA on 10/02/16.
 */
public class DepositsFragment extends Fragment implements  OnChartValueSelectedListener {

    private DepositsDelegate delegate;

    private View rootView;
    private ScrollView scrollView;

    private CustomerTextView txtLastMonth;
    private CustomerTextView txtCurrentMont;

    private CustomExpandableHeightListView expandableListView;
    private PieChart graphicPie;
    private Typeface tf;
    private int aux;
    private int axuHeight;
    private ExpandableListViewDepositsAdapter adapter;

    public DepositsFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_deposits, container, false);
        init();
        return rootView;
    }


    private void init(){
        delegate = new DepositsDelegate(this);
        findViews();
        createGraphic();
        createExpandableListView();
        initialize();
    }

    private void initialize(){
        aux = 0;
        axuHeight = 0;
        txtLastMonth.setText(Tools.lastMonth);
        txtCurrentMont.setText(Tools.currentMonth);
       // setListViewHeight(expandableListView);
        expandableListView.expandGroup(0);
    }
    private void findViews(){
        graphicPie = (PieChart) rootView.findViewById(R.id.graphicPie);
        expandableListView = (CustomExpandableHeightListView) rootView.findViewById(R.id.expLsAccount);
        txtCurrentMont = (CustomerTextView) rootView.findViewById(R.id.txtCurrentMonth);
        txtLastMonth = (CustomerTextView) rootView.findViewById(R.id.txtLastMonth);
        scrollView = (ScrollView) rootView.findViewById(R.id.scrollDeposit);
    }

    private void createGraphic(){
        graphicPie.setUsePercentValues(true);
        graphicPie.setDescription("");
        graphicPie.setExtraOffsets(5, 0, 5, 5);
        graphicPie.setPieCircle(true);

        tf = Typeface.createFromAsset(getActivity().getAssets(), "sans_semibold.ttf");

        graphicPie.setCenterTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "stag_sans_book.ttf"));
        graphicPie.setCenterText(delegate.generateCenterSpannableText());
        graphicPie.setDrawCenterText(true);

        graphicPie.setDragDecelerationFrictionCoef(0.95f);

        graphicPie.setDrawHoleEnabled(true);
        graphicPie.setHoleColor(Color.WHITE);

        graphicPie.setTransparentCircleColor(Color.WHITE);
        graphicPie.setTransparentCircleAlpha(110);
        graphicPie.setRotationEnabled(false);
        graphicPie.setHoleRadius(70f);
        graphicPie.setRotationAngle(-90);
        graphicPie.setTransparentCircleRadius(61f);
        graphicPie = delegate.setData(3, 100, graphicPie, tf);

       // graphicPie.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // graphicPie.spin(2000, 0, 360);

        Legend l = graphicPie.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        l.setEnabled(false);
    }

    private void createExpandableListView(){
        //adapter = new ExpandableListViewDepositsAdapter(this,delegate.getData());
        expandableListView.setAdapter(adapter);
        expandableListView.setExpanded(true);

    }

    public void connet(int index) {
        ((ExpandableListViewDepositsAdapter)expandableListView.getExpandableListAdapter()).setFlag(false);
        if(index<2) {
            if (!expandableListView.isGroupExpanded(index)) {
                if (aux != -1)
                    expandableListView.collapseGroup(aux);
                expandableListView.expandGroup(index);
                aux = index;
            } else {
                expandableListView.collapseGroup(index);
                aux = -1;
            }
        }else{
            expandableListView.collapseGroup(aux);
            aux = -1;
        }
        ((ExpandableListViewDepositsAdapter)expandableListView.getExpandableListAdapter()).setFlag(true);

    }

   /* @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        graphicPie.setIndex(groupPosition);
        graphicPie.drawSelected1();
        android.util.Log.e("dorinax", "droup: " + groupPosition + " aux " + aux);
        android.util.Log.e("dorinax", "tamaño: "+ expandableListView.getExpandableListAdapter().getGroupCount() + " aux " + aux);
            if (aux > -1) {
                expandableListView.collapseGroup(aux);
                aux = groupPosition;
            } else {
                aux = groupPosition;
            }
        android.util.Log.e("dorinax", "droup: "+ groupPosition + " aux " + aux);
        return true;
    }*/

    public ExpandableListView getExpandableListView(){

        return expandableListView;
    }


    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        if(e != null) {
            android.util.Log.i("MPAndroidChart", "seleccionado ***********::" + e.getXIndex());
        }else
            android.util.Log.i("MPAndroidChart", "nulooo*****" );
    }

    @Override
    public void onNothingSelected(boolean b) {

    }
}
