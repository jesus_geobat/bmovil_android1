package com.bancomer.apicheckupfinanciero.gui.views.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.commons.ConstantsCUF;
import com.bancomer.apicheckupfinanciero.gui.Delegates.CreditPaymentsDelegate;
import com.bancomer.apicheckupfinanciero.gui.Delegates.MainDelegate;
import com.bancomer.apicheckupfinanciero.gui.views.activities.MainActivity;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.DetailCredtPaymentsFragment;
import com.bancomer.apicheckupfinanciero.models.CreditCardList;
import com.bancomer.apicheckupfinanciero.utils.CustomerTextView;
import com.bancomer.apicheckupfinanciero.utils.Tools;

import java.util.ArrayList;

/**
 * Created by DMEJIA on 12/05/16.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CreditsAdapter extends BaseAdapter implements View.OnLayoutChangeListener{

    private Context context;
    private CreditPaymentsDelegate delegate;
    private Fragment fragment;

    private ImageView imgIndicator;
    private ImageView imgLine;
    private ImageView imgToolTip;
    private ImageView icon;

    private CustomerTextView txtDate;
    private CustomerTextView txtAmount;


    private ArrayList<CreditCardList> creditCardLists;
    private int position;
    private DisplayMetrics metrics;
    private ArrayList<View> items;

    private String paymentDate;
    private int totalDays;

    public CreditsAdapter(Context context, ArrayList<CreditCardList> creditCardLists, CreditPaymentsDelegate delegate, Fragment fragment){
        this.context = context;
        this.creditCardLists = creditCardLists;
        this.delegate = delegate;
        this.fragment = fragment;
        calculateDimens();
        items = new ArrayList<View>();
    }

    private void calculateDimens(){
        metrics = context.getResources().getDisplayMetrics();
    }
    @Override
    public int getCount() {
        return creditCardLists.size();
    }

    @Override
    public Object getItem(int position) {
        return creditCardLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.layout_item_line_time,
                    null);
        }

        this.position = position;


        imgIndicator = (ImageView)convertView.findViewById(R.id.indicator);
        imgLine = (ImageView)convertView.findViewById(R.id.lineTime);
        imgToolTip = (ImageView)convertView.findViewById(R.id.img_tool_tip);
        icon = (ImageView)convertView.findViewById(R.id.imgLoands);

        txtAmount = (CustomerTextView)convertView.findViewById(R.id.txt_amount);

        txtDate = (CustomerTextView)convertView.findViewById(R.id.txt_date);
        paymentDate = creditCardLists.get(position).getPreviousPeriodsArrayList().get(0).getPaymentDate();
        totalDays = creditCardLists.get(position).getNumDays();
        txtDate.setText(paymentDate);

        txtAmount.setText(
                creditCardLists.get(position).getPreviousPeriodsArrayList().get(0).getAmountOutstandingDebt());
        icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ico_tarcre_ver));
        delegate.setColorGraphic(imgIndicator, imgLine, icon);

        convertView.addOnLayoutChangeListener((View.OnLayoutChangeListener) this);

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ImageView i = (ImageView)v;
                FragmentManager fm = ((MainActivity)context).getSupportFragmentManager();
                DetailCredtPaymentsFragment dialog = null;
                dialog = new DetailCredtPaymentsFragment(creditCardLists.get(position).getPreviousPeriodsArrayList().get(0).getAmountOutstandingDebt(),
                        creditCardLists.get(position).getPreviousPeriodsArrayList().get(0).getPaymentDate(),
                        ConstantsCUF.EXCELENTE,
                        i.getDrawable(),
                        Tools.getDay(creditCardLists.get(position).getPreviousPeriodsArrayList().get(0).getPaymentDate()),
                        creditCardLists.get(position).getNumDays(),
                        false,
                        fragment,
                        creditCardLists.get(position).getPreviousPeriodsArrayList().get(0));
                dialog.show(fm, "Ticket");

            }
        });
        return convertView;
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        final int[] w = new int[2];

        imgLine.measure(0,0);
        android.util.Log.w("g_f_f", "imgLine.getWidth()"+ imgLine.getWidth());
        RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams)imgIndicator.getLayoutParams();
        float margint =  (5 * metrics.scaledDensity);
        p.leftMargin = (int) (imgLine.getLeft() + ((imgLine.getWidth()- margint*2)/totalDays*Tools.getDay(paymentDate))+ margint);
        imgIndicator.setLayoutParams(p);

        w[0] = (int) txtAmount.getWidth();
        w[1] = (int) txtDate.getWidth();

        android.util.Log.i("d_medidas", "valores: d" + w[0] + " posiciòn::: " +imgLine.getMeasuredHeight());
        android.util.Log.i("d_medidas", "valores: d" + w[1]);



        if (w[0] > 0) {

            RelativeLayout.LayoutParams t = (RelativeLayout.LayoutParams) imgToolTip.getLayoutParams();
            t.leftMargin = (int) ((p.leftMargin - (w[0] / 2)));
            t.topMargin = imgIndicator.getTop() - (int) (txtAmount.getHeight());
            t.width = w[0];
            t.height = (int) (txtAmount.getMeasuredHeight());

            imgToolTip.setLayoutParams(t);

            RelativeLayout.LayoutParams a = (RelativeLayout.LayoutParams) txtAmount.getLayoutParams();
            a.leftMargin = p.leftMargin - w[0] / 2;
            a.topMargin = (int) (t.topMargin * .75);

            txtAmount.setLayoutParams(a);


            //toolTip1.setBackgroundDrawable(new BitmapDrawable(getTooTip(w[0], (int) (txtAmount.getMeasuredHeight() * 1.5))));

            RelativeLayout.LayoutParams d = (RelativeLayout.LayoutParams) txtDate.getLayoutParams();
            d.leftMargin = p.leftMargin + imgIndicator.getWidth() / 2 - w[1] / 2;
            d.topMargin = imgIndicator.getTop() + txtDate.getHeight();
            txtDate.setLayoutParams(d);
        }
    }

}
