package com.bancomer.apicheckupfinanciero.gui.Delegates;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.os.Build;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.controller.InitCheckUp;
import com.bancomer.apicheckupfinanciero.gui.views.activities.MainActivity;
import com.bancomer.apicheckupfinanciero.gui.views.fragments.CreditPaymentsFragment;
import com.bancomer.apicheckupfinanciero.io.Server;
import com.bancomer.apicheckupfinanciero.io.ServerResponseImpl;
import com.bancomer.apicheckupfinanciero.models.PagoCreditos;
import com.bancomer.apicheckupfinanciero.utils.Tools;

import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by DMEJIA on 01/06/16.
 */
public class CreditPaymentsDelegate extends BaseDelegateImp{

    private CreditPaymentsFragment fragment;


    public CreditPaymentsDelegate(CreditPaymentsFragment fragment){
        this.fragment = fragment;
    }

    public void setColorGraphic(ImageView indicator, ImageView line, ImageView icon){
        int color = Tools.getIntColor(((MainActivity)fragment.getActivity()).getDelegate().getStatus(),fragment.getActivity());
        indicator.clearColorFilter();
        line.clearColorFilter();
        icon.clearColorFilter();

        icon.setColorFilter(color);
        line.setColorFilter(color);
        indicator.setColorFilter(color);
    }

    public void positionViews(final TextView txtAmount, final TextView txtDate, final ImageView indicator, final ImageView toolTip,final float left){
        txtAmount.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) indicator.getLayoutParams();
                float margint = (5 * fragment.getActivity().getResources().getDisplayMetrics().scaledDensity);
               // p.leftMargin = (int) (left + ((v.getWidth() - margint * 2) / 30 * date[count]) + margint);
                //count++;
                indicator.setLayoutParams(p);

                final int[] w = new int[2];
                w[0] = (int) txtAmount.getWidth();
                w[1] = (int) txtDate.getWidth();

                if (w[0] > 0) {

                    RelativeLayout.LayoutParams t = (RelativeLayout.LayoutParams) toolTip.getLayoutParams();
                    t.leftMargin = (int) ((p.leftMargin - (w[0] / 2)));
                    t.topMargin = indicator.getTop() - (int) (txtAmount.getHeight());
                    t.width = w[0];
                    t.height = (int) (txtAmount.getMeasuredHeight());

                    toolTip.setLayoutParams(t);

                    RelativeLayout.LayoutParams a = (RelativeLayout.LayoutParams) txtAmount.getLayoutParams();
                    a.leftMargin = p.leftMargin - w[0] / 2;
                    a.topMargin = (int) (t.topMargin * .75);

                    txtAmount.setLayoutParams(a);


                    //toolTip1.setBackgroundDrawable(new BitmapDrawable(getTooTip(w[0], (int) (txtAmount.getMeasuredHeight() * 1.5))));

                    RelativeLayout.LayoutParams d = (RelativeLayout.LayoutParams) txtDate.getLayoutParams();
                    d.leftMargin = p.leftMargin + indicator.getWidth() / 2 - w[1] / 2;
                    d.topMargin = indicator.getTop() + txtDate.getHeight();
                    txtDate.setLayoutParams(d);

                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                        txtAmount.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        txtAmount.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }

                }


            }
        });
    }

    public void realizaOperacion(){
        doNetworkOperation(Server.CONSULTA_DETALLE_CREDITOS,null,this);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse serverResponse) {
        if(operationId == Server.CONSULTA_DETALLE_CREDITOS) {
            if (serverResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                PagoCreditos pagoCreditos = (PagoCreditos) serverResponse.getResponse();
                android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().size());
                if (pagoCreditos.getCreditCardLists().size() > 0) {
                    android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().get(0).getAmountMinimumPayment());
                    android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().get(0).getPreviousPeriodsArrayList().size());
                    android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().get(0).getProductBase().getErrorInfo());
                    android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().get(0).getAmountOutstandingDebt());
                    android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().get(0).getAmountPaymentWithoutInterest());
                    android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().get(0).getCardNumber());
                    android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().get(0).getPreviousPeriodsArrayList().get(0).getAmountMinimumPayment());
                    android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().get(0).getPreviousPeriodsArrayList().get(0).getAmountPaymentWithoutInterest());
                    android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().get(0).getPreviousPeriodsArrayList().get(0).getAmountOutstandingDebt());
                    android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().get(0).getPreviousPeriodsArrayList().get(0).getEndDate());
                    android.util.Log.e("pagocreditos", "1: " + pagoCreditos.getCreditCardLists().get(0).getPreviousPeriodsArrayList().get(0).getPaymentDate());
                }
                MainDelegate mainDelegate = ((MainActivity) fragment.getActivity()).getDelegate();
                        mainDelegate.setObjPagoCreditos(pagoCreditos);
                fragment.inflateList(mainDelegate);
            } else {
                ((MainActivity)fragment.getActivity()).getDelegate().setObjPagoCreditos(null);

                Tools.showErrorMessage(serverResponse.getMessageText(),
                        InitCheckUp.getInstance().getActivityController().getCurrentActivity(),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((MainActivity) fragment.getActivity()).setCurrentItem(0);
                            }
                        });
            }
        }
    }

    @Override
    public void doNetworkOperation(int i, Hashtable<String, ?> hashtable, BaseViewController baseViewController) {
        super.doNetworkOperation(i, hashtable, baseViewController);
    }

    @Override
    public void doNetworkOperation(int i, Hashtable<String, ?> hashtable, BaseDelegate baseViewController) {
       // InitCheckUp.getInstance().getBaseSubapplication().invokeNetworkOperation(i, hashtable, baseViewController,new PagoCreditos(), "Operación en curso", "Conectando con servidor remoto", false);

    }
}
