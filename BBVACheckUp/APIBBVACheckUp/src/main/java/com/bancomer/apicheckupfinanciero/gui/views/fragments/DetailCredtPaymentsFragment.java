package com.bancomer.apicheckupfinanciero.gui.views.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.controller.InitCheckUp;
import com.bancomer.apicheckupfinanciero.gui.Delegates.DetailDelegate;
import com.bancomer.apicheckupfinanciero.gui.Delegates.MainDelegate;
import com.bancomer.apicheckupfinanciero.gui.views.activities.MainActivity;
import com.bancomer.apicheckupfinanciero.gui.views.adapters.ListDetailCreditAdapter;
import com.bancomer.apicheckupfinanciero.models.PreviousPeriods;
import com.bancomer.apicheckupfinanciero.utils.CustomerTextView;
import com.bancomer.apicheckupfinanciero.utils.ScaleTool;
import com.bancomer.apicheckupfinanciero.utils.Tools;

import bancomer.api.common.timer.TimerController;

/**
 * Created by DMEJIA on 05/05/16.
 */
@SuppressLint("ValidFragment")
public class DetailCredtPaymentsFragment extends DialogFragment implements View.OnClickListener{

    private View rootView;
    private LinearLayout lnPrincipal;
    private ListView lsDetail;

    private RelativeLayout rlItemLine;

    private ImageView imgLine;
    private ImageView indicator;
    private ImageView toolTip;
    private ImageView imgLoands;
    private ImageView imgLogo;
    private ImageView imgClose;

    private TextView txtAmount;
    private TextView txtDate;
    private CustomerTextView txtStatus;


    private String amount;
    private String date;
    private String status;

    private int day;
    private int totalDays;
    private Drawable icon;

    private boolean spend;
    private Fragment context;
    private DetailDelegate delegate;
    private PreviousPeriods previousPeriods;

    public DetailCredtPaymentsFragment(String amount, String date, String status, Drawable icon, int day, int totalDays, boolean spend, Fragment context, PreviousPeriods previousPeriods){
        this.amount = amount;
        this.date = date;
        this.day = day;
        this.icon = icon;
        this.status = status;
        this.totalDays = totalDays;
        this.spend = spend;
        this.context = context;
        this.previousPeriods = previousPeriods;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_detal_credit, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        delegate = (DetailDelegate) InitCheckUp.getInstance().getParentManager().getBaseDelegateForKey(DetailDelegate.DETAIL_DELEGATE);
        init();
        return rootView;
    }

    private void init(){
        findViewById();
       // scale();
        setDataLine();
        inflateList();
    }

    private void findViewById(){
        lnPrincipal = (LinearLayout)rootView.findViewById(R.id.lnPrincipalDetailCredit);
        lnPrincipal.setOnClickListener(this);
        lsDetail = (ListView)rootView.findViewById(R.id.lsDetail);

        txtAmount = (TextView) rootView.findViewById(R.id.txt_amount);

        txtDate = (TextView) rootView.findViewById(R.id.txt_date);

        txtStatus = (CustomerTextView) rootView.findViewById(R.id.txtStatus);

        imgLogo = (ImageView) rootView.findViewById(R.id.imgLogoDetail);
        imgClose = (ImageView) rootView.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);

        indicator = (ImageView) rootView.findViewById(R.id.indicator);

        toolTip = (ImageView) rootView.findViewById(R.id.img_tool_tip);

        imgLine = (ImageView) rootView.findViewById(R.id.lineTime);

        String g = "3";
        imgLoands = (ImageView) rootView.findViewById(R.id.imgLoands);


        rlItemLine = (RelativeLayout) rootView.findViewById(R.id.rlItemLine);
        imgLine.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final int[] w = new int[2];

                        RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) indicator.getLayoutParams();
                        float margint = (5 * getResources().getDisplayMetrics().scaledDensity);
                        p.leftMargin = (int) ((imgLine.getLeft() + ((imgLine.getWidth() - margint * 2) /totalDays * day) + margint));
                        indicator.setLayoutParams(p);

                        w[0] = (int) txtAmount.getWidth();
                        w[1] = (int) txtDate.getWidth();


                        if (w[0] > 0) {

                            RelativeLayout.LayoutParams t = (RelativeLayout.LayoutParams) toolTip.getLayoutParams();
                            t.leftMargin = (int) ((p.leftMargin - (w[0] / 2)));
                            t.topMargin = (int) (indicator.getTop() - (txtAmount.getHeight() * 1.2));
                            t.width = w[0];
                            t.height = (int) (txtAmount.getMeasuredHeight() * 1.2);

                            toolTip.setLayoutParams(t);

                            RelativeLayout.LayoutParams a = (RelativeLayout.LayoutParams) txtAmount.getLayoutParams();
                            a.leftMargin = p.leftMargin - w[0] / 2;
                            a.topMargin = (t.topMargin);

                            txtAmount.setLayoutParams(a);


                            //toolTip1.setBackgroundDrawable(new BitmapDrawable(getTooTip(w[0], (int) (txtAmount.getMeasuredHeight() * 1.5))));

                            if (!date.equals("")) {
                                RelativeLayout.LayoutParams d = (RelativeLayout.LayoutParams) txtDate.getLayoutParams();
                                d.leftMargin = p.leftMargin + indicator.getWidth() / 2 - w[1] / 2;
                                d.topMargin = indicator.getTop() + txtDate.getHeight();
                                txtDate.setLayoutParams(d);
                            } else
                                txtDate.setVisibility(View.GONE);

                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                                imgLine.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            } else {
                                imgLine.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            }

                        }
            }
        });
        //lnDetail.getResources();
    }

    private void scale(){
        ScaleTool scaleTool = new ScaleTool(getActivity());
        scaleTool.scaleWidth(lnPrincipal, 400);
    }

    private void inflateList(){
        ListDetailCreditAdapter adapter = new ListDetailCreditAdapter(getActivity(), spend?getResources().getStringArray(R.array.array_prueba_spend):getResources().getStringArray(R.array.array_prueba),previousPeriods);
        lsDetail.setAdapter(adapter);

        lsDetail.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TimerController.getInstance().resetTimer();
            }
        });
    }


    private void setDataLine(){
        int color;
        if(spend)
            color = Tools.getIntColor(status, getActivity());
        else
            color = Tools.getIntColor(((MainActivity)getActivity()).getDelegate().getStatus(), getActivity());

        setLogoDetail();
        txtAmount.setText(amount);
        txtDate.setText(date);
        txtStatus.setText(status);
        txtStatus.setTextColor(color);
        indicator.clearColorFilter();
        indicator.setColorFilter(color);
        imgLine.clearColorFilter();
        imgLine.setColorFilter(color);
        imgLoands.setImageDrawable(icon);
        imgLoands.clearColorFilter();
        imgLoands.setColorFilter(color);

    }
    @Override
    public void onClick(View v) {
        if(lnPrincipal == v){
            TimerController.getInstance().resetTimer();
        }
        else {
            getDialog().dismiss();

            if(context instanceof CreditPaymentsFragment){
                ((CreditPaymentsFragment) context).setShowDetail(true);
            }else if(context instanceof  ExpensesFragment){
                ((ExpensesFragment) context).setShowDetail(true);
            }
        }
    }

    private void setLogoDetail() {
        Resources res = getActivity().getResources();
        Bitmap auto = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_pop_auto_ver))).getBitmap();
        Bitmap hipo = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_pop_hipo_ver))).getBitmap();
        Bitmap nomi = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_pop_nomi_ver))).getBitmap();
        Bitmap pers = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_pop_pers_ver))).getBitmap();
        Bitmap tarcre = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_pop_tarcre_ver))).getBitmap();
        Bitmap compra = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_pop_compra_ver))).getBitmap();

        Bitmap auto1 = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_auto_ver))).getBitmap();
        Bitmap hipo1 = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_hipo_ver))).getBitmap();
        Bitmap nomi1 = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_nomi_ver))).getBitmap();
        Bitmap pers1 = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_pers_ver))).getBitmap();
        Bitmap tarcre1 = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_tarcre_ver))).getBitmap();
        Bitmap compra1 = ((BitmapDrawable) (res.getDrawable(R.drawable.ico_compra_ver))).getBitmap();

        Drawable logo = null;
        if (((BitmapDrawable) icon).getBitmap() == auto || ((BitmapDrawable) icon).getBitmap() == auto1) {
            logo = res.getDrawable(R.drawable.pres_auto);
            icon = (res.getDrawable(R.drawable.ico_pop_auto_ver));
        } else if (((BitmapDrawable) icon).getBitmap() == hipo || ((BitmapDrawable) icon).getBitmap() == hipo1) {
            logo = res.getDrawable(R.drawable.pres_hipo);
            icon = (res.getDrawable(R.drawable.ico_pop_hipo_ver));
        } else if (((BitmapDrawable) icon).getBitmap() == nomi || ((BitmapDrawable) icon).getBitmap() == nomi1){
            logo = res.getDrawable(R.drawable.pres_nomina);
            icon = (res.getDrawable(R.drawable.ico_pop_nomi_ver));
        }else if(((BitmapDrawable)icon).getBitmap() == pers || ((BitmapDrawable)icon).getBitmap() == pers1) {
            logo = res.getDrawable(R.drawable.pres_personal);
            icon = (res.getDrawable(R.drawable.ico_pop_pers_ver));
        }else if(((BitmapDrawable)icon).getBitmap() == tarcre || ((BitmapDrawable)icon).getBitmap() == tarcre1) {
            logo = res.getDrawable(R.drawable.tarjeta_credito);
            icon = (res.getDrawable(R.drawable.ico_pop_tarcre_ver));
        }else if(((BitmapDrawable)icon).getBitmap() == compra || ((BitmapDrawable)icon).getBitmap() == compra1) {
            logo = res.getDrawable(R.drawable.compra_per);
            icon = (res.getDrawable(R.drawable.ico_pop_compra_ver));
        }
        if(logo != null){
            android.util.Log.e("o_o", "si tiene una imagen xD");
        }
        imgLogo.setBackgroundDrawable(logo);

    }
}
