package com.bancomer.apicheckupfinanciero.gui.Delegates;

import com.bancomer.apicheckupfinanciero.gui.views.fragments.DetailCredtPaymentsFragment;
import com.bancomer.apicheckupfinanciero.io.ServerResponseImpl;

import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;

/**
 * Created by DMEJIA on 20/06/16.
 */
public class DetailDelegate extends BaseDelegateImp{
    public static final long DETAIL_DELEGATE = 201606204566765659L;

    private DetailCredtPaymentsFragment fragment;

    public DetailDelegate(DetailCredtPaymentsFragment fragment){
        this.fragment = fragment;
    }

    public void realizaOperacion(){

    }

    @Override
    public void analyzeResponse(String operationId, ServerResponseImpl response) {
        super.analyzeResponse(operationId, response);
    }

    @Override
    public void doNetworkOperation(int i, Hashtable<String, ?> hashtable, BaseViewController baseViewController) {
        super.doNetworkOperation(i, hashtable, baseViewController);
    }

    @Override
    public void doNetworkOperation(int i, Hashtable<String, ?> hashtable, BaseDelegate baseViewController) {
        super.doNetworkOperation(i, hashtable, baseViewController);
    }
}
