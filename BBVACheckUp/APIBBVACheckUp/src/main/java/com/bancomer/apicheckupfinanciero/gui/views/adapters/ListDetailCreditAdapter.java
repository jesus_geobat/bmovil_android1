package com.bancomer.apicheckupfinanciero.gui.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bancomer.apicheckupfinanciero.R;
import com.bancomer.apicheckupfinanciero.models.PreviousPeriods;
import com.bancomer.apicheckupfinanciero.utils.CustomerTextView;

/**
 * Created by DMEJIA on 12/05/16.
 */
public class ListDetailCreditAdapter extends BaseAdapter{

    private Context context;

    private CustomerTextView txtHeader;
    private CustomerTextView txtDescription;

    private String [] arrayHeaders;
    private String [] arrayDescription;
    private PreviousPeriods previousPeriods;

    public ListDetailCreditAdapter(Context context, String [] arrayDescription, PreviousPeriods previousPeriods){

        arrayHeaders = arrayDescription.length == 4 ?context.getResources().getStringArray(R.array.array_spend):context.getResources().getStringArray(R.array.array_tdc_titles);
        this.arrayDescription = arrayDescription;
        this.context = context;
        this.previousPeriods = previousPeriods;

        if(previousPeriods != null)
            getData();
    }

    private void getData(){
        arrayDescription = new String[6];
        arrayDescription[0] = previousPeriods.getAmountOutstandingDebt();
        arrayDescription[1] = previousPeriods.getAmountMinimumPayment();
        arrayDescription[2] = previousPeriods.getAmountPaymentWithoutInterest();
        arrayDescription[3] = previousPeriods.getAmountOutstandingDebt();
        arrayDescription[4] = previousPeriods.getEndDate();
        arrayDescription[5] = previousPeriods.getPaymentDate();

    }

    @Override
    public int getCount() {
        return arrayHeaders.length;
    }

    @Override
    public Object getItem(int position) {
        return arrayHeaders[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.layout_item_lista_detalle,
                    null);
        }

        txtHeader = (CustomerTextView) convertView.findViewById(R.id.txtHeader);
        txtDescription = (CustomerTextView) convertView.findViewById(R.id.txtDescription);

        txtHeader.setText(arrayHeaders[position]);
        txtDescription.setText(arrayDescription[position]);
        return convertView;
    }
}
