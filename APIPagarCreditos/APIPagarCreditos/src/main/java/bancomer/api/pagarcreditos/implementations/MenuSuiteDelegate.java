package bancomer.api.pagarcreditos.implementations;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.bancomer.mbanking.softtoken.SuiteAppApi;

import bancomer.api.pagarcreditos.R;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.common.PropertiesManager;

//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.LoginDelegate;

public class MenuSuiteDelegate extends BaseDelegate {

    public final static long MENU_SUITE_DELEGATE_ID = 0x3329474451002cd6L;

    private MenuSuiteViewController menuSuiteViewController;

    private boolean isCallActive;
    private boolean bMovilSelected;

    public MenuSuiteDelegate() {
    }

    public boolean isbMovilSelected() {
        return bMovilSelected;
    }

    public void setbMovilSelected(boolean bMovilSelected) {
        this.bMovilSelected = bMovilSelected;
    }

    public boolean isCallActive() {
        return isCallActive;
    }

    public void setCallActive(boolean isCallActive) {
        this.isCallActive = isCallActive;
    }

    public boolean isDisconnected() {
        SuiteAppPagoCreditoApi suiteApp = SuiteAppPagoCreditoApi.getInstance();

        ConnectivityManager connectivity = (ConnectivityManager)
                suiteApp.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void startBmovilApp() {
        if (SuiteAppPagoCreditoApi.getInstance().getBmovilApplication() == null) {
            SuiteAppPagoCreditoApi.getInstance().startBmovilApp();
        }
    }

    public int getBmovilAppStatus(SuiteAppPagoCreditoApi suiteApp) {
        return suiteApp.getBmovilApplication().getApplicationStatus();
    }

    public MenuSuiteViewController getMenuSuiteViewController() {
        return menuSuiteViewController;
    }

    public void setMenuSuiteViewController(MenuSuiteViewController menuSuiteViewController) {
        this.menuSuiteViewController = menuSuiteViewController;
    }

    public void bmovilSelected() {

        //EA#10, RN8 y RN9: comprobar la conectividad a internet antes de acceder a la aplicación
        if (this.isDisconnected()) {
            menuSuiteViewController.setButtonsDisabled(false);
            menuSuiteViewController.showInformationAlert(R.string.menuSuite_alert_disconnected);

        } else {
            startBmovilApp();
            // Contin�a el flujo normal del escenario principal
            // Modificacion P026 Suite
            if (existePendienteDescarga()) {

                showActivacionSTEA12();

            } else {

                if (buscarBanderasBmovil()) {

                    showActivacionSTEP();

                } else {
                    if (PropertiesManager.getCurrent().getBmovilActivated()) {
//						showLogin();
                    } else {
                        consultaEstatusBmovil();
                    }
                }
            }
        }
    }

    /**
     * Devuelve si existe el archivo PendienteDescarga
     *
     * @return
     */
    private boolean existePendienteDescarga() {
        boolean respuesta = false;
        SofttokenActivationBackupManager manager = SofttokenActivationBackupManager
                .getCurrent();
        if (manager.existsABackup()) {
            respuesta = true;
        }
        return respuesta;
    }

    private void showActivacionSTEA12() {
        cambiarDeBMovilTokenMovil().showActivacionSTEA12();
    }

    /**
     * Redirige al caso de uso ActivacionST escenario principal
     */
    private void showActivacionSTEP() {
        cambiarDeBMovilTokenMovil().showPantallaIngresoDatos();
    }

	/*private void showActivacionST(){
		cambiarDeBMovilTokenMovil().showActivacionSTEP25();
	}*/

    private SofttokenViewsController cambiarDeBMovilTokenMovil() {
        SofttokenViewsController viewsController = SuiteAppApi.getInstanceApi()
                .getSofttokenApplicationApi().getSottokenViewsController();
        menuSuiteViewController.setParentViewsController(viewsController);
        viewsController.setCurrentActivityApp(menuSuiteViewController);

        return viewsController;
    }

    private boolean buscarBanderasBmovil() {
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);

        if (session.getUsername() == null || Constants.EMPTY_STRING.equals(session.getUsername())) {
            PropertiesManager.getCurrent().setBmovilActivated(false);
        }
        boolean banderaBmovil = Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CONTRATAR2x1));
        return banderaBmovil;
    }

    private void consultaEstatusBmovil() {
        BmovilApp bmovilApp = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication();
        BmovilViewsController bmovilViewsController = bmovilApp.getBmovilViewsController();
        bmovilViewsController.setCurrentActivityApp(menuSuiteViewController);
//		bmovilViewsController.showConsultaEstatusAplicacionDesactivada();
    }

    public void leerContratacion() {
        int pendingStatus = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getPendingStatus();

        if (pendingStatus > 0) {
        } else {
//			showLogin();
        }
    }

//	private void showLogin() {
//		LoginDelegate loginDelegate = (LoginDelegate) SuiteApp.getInstance()
//				.getBmovilApplication().getBmovilViewsController()
//				.getBaseDelegateForKey(LoginDelegate.LOGIN_DELEGATE_ID);
//		if (loginDelegate == null) {
//			loginDelegate = new LoginDelegate();
//			SuiteApp.getInstance()
//					.getBmovilApplication()
//					.getBmovilViewsController()
//					.addDelegateToHashMap(LoginDelegate.LOGIN_DELEGATE_ID,
//							loginDelegate);
//		}
//
//		// LoginDelegate loginDelegate = new LoginDelegate();
//		BmovilApp bmovilApp = SuiteApp.getInstance().getBmovilApplication();
//		BmovilViewsController bmovilViewsController = bmovilApp
//				.getBmovilViewsController();
//		bmovilViewsController.setCurrentActivityApp(menuSuiteViewController);
//		bmovilViewsController.addDelegateToHashMap(
//				LoginDelegate.LOGIN_DELEGATE_ID, loginDelegate);
//		bmovilViewsController.showLogin();
//	}

    public void llamarLineaBancomer(String numeroTel) {
        try {
            isCallActive = true;
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse(Constants.TEL_URI + numeroTel));
            menuSuiteViewController.startActivity(callIntent);
        } catch (ActivityNotFoundException e) {
            menuSuiteViewController.showErrorMessage(menuSuiteViewController.getString(R.string.menuSuite_callErrorMessage));
        }
    }

    // #region Softtoken.
    public void softtokenSelected() {
//		if(null == SuiteAppApi.getInstanceApi().getSofttokenApplicationApi())
//			SuiteAppAdmonApi.getInstance().startSofttokenApp();
//		
//		SofttokenViewsController viewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
//		if(SuiteAppAdmonApi.getSofttokenStatus()) {
//			menuSuiteViewController.restableceMenu();
//			viewsController.setCurrentActivityApp(menuSuiteViewController);
//			viewsController.showPantallaGeneraOTPST(false);
//		} else {
//			viewsController.showContratacionSotfttoken(menuSuiteViewController);
//		}
    }

    public void leerContratacionST() {

    }
    // #endregion

    public void onBackPressed() {
//		if(menuSuiteViewController.getLoginViewController() != null && 
//		   menuSuiteViewController.getLoginViewController().getVisibility() == View.VISIBLE) {
//			menuSuiteViewController.plegarOpcion();
//		} else if(menuSuiteViewController.getAplicacionDesactivadaViewController() != null && 
//				  menuSuiteViewController.getAplicacionDesactivadaViewController().getVisibility() == View.VISIBLE) {
//			menuSuiteViewController.plegarOpcionAplicacionDesactivada();
//		} else 
        if (menuSuiteViewController.getContratacionSTViewController() != null &&
                menuSuiteViewController.getContratacionSTViewController().getVisibility() == View.VISIBLE) {
            menuSuiteViewController.plegarOpcionST();
        } else {
            SuiteAppPagoCreditoApi.getInstance().cierraAplicacionSuite();
        }
    }


    /**
     * Se selecciona la opci�n de men� Token M�vil.
     *
     * @param sender la opci�n seleccionada
     */
    public void onBtnContinuarclick(View sender) {
//		SofttokenApp softtokenApp = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi();
//
//		SofttokenViewsController viewsController = softtokenApp.getSottokenViewsController();
//		if (SuiteAppAdmonApi.getSofttokenStatus()) {
//			menuSuiteViewController.restableceMenu();
//			viewsController.setCurrentActivityApp(menuSuiteViewController);
//			viewsController.showPantallaGeneraOTPST(false);
//		} else {
//			viewsController.setCurrentActivityApp(menuSuiteViewController);
//			
//			SofttokenActivationBackupManager manager = SofttokenActivationBackupManager
//					.getCurrent();
//
//			if (manager.existsABackup()) {
//				softtokenApp.getSottokenViewsController()
//						.showPantallaCActivacionST(true);
//			} else {
//				softtokenApp.getSottokenViewsController()
//						.showPantallaIngresoDatos(menuSuiteViewController);
//			}
//			
//			//EA#10, RN8 y RN9: comprobar la conectividad a internet antes de acceder a la aplicación
//			if (this.isDisconnected()) {
//				menuSuiteViewController.setButtonsDisabled(false);
//				menuSuiteViewController.showInformationAlert(R.string.menuSuite_alert_disconnected);
//				
//			} else {
//				// Contin�a el flujo normal del EA#2: paso 6
//				if(existePendienteDescarga()) {
//					showActivacionSTEA12();
//				} else {
//					showActivacionSTEP();
//				}
//			}
//		}
    }

    /**
     * Metodo para cargar datos implementacion P026 BConnect EA#9, EA#10, EA#11, EA#12,EA#13
     */
    public void cargaTelSeedKeystore() {

        // Recoger sesion del contexto
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        // Recoger KeyStoreManager
//		KeyStoreManager keySMan = session.getKeyStoreManager();
        KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();

        // Inicializa Telefono y seed
        String telefono = null;
        String seed = null;

        try {
            // Telefono e IUM de KeyStore
            telefono = kswrapper.getUserName();
            seed = kswrapper.getSeed();

            if (Server.ALLOW_LOG) Log.i("Key", "BConnect MenuSuiteDelegate Username: " + telefono);
            if (Server.ALLOW_LOG) Log.i("Key", "BConnect MenuSuiteDelegate Seed: " + seed);

        } catch (Exception e1) {
            // TODO Auto-generated catch block
            if (Server.ALLOW_LOG) e1.printStackTrace();
        }

        // Comprobar Bmovil en estatusAplicaciones
        if (PropertiesManager.getCurrent().getBmovilActivated()) {

            // Si bmovil activado, validar telefono e IUM en KeyStore
            if (!Tools.validaSeed(seed) || !Tools.validaTelefono(telefono)) {


                // Si datos no validos, comprueba en archivo datosBmovil
                DatosBmovilFileManager datosBmovil = DatosBmovilFileManager
                        .getCurrent();
                if (Server.ALLOW_LOG) Log.i("BConnect", "datosBmovil " + datosBmovil);
                seed = datosBmovil.getSeedStr();
                telefono = datosBmovil.getLogin();
                if (Server.ALLOW_LOG) Log.i("DatosBmovil", "seed " + seed);
                if (Server.ALLOW_LOG) Log.i("DatosBmovil", "telefono " + telefono);
                // Validar telefono y seed
                if (Tools.validaSeed(seed) && Tools.validaTelefono(telefono)) {
                    // EA#10
                    try {
                        // Copia telefono y seed a keyStore

                        if (Server.ALLOW_LOG) Log.i("Key",
                                "BConnect MenuSuiteDelegate SET Username: "
                                        + telefono);
                        if (Server.ALLOW_LOG) Log.i("Key", "BConnect MenuSuiteDelegate SET Seed: "
                                + seed);
                        kswrapper.setUserName(telefono);
                        kswrapper.setSeed(seed);
                        kswrapper.storeValueForKey(Constants.CENTRO, Constants.BMOVIL);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        if (Server.ALLOW_LOG) e.printStackTrace();
                    }

                    // No borrar ambos datos de datosBmovil EA310 Paso 5
                    //datosBmovil.setSeed("");
                    //datosBmovil.setLogin("");

                } else {
                    // Si no se valida, cambia a bmovil = false EA#11
                    PropertiesManager.getCurrent().setBmovilActivated(false);
                }
            }
        } else {
            // Si Bmovil desactivado o no existe estatusAplicaciones
            // Validar telefono y seed EA#12
            if (Tools.validaSeed(seed) && Tools.validaTelefono(telefono)) {
                // Si los datos son validos borra datos en keychain
                try {
                    kswrapper.setUserName(" ");
                    kswrapper.setSeed(" ");
                    kswrapper.storeValueForKey(Constants.CENTRO, " ");
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    if (Server.ALLOW_LOG) e.printStackTrace();
                }
            }
        }
    }
}
