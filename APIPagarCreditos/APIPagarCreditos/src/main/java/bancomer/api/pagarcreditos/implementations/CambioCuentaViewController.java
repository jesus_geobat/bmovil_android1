package bancomer.api.pagarcreditos.implementations;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.base.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.commons.GuiTools;
import bancomer.api.pagarcreditos.commons.TrackingHelper;
import bancomer.api.pagarcreditos.gui.commons.controllers.ListaSeleccionViewController;
import bancomer.api.pagarcreditos.models.CambioCuenta;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

public class CambioCuentaViewController extends BaseViewController {

    CambioCuentaDelegate delegate;
    CambioCuenta cambioCuenta;
    LinearLayout contenedorPrincipal;
    LinearLayout contenedorCuentas;
    ListaSeleccionViewController listaCuentas;
    //AMZ
    private BmovilViewsController parentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppPagoCreditoApi.getResourceId("layout_bmovil_cambio_cuenta_asociada_admon", "layout"));
        SuiteAppPagoCreditoApi.getInstance()
                .getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this);
        SuiteAppPagoCreditoApi.appContext = this;
        SuiteApp.appContext = this;
        setTitle(R.string.bmovil_cambio_cuenta_title, R.drawable.bmovil_cambiocuenta_icono);
        //AMZ
        parentManager = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("camb cuenta", parentManager.estados);

        setParentViewsController(SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController());
        setDelegate((CambioCuentaDelegate) parentViewsController.getBaseDelegateForKey(CambioCuentaDelegate.CAMBIO_CUENTA_DELEGATE_ID));
        delegate = (CambioCuentaDelegate) getDelegate();
        delegate.setViewController(this);
        if (cambioCuenta == null)
            cambioCuenta = new CambioCuenta();
        findViews();
        scaleForCurrentScreen();
        inicializarPantalla();
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SuiteApp.appContext = this;
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

    @Override
    public void onBackPressed() {
        parentViewsController.removeDelegateFromHashMap(CambioCuentaDelegate.CAMBIO_CUENTA_DELEGATE_ID);
        super.onBackPressed();
    }

    /**
     *
     */
    private void findViews() {
        contenedorPrincipal = (LinearLayout) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_cuenta_contenedor_principal", "id"));
        contenedorCuentas = (LinearLayout) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_cuenta_contenedor_tarjeta", "id"));


    }

    /**
     * Escala la vistas en base a la pantalla donde se muestra
     */
    private void scaleForCurrentScreen() {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());

        gTools.scale(contenedorPrincipal);
        gTools.scale(contenedorCuentas);
        //gTools.scale(findViewById(R.id.cambio_cuenta_btnContinuar));
    }

    /**
     * Inicializacion de componentes con la lista de cuentas del usuario
     */
    @SuppressWarnings("deprecation")
    private void inicializarPantalla() {
        LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        listaCuentas = new ListaSeleccionViewController(this, params, parentViewsController);
        ArrayList<Object> cuentasAMostrar = delegate.getCuentasUsuario();
        listaCuentas.setDelegate(delegate);
        listaCuentas.setNumeroColumnas(2);
        listaCuentas.setLista(cuentasAMostrar);
        listaCuentas.setOpcionSeleccionada(0);
        listaCuentas.setSeleccionable(false);
        listaCuentas.setAlturaFija(false);
        listaCuentas.setNumeroFilas(cuentasAMostrar.size());
        listaCuentas.setExisteFiltro(false);
        listaCuentas.setTitle(getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.cambio.cuenta.selecciona", "string")));
        listaCuentas.cargarTabla();
        contenedorCuentas.addView(listaCuentas);
    }

    /**
     * Retorna el objeto CambioCuenta con la cuenta seleccionada por el usuario
     *
     * @return cambioCuenta
     */
    public CambioCuenta getCambioCuenta() {
        return cambioCuenta;
    }


    /**
     * Realiza el set de la cuenta seleccionada al Objeto CambioCuenta
     * e invoca la confirmacion de la operacion
     */
    public void actualizarCuenta(Object cuenta) {
        Account account = (Account) cuenta;
        cambioCuenta.setAccount(account);
        //AMZ
        Map<String, Object> Paso1OperacionMap = new HashMap<String, Object>();
        //AMZ
        Paso1OperacionMap.put("evento_paso1", "event46");
        Paso1OperacionMap.put("&&products", "operaciones;admin+cambio cuenta");
        Paso1OperacionMap.put("eVar12", "paso1:cambio cuenta");
        TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
        showConfirmacion();
    }

    /**
     * Invoca mostrar la confirmacion al delegate de operacion
     */
    private void showConfirmacion() {
        delegate.showConfirmacion();
    }

    /**
     *
     */
    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }
}
