package bancomer.api.pagarcreditos.models;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

public class ResultadoPagoCreditoData implements ParsingHandler {

    private static final long serialVersionUID = 1L;

    private String folio;
    private String cuentaCargo;
    private String saldoCuenta;
    private String fechaPago;
    private String horaPago;
    private String numRec;
    private String pagoRequerido;
    private String pagoReqMoneda;
    private String pagoAnticipo;
    private String pagoAntiMoneda;
    private String pagoTotal;
    private String pagoTotalMoneda;

    public ResultadoPagoCreditoData() {
        this.folio = "";
        this.cuentaCargo = "";
        this.saldoCuenta = "";
        this.fechaPago = "";
        this.horaPago = "";
        this.numRec = "";
        this.pagoRequerido = "";
        this.pagoReqMoneda = "";
        this.pagoAnticipo = "";
        this.pagoAntiMoneda = "";
        this.pagoTotal = "";
        this.pagoTotalMoneda = "";
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCuentaCargo() {
        return cuentaCargo;
    }

    public void setCuentaCargo(String cuentaCargo) {
        this.cuentaCargo = cuentaCargo;
    }

    public String getSaldoCuenta() {
        return saldoCuenta;
    }

    public void setSaldoCuenta(String saldoCuenta) {
        this.saldoCuenta = saldoCuenta;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getHoraPago() {
        return horaPago;
    }

    public void setHoraPago(String horaPago) {
        this.horaPago = horaPago;
    }

    public String getNumRec() {
        return numRec;
    }

    public void setNumRec(String numRec) {
        this.numRec = numRec;
    }

    public String getPagoRequerido() {
        return pagoRequerido;
    }

    public void setPagoRequerido(String pagoRequerido) {
        this.pagoRequerido = pagoRequerido;
    }

    public String getPagoReqMoneda() {
        return pagoReqMoneda;
    }

    public void setPagoReqMoneda(String pagoReqMoneda) {
        this.pagoReqMoneda = pagoReqMoneda;
    }

    public String getPagoAnticipo() {
        return pagoAnticipo;
    }

    public void setPagoAnticipo(String pagoAnticipo) {
        this.pagoAnticipo = pagoAnticipo;
    }

    public String getPagoAntiMoneda() {
        return pagoAntiMoneda;
    }

    public void setPagoAntiMoneda(String pagoAntiMoneda) {
        this.pagoAntiMoneda = pagoAntiMoneda;
    }

    public String getPagoTotal() {
        return pagoTotal;
    }

    public void setPagoTotal(String pagoTotal) {
        this.pagoTotal = pagoTotal;
    }

    public String getPagoTotalMoneda() {
        return pagoTotalMoneda;
    }

    public void setPagoTotalMoneda(String pagoTotalMoneda) {
        this.pagoTotalMoneda = pagoTotalMoneda;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub
        try {
            this.folio = parser.parseNextValue("folio");
            this.cuentaCargo = parser.parseNextValue("cuentaCargo");
            this.saldoCuenta = parser.parseNextValue("saldoCuenta");
            this.fechaPago = parser.parseNextValue("fechaPago");
            this.horaPago = parser.parseNextValue("horaPago");
            this.numRec = parser.parseNextValue("numRec");
            this.pagoRequerido = parser.parseNextValue("pagoRequerido");
            this.pagoReqMoneda = parser.parseNextValue("pagoReqMoneda");
            this.pagoAnticipo = parser.parseNextValue("pagoAnticipo");
            this.pagoAntiMoneda = parser.parseNextValue("pagoAntiMoneda");
            this.pagoTotal = parser.parseNextValue("pagoTotal");
            this.pagoTotalMoneda = parser.parseNextValue("pagoTotalMoneda");
        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
        }
    }
}
