package bancomer.api.pagarcreditos.implementations;

import android.content.BroadcastReceiver;

import com.bancomer.base.SuiteApp;

import java.util.Hashtable;

import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.gui.delegates.PagarCreditoDelegate;
import bancomer.api.pagarcreditos.models.PagoDeCredito;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;


public class ResultadosDelegate extends DelegateBaseOperacion {

    public final static long RESULTADOS_DELEGATE_ID = 0x1ef4f7e61ca112bfL;

    private DelegateBaseOperacion operationDelegate;
    private int listaOpcionesMenu;
    //One Click
    private boolean isSMS = false;
    private boolean isEmail = false;
    //Termina One Click
    private Boolean frecOpOK = false;

    /**
     * The BroadcastReceiver that we use to listen for the notification back.
     */
    private BroadcastReceiver mBroadcastReceiver;

    private ResultadosViewController resultadosViewController;

    public ResultadosDelegate(DelegateBaseOperacion operationDelegate) {
        this.operationDelegate = operationDelegate;
        listaOpcionesMenu = operationDelegate.getOpcionesMenuResultados();
    }

    public Boolean getFrecOpOK() {
        return frecOpOK;
    }

    public DelegateBaseOperacion getOperationDelegate() {
        return operationDelegate;
    }

    public void setResultadosViewController(ResultadosViewController viewController) {
        this.resultadosViewController = viewController;
    }

    public BroadcastReceiver getmBroadcastReceiver() {
        return mBroadcastReceiver;
    }

    public void setmBroadcastReceiver(BroadcastReceiver mBroadcastReceiver) {
        this.mBroadcastReceiver = mBroadcastReceiver;
    }

    public void consultaDatosLista() {
        resultadosViewController.setListaDatos(operationDelegate.getDatosTablaResultados());
        if (operationDelegate instanceof PagarCreditoDelegate) {
            PagoDeCredito pagoCredito = ((PagarCreditoDelegate) operationDelegate).getPagoCreditoData();
            if (pagoCredito.getTipoPago().equals("LI")) {
                String texto = "";
                if (pagoCredito.getTipoCredito().equals("H")) {
                    texto = "¡FELICIDADES! \n" +
                            "Liquidaste tu crédito, comunícate al 01800 - 122 6630 para iniciar el trámite de liberación de tu hipoteca";
                } else if (pagoCredito.getTipoCredito().equals("P") || pagoCredito.getTipoCredito().equals("C") || pagoCredito.getTipoCredito().equals("N")) {
                    if (Session.getInstance(SuiteApp.appContext).getEmail() != null && !Session.getInstance(SuiteApp.appContext).getEmail().equals("")) {
                        texto = "¡FELICIDADES! \n" +
                                "Liquidaste tu crédito, en breve enviaremos la carta de No Adeudo a tu correo electrónico\n";
                    } else {
                        texto = "¡FELICIDADES! \n" +
                                "Liquidaste tu crédito, Bancomer enviará a tu domicilio la carta de No Adeudo\n";
                    }
                }
                resultadosViewController.showInformationAlert(texto);
            }
        }
    }

    public DelegateBaseOperacion consultaOperationDelegate() {
        return operationDelegate;
    }

    public void enviaPeticionOperacion() {

    }

    public void enviaSMS() {
        if (operationDelegate instanceof PagarCreditoDelegate) {
            isEmail = false;
            isSMS = true;

            Session session = Session.getInstance(SuiteApp.appContext);

            Hashtable<String, String> paramTable = new Hashtable<String, String>();
            paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
            paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
            paramTable.put(ServerConstants.NUMERO_CREDITO, ((PagarCreditoDelegate) operationDelegate).getCreditoActual().getNumeroCredito());
            paramTable.put("tipoCredito", ((PagarCreditoDelegate) operationDelegate).getCreditoActual().getTipoCredito());
            paramTable.put("tipoNoti", "M");
            paramTable.put("folioOperacion", ((PagarCreditoDelegate) operationDelegate).getResultado().getFolio());
            paramTable.put("indicador", "");
            paramTable.put("versionFase","1");

            operationDelegate.doNetworkOperation(ApiConstants.OP_ENVIO_NOTI_PAGO_CREDITO, paramTable, true, null, resultadosViewController);
        }
    }

    public void guardaPDF() {

    }

    public void enviaEmail() {
        if (operationDelegate instanceof PagarCreditoDelegate) {
            isEmail = true;
            isSMS = false;

            Session session = Session.getInstance(SuiteApp.appContext);

            Hashtable<String, String> paramTable = new Hashtable<String, String>();
            paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
            paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
            paramTable.put(ServerConstants.NUMERO_CREDITO, ((PagarCreditoDelegate) operationDelegate).getCreditoActual().getNumeroCredito());
            paramTable.put("tipoCredito", ((PagarCreditoDelegate) operationDelegate).getCreditoActual().getTipoCredito());
            paramTable.put("tipoNoti", "C");
            paramTable.put("folioOperacion", ((PagarCreditoDelegate) operationDelegate).getResultado().getFolio());
            paramTable.put("indicador", "");
            paramTable.put("versionFase","1");

            operationDelegate.doNetworkOperation(ApiConstants.OP_ENVIO_NOTI_PAGO_CREDITO, paramTable, true, null, resultadosViewController);
        }
    }

    public void guardaFrecuente() {
        ((BmovilViewsController) resultadosViewController.getParentViewsController()).showAltaFrecuente(operationDelegate);
    }

    public void guardaRapido() {

    }

    public void borraOperacion() {

    }

    @Override
    public int getNombreImagenEncabezado() {
        return operationDelegate.getNombreImagenEncabezado();
    }

    @Override
    public int getTextoEncabezado() {
        return operationDelegate.getTextoEncabezado();
    }

    @Override
    public String getTextoTituloResultado() {
        return operationDelegate.getTextoTituloResultado();
    }

    @Override
    public int getColorTituloResultado() {
        return operationDelegate.getColorTituloResultado();
    }

    @Override
    public String getTextoPantallaResultados() {
        return operationDelegate.getTextoPantallaResultados();
    }

    @Override
    public String getTituloTextoEspecialResultados() {
        return operationDelegate.getTituloTextoEspecialResultados();
    }

    @Override
    public String getTextoEspecialResultados() {
        return operationDelegate.getTextoEspecialResultados();
    }

    @Override
    public int getOpcionesMenuResultados() {
        return listaOpcionesMenu;
    }

    @Override
    public String getTextoAyudaResultados() {
        return operationDelegate.getTextoAyudaResultados();
    }

    //One CLick
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (operationId == ApiConstants.OP_ENVIO_NOTI_PAGO_CREDITO) {
            if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                if (isSMS) {
                    resultadosViewController.showInformationAlert("Mensaje de texto enviado exitosamente, en breve lo recibirás en tu teléfono");
                    listaOpcionesMenu = operationDelegate.getOpcionesMenuResultados() - DelegateBaseOperacion.SHOW_MENU_SMS;
                } else if (isEmail) {
                    resultadosViewController.showInformationAlert("Correo electrónico enviado exitosamente");
                }
            } else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
                resultadosViewController.showInformationAlertEspecial(resultadosViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
            }
        }
    }

    public ResultadosViewController getResultadosViewController() {
        return resultadosViewController;
    }
    //Termina One CLick
}
