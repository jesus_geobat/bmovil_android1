package bancomer.api.pagarcreditos.models;


import suitebancomer.aplicaciones.bmovil.classes.model.Account;

public class CambioCuenta {

    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
