package bancomer.api.pagarcreditos.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.gui.controllers.PagarCreditoViewController;
import bancomer.api.pagarcreditos.implementations.BaseViewController;
import bancomer.api.pagarcreditos.implementations.BmovilViewsController;
import bancomer.api.pagarcreditos.implementations.ConfirmacionViewController;
import bancomer.api.pagarcreditos.implementations.DelegateBaseAutenticacion;
import bancomer.api.pagarcreditos.implementations.InitPagoCredito;
import bancomer.api.pagarcreditos.implementations.SuiteAppPagoCreditoApi;
import bancomer.api.pagarcreditos.models.Credito;
import bancomer.api.pagarcreditos.models.ImportesPagoCreditoData;
import bancomer.api.pagarcreditos.models.PagoDeCredito;
import bancomer.api.pagarcreditos.models.ResultadoPagoCreditoData;
import bancomer.api.pagarcreditos.models.SimulaImportesPagoCreditoData;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

public class PagarCreditoDelegate extends DelegateBaseAutenticacion {

    public final static long PAGO_CREDITO_DELEGATE_ID = 4L;
    private PagarCreditoViewController pagarCreditoViewController;

    private Account cuentaTDC;
    private ImportesPagoCreditoData importesPagoCredito;

    private PagoDeCredito pagoCredito;
    private Constants.Operacion operacion = Constants.Operacion.pagoCreditoCH;

    private ResultadoPagoCreditoData resultado;
    private SimulaImportesPagoCreditoData simulaImportesPagoCreditoData;

    private Account cuentaOrigenSeleccionada = null;
    private Credito creditoActual;

    /********************************************************* End Variables *******************************************************/

    /*********************************************************
     * Getters&Setters
     *******************************************************/

    public Account getCuentaOrigenSeleccionada() {
        return cuentaOrigenSeleccionada;
    }

    public void setCuentaOrigenSeleccionada(Account cuentaOrigenSeleccionada) {
        this.cuentaOrigenSeleccionada = cuentaOrigenSeleccionada;
    }

    public Credito getCreditoActual() {
        return creditoActual;
    }

    public void setCreditoActual(Credito creditoActual) {
        this.creditoActual = creditoActual;
    }

    public ResultadoPagoCreditoData getResultado() {
        return resultado;
    }


    public PagoDeCredito getPagoCreditoData() {
        return pagoCredito;
    }

    public void setPagoCreditoData(PagoDeCredito dataTDC) {
        this.pagoCredito = dataTDC;
    }

    public ImportesPagoCreditoData getImportesPagoCredito() {
        return importesPagoCredito;
    }

    public void setImportesPagoCredito(ImportesPagoCreditoData importesPagoCredito) {
        this.importesPagoCredito = importesPagoCredito;
    }

    public Account getCuentaTDC() {
        return cuentaTDC;
    }

    public void setCuentaTDC(Account cuentaTDC) {
        this.cuentaTDC = cuentaTDC;
    }

    public void setCuentaActual(Account cuenta) {
        cuentaTDC = cuenta;
    }

    public BaseViewController getPagarCreditoViewController() {
        return pagarCreditoViewController;
    }

    public void setPagarCreditoViewController(PagarCreditoViewController pagarCreditoViewController) {
        this.pagarCreditoViewController = pagarCreditoViewController;
    }

    /*********************************************************
     * End Getters&Setters
     *******************************************************/

    public void setDataPagoCredito() {
        pagoCredito = new PagoDeCredito();
        pagoCredito.setTipoCredito(creditoActual.getTipoCredito());
        pagoCredito.setNumCredito(creditoActual.getNumeroCredito());
        pagoCredito.setCuentaRetiro(pagarCreditoViewController.getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada().getNumber());
        pagoCredito.setNombreBeneficiario(Session.getInstance(SuiteAppPagoCreditoApi.appContext).getNombreCliente());
        pagoCredito.setImportePago(importesPagoCredito.getPagoRequerido());
        pagoCredito.setImportePagoMoneda(importesPagoCredito.getPagoRequeridoMoneda());
        pagoCredito.setImporteLiq(importesPagoCredito.getPagoLiquidacion());
        pagoCredito.setImporteLiqMoneda(importesPagoCredito.getPagoLiquidacionMoneda());
        pagoCredito.setTipoPago(pagarCreditoViewController.getTipoPago());
        if (simulaImportesPagoCreditoData != null) {
            pagoCredito.setImportePagoRecibos(simulaImportesPagoCreditoData.getImpPagoRecibos());
            pagoCredito.setImportePagoRecibosMoneda(simulaImportesPagoCreditoData.getImpPagoRecibosMoneda());
            pagoCredito.setImporteAntDif(simulaImportesPagoCreditoData.getImpAntDif());
            pagoCredito.setImporteAntDifMoneda(simulaImportesPagoCreditoData.getImpAntDifMoneda());
            pagoCredito.setImporteTotal(simulaImportesPagoCreditoData.getImpTotal());
            pagoCredito.setImporteTotalMoneda(simulaImportesPagoCreditoData.getImpTotalMoneda());
        }
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c.getTime());
        pagoCredito.setFechaPago(formattedDate);
        pagoCredito.setTipoAnticipo(pagarCreditoViewController.getTipoAnticipo());
    }

    /**
     * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada seg�n sea reguerido.
     *
     * @return
     */
    public ArrayList<Account> cargaCuentasOrigen() {
        Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();

        ArrayList<Account> accountsArray = new ArrayList<Account>();
        Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
        Boolean cuentaEjeTDC = true;

        if (profile == Constants.Perfil.avanzado) {
            for (Account acc : accounts) {
                if (acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE)) {
                    accountsArray.add(acc);
                    break;
                }
            }
            for (Account acc : accounts) {
                if (!acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE))
                    accountsArray.add(acc);
            }
        } else {
            for (Account acc : accounts) {
                if (acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE))
                    accountsArray.add(acc);
            }
        }

        if (((profile == Constants.Perfil.basico) || (accounts.length == 1)) && (Tools.obtenerCuentaEje().getType().equals(Constants.CREDIT_TYPE))) {
            pagarCreditoViewController.showInformationAlert(R.string.error_cuenta_eje_credito);
        }

        return accountsArray;
    }

    public void consultaImportesPagoCredito(String numeroCredito) {
        Session session = Session.getInstance(SuiteApp.appContext);

        //prepare data
        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
        paramTable.put(ServerConstants.NUMERO_CREDITO, numeroCredito);
        paramTable.put("versionFase", "1");

        doNetworkOperation(ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS, paramTable, true, new ImportesPagoCreditoData(), pagarCreditoViewController);
    }

    public void consultaSimulaImportesPagoCredito(String numeroCredito, String importeACalcular, String tipoAnticipio) {
        Session session = Session.getInstance(SuiteApp.appContext);

        //prepare data
        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
        paramTable.put(ServerConstants.NUMERO_CREDITO, numeroCredito);
        paramTable.put(ServerConstants.IMPORTE_CALCULAR, importeACalcular);
        paramTable.put(ServerConstants.TIPO_ANTICIPO, tipoAnticipio);

        doNetworkOperation(ApiConstants.OP_SIMULA_IMPORTES_PAGO_CREDITO, paramTable, true, new SimulaImportesPagoCreditoData(), pagarCreditoViewController);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (operationId == ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS) {
            InitPagoCredito.initPagoCredito.returnDataToPrincipal(String.valueOf(ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS), response);
        } else if (operationId == ApiConstants.OP_PAGO_CREDITO_CH) {
            if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                resultado = (ResultadoPagoCreditoData) response.getResponse();
                showResultados();
            } else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
                //pagarCreditoViewController.showInformationAlertEspecial(pagarCreditoViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
            }
        } else if (operationId == ApiConstants.OP_SIMULA_IMPORTES_PAGO_CREDITO) {
            pagarCreditoViewController.ocultaIndicadorActividad();
            if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                simulaImportesPagoCreditoData = (SimulaImportesPagoCreditoData) response.getResponse();
                if (simulaImportesPagoCreditoData.getReduccion() != null && simulaImportesPagoCreditoData.getReduccion().length() > 0) {
                    pagarCreditoViewController.showInformationAlertEspecial(pagarCreditoViewController.getString(R.string.label_error), "", simulaImportesPagoCreditoData.getReduccion(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            pagarCreditoViewController.ejecutarConfirmacion();
                        }
                    });
                } else {
                    pagarCreditoViewController.ejecutarConfirmacion();
                }
            } else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
                if (response.getMessageCode().equals("UGE5936")) {
                    pagarCreditoViewController.hideCurrentDialog();
                    pagarCreditoViewController.setHabilitado(true);
                    pagarCreditoViewController.showInformationAlertEspecial(pagarCreditoViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            pagarCreditoViewController.selectRadioLiquidacion();
                        }
                    });
                } else {
                    pagarCreditoViewController.showInformationAlertEspecial(pagarCreditoViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
                }
            }
        }
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler handler, BaseViewController caller) {
        BmovilViewsController parent = null;
        if (pagarCreditoViewController.getParentViewsController() instanceof BmovilViewsController) {
            parent = ((BmovilViewsController) pagarCreditoViewController.getParentViewsController());
        } else {
            parent = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController();
        }
        parent.getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, caller, true);
    }

    /*****/
    @Override
    public Constants.TipoOtpAutenticacion tokenAMostrar() {
        Constants.TipoOtpAutenticacion value = null;
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        value = Autenticacion.getInstance().tokenAMostrar(this.operacion, perfil, Tools.getDoubleAmountFromServerString(pagarCreditoViewController.getImporteSeleccionado()));

        return value;
    }

    @Override
    public boolean mostrarNIP() {
        boolean mostrar = false;
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        mostrar = Autenticacion.getInstance().mostrarNIP(this.operacion, perfil, Tools.getDoubleAmountFromServerString(pagarCreditoViewController.getImporteSeleccionado()));

        return mostrar;
    }

    @Override
    public boolean mostrarContrasenia() {
        boolean mostrar = false;
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        mostrar = Autenticacion.getInstance().mostrarContrasena(this.operacion, perfil, Tools.getDoubleAmountFromServerString(pagarCreditoViewController.getImporteSeleccionado()));

        return mostrar;
    }


    @Override
    public boolean mostrarCVV() {
        boolean mostrar = false;
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        mostrar = Autenticacion.getInstance().mostrarCVV(this.operacion, perfil, Tools.getDoubleAmountFromServerString(pagarCreditoViewController.getImporteSeleccionado()));

        return mostrar;
    }


    public void showConfirmacion() {
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacion(this);
    }

    public void showResultados() {
        this.actualizarMontoCuentas();

        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().showResultadosViewController(this, getNombreImagenEncabezado(), getTextoEncabezado());
    }

    @Override
    public long getDelegateIdentifier() {
        // TODO Auto-generated method stub
        return super.getDelegateIdentifier();
    }


    /**
     * Actualiza los montos de las cuentas de origen y destino.
     */
    @SuppressWarnings("unchecked")
    public void actualizarMontoCuentas() {
        try {
            Session.getInstance(SuiteApp.appContext).actualizaMonto(getCuentaOrigen(),
                    resultado.getSaldoCuenta());
        } catch (Exception ex) {
            Log.e("actualizarMontoCuentas", "Error al interpretar los datos de cuentas del servidor.", ex);
        }
    }

    public Account getCuentaOrigen() {
        return pagarCreditoViewController.getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada();
    }

    @Override
    public void realizaOperacion(ConfirmacionViewController confirmacionViewController, String contrasenia,
                                 String nip, String token, String campoTarjeta, String cvv) {
        Session session = Session.getInstance(SuiteApp.appContext);

        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
        paramTable.put(ServerConstants.NUMERO_CREDITO, creditoActual.getNumeroCredito());

        paramTable.put(ServerConstants.CODIGO_NIP, nip == null ? "" : nip);
        paramTable.put(ServerConstants.CODIGO_CVV2, cvv == null ? "" : cvv);
        paramTable.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
        paramTable.put(ServerConstants.CADENA_AUTENTICACION, Autenticacion.getInstance().getCadenaAutenticacion(this.operacion, session.getClientProfile()));
        paramTable.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);

        paramTable.put(ServerConstants.IMPORTE_PAGO, pagarCreditoViewController.getImporteSeleccionado());
        paramTable.put(ServerConstants.TIPO_CREDITO, creditoActual.getTipoCredito());
        paramTable.put(ServerConstants.CUENTA_CARGO, getCuentaOrigen().getFullNumber());

        paramTable.put("tipoPago", pagarCreditoViewController.getTipoPago());
        paramTable.put("tipoAnticipo", pagarCreditoViewController.getTipoAnticipo());
        paramTable.put("pagoMinimo", importesPagoCredito.getPagoMinimo());
        paramTable.put("versionFase", "1");

        paramTable.put(ServerConstants.PARAMS_TEXTO_EN, "");

        List<String> listaEncriptar = Arrays.asList(ServerConstants.JSON_IUM_ETIQUETA,
                ServerConstants.NUMERO_CELULAR, ServerConstants.NUMERO_CREDITO,
                ServerConstants.CODIGO_NIP, ServerConstants.CODIGO_CVV2,
                ServerConstants.CODIGO_OTP, ServerConstants.CVE_ACCESO);
        Encripcion.setContext(SuiteAppPagoCreditoApi.appContext);
        Encripcion.encriptaCadenaAutenticacion(paramTable, listaEncriptar);
        //JAIG
        doNetworkOperation(ApiConstants.OP_PAGO_CREDITO_CH, paramTable, true, new ResultadoPagoCreditoData(), confirmacionViewController);
    }

    @Override
    public ArrayList<Object> getDatosTablaConfirmacion() {
        String tipoPago = pagarCreditoViewController.getTipoPago();
        String tipoMoneda = importesPagoCredito.getMoneda();

        ArrayList<Object> tabla = new ArrayList<Object>();
        ArrayList<String> fila;
        //Cuenta retiro
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_cuenta_retiro));
        fila.add(Tools.hideAccountNumber(pagoCredito.getCuentaRetiro()));
        tabla.add(fila);
        //No Credito
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_num_credito));
        fila.add(pagoCredito.getNumCredito());
        tabla.add(fila);
        //Beneficiario
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_beneficiario));
        fila.add(pagoCredito.getNombreBeneficiario());
        tabla.add(fila);
        //Tipo credito
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_tipo_credito));
        fila.add(getTipoCredito(pagoCredito.getTipoCredito()));
        tabla.add(fila);
        //2a-2b
        fila = new ArrayList<String>();
        if (tipoPago.equals("LI")) {
            fila.add(pagarCreditoViewController.getString(R.string.pago_credito_liquidacion));
        } else if (tipoPago.equals("PR") || tipoPago.equals("OC")) {
            fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_pago_recibos));
        }
        if (tipoPago.equals("PR")) {
            fila.add(Tools.formatAmount(pagoCredito.getImportePago(), false));
        } else if (tipoPago.equals("LI")) {
            fila.add(Tools.formatAmount(pagoCredito.getImporteLiq(), false));
        } else if (tipoPago.equals("OC")) {
            fila.add(Tools.formatAmount(pagoCredito.getImportePagoRecibos(), false));
        }
        //2a-2c
        if (tipoPago.equals("OC") && (tipoMoneda.equals("UDI") || tipoMoneda.equals("VSM"))) {
            fila.add(Tools.formatAmountWithCurrency(pagoCredito.getImportePagoRecibosMoneda(), tipoMoneda + " ", false));
        }
        tabla.add(fila);

        //3a-3b-3c
        if (tipoPago.equals("OC")) {
            //3a-3b
            fila = new ArrayList<String>();
            fila.add(pagarCreditoViewController.getString(R.string.pago_credito_anticipo_diferido));
            fila.add(Tools.formatAmount(pagoCredito.getImporteAntDif(), false));
            //3a-3c
            if ((tipoMoneda.equals("UDI") || tipoMoneda.equals("VSM"))) {
                fila.add(Tools.formatAmountWithCurrency(pagoCredito.getImporteAntDifMoneda(), tipoMoneda + " ", false));
            }
            tabla.add(fila);
        }
        //4a-4b-4c
        if (tipoPago.equals("OC")) {
            //4a-4b
            fila = new ArrayList<String>();
            fila.add(pagarCreditoViewController.getString(R.string.pago_credito_importe_total));
            fila.add(Tools.formatAmount(pagoCredito.getImporteTotal(), false));
            //4a-4c
            if ((tipoMoneda.equals("UDI") || tipoMoneda.equals("VSM"))) {
                fila.add(Tools.formatAmountWithCurrency(pagoCredito.getImporteTotalMoneda(), tipoMoneda + " ", false));
            }
            tabla.add(fila);
        }
        //1a-1b
        if ((tipoMoneda.equals("UDI") || tipoMoneda.equals("VSM")) && (tipoPago.equals("PR") || tipoPago.equals("LI"))) {
            fila = new ArrayList<String>();
            fila.add(tipoMoneda);
            if (tipoPago.equals("PR")) {
                fila.add(Tools.formatAmountWithCurrency(pagoCredito.getImportePagoMoneda(), "", false));
            } else if (tipoPago.equals("LI")) {
                fila.add(Tools.formatAmountWithCurrency(pagoCredito.getImporteLiqMoneda(), "", false));
            }
            tabla.add(fila);
        }
        //Fecha
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_fecha));
        fila.add(pagoCredito.getFechaPago());
        tabla.add(fila);

        return tabla;
    }

    @Override
    public ArrayList<Object> getDatosTablaResultados() {
        String tipoPago = pagarCreditoViewController.getTipoPago();
        String tipoMoneda = importesPagoCredito.getMoneda();

        ArrayList<Object> tabla = new ArrayList<Object>();
        ArrayList<String> fila;
        //Cuenta retiro
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_cuenta_retiro));
        fila.add(Tools.hideAccountNumber(pagoCredito.getCuentaRetiro()));
        tabla.add(fila);
        //No Credito
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_num_credito));
        fila.add(pagoCredito.getNumCredito());
        tabla.add(fila);
        //Beneficiario
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_beneficiario));
        fila.add(pagoCredito.getNombreBeneficiario());
        tabla.add(fila);
        //Tipo credito
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_tipo_credito));
        fila.add(getTipoCredito(pagoCredito.getTipoCredito()));
        tabla.add(fila);
        //2a-2b
        fila = new ArrayList<String>();
        if (tipoPago.equals("LI")) {
            fila.add(pagarCreditoViewController.getString(R.string.pago_credito_liquidacion));
        } else if (tipoPago.equals("PR") || tipoPago.equals("OC")) {
            fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_pago_recibos));
        }
        if (tipoPago.equals("PR")) {
            fila.add(Tools.formatAmount(pagoCredito.getImportePago(), false));
        } else if (tipoPago.equals("LI")) {
            fila.add(Tools.formatAmount(pagoCredito.getImporteLiq(), false));
        } else if (tipoPago.equals("OC")) {
            fila.add(Tools.formatAmount(resultado.getPagoRequerido(), false));
        }
        //2a-2c
        if (tipoPago.equals("OC") && (tipoMoneda.equals("UDI") || tipoMoneda.equals("VSM"))) {
            fila.add(Tools.formatAmountWithCurrency(resultado.getPagoReqMoneda(), tipoMoneda + " ", false));
        }
        tabla.add(fila);

        //3a-3b-3c
        if (tipoPago.equals("OC")) {
            //3a-3b
            fila = new ArrayList<String>();
            fila.add(pagarCreditoViewController.getString(R.string.pago_credito_anticipo_diferido));
            fila.add(Tools.formatAmount(resultado.getPagoAnticipo(), false));
            //3a-3c
            if ((tipoMoneda.equals("UDI") || tipoMoneda.equals("VSM"))) {
                fila.add(Tools.formatAmountWithCurrency(resultado.getPagoAntiMoneda(), tipoMoneda + " ", false));
            }
            tabla.add(fila);
        }
        //4a-4b-4c
        if (tipoPago.equals("OC")) {
            //4a-4b
            fila = new ArrayList<String>();
            fila.add(pagarCreditoViewController.getString(R.string.pago_credito_importe_total));
            fila.add(Tools.formatAmount(resultado.getPagoTotal(), false));
            //4a-4c
            if ((tipoMoneda.equals("UDI") || tipoMoneda.equals("VSM"))) {
                    fila.add(Tools.formatAmountWithCurrency(resultado.getPagoTotalMoneda(), tipoMoneda + " ", false));
            }
            tabla.add(fila);
        }
        //1a-1b
        if ((tipoMoneda.equals("UDI") || tipoMoneda.equals("VSM")) && (tipoPago.equals("PR") || tipoPago.equals("LI"))) {
            fila = new ArrayList<String>();
            fila.add(tipoMoneda + " ");
            if (tipoPago.equals("PR")) {
                fila.add(Tools.formatAmountWithCurrency(pagoCredito.getImportePagoMoneda(), "", false));
            } else if (tipoPago.equals("LI")) {
                fila.add(Tools.formatAmountWithCurrency(pagoCredito.getImporteLiqMoneda(), "", false));
            }
            tabla.add(fila);
        }
        //Fecha
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_fecha));
        fila.add(resultado.getFechaPago());
        tabla.add(fila);
        //Hora
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_hora));
        fila.add(Tools.formatTime(resultado.getHoraPago()));
        tabla.add(fila);
        //Folio
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_folio));
        fila.add(resultado.getFolio());
        tabla.add(fila);
        //Recibo numero
        if (!tipoPago.equals("LI") && (!((tipoPago.equals("OC")) && (pagoCredito.getImportePago().equals("000"))))) {
            fila = new ArrayList<String>();
            fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_recibo_numero));
            fila.add(resultado.getNumRec());
            tabla.add(fila);
        }

        return tabla;
    }

    private String getTipoCredito(String tipo) {
        String ret = tipo;
        if (tipo.equalsIgnoreCase("C")) {
            ret = "Consumo";
        } else if (tipo.equalsIgnoreCase("H")) {
            ret = "Hipotecario";
        } else if (tipo.equalsIgnoreCase("P")) {
            ret = "Préstamo Personal";
        } else if (tipo.equalsIgnoreCase("N")) {
            ret = "Nómina";
        }

        return ret;
    }

    /**
     * Funcion para logica de mostrar tabla abonos
     *
     * @return
     */
    public Boolean checkTablaAbonos(){

        // Inicializacion de variable retorno
        // No mostrar
        Boolean muestraTablaAbonos= false;

        // Variable tipo credito y pago requerido
        String tipoCredito = creditoActual.getTipoCredito();
        String pagoRequerido = importesPagoCredito.getPagoRequerido();

        //
        String pagoMinimo = importesPagoCredito.getPagoMinimo();
        Double cantidadPagoMinimo = Double.parseDouble(pagoMinimo) / 100;

        Double otraCantidad = Double.parseDouble(pagarCreditoViewController.getValorOtraCantidad().getAmount());

        // En caso de tipo credito Nomina, consumo, prestamos personal y pago requerido 000 se muestra tabla abonos
        if (((tipoCredito.equals("P") || tipoCredito.equals("N") || tipoCredito.equals("C")) && pagoRequerido.equals("000")) && (otraCantidad > cantidadPagoMinimo)) {
            // Mostrar tabla abonos
            muestraTablaAbonos = true;
        }
        // Retornar valor para mostrar tabla abonos
        return muestraTablaAbonos;
    }


    @Override
    public int getOpcionesMenuResultados() {
        return SHOW_MENU_SMS | SHOW_MENU_EMAIL;
    }

    @Override
    public int getNombreImagenEncabezado() {
        return R.drawable.api_pago_credito_icono_pagar;
    }

    @Override
    public int getTextoEncabezado() {
        return R.string.pago_credito_titulo;
    }

    @Override
    public String getTextoTituloResultado() {
        return pagarCreditoViewController.getString(R.string.transferir_detalle_operacion_exitosaTitle);
    }

    @Override
    public int getColorTituloResultado() {
        return R.color.verde_limon;
    }

    @Override
    public String getTextoAyudaResultados() {
        return "Para guardar esta información oprime el botón de opciones";
    }

    @Override
    public void performAction(Object obj) {

        if (Server.ALLOW_LOG) Log.d("TransferirMisCuentasDelegate", "Regrese de lista Seleccion");
        if (Server.ALLOW_LOG) Log.d("PagoTDCDelegate", "Seleccion false");
        pagarCreditoViewController.muestraListaCuentas();
        pagarCreditoViewController.getComponenteCtaOrigen().setSeleccionado(false);
    }
}
