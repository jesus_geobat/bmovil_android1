/**
 *
 */
package bancomer.api.pagarcreditos.models;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * @author Francisco.Garcia
 */
public class CambioTelefonoResult implements ParsingHandler {

    private String folio;

    public String getFolio() {
        return folio;
    }

    /*
     *
     */
    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        throw new UnsupportedOperationException(getClass().getName());
    }

    /**
     *
     */
    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        folio = parser.parseNextValue("folioArq");
    }

}
