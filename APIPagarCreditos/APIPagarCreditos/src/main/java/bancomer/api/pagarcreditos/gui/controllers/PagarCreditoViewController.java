package bancomer.api.pagarcreditos.gui.controllers;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;

import bancomer.api.common.commons.Constants;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.commons.GuiTools;
import bancomer.api.pagarcreditos.commons.TrackingHelper;
import bancomer.api.pagarcreditos.gui.commons.controllers.AmountField;
import bancomer.api.pagarcreditos.gui.commons.controllers.CuentaOrigenViewController;
import bancomer.api.pagarcreditos.gui.delegates.PagarCreditoDelegate;
import bancomer.api.pagarcreditos.implementations.BaseViewController;
import bancomer.api.pagarcreditos.implementations.BmovilViewsController;
import bancomer.api.pagarcreditos.implementations.InitPagoCredito;
import bancomer.api.pagarcreditos.implementations.SuiteAppPagoCreditoApi;
import bancomer.api.pagarcreditos.io.BanderasServer;
import bancomer.api.pagarcreditos.models.ImportesPagoCreditoData;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

public class PagarCreditoViewController extends BaseViewController implements View.OnClickListener {

    private PagarCreditoDelegate delegate;

    public BmovilViewsController parentManager;

    private LinearLayout cuentaRetiro;

    private CuentaOrigenViewController componenteCtaOrigen;
    private ImageButton continuarButton;

    private TextView fechaCorte;

    private TextView valorPagoRequerido;
    private TextView valorMonedaPagoRequerido;

    private TextView valorLiquidacion;
    private TextView valorMonedaLiquidacion;

    private RadioButton radioPagoRequerido;
    private LinearLayout radioPagoRequeridoCelda;
    private RadioButton radioLiquidacion;
    private LinearLayout radioLiquidacionCelda;
    private RadioButton radioOtraCantidad;
    private LinearLayout radioOtraCantidadCelda;



    private boolean patrimonial = false;
    /**
     * validacion Patrimonial
     * @param savedInstanceState
     */

    private AmountField valorOtraCantidad;

    private TextView lblAbonos;
    private LinearLayout listaAbonos;

    private RadioButton radioAntCuota;
    private LinearLayout radioAntCuotaCelda;
    private RadioButton radioAntPlazo;
    private LinearLayout radioAntPlazoCelda;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.activity_pagar_credito_view_controller);
        SuiteApp.appContext = this;

            setTitle(R.string.pago_credito_titulo, R.drawable.api_pago_credito_icono_pagar);
        //marqueeEnabled = true;
        //AMZ
        parentManager = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("pagar tdc", parentManager.estados);

        setParentViewsController(SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController());
        delegate = (PagarCreditoDelegate) parentViewsController.getBaseDelegateForKey(PagarCreditoDelegate.PAGO_CREDITO_DELEGATE_ID);
        delegate.setPagarCreditoViewController(this);


        findViews();

        configurarDatosPantalla();

        scaleForCurrentScreen();
        muestraComponenteCuentaOrigen();

        //checkTablaAbonos();
        InitPagoCredito init = InitPagoCredito.getInstance(this, new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
        init.setPatrimonial(patrimonial);
    }

    private void findViews() {
        cuentaRetiro = (LinearLayout) findViewById(R.id.pago_credito_cuenta_origen);

        fechaCorte = (TextView) findViewById(R.id.fechaCorte);

        valorPagoRequerido = (TextView) findViewById(R.id.valor_dolar_pago_requerido);
        valorPagoRequerido.setSelected(true);
        valorPagoRequerido.setSingleLine(true);
        valorMonedaPagoRequerido = (TextView) findViewById(R.id.valor_udi_pago_requerido);

        valorLiquidacion = (TextView) findViewById(R.id.valor_dolar_liquidacion);
        valorLiquidacion.setSelected(true);
        valorLiquidacion.setSingleLine(true);
        valorMonedaLiquidacion = (TextView) findViewById(R.id.valor_udi_liquidacion);

        radioPagoRequerido = (RadioButton) findViewById(R.id.radioButton_pago_requerido);
        radioPagoRequerido.setOnClickListener(this);
        radioPagoRequeridoCelda = (LinearLayout) findViewById(R.id.celda_pago_requerido);
        radioPagoRequeridoCelda.setOnClickListener(this);

        radioLiquidacion = (RadioButton) findViewById(R.id.radioButton_liquidacion);
        radioLiquidacion.setOnClickListener(this);
        radioLiquidacionCelda = (LinearLayout) findViewById(R.id.celda_liquidacion);
        radioLiquidacionCelda.setOnClickListener(this);

        radioOtraCantidad = (RadioButton) findViewById(R.id.radioButton_otra_cantidad);
        radioOtraCantidad.setOnClickListener(this);
        radioOtraCantidadCelda = (LinearLayout) findViewById(R.id.celda_otra_cantidad);
        radioOtraCantidadCelda.setOnClickListener(this);

        valorOtraCantidad = (AmountField) findViewById(R.id.edit_otra_cantidad);
        valorOtraCantidad.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkTablaAbonos();
            }
        });

        lblAbonos = (TextView) findViewById(R.id.lbl_importe_digitado);
        listaAbonos = (LinearLayout) findViewById(R.id.lista_abonos);

        radioAntCuota = (RadioButton) findViewById(R.id.radioButton_cuota);
        radioAntCuota.setOnClickListener(this);
        radioAntCuotaCelda = (LinearLayout) findViewById(R.id.celda_cuota);
        radioAntCuotaCelda.setOnClickListener(this);

        radioAntPlazo = (RadioButton) findViewById(R.id.radioButton_plazo);
        radioAntPlazo.setOnClickListener(this);
        radioAntPlazoCelda = (LinearLayout) findViewById(R.id.celda_plazo);
        radioAntPlazoCelda.setOnClickListener(this);

        continuarButton = (ImageButton) findViewById(R.id.PTDC_boton_continuar);
        continuarButton.setOnClickListener(this);
    }

    public void configurarDatosPantalla() {
        String fecha = delegate.getImportesPagoCredito().getFechaPago();
        if (fecha.matches("^\\d{4}/\\d{2}/\\d{2}$")) {
            fechaCorte.setText(Tools.formatDateTDC(fecha));
        } else {
            if (fecha.contains("En actualizaci")) {
                fecha = "En actualización";
            }
            fechaCorte.setText(fecha);
            if (!fecha.matches("^\\d{2}/\\d{2}/\\d{4}$")) {
                fechaCorte.setTextColor(getResources().getColor(R.color.tercer_azul));
            }
        }

        configurarCeldaPagoRequerido();
        configurarCeldaLiquidacion();

        valorOtraCantidad.setAmount(delegate.getImportesPagoCredito().getPagoMinimo());

        String pagoRequerido = delegate.getImportesPagoCredito().getPagoRequerido();
        if (pagoRequerido.equals("000")) {
            radioPagoRequerido.setEnabled(false);
            radioPagoRequeridoCelda.setEnabled(false);
            changeRadioImportes(radioOtraCantidad);
        } else if (!pagoRequerido.matches("^\\d+$")) {
            radioPagoRequerido.setEnabled(false);
            radioPagoRequeridoCelda.setEnabled(false);
            changeRadioImportes(radioOtraCantidad);
        }
    }

    private void configurarCeldaPagoRequerido() {
        ImportesPagoCreditoData importesPagoCredito = delegate.getImportesPagoCredito();
        String moneda = importesPagoCredito.getMoneda();
        String pagoRequerido = importesPagoCredito.getPagoRequerido();
        String pagoRequeridoMoneda = importesPagoCredito.getPagoRequeridoMoneda();

        if (pagoRequerido.matches("^\\d+$")) {
            boolean isNegative = pagoRequerido.startsWith("-");
            pagoRequerido = Tools.formatAmount(pagoRequerido, isNegative);
        } else {
            if (pagoRequerido.contains("En actualizaci")) {
                pagoRequerido = "En actualización";
            }
        }
        this.valorPagoRequerido.setText(pagoRequerido);

        if (moneda.compareTo("UDI") == 0 || moneda.compareTo("VSM") == 0) {
            if (pagoRequeridoMoneda.matches("^\\d+$")) {
                boolean isNegative2 = pagoRequeridoMoneda.startsWith("-");
                pagoRequeridoMoneda = Tools.formatAmountWithCurrency(pagoRequeridoMoneda, moneda + " ", isNegative2);
            }
            valorMonedaPagoRequerido.setText(pagoRequeridoMoneda);
            valorMonedaPagoRequerido.setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.celda_pago_requerido).getLayoutParams().height *= 0.7;
        }
    }

    private void configurarCeldaLiquidacion() {
        ImportesPagoCreditoData importesPagoCredito = delegate.getImportesPagoCredito();
        String moneda = importesPagoCredito.getMoneda();
        String pagoLiquidacion = importesPagoCredito.getPagoLiquidacion();
        String pagoLiquidacionMoneda = importesPagoCredito.getPagoLiquidacionMoneda();

        if (pagoLiquidacion.matches("^\\d+$")) {
            boolean isNegative = pagoLiquidacion.startsWith("-");
            pagoLiquidacion = Tools.formatAmount(pagoLiquidacion, isNegative);
        } else {
            if (pagoLiquidacion.contains("En actualizaci")) {
                pagoLiquidacion = "En actualización";
            }
        }
        this.valorLiquidacion.setText(pagoLiquidacion);

        if (moneda.compareTo("UDI") == 0 || moneda.compareTo("VSM") == 0) {
            if (pagoLiquidacionMoneda.matches("^\\d+$")) {
                boolean isNegative2 = pagoLiquidacionMoneda.startsWith("-");
                pagoLiquidacionMoneda = Tools.formatAmountWithCurrency(pagoLiquidacionMoneda, moneda + " ", isNegative2);
            }
            valorMonedaLiquidacion.setText(pagoLiquidacionMoneda);
            valorMonedaLiquidacion.setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.celda_liquidacion).getLayoutParams().height *= 0.7;
        }
    }

    private void scaleForCurrentScreen() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.rootLayout));

        guiTools.scale(findViewById(R.id.pago_credito_cuenta_origen));
        guiTools.scale(findViewById(R.id.detalle_transfer_cuenta_destino_text), true);
        guiTools.scale(findViewById(R.id.detalle_transfer_cuenta_destino_edit));

        guiTools.scale(findViewById(R.id.accdataLayout));
        guiTools.scale(findViewById(R.id.listaFechas));
        guiTools.scale(findViewById(R.id.linear01));
        guiTools.scale(findViewById(R.id.fechPago), true);
        guiTools.scale(findViewById(R.id.fechaCorte), true);

        guiTools.scale(findViewById(R.id.importe), true);

        guiTools.scale(findViewById(R.id.lista));

        guiTools.scale(findViewById(R.id.celda_pago_requerido));
        guiTools.scale(findViewById(R.id.pago_requerido), true);
        guiTools.scale(findViewById(R.id.valores_pago_requerido));
        guiTools.scale(findViewById(R.id.valor_udi_pago_requerido), true);
        guiTools.scale(findViewById(R.id.valor_dolar_pago_requerido), true);
        guiTools.scale(findViewById(R.id.radioButton_pago_requerido));

        guiTools.scale(findViewById(R.id.celda_liquidacion));
        guiTools.scale(findViewById(R.id.liquidacion), true);
        guiTools.scale(findViewById(R.id.valores_liquidacion));
        guiTools.scale(findViewById(R.id.valor_udi_liquidacion), true);
        guiTools.scale(findViewById(R.id.valor_dolar_liquidacion), true);
        guiTools.scale(findViewById(R.id.radioButton_liquidacion));

        guiTools.scale(findViewById(R.id.celda_otra_cantidad));
        guiTools.scale(findViewById(R.id.otra_cantidad), true);
        guiTools.scale(findViewById(R.id.radioButton_otra_cantidad));

        guiTools.scale(findViewById(R.id.edit_otra_cantidad), true);

        guiTools.scale(findViewById(R.id.lbl_importe_digitado), true);

        guiTools.scale(findViewById(R.id.lista_abonos));
        guiTools.scale(findViewById(R.id.celda_cuota));
        guiTools.scale(findViewById(R.id.anticipo_cuota), true);
        guiTools.scale(findViewById(R.id.radioButton_cuota));
        guiTools.scale(findViewById(R.id.celda_plazo));
        guiTools.scale(findViewById(R.id.anticipo_plazo), true);
        guiTools.scale(findViewById(R.id.radioButton_plazo));

        guiTools.scale(findViewById(R.id.continuarlayout));
        guiTools.scale(continuarButton);
    }

    @SuppressWarnings("deprecation")
    public void muestraComponenteCuentaOrigen() {
        ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        componenteCtaOrigen = new CuentaOrigenViewController(this, params, parentViewsController, this);
        componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
        componenteCtaOrigen.setDelegate(delegate);
        componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
        if (delegate.getCuentaOrigenSeleccionada() != null) {
            componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getCuentaOrigenSeleccionada()));
        } else {
            componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
        }
        componenteCtaOrigen.setPatrimonial(patrimonial);
        componenteCtaOrigen.init();
        componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.transferir);
        cuentaRetiro.addView(componenteCtaOrigen);
    }

    private void changeRadioImportes(RadioButton button) {
        radioPagoRequerido.setChecked(false);
        radioLiquidacion.setChecked(false);
        radioOtraCantidad.setChecked(false);

        button.setChecked(true);
        if (button.equals(radioOtraCantidad)) {
            valorOtraCantidad.setVisibility(View.VISIBLE);
            //checkTablaAbonos();
        } else {
            valorOtraCantidad.setVisibility(View.GONE);
            lblAbonos.setVisibility(View.GONE);
            listaAbonos.setVisibility(View.GONE);
        }
    }

    public void selectRadioLiquidacion() {
        changeRadioImportes(radioLiquidacion);
    }

    private void changeRadioAbonos(RadioButton button) {
        radioAntCuota.setChecked(false);
        radioAntPlazo.setChecked(false);

        button.setChecked(true);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(radioPagoRequerido) || v.equals(radioPagoRequeridoCelda)) {
            changeRadioImportes(radioPagoRequerido);
        } else if (v.equals(radioLiquidacion) || v.equals(radioLiquidacionCelda)) {
            selectRadioLiquidacion();
        } else if (v.equals(radioOtraCantidad) || v.equals(radioOtraCantidadCelda)) {
            changeRadioImportes(radioOtraCantidad);
        } else if (v.equals(radioAntCuota) || v.equals(radioAntCuotaCelda)) {
            changeRadioAbonos(radioAntCuota);
        } else if (v.equals(radioAntPlazo) || v.equals(radioAntPlazoCelda)) {
            changeRadioAbonos(radioAntPlazo);
        } else if (v.equals(continuarButton)) {
            if (validaOtraCantidad()) {
                ejecutarConfirmacion();
            }
        }
    }

    private Boolean validaOtraCantidad() {
        Boolean todoOk = false;

        String pagoMinimo = delegate.getImportesPagoCredito().getPagoMinimo();

        if (radioOtraCantidad.isChecked()) {
            Double otraCantidad = Double.parseDouble(valorOtraCantidad.getAmount());

            Double cantidadPagoMinimo = Double.parseDouble(pagoMinimo) / 100;

            if (Server.ALLOW_LOG)
                Log.i("Validando otra cantidad", "cantidad introducida " + otraCantidad + " cantidadPagoMinimo " + cantidadPagoMinimo);

            if (otraCantidad <= 0.0) {
                //EX#3
                showInformationAlert(R.string.pago_credito_ingresar_importe, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        valorOtraCantidad.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(valorOtraCantidad, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            } else if (otraCantidad < cantidadPagoMinimo) {
                //EA#7
                showInformationAlert(R.string.pago_credito_importe_menor, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        valorOtraCantidad.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(valorOtraCantidad, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            } else if (valorOtraCantidad.getAmount().replace(".", "").matches("^\\d+$")) {
                delegate.consultaSimulaImportesPagoCredito(delegate.getCreditoActual().getNumeroCredito(), valorOtraCantidad.getAmount().replace(".", ""), getTAntCheck());
            } else {
                todoOk = true;
            }
        } else {
            todoOk = true;
        }

        return todoOk;
    }

    public Boolean ejecutarConfirmacion() {
        Double cantidadSeleccionada = Double.parseDouble(getImporteSeleccionado().substring(0, getImporteSeleccionado().length() - 2));
        if (cantidadSeleccionada > delegate.getCuentaOrigen().getBalance()) {
            hideCurrentDialog();
            setHabilitado(true);
            showInformationAlert(getString(R.string.pago_credito_saldo_insuficiente_alert));
        } else {
            delegate.setDataPagoCredito();
            this.delegate.showConfirmacion();
        }
        return true;
    }

    public String getImporteSeleccionado() {
        String importe = "0";

        if (radioPagoRequerido.isChecked()) {
            importe = delegate.getImportesPagoCredito().getPagoRequerido();
        } else if (radioLiquidacion.isChecked()) {
            importe = delegate.getImportesPagoCredito().getPagoLiquidacion();
        } else if (radioOtraCantidad.isChecked()) {
            importe = valorOtraCantidad.getAmount().replace(".", "");
        }
        if (Server.ALLOW_LOG) System.out.println("Importe Raw: " + importe);
        return importe;
    }

    public String getTipoPago() {
        String tipoPago = "";

        if (radioPagoRequerido.isChecked()) {
            tipoPago = "PR";
        } else if (radioLiquidacion.isChecked()) {
            tipoPago = "LI";
        } else if (radioOtraCantidad.isChecked()) {
            tipoPago = "OC";
        }
        return tipoPago;
    }

    public String getTipoAnticipo() {
        String tipoAnticipo = "";

        if (isCheckedRadioAntCuota()) {
            tipoAnticipo = "01";
        } else if (isCheckedRadioAntPlazo()) {
            tipoAnticipo = "02";
        }
        return tipoAnticipo;
    }

    public String getTAntCheck() {
        String tipoAnticipo = "";

        if (isCheckedRadioAntCuota()) {
            tipoAnticipo = "1";
        } else if (isCheckedRadioAntPlazo()) {
            tipoAnticipo = "2";
        }
        return tipoAnticipo;
    }

    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
      //  parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void goBack() {
        super.goBack();
    }

    public void actualizaCuentaOrigen(Account cuenta) {
        if (Server.ALLOW_LOG)
            Log.d("TransferirMisCuentasDetalleViewController", "Actualizar la cuenta origen actual por  " + cuenta.getNumber());
        delegate.setCuentaOrigenSeleccionada(cuenta);
        componenteCtaOrigen.getImgDerecha().setEnabled(true);
        componenteCtaOrigen.getImgIzquierda().setEnabled(true);
        componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
    }

    @SuppressWarnings("deprecation")
    public void muestraListaCuentas() {
        componenteCtaOrigen.getImgDerecha().setEnabled(true);
        componenteCtaOrigen.getImgIzquierda().setEnabled(true);
        componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
    }


    /**
     * EA#10 Funcion para mostrar opcion tabla abonos
     */
    private void checkTablaAbonos() {

        // Comprobar si debe mostrar tabla abonos
        if (delegate.checkTablaAbonos()) {
            //EA#10
            // Mostrar tabla abonos
            lblAbonos.setVisibility(View.VISIBLE);
            listaAbonos.setVisibility(View.VISIBLE);
        }else{
            // Mostrar tabla abonos
            lblAbonos.setVisibility(View.GONE);
            listaAbonos.setVisibility(View.GONE);
        }
    }

    @Override
    public PagarCreditoDelegate getDelegate() {
        return delegate;
    }

    public void setDelegate(PagarCreditoDelegate delegate) {
        this.delegate = delegate;
    }

    public CuentaOrigenViewController getComponenteCtaOrigen() {
        return componenteCtaOrigen;
    }

    public void setComponenteCtaOrigen(
            CuentaOrigenViewController componenteCtaOrigen) {
        this.componenteCtaOrigen = componenteCtaOrigen;
    }

    public boolean isCheckedRadioAntCuota() {
        return listaAbonos.getVisibility() == View.VISIBLE && radioAntCuota.isChecked();
    }

    public boolean isCheckedRadioAntPlazo() {
        return listaAbonos.getVisibility() == View.VISIBLE && radioAntPlazo.isChecked();
    }

    public AmountField getValorOtraCantidad() {
        return valorOtraCantidad;
    }

    public void setValorOtraCantidad(AmountField valorOtraCantidad) {
        this.valorOtraCantidad = valorOtraCantidad;
    }


}