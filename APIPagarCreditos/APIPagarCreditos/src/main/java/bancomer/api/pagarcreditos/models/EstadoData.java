package bancomer.api.pagarcreditos.models;

import android.util.Log;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

public class EstadoData implements ParsingHandler {

    /**
     *
     */
    private static final long serialVersionUID = -1880051584968511250L;

    private String estado;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        try {
            this.estado = parser.parseNextValue("estado");
        } catch (Exception e) {
            if (Server.ALLOW_LOG)
                Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
        }

    }
}
