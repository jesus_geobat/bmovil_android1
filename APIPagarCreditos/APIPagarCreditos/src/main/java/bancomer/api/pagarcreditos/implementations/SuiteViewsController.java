package bancomer.api.pagarcreditos.implementations;

import android.app.AlertDialog;

import bancomer.api.pagarcreditos.R;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;

public class SuiteViewsController extends BaseViewsController {

    public void showMenuSuite(boolean inverted) {
        showMenuSuite(inverted, null);
    }

    public void showMenuSuite(boolean inverted, String[] extras) {
        MenuSuiteDelegate suiteDelegate = (MenuSuiteDelegate) getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
        if (suiteDelegate == null) {
            suiteDelegate = new MenuSuiteDelegate();
            addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, suiteDelegate);
        }
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        boolean activado = session.isApplicationActivated();
        suiteDelegate.setbMovilSelected(extras != null && extras[0].equalsIgnoreCase("bmovilselected"));

		/*if (android.os.Build.VERSION.SDK_INT >= 11) {
            showViewController(MenuSuiteViewController.class, Intent.FLAG_ACTIVITY_CLEAR_TASK |
														Intent.FLAG_ACTIVITY_NEW_TASK, inverted);
		} else {
			showViewController(MenuSuiteViewController.class, IntentCompat.FLAG_ACTIVITY_CLEAR_TASK |
																Intent.FLAG_ACTIVITY_NEW_TASK, inverted);
		}*/
        showViewController(SuiteAppPagoCreditoApi.getInstance().getIntentMenuSuit().getClass());
        //showViewController(MenuSuiteViewController.class, Intent.FLAG_ACTIVITY_CLEAR_TOP, inverted);
    }

    public void showContactanos() {
        AlertDialog.Builder contactDialog = new AlertDialog.Builder(currentViewControllerApp);
        contactDialog.setTitle(currentViewControllerApp.getString(R.string.menuSuite_callTitle));

        if (Tools.hasPhoneAbility(currentViewControllerApp)) {
            contactDialog.setMessage(currentViewControllerApp.getString(R.string.menuSuite_callMessage));
            contactDialog.setPositiveButton(currentViewControllerApp.getString(R.string.menuSuite_callLocalNumberFormatted), (MenuSuiteViewController) currentViewControllerApp);
            contactDialog.setNeutralButton(currentViewControllerApp.getString(R.string.menuSuite_callAwayNumberFormatted), (MenuSuiteViewController) currentViewControllerApp);
            contactDialog.setNegativeButton(currentViewControllerApp.getString(R.string.common_cancel), (MenuSuiteViewController) currentViewControllerApp);
        } else {
            contactDialog.setMessage(currentViewControllerApp.getString(R.string.menuSuite_noCallMessage));
            contactDialog.setNeutralButton(currentViewControllerApp.getString(R.string.common_accept), null);
        }

        contactDialog.setCancelable(false);
        contactDialog.show();
    }
//
//	public void showContratacionPendiente() {
//
//		//TODO
//	}

}
