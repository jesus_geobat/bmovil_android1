package bancomer.api.pagarcreditos.models;

import android.util.Log;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class SimulaImportesPagoCreditoData implements ParsingHandler {

    private static final long serialVersionUID = 1L;

    private String numeroCredito;
    private String tipoMoneda;
    private String impPagoRecibos;
    private String impPagoRecibosMoneda;
    private String impAntDif;
    private String impAntDifMoneda;
    private String impTotal;
    private String impTotalMoneda;
    private String reduccion;

    public SimulaImportesPagoCreditoData() {
        this.numeroCredito = "";
        this.tipoMoneda = "";
        this.impPagoRecibos = "";
        this.impPagoRecibosMoneda = "";
        this.impAntDif = "";
        this.impAntDifMoneda = "";
        this.impTotal = "";
        this.impTotalMoneda = "";
        this.reduccion = "";
    }

    public String getNumeroCredito() {
        return numeroCredito;
    }

    public void setNumeroCredito(String numeroCredito) {
        this.numeroCredito = numeroCredito;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public String getImpPagoRecibos() {
        return impPagoRecibos;
    }

    public void setImpPagoRecibos(String impPagoRecibos) {
        this.impPagoRecibos = impPagoRecibos;
    }

    public String getImpPagoRecibosMoneda() {
        return impPagoRecibosMoneda;
    }

    public void setImpPagoRecibosMoneda(String impPagoRecibosMoneda) {
        this.impPagoRecibosMoneda = impPagoRecibosMoneda;
    }

    public String getImpAntDif() {
        return impAntDif;
    }

    public void setImpAntDif(String impAntDif) {
        this.impAntDif = impAntDif;
    }

    public String getImpAntDifMoneda() {
        return impAntDifMoneda;
    }

    public void setImpAntDifMoneda(String impAntDifMoneda) {
        this.impAntDifMoneda = impAntDifMoneda;
    }

    public String getImpTotal() {
        return impTotal;
    }

    public void setImpTotal(String impTotal) {
        this.impTotal = impTotal;
    }

    public String getImpTotalMoneda() {
        return impTotalMoneda;
    }

    public void setImpTotalMoneda(String impTotalMoneda) {
        this.impTotalMoneda = impTotalMoneda;
    }

    public String getReduccion() {
        return reduccion;
    }

    public void setReduccion(String reduccion) {
        this.reduccion = reduccion;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        try {
            this.numeroCredito = parser.parseNextValue("numeroCredito");
            this.tipoMoneda = parser.parseNextValue("tipoMoneda");
            this.impPagoRecibos = parser.parseNextValue("impPagoRecibos");
            this.impPagoRecibosMoneda = parser.parseNextValue("impPagoRecibosMoneda");
            this.impAntDif = parser.parseNextValue("impAntDif");
            this.impAntDifMoneda = parser.parseNextValue("impAntDifMoneda");
            this.impTotal = parser.parseNextValue("impTotal");
            this.impTotalMoneda = parser.parseNextValue("impTotalMoneda");
            this.reduccion = parser.parseNextValue("reduccion");
        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
        }
    }
}
