/**
 *
 */
package bancomer.api.pagarcreditos.implementations;

import android.util.Log;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.gui.delegates.PagarCreditoDelegate;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.to.ParamTo;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * @author lbermejo
 */
public class ConfirmacionServiceProxy implements IConfirmacionServiceProxy {

    /**
     */
    private static final long serialVersionUID = 3925667201345924488L;
    private BaseDelegateCommons baseDelegateCommons;

    public ConfirmacionServiceProxy(BaseDelegateCommons bdc) {
        this.baseDelegateCommons = bdc;
    }

    @Override
    public ArrayList<Object> getListaDatos() {
        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getListaDatos >> delegate");

        final ConfirmacionDelegate delegate = (ConfirmacionDelegate) baseDelegateCommons;
        ArrayList<Object> list = delegate.consultaOperationsDelegate().getDatosTablaConfirmacion();

        return list;
    }

    @Override
    public ConfirmacionViewTo showFields() {
        ConfirmacionViewTo to = new ConfirmacionViewTo();
        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy showFields >> delegate");

        final ConfirmacionDelegate delegate = (ConfirmacionDelegate) baseDelegateCommons;
        to.setShowContrasena(delegate.consultaDebePedirContrasena());
        to.setShowNip(delegate.consultaDebePedirNIP());
        to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
        to.setShowCvv(delegate.consultaDebePedirCVV());
        to.setShowTarjeta(delegate.mostrarCampoTarjeta());

        to.setInstrumentoSeguridad(delegate.consultaTipoInstrumentoSeguridad());
        to.setTokenAMostrar(delegate.tokenAMostrar());

        to.setTextoAyudaInsSeg(
                delegate.getTextoAyudaInstrumentoSeguridad(
                        to.getInstrumentoSeguridad()));
        /*
			//mostrarContrasena(confirmacionDelegate.consultaDebePedirContrasena());
			//mostrarNIP(confirmacionDelegate.consultaDebePedirNIP());
			//mostrarASM(confirmacionDelegate.consultaInstrumentoSeguridad());
			//mostrarCVV(confirmacionDelegate.consultaDebePedirCVV());
			//mostrarCampoTarjeta(confirmacionDelegate.mostrarCampoTarjeta());  */
        return to;
    }

    @Override
    public Integer getMessageAsmError(Constants.TipoInstrumento tipoInstrumento) {

        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getMessageAsmError >> delegate");

        int idMsg = 0;
        switch (tipoInstrumento) {
            case OCRA:
                idMsg = R.string.confirmation_ocra;
                break;
            case DP270:
                idMsg = R.string.confirmation_dp270;
                break;
            case SoftToken:
                if (SuiteAppPagoCreditoApi.getSofttokenStatus()) {
                    idMsg = R.string.confirmation_softtokenActivado;
                } else {
                    idMsg = R.string.confirmation_softtokenDesactivado;
                }
                break;
            default:
                break;
        }

        return idMsg;
		
		/*switch (tipoInstrumentoSeguridad) {
			case OCRA:
				mensaje += getEtiquetaCampoOCRA();
				break;
			case DP270:
				mensaje += getEtiquetaCampoDP270();
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					mensaje += getEtiquetaCampoSoftokenActivado();
				} else {
					mensaje += getEtiquetaCampoSoftokenDesactivado();
				}
				break;
			default:
				break;
		}
		*/
    }

    @Override
    public String loadOtpFromSofttoken(Constants.TipoOtpAutenticacion tipoOtpAutenticacion) {
        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy loadOtpFromSofttoken >> delegate");

        final ConfirmacionDelegate delegate = (ConfirmacionDelegate) baseDelegateCommons;
        String res = delegate.loadOtpFromSofttoken(tipoOtpAutenticacion);
        return res;
    }

    @Override
    public Boolean doOperation(ParamTo to) {
        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy doOperation >> delegate");

        final ConfirmacionDelegate delegate = (ConfirmacionDelegate) baseDelegateCommons;
        ConfirmacionViewTo params = (ConfirmacionViewTo) to;
        ConfirmacionViewController caller = new ConfirmacionViewController();
        caller.setDelegate(delegate);
        caller.setParentViewsController(((BmovilViewsController)
                SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getViewsController()));
        delegate.setConfirmacionViewController(caller);

        try {
            if (delegate.consultaOperationsDelegate() instanceof PagarCreditoDelegate) {
                delegate.consultaOperationsDelegate().realizaOperacion(
                        caller, params.getContrasena(), params.getNip(), params.getAsm(),
                        params.getTarjeta(), params.getCvv());
            } else {
                delegate.consultaOperationsDelegate().realizaOperacion(
                        caller, params.getContrasena(), params.getNip(),
                        params.getAsm(), params.getTarjeta());
            }
            return Boolean.TRUE;


        } catch (Exception e) {
            //TODO Log
            return Boolean.FALSE;
        }

    }

    @Override
    public Integer consultaOperationsIdTextoEncabezado() {

        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy consultaOperationsIdTextoEncabezado >> delegate");
        //getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()
        final ConfirmacionDelegate delegate = (ConfirmacionDelegate) baseDelegateCommons;
        int res = delegate.consultaOperationsDelegate().getTextoEncabezado();
        return res;
    }

    @Override
    public BaseDelegateCommons getDelegate() {
        return baseDelegateCommons;
    }


}
