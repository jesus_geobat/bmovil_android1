package bancomer.api.pagarcreditos.models;

public class PagoDeCredito {
    private String tipoCredito;
    private String numCredito;
    private String cuentaRetiro;
    private String nombreBeneficiario;
    private String importePago;
    private String importePagoMoneda;
    private String importeLiq;
    private String importeLiqMoneda;
    private String tipoPago;
    private String importePagoRecibos;
    private String importePagoRecibosMoneda;
    private String importeAntDif;
    private String importeAntDifMoneda;
    private String importeTotal;
    private String importeTotalMoneda;
    private String fechaPago;
    private String tipoAnticipo;

    public PagoDeCredito() {
        tipoCredito = "";
        numCredito = "";
        cuentaRetiro = "";
        nombreBeneficiario = "";
        importePago = "";
        importePagoMoneda = "";
        importeLiq = "";
        importeLiqMoneda = "";
        tipoPago = "";
        importePagoRecibos = "";
        importePagoRecibosMoneda = "";
        importeAntDif = "";
        importeAntDifMoneda = "";
        importeTotal = "";
        importeTotalMoneda = "";
        fechaPago = "";
        tipoAnticipo = "";
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public String getNumCredito() {
        return numCredito;
    }

    public void setNumCredito(String numCredito) {
        this.numCredito = numCredito;
    }

    public String getCuentaRetiro() {
        return cuentaRetiro;
    }

    public void setCuentaRetiro(String cuentaRetiro) {
        this.cuentaRetiro = cuentaRetiro;
    }

    public String getNombreBeneficiario() {
        return nombreBeneficiario;
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }

    public String getImportePago() {
        return importePago;
    }

    public void setImportePago(String importePago) {
        this.importePago = importePago;
    }

    public String getImportePagoMoneda() {
        return importePagoMoneda;
    }

    public void setImportePagoMoneda(String importePagoMoneda) {
        this.importePagoMoneda = importePagoMoneda;
    }

    public String getImporteLiq() {
        return importeLiq;
    }

    public void setImporteLiq(String importeLiq) {
        this.importeLiq = importeLiq;
    }

    public String getImporteLiqMoneda() {
        return importeLiqMoneda;
    }

    public void setImporteLiqMoneda(String importeLiqMoneda) {
        this.importeLiqMoneda = importeLiqMoneda;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getImportePagoRecibos() {
        return importePagoRecibos;
    }

    public void setImportePagoRecibos(String importePagoRecibos) {
        this.importePagoRecibos = importePagoRecibos;
    }

    public String getImportePagoRecibosMoneda() {
        return importePagoRecibosMoneda;
    }

    public void setImportePagoRecibosMoneda(String importePagoRecibosMoneda) {
        this.importePagoRecibosMoneda = importePagoRecibosMoneda;
    }

    public String getImporteAntDif() {
        return importeAntDif;
    }

    public void setImporteAntDif(String importeAntDif) {
        this.importeAntDif = importeAntDif;
    }

    public String getImporteAntDifMoneda() {
        return importeAntDifMoneda;
    }

    public void setImporteAntDifMoneda(String importeAntDifMoneda) {
        this.importeAntDifMoneda = importeAntDifMoneda;
    }

    public String getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(String importeTotal) {
        this.importeTotal = importeTotal;
    }

    public String getImporteTotalMoneda() {
        return importeTotalMoneda;
    }

    public void setImporteTotalMoneda(String importeTotalMoneda) {
        this.importeTotalMoneda = importeTotalMoneda;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getTipoAnticipo() {
        return tipoAnticipo;
    }

    public void setTipoAnticipo(String tipoAnticipo) {
        this.tipoAnticipo = tipoAnticipo;
    }
}
