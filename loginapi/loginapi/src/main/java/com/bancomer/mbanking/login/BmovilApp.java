package com.bancomer.mbanking.login;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.login.BmovilViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

import suitebancomer.aplicaciones.bmovil.classes.model.login.LoginData;
import suitebancomer.classes.gui.controllers.login.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

@SuppressLint("HandlerLeak")
public class BmovilApp extends BaseSubapplication {

	/**
	 * Message for the handler, indicates that session must be expired.
	 */
	private static final int LOGOUT_MESSAGE = 0;

	/**
	 * The local session validity timer.
	 */
	private Timer sessionTimer = null;

	/**
	 * Task that manages session logout
	 */
	public SessionLogoutTask logoutTask;

	public static int STEP_TOTAL = 0;
	public static int STEP_ACTUAL = 0;


	//////////////////////////////////////////////////////////////
	//					Methods									//
	//////////////////////////////////////////////////////////////    

	/**
	 * Constructor básico, se inicializa aqui debido a que no puede extender de un objeto Aplicación, ya que no es posible
	 * que exista mas de uno
	 */
	public BmovilApp(final SuiteAppLogin suiteApp) {
		super(suiteApp);
		viewsController = new BmovilViewsController(this);
	}

	public BmovilViewsController getBmovilViewsController() {
		return (BmovilViewsController)viewsController;
	}

	
	public Timer getSessionTimer() {
		return sessionTimer;
	}
	public void setSessionTimer(final Timer sessionTimer) {
		this.sessionTimer=sessionTimer;
		
	}



	public void initTimer() {
		sessionTimer = new Timer(true);
		logoutTask = new SessionLogoutTask();
	}
	
	public TimerTask getLogoutTask() {
		return logoutTask;
	}
	
	//////////////////////////////////////////////////////////////////////////////
	//                Network operations										//
	//////////////////////////////////////////////////////////////////////////////
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	@Override
	protected void invokeNetworkOperation(final int operationId,
										  final Hashtable<String, ?> params,
										  final BaseViewController caller,
										  final String progressLabel,
										  final String progressMessage,
										  final boolean callerHandlesError,final Class<?> objResponse) {
		//we stop the timer during server calls
		if (logoutTask != null) {
			logoutTask.cancel();
		}
		super.invokeNetworkOperation(operationId, params, caller, progressLabel, progressMessage, callerHandlesError, objResponse);
	}

	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	@Override
	protected void analyzeNetworkResponse(final Integer operationId,
										  final ServerResponse response,
										  final Throwable throwable,
										  final BaseViewController caller,
										  final boolean callerHandlesError) {

		if (operationId.intValue() == Server.CLOSE_SESSION_OPERATION) {

	//		cancelOngoingNetworkOperations();

			Session.getInstance(SuiteAppLogin.appContext).setValidity(Session.INVALID_STATUS);
			Session.getInstance(SuiteAppLogin.appContext).saveRecordStore();
			new Thread(new Runnable() {
				public void run() {
					try {
						System.gc();
						//Session.getInstance(SuiteApp.appContext).storeSession();
						//Incidencia 22218
						
					} catch (Throwable t) {
						if(Server.ALLOW_LOG)
							Log.d("BmovilApp", t.getMessage());
					} finally {
						suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp().runOnUiThread(new Runnable() {
							public void run() {
								
								// invalid session, request login
								sessionTimer.cancel();
								suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp().hideCurrentDialog();
								
//								suiteApp.cierraAplicacionBmovil();
								if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Si se cerro la sesión");
							}
						});
					}
				}
			}).start();

		} else {
			super.analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);
			resetLogoutTimer();
		}
	}

	/**
	 * Initiates a session closure process
	 */
	public void closeSession(final boolean isHomeKeyPressed) {
		this.setHomeKeyPressed(isHomeKeyPressed);
		final Session session = Session.getInstance(suiteApp.getApplicationContext());
		closeSession(session.getUsername(), session.getIum(), session.getClientNumber());
	}
	
	/**
	 * Initiates a session closure process, cuando se borran los datos y es
	 * necesario hacer el cierre de sesion
	 */
	public void closeSession(final String username,final  String ium,final  String client) {
		applicationLogged = false;
		
		// close session
		System.gc();
		final Hashtable<String, String> params = new Hashtable<String, String>();

		params.put(Server.USERNAME_PARAM, username);
		params.put(ServerConstants.IUM_ETIQUETA, ium);
		params.put(Server.USER_ID_PARAM, client);
		
		BaseViewController caller = null;
		if (viewsController != null) {
			caller = (BaseViewController)viewsController.getCurrentViewController();
		}

		invokeNetworkOperation(Server.CLOSE_SESSION_OPERATION, params, caller,
				suiteApp.getApplicationContext().getString(R.string.alert_closeSession),
				suiteApp.getApplicationContext().getString(R.string.alert_thank_you), true,LoginData.class);
		
		if(Server.ALLOW_LOG) Log.e("CerrarSesion", "La sesion fue cerrada satisfactoriamente.");
	}
	
	public void closeBmovilAppSession(final BaseViewController caller) {
		applicationLogged = false;
		final Session session = Session.getInstance(suiteApp.getApplicationContext());
		final String username = session.getUsername();
		final String ium = session.getIum();
		final String client = session.getClientNumber();
				
		// close session
		System.gc();
		final Hashtable<String, String> params = new Hashtable<String, String>();

		params.put(Server.USERNAME_PARAM, username);
		params.put(ServerConstants.IUM_ETIQUETA, ium);
		params.put(Server.USER_ID_PARAM, client);
		
		//BaseViewController caller = null;
		//if (viewsController != null) {
			//caller = viewsController.getCurrentViewController();
		//}

		invokeNetworkOperation(Server.CLOSE_SESSION_OPERATION, params, caller,
				suiteApp.getApplicationContext().getString(R.string.alert_closeSession),
				suiteApp.getApplicationContext().getString(R.string.alert_thank_you), true,LoginData.class);
		
		if(Server.ALLOW_LOG) Log.e("CerrarSesion", "La sesion fue cerrada satisfactoriamente.");
	}

	public int getApplicationStatus() {
		return Session.getInstance(SuiteAppLogin.appContext).getPendingStatus();
	}
	
	/////////////////////////////////////////////////////////////////////////////
	//                Local session management                                 //
	/////////////////////////////////////////////////////////////////////////////
	/**
	 * Records a user activity event (keystroke), in order to
	 * check session validity
	 */
	public void userActivityEvent() {
		resetLogoutTimer();
	}

	/**
	 * Logs the user out and discards the current session.
	 */
	public synchronized void logoutApp(final boolean isHomeKeyPressed){
		final Session session = Session.getInstance(suiteApp.getApplicationContext());

		if(session.getValidity() == Session.VALID_STATUS){
			session.setValidity(Session.INVALID_STATUS);
			closeSession(isHomeKeyPressed);
		}
	}

	/**
	 * Called whenever the user performs an action or touches the screen
	 */
	public void resetLogoutTimer() {
		if (logoutTask != null) {
			logoutTask.cancel();	
		}
		if(sessionTimer != null) {
			logoutTask = new SessionLogoutTask();
			if(Session.getInstance(suiteApp.getApplicationContext()).getValidity() == Session.VALID_STATUS){
				final long timeoutMilis = Session.getInstance(suiteApp.getApplicationContext()).getTimeout();
				sessionTimer.schedule(logoutTask, timeoutMilis);
			}
		}
	}

	/**
	 * We must use a TimerTask class to execute logout actions after the
	 * timer runs out.
	 */
	private class SessionLogoutTask extends TimerTask {

		@Override
		public void run() {
			applicationLogged = false;
			final Message msg = new Message();
			msg.what = BmovilApp.LOGOUT_MESSAGE;
			mApplicationHandler.sendMessage(msg);
		}

	}

	/**
	 * Use handler to respond to logout messages given by the timer.
	 */
	private Handler mApplicationHandler = new Handler() {

		public void handleMessage(final Message msg) {

			if(msg.what == BmovilApp.LOGOUT_MESSAGE){
				logoutApp(false);
			}
		}
	};


	
}
