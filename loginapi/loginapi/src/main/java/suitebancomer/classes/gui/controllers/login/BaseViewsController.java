package suitebancomer.classes.gui.controllers.login;

import android.app.Activity;

import java.lang.reflect.Method;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;

public class BaseViewsController extends BaseViewsControllerCommons {
	
	private static Method methodOverridePendingTransition;
	

	private boolean isActivityChanging;
	
	protected BaseViewController rootViewController;
	protected BaseViewController currentViewControllerApp;
	protected static BaseViewController currentViewController;
	
	static {
		final Method[] actMethods = Activity.class.getMethods();
		final int methodSize = actMethods.length;
		for (int i=0; i<methodSize; i++) {
			if (actMethods[i].getName().equals("overridePendingTransition")) {
				methodOverridePendingTransition = actMethods[i];
				break;
			}
		}
	}
	
	public BaseViewController getRootViewController() {
		return rootViewController;
	}
	
	public BaseViewController getCurrentViewControllerApp() {
		return currentViewControllerApp;
	}
	
	public BaseViewController getCurrentViewController() {
		return currentViewController;
	}
	
	public void setCurrentActivityApp(final BaseViewController currentViewController) {
		setCurrentActivityApp(currentViewController, false); 
	}
	
	public void setCurrentActivityApp(final BaseViewController currentViewController,final boolean asRootViewController) {
		this.currentViewControllerApp = currentViewController; 
		if (rootViewController == null) {
			this.rootViewController = currentViewController;
		}
		isActivityChanging = false;
		
		BaseViewsController.currentViewController = currentViewController; 
	}
	
	public boolean isActivityChanging() {
		return isActivityChanging;
	}
	
	public void setActivityChanging(final boolean flag) {
		isActivityChanging = flag;
	}

    public void onUserInteraction() {}
    
    public boolean consumeAccionesDePausa() {
    	return false;
    }
    
    public boolean consumeAccionesDeReinicio() {
    	return false;
    }
    
    public boolean consumeAccionesDeAlto() {
    	return false;
    }
    
    /**
     * Muestra el menu inicial de la aplicación que implemente el método.
     */
    public void showMenuInicial(){}
}
