package suitebancomer.aplicaciones.bmovil.classes.model.login;

import android.util.Log;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by joseangelgonzalezmagana on 08/08/16.
 */
public class AllowedData implements ParsingHandler {

    private String arquitectura;
    private String edocuenta;
    private String tokenCorporativo;

    public String getTokenCorporativo() {
        return tokenCorporativo;
    }

    public void setTokenCorporativo(String tokenCorporativo) {
        this.tokenCorporativo = tokenCorporativo;
    }

    public String getEdocuenta() {
        return edocuenta;
    }

    public void setEdocuenta(String edocuenta) {
        this.edocuenta = edocuenta;
    }



    public String getArquitectura() {
        return arquitectura;
    }

    public void setArquitectura(String arquitectura) {
        this.arquitectura = arquitectura;
    }


    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        arquitectura = parser.parseNextValue("convivencia").replace("\u0093", "\"").replace("\u0094", "\"").replace("<&#33;---&#8226;--->","");
        edocuenta = parser.parseNextValue("edoCuenta").replace("\u0093", "\"").replace("\u0094", "\"").replace("<&#33;---&#8226;--->","");
        tokenCorporativo = parser.parseNextValue("tokenCorporativo").replace("\u0093", "\"").replace("\u0094", "\"").replace("<&#33;---&#8226;--->","");
    }

}
