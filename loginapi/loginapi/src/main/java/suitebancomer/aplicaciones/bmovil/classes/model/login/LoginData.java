/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomer.aplicaciones.bmovil.classes.model.login;

import android.util.Log;

import com.bancomer.base.SuiteApp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Compania;
import bancomer.api.common.model.Convenio;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Catalog;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Campania;

/**
 * LoginData wraps the information involved in the response from the server
 * after a login operation.
 *
 * @author Stefanini IT Solutions.
 */

public class LoginData  implements ParsingHandler {


    /**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -400496845278428572L;

	/**
     * The Bancomer token.
     */
    private String bancomerToken = null;
    
    /**
     * Vía 1 o 2
     */
    private String via = null;

    /**
     * The client identifier for session purposes.
     */
    private String clientNumber = null;

    /**
     * The server date.
     */
    private String serverDate = null;

    /**
     * The server time.
     */
    private String serverTime = null;

    /**
     * The user account list.
     */
    private Account[] accounts = null;

    /**
     * The catalogs.
     */
    private Catalog[] catalogs = null;

    /**
     * The version of each catalog.
     */
    private String[] catalogVersions = null;

    /**
     * The optional message to show after a successful login.
     */
    private String message = null;

    /**
     * The message to show that is an update available for the application.
     */
    private String updateMessage = null;

    /**
     * URL for the case in that the response is an update message.
     */
    private String updateURL = null;
    
    /**
     * Nombre del cliente
     */
    private String nombreCliente;
    
    /**
     * Compañia a la que pertenece el teléfono del cliente
     */
    private String companiaTelCliente;
    
    /**
     * Correo electrónico del cliente
     */
    private String emailCliente;
    
    /**
     * Perfil del cliente
     */
    private String perfilCliente;
    
    /**
     * Tipo de instrumento de seguridad del cliente
     */
    private String insSeguridadCliente;
    
    /**
     * Estatus del servicio
     */
    private String estatusServicio;
    
    /**
     * The session timeout in minutes.
     */
    private int timeout = 1;
    
    private String estatusInstrumento;
    
    private CatalogoVersionado catalogoTA;
    
    private CatalogoVersionado catalogoDineroMovil;
    
    private CatalogoVersionado catalogoConvenios;
    
    private CatalogoVersionado catalogoMantenimientoSPEI;
    
    private String autenticacionJson;
    
    private String limiteOperacion;
    
    private String jsonRapidos;
    
    /**
     * Estatus de alertas (alta, modificacion o vacio).
     */
    private String estatusAlertas;
    
    /**
     * Fecha de contratacci�n de bancomer m�vil.
     */
    private String fechaContratacion;
    
    private String candidatoPromociones;
    private ArrayList<Campania> listaPromocionesJSON;
    private String contratoBBVAlink;
    private String marcaTendero;
    
    /**
     * Catálogo de compañías telefonicas.
     */
    private String catTelefonicas;
  //One Click
    /** json de promociones
     */
    private String isPromocion;
	private String jSonPromociones;

/////////////////////////////////////////
	
	//CS raw Json.
    private String csJson;	
    
    //HS raw Json.
    private String hsJson;
	
////////////////////////////////////////
	
    /**
     * Default constructor.
     */
    public LoginData() {
    }
    
    /**
	 * @return the emailCliente
	 */
	public String getEmailCliente() {
		return emailCliente;
	}

	/**
	 * @return the estatusInstrumento
	 */
	public String getEstatusInstrumento() {
		return estatusInstrumento;
	}

	/**
     * Get the client identifier for session purposes.
     * @return the client identifier for session purposes
     */
    public String getClientNumber() {
        return clientNumber;
    }

    /**
     * get the server date.
     * @return the server date
     */
    public String getServerDate() {
        return serverDate;
    }

    /**
     * Get the server time.
     * @return the server time
     */
    public String getServerTime() {
        return serverTime;
    }

    /**
     * Get via
     * @return via
     */
    public String getVia() {
        return via;
    }
    
    /**
     * Get the Bancomer token.
     * @return the Bancomer token
     */
    public String getBancomerToken() {
        return bancomerToken;
    }

    /**
     * get the user's accounts.
     * @return the user's accounts
     */
    public Account[] getAccounts() {
        return accounts;
    }

    /**
     * Get the catalogs.
     * @return the catalogs
     */
    public Catalog[] getCatalogs() {
        return catalogs;
    }
    
    /**
     * Get the authentication JSON.
     * @return Authentication JSON string.
     */
    public String getAuthenticationJson() {
    	return this.autenticacionJson;
    }
    
    public CatalogoVersionado getCatalogoTiempoAire() {
    	return catalogoTA;
    }
    
    public CatalogoVersionado getCatalogoDineroMovil() {
    	return catalogoDineroMovil;
    }

    public CatalogoVersionado getCatalogoServicios() {
    	return catalogoConvenios;
    }
    
    public CatalogoVersionado getCatalogoMantenimientoSPEI() {
    	return catalogoMantenimientoSPEI;
    }
    
    /**
     * Returns the catalog version array.
     * @return the catalog version array
     */
    public String[] getCatalogVersions() {
        return catalogVersions;
    }

    /**
     * Get the optional message.
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Get the updating message.
     * @return the updating message
     */
    public String getUpdateMessage() {
        return updateMessage;
    }
    /**
     * Get the updating URL.
     * @return the URL
     */
    public String getUpdateURL() {
        return updateURL;
    }

    /**
     * Get the session timeout.
     * @return the session timeout, in minutes
     */
    public int getTimeout() {
        return timeout;
    }
    
    public String getPerfiCliente() {
    	return perfilCliente;
    }
    
    public String getInsSeguridadCliente() {
    	return insSeguridadCliente;
    }
    
    public String getEstatusServicio() {
    	return estatusServicio;
    }
    
    public String getEstatusIS() {
    	return estatusInstrumento;
    }

    public String getCompaniaTelCliente() {
		return companiaTelCliente;
	}
    
    public String getJsonRapidos() {
    	return jsonRapidos;
    }

	/**
	 * @return the estatusAlertas
	 */
	public String getEstatusAlertas() {
		return estatusAlertas;
	}

	/**
	 * @return the fechaContratacion
	 */
	public String getFechaContratacion() {
		return fechaContratacion;
	}

	/**
	 * @return the catTelefonicas
	 */
	public String getCatTelefonicas() {
		return catTelefonicas;
	}

	/**
	 * @return the nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}
	//One click
		public String getIsPromocion() {
			return isPromocion;
		}

		public void setIsPromocion(final String isPromocion) {
			this.isPromocion = isPromocion;
		}
		
		
		/////////////////////////////////////

		//Set and Get which belong to csJson.
		public String getCSJson(){
			return csJson;
		}

		public void setCSJson(final String csJson)
		{
			this.csJson=csJson;
		}

		//Set and Get which belong to hsJson


		public String getHSJson()
		{
			return hsJson;
		}

		public void setHSJson(final String hsJson)
		{
			this.hsJson=hsJson;
		}


		/////////////////////////////////////
	
	/**
     * Process the login response and store the attributes.
     * @param parser reference to the parser
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
		public void process(final Parser parser) throws IOException, ParsingException {
	    	//MODIFIED BY Michael Andrade
	    	//Parsing new server response
	    	clientNumber = parser.parseNextValue("TE");
	    	serverDate = parser.parseNextValue("FE");
	    	serverTime = parser.parseNextValue("HR");
	    	final String timeoutStr = parser.parseNextValue("TO");
	        try {
	            timeout = Integer.parseInt(timeoutStr);
	        } catch (Throwable th) {
	            timeout = 1;
	        }
	        accounts = parseAccounts(parser, serverDate);
	        catalogs = new Catalog[4];
	        catalogVersions = new String[8];
	        final String next = parseCatalogs(parser, catalogs, catalogVersions);
	        message = next.substring(2, next.length()); //MP value is required
	        updateMessage = parser.parseNextValue("ME", false);
	        updateURL = parser.parseNextValue("UR", false);
	        via = parser.parseNextValue("VA");
	        estatusInstrumento = parser.parseNextValue("EI");
	        companiaTelCliente = parser.parseNextValue("CM", false);
	        emailCliente = parser.parseNextValue("EM", false);
	        perfilCliente = parser.parseNextValue("PR", false);
	        insSeguridadCliente = parser.parseNextValue("IS", false);
	        

	        
	        jsonRapidos = parser.parseNextValue("RP", false);
	        if(!Tools.isEmptyOrNull(jsonRapidos))
	        	jsonRapidos = jsonRapidos.replaceAll("\":\"\"", "\":[]");
	        
	        // Se establece el json de rápidos como un arreglo vacio.
	        jsonRapidos = "{\"rapidas\":[]}";
	        
	        Session.getInstance(SuiteApp.appContext).parseRapidosJson(jsonRapidos);
	        String tmpEntity;
	        do {
	        	tmpEntity = parser.parseNextEntity();
	        } while(processAux(tmpEntity));

            processAux(parser, tmpEntity);

            nombreCliente = parser.parseNextValue("NO", false);
	    	autenticacionJson = parser.parseNextValue("AU", false);
	    	limiteOperacion = parser.parseNextValue("LO");
	    	
	    	estatusAlertas = parser.parseNextValue("EA");
	    	fechaContratacion = parser.parseNextValue("FC");
	    	if (null == fechaContratacion) {
	    		fechaContratacion = "";
	    		//One Click
	        	if(estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
	        	isPromocion=parser.parseNextValue("PM");

	        	}
	            //Termina One click
	    	}

            processAux2(parser);

            processAux3(parser);
            //////////////////////END SPEI OPERATIVA DIARIA./////////////////////////////////
	    	
	    	
	    	Session.getInstance(SuiteApp.appContext).setAuthenticationJson(autenticacionJson);
	    	double limite;
	    	if(!Constants.SOBREESCRIBIR_LO)
	    		limite = Double.parseDouble(limiteOperacion) / 100.0;
	    	else
	    		limite = Constants.LO_SOBREESCRITO;
	    	Autenticacion.getInstance().setLimiteOperacion(limite);
       }

    private void processAux3(final Parser parser) throws ParsingException, IOException {
        if(estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)){
            hsJson = parser.parseNextValue("HS",false);
            if(!Tools.isEmptyOrNull(hsJson))
                Session.getInstance(SuiteApp.appContext).saveCatalogoHorasServicio(hsJson);

            if(ServerCommons.ALLOW_LOG) Log.e("switch value", String.valueOf(Session.getSwitchSPEI()));
        }
    }

    private void processAux2(final Parser parser) throws ParsingException, IOException {
        if(!estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
            catTelefonicas = parser.parseNextValue("TM", false);
            if(!Tools.isEmptyOrNull(catTelefonicas))
                Session.getInstance(SuiteApp.appContext).saveCatalogoCompaniasTelefonicas(catTelefonicas);
        }

        if(estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)){
            candidatoPromociones = parser.parseNextValue("PM");
             if(candidatoPromociones.compareTo("SI")==0){
                    jSonPromociones= parser.parseNextValue("LP", false);

                    if(!Tools.isEmptyOrNull(jSonPromociones))
                        jSonPromociones = jSonPromociones.replaceAll("\":\"\"", "\":[]");



                    Session.getInstance(SuiteApp.appContext).parsePromocionesJson(jSonPromociones);
             }else{
                final String listaPromociones = parser.parseNextValue("LP", false);
                 Session.getInstance(SuiteApp.appContext).parsePromocionesJson(listaPromociones);
                listaPromocionesJSON = parseListaPromociones(listaPromociones);
            }


            contratoBBVAlink = parser.parseNextValue("LK");

            final String stringMarcaTendero = parser.parseNextValue("TN", false);
            marcaTendero = parseMarcaTendero(stringMarcaTendero);
        }

        ////////////////////SPEI OPERATIVA DIARIA/////////////////////////////////////////

        if(estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)){
        Session.setListA(new ArrayList<Account>());
        Session.setListB(new ArrayList<Account>());
        csJson= parser.parseNextValue("CS",false);
        if(!Tools.isEmptyOrNull(csJson))
            Session.getInstance(SuiteApp.appContext).parserCS(csJson);

        }


    }

    private Boolean processAux( final String tmpEntity){
        if(!tmpEntity.equals("TA") && !tmpEntity.contains("DM") && !tmpEntity.contains("SV") && !tmpEntity.contains("ST") && !tmpEntity.contains("MS")){
            return true;
        }else{
            return false;
        }
    }
    private void processAux(final Parser parser,  String tmpEntity) throws ParsingException, IOException {
        if (tmpEntity.contains("ST")) {
            estatusServicio = tmpEntity.substring(2);
            parser.parseNextValue("ST", false);
        } else {
            if (tmpEntity.contains("TA")) {
                catalogoTA = parseCompanyCatalog(parser);
                catalogVersions[4] = catalogoTA.getVersion();
                tmpEntity = parser.parseNextEntity();
            }

            if (tmpEntity.contains("DM")) {
                catalogoDineroMovil = parseCompanyCatalog(parser);
                catalogVersions[5] = catalogoDineroMovil.getVersion();
                tmpEntity = parser.parseNextEntity();
            }

            if (tmpEntity.contains("SV")) {
                catalogoConvenios = parseConvenioCatalog(parser);
                catalogVersions[6] = catalogoConvenios.getVersion();
                tmpEntity = parser.parseNextEntity();
            }

            if (tmpEntity.contains("MS")) {
                catalogoMantenimientoSPEI = parseSpeiCatalog(parser);
                catalogVersions[7] = catalogoMantenimientoSPEI.getVersion();
                tmpEntity = "";
            }

            if (tmpEntity.equals("")) {
                estatusServicio = parser.parseNextValue("ST");
            } else if (tmpEntity.length() > 3){
                estatusServicio = tmpEntity.substring(2, tmpEntity.length());
            } else {
                estatusServicio = "";
            }
        }
    }


    // private CatalogoVersionado parseMantenimientoSPEICatalog(Parser parser) {
		// TODO Auto-generated method stub
	//	return null;
	//}

	/**
     * Parse the account list.
     * @param parser reference to the parse
     * @param date the server date
     * @return the account list
     * @throws IOException on input output errors
     * @throws ParsingException on communication errors
     * @throws NumberFormatException on parsing errors
     */
    private static Account[] parseAccounts(final Parser parser,final  String date) throws IOException, NumberFormatException, ParsingException {
        String accountType;
        String currency;
        String accountNumber;
        String amount;
        String concept;
        String visible;
        String alias;
        parser.parseNextValue("CT");
        final int accounts = Integer.parseInt(parser.parseNextValue("OC"));
        Account[] result = new Account[accounts];
        Account account;
        for (int i = 0; i < accounts; i++) {
            accountType = parser.parseNextValue("TP");
            alias = parser.parseNextValue("AL");
            currency = parser.parseNextValue("DV");
            accountNumber = parser.parseNextValue("AS");
            amount = parser.parseNextValue("IM");
            concept = parser.parseNextValue("CP");
            visible = parser.parseNextValue("VI");
            account = new Account(accountNumber, Tools.getDoubleAmountFromServerString(amount), Tools.formatDate(date), "S".equals(visible), currency, accountType, concept, alias);
            result[i] = account;
        }
        return result;
    }

    /**
     * Parse a catalog.
     * @param parser a reference to the parser
     * @return the catalog
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     * @throws NumberFormatException on format errors
     */
    private static Catalog parseCatalog(final Parser parser) throws IOException, NumberFormatException, ParsingException {
       final Catalog result = new Catalog();
        String entry;
        String code;
        String value;
        final int entries = Integer.parseInt(parser.parseNextValue("OC"));
        for (int i = 0; i < entries; i++) {
            entry = parser.parseNextEntity(true);
            if (entry != null) {
               final int pos = entry.indexOf('-');
                code = entry.substring(0, pos);
                value = entry.substring(pos + 1);
                result.add(code, value);
            }
        }
        return result;
    }
    


    /**
     * Parse a catalog.
     * @param parser a reference to the parser
     * @return the catalog
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     * @throws NumberFormatException on format errors
     */
    private static CatalogoVersionado parseCompanyCatalog(final Parser parser) throws IOException, NumberFormatException, ParsingException {
        String nombre;
        String imagen;
        String clave;
        String montos;
        String orden;
        final String version = parser.parseNextValue("VE");
        final int entries = Integer.parseInt(parser.parseNextValue("OC"));
        CatalogoVersionado catalogoVersionado = new CatalogoVersionado(version);
        Compania compania;
        
        for (int i = 0; i < entries; i++) {
            nombre = parser.parseNextValue("OA");
            imagen = parser.parseNextValue("MG");
            clave = parser.parseNextValue("CV");
            montos = parser.parseNextValue("IM");
            orden = parser.parseNextValue("OR");
            
            compania = new Compania(nombre, imagen, clave, montos, orden);
            catalogoVersionado.agregarObjeto(compania);
        }
        return catalogoVersionado;
    }
    
    /**
     * Parse a catalog.
     * @param parser a reference to the parser
     * @return the catalog
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     * @throws NumberFormatException on format errors
     */
    private static CatalogoVersionado parseConvenioCatalog(final Parser parser) throws IOException, NumberFormatException, ParsingException {
        String nombre;
        String imagen;
        String clave;
        String orden;
        final String version = parser.parseNextValue("VE");
        final int entries = Integer.parseInt(parser.parseNextValue("OC"));
        final CatalogoVersionado catalogoVersionado = new CatalogoVersionado(version);
        Convenio convenio;
        
        for (int i = 0; i < entries; i++) {
        	clave = parser.parseNextValue("CI");
            imagen = parser.parseNextValue("MG");
            nombre = parser.parseNextValue("NM");
            orden = parser.parseNextValue("OR");
            
            convenio = new Convenio(nombre, imagen, clave, orden);
            catalogoVersionado.agregarObjeto(convenio);
        }
        return catalogoVersionado;
    }
    
    /**
	 * The SPEI companies catalog.
	 */
	CatalogoVersionado speiCatalog = null;
	
	/**
	 * @return The SPEI companies catalog.
	 */
	public CatalogoVersionado getSpeiCatalog() {
		return speiCatalog;
	}

   // private static CatalogoVersionado parseMantenimientoSPEICatalog(Parser parser){ //throws IOException, NumberFormatException, ParsingException {
    	private CatalogoVersionado parseSpeiCatalog(final Parser parser) {
    	/*String nombreCompania;
    	String nombreImagen;
    	String claveCompania;
    	String orden;*/
    	String nombre;
        String imagen = null;
        String clave;
        String orden;
        Compania compania;
        CatalogoVersionado catalog;
        try {
    	final String version = parser.parseNextValue("VE");
    	final int entries = Integer.parseInt(parser.parseNextValue("OC"));
    	catalog = new CatalogoVersionado(version);
    //	MantenimientoSpei mantenimientoSpei;
    	
    	for (int i = 0; i < entries; i++) {
    		nombre = parser.parseNextValue("OA");
    		imagen = parser.parseNextValue("MG");
    		clave = parser.parseNextValue("CV");
            orden = parser.parseNextValue("OR");
            compania = new Compania(nombre, imagen, clave,"", orden);
           // catalogoVersionado.agregarObjeto(mantenimientoSpei);
            catalog.agregarObjeto(compania);
        }
        }catch (Exception ex) {
        	if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while parsing the SPEI companies catalog.", ex);
    	catalog = null;
        }
    return speiCatalog = catalog;
    }
    
    
    private static ArrayList<Campania> parseListaPromociones(final String listaPromociones) {
    	
    	ArrayList<Campania> listaCampaniasJSON = null;
    	
    	if ((listaPromociones != null) && (!"".equals(listaPromociones))) {
    		listaCampaniasJSON = new ArrayList<Campania>();
    		JSONObject jsonObject = null;
        	
        	try {
        		jsonObject = new JSONObject(listaPromociones);
    		} catch (JSONException ex) {
    			jsonObject = null;
    			if(ServerCommons.ALLOW_LOG) Log.e("LoginData", "Error while trying to parse Json.", ex);
    		}
        	
        	if (null != jsonObject){

        		try {
        			final JSONArray arrayCampanias = jsonObject.getJSONArray("campanias");
    			
        			for (int i = 0; i < arrayCampanias.length(); i++) {
        				final JSONObject campaniaJSON = arrayCampanias.getJSONObject(i);
        				final String claveCampania = campaniaJSON.getString("cveCamp");
        				final String descripcionOferta = campaniaJSON.getString("desOferta");
        				final String monto = campaniaJSON.getString("monto");
    				
        				final Campania campania = new Campania(claveCampania, descripcionOferta, monto);
        				listaCampaniasJSON.add(campania);
    				}
    			
        		} catch (JSONException ex) {
        			if(ServerCommons.ALLOW_LOG) Log.e("LoginData", "Error while reading the JSON.", ex);
        		}
        	}
    	}
    	
    	return listaCampaniasJSON;
    }
    
    private String parseMarcaTendero(final String stringTendero) {
    	JSONObject jsonObject = null;
    	String tendero = null;
    	
    	if ((stringTendero != null) && (!"".equals(stringTendero))) {
    		try {
        		jsonObject = new JSONObject(stringTendero);
        		tendero = jsonObject.getString("tendero");
    		} catch (JSONException ex) {
    			jsonObject = null;
    			if(ServerCommons.ALLOW_LOG) Log.e("LoginData", "Error while trying to parse Json.", ex);
    		}
    	}
    	
    	return tendero;
    }
    
    /**
     * Parse the collection of catalogs.
     * @param parser a reference to the parser
     * @param catalogs (out) parsed catalogs
     * @param catalogVersions version of catalog
     * @return the optional message
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    private static String parseCatalogs(final Parser parser,
                                        final Catalog[] catalogs,final  String... catalogVersions)
    		throws IOException, ParsingException {
        String next = null;
        String entity;
        boolean finished = false;
        do {
            entity = parser.parseNextEntity();
            if (entity == null) {
                next = null;
                finished = true;
            } else {
               final ParseCatalogsAux parseCatalogsAux = new ParseCatalogsAux(parser, catalogs, next, entity, finished, catalogVersions).invoke();
                finished = parseCatalogsAux.isFinished();
                next = parseCatalogsAux.getNext();
            }
        } while (!finished);


        return next;
    }

    @Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

    private static class ParseCatalogsAux {
        private Parser parser;
        private Catalog[] catalogs;
        private String next;
        private String entity;
        private boolean finished;
        private String[] catalogVersions;

        public ParseCatalogsAux(final Parser parser, final Catalog[] catalogs,final String next,final String entity,final boolean finished, final String... catalogVersions) {
            this.parser = parser;
            this.catalogs = catalogs;
            this.next = next;
            this.entity = entity;
            this.finished = finished;
            this.catalogVersions = catalogVersions;
        }

        public String getNext() {
            return next;
        }

        public boolean isFinished() {
            return finished;
        }

        public ParseCatalogsAux invoke() throws IOException, ParsingException {
            if (entity.startsWith("C1")) {
                catalogVersions[0] = entity.substring(2);
                catalogs[0] = parseCatalog(parser);
                //System.out.println("Received Parsed Version C1=" + catalogVersions[0]);
            } else if (entity.startsWith("C4")) {
                catalogVersions[1] = entity.substring(2);
                catalogs[1] = parseCatalog(parser);
                //System.out.println("Received Parsed Version C2=" + catalogVersions[1]);
            } else if (entity.startsWith("C5")) {
                catalogVersions[2]=entity.substring(2);
                catalogs[2]=parseCatalog(parser);
                //System.out.println("Received Parsed Version C3=" + catalogVersions[2]);
            } else if (entity.startsWith("C8")) {
                catalogVersions[3] = entity.substring(2);
                catalogs[3] = parseCatalog(parser);
                //System.out.println("Received Parsed Version C4=" + catalogVersions[3]);
            } else if (entity.startsWith("TA")) {
                catalogVersions[4] = entity.substring(2);
                catalogs[4] = parseCatalog(parser);
                //System.out.println("Received Parsed Version C5=" + catalogVersions[4]);
            } else if (entity.startsWith("DM")) {
                catalogVersions[5] = entity.substring(2);
                catalogs[5] = parseCatalog(parser);
                //System.out.println("Received Parsed Version C6=" + catalogVersions[5]);
            } else if (entity.startsWith("SV")) {////Se Agrego nuevo else if
                catalogVersions[6] = entity.substring(2);
                catalogs[6] = parseCatalog(parser);
                //System.out.println("Received Parsed Version C7=" + catalogVersions[6]);
            } else if (entity.startsWith("MS")) {
                catalogVersions[7] = entity.substring(2);
                catalogs[7] = parseCatalog(parser);
            } else {
                next = entity;
                finished = true;
            }
            return this;
        }
    }
}
