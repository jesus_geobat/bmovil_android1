package tracking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



import com.adobe.mobile.Analytics;

public class TrackingHelperCredit {
	
	public static void trackState(String state, ArrayList<String> mapa){
	       HashMap<String, Object> purchaseDictionary = new HashMap<String,Object>();
	       purchaseDictionary.put("channel", "android");

	       mapa.add(state);
	       String listString = "";

	       for (String s : mapa)
	       {
	           listString += ":" + s;
	       }
	       String res = (purchaseDictionary.get("channel").toString() + listString);
	       
	       switch (mapa.size()) {
	       case 1:
	             purchaseDictionary.put("prop1",res);
	             break;
	       case 2:
	             purchaseDictionary.put("prop2",res);
	             break;
	       case 3:
	             purchaseDictionary.put("prop3",res);
	             break;
	       case 4:
	             purchaseDictionary.put("prop4",res);
	             break;
	       case 5:
	             purchaseDictionary.put("prop5",res);
	             break;
	       case 6:
	             purchaseDictionary.put("prop6",res);
	             break;
	       case 7:
	             purchaseDictionary.put("prop7",res);
	             break;
	       case 8:
	             purchaseDictionary.put("prop8",res);
	             break;
	       case 9:
	             purchaseDictionary.put("prop9",res);
	             break;
	       case 10:
	             purchaseDictionary.put("prop10",res);
	             break;
	       case 11:
	    	   purchaseDictionary.put("prop11",res);
	             break;
	             
	       default:
	             break;
	       }
	       
	       
	       purchaseDictionary.put("appState",res);
	       purchaseDictionary.put("hier1",res);
	       
	       //ARR Encriptacion
	       
//	       if(LoginDelegate.encriptacionEnLogin)
//	       {
//	    	   String nombreUsuario = LoginDelegate.getNombreUsuario();
//		       String nombreUsuarioEncriptado = null;
//		       try 
//		       {
//				nombreUsuarioEncriptado = new String(TripleDESEncryptionHelperCredit.encryptText(nombreUsuario));
//		       } catch (Exception e) 
//		       {
//				// TODO Auto-generated catch block
//		    	   if(Server.ALLOW_LOG) e.printStackTrace();
//		       }
//
//		       purchaseDictionary.put("eVar15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("prop15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("eVar16", "personas");
//		       purchaseDictionary.put("prop16", "personas");
//	       }
//	       else if(ConsultaEstatusAplicacionDesactivadaDelegate.res)
//	       {
//	    	   String nombreUsuario = ConsultaEstatusAplicacionDesactivadaViewController.getNombreUsuario();
//	    	   
//	    	   String nombreUsuarioEncriptado = null;
//		       try 
//		       {
//				nombreUsuarioEncriptado = new String(TripleDESEncryptionHelperCredit.encryptText(nombreUsuario));
//		       } catch (Exception e) 
//		       {
//				// TODO Auto-generated catch block
//		    	   if(Server.ALLOW_LOG) e.printStackTrace();
//		       }
//
//		       purchaseDictionary.put("eVar15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("prop15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("eVar16", "personas");
//		       purchaseDictionary.put("prop16", "personas");
//	    	   
//	       }
	       
	      
	       
	       
	       Analytics.trackState(res,purchaseDictionary);
	       
	}


	//ARR
	public static void trackClickLogin (Map<String, Object> contextData){
	
		Analytics.trackAction("clickLogin", contextData);

	}
	
	//ARR	
	public static void trackDesconexiones(Map<String, Object> contextData){
		
		Analytics.trackAction("clickDesconexiones", contextData);
	}
	
	//ARR
	public static void trackMenuPrincipal(Map<String, Object> contextData){
		
		Analytics.trackAction("clickMenu", contextData);
		
	}
	
	//ARR
	public static void trackFrecuente(Map<String, Object> contextData){
		
		Analytics.trackAction("clickFrecuentes", contextData);
	}
	
	//ARR
	public static void trackInicioOperacion(Map<String, Object> contextData){
		Analytics.trackAction("inicioOperacion", contextData);
	}
	
	//ARR
	public static void trackPaso1Operacion(Map<String, Object> contextData){
		Analytics.trackAction("paso1Operacion", contextData);
	}
	
	//ARR
	public static void trackPaso2Operacion(Map<String, Object> contextData){
		Analytics.trackAction("paso2Operacion", contextData);
	}
	
	//ARR
		public static void trackPaso3Operacion(Map<String, Object> contextData){
			Analytics.trackAction("paso3Operacion", contextData);
		}
		
	//ARR
		public static void trackOperacionRealizada(Map<String, Object> contextData){
			Analytics.trackAction("operacionRealizada", contextData);
		}
	
	//ARR
		public static void trackInicioConsulta(Map<String, Object> contextData){
			Analytics.trackAction("inicioConsulta", contextData);
		}
		
	//ARR
		public static void trackEnvioConfirmacion(Map<String, Object> contextData){
			Analytics.trackAction("envioConfirmacion", contextData);
		}
		
		public static void trackConsultaRealizada(Map<String, Object> contextData){
			Analytics.trackAction("consultaRealizada", contextData);
		}
		//ARR

			public static void trackClickBanner(Map<String, Object> click_bannerMap,Map<String, Object> click_bannerMap2) {
				Analytics.trackAction("clickBanner", click_bannerMap);
				// TODO Auto-generated method stub
			}

	public static void trackSimulacionRealizada (Map<String, Object> click_simulacion_realizada) {
		Analytics.trackAction("clickSimulacionRealizada", click_simulacion_realizada);
	}

	public static void trackInicioSimulador (Map<String, Object> click_inicio_simulador) {
		Analytics.trackAction("clickInicioSimulador", click_inicio_simulador);
	}
}
	

