package org.json.me;

import org.json.JSONException;
import org.json.JSONObject;

public class CreditJSONTools {
	
	public static boolean isJSONValid(String test) {
        boolean valid = false;
        try {
            new JSONObject(test);
            valid = true;
        } catch (JSONException ex) {
            valid = false;
        }
        
        return valid;
    }
}
