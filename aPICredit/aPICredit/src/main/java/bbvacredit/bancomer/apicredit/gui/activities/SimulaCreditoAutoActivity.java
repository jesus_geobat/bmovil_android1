package bbvacredit.bancomer.apicredit.gui.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

import bancomer.api.common.timer.TimerController;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.ListaDatosController;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.SimulaCreditoAutoDelegate;
import bbvacredit.bancomer.apicredit.models.Producto;
import bbvacredit.bancomer.apicredit.R;

public class SimulaCreditoAutoActivity extends BaseActivity implements OnClickListener {
	
	private SimulaCreditoAutoDelegate delegate;
	private ImageButton backButton;
	
	// Menu Button
	private Button resumenBtn;
	
	private ImageView simulaBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState,SHOW_HEADER|SHOW_TITLE, R.layout.activity_simula_credito_auto);
		isOnForeground=true;
		MainController.getInstance().setCurrentActivity(this);
		
		init();
		//delegate.doRecalculoNC(this);
		deleteSimulation();
	}
	
	private void init(){
		// le indicamos al delegate que no es contratacion
		Tools.setIsContratacionPreference(false);
		delegate = new SimulaCreditoAutoDelegate();
		setSaldo();
		// Ocultamos la info si es necesario
		if(delegate.getOcultaInfo()){
			ocultaInfoNC();
		}else{
			pintarInfoTabla();
		}
		
		scaleToCurrentScreen();
		mapearBotones();
		
		//Tools.getCurrentSession().setVisibilityButton(this, R.id.simCAutoBottomMenuRC);
	}
	
	private void setSaldo(){
		TextView saldo = (TextView)findViewById(R.id.simCAutoILMtxt);
		
		saldo.setText(GuiTools.getMoneyString(delegate.getSaldo().toString()));
	}
	
	// Oculta Info
	private void ocultaInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCAuto);
		tlayout.setVisibility(View.INVISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		tableLayout.setVisibility(View.INVISIBLE);
	}
	
	private void muestraInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCAuto);
		tlayout.setVisibility(View.VISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		tableLayout.setVisibility(View.VISIBLE);
	}
	// End Oculta Info
	
	private void mapearBotones(){		
		//backButton = (ImageButton)findViewById(R.id.headerRefresh);
		backButton.setOnClickListener(this);
		
		resumenBtn = (Button)findViewById(R.id.simCAutoBottomMenuRC);
		resumenBtn.setOnClickListener(this);
		
		//simulaBtn = (ImageView)findViewById(R.id.simCAutoHeaderLogo);
		//simulaBtn.setOnClickListener(this);
	}
	
	/**
	 * Escala los elementos de la pantalla para la resoluci�n actual.
	 */
	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.simCAutoBodyLayout));
		guiTools.scale(findViewById(R.id.simCAutoTitle),true);
		guiTools.scale(findViewById(R.id.simCAutoSubTitleTCImg));
		// Alberto
		//guiTools.scale(findViewById(R.id.simCAutoLText),true);
		guiTools.scale(findViewById(R.id.simCAutoILMtxt),true);
		// Alberto
		//guiTools.scale(findViewById(R.id.simCAutoHeaderLogo));
		guiTools.scale(findViewById(R.id.simCAutoBottomLayout));
		guiTools.scale(findViewById(R.id.simCAutoBottomMenuRC),true);
		
		guiTools.scale(findViewById(R.id.simCAuto));
		guiTools.scale(findViewById(R.id.simCAutoLlblAuxiliar),true);
		// Alberto
		//guiTools.scale(findViewById(R.id.simCAutoLblInfo),true);
		guiTools.scale(findViewById(R.id.RelativeLayoutText1));

	}
	
	private void ocultaCamposTabla(){
		LinearLayout layout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		layout.removeAllViews();
	}
	
	public void pintarInfoTabla(){
		ocultaCamposTabla();
		
		muestraInfoNC();
		
		ArrayList<Producto> lista = delegate.getProductTable();
		Iterator<Producto> it = lista.iterator();
		Integer cont = 0;

		ListaDatosController listaController;
		while(it.hasNext()){
			Producto p = it.next();

			listaController = new ListaDatosController((LinearLayout)findViewById(R.id.RelativeLayoutText1), this, this, R.id.RelativeLayoutText1);
			listaController.addElement(p.getDesProd(), GuiTools.getMoneyString(p.getSubproducto().get(0).getMonMax().toString()), false);
			
			++cont;
		}
	}

	@Override
	public void onClick(View v) {
		isOnForeground=false;
		/*if(v.getId() == R.id.headerRefresh){
			delegate.setRet();
			delegate.redirectToView(OldMenuPrincipalActivity.class);
		}else */if(v.getId() == R.id.simCAutoBottomMenuRC){
			delegate.redirectToView(ResumenActivity.class);
		//}else if(v.getId() == R.id.simCAutoHeaderLogo){
			//delegate.doRecalculoNC(this);
			
		}
		
	}
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(OldMenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }

	/**
	 * Created June 8th, 2015,
	 */
	
	private void deleteSimulation()
	{
		delegate.deleteSimulationRequest(this,true);
	}
	
	public void doRecalculo()
	{
		Log.d("Eliminó Simulación", "ok");
		delegate.doRecalculoNC(this);
	}
}
