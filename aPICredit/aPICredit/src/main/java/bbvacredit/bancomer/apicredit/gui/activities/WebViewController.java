package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.io.Server;

/**
 * Created by OOROZCO on 1/5/16.
 */
public class WebViewController extends BaseActivity {

    private static String params;
    private static boolean isAuto;

    public static void setIsAuto(boolean isAuto) {
        WebViewController.isAuto = isAuto;
    }

    public static void setParams(String params) {
        WebViewController.params = params;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER, R.layout.activity_credit_resultado_simula_webview);


        if(isAuto)
            ((ImageView)findViewById(R.id.imgBBVACreditEncabezado)).setImageResource(R.drawable.txt_auto);
        else
            ((ImageView)findViewById(R.id.imgBBVACreditEncabezado)).setImageResource(R.drawable.txt_hipotecario);


        setActivityChanging(false);

        MainController.getInstance().setContext(this);
        MainController.getInstance().setCurrentActivity(this);


        final ProgressDialog pd = ProgressDialog.show(this, "", "Cargando...", true);


        final WebView mWebView = (WebView) findViewById(R.id.wbvContenido);
        mWebView.getSettings().setJavaScriptEnabled(true);


        String url = Server.URL_CONTACTAME + "?" + params;
        mWebView.loadUrl(url);

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                pd.show();
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                pd.dismiss();
            }
        });


    }


    @Override
    public void onBackPressed() {

        SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
        if(sp.getBoolean(ConstantsCredit.SHARED_IS_RESUMEN,false)) {
            MenuPrincipalActivity.getInstance().setIsFromWB(true);
            Log.i("resumenviewcontroller","getIsFrom: "+MenuPrincipalActivity.getInstance().isFromWB());
            MainController.getInstance().showScreen(ResumenViewController.class);
//            MainController.getInstance().showScreen(MenuPrincipalActivity.class);
//            ((DetalleHipotecarioViewController)Session.getInstance().getoMenuPA().getActiveFragment()).verResumenPagos();
//            Log.i("fragment","getActiveFragment: "+Session.getInstance().getoMenuPA().getActiveFragment());
        }else
            MainController.getInstance().showScreen(MenuPrincipalActivity.class);

        super.onBackPressed();
    }
}
