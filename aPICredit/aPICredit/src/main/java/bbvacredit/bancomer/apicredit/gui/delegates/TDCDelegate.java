package bbvacredit.bancomer.apicredit.gui.delegates;

import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import java.util.Hashtable;
import java.util.Iterator;

//import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.DetalleTDCViewController;
import bbvacredit.bancomer.apicredit.gui.activities.MenuPrincipalActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SeekArc;
import bbvacredit.bancomer.apicredit.gui.activities.TDCSeekBarViewController;
import bbvacredit.bancomer.apicredit.gui.activities.TDCViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ConsultaDatosTDCData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by Packo on 01/04/2016.
 */
public class TDCDelegate extends BaseDelegateOperacion {

    private TDCViewController controller;
    private TDCSeekBarViewController controllerOp;

    public Producto producto;
    private Integer productIndex = 0;
    private Session session;
    private ObjetoCreditos data;
    private boolean isDetalleAlternativaTDC;
    private boolean isSavingOperation = false;
    private boolean isDeleteOperation = false; //si es true, borrar simulación
    private DetalleTDCViewController detalleTDC;

    private Promociones promocion;

    public Promociones getPromocion() {
        return promocion;
    }

    public void setPromocion(Promociones promocion) {
        this.promocion = promocion;
    }

    public DetalleTDCViewController getDetalleTDC() {
        return detalleTDC;
    }

    public void setDetalleTDC(DetalleTDCViewController detalleTDC) {
        this.detalleTDC = detalleTDC;
    }

    public boolean isDetalleAlternativaTDC() {
        return isDetalleAlternativaTDC;
    }

    public void setIsDetalleAlternativaTDC(boolean isDetalleAlternativaTDC) {
        this.isDetalleAlternativaTDC = isDetalleAlternativaTDC;
    }

    public void setController(TDCViewController controller) {
        this.controller = controller;
    }


    public Producto getProducto() {
        return producto;
    }

    public void setProducto() {
        SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL,0);
//        productIndex = sp.getInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, 0);
//        producto = data.getProductos().get(productIndex);
        producto = Tools.getProductByCve("0TDC", data.getProductos());
    }

    public ObjetoCreditos getData() {
        return data;
    }

    public TDCDelegate(TDCViewController controller){
        this.controller = controller;
        session = Tools.getCurrentSession();
        data = session.getDataFromCreditos();
        setProducto();
    }

    public TDCDelegate(TDCSeekBarViewController controller){
        this.controllerOp = controller;
        session = Tools.getCurrentSession();
        data = session.getDataFromCreditos();
        setProducto();
    }

    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(Server.CALCULO_ALTERNATIVAS_OPERACION, ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        if(isSavingOperation) {
            addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = false;
        }else {
            addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = true;
        }
        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    public void doRecalculoContract(){
        Log.i("Credit","doRecalculoContract");
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);
        Producto ccr = controller.getProducto();

        int montoSel = 0;
        String montSel = controllerOp.edtMontoVisible.getText().toString();

        montSel = montSel.replace("$","");
        montSel = montSel.replace(",","");

        if(controllerOp.edtMontoVisible.getText().toString() == null) {
            Log.i("Credit","edtm es nulo");
        }else{
            montoSel = Integer.valueOf(montSel);
            Log.i("Credit","no es nulo, "+montoSel);
        }

//        Log.w("Valor del monto",controller.getMonto());

        addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM,paramTable);
        addParametro(ccr.getSubproducto().get(0).getCveSubp(), ServerConstantsCredit.CVESUBP_PARAM,paramTable);
        addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM,paramTable);
        addParametro("", ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    private void setSimLooking4Cve(String cve){
        Iterator<Producto> it = data.getProductos().iterator();

        while(it.hasNext()){
            Producto pr = it.next();
            if(pr.getCveProd().equals(cve)) pr.setIndSimBoolean(true);
        }
    }

    public void analyzeResponse(String operationId, ServerResponse response) {
        if (getCodigoOperacion().equals(operationId)) {
            Log.i("Credit", "operationID: " + operationId);
            if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                Log.i("Credit", "OPERATION_SUCCESSFUL");
                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());

                for(int i = 0; i < respuesta.getProductos().size(); i++){
                    if(respuesta.getProductos().get(i).getCveProd().equals(ConstantsCredit.TARJETA_CREDITO)){
                        Log.i("contrataTDC", "getMontoSolicitado: " + respuesta.getProductos().get(i).getMontoDeseS());
                        String montoSolicitadoTDC = respuesta.getProductos().get(i).getMontoDeseS();
                        data.setMontotSol(montoSolicitadoTDC);
                    }
                }

                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                productIndex = session.getProductoIndexByCve(ConstantsCredit.TARJETA_CREDITO);
//                producto = data.getProductos().get(productIndex);
                producto = Tools.getProductByCve("0TDC",data.getProductos());
                if (data.getProductos().get(productIndex).getCveProd().equals(ConstantsCredit.TARJETA_CREDITO)) {
                    data.getProductos().get(productIndex).setIndSimBoolean(true);
                } else {
                    setSimLooking4Cve(ConstantsCredit.TARJETA_CREDITO);
                }
                MainController.getInstance().ocultaIndicadorActividad();

                session.setCreditos(data);
                setProducto();
                saveOrDeleteSimulationRequest(true);

            }
        } else if (Server.CONSULTA_TDC.equals(operationId)) {
            Log.i("Credit", "CONSULTA_TDC");
            if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                ConsultaDatosTDCData respuesta = (ConsultaDatosTDCData) response.getResponse();
                session.setDataTDC(respuesta);
                Log.w("Llego a Analyze", "Exito");
            }
        } else if (Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            Log.i("Credit", "CALCULO_ALTERNATIVAS_OPERACION");
            if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {

                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());

                for(int i = 0; i < respuesta.getProductos().size(); i++){
                    if(respuesta.getProductos().get(i).getCveProd().equals(ConstantsCredit.TARJETA_CREDITO)){
                        Log.i("contrataTDC", "getMontoSolicitado: " + respuesta.getProductos().get(i).getMontoDeseS());
                        String montoSolicitadoTDC = respuesta.getProductos().get(i).getMontoDeseS();
                        data.setMontotSol(montoSolicitadoTDC);
                    }
                }

                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

//                productIndex = session.getProductoIndexByCve(ConstantsCredit.INCREMENTO_LINEA_CREDITO);
//                producto = data.getProductos().get(productIndex);
                producto = Tools.getProductByCve("0TDC",data.getProductos());

                Log.i("simulador","tdcDelegate, getIndsim: "+data.getProductos().get(productIndex).getIndSim());
                MainController.getInstance().ocultaIndicadorActividad();

                session.setCreditos(data);

                setProducto();

                if(isDeleteOperation){
                    Log.i("eliminar","se eliminó operacion TDC");
                    isDeleteOperation = false;

                    getDetalleTDC().simpleUpdateMenu(producto.getCveProd());
                }else {
//                    controller.showDetalleTDC();
                }
            }
        }
    }

        @Override
    protected String getCodigoOperacion() {
        return Server.CALCULO_OPERACION;
    }

    public void doDetalleAlternativasTDC(){

//        Log.i("tdc","TDCDelegate, doDetalleAlternativaTDC");

        Log.i("contrataTDC", "detalleTDCDelegate, data.montosolicitado: " + data.getMontotSol());
        Log.i("contrataTDC", "detalleTDCDelegate, producto.montosolicitado: " + producto.getMontoDeseS());

        String ium = session.getIum();
        String numeroCelular = session.getNumCelular();

        String producto = getProducto().getCveProd();
        String cliente = session.getIdUsuario();
        Session.getInstance().setCveCamp(producto);

        DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(cliente,ium,numeroCelular,producto,null);
        delegate.consultaDetalleAlternativaTDCTask(this.producto);
    }
}
