package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.LabelMontoTotalCredit;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.gui.delegates.OfertasListViewDelegate;
import bbvacredit.bancomer.apicredit.models.Producto;

/**
 * Created by FHERNANDEZ on 13/12/16.
 */
public class OfertasListViewAdapter extends BaseAdapter{

    private LayoutInflater inflator;
    private LayoutInflater inflatorPopup;

    private Context context;

    private MenuPrincipalActivitySeekBar activity;

    private ArrayList<Producto> listaOfertas;

    private ImageView imgvProd;
    private ImageView imgvShowDetalle;
    private ImageView imgvAyuda;

    private ImageButton imgbContratarCredito;

    private LabelMontoTotalCredit edtMontoVisible;

    private TextView txtvDescProd;
    private TextView txtvMontoMinimo;
    private TextView txtvMontoMaximo;
    private TextView txtvCat;
    private TextView txtvTasa;
    private TextView txtvTextSaldoTotal;
    private TextView txtvSaldoTotalPagar;
    private TextView txtvOpcion;
    private TextView txtvPlazo;

    private LinearLayout lnlDetalleProd;
    private LinearLayout lnlOpcion;
    private LinearLayout lnlPlazo;

    private SeekBar seekBarSimulador;

    private OfertasListViewDelegate delegate;

    private boolean isShowingDetalle = true;
    private boolean isSettingText = false;
    private boolean isResetting = false;
    private boolean mAcceptCents=false;
    private boolean flag=true;

    private StringBuffer typedString= new StringBuffer();
    private String amountString=null;

    private String montoMax = "";
    private String montMin = "";

    private String cveProd;

    final int INCREMENT_PRODUC_ANOM = 100;
    final int INCREMENT_ALL_PRODUCT = 1000;

    private View auxView;
    private View idView;

    private Producto producto;

    public OfertasListViewAdapter(){

    }

    public OfertasListViewAdapter(Context context, ArrayList<Producto>listaOfertas,
                                  MenuPrincipalActivitySeekBar activity){
        this.context        = context;
        this.listaOfertas   = listaOfertas;
        this.activity       = activity;
    }


    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

//    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        return null;
    }
//        View vista = convertView;
//        final int point = position;
//        final Producto oProducto = listaOfertas.get(point);
//        this.producto = oProducto;
//
//        delegate = new OfertasListViewDelegate(this);
//
//        try{
//            if(vista == null){
//                inflator = LayoutInflater.from(context);
//                vista = inflator.inflate(R.layout.activity_credit_item_listview_creditos, parent, false);
//            }
//            final View finalVista = vista;
//            idView = finalVista;
//            auxView = finalVista;
//
//            if(oProducto!=null) {
//
//                final ViewHolder vh = new ViewHolder();
//                vh.edtMontoVisible = (EditText)vista.findViewById(R.id.edtMontoVisible);
//                vh.imgvProd        = (ImageView)finalVista.findViewById(R.id.imgvProd);
//                vh.imgvShowDetalle = (ImageView)finalVista.findViewById(R.id.imgvShowDetalle);
//                vh.seekBarSimulador= (SeekBar)finalVista.findViewById(R.id.seekBarSimulador);
//
//                vista.setTag(vh);
////                vista.setId(activity.getAsigViewHolder(((Producto) getItem(point)).getCveProd()));
//                Log.w("viewholder", "cveProd guardado: " + ((Producto) getItem(point)).getCveProd() + ", con id: " + idView.getId());
//
//
//                vh.imgvShowDetalle.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
////                        getViewTypeCount();
//                        Log.i("viewholder","buscar producto: getItem: "+((Producto)getItem(point)).getCveProd());
////                        Log.i("viewholder","viewholder del producto: getItem: "+activity.getAsigViewHolder(((Producto) getItem(point)).getCveProd()));
//                        Log.w("viewholder", "cveProd recuperado: " + ((Producto) getItem(point)).getCveProd() + ", con id: " + idView.getId());
//                        if(activity.isShowDetalle()){ //si true, oculta detalle y muestra el actual.
//                            activity.setIsShowDetalle(false);
//                            ((LinearLayout) finalVista.findViewById(R.id.lnlDetalleProd)).setVisibility(View.GONE);
//                            ((ImageView)finalVista.findViewById(R.id.imgvShowDetalle)).setImageResource(R.drawable.nvo_icon_masdetalle);
//
//                        }else{
//                            activity.setIsShowDetalle(true);
//                            ((LinearLayout) finalVista.findViewById(R.id.lnlDetalleProd)).setVisibility(View.VISIBLE);
//                            validateProduct(oProducto);
//                            ((ImageView)finalVista.findViewById(R.id.imgvShowDetalle)).setImageResource(R.drawable.nvo_icon_menosdetalle);
//                        }
//                    }
//                });
//
//                vh.imgvAyuda       = (ImageView)finalVista.findViewById(R.id.imgvAyuda);
//                vh.imgvAyuda.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        PopUpAyudaViewController.mostrarPopUp(activity,oProducto.getCveProd());
//                    }
//                });
//
//
//                setValuesProduct(oProducto, finalVista);
////                setValueSeekBar(finalVista, oProducto);
//                final double mmax = Double.parseDouble(Tools.validateMont(getMontoMax(), false));
//                final double mmin = Double.parseDouble(Tools.validateMont(getMontMin(), false));
//
//                double auxMmax = mmax-mmin;
//
//                setVisibleMont(vista);
//
//                if(oProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.ADELANTO_NOMINA))
//                    auxMmax = auxMmax/INCREMENT_PRODUC_ANOM;
//                else
//                    auxMmax = auxMmax/INCREMENT_ALL_PRODUCT;
//
//                final double finalAuxMmax = auxMmax;
//
////        seekBarSimulador.setProgress((int)(mmin*2));
////        seekBarSimulador.setSecondaryProgress((int)(mmin*3));
//
////                ((SeekBar)vista.findViewById(R.id.seekBarSimulador))
//                        vh.seekBarSimulador.setMax((int) auxMmax);
////                ((SeekBar)vista.findViewById(R.id.seekBarSimulador)).
//                        vh.seekBarSimulador.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//
//                    @Override
//                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//
//                        String visibleMont;
//
//                        int incremental;
//                        final int progressChanged = progress;
////                final int auxProgresC = cveProd.equals(ConstantsCredit.ADELANTO_NOMINA) ?
////                        progressChanged * INCREMENT_PRODUC_ANOM : progressChanged * INCREMENT_ALL_PRODUCT;
//
//                        if (oProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.ADELANTO_NOMINA)) {
//                            incremental = INCREMENT_PRODUC_ANOM;
//                        } else {
//                            incremental = INCREMENT_ALL_PRODUCT;
//                        }
//
//                        Log.i("anom", "simulando anom, mmin: " + mmin);
//                        Log.i("anom", "simulando anom, incremental: " + incremental);
//                        Log.i("anom", "simulando anom, finalAuxMmax: " + finalAuxMmax);
//                        Log.i("anom", "simulando anom, mmax: " + mmax);
//
//                        final int progresFinal = calculatePogress(progressChanged, incremental, (int) mmin, (int) finalAuxMmax, (int) mmax);
//
//                        visibleMont = Tools.formatUserAmount(Integer.toString(progresFinal));
//
//                        vh.edtMontoVisible.setText(visibleMont);
//                        Log.i("monmax", "Seteando monto maximo, onProgress ");
//                    }
//
//                    @Override
//                    public void onStartTrackingTouch(SeekBar seekBar) {
//                    }
//
//                    @Override
//                    public void onStopTrackingTouch(SeekBar seekBar) {
////                validateProduct(prod);
//                        //lanza petición de cálculo
//                    }
//                });
//
//
//            }
//
//
//        }catch (Exception ex){
//            ex.printStackTrace();
//        }
//        return vista;
//    }

    /**
     * Método que llena los datos del detalle de una simulación de producto
     * toma los valores de session.getAuxCredit
     */
    public void setDetalleProducto(String prodCve){
        //se actualiza el adapter
//        activity.
        String cveProd = prodCve;
        Producto productoSim = delegate.getAuxProductoByCveProd(cveProd);

        Log.i("detalle","producto: "+cveProd);
        Log.i("detalle", "prodCve: " + prodCve);
        validateViewsToShow(cveProd);
    }

    private void validateViewsToShow(String cveProd){
        final LinearLayout lnlPlazoTipo = (LinearLayout)auxView.findViewById(R.id.lnlPlazoTipo);
        final LinearLayout lnlPlazo = (LinearLayout)auxView.findViewById(R.id.lnlPlazo);
        final LinearLayout lnlTipo = (LinearLayout)auxView.findViewById(R.id.lnlTipo);
//        final LinearLayout lnlPlazoTipoValues = (LinearLayout)auxView.findViewById(R.id.lnlPlazoTipoValues);
        final LinearLayout lnlPagoMMaximo = (LinearLayout)auxView.findViewById(R.id.lnlPagoMMaximo);
        final LinearLayout lnlDetalleProd = (LinearLayout)auxView.findViewById(R.id.lnlDetalleProd);

        final TextView txtvTipo = (TextView)auxView.findViewById(R.id.txtvTipo);
        final TextView txtvMasInfo = (TextView)auxView.findViewById(R.id.txtvMasInfo);
        final TextView txtvPlazo = (TextView)auxView.findViewById(R.id.txtvPlazo);
        final TextView txtvCat = (TextView)auxView.findViewById(R.id.txtvCat);
        final TextView txtvTasa = (TextView)auxView.findViewById(R.id.txtvTasa);


        final ImageView imgvPlazo = (ImageView)auxView.findViewById(R.id.imgvPlazo);
        final ImageView imgvTipo = (ImageView)auxView.findViewById(R.id.imgvTipo);

        final ImageButton imgvContratarCredito = (ImageButton)auxView.findViewById(R.id.imgvContratarCredito);


        Log.i("detalle","validando auxView: "+auxView);
        Log.i("detalle", "validando cveProd: " + cveProd);


//        ((LinearLayout)auxView.findViewById(R.id.lnlDetalleProd)).setVisibility(View.VISIBLE);

        if(cveProd.equalsIgnoreCase(ConstantsCredit.CREDITO_AUTO) ||
                cveProd.equalsIgnoreCase(ConstantsCredit.CREDITO_HIPOTECARIO)){
            Log.i("detalle","auto, hipo");
            lnlPlazoTipo.setVisibility(View.VISIBLE);
//            lnlPlazoTipoValues.setVisibility(View.VISIBLE);
            lnlPagoMMaximo.setVisibility(View.VISIBLE);
            imgvContratarCredito.setVisibility(View.GONE);
            txtvMasInfo.setVisibility(View.VISIBLE);

        }else if(cveProd.equalsIgnoreCase(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO) ||
                cveProd.equalsIgnoreCase(ConstantsCredit.CREDITO_NOMINA)){
            Log.i("detalle","ppi 0nom");

            lnlPagoMMaximo.setVisibility(View.GONE);
            txtvTipo.setVisibility(View.INVISIBLE);
            lnlTipo.setVisibility(View.GONE);
            imgvContratarCredito.setVisibility(View.VISIBLE);
            txtvMasInfo.setVisibility(View.GONE);
        }else{
            Log.i("detalle", "tdc, anom, ilc y tdc");
            imgvContratarCredito.setVisibility(View.VISIBLE);
            lnlPlazoTipo.setVisibility(View.GONE);
            lnlPagoMMaximo.setVisibility(View.VISIBLE);
        }

    }



}
