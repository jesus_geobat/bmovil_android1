package bbvacredit.bancomer.apicredit.models;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.CreditJSONAble;

import java.io.IOException;

import bancomer.api.apiventatdc.commons.ConstantsVentaTDC;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import bbvacredit.bancomer.apicredit.io.ParsingHandlerJSON;

/**
 * Created by OOROZCO on 8/28/15.
 */
public class OfertaTDC implements ParsingHandlerJSON, CreditJSONAble {


    String lineaCredito;
    String tddAsociada;
    String cat;
    String fechaCat;
    String anualidad;
    String producto;
    String tazaPonderada;
    String domicilio;
    String domicilioSucursal;
    String soloCompras;
    String desProducto;





    public String getDesProducto() {
        return desProducto;
    }

    public void setDesProducto(String desProducto) {
        this.desProducto = desProducto;
    }

    public String getLineaCredito() {
        return lineaCredito;
    }

    public String getTddAsociada() {
        return tddAsociada;
    }

    public void setTddAsociada(String tddAsociada) {
        this.tddAsociada = tddAsociada;
    }

    public void setLineaCredito(String lineaCredito) {
        this.lineaCredito = lineaCredito;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getFechaCat() {
        return fechaCat;
    }

    public void setFechaCat(String fechaCat) {
        this.fechaCat = fechaCat;
    }

    public String getAnualidad() {
        return anualidad;
    }

    public void setAnualidad(String anualidad) {
        this.anualidad = anualidad;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getSoloCompras() {
        return soloCompras;
    }

    public void setSoloCompras(String soloCompras) {
        this.soloCompras = soloCompras;
    }

    public String getTazaPonderada() {
        return tazaPonderada;
    }

    public void setTazaPonderada(String tazaPonderada) {
        this.tazaPonderada = tazaPonderada;
    }

    public String getDomicilioSucursal() {
        return domicilioSucursal;
    }

    public void setDomicilioSucursal(String domicilioSucursal) {
        this.domicilioSucursal = domicilioSucursal;
    }


    @Override
    public void fromJSON(String jsonString) {

    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public void processJSON(String jsonString) throws JSONException {

        JSONObject obj = new JSONObject(jsonString);

        lineaCredito    = obj.getString(ConstantsVentaTDC.LINEA_CREDITO_TAG);
        tddAsociada     = obj.getString(ConstantsVentaTDC.TDD_ASOCIADA_TAG);
        producto        = obj.getString(ConstantsVentaTDC.PRODUCTO_TAG);
        desProducto     = obj.getString(ConstantsVentaTDC.DES_PRODUCTO_TAG);
        cat             = obj.getString(ConstantsVentaTDC.CAT_TAG);
        fechaCat        = obj.getString(ConstantsVentaTDC.FECHA_CAT_TAG);
        anualidad       = obj.getString(ConstantsVentaTDC.ANUALIDAD_TAG);
        tazaPonderada   = obj.getString(ConstantsVentaTDC.TAZA_PONDERADA_TAG);
        domicilio       = obj.getString(ConstantsVentaTDC.DOMICILIO_TAG);
        domicilioSucursal = obj.getString(ConstantsVentaTDC.DOMICILIO_SUCURSAL_TAG);
        soloCompras     = obj.getString(ConstantsVentaTDC.SOLO_COMPRAS_TAG);
    }
}
