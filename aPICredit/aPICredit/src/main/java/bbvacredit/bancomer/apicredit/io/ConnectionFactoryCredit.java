package bbvacredit.bancomer.apicredit.io;

import android.content.Context;
import android.util.Log;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.me.CreditJSONAble;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Map;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class ConnectionFactoryCredit {

    private int operationId;
    private Hashtable<String, ?> params;
    private Object responseObject;
    private ParametersTO parameters;
    private boolean isJson;
    private Context context;
    private ApiConstants.isJsonValueCode isJsonValueCode;

    public ConnectionFactoryCredit(final int operationId, final Hashtable<String, ?> params,
                                   final boolean isJson, final Object responseObject, Context context, final ApiConstants.isJsonValueCode isJsonValueCode) {
        super();
        this.operationId = operationId;
        this.params = params;
        this.responseObject = responseObject;
        this.isJson = isJson;
        this.isJsonValueCode = isJsonValueCode;
        this.context=context;
    }

    /**
     * realiza la peticon a server
     * @return
     * @throws Exception
     */
    public IResponseService processConnectionWithParams() throws Exception {

        final Integer opId = Integer.valueOf(operationId);
        parameters = new ParametersTO();
        parameters.setSimulation(ServerCommons.SIMULATION);
        if (!ServerCommons.SIMULATION) {
            parameters.setProduction(!ServerCommons.DEVELOPMENT);
            parameters.setDevelopment(ServerCommons.DEVELOPMENT);
        }
        parameters.setOperationId(opId.intValue());
        final Hashtable<String, String> mapa= new Hashtable<String, String>();
        for (final Map.Entry<String, ?> entry : params.entrySet()) {
            final String key = entry.getKey();
            final Object value = entry.getValue();
            mapa.put(key, (String) (value==null?"":value));
        }
        //
        parameters.setParameters(mapa);
        parameters.setJson(isJson);
        parameters.setIsJsonValue(this.isJsonValueCode);
        IResponseService resultado = new ResponseServiceImpl();
        final ICommService serverproxy = new CommServiceProxy(context);
        try {
            resultado = serverproxy.request(parameters,
                    responseObject == null ? new Object().getClass()
                            : responseObject.getClass());
        } catch (UnsupportedEncodingException e) {
            throw new Exception(e);
        } catch (ClientProtocolException e) {
            throw new Exception(e);
        } catch (IllegalStateException e) {
            throw new Exception(e);
        } catch (IOException e) {
            throw new Exception(e);
        } catch (JSONException e) {
            throw new Exception(e);


        }

        return resultado;

    }

    /**
     * convierte la respuesta del server del tipo IResponseService
     * a uns ServerResponseImpl originario del api
     * @param resultado
     * @param handler
     * @return
     * @throws Exception
     */
    public ServerResponse parserConnection(final IResponseService resultado,
                                           final Object handler) throws Exception {
        ServerResponse response=null;
        ParsingHandlerJSON handlerP=null;
        bbvacredit.bancomer.apicredit.io.ParsingHandler handlerPOld=null;
        //si la peticon es por ParsingHandler
        if(resultado.getObjResponse() instanceof  bbvacredit.bancomer.apicredit.io.ParsingHandler){
            if (ApiConstants.OPERATION_SUCCESSFUL == resultado.getStatus()) {
                // Que pasa si no es JSON la respuesta
                Reader reader = null;
                try {
                    reader = new StringReader(resultado.getResponseString());
                    bbvacredit.bancomer.apicredit.io.Parser parser = new bbvacredit.bancomer.apicredit.io.Parser(reader);
                    if (handler != null) {
                        if(handlerP==null){
                            Class<?> clazz = handler.getClass();
                            handlerPOld = (bbvacredit.bancomer.apicredit.io.ParsingHandler)clazz.cast(handler);
                        }
                        response= new ServerResponse(handlerPOld);
                        response.process(parser);
                    }else{
                        response= new ServerResponse(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
                        response.process(parser);
                    }
                }catch(Exception e){
                    throw new Exception(e);
                }finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Throwable ignored) {
                            throw new Exception(ignored);

                        }
                    }
                }
            }
        }else if (ApiConstants.OPERATION_SUCCESSFUL == resultado.getStatus()) {

            if(resultado.getObjResponse() instanceof ParsingHandlerJSON){
                handlerP= (ParsingHandlerJSON) resultado.getObjResponse();

                if (parameters.isJson() ) {
                    if (handler != null) {
                        try {
                            if(handlerP==null){
                                handlerP=(ParsingHandlerJSON)handler;
                            }
                            ((CreditJSONAble)handlerP).fromJSON(resultado.getResponseString());
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            throw new Exception(e);
                        }
                    }
                    response = new ServerResponse(resultado.getStatus(),
                            resultado.getMessageCode(), resultado.getMessageText(), handlerP);
                }
            }
		}else{
			try {
				handlerP=null;
                handlerPOld=null;
                response= new ServerResponse(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
			} catch (Exception e) {
				// TODO: handle exception
				throw new Exception(e);
			}

		}

		return response;
	}

}
