package bbvacredit.bancomer.apicredit.gui.activities;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import java.util.ArrayList;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.io.ServerResponse;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.ImageLoader;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.MenuPrincipalCreditDelegate;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.models.Opinator;
import bbvacredit.bancomer.apicredit.models.Producto;
import tracking.TrackingHelperCredit;

/**
 * Created by OOROZCO on 11/26/15.
 */
public class MenuPrincipalActivity extends BaseActivity implements CallBackModule{

    // ----------------   Attributes Region Opening ------------------ //
    private MenuPrincipalCreditDelegate delegate;
    private MainController parentManager = MainController.getInstance();
    private ArrayList <String> productsKeyList = new ArrayList<String>();
    private ArrayList <String> viewsNumberList = new ArrayList<String>();

        // ------> Layouts & Fragment Container
    private LinearLayout mainContainer;
    private LinearLayout secondTile;
    private FrameLayout fragmentContainer;
    private LinearLayout cintilloRevire;

        // ------> ImageButtons Declaration
    private ImageButton imgOfertado1;
    private ImageButton imgOfertado2;
    private ImageButton imgOfertado3;
    private ImageButton imgOfertado4;
    private ImageButton imgOfertado5;
    private ImageButton imgOfertado6;

        // -------> Labels Declaration
    private TextView lblMontoCredito1;
    private TextView lblMontoCredito2;
    private TextView lblMontoCredito3;
    private TextView lblMontoCredito4;
    private TextView lblMontoCredito5;
    private TextView lblMontoCredito6;
    private TextView nuevaTasaRevire;
    private TextView viejaTasaRevire;

    private ImageView dividerSimulador;

        // ------> Fragments Declaration
    private Fragment activeFragment;
    private Fragment activeFragmentH;
    private SimularPrestamosViewController reportsFragment;
    private DetalleTDCViewController detalleTDCFragment;
    private TDCViewController tdcFragment;
    private ILCViewController ilcFragment;
    private NominaPPIViewController nominaPPIFragment;
    private HipotecarioViewController hipotecarioFragment;
    private AutoViewController autoFragment;
    private AdelantoNominaViewController adelantoNFragment;

    private boolean isSimulate;
    private boolean noSimulado = false;
    private boolean isFromWB = false;
    private Fragment oFragment;
    private String cveProd;

    private static MenuPrincipalActivity menuPAInstance = null;
    private static final Object lock = new Object();

    public String respuestaOpinator;
    public String nCliente;

    public boolean isSwhowOpinator; //indicará si se mostrará la encuesta de abandono o éxito.

    // ----------------   Attributes Region Closing ------------------ //

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_BTN_ELIMINAR,  R.layout.activity_credit_menu_principal);
//        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_BTN_ELIMINAR, R.layout.activity_credit_item_listview_creditos);


        menuPAInstance = this;

        TrackingHelperCredit.trackState("credit pglobal", parentManager.estados);

        setActivityChanging(false);

        MainController.getInstance().setContext(this);
        MainController.getInstance().setCurrentActivity(this);
        setNombreCliente();

        Session.getInstance().setoMenuPA(this);
        imgBtnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert("");
            }
        });

        initView();

        if(Session.getInstance().isShowOpinator()) { //bandera que enciende o apaga la encuensta
            Log.i("opinator","se hace petición de opinator");
            consultaOpinator();
        }else
            Log.i("opinator","no se hace consulta de opinator");

        setIsSimulate(noSimulado);

        Log.i("IUMUser", "Print ium: " + Session.getInstance().getIum());

        if(Session.getInstance().getOperacion().equals("consultaCreditoAdelanto")){
            oneCall();
            Session.getInstance().setOperacion("");
        }

    }

    private void initView(){
        setCredits();
        setEnvironment();
        showSimuladorPrestamo();
        setCintilloRevire("","",false);
    }

    public static MenuPrincipalActivity getInstance(){
        if(menuPAInstance == null){
            synchronized (lock){
                if(menuPAInstance == null){
                    menuPAInstance = new MenuPrincipalActivity();
                }
            }
        }
        return  menuPAInstance;
    }

    private void setCredits() {
        Session session = Tools.getCurrentSession();
        if(session.getCreditos() != null){
            delegate = new MenuPrincipalCreditDelegate();
            delegate.setDataFromCredit(session.getCreditos());
            delegate.setShowCreditosContratados(session.getShowCreditosContratados());
            delegate.setShowCreditosOfertados(session.getShowCreditosOfertados());
        }else{
            delegate = new MenuPrincipalCreditDelegate();
        }
    }

    private void setEnvironment() {
        setUI();
        showComponentSetGraphics();
    }

    public void showComponentSetGraphics(){
        showComponents(delegate.getCreditosOfertados().size());
        setGraphics(delegate.getCreditosOfertados().size());
    }

    public void updateMenu(String cveProd) {
        Log.i("plazo","cveProd: "+cveProd);
        setCredits();
        showComponents(delegate.getCreditosOfertados().size());
        setGraphics(delegate.getCreditosOfertados().size());
        updateSelectedGraphic(cveProd);
    }

    private void updateSelectedGraphic(String cveProd) {
        if(cveProd.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
            setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.INCREMENTO_LINEA_CREDITO)), ConstantsCredit.INCREMENTO_LINEA_CREDITO);
        }else if(cveProd.equals(ConstantsCredit.TARJETA_CREDITO)){
            setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.TARJETA_CREDITO)), ConstantsCredit.TARJETA_CREDITO);
        }else if(cveProd.equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
            setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_HIPOTECARIO)), ConstantsCredit.CREDITO_HIPOTECARIO);
        }else if(cveProd.equals(ConstantsCredit.CREDITO_AUTO)){
            setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_AUTO)), ConstantsCredit.CREDITO_AUTO);
        }else if(cveProd.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
            setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)), ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO);
        }else if(cveProd.equals(ConstantsCredit.CREDITO_NOMINA)){
            setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_NOMINA)), ConstantsCredit.CREDITO_NOMINA);
        }else if(cveProd.equals(ConstantsCredit.ADELANTO_NOMINA)){
            setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.ADELANTO_NOMINA)),ConstantsCredit.ADELANTO_NOMINA);
        }
    }

    private void setUI() {

        findViewById(R.id.header_layout).setOnDragListener(new DragListener());

        mainContainer = (LinearLayout) findViewById(R.id.mainContainer);
        mainContainer.setOnDragListener(new DragListener());

        secondTile = (LinearLayout) findViewById(R.id.secondTile);

        fragmentContainer = (FrameLayout) findViewById(R.id.fragmentContainer);
        fragmentContainer.setOnDragListener(new DragListener());

        imgOfertado1 = (ImageButton) findViewById(R.id.imgOfertado1);
        imgOfertado1.setOnTouchListener(new TouchListener());

        imgOfertado2 = (ImageButton) findViewById(R.id.imgOfertado2);
        imgOfertado2.setOnTouchListener(new TouchListener());

        imgOfertado3 = (ImageButton) findViewById(R.id.imgOfertado3);
        imgOfertado3.setOnTouchListener(new TouchListener());

        imgOfertado4 = (ImageButton) findViewById(R.id.imgOfertado4);
        imgOfertado4.setOnTouchListener(new TouchListener());

        imgOfertado5 = (ImageButton) findViewById(R.id.imgOfertado5);
        imgOfertado5.setOnTouchListener(new TouchListener());

        imgOfertado6 = (ImageButton) findViewById(R.id.imgOfertado6);
        imgOfertado6.setOnTouchListener(new TouchListener());

        lblMontoCredito1 = (TextView) findViewById(R.id.lblMontoCredito1);
        lblMontoCredito2 = (TextView) findViewById(R.id.lblMontoCredito2);
        lblMontoCredito3 = (TextView) findViewById(R.id.lblMontoCredito3);
        lblMontoCredito4 = (TextView) findViewById(R.id.lblMontoCredito4);
        lblMontoCredito5 = (TextView) findViewById(R.id.lblMontoCredito5);
        lblMontoCredito6 = (TextView) findViewById(R.id.lblMontoCredito6);

        dividerSimulador = (ImageView) findViewById(R.id.dividerSimulador);
        cintilloRevire = (LinearLayout)findViewById(R.id.cintilloRevire);
        nuevaTasaRevire = (TextView) findViewById(R.id.nuevaTasaRevire);
        viejaTasaRevire = (TextView) findViewById(R.id.viejaTasaRevire);


    }

    private void showComponents(int size) {
        switch (size) {
            case 1:
                secondTile.setVisibility(View.GONE);
                imgOfertado1.setVisibility(View.GONE);
                imgOfertado3.setVisibility(View.GONE);
                lblMontoCredito1.setText("");
                lblMontoCredito3.setText("");
                break;
            case 2:
                secondTile.setVisibility(View.GONE);
                imgOfertado3.setVisibility(View.GONE);
                lblMontoCredito3.setText("");
                break;
            case 3:
                secondTile.setVisibility(View.GONE);
                break;
            case 4:
                imgOfertado4.setVisibility(View.GONE);
                imgOfertado6.setVisibility(View.GONE);
                lblMontoCredito4.setText("");
                lblMontoCredito6.setText("");
                break;
            case 5:
                imgOfertado6.setVisibility(View.GONE);
                lblMontoCredito6.setText("");
                break;
        }
    }

    private void setGraphics(int size) {

        ArrayList<Producto> products = delegate.getCreditosOfertados();

        switch (size) {
            case 1:
                imgOfertado2.setImageResource(GuiTools.getCreditIcon(products.get(0).getCveProd()));
                lblMontoCredito2.setText(GuiTools.getMoneyString(String.valueOf(products.get(0).getMontoMaxS())));
                productsKeyList.add(products.get(0).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_2);
                break;
            case 2:
                imgOfertado1.setImageResource(GuiTools.getCreditIcon(products.get(0).getCveProd()));
                lblMontoCredito1.setText(GuiTools.getMoneyString(String.valueOf(products.get(0).getMontoMaxS())));
                productsKeyList.add(products.get(0).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_1);

                imgOfertado2.setImageResource(GuiTools.getCreditIcon(products.get(1).getCveProd()));
                lblMontoCredito2.setText(GuiTools.getMoneyString(String.valueOf(products.get(1).getMontoMaxS())));
                productsKeyList.add(products.get(1).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_2);
                break;
            case 3:
                imgOfertado1.setImageResource(GuiTools.getCreditIcon(products.get(0).getCveProd()));
                lblMontoCredito1.setText(GuiTools.getMoneyString(String.valueOf(products.get(0).getMontoMaxS())));
                productsKeyList.add(products.get(0).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_1);

                imgOfertado2.setImageResource(GuiTools.getCreditIcon(products.get(1).getCveProd()));
                lblMontoCredito2.setText(GuiTools.getMoneyString(String.valueOf(products.get(1).getMontoMaxS())));
                productsKeyList.add(products.get(1).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_2);

                imgOfertado3.setImageResource(GuiTools.getCreditIcon(products.get(2).getCveProd()));
                lblMontoCredito3.setText(GuiTools.getMoneyString(String.valueOf(products.get(2).getMontoMaxS())));
                productsKeyList.add(products.get(2).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_3);
                break;
            case 4:
                imgOfertado1.setImageResource(GuiTools.getCreditIcon(products.get(0).getCveProd()));
                lblMontoCredito1.setText(GuiTools.getMoneyString(String.valueOf(products.get(0).getMontoMaxS())));
                productsKeyList.add(products.get(0).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_1);

                imgOfertado2.setImageResource(GuiTools.getCreditIcon(products.get(1).getCveProd()));
                lblMontoCredito2.setText(GuiTools.getMoneyString(String.valueOf(products.get(1).getMontoMaxS())));
                productsKeyList.add(products.get(1).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_2);

                imgOfertado3.setImageResource(GuiTools.getCreditIcon(products.get(2).getCveProd()));
                lblMontoCredito3.setText(GuiTools.getMoneyString(String.valueOf(products.get(2).getMontoMaxS())));
                productsKeyList.add(products.get(2).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_3);

                imgOfertado5.setImageResource(GuiTools.getCreditIcon(products.get(3).getCveProd()));
                lblMontoCredito5.setText(GuiTools.getMoneyString(String.valueOf(products.get(3).getMontoMaxS())));
                productsKeyList.add(products.get(3).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_5);
                break;
            case 5:
                imgOfertado1.setImageResource(GuiTools.getCreditIcon(products.get(0).getCveProd()));
                lblMontoCredito1.setText(GuiTools.getMoneyString(String.valueOf(products.get(0).getMontoMaxS())));
                productsKeyList.add(products.get(0).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_1);

                imgOfertado2.setImageResource(GuiTools.getCreditIcon(products.get(1).getCveProd()));
                lblMontoCredito2.setText(GuiTools.getMoneyString(String.valueOf(products.get(1).getMontoMaxS())));
                productsKeyList.add(products.get(1).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_2);

                imgOfertado3.setImageResource(GuiTools.getCreditIcon(products.get(2).getCveProd()));
                lblMontoCredito3.setText(GuiTools.getMoneyString(String.valueOf(products.get(2).getMontoMaxS())));
                productsKeyList.add(products.get(2).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_3);

                imgOfertado4.setImageResource(GuiTools.getCreditIcon(products.get(3).getCveProd()));
                lblMontoCredito4.setText(GuiTools.getMoneyString(String.valueOf(products.get(3).getMontoMaxS())));
                productsKeyList.add(products.get(3).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_4);

                imgOfertado5.setImageResource(GuiTools.getCreditIcon(products.get(4).getCveProd()));
                lblMontoCredito5.setText(GuiTools.getMoneyString(String.valueOf(products.get(4).getMontoMaxS())));
                productsKeyList.add(products.get(4).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_5);
                break;
            case 6:
                imgOfertado1.setImageResource(GuiTools.getCreditIcon(products.get(0).getCveProd()));
                lblMontoCredito1.setText(GuiTools.getMoneyString(String.valueOf(products.get(0).getMontoMaxS())));
                productsKeyList.add(products.get(0).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_1);

                imgOfertado2.setImageResource(GuiTools.getCreditIcon(products.get(1).getCveProd()));
                lblMontoCredito2.setText(GuiTools.getMoneyString(String.valueOf(products.get(1).getMontoMaxS())));
                productsKeyList.add(products.get(1).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_2);

                imgOfertado3.setImageResource(GuiTools.getCreditIcon(products.get(2).getCveProd()));
                lblMontoCredito3.setText(GuiTools.getMoneyString(String.valueOf(products.get(2).getMontoMaxS())));
                productsKeyList.add(products.get(2).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_3);

                imgOfertado4.setImageResource(GuiTools.getCreditIcon(products.get(3).getCveProd()));
                lblMontoCredito4.setText(GuiTools.getMoneyString(String.valueOf(products.get(3).getMontoMaxS())));
                productsKeyList.add(products.get(3).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_4);

                imgOfertado5.setImageResource(GuiTools.getCreditIcon(products.get(4).getCveProd()));
                lblMontoCredito5.setText(GuiTools.getMoneyString(String.valueOf(products.get(4).getMontoMaxS())));
                productsKeyList.add(products.get(4).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_5);

                imgOfertado6.setImageResource(GuiTools.getCreditIcon(products.get(5).getCveProd()));
                lblMontoCredito6.setText(GuiTools.getMoneyString(String.valueOf(products.get(5).getMontoMaxS())));
                productsKeyList.add(products.get(5).getCveProd());
                viewsNumberList.add(ConstantsCredit.IMG_OFERTADO_6);
                break;
        }
    }

    // ------------------------ Drag & Drop Classes--------------------------- //
    private final class DragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DROP:
                    try {
                        View view = (View) event.getLocalState();
                        view.setVisibility(View.VISIBLE);
                        selectFlow(view.getId());
                        isCreditShown(true);
                    } catch(OutOfMemoryError e){
                        e.printStackTrace();
                    } catch(NullPointerException e){
                        e.printStackTrace();
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    private final class TouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data, shadowBuilder, view, 0);
                view.setVisibility(View.VISIBLE);
                return true;
            } else {
                return false;
            }
        }
    }

    // --------------------------- Flow Selection ----------------------- //
    private void selectFlow(int idView) {
        if(idView == R.id.imgOfertado1)
        {
//           Log.i("posiciong", "flowA1: " + flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_1))));
            if(flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_1))))
                delegate.addProductId(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_1));

        }else if(idView == R.id.imgOfertado2)
        {
//            Log.i("posiciong", "flowA2: " + flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_2))));
            if(flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_2))))
                delegate.addProductId(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_2));
        }else if(idView == R.id.imgOfertado3)
        {
//            Log.i("posiciong", "flowA3: " + flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_3))));
            if(flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_3))))
                delegate.addProductId(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_3));
        }if(idView == R.id.imgOfertado4)
        {
//            Log.i("posiciong", "flowA4: " + flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_4))));
            if(flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_4))))
                delegate.addProductId(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_4));
        }else if(idView == R.id.imgOfertado5)
        {
//            Log.i("posiciong", "flowA5: " + flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_5))));
            if(flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_5))))
                delegate.addProductId(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_5));
        }else if(idView == R.id.imgOfertado6)
        {
//            Log.i("posiciong", "flowA6: " + flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_6))));
            if(flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_6))))
                delegate.addProductId(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_6));
        }

    }

    public void showAlert(final String cveProd){
        new AlertDialog.Builder(getInstance())
                .setTitle("Alert")
                .setMessage(getInstance().getResources().getString(R.string.menuprincipalactivity_alert_borrar_simulacion))
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        setCveProd(cveProd);
                        delegate.setCveProd(cveProd);
//                        delegate.doEliminar(getInstance());
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Atras", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void validateProductToShow(String cveProd){

        hideSoftKeyboard();
        setCintilloRevire("", "", false);
        if(cveProd.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO))
            showILC();
        else if(cveProd.equals(ConstantsCredit.TARJETA_CREDITO))
            showTDC();
        else if(cveProd.equals(ConstantsCredit.CREDITO_HIPOTECARIO))
            showHipotecario();
        else if(cveProd.equals(ConstantsCredit.CREDITO_AUTO))
            showAuto();
        else if(cveProd.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO))
            showPPI();
        else if(cveProd.equals(ConstantsCredit.CREDITO_NOMINA))
            showNomina();
        else if(cveProd.equals(ConstantsCredit.ADELANTO_NOMINA))
            showAdelantoNomina();
    }

    private boolean validateSimulation(boolean indSimu, String monto, String cveProd){
        boolean isValidate = validateMontByProduct(monto);
        if(isValidate){ //false = no muestra gráfica, true = muestra gráfica
            if(!indSimu){ //si no ha sido simulado muestra producto
                Log.i("posiciong","se muestra producto");
                validateProductToShow(cveProd);
            }else{
                Log.i("posiciong","se muestra alerta");
                showAlert(cveProd);
            }
        }

        return isValidate;
    }

    private boolean flowAccordingView(String cveProd) {

        String indSimuFlow = "";
        String montoFlow = "";

        Session oSession = Tools.getCurrentSession();
        ArrayList<Producto> getProductos = oSession.getCreditos().getProductos();

        if(cveProd.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
            indSimuFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.INCREMENTO_LINEA_CREDITO)).getIndSim();
            montoFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.INCREMENTO_LINEA_CREDITO)).getMontoMaxS();

        }else if(cveProd.equals(ConstantsCredit.TARJETA_CREDITO)){
            indSimuFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.TARJETA_CREDITO)).getIndSim();
            montoFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.TARJETA_CREDITO)).getMontoMaxS();

        }else if(cveProd.equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
            indSimuFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_HIPOTECARIO)).getIndSim();
            montoFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_HIPOTECARIO)).getMontoMaxS();

        }else if(cveProd.equals(ConstantsCredit.CREDITO_AUTO)){
            indSimuFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_AUTO)).getIndSim();
            montoFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_AUTO)).getMontoMaxS();

        }else if(cveProd.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
            indSimuFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)).getIndSim();
            montoFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)).getMontoMaxS();

        }else if(cveProd.equals(ConstantsCredit.CREDITO_NOMINA)){
            indSimuFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_NOMINA)).getIndSim();
            montoFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_NOMINA)).getMontoMaxS();

        }else if(cveProd.equals(ConstantsCredit.ADELANTO_NOMINA)){
            indSimuFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.ADELANTO_NOMINA)).getIndSim();
            montoFlow = getProductos.get(productsKeyList.indexOf(ConstantsCredit.ADELANTO_NOMINA)).getMontoMaxS();
        }
        Log.i("posiciong","producto a validar: "+cveProd);

        return (validateSimulation(indSimuFlow.equalsIgnoreCase("s") ? true : false, montoFlow, cveProd));

    }

    private boolean validateMontByProduct(String monto){
        Log.i("simulador","valida monto: "+monto);

        boolean resp = false;
        monto = monto.substring(0, monto.length() - 3);
        Log.w("simulador","valor monto: "+monto);

        if(Integer.valueOf(monto) > 0.00){
            resp = true;
        }

        return resp;
    }

    // ------------------------ Fragments Control ----------------------- //

    public Fragment getActiveFragment(){
        return activeFragment;
    }

    public Fragment getActiveFragmentH(){
        return activeFragmentH;
    }

    public void setActiveFragment(Fragment activeFragment){
        this.activeFragment = activeFragment;
    }

    public void setActiveFragmentH(Fragment activeFragmentH){
        this.activeFragmentH = activeFragmentH;
    }

    private void removeActiveFragment() {
        if (getActiveFragment() != null) {
            getFragmentManager().beginTransaction().remove(getActiveFragment()).commit();
        }
        setActiveFragment(null);
    }

    private void showActiveFragment() {
        getFragmentManager().beginTransaction().add(R.id.fragmentContainer, getActiveFragment()).commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        getFragmentManager().beginTransaction().commitAllowingStateLoss();
    }

    // -------------  Clear Fragment Main Method ------------  //

    public void resetView() {
        try {
            removeActiveFragment();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        setGraphics(delegate.getCreditosOfertados().size());
    }

    // --------------- Transition Methods ------------------------- //

    private void setSelectedGraphic(String viewId, String cveProd) {
        if(viewId.equals(ConstantsCredit.IMG_OFERTADO_1)){
            imgOfertado1.setImageResource(GuiTools.getSelectedCreditIcon(cveProd));
            lblMontoCredito1.setText(GuiTools.getSelectedLabel(cveProd));
        }else if(viewId.equals(ConstantsCredit.IMG_OFERTADO_2)){
            imgOfertado2.setImageResource(GuiTools.getSelectedCreditIcon(cveProd));
            lblMontoCredito2.setText(GuiTools.getSelectedLabel(cveProd));
        }else if(viewId.equals(ConstantsCredit.IMG_OFERTADO_3)){
            imgOfertado3.setImageResource(GuiTools.getSelectedCreditIcon(cveProd));
            lblMontoCredito3.setText(GuiTools.getSelectedLabel(cveProd));
        }else if(viewId.equals(ConstantsCredit.IMG_OFERTADO_4)){
            imgOfertado4.setImageResource(GuiTools.getSelectedCreditIcon(cveProd));
            lblMontoCredito4.setText(GuiTools.getSelectedLabel(cveProd));
        }else if(viewId.equals(ConstantsCredit.IMG_OFERTADO_5)){
            imgOfertado5.setImageResource(GuiTools.getSelectedCreditIcon(cveProd));
            lblMontoCredito5.setText(GuiTools.getSelectedLabel(cveProd));
        }else if(viewId.equals(ConstantsCredit.IMG_OFERTADO_6)){
            imgOfertado6.setImageResource(GuiTools.getSelectedCreditIcon(cveProd));
            lblMontoCredito6.setText(GuiTools.getSelectedLabel(cveProd));
        }
    }

    public void showSimuladorPrestamo(){
        try {
            if (reportsFragment == null)
                reportsFragment = new SimularPrestamosViewController();
            else {
                reportsFragment = null;
                reportsFragment = new SimularPrestamosViewController();
            }
            setActiveFragment(reportsFragment);
//            showActiveFragment();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void showILC(){
        resetView();
        setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.INCREMENTO_LINEA_CREDITO)), ConstantsCredit.INCREMENTO_LINEA_CREDITO);
        ilcFragment = new ILCViewController();
        Producto producto = delegate.getCreditosOfertados().get(productsKeyList.indexOf(ConstantsCredit.INCREMENTO_LINEA_CREDITO));
        ilcFragment.setProducto(producto);
        setActiveFragment(ilcFragment);
        SeekArc.isAdelantoNomina = false;
        SeekArc.mMax = producto.getSubproducto().get(0).getMonMax();
        SeekArc.mMin = producto.getSubproducto().get(0).getMonMin();
//        showActiveFragment();
    }

    public void showNomina(){
        resetView();
        setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_NOMINA)), ConstantsCredit.CREDITO_NOMINA);
        nominaPPIFragment = new NominaPPIViewController();
        Producto producto = Tools.getCurrentSession().getCreditos().getProductos().get(productsKeyList.indexOf(ConstantsCredit.CREDITO_NOMINA));
        nominaPPIFragment.setProducto(producto);
        nominaPPIFragment.setPPI(false);
        setActiveFragment(nominaPPIFragment);
        SeekArc.isAdelantoNomina = false;

        String auxMax = producto.getMontoMaxS();
        String auxMin = producto.getMontoMinS();

        if(auxMax.endsWith(".00"))
            auxMax = auxMax.replace(".00","");
        else
            auxMax = auxMax.substring(0, auxMax.length() - 3);
        if(auxMax.contains(","))
            auxMax = auxMax.replace(",","");

        if(auxMin.endsWith(".00"))
            auxMin = auxMin.replace(".00", "");
        if(auxMin.contains(","))
            auxMin = auxMin.replace(",","");

        SeekArc.mMax = Integer.parseInt(auxMax);
        SeekArc.mMin = Integer.parseInt(auxMin);
        showActiveFragment();
    }

    public void showPPI(){
        resetView();
        setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)), ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO);
        nominaPPIFragment = new NominaPPIViewController();
        Producto producto = Tools.getCurrentSession().getCreditos().getProductos().get(productsKeyList.indexOf(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO));
        nominaPPIFragment.setProducto(producto);
        nominaPPIFragment.setPPI(true);
        setActiveFragment(nominaPPIFragment);
        SeekArc.isAdelantoNomina = false;

        String auxMax = producto.getMontoMaxS();
        String auxMin = producto.getMontoMinS();

        if(auxMax.endsWith(".00"))
            auxMax = auxMax.replace(".00","");
        else
            auxMax = auxMax.substring(0, auxMax.length() - 3);

        if(auxMax.contains(","))
            auxMax = auxMax.replace(",","");

        if(auxMin.endsWith(".00"))
            auxMin = auxMin.replace(".00", "");
        if(auxMin.contains(","))
            auxMin = auxMin.replace(",","");

        SeekArc.mMax = Integer.parseInt(auxMax);
        SeekArc.mMin = Integer.parseInt(auxMin);
        showActiveFragment();
    }

    public void showHipotecario(){
        resetView();
        setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_HIPOTECARIO)), ConstantsCredit.CREDITO_HIPOTECARIO);
        hipotecarioFragment =  new HipotecarioViewController();
        Producto producto = Tools.getCurrentSession().getCreditos().getProductos().get(productsKeyList.indexOf(ConstantsCredit.CREDITO_HIPOTECARIO));
        hipotecarioFragment.setProducto(producto);
        setActiveFragment(hipotecarioFragment);
        SeekArc.isAdelantoNomina = false;

        String auxMax = producto.getMontoMaxS();
        String auxMin = producto.getMontoMinS();


        if(auxMax.endsWith(".00"))
            auxMax = auxMax.replace(".00","");
        else
            auxMax = auxMax.substring(0, auxMax.length() - 3);
        Log.i("plazo","auxMax showHip: "+auxMax);

        if(auxMax.contains(","))
            auxMax = auxMax.replace(",","");

        if(auxMin.endsWith(".00"))
            auxMin = auxMin.replace(".00","");
        else
            auxMin = auxMin.substring(0, auxMin.length() - 3);

        Log.i("plazo","auxMax auxMin: "+auxMin);

        if(auxMin.contains(","))
            auxMin = auxMin.replace(",","");

        SeekArc.mMax = Integer.parseInt(auxMax);
        SeekArc.mMin = Integer.parseInt(auxMin);
        showActiveFragment();
    }

    public void showAuto(){

        resetView();
        setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.CREDITO_AUTO)), ConstantsCredit.CREDITO_AUTO);
        autoFragment =  new AutoViewController();
        Producto producto = Tools.getCurrentSession().getCreditos().getProductos().get(productsKeyList.indexOf(ConstantsCredit.CREDITO_AUTO));
        autoFragment.setProducto(producto);
        setActiveFragment(autoFragment);
        SeekArc.isAdelantoNomina = false;

        Log.i("update","showAuto, max: "+producto.getMontoMaxS());

        String auxMax = producto.getMontoMaxS();
        String auxMin = producto.getMontoMinS();

        if(auxMax.endsWith(".00"))
            auxMax = auxMax.replace(".00","");
        else
            auxMax = auxMax.substring(0, auxMax.length() - 3);

        if(auxMax.contains(","))
            auxMax = auxMax.replace(",", "");

        if(auxMin.endsWith(".00"))
            auxMin = auxMin.replace(".00","");
        else
            auxMin = auxMin.substring(0,auxMin.length()-3);
        if(auxMin.contains(","))
            auxMin = auxMin.replace(",","");

        SeekArc.mMax = Integer.parseInt(auxMax);
        SeekArc.mMin = Integer.parseInt(auxMin);
        showActiveFragment();
    }

    // Code For Adelanto Nómina
    public void showAdelantoNomina(){
        resetView();
        setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.ADELANTO_NOMINA)), ConstantsCredit.ADELANTO_NOMINA);
        adelantoNFragment = new AdelantoNominaViewController();
        Producto producto = delegate.getCreditosOfertados().get(productsKeyList.indexOf(ConstantsCredit.ADELANTO_NOMINA));
        adelantoNFragment.setProducto(producto);

        Log.i("anom", "productsKeyList.indexOf(ConstantsCredit.ADELANTO_NOMINA: " + productsKeyList.indexOf(ConstantsCredit.ADELANTO_NOMINA));
        Log.i("anom", "showAnom: " + producto.getCveProd());

        if(producto == null)
            Log.i("Detalle","producto esnulo");

        setActiveFragment(adelantoNFragment);
        SeekArc.isAdelantoNomina = true;
        SeekArc.mMax = producto.getSubproducto().get(0).getMonMax();
        SeekArc.mMin = producto.getSubproducto().get(0).getMonMin();
        Log.i("adelantos", "mMax: " + producto.getSubproducto().get(0).getMonMax());
        Log.i("adelantos", "mMin: " + producto.getSubproducto().get(0).getMonMin());
        showActiveFragment();
    }

    /**
     * var 12 eventoque se ejecuta
     */
    // Code For VENTA TDC Flow //
    public void showTDC(){
        resetView();
        setSelectedGraphic(viewsNumberList.get(productsKeyList.indexOf(ConstantsCredit.TARJETA_CREDITO)), ConstantsCredit.TARJETA_CREDITO);
        tdcFragment = new TDCViewController();
        Producto producto = delegate.getCreditosOfertados().get(productsKeyList.indexOf(ConstantsCredit.TARJETA_CREDITO));
        tdcFragment.setProducto(producto);
        setActiveFragment(tdcFragment);
        SeekArc.isAdelantoNomina = false;
        SeekArc.mMax = producto.getSubproducto().get(0).getMonMax();
        SeekArc.mMin = producto.getSubproducto().get(0).getMonMin();
        Log.i("Credit", "mMax: " + producto.getSubproducto().get(0).getMonMax());
        Log.i("Credit", "mMin: " + producto.getSubproducto().get(0).getMonMin());
        TrackingHelperCredit.trackState("creditsimTDCCont", parentManager.estados);
        showActiveFragment();
    }

    public void actualizaGrafica(String monto){

        if(getActiveFragment() instanceof AutoViewController)
            autoFragment.actualizaGrafica(monto);

        else if(getActiveFragment() instanceof NominaPPIViewController)
            nominaPPIFragment.actualizaGrafica(monto);

        else if(getActiveFragment() instanceof HipotecarioViewController)
            hipotecarioFragment.actualizaGrafica(monto);

        else if(getActiveFragment() instanceof AdelantoNominaViewController)
            adelantoNFragment.actualizaGrafica(monto);

        else if(getActiveFragment() instanceof ILCViewController)
            ilcFragment.actualizaGrafica(monto);

        else if(getActiveFragment() instanceof TDCViewController)
            tdcFragment.actualizaGrafica(monto);

    }


    public void showPopUpInforma(final DetalleTDCViewController v) {
        LayoutInflater inflater = getLayoutInflater();
        String url ="";
        final AlertDialog alertDialog;

        View dialoglayout = inflater.inflate(R.layout.activity_credit_informa_tdc, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipalActivity.this);
        builder.setView(dialoglayout);
        alertDialog = builder.create();
        // show it
        alertDialog.show();

        ImageButton btnSolicitar = (ImageButton) dialoglayout.findViewById(R.id.btnSolicitar);
        ImageButton btnCerrar = (ImageButton) dialoglayout.findViewById(R.id.btnCerrar);
        ImageView   imgPublicidad = (ImageView) dialoglayout.findViewById(R.id.imgPublicidad);
        LinearLayout linearTDCImage = (LinearLayout) dialoglayout.findViewById(R.id.linearTDCImage);
        ProgressBar progresBarTDC = (ProgressBar)dialoglayout.findViewById(R.id.progressBarTDC);

        scaleToCurrentScreen();

        //Solo va a cambiar la imagen cuando se encuentre en test o producción
        ImageLoader loader = new ImageLoader(getApplicationContext());
            url = ConstantsCredit.URL_DINAMIC_IMAGE_PROD;
        Log.i("imagen", "imagen nueva, url: "+url);
        linearTDCImage.setVisibility(View.VISIBLE);
        loader.DisplayImage(url, imgPublicidad);
        progresBarTDC.setVisibility(View.GONE);

        btnSolicitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                v.solicitarAction();
                v.preferences();
                v.contratarTDC();
                alertDialog.dismiss();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void scaleToCurrentScreen(){
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(findViewById(R.id.imgPublicidad), true);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(Tools.getCurrentSession().getShowCreditosContratados()) {
            if(isSimulate() && (getoFragment() != null)){
                MainController.getInstance().showScreen(MenuPrincipalActivity.class);
            }else{
                if(getActiveFragment() instanceof  SimularPrestamosViewController){
                    goBmovilMenu();
                }else {
                    MainController.getInstance().showScreen(MenuPrincipalActivity.class);
                }
            }

        }else{
            if(parentManager.getOperacion() == null) { //si viene nulo
                goBmovilMenu();
            }else {
                if(parentManager.getOperacion().isEmpty())
                    goBmovilMenu();
                else {

                    //agregar código para el regreso al ap de nómina
                    super.onBackPressed();
                }
            }
        }
    }

    //se lana petición para saber si se le mostrará la encuesta
    public void consultaOpinator(){

        Session osession = Session.getInstance();
        String numCel = osession.getNumCelular();

        String url = ConstantsCredit.BASE_OPINATOR+"id="+numCel+"&"+"designator="+ConstantsCredit.BASE_OPINATOR_DESIGNATOR;
        Log.i("opinator", "url construida: " + url);

        if(Tools.readURL(url)) {
            Log.i("opinator", "respuesta: " + getRespuestaOpinator());
            Opinator opinator = new Opinator();
            opinator.fromJSON(getRespuestaOpinator());

            if (!opinator.isResponseObjet()) { //si regresa true, se muestra opinator
                Log.i("opinator", "muestra encuesta");
                isSwhowOpinator = true;
            }else
                isSwhowOpinator = false;
        }
    }

    public void setCintilloRevire(String nuevaTasa, String viejaTasa, boolean isShow){

        Log.i("revire","se intenta mostrar cintillo");

        if(isShow) {
            Log.i("revire","se muestra cintillo verde");
            Typeface typeface1 = Typeface.createFromAsset(getAssets(), "fonts/libre2.ttf");
            viejaTasaRevire.setTypeface(typeface1);
            viejaTasaRevire.setPaintFlags(viejaTasaRevire.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            cintilloRevire.setVisibility(View.VISIBLE);
            dividerSimulador.setVisibility(View.GONE);

            nuevaTasaRevire.setText(nuevaTasa + "%");
            viejaTasaRevire.setText(getResources().getString(R.string.cintillo_revire_antes) + " "+viejaTasa + "%");
        }else{
            cintilloRevire.setVisibility(View.GONE);
            dividerSimulador.setVisibility(View.VISIBLE);
        }
    }

    private void goBmovilMenu(){
        MainController.getInstance().showScreen(MainController.getInstance().getMenuSuiteController().getClass());
    }

    public void isCreditShown(boolean flag) {
        delegate.setCreditState(flag);
    }

    @Override
    public void returnToPrincipal() {
        MainController.getInstance().showScreen(MenuPrincipalActivity.class);
    }

    @Override
    public void returnDataFromModule(String s, ServerResponse serverResponse) {

    }

    private void oneCall() {

        flowAccordingView(productsKeyList.get(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_3)));
        delegate.addProductId(viewsNumberList.indexOf(ConstantsCredit.IMG_OFERTADO_3));
    }

    public String getRespuestaOpinator() {
        return respuestaOpinator;
    }

    public void setRespuestaOpinator(String respuestaOpinator) {
        this.respuestaOpinator = respuestaOpinator;
    }

    public void setNombreCliente(){
        this.nCliente = MainController.getInstance().getSession().getNombreCliente();
        Log.e("correo","stNombrecliente: "+this.nCliente);
    }

    public String getNombreCliente(){
        return this.nCliente;
    }

    public boolean isSimulate() {
        return isSimulate;
    }

    public void setIsSimulate(boolean isSimulate) {
        this.isSimulate = isSimulate;
    }

    public Fragment getoFragment() {
        return oFragment;
    }

    public void setoFragment(Fragment oFragment) {
        this.oFragment = oFragment;
    }

    public String getCveProd() {
        return cveProd;
    }

    public void setCveProd(String cveProd) {
        this.cveProd = cveProd;
    }

    public boolean isFromWB() {
        return isFromWB;
    }

    public void setIsFromWB(boolean isFromWB) {
        this.isFromWB = isFromWB;
    }
}

