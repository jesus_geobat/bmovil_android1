package bbvacredit.bancomer.apicredit.gui.activities;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.common.timer.TimerController;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.MenuPrincipalCreditDelegate;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ControlEventos;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by FHERNANDEZ on 09/12/16.
 */
public class MenuPrincipalActivitySeekBar extends BaseActivity implements CallBackModule, View.OnClickListener{

    //Declarar importancia de los productos

    private MenuPrincipalCreditDelegate delegate;
    private OfertasListViewAdapter adapter;

    int scr_w, scr_h;

    //variables que indican si se ha desplegado el detalle
    public boolean isTDCDetail = false, isAnomDetail = false, isPPIetail = false,
            isOnomDetail = false, isAutoDetail = false, isHipoDetail = false, isILCdetail = false,
            isEFIDetail = false;

    public boolean isTDCS = false, isAnomS = false, isPPIS = false, isOnomS = false,
                    isAutoS = false, isHipoS = false, isILCS = false, isEFI = false;

    //variables que indican si el producto tiene campaña
    public boolean isConsumoCamp = false, isTDCCamp = false, isILCCamp = false;

    //variables que indican si se ha editado el texto, esto para verificar que cuando se cambie de edittex de producto, lance la operación
    //de calculo o eliminación
    public boolean isTDCEditable = false, isANOMEditable = false, isPPIEditable = false,
                    isAUTOEditable = false, isHIPOEditable = false, isILCEditable = false;

    public String isTDCEditableS, isANOMEditableS, isPPIEditableS, isAUTOEditableS,
                    isHIPOEditableS, isILCEditableS;

    public TDCSeekBarViewController                tdcFragment             = null;
    public AdelantoNominaSeekBarViewController     anomFragment            = null;
    public PrestamoPersonalSeekBarViewController   ppiFragment             = null;
    public CreditoNominaSeekBarViewController      creditoNominaFragment   = null;
    public AutoSeekBarViewController               autoFragment            = null;
    public HipotecarioSeekBarViewController        hipotecarioFragment     = null;
    public ILCSeekBarViewController                ilcFragment             = null;
//    private EfectivoInmediatoSeekBarViewController  efiFragment             = null;

    private final int idTDC = 01, idANOM = 02, idPPI = 03, idONOM = 04, idAUTO = 05, idHIPO = 06, idILC = 07, idEFI = 8;

    private Fragment oFragment;

    private String nCliente;

    //Componentes
    private Button btnCreditosDisponibles, btnCreditosContratados;

    private ImageView imgvLimpiarSim;

    private TextView txtvCreditos;

    private LinearLayout lnlBase;

    private Session oSession;

    private boolean isClearOnlyOne = true;

    private MenuPrincipalActivitySeekBar menuPrincipalActivitySeekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_BTN_ELIMINAR, R.layout.activity_credit_listview_creditos);

        setActivityChanging(true);
//        setActivityChanging(false);

        MainController.getInstance().setContext(this);
        MainController.getInstance().setCurrentActivity(this);

        menuPrincipalActivitySeekBar = this;
        delegate = new MenuPrincipalCreditDelegate();
        oSession = Tools.getCurrentSession();

        if (oSession.isClearOnly()) {
            oSession.setIsClearOnly(false);
            isClearOnlyOne = false;
            delegate.setCveProd("");
            delegate.doEliminar(menuPrincipalActivitySeekBar);

        }else {
            initView();

            Point size = new Point();
            scr_w = size.x;
            scr_h = size.y;
            hideSoftKeyboard();
            imgBtnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAlert("");
                }
            });
        }
    }

    private void initView(){
        setnCliente();
        setCredits();
        relationUI();
        setProductSeekBar();
    }

    private void setProductSeekBar(){
//        Tools.orderProducts(false); //false to save initial array
        buildFramDinamyc();
//        showComponentes();
    }

    /**
     * Si hay ofertas y tiene alternativas, primero se muestran las ofertas seguido de las alternativas
     * Si hay oferta que a su vez se encuentra en alternativa, se muestra la oferta con sus datos ofertados, si el usuario/cliente
     *      hace cambio en el monto se manda por simulador, si no hace cambio, se manda por flujo de oneclick
     * Si hay oferta pero no tiene alternativa esa oferta, el monto no se podrá modificar
     * Si no hay oferta pero si hay alternativas, flujo normal.
     */
    private void buildFramDinamyc(){

        if(MainController.getInstance().getPromociones() != null) {
            Promociones[] promociones = MainController.getInstance().getPromociones();
            //hay promociones, validar que tipo de promociones tiene
            if (promociones != null) {
                if (promociones.length != 0) {

                    for (int i = 0; i < promociones.length; i++) {

                        final String cveCamp = promociones[i].getCveCamp();
                        final int idCveCamp = asignIdByProduct(cveCamp.substring(0, 4));

                        FrameLayout fLayout = new FrameLayout(this);
                        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        fLayout.setLayoutParams(p);
                        fLayout.setId(idCveCamp);

                        if (cveCamp.startsWith(ConstantsCredit.CLAVE_EFI)) {

//                            showEFI(idCveCamp, promociones[i]);

                        } else if (cveCamp.startsWith(ConstantsCredit.CLAVE_CONSUMO)) {
                            isConsumoCamp = true;
                            showPPI(true);
                            ppiFragment.setPromocionConsumo(promociones[i]);

//                            showPPI();
                            showFragment(ppiFragment,idCveCamp);

                        } else if (cveCamp.startsWith(ConstantsCredit.CLAVE_TDC)) {
                            isTDCCamp = true;
                            showTDC();
                            tdcFragment.setPromocionTDC(promociones[i]);

                            showFragment(tdcFragment, idCveCamp);

                        } else if (cveCamp.startsWith(ConstantsCredit.CLAVE_ILC)) {
                            isILCCamp = true;
                            showILC();
                            ilcFragment.setPromocionILC(promociones[i]);
                            showFragment(ilcFragment, idCveCamp);
                        }

                        lnlBase.addView(fLayout);
                    }
                }
            } else
                Log.e("promociones", "Promociones nulas, no hay promociones");
        }else
            Log.e("promociones", "Promociones nulas, no hay promociones");

        if (Tools.getCurrentSession().getCreditos() != null) {

            ArrayList<Producto> productoArrayList = Tools.getCurrentSession().getCreditos().getProductos();

            if (productoArrayList != null) {  //construye dinámiamente frames para los demás productos

                for (int i = 0; i < productoArrayList.size(); i++) {

                    final String cveProd = productoArrayList.get(i).getCveProd();
                    final int idCveProd = asignIdByProduct(cveProd);

                    FrameLayout fLayout = new FrameLayout(this);
                    RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    fLayout.setLayoutParams(p);
                    fLayout.setId(idCveProd);

                    if (cveProd.equalsIgnoreCase(ConstantsCredit.TARJETA_CREDITO)) {
                        if (!isTDCCamp) {
                            showTDC();
                            showFragment(tdcFragment,idCveProd);
                        }

                    } else if (cveProd.equalsIgnoreCase(ConstantsCredit.ADELANTO_NOMINA)) {
                        showAdelantoNomina(idCveProd);

                    } else if (cveProd.equalsIgnoreCase(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)) {
                        if (!isConsumoCamp) {
                            showPPI(true);
                            showFragment(ppiFragment,idCveProd);
                        }

                    } else if (cveProd.equalsIgnoreCase(ConstantsCredit.CREDITO_NOMINA)) {
                    if(!isConsumoCamp)
                        showPPI(false);

                    } else if (cveProd.equalsIgnoreCase(ConstantsCredit.CREDITO_AUTO)) {
                        showAuto(idCveProd);

                    } else if (cveProd.equalsIgnoreCase(ConstantsCredit.CREDITO_HIPOTECARIO)) {
                        showHipo(idCveProd);

                    } else if (cveProd.equalsIgnoreCase(ConstantsCredit.INCREMENTO_LINEA_CREDITO)) {
                        if (!isILCCamp) {
                            showILC();
                            showFragment(ilcFragment,idCveProd);
                        }
                    }

                    lnlBase.addView(fLayout);
                }
            }
        }else
            Log.e("simulador","sin creditos ofertados");
    }

//    private void add

    private int asignIdByProduct(String cveProd){
     int idProd = 0;

        if (cveProd.equalsIgnoreCase(ConstantsCredit.TARJETA_CREDITO) || cveProd.startsWith(ConstantsCredit.CLAVE_TDC))
            idProd = idTDC;
        else if (cveProd.equalsIgnoreCase(ConstantsCredit.ADELANTO_NOMINA))
            idProd = idANOM;
        else if (cveProd.equalsIgnoreCase(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO) || cveProd.startsWith(ConstantsCredit.CLAVE_CONSUMO))
            idProd = idPPI;
        else if (cveProd.equalsIgnoreCase(ConstantsCredit.CREDITO_NOMINA) || cveProd.startsWith(ConstantsCredit.CLAVE_CONSUMO) )
            idProd = idONOM;
        else if (cveProd.equalsIgnoreCase(ConstantsCredit.CREDITO_AUTO))
            idProd = idAUTO;
        else if (cveProd.equalsIgnoreCase(ConstantsCredit.CREDITO_HIPOTECARIO))
            idProd = idHIPO;
        else if (cveProd.equalsIgnoreCase(ConstantsCredit.INCREMENTO_LINEA_CREDITO) || cveProd.startsWith(ConstantsCredit.CLAVE_ILC))
            idProd = idILC;
        else if(cveProd.startsWith(ConstantsCredit.CLAVE_EFI))
            idProd = idEFI;

        return idProd;
    }

    //01
    private void showTDC(){
        tdcFragment = new TDCSeekBarViewController();

        if(Session.getInstance().getCreditos()!=null) {
            if (Tools.getProductByCve(ConstantsCredit.TARJETA_CREDITO, Session.getInstance().getCreditos().getProductos()) != null)
                tdcFragment.setProducto(Tools.getProductByCve(ConstantsCredit.TARJETA_CREDITO, Session.getInstance().getCreditos().getProductos()));
        }
//        showFragment(tdcFragment, fmlId);
    }

    //02
    private void showAdelantoNomina(int fmlId){
        anomFragment = new AdelantoNominaSeekBarViewController();
        anomFragment.setProducto(Tools.getProductByCve(ConstantsCredit.ADELANTO_NOMINA, Session.getInstance().getCreditos().getProductos()));

        showFragment(anomFragment, fmlId);
    }

    //03
    private void showPPI(boolean isppi){
        ppiFragment = new PrestamoPersonalSeekBarViewController();

        if(Session.getInstance().getCreditos()!=null) {
            if (Tools.getProductByCve(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO, Session.getInstance().getCreditos().getProductos()) != null) {
                if (isppi)
                    ppiFragment.setProducto(Tools.getProductByCve(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO, Session.getInstance().getCreditos().getProductos()));
                else
                    ppiFragment.setProducto(Tools.getProductByCve(ConstantsCredit.CREDITO_NOMINA, Session.getInstance().getCreditos().getProductos()));

                ppiFragment.isppi = isppi;
            }
        }

//        showFragment(ppiFragment, fmlId);
    }

    //05
    private void showAuto(int fmlId){
        autoFragment = new AutoSeekBarViewController();
        autoFragment.setProducto(Tools.getProductByCve(ConstantsCredit.CREDITO_AUTO, Session.getInstance().getCreditos().getProductos()));

        showFragment(autoFragment, fmlId);
    }

    //06
    private void showHipo(int fmlId){
        hipotecarioFragment = new HipotecarioSeekBarViewController();
        hipotecarioFragment.setProducto(Tools.getProductByCve(ConstantsCredit.CREDITO_HIPOTECARIO, Session.getInstance().getCreditos().getProductos()));

        showFragment(hipotecarioFragment, fmlId);
    }

    //07
    private void showILC(){
        ilcFragment = new ILCSeekBarViewController();
        if(Session.getInstance().getCreditos()!=null) {
            if (Tools.getProductByCve(ConstantsCredit.INCREMENTO_LINEA_CREDITO, Session.getInstance().getCreditos().getProductos()) != null)
                ilcFragment.setProducto(Tools.getProductByCve(ConstantsCredit.INCREMENTO_LINEA_CREDITO, Session.getInstance().getCreditos().getProductos()));

        }
//        showFragment(ilcFragment, fmlId);
    }

    private void showEFI(int fmlId, Promociones promociones){
//        efiFragment = new EfectivoInmediatoSeekBarViewController();
//        efiFragment.setPromocion(promociones);
//
//        showFragment(efiFragment, fmlId);
    }

    public void updateGraph(String cveProd, String amount){

        String auxAmount = Tools.clearAmounr(amount);

        if(cveProd.equals(ConstantsCredit.TARJETA_CREDITO)){
            tdcFragment.updateGraph(auxAmount);

        }else if(cveProd.equals(ConstantsCredit.ADELANTO_NOMINA)){
            anomFragment.updateGraph(auxAmount);

        }else if(cveProd.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
            ppiFragment.updateGraph(amount);

        }else if(cveProd.equals(ConstantsCredit.CREDITO_NOMINA)){
            creditoNominaFragment.updateGraph(amount);

        }else if(cveProd.equals(ConstantsCredit.CREDITO_AUTO)){
            autoFragment.updateGraph(amount);

        }else if(cveProd.equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
            hipotecarioFragment.updateGraph(amount);

        }else if(cveProd.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
            ilcFragment.updateGraph(amount);

        }
    }


    //tras cualquier petición (2, 3, 4 o 5) se actualizan los valores de todos los productos
    //@param isOperationDelete, si es true se setean montos originales, si es false, se setean los recalculados
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void updateAllProductData(Fragment fragment, boolean isOperationDelete){
        Producto producto;
        int monmax;
        try {
            if (tdcFragment == null) {}
            else{
                if(! (fragment instanceof  TDCSeekBarViewController)) {
                    if (isOperationDelete)
                        producto = Tools.getProductByCve(ConstantsCredit.TARJETA_CREDITO, Session.getInstance().getCreditos().getProductos());
                    else
                        producto = Tools.getProductByCve(ConstantsCredit.TARJETA_CREDITO, Session.getInstance().getAuxCreditos().getProductos());

                    if (producto != null) {
                        if (producto.getIndSim().equalsIgnoreCase("N")) {
                            monmax = Integer.parseInt(Tools.validateMont(producto.getMontoMaxS(), true));

                            if (monmax > 0) {
                                isTDCCamp = false;
                                tdcFragment.setProducto(producto);
                                tdcFragment.eneableProd();
                                tdcFragment.initView(tdcFragment.getView());
                            } else {
                                tdcFragment.dissableProd();
                            }
                        }
                    }
                }
            }

            if (anomFragment == null) {}
            else{
                if(! (fragment instanceof AdelantoNominaSeekBarViewController)) {
                    if(isOperationDelete)
                        producto = Tools.getProductByCve(ConstantsCredit.ADELANTO_NOMINA, Session.getInstance().getCreditos().getProductos());
                    else
                        producto = Tools.getProductByCve(ConstantsCredit.ADELANTO_NOMINA, Session.getInstance().getAuxCreditos().getProductos());

                    if (producto != null) {
                        if (producto.getIndSim().equalsIgnoreCase("N")) {
                            monmax = Integer.parseInt(Tools.validateMont(producto.getMontoMaxS(), true));
                            if (monmax > 0) {
                                anomFragment.setProducto(producto);
                                anomFragment.enableProd();
                                anomFragment.initView(anomFragment.getView());
                            } else {
                                anomFragment.dissableProd();
                            }
                        }
                    }
                }
            }

            if (ppiFragment == null) {}
            else{
                if (!(fragment instanceof  PrestamoPersonalSeekBarViewController)) {
                    if (isOperationDelete)
                        producto = Tools.getProductByCve(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO, Session.getInstance().getCreditos().getProductos());
                    else
                        producto = Tools.getProductByCve(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO, Session.getInstance().getAuxCreditos().getProductos());

                    if(producto!= null) {
                        if (producto.getIndSim().equalsIgnoreCase("N")) {
                            monmax = Integer.parseInt(Tools.validateMont(producto.getMontoMaxS(), true));

                            if (monmax > 0) {
                                isConsumoCamp = false;
                                ppiFragment.setProducto(producto);
                                ppiFragment.enableProd();
                                ppiFragment.initView(ppiFragment.getView());
                            } else {
                                ppiFragment.dissableProd();
                            }
                        }
                    }
                }
            }

            if (autoFragment == null) {}
            else{
                if (!(fragment instanceof  AutoSeekBarViewController)) {
                    if (isOperationDelete)
                        producto = Tools.getProductByCve(ConstantsCredit.CREDITO_AUTO, Session.getInstance().getCreditos().getProductos());
                    else
                        producto = Tools.getProductByCve(ConstantsCredit.CREDITO_AUTO, Session.getInstance().getAuxCreditos().getProductos());

                    if(producto != null) {
                        if (producto.getIndSim().equalsIgnoreCase("N")) {
                            monmax = Integer.parseInt(Tools.validateMont(producto.getMontoMaxS(), true));

                            if (monmax > 0) {
                                autoFragment.setProducto(producto);
                                autoFragment.enableProd();
                                autoFragment.initView(autoFragment.getView());
                            } else {
                                autoFragment.dissableProd();
                            }
                        }
                    }
                }
            }

            if (hipotecarioFragment == null) {}
            else{
                if (!(fragment instanceof HipotecarioSeekBarViewController)) {
                    if (isOperationDelete)
                        producto = Tools.getProductByCve(ConstantsCredit.CREDITO_HIPOTECARIO, Session.getInstance().getCreditos().getProductos());
                    else
                        producto = Tools.getProductByCve(ConstantsCredit.CREDITO_HIPOTECARIO, Session.getInstance().getAuxCreditos().getProductos());

                    if (producto != null) {
                        if (producto.getIndSim().equalsIgnoreCase("N")) {
                            monmax = Integer.parseInt(Tools.validateMont(producto.getMontoMaxS(), true));
                            if (monmax > 0) {
                                hipotecarioFragment.setProducto(producto);
                                hipotecarioFragment.enableProd();
                                hipotecarioFragment.initView(hipotecarioFragment.getView());
                            } else {
                                hipotecarioFragment.dissableProd();
                            }
                        }
                    }
                }
            }

            if (ilcFragment == null) {}
            else{
                if(!(fragment instanceof ILCSeekBarViewController)) {
                    if (isOperationDelete)
                        producto = Tools.getProductByCve(ConstantsCredit.INCREMENTO_LINEA_CREDITO, Session.getInstance().getCreditos().getProductos());
                    else
                        producto = Tools.getProductByCve(ConstantsCredit.INCREMENTO_LINEA_CREDITO, Session.getInstance().getAuxCreditos().getProductos());

                    if(producto != null) {
                        if (producto.getIndSim().equalsIgnoreCase("N")) {
                            monmax = Integer.parseInt(Tools.validateMont(producto.getMontoMaxS(), true));

                            if (monmax > 0) {
                                isILCCamp = false;
                                ilcFragment.setProducto(producto);
                                ilcFragment.enableProd();
                                ilcFragment.initView(ilcFragment.getView());
                            } else {
                                ilcFragment.dissableProd();
                            }
                        }
                    }
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    //Si se ha clickeado en un monto se limpia, si se clickea a otro, el anterior debe guardar el mismo valor

    /**
     isILCEditable = false;

     isILCEditableS;
     */
    public void validateEdtMontoVisible(){

        if(isTDCEditable && (isTDCEditableS !=null || !isTDCEditableS.equals(""))){
            tdcFragment.edtMontoVisible.setText(isTDCEditableS);
            isTDCEditable = false;
            isTDCEditableS = "";

        }else  if(isANOMEditable && (isANOMEditableS !=null || !isANOMEditableS.equals(""))){
            anomFragment.edtMontoVisible.setText(isANOMEditableS);
            isANOMEditable = false;
            isANOMEditableS = "";

        }else  if(isPPIEditable && (isPPIEditableS !=null || !isPPIEditableS.equals(""))){
            ppiFragment.edtMontoVisible.setText(isPPIEditableS);
            isPPIEditable = false;
            isPPIEditableS = "";

        }else  if(isAUTOEditable && (isAUTOEditableS !=null || !isAUTOEditableS.equals(""))){
            autoFragment.edtMontoVisible.setText(isAUTOEditableS);
            isAUTOEditable = false;
            isAUTOEditableS = "";

        }else  if(isHIPOEditable && (isHIPOEditableS !=null || !isHIPOEditableS.equals(""))){
            hipotecarioFragment.edtMontoVisible.setText(isHIPOEditableS);
            isHIPOEditable = false;
            isHIPOEditableS = "";

        }else  if(isILCEditable && (isILCEditableS !=null || !isILCEditableS.equals(""))){
            ilcFragment.edtMontoVisible.setText(isILCEditableS);
            isILCEditable = false;
            isILCEditableS = "";
        }else
            Log.i("edt","primer monto a modificar, no se reestablece ninguno");


    }

    //Valida si algun detalle se ha desplegado, para que cuando se intente desplegar un segundo detalle, oculte el anterior
    public void validateIsShowAnyDetail(Fragment oFragment){

        if(!(oFragment instanceof TDCSeekBarViewController) && isTDCDetail){
            isTDCDetail = false;
            tdcFragment.setIsShowDetalle(true);
            tdcFragment.validateLayoutToShow();

        } else if (!(oFragment instanceof AdelantoNominaSeekBarViewController) && isAnomDetail){
            isAnomDetail = false;
            anomFragment.setIsShowDetalle(true);
            anomFragment.validateLayoutToShow();

        }else if (!(oFragment instanceof PrestamoPersonalSeekBarViewController) && isPPIetail){
            isPPIetail = false;
            ppiFragment.setIsShowDetalle(true);
            ppiFragment.validateLayoutToShow();

        }else if (!(oFragment instanceof CreditoNominaSeekBarViewController) && isOnomDetail){
            isOnomDetail = false;
            creditoNominaFragment.setIsShowDetalle(true);
            creditoNominaFragment.validateLayoutToShow();

        }else if (!(oFragment instanceof AutoSeekBarViewController) && isAutoDetail){
            isAutoDetail = false;
            autoFragment.setIsShowDetalle(true);
            autoFragment.validateLayoutToShow();

        }else if (!(oFragment instanceof HipotecarioSeekBarViewController) && isHipoDetail){
            isHipoDetail = false;
            hipotecarioFragment.setIsShowDetalle(true);
            hipotecarioFragment.validateLayoutToShow();

        }else if (!(oFragment instanceof ILCSeekBarViewController) && isILCdetail){
            isILCdetail = false;
            ilcFragment.setIsShowDetalle(true);
            ilcFragment.validateLayoutToShow();

        }else if (!(oFragment instanceof  EfectivoInmediatoSeekBarViewController) && isEFIDetail) {
            isEFIDetail = false;
//            efiFragment.setIsShowDetalle(true);
//            efiFragment.validateLayoutToShow();
        }else{
            Log.i("detalle", "no se oculta ningún detalle, es el primero");
        }
    }

    public void validateTxtvColor(Fragment oFragment){

        if(!(oFragment instanceof TDCSeekBarViewController) && isTDCS){
            isTDCS = false;
            tdcFragment.setColor(true);

        } else if (!(oFragment instanceof AdelantoNominaSeekBarViewController) && isAnomS){
            isAnomS = false;
            anomFragment.setColor(true);

        }else if (!(oFragment instanceof PrestamoPersonalSeekBarViewController) && isPPIS){
            isPPIS = false;
            ppiFragment.setColor(true);

        }else if (!(oFragment instanceof CreditoNominaSeekBarViewController) && isOnomS){
            isOnomS = false;
            creditoNominaFragment.setColor(true);

        }else if (!(oFragment instanceof AutoSeekBarViewController) && isAutoS){
            isAutoS = false;
            autoFragment.setColor(true);

        }else if (!(oFragment instanceof HipotecarioSeekBarViewController) && isHipoS){
            isHipoS = false;
            hipotecarioFragment.setColor(true);

        }else if (!(oFragment instanceof ILCSeekBarViewController) && isILCS){
            isILCS = false;
            ilcFragment.setColor(true);

        }else if (!(oFragment instanceof  EfectivoInmediatoSeekBarViewController) && isEFI){
            isEFI = false;
//            efiFragment.setColor(true);

        }else{
            Log.i("detalle", "no se oculta ningún detalle, es el primero");
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void showFragment(Fragment fragment, int frameLayout){
        getFragmentManager().beginTransaction().add(frameLayout,fragment).commit();
    }

    public void showAlert(final String cveProd){
        new AlertDialog.Builder(menuPrincipalActivitySeekBar)
                .setTitle("Alert")
                .setMessage(getApplicationContext().getResources().getString(R.string.menuprincipalactivity_alert_borrar_simulacion))
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
//                        setCveProd(cveProd);
                        delegate.setCveProd(cveProd);
                        delegate.doEliminar(menuPrincipalActivitySeekBar);
                        isClearOnlyOne = false;
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Atras", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void relationUI(){
        btnCreditosContratados  = (Button)findViewById(R.id.btnCreditosContratados);
        btnCreditosContratados.setOnClickListener(this);
        btnCreditosDisponibles  = (Button)findViewById(R.id.btnCreditosDisponibles);
        btnCreditosDisponibles.setOnClickListener(this);
        imgvLimpiarSim          = (ImageView)findViewById(R.id.imgvLimpiarSim);
        txtvCreditos            = (TextView)findViewById(R.id.txtvCreditos);

        lnlBase                 = (LinearLayout)findViewById(R.id.lnlBase);

    }

    private void setCredits() {
        Session session = Tools.getCurrentSession();

        if (session.getCreditos() != null) {
            delegate.setDataFromCredit(session.getCreditos());
            delegate.setShowCreditosContratados(session.getShowCreditosContratados());
            delegate.setShowCreditosOfertados(session.getShowCreditosOfertados());

            for (int i = 0; i< session.getCreditos().getProductos().size(); i++){
                Log.i("getProds","cveProd: "+session.getCreditos().getProductos().get(i).getCveProd()+", montmin: "+session.getCreditos().getProductos().get(i).getMontoMinS());
            }

        } else {
            delegate = new MenuPrincipalCreditDelegate();
        }
    }

    @Override
    protected void onStop() {
        if(ControlEventos.isAppIsInBackground(this.getBaseContext())){ //Valida el estado de la app si es change Activity no hace nada
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "La app se fue a Background");
            }
            TimerController.getInstance().initTimer(this, TimerController.getInstance().getClase());
        }
        super.onStop();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    @Override
    public void onBackPressed() {
        Session.getInstance().setIsClearOnly(true);
        MainController.getInstance().showScreen(MainController.getInstance().getMenuSuiteController().getClass());
//        MainController.getInstance().showScreen(MenuPrincipalActivity.class);
    }

    public String getnCliente() {
        return nCliente;
    }

    public void setnCliente() {
        this.nCliente = MainController.getInstance().getSession().getNombreCliente();
    }

    @Override
    public void returnToPrincipal() {
        if(Tools.getCurrentSession().getCreditos() != null)
            Session.getInstance().setIsClearOnly(true);
        MainController.getInstance().showScreen(MenuPrincipalActivitySeekBar.class);
    }

    @Override
    public void returnDataFromModule(String operation, ServerResponse response) {

    }

    @Override
    public void onClick(View v) {

    }
}
