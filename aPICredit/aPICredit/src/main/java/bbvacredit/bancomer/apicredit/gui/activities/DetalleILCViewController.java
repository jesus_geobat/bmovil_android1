package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.DetalleDeAlternativaDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.ILCDelegate;

/**
 * Created by OOROZCO on 02/12/15.
 */
public class DetalleILCViewController extends Fragment {

    // ------------- Attributes Region ----------- //

    private ImageButton btnRecalcularSimulacion;
    private ImageButton btnIncrementar;
    private DetalleDeAlternativaDelegate delegate;
    private ILCDelegate delegateOp;

    private TextView lblTDC2;
    private TextView lblMontoLimiteActual;
    private TextView lblResultadoPago;
    private TextView lblCat;

    private Session session;
    private Integer getProdIndex;

    public void setDelegateOp(ILCDelegate delegateOp) {
        this.delegateOp = delegateOp;
    }

    public void setDelegate(DetalleDeAlternativaDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_credit_resultado_simulacion_ilc, container, false);

        if (rootView != null)
            setUI(rootView);

        ((MenuPrincipalActivity) getActivity()).setIsSimulate(true);
        ((MenuPrincipalActivity) getActivity()).setoFragment(this);

        delegateOp.setDetalleILC(this);

        return  rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        session = Tools.getCurrentSession();
        getProdIndex = session.getProductoIndexByCve(ConstantsCredit.INCREMENTO_LINEA_CREDITO);

        Log.i("opinator", "guardando cveProd: " + delegateOp.getData().getProductos().get(getProdIndex).getCveProd());
    }

    @Override
    public void onStart() {
        super.onStart();
        setViewData();
    }

    private void setUI(View rootView)
    {
        btnRecalcularSimulacion = (ImageButton) rootView.findViewById(R.id.btnRecalcularSimulacionm);
        btnRecalcularSimulacion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //((MenuPrincipalActivity)getActivity()).showILC();
                delegateOp.saveOrDeleteSimulationRequest(false);
            }
        });

        btnIncrementar = (ImageButton) rootView.findViewById(R.id.btnIncrementar);

        btnIncrementar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                confirmarIncremento(getActivity());
            }
        });


        lblTDC2 = (TextView) rootView.findViewById(R.id.lblTDC2);
        lblCat = (TextView)  rootView.findViewById(R.id.lblCat);
        lblMontoLimiteActual = (TextView)  rootView.findViewById(R.id.lblMontoLimiteActual);
        lblResultadoPago = (TextView)  rootView.findViewById(R.id.lblResultadoPago);


        rootView.findViewById(R.id.btnVerResumenPagos).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Tools.getCurrentSession().setOfertaILC(delegate.getOfertaILC());
                MainController.getInstance().showScreen(ResumenViewController.class);
                MainController.getInstance().setFlowFragment(DetalleILCViewController.this);
            }

        });
    }

    public void setViewData()
    {
        String text =  getActivity().getString(R.string.lblTDC2) + " " + Tools.hideAccountNumber(delegate.getIlcViewController().getDelegate().getProducto().getNumTDCS());
        String fechaCat="";
        lblTDC2.setText(text);
        Log.w("ILC", "Terminaciónn: " + text);
        Log.w("ILC", "Terminaciónn: " + delegate.getIlcViewController().getDelegate().getProducto().getNumTDCS());
        lblMontoLimiteActual.setText(Tools.formatAmount(delegate.getOfertaILC().getLineaActual(), false));
        lblResultadoPago.setText(Tools.formatAmount(delegate.getOfertaILC().getLineaFinal(), false));

        Log.i("ILC", "fecha: " + delegate.getOfertaILC().getFechaCat());

        String fecha = delegate.getOfertaILC().getFechaCat();
        SimpleDateFormat sDF = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date oDate = sDF.parse(fecha);
            sDF = new SimpleDateFormat("dd-MM-yyyy");
            fechaCat = sDF.format(oDate);

            System.out.println(fechaCat + " - " + oDate.toString());

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }


    public void confirmarIncremento(Context context){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(context);
        dialogo1.setTitle("Aviso");
        dialogo1.setMessage("¿Al autorizar esta oferta incrementará tu línea de crédito.?");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                delegate.contratacionILC();
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {

            }
        });
        dialogo1.show();
    }


    public void incrementarAction()
    {
        delegate.contratacionILC();
    }

    public void simpleUpdateMenu(String cveProd){

        if(((MenuPrincipalActivity)getActivity()) == null)
            Log.i("eliminar", "Menu es nulo");

        Log.i("eliminar", "actualizaMenu ILC, cveProd: " + cveProd);
        ((MenuPrincipalActivity)getActivity()).updateMenu(cveProd);
        getFragmentManager().beginTransaction().remove(((MenuPrincipalActivity) getActivity()).getActiveFragment()).commit();
        ((MenuPrincipalActivity)getActivity()).showILC();
    }


}

