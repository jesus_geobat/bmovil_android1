package bbvacredit.bancomer.apicredit.gui.delegates;

import android.util.Log;

import java.util.Hashtable;
import java.util.Iterator;

import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.AdelantoNominaSeekBarViewController;
import bbvacredit.bancomer.apicredit.gui.activities.OfertasListViewAdapter;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;

/**
 * Created by FHERNANDEZ on 28/12/16.
 */
public class AdelantoNominaSeekBarDelegate extends BaseDelegateOperacion {

    private AdelantoNominaSeekBarViewController controller;
    private Producto producto;
    private ObjetoCreditos data;
    private ObjetoCreditos auxData;
    private Session session;
    private boolean isDeleteOperation = false; //si es true, borrar simulación
    private boolean productoConPlazo;
    private String indSim;

    public AdelantoNominaSeekBarDelegate(AdelantoNominaSeekBarViewController controller){
        this.controller = controller;
        session = Tools.getCurrentSession();
    }


    public void analyzeResponse(String operationId, ServerResponse response) {
        if(getCodigoOperacion().equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                MainController.getInstance().ocultaIndicadorActividad();
                session.setAuxCreditos(data);
//                session.setCreditos(data);
                setProducto(Tools.getProductByCve("ANOM", data.getProductos()));

                //actualización de los objetos
                controller.updateData(false);

                indSim = getProducto().getIndSim();
                Log.i("indsim", "recalcOp, tdcIndSim: " + indSim);
                controller.setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);

//                saveOrDeleteSimulationRequest(true);

                controller.setSimulation();
                controller.isSimulated = true;

                if(controller.isShowDetalle()){
                    controller.setDetailProduct();

                }else if (!controller.isFromBar) {
                    controller.setIsShowDetalle(false);
                    controller.validateLayoutToShow();
                }

            }
        }else if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

                CalculoData respuesta = (CalculoData) response.getResponse();

                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

//                session.setCreditos(data);
                session.setAuxCreditos(data);
                setProducto(Tools.getProductByCve("ANOM", data.getProductos()));

                //actualización de los objetos

                indSim = getProducto().getIndSim();
                controller.setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);


                if(isDeleteOperation){
                    controller.updateData(true);
                    isDeleteOperation = false;
                    controller.isSimulated = false;
                    doRecalculo();

                }else{
                    controller.updateData(false);
                    Log.i("contrato","contratar anom por simulador");
                    controller.adquiereANOM();
                }
            }
        }
    }

    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(Server.CALCULO_ALTERNATIVAS_OPERACION, ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        if (isSavingOperation) {
            addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = false;
        } else {
            addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = true;
        }

        Hashtable<String, String> paramTable2 = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    public void doRecalculo(){

        String plazoSel= "";
        String cvePlazoSel = "";
        String montoSel = "";

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
        Producto ccr = controller.getProducto();

        Log.i("detalle","cveProd doRecalc: "+ccr.getCveProd());
        montoSel = controller.edtMontoVisible.getText().toString();
        montoSel = montoSel.replace("$","");
        montoSel = montoSel.replace(",","");
        cvePlazoSel = ccr.getCvePzoE();

        addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM, paramTable);
        addParametro(ccr.getSubproducto().get(0).getCveSubp(), ServerConstantsCredit.CVESUBP_PARAM, paramTable);
        addParametro(cvePlazoSel, ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        addParametro("", ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM, paramTable);

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    public void consultaDetalleAlternativa(){

        Session session = Tools.getCurrentSession();
        Integer getProdIndex = session.getProductoIndexByCve(ConstantsCredit.ADELANTO_NOMINA);

        String ium = session.getIum();
        String numeroCelular = session.getNumCelular();

        String cveCamp = controller.getProducto().getCveProd();
//                this.getData().getProductos().get(getProdIndex).getCveProd();
        String cliente = session.getIdUsuario();

        Session.getInstance().setCveCamp(cveCamp);

        DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(cliente,ium,numeroCelular,cveCamp,controller.getProducto());
        delegate.consultaDetalleAlternativasTask(getProducto());
    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }


    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    protected String getCodigoOperacion() {
        return Server.CALCULO_OPERACION;
    }
}
