package bbvacredit.bancomer.apicredit.gui.delegates;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.MenuPrincipalActivity;
import bbvacredit.bancomer.apicredit.gui.activities.MenuPrincipalActivitySeekBar;
import bbvacredit.bancomer.apicredit.gui.activities.OldMenuPrincipalActivity;
import bbvacredit.bancomer.apicredit.gui.activities.PrestamosContratadosViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ConsultaAlternativasData;
import bbvacredit.bancomer.apicredit.models.CreditoContratado;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;
import bbvacredit.bancomer.apicredit.R;


public class MenuPrincipalCreditDelegate  extends BaseDelegateOperacion {

	private Session session;

	private ConsultaAlternativasData data;

	private Boolean showCreditosOfertados = true;

	private Boolean showCreditosContratados = true;

	private Integer tipoOperacion;

	//private OldMenuPrincipalActivity act;

	private MenuPrincipalActivitySeekBar oMenuPrincipal;

	private boolean isDeleteOperation = false;

	private String cveProd;

	public String getCveProd() {
		return cveProd;
	}

	public void setCveProd(String cveProd) {
		this.cveProd = cveProd;
	}

	public Boolean getRegresar(){
		if(!getCreditsSim().isEmpty() || !getProductsSim().isEmpty()){
			session.setRegresar(true);
		}else{
			session.setRegresar(false);
		}
		return session.getRegresar();
	}

	public ConsultaAlternativasData getData() {
		return data;
	}

	public void setData(ConsultaAlternativasData data) {
		this.data = data;
	}

	public Boolean getShowCreditosContratados() {
		return showCreditosContratados;
	}

	public void setShowCreditosContratados(Boolean showCreditosContratados) {
		this.showCreditosContratados = showCreditosContratados;
	}

	public Boolean getShowCreditosOfertados() {
		return showCreditosOfertados;
	}

	public void setShowCreditosOfertados(Boolean showCreditosOfertados) {
		this.showCreditosOfertados = showCreditosOfertados;
	}

	/**
	 * Default constructor
	 */
	public MenuPrincipalCreditDelegate() {
		session = Session.getInstance(MainController.getInstance().getContext());
	}

	public void setDataFromCredit(ObjetoCreditos creditos) {
		data = new ConsultaAlternativasData(creditos.getEstado(), creditos.getMontotSol(), creditos.getPorcTotal(), creditos.getPagMTot(), null, creditos.getProductos(), creditos.getCreditos(), creditos.getCreditosContratados(), null, null);
	}

	public <T> void redirectToView(Class<T> c){
		MainController.getInstance().showScreen(c);
	}

	@Override
	protected String getCodigoOperacion() {
		// TODO Auto-generated method stub
		return Server.CONSULTA_ALTERNATIVAS;
	}

	public ArrayList<Producto> getCreditosOfertados(){
		return data.getProductos();
	}

	public ArrayList<CreditoContratado> getCreditosContratados(){
		return data.getCreditosContratados();
	}

	public void resetStates(){
		Iterator<Producto> it = session.getCreditos().getProductos().iterator();

		while(it.hasNext()){
			it.next().setIndSimBoolean(false);
		}

		Iterator<CreditoContratado> itC = session.getCreditos().getCreditosContratados().iterator();

		while(itC.hasNext()){
			itC.next().setIndicadorSim(false);
		}

		data.setProductos(session.getCreditos().getProductos());
		data.setCreditosContratados(session.getCreditos().getCreditosContratados());
	}

	public void saveStates(HashMap<Integer,Session.State> states){
		session.setStateList(states);
	}

	public void resetContStates(){

		Iterator<CreditoContratado> itC = session.getCreditos().getCreditosContratados().iterator();

		while(itC.hasNext()){
			itC.next().setIndicadorSim(false);
		}

		data.setCreditosContratados(session.getCreditos().getCreditosContratados());
	}

	public HashMap<Integer,Session.State> getStates(){
		return session.getStateList();
	}

	private Integer getNumeroOperacion(String op){
		Integer ret = 0;
		if(op.equals(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS_STR)){
			ret = ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS;
		}else if(op.equals(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS_STR)){
			ret = ConstantsCredit.OP_GUARDAR_ALTERNATIVAS;
		}else if(op.equals(ConstantsCredit.OP_ELIMINAR_STR)){
			ret = ConstantsCredit.OP_ELIMINAR;
		}

		return ret;
	}

	/**
	 * Peticiones
	 */

	private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}else{
			paramTable.put(cnt, "");
			Log.d(param.getClass().getName(), param.toString() + " empty or null");
		}
	}

	private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}
	}

	public void addProductId(Integer index, Boolean isLiquidacion){
		SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
		Editor editor = sp.edit();
		if(isLiquidacion){
			editor.putInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX_LIQ, index);

		}else{
			editor.putInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, index);

		}
		editor.commit();
	}

    public void addProductId(Integer index){
        SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
        Editor editor = sp.edit();
        editor.putInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, index);
        editor.commit();
    }

    public void setCreditState(boolean flag){
        SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
        Editor editor = sp.edit();
        editor.putBoolean(ConstantsCredit.IS_SHOWN, flag);
        editor.commit();
    }

	public ArrayList<Producto> getProductsSim(){
		ArrayList<Producto> lista = new ArrayList<Producto>();
		if(data.getProductos() != null){
			Iterator<Producto> it = data.getProductos().iterator();

			while(it.hasNext()){
				Producto p = it.next();
				if(p.getIndSimBoolean()){
					lista.add(p);
				}
			}

		}
		return lista;
	}

	public ArrayList<CreditoContratado> getCreditsSim(){

		ArrayList<CreditoContratado> lista = new ArrayList<CreditoContratado>();
		if(data.getProductos() != null){
			Iterator<CreditoContratado> it = data.getCreditosContratados().iterator();

			while(it.hasNext()){
				CreditoContratado cr = it.next();
				if(cr.getIndicadorSim()){
					lista.add(cr);
				}
			}
		}
		return lista;
	}

	public void doEliminar(MenuPrincipalActivitySeekBar context){
		this.oMenuPrincipal = context;
		isDeleteOperation = true;

		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();

		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);

		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);

		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);

		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

		// Mapeamos el tipo de operacion -> 4
		addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);
		tipoOperacion = ConstantsCredit.OP_ELIMINAR;

		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
		this.doNetworkOperation(Server.CALCULO_OPERACION, paramTable2,true,new CalculoData(),MainController.getInstance().getContext());
	}

	public void doGuardar(OldMenuPrincipalActivity act){
		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();

		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);

		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);

		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);

		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

		// Mapeamos el tipo de operacion -> 3
		addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);
		tipoOperacion = ConstantsCredit.OP_GUARDAR_ALTERNATIVAS;
		//this.act = act;
		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
		this.doNetworkOperation(Server.CALCULO_OPERACION, paramTable2,true,new CalculoData(),MainController.getInstance().getContext());
	}

	public void doCalculo(){

		Hashtable<String, String> paramTable = new Hashtable<String, String>();

		// Mapeamos el codigo de operacion
		addParametro(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);

		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);

		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);

		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

		//This line is used to call the new Activity.		

		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.consultaAlternativas(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2,true,new ConsultaAlternativasData(), MainController.getInstance().getContext());
	}

	public void changeContSimulated(Integer index,OldMenuPrincipalActivity act){
		if(session.getCreditos().getCreditosContratados() != null){
			if(session.getCreditos().getCreditosContratados().size() > index){
				session.getCreditos().getCreditosContratados().get(index).setIndicadorSim(false);
				data.getCreditosContratados().get(index).setIndicadorSim(false);
				act.checkStates();
//				session.setVisibilityButton(act, R.id.pGlobalBottomMenuRC);
//				session.setVisibilityButton(act, R.id.pGlobalBottomMenuGuardar);
			}
		}
	}

	public void changeOfertSimulated(Integer index,OldMenuPrincipalActivity act){
		if(session.getCreditos().getProductos() != null){
			if(session.getCreditos().getProductos().size() > index){
				session.getCreditos().getProductos().get(index).setIndSimBoolean(false);
				data.getProductos().get(index).setIndSimBoolean(false);
				act.checkStates();
//				session.setVisibilityButton(act, R.id.pGlobalBottomMenuRC);
//				session.setVisibilityButton(act, R.id.pGlobalBottomMenuGuardar);
			}
		}
	}

	private void setSim2False(){
		for (int i = 0; i < data.getProductos().size(); i++) {
			data.getProductos().get(i).setIndSimBoolean(Boolean.FALSE);
		}
	}

	private void setSim2FalseCContratados(){

		/**
		 * The line 330 was causing an "array out of bounds" problem, so it was
		 * changed by the line 331.
		 * Aril 8th, 2015.
		 * Modified by Omar Orozco Silverio. 
		 */
		//for (int i = 0; i < data.getProductos().size(); i++) {
		for (int i = 0; i < data.getCreditosContratados().size(); i++) {	
			data.getCreditosContratados().get(i).setIndicadorSim(Boolean.FALSE);
		}
	}

	public void analyzeResponse(String operationId, ServerResponse response) {

		if(getCodigoOperacion().equals(operationId)){
			if((response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)||(response.getStatus() == ServerResponse.OPERATION_OPTIONAL_UPDATE)){

                MainController.getInstance().setHabilitarVista(false);

                data = (ConsultaAlternativasData) response.getResponse();

				if(data == null)
					Log.e("Error", "No existe información");

				// Comprobamos que la lista de productos no sea vac�a
				if((data.getProductos() != null)&&(!data.getProductos().isEmpty())){
					// seteamos el indicadorSim a false de todo el objeto
//					setSim2False();
				}else{
					// No se muestran
					showCreditosOfertados = false;
				}

				// Comprobamos que la lista de CredContratados no sea vac�a
				if((data.getCreditosContratados() != null)&&(!data.getCreditosContratados().isEmpty())){
					// seteamos el indicadorSim a false de todo el objeto
//					setSim2FalseCContratados();
				}else{
					// No se muestran
					showCreditosContratados = false;
				}

				// Guradar en sesion parametros para mostrar creditos
				session.setShowCreditosContratados(showCreditosContratados);
				session.setShowCreditosOfertados(showCreditosOfertados);

				Log.e("estado", data.getEstado());
				if(data.getProductos()==null)
					Log.e("Productos", "nulos");
				// Guardamos el objeto Creditos
				Log.i("session","MenuPCD, creditosContratados: "+data.getCreditosContratados());
				session.setCreditos(new ObjetoCreditos(data.getEstado(), data.getMontotSol(), data.getPorcTotal(),
						data.getPagMTot(), data.getProductos(), data.getCreditos(), data.getCreditosContratados()));

				ArrayList<Producto>list = data.getProductos();
				for (int i = 0; i<list.size(); i++){
					Log.i("update","MPCD.CalOperacion. CveProd: "+list.get(i).getCveProd()+", monMax: "+list.get(i).getMontoMaxS()+", indS: "+list.get(i).getIndSim());
				}

				for (int i = 0; i<data.getCreditosContratados().size(); i++) {
					Log.i("creditoscontr","getCreditosContratados: "+data.getCreditosContratados().get(i).getCveProd());
				}

				if(!showCreditosOfertados && !showCreditosContratados) {
					MainController.getInstance().getActivityController().getCurrentActivity().showInformationAlertPL(MainController.getInstance().getContext().getString((R.string.label_alert_information_noCredits)));
				} else {
                    if(!showCreditosOfertados & showCreditosContratados)
                        redirectToView(PrestamosContratadosViewController.class);
                    else
//                        redirectToView(MenuPrincipalActivity.class);
					redirectToView(MenuPrincipalActivitySeekBar.class);
				}
			}
			else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				MainController.getInstance().getMenuSuiteController().habilitarVista();
				Log.i("mensajes","getResponse: "+response.getResponse());
				Log.i("mensajes","getMesasC: "+response.getMessageCode());
				Log.i("mensajes","getMesasT: "+response.getMessageText());
				Log.i("mensajes","getStatus"+response.getStatus());

				if (MainController.getInstance().getPromociones()!=null || MainController.getInstance().getPromociones().length>0){
					Tools.getCurrentSession().setIsClearOnly(false);
					redirectToView(MenuPrincipalActivitySeekBar.class);
				}
			}
		}else if(Server.CALCULO_OPERACION.equals(operationId)){
			if((response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)||(response.getStatus() == ServerResponse.OPERATION_OPTIONAL_UPDATE)) {
				CalculoData respuesta = (CalculoData) response.getResponse();
//				data = (ConsultaAlternativasData) response.getResponse();


					if (isDeleteOperation) { //si mandó petición de borrar, actualizar menú y mostrar posición global con valores iniciales


						// Comprobamos que la lista de productos no sea vac�a
//						if ((respuesta.getProductos() != null) && (!respuesta.getProductos().isEmpty())) {
//							setSim2False(); // seteamos el indicadorSim a false de todo el objeto
//						} else {
//							showCreditosOfertados = false;
//						}
//						if ((data.getCreditosContratados() != null) && (!data.getCreditosContratados().isEmpty())) {
//							setSim2FalseCContratados(); // seteamos el indicadorSim a false de todo el objeto
//						} else {
//							showCreditosContratados = false;
//						}

						session.setCreditos(new ObjetoCreditos(respuesta.getEstado(), respuesta.getMontotSol(), respuesta.getPorcTotal(),
								respuesta.getPagMTot(), respuesta.getProductos(), respuesta.getCreditos(), null));
//						Log.i("dataget", "data.getCredSize: " + data.getCreditosContratados().size());

						// Guradar en sesion parametros para mostrar creditos
						session.setShowCreditosContratados(showCreditosContratados);
						session.setShowCreditosOfertados(showCreditosOfertados);

						isDeleteOperation = false;
						if (!getCveProd().isEmpty()) {
	//						oMenuPrincipal.updateMenu(oMenuPrincipal.getCveProd());
							validateProductToShow();
						} else {
	//						MainController.getInstance().showScreen(MenuPrincipalActivity.class);
							MainController.getInstance().showScreen(MenuPrincipalActivitySeekBar.class);
						}

					}

				MainController.getInstance().ocultaIndicadorActividad();
			}

		}
	}

	private void validateProductToShow(){

		Log.i("eliminar","muestra elemento: "+getCveProd()+", despues de eliminar");

		String cvePro = getCveProd();

//		oMenuPrincipal.setCintilloRevire("","",false);
//		if(cveProd.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO))
//			oMenuPrincipal.showILC();
//		else if(cvePro.equals(ConstantsCredit.TARJETA_CREDITO))
//			oMenuPrincipal.showTDC();
//		else if(cvePro.equals(ConstantsCredit.CREDITO_HIPOTECARIO))
//			oMenuPrincipal.showHipotecario();
//		else if(cvePro.equals(ConstantsCredit.CREDITO_AUTO))
//			oMenuPrincipal.showAuto();
//		else if(cvePro.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO))
//			oMenuPrincipal.showPPI();
//		else if(cvePro.equals(ConstantsCredit.CREDITO_NOMINA))
//			oMenuPrincipal.showNomina();
//		else if(cvePro.equals(ConstantsCredit.ADELANTO_NOMINA))
//			oMenuPrincipal.showAdelantoNomina();


		setCveProd("");
	}

	public void setSessionByBmovil(String userNumber,String password,String ium, String user,String mail,String operacion) {
		session.setIdUsuario(userNumber);
		session.setPassword(password);
		session.setIum(ium);
		Log.e("Número Celular", user);
		session.setNumCelular(user);
		session.setEmail(mail);
		session.setOperacion(operacion);
	}


}
