package bbvacredit.bancomer.apicredit.common;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.ArrowKeyMovementMethod;
import android.text.method.MovementMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.EditText;
import android.widget.TextView;

import bbvacredit.bancomer.apicredit.gui.activities.AutoViewController;
import bbvacredit.bancomer.apicredit.gui.activities.MenuPrincipalActivity;
import bbvacredit.bancomer.apicredit.gui.activities.MenuPrincipalActivitySeekBar;

/**
 * Created by Packo on 23/05/2016.
 */
public class LabelMontoTotalCredit extends EditText{

    private String montoSolicitado;
    private String montoMax;
    private String montoMin;
    private String cveProd;

    public String getCveProd() {
        return cveProd;
    }

    public void setCveProd(String cveProd) {
        this.cveProd = cveProd;
    }

    public String getMontoMax() {
        return montoMax;
    }

    public void setMontoMax(String montoMax) {
        this.montoMax = montoMax;
    }

    public String getMontoMin() {
        return montoMin;
    }

    public void setMontoMin(String montoMin) {
        this.montoMin = montoMin;
    }

    public LabelMontoTotalCredit(Context context) {
        super(context);
    }

    public LabelMontoTotalCredit(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LabelMontoTotalCredit(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected boolean getDefaultEditable() {
        return true;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {

        //si algun producto retorna true, se manda llamar el back de la aplicación, si retorna false
        //se calcula el monto ingresado
        boolean isBackProd = true;

        try {
            final String cveP = getCveProd();
            Log.i("editar","cveProd: "+cveP);

            if (cveP.equalsIgnoreCase(ConstantsCredit.ADELANTO_NOMINA)){
                isBackProd = ((MenuPrincipalActivitySeekBar)getContext()).anomFragment.isBackProd;
                if(!isBackProd)
                    ((MenuPrincipalActivitySeekBar)getContext()).anomFragment.isBackProd = true;

            }else if (cveP.equalsIgnoreCase(ConstantsCredit.CREDITO_AUTO)){
                isBackProd = ((MenuPrincipalActivitySeekBar)getContext()).autoFragment.isBackProd;
                if(!isBackProd)
                    ((MenuPrincipalActivitySeekBar)getContext()).autoFragment.isBackProd = true;

            }else if (cveP.equalsIgnoreCase(ConstantsCredit.CREDITO_HIPOTECARIO)){
                isBackProd = ((MenuPrincipalActivitySeekBar)getContext()).hipotecarioFragment.isBackProd;
                if (!isBackProd)
                    ((MenuPrincipalActivitySeekBar)getContext()).hipotecarioFragment.isBackProd = true;

            }else if (cveP.equalsIgnoreCase(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
                isBackProd = ((MenuPrincipalActivitySeekBar)getContext()).ppiFragment.isBackProd;
                if (!isBackProd)
                    ((MenuPrincipalActivitySeekBar)getContext()).ppiFragment.isBackProd = true;

            }else if (cveP.equalsIgnoreCase(ConstantsCredit.TARJETA_CREDITO)){
                isBackProd = ((MenuPrincipalActivitySeekBar)getContext()).tdcFragment.isBackProd;
                if (!isBackProd)
                    ((MenuPrincipalActivitySeekBar)getContext()).tdcFragment.isBackProd = true;

            }else if (cveP.equalsIgnoreCase(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
                isBackProd = ((MenuPrincipalActivitySeekBar)getContext()).ilcFragment.isBackProd;
                if (!isBackProd)
                    ((MenuPrincipalActivitySeekBar)getContext()).ilcFragment.isBackProd = true;

            }else if (cveP.equalsIgnoreCase(ConstantsCredit.CREDITO_NOMINA)){
                isBackProd = ((MenuPrincipalActivitySeekBar)getContext()).ppiFragment.isBackProd;
                if (!isBackProd)
                    ((MenuPrincipalActivitySeekBar)getContext()).ppiFragment.isBackProd = true;

            }

            Log.i("editar","cveProd, isBack: "+isBackProd);

            if (isBackProd){
                ((MenuPrincipalActivitySeekBar)getContext()).onBackPressed();

            }else if(keyCode == KeyEvent.KEYCODE_BACK){

                montoSolicitado = getText().toString();
                montoMax = getMontoMax();
                montoMin = getMontoMin();

                if(!getText().toString().trim().equals("")) {
                    String respuesta = Tools.validateMont(montoMax, montoMin, montoSolicitado, null);
                    Log.i("compare","respuesta: "+respuesta);

                    if (respuesta.equals(ConstantsCredit.MONTO_MAYOR)) {
                        setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                        ((MenuPrincipalActivitySeekBar)getContext()).updateGraph(getCveProd(),montoMax);

                    } else if (respuesta.equals(ConstantsCredit.MONTO_MENOR)) {
                        setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMin)));
                        ((MenuPrincipalActivitySeekBar)getContext()).updateGraph(getCveProd(),montoMin);

                    } else if (respuesta.equals(ConstantsCredit.MONTO_EN_RANGO)) {
                        if(!getText().toString().startsWith("$"))
                            setText("$"+getText());
                        String mont = getText().toString();
                        ((MenuPrincipalActivitySeekBar)getContext()).updateGraph(getCveProd(),montoSolicitado);
                    }
                }else {
                    setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                    ((MenuPrincipalActivitySeekBar)getContext()).updateGraph(getCveProd(), montoMin);
                }

                setSelection(length());
                clearFocus();
                endBatchEdit();

                ((MenuPrincipalActivitySeekBar)getContext()).hideSoftKeyboard();

            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return super.onKeyPreIme(keyCode, event);
    }

    public void extendSelection(int index) {
        Selection.extendSelection(getText(), index);
    }
    public void selectAll() {
        Selection.selectAll(getText());
    }
    public void setSelection(int index) {
        Selection.setSelection(getText(), index);
    }
    public void setSelection(int start, int stop) {
        Selection.setSelection(getText(), start, stop);
    }
    @Override
    protected MovementMethod getDefaultMovementMethod() {
        return ArrowKeyMovementMethod.getInstance();
    }

    @Override
    public Editable getText() {
        return (Editable) super.getText();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, BufferType.EDITABLE);
    }

    @Override
    public void setEllipsize(TextUtils.TruncateAt ellipsis) {
        if (ellipsis == TextUtils.TruncateAt.MARQUEE) {
            throw new IllegalArgumentException("EditText cannot use the ellipsize mode "
                    + "TextUtils.TruncateAt.MARQUEE");
        }
        super.setEllipsize(ellipsis);
    }

    @Override
    public void onInitializeAccessibilityEvent(AccessibilityEvent event) {
        super.onInitializeAccessibilityEvent(event);
        event.setClassName(EditText.class.getName());
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo info) {
        super.onInitializeAccessibilityNodeInfo(info);
        info.setClassName(EditText.class.getName());
    }


}
