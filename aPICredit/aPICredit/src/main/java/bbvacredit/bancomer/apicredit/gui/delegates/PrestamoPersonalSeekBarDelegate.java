package bbvacredit.bancomer.apicredit.gui.delegates;

import android.util.Log;

import java.util.Hashtable;

import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.models.OfertaConsumo;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.AdelantoNominaSeekBarViewController;
import bbvacredit.bancomer.apicredit.gui.activities.CreditoNominaSeekBarViewController;
import bbvacredit.bancomer.apicredit.gui.activities.PrestamoPersonalSeekBarViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by FHERNANDEZ on 28/12/16.
 */
public class PrestamoPersonalSeekBarDelegate extends BaseDelegateOperacion {

    private PrestamoPersonalSeekBarViewController controller;
    private Producto producto;
    private ObjetoCreditos data;
    private ObjetoCreditos auxData;
    private Session session;
    private boolean isDeleteOperation = false; //si es true, borrar simulación
    private boolean productoConPlazo;
    private boolean isPPI = false;

    private boolean isSavingOperation = false;
    private boolean updateData = false;

    private Promociones promociones;

    private String indSim, cveSub, cvePze;

    public PrestamoPersonalSeekBarDelegate(PrestamoPersonalSeekBarViewController controller){
        this.controller = controller;
        session = Tools.getCurrentSession();
    }

    public void analyzeResponse(String operationId, ServerResponse response) {

        if(Server.CONSULTA_DETALLE_CONSUMO.equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
//                Log.i("consumo","oneclick detalle exitoso");
                bbvacredit.bancomer.apicredit.models.OfertaConsumo oferta = (bbvacredit.bancomer.apicredit.models.OfertaConsumo) response.getResponse();
//                Log.i("consumo","oferta: "+oferta.getImporte());
//                Log.i("consumo","oferta: "+oferta.getProducto());
//                Log.i("consumo","oferta: "+oferta.getPlazo());
//                Log.i("consumo","oferta: "+oferta.getPlazoDes());
//                Log.i("consumo","oferta: "+oferta.getFechaCat());
//                Log.i("consumo", "oferta: " + oferta.getTasaMensual());

                controller.setOfertaConsumo(oferta);
                if (!controller.isFromBar)
                    controller.validateLayoutToShow();
            }
        }
        if(getCodigoOperacion().equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                MainController.getInstance().ocultaIndicadorActividad();
                session.setAuxCreditos(data);
                setProducto(Tools.getProductByCve(controller.isppi ? "0PPI" : "0NOM", data.getProductos()));

                //actualización de los objetos
                controller.updateData(false);

                indSim = getProducto().getIndSim();
                controller.setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);

                controller.setSimulation();
                controller.isSimulated = true;

                if(controller.isShowDetalle()){
                    controller.setDetailProduct();

                }else if (!controller.isFromBar) {
                    controller.setIsShowDetalle(false);
                    controller.validateLayoutToShow();
                }

            }
        }else if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

                CalculoData respuesta = (CalculoData) response.getResponse();

                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                MainController.getInstance().ocultaIndicadorActividad();
                session.setAuxCreditos(data);  //se guarda por separado

                setProducto(Tools.getProductByCve(controller.isppi ? "0PPI" : "0NOM", data.getProductos()));


                indSim = getProducto().getIndSim();

                controller.setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);


                if(isDeleteOperation){
                    controller.updateData(true);
                    isDeleteOperation = false;
                    controller.isSimulated = false;

                    if (updateData)
                        updateData(cveSub, cvePze,false);
                    else
                        doRecalculo();

                }else if(updateData) {

                    updateData = false;
                    controller.updateData(false);

                    //si actualizó montos máximos, pintarlos
                    setCleanedValues();

                    doRecalculo();

                }else{
                    controller.updateData(false);
//                    Log.i("contrato","contrato simulador, producto consumo");
                    contratarConsumoSimulador();
                }
            }
        }
    }

    private void setCleanedValues(){
        controller.setProducto(getProducto());
        controller.initView(controller.getView());
    }

    public void consultaDetalleConsumoOneClick(){
        String user = session.getNumCelular();
        String ium  = session.getIum();
        String cveCamp = getPromociones().getCveCamp();

        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put("numeroCelular",user );
        params.put("claveCamp",cveCamp);
        params.put("IUM", ium);
        params.put("importePar", "000");

        Hashtable<String, String> paramTable2  = AuxConectionFactory.consultaDetalleConsumo(params);
        this.doNetworkOperation(Server.CONSULTA_DETALLE_CONSUMO, paramTable2, true, new bbvacredit.bancomer.apicredit.models.OfertaConsumo(),
                MainController.getInstance().getContext());

    }

    //lanza peticion detalle consumo
    public void iniciaflucjoOneClickConsumo(){

        MainController mainCntr = MainController.getInstance();
        InitConsumo init = InitConsumo.getInstance(mainCntr.getActivityController().getCurrentActivity(),
                controller.getPromocionConsumo(), mainCntr.getDelegateOTP(), mainCntr.getSession(),
                mainCntr.getAutenticacion(), mainCntr.isSofTokenStatus(), mainCntr.getClient()
                , mainCntr.getHostInstance(), false, Server.DEVELOPMENT,
                Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
        mainCntr.getActivityController().getCurrentActivity().setActivityChanging(true);
        init.ejecutaAPIConsumo();

    }

    public void contratarConsumoSimulador(){
        Session.getInstance().setCveCamp(producto.getCveProd());
        Session session = Session.getInstance(MainController.getInstance().getContext());

        DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(session.getIdUsuario(),
                session.getIum(), session.getNumCelular(), producto.getCveProd(),getProducto());
        delegate.consultaDetalleAlternativasTask(getProducto());
    }

    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(Server.CALCULO_ALTERNATIVAS_OPERACION, ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        if (isSavingOperation) {
            addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = false;
        } else {
            addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = true;
        }

        Hashtable<String, String> paramTable2 = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
    }

    public void updateData(String cveSub, String cvePzoE, boolean isUpdate){

        updateData = true;
//        isSavingOperation = false;

        this.cveSub = cveSub;
        this.cvePze = cvePzoE;

        if(isUpdate){
            saveOrDeleteSimulationRequest(false);
        }else {
            Hashtable<String, String> paramTable = new Hashtable<String, String>();

            addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
            addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
            addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
            addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

            addParametroObligatorio(ConstantsCredit.OP_AJUSTE_PLAZOS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);

            Producto ccr = getProducto();

            addParametroObligatorio(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM, paramTable);
            addParametroObligatorio(cvePzoE, ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
            addParametroObligatorio("", ServerConstantsCredit.MON_DESE_PARAM, paramTable);
            addParametroObligatorio(cveSub, ServerConstantsCredit.CVESUBP_PARAM, paramTable);
            Hashtable<String, String> paramTable2 = AuxConectionFactoryCredit.calculo(paramTable);
            this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
        }
    }

    public void doRecalculo(){

        String cvePlazoSel = "";
        String montoSel = "";

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
        Producto ccr = controller.getProducto();

//        Log.i("detalle","cveProd doRecalc: "+ccr.getCveProd());
        montoSel = controller.edtMontoVisible.getText().toString();
        montoSel = montoSel.replace("$", "");
        montoSel = montoSel.replace(",", "");
        cvePlazoSel = controller.buscarClave(ccr.getDessubpE(),controller.getCvePlazo(),controller.buscarPlazo);

//        Log.i("detalle","cvePlazoSel: "+cvePlazoSel);

        addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM, paramTable);
        addParametro(ccr.getSubproducto().get(0).getCveSubp(), ServerConstantsCredit.CVESUBP_PARAM, paramTable);
        addParametro(cvePlazoSel, ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM, paramTable);

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }


    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }


    public Promociones getPromociones() {
        return promociones;
    }

    public void setPromociones(Promociones promociones) {
        this.promociones = promociones;
    }

    @Override
    protected String getCodigoOperacion() {
        return Server.CALCULO_OPERACION;
    }
}
