package bbvacredit.bancomer.apicredit.gui.delegates;

import android.util.Log;

import com.bancomer.apiilc.gui.controllers.DetalleILCViewController;
import com.bancomer.apiilc.gui.delegates.DetalleOfertaILCDelegate;
import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.apiilc.models.OfertaILC;

import java.util.Hashtable;
import java.util.LinkedHashMap;

import bancomer.api.common.gui.controllers.BaseViewsController;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.AdelantoNominaSeekBarViewController;
import bbvacredit.bancomer.apicredit.gui.activities.ILCSeekBarViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by FHERNANDEZ on 28/12/16.
 */
public class ILCSeekBarDelegate extends BaseDelegateOperacion {

    private ILCSeekBarViewController controller;
    private Producto producto;
    private ObjetoCreditos data;
    private ObjetoCreditos auxData;
    private Session session;
    private boolean isDeleteOperation = false; //si es true, borrar simulación
    private boolean productoConPlazo;

    private String indSim;

    private LinkedHashMap<Long, BaseDelegate> delegatesHashMap = new LinkedHashMap<Long, BaseDelegate>();;

    private Promociones promocion;

    public ILCSeekBarDelegate(ILCSeekBarViewController controller){
        this.controller = controller;
        session = Tools.getCurrentSession();
    }

    public void analyzeResponse(String operationId, ServerResponse response) {
        //OneClick
        if (Server.CONSULTA_DETALLE_ILC.equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                bbvacredit.bancomer.apicredit.models.OfertaILC ofILC = (bbvacredit.bancomer.apicredit.models.OfertaILC)response.getResponse();
//                Log.i("ilc","exito consulta ilc por oneclick");
//                Log.i("ilc","respuesta consulta ilc oneclick: "+response.getResponse());
//                Log.i("ilc","respuesta consulta ilc getCat: "+ofILC.getCat());
//                Log.i("ilc","datos ilc. importe: "+ofILC.getImporte());
//                Log.i("ilc","datos ilc. linea actual: "+ofILC.getLineaActual());
//                Log.i("ilc", "datos ilc linea final: " + ofILC.getLineaFinal());
                controller.setOfertaILCC(ofILC);
                controller.validateLayoutToShow();
            }

        }else if(getCodigoOperacion().equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                MainController.getInstance().ocultaIndicadorActividad();
                session.setAuxCreditos(data);
//                session.setCreditos(data);
                setProducto(Tools.getProductByCve("0ILC", data.getProductos()));

                indSim = getProducto().getIndSim();
                controller.setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);

                //actualización de los objetos
                controller.updateData(false);

//                saveOrDeleteSimulationRequest(true);
                oneClickILC();
            }
        }else if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

                CalculoData respuesta = (CalculoData) response.getResponse();

                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                MainController.getInstance().ocultaIndicadorActividad();
                session.setAuxCreditos(data);  //se guarda por separado
//                session.setCreditos(data);
                setProducto(Tools.getProductByCve("0ILC", data.getProductos()));
                indSim = getProducto().getIndSim();
                controller.setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);

                //actualización de los objetos

                if(isDeleteOperation){
                    controller.updateData(true);
                    isDeleteOperation = false;
                    controller.isSimulated = false;
                    doRecalculo();

                }else{
                    controller.updateData(false);
                    Log.i("ilc", "showDetalleILC, getProd: " + getProducto().getCveProd());
                    controller.contratarILC();
                }
            }
        }
    }

    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(Server.CALCULO_ALTERNATIVAS_OPERACION, ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        if (isSavingOperation) {
            addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = false;
        } else {
            addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = true;
        }

        Hashtable<String, String> paramTable2 = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    public void doRecalculo(){

        String plazoSel= "";
        String cvePlazoSel = "";
        String montoSel = "";

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
        Producto ccr = getProducto();

        Log.i("detalle","cveProd doRecalc: "+ccr.getCveProd());
        montoSel = controller.edtMontoVisible.getText().toString();
        montoSel = montoSel.replace("$","");
        montoSel = montoSel.replace(",","");

        addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM,paramTable);
        addParametro(ccr.getSubproducto().get(0).getCveSubp(), ServerConstantsCredit.CVESUBP_PARAM,paramTable);
        addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM,paramTable);
        addParametro("", ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    public void oneClickILC() {
        Session.getInstance().setCveCamp(producto.getCveProd());
        Session session = Session.getInstance(MainController.getInstance().getContext());

        DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(session.getIdUsuario(), session.getIum(), session.getNumCelular(), producto.getCveProd(),getProducto());
        delegate.setIlcViewController(controller);
        delegate.consultaDetalleAlternativasTask(getProducto());
        controller.setDelegateOp(delegate);
    }

    public void doOneClickILC(){
        Session session = Tools.getCurrentSession();
        String user = session.getNumCelular();
        String ium= session.getIum();
        Hashtable<String, String> params = new Hashtable<String, String>();
        //  params.put("operacion", "cDetalleOfertaBMovil");
        params.put("numeroCelular", user);
        params.put("cveCamp", promocion.getCveCamp());
        params.put("IUM", ium);

        Hashtable<String, String> paramTable2 = AuxConectionFactoryCredit.realizaOPEfi(params);
        doNetworkOperation(Server.CONSULTA_DETALLE_ILC, paramTable2, true, new bbvacredit.bancomer.apicredit.models.OfertaILC(), MainController.getInstance().getContext());
    }

    public BaseDelegate getBaseDelegateForKey(long key) {
        BaseDelegate baseDelegate = delegatesHashMap.get(Long.valueOf(key));
        return baseDelegate;
    }

    public void initAPIILC(OfertaILC ofertaILC){

        MainController mainCntr = MainController.getInstance();
//Promociones promocion,
        InitILC init = InitILC.getInstance(mainCntr.getActivityController().getCurrentActivity(), controller.getPromocionILC(),
                mainCntr.getDelegateOTP(), mainCntr.getSession(), mainCntr.getAutenticacion(),
                mainCntr.isSofTokenStatus(), mainCntr.getClient(), mainCntr.getHostInstance(),
                mainCntr.getListaCuentas(), Server.DEVELOPMENT,
                Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
        init.ejecutaAPIILC();
        mainCntr.getActivityController().getCurrentActivity().setActivityChanging(true);

    }

    public Promociones getPromocion() {
        return promocion;
    }

    public void setPromocion(Promociones promocion) {
        this.promocion = promocion;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    protected String getCodigoOperacion() {
        return Server.CALCULO_OPERACION;
    }
}
