package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.ICommonSession;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.HipotecarioDelegate;
import tracking.TrackingHelperCredit;

/**
 * Created by OOROZCO on 12/9/15.
 */
public class DetalleHipotecarioViewController extends Fragment {

    // ------------- Attributes Region ----------- //

    private ImageButton btnRecalcularSimulacion;
    private ImageButton btnSolicitar;
    private TextView lblResultadoPago;
    private TextView lblResultadoPlazo;
    private TextView lblResultadoTasa;
    private TextView lblCat;
    private HipotecarioDelegate delegate;
    private Button btnVerResumenPagos;
    private HipotecarioViewController hipo;
    private Session session;
    private Integer getProdIndex;

    public HipotecarioViewController getHipo() {
        return hipo;
    }

    public void setHipo(HipotecarioViewController hipo) {
        this.hipo = hipo;
    }

    public void setDelegate(HipotecarioDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_credit_resultado_simulacion, container, false);
        delegate.setDetalleHipo(this);


        if (rootView != null)
            setUI(rootView);

        ((MenuPrincipalActivity) getActivity()).setIsSimulate(true);
        ((MenuPrincipalActivity) getActivity()).setoFragment(this);

        return  rootView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        session = Tools.getCurrentSession();
        getProdIndex = session.getProductoIndexByCve(ConstantsCredit.CREDITO_HIPOTECARIO);

        Log.i("opinator","guardando cve: "+delegate.getData().getProductos().get(getProdIndex).getCveProd());

        String tasa= getActivity().getString(R.string.lblTasaNomina);
        String cat = getActivity().getString(R.string.lblautocat)+ " " +delegate.getData().getProductos().get(getProdIndex).getCATS() + " " + getActivity().getString(R.string.lblautocomp)+ " "
                +delegate.getData().getProductos().get(getProdIndex).getFechaCatS() + ". " + getActivity().getString(R.string.lblautocomp2);

        lblResultadoPago.setText(GuiTools.getMoneyString(delegate.getData().getProductos().get(getProdIndex).getPagMProdS()));
        lblResultadoPlazo.setText(delegate.getData().getProductos().get(getProdIndex).getDesPlazoE());
        lblResultadoTasa.setText(tasa + " " + delegate.getData().getProductos().get(getProdIndex).getTasaS() + "%");
        lblCat.setText(cat);

        Log.i("email", "montoDeseado: " + delegate.getData().getProductos().get(getProdIndex).getMontoDeseS());
        Log.i("email", "desPlazoE: " + delegate.getProducto().getDesPlazoE());
    }

    private void setUI(View rootView){

        lblCat = (TextView)  rootView.findViewById(R.id.lblCat);
        lblResultadoPago = (TextView)  rootView.findViewById(R.id.lblResultadoPago);
        lblResultadoPlazo = (TextView) rootView.findViewById(R.id.lblResultadoPlazo);
        lblResultadoTasa = (TextView) rootView.findViewById(R.id.lblResultadoTasa);

        btnRecalcularSimulacion = (ImageButton) rootView.findViewById(R.id.btnRecalcularSimulacion);
        btnRecalcularSimulacion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                delegate.saveOrDeleteSimulationRequest(false); //se manda false para eliminar operación
            }
        });

        btnSolicitar = (ImageButton) rootView.findViewById(R.id.btnContratarANOM);
//        btnSolicitar.setImageDrawable(getResources().getDrawable(R.drawable.btn_solicitar));
        btnSolicitar.setBackground(getResources().getDrawable(R.drawable.btn_solicitar));
        btnSolicitar.setVisibility(View.GONE);
        btnSolicitar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                solicitarAction();

                SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
                SharedPreferences.Editor editor = sp.edit();
                editor.putBoolean(ConstantsCredit.SHARED_IS_RESUMEN, false);
                editor.commit();

            }
        });


        btnVerResumenPagos = (Button) rootView.findViewById(R.id.btnVerResumenPagos);
        btnVerResumenPagos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                verResumenPagos();
            }
        });

    }

    public void verResumenPagos(){
        MainController.getInstance().showScreen(ResumenViewController.class);
        MainController.getInstance().setFlowFragment(DetalleHipotecarioViewController.this);
    }


    public void solicitarAction() {
        String params = "";
        ICommonSession sesion = MainController.getInstance().getSession();
        Session oSession = Tools.getCurrentSession();
        Log.i("session","nombreCliente: "+sesion.getNombreCliente());
        params = "nombredepila=" + sesion.getNombreCliente().split(" ")[0] + "&segmento=HIP&credito=" + delegate.getController().getMonto() + "&idpagina=simulacion";
        Map<String,Object> click_paso2_operacion = new HashMap<String,Object>();
        click_paso2_operacion.put("inicio","event45");
        click_paso2_operacion.put("&&products", "simulador;simulador:simulador credito hipotecario");
        click_paso2_operacion.put("eVar12", "seleccion contactame");
        TrackingHelperCredit.trackPaso1Operacion(click_paso2_operacion);
        WebViewController.setParams(params);
        WebViewController.setIsAuto(false);
        MainController.getInstance().showScreen(WebViewController.class);

    }

    public void simpleUpdateMenu(String cveProd){

        if(((MenuPrincipalActivity)getActivity()) == null)
            Log.i("eliminar", "Menu es nulo");

        Log.i("eliminar", "actualizaMenu HIPO, cveProd: " + cveProd);
        ((MenuPrincipalActivity)getActivity()).updateMenu(cveProd);
        getFragmentManager().beginTransaction().remove(((MenuPrincipalActivity) getActivity()).getActiveFragment()).commit();
        ((MenuPrincipalActivity)getActivity()).showHipotecario();
    }

}
