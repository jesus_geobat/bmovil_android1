package bbvacredit.bancomer.apicredit.gui.activities;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.LabelMontoTotalCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.gui.delegates.AdelantoNominaSeekBarDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.PrestamoPersonalSeekBarDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.PrestamosContratadosDelegate;
import bbvacredit.bancomer.apicredit.models.Producto;

/**
 * Created by FHERNANDEZ on 30/12/16.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CreditoNominaSeekBarViewController extends Fragment implements View.OnClickListener{

    private Producto producto;

    private ImageView imgvProd;
    private ImageView imgvShowDetalle;
    private ImageView imgvAyuda;

    private ImageButton imgvContratarCredito;

    private LabelMontoTotalCredit edtMontoVisible;

    private TextView txtvDescProd;
    private TextView txtvMontoMinimo;
    private TextView txtvMontoMaximo;
    private TextView txtvCat;
    private TextView txtvTasa;
    private TextView txtvTextSaldoTotal;
    private TextView txtvSaldoTotalPagar;
    private TextView txtvOpcion;
    private TextView txtvPlazo;
    private TextView txtvTipo;
    private TextView txtvMasInfo;
    private TextView txtvPagoMMax;
    private TextView txtvMontoMMax;

    private LinearLayout lnlDetalleProd;
    private LinearLayout lnlTipo;
    private LinearLayout lnlPlazo;
    private LinearLayout lnlPlazoTipo;
    private LinearLayout lnlPagoMMaximo;
    private LinearLayout lnlPagoMMaximo2;

    private SeekBar seekBarSimulador;

    private String montoMax = "";
    private String montMin = "";
    private StringBuffer typedString= new StringBuffer();
    private String amountString=null;
    private boolean indSimProd;
    private String auxFinalMax;

    private boolean isSettingText = false;
    private boolean isResetting = false;
    private boolean mAcceptCents=false;
    private boolean flag=true;
    public boolean isShowDetalle;
    public boolean isSimulated = false;
    public boolean isFromBar;


    private PrestamoPersonalSeekBarDelegate delegate;
    private CreditoNominaSeekBarViewController controller;

    private final int INCREMENTAL = 1000;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_credit_item_listview_creditos,container,false);

        if(rootView != null){
            controller = this;
            initView(rootView);
//            delegate = new PPS(this);
//            delegate = new PrestamoPersonalSeekBarDelegate(this);
            delegate.setProducto(getProducto());
            setIsShowDetalle(false);
        }

        return rootView;
    }

    private void initView(View rootView){
        findViews(rootView);
        validateSO();
        setValuesProduct();
        setValuesSeekBar();
        setVisibleMont(rootView);

    }

    private void setVisibleMont(final View view){

        edtMontoVisible.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                reset();
                mAcceptCents = true;
                isSettingText = true;
                edtMontoVisible.setText("");
                return false;
            }
        });

        edtMontoVisible.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                String amountField = edtMontoVisible.getText().toString();
                if (!isSettingText) {
                    amountString = amountField;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String amountField = edtMontoVisible.getText().toString();

                if (s.toString().equals(".") || s.toString().equals("0")) {
                    edtMontoVisible.setText("");
                } else {

                    if (!isSettingText && !isResetting) {
                        if (!flag) {
                            String aux;
                            try {
                                aux = edtMontoVisible.getText().toString();//.substring(0, edtMontoVisible.length() - 3);
                                typedString = new StringBuffer();

                                if (aux.charAt(0) == '0')
                                    aux = aux.substring(1, aux.length());
                            } catch (Exception ex) {
                                aux = "";

                            }
                            for (int i = 0; i < aux.length(); i++) {
                                if (aux.charAt(i) != ',')
                                    typedString.append(aux.charAt(i));
                            }
                            flag = true;

                            setFormattedText();
                            amountField = edtMontoVisible.getText().toString();
                            amountString = amountField;
                        } else {
                            try {
                                if (edtMontoVisible.length() < amountString.length()) {
                                    reset();
                                } else if (edtMontoVisible.length() > amountString.length()) {

                                    int newCharIndex = edtMontoVisible.getSelectionEnd() - 1;
                                    //there was no selection in the field
                                    if (newCharIndex == -2) {
                                        newCharIndex = edtMontoVisible.length() - 1;
                                    }

                                    char num = amountField.charAt(newCharIndex);
                                    if (!(num == '0' && typedString.length() == 0)) {
                                        typedString.append(num);
                                    }
                                    setFormattedText();
                                }
                            } catch (StringIndexOutOfBoundsException ex) {
                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                isSettingText = false;
            }
        });

        edtMontoVisible.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager manager = (InputMethodManager) ((MenuPrincipalActivitySeekBar) getActivity()).getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (manager != null) {
                        manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                        String auxMon = edtMontoVisible.getText().toString();
                        String respuesta = "";

                        if (edtMontoVisible.getText().toString().equals("")) {
                            edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                            updateGraph(montMin);
                        } else {
                            respuesta = Tools.validateMont(edtMontoVisible.getMontoMax(),
                                    edtMontoVisible.getMontoMin(), auxMon, null);
                            try {
                                if (respuesta.equals(ConstantsCredit.MONTO_MAYOR)) {
                                    edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                                    updateGraph(montoMax);
                                } else if (respuesta.equals(ConstantsCredit.MONTO_MENOR)) {
                                    edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montMin)));
                                    updateGraph(montMin);
                                } else if (respuesta.equals(ConstantsCredit.MONTO_EN_RANGO)) {
                                    String auxLbl = edtMontoVisible.getText().toString();
                                    if (!auxLbl.startsWith("$"))
                                        edtMontoVisible.setText("$" + auxLbl);
                                    updateGraph(auxLbl);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                    edtMontoVisible.clearFocus();
                }


                return false;
            }
        });

        edtMontoVisible.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    String amount = edtMontoVisible.getText().toString();

                    if (!amount.equals("") || amount != null) {
                        if (amount.contains(","))
                            amount = amount.replace(",", "");

                        if (amount.contains("$"))
                            amount = amount.replace("$", "");

                        amount = amount.substring(0, amount.length() - 1);

                        edtMontoVisible.setText(Tools.formatUserAmount(amount));

                    }
                }

                return false;
            }
        });
    }

    public void updateGraph(String amount){
        double newAmount = 0.0, finalmmax = 0.0;
        int finalP = 0, auxMonMin = 0, auxAmount2 = 0;
        String auxAmount = Tools.clearAmounr(amount);

        if(auxAmount.equals(getMontMin())){
            seekBarSimulador.setProgress(0);

        }else if(auxAmount.equals(getMontoMax())){
            finalmmax = Double.parseDouble(getAuxFinalMax());
            seekBarSimulador.setProgress((int) finalmmax);

        }else {

            auxMonMin = Integer.parseInt(getMontMin());
            auxAmount2 = Integer.parseInt(auxAmount);

            finalP = auxAmount2-auxMonMin;
            newAmount = finalP / INCREMENTAL;

            seekBarSimulador.setProgress((int)newAmount);
        }
    }

    private void setValuesSeekBar(){

        final double mmax = Double.parseDouble(Tools.validateMont(getMontoMax(), false));
        final double mmin = Double.parseDouble(Tools.validateMont(getMontMin(), false));
        double auxMmax = mmax-mmin;

        auxMmax = auxMmax/INCREMENTAL;

        final double finalAuxMmax = auxMmax;

        setAuxFinalMax(String.valueOf(finalAuxMmax));

        seekBarSimulador.setMax((int) finalAuxMmax);
        seekBarSimulador.setProgress((int) finalAuxMmax);

        seekBarSimulador.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String visibleMont;

                final int progressChanged = progress;
                final int progresFinal = calculatePogress(progressChanged, (int) mmin, (int) finalAuxMmax, (int) mmax);

                visibleMont = Tools.formatUserAmount(Integer.toString(progresFinal));
                edtMontoVisible.setText(visibleMont);
                edtMontoVisible.setCursorVisible(false);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                isSimulated = true;
                isFromBar = true;

                ((MenuPrincipalActivitySeekBar)getActivity()).isOnomS = true;
                ((MenuPrincipalActivitySeekBar)getActivity()).validateTxtvColor(controller);
                setColor(false);

                validateProducSimulated();
            }
        });
    }

    private void validateProducSimulated(){
        if (isIndSimProd()){ //si ya fue simulado, elimina y recalcula
            delegate.saveOrDeleteSimulationRequest(false); //true guarda, false elimina
        }else{
            delegate.doRecalculo();
        }

    }

    private void setValuesProduct(){
        String montoMaximo, auxMMax, montoMinimo, auxMMin,
                montoFlow, cveProd, indSim;

        int imgProducto, descProd;

        cveProd         = getProducto().getCveProd();
        montoMaximo     = getProducto().getMontoMaxS();
        montoMinimo     = getProducto().getMontoMinS();
        indSim          = getProducto().getIndSim();
        imgProducto     = GuiTools.getCreditIcon(cveProd, false);
        descProd        = GuiTools.getSelectedLabel(cveProd);
        montoFlow       = getProducto().getMontoMaxS();   //valor que se tomará como monto máximo para saber si es mayor a 0.

        imgvProd.setImageResource(imgProducto);
        txtvDescProd.setText(descProd);

        auxMMax = Tools.formatUserAmount(Tools.validateMont(montoMaximo,true));
        auxMMin = Tools.formatUserAmount(Tools.validateMont(montoMinimo, true));

        if(auxMMax.contains("$"))
            auxMMax = auxMMax.replace("$","");
        if(auxMMin.contains("$"))
            auxMMin = auxMMin.replace("$","");

        setMontoMax(Tools.clearAmounr(auxMMax));
        setMontMin(Tools.clearAmounr(auxMMin));

        setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);

        txtvMontoMaximo.setText(auxMMax);
        txtvMontoMinimo.setText(auxMMin);


        edtMontoVisible.setMontoMax(getMontoMax());
        edtMontoVisible.setMontoMin(getMontMin());
        edtMontoVisible.setCveProd(producto.getCveProd());

        edtMontoVisible.setText(Tools.formatUserAmount(getMontoMax()));
    }

    private void validateSO(){
        Log.i("version","version android device: "+Build.VERSION.SDK_INT);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH){
            Log.i("version","version android es menor");
            seekBarSimulador.setThumb(getResources().getDrawable(R.drawable.seek_bar_thumb));
            seekBarSimulador.setProgressDrawable(getResources().getDrawable(R.drawable.custom_seek_bar));
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setThumbOffset(0);
        }
    }

    private void findViews(View vista){
        imgvProd        = (ImageView)vista.findViewById(R.id.imgvProd);
        imgvShowDetalle = (ImageView)vista.findViewById(R.id.imgvShowDetalle);
        imgvShowDetalle.setOnClickListener(this);

        imgvAyuda       = (ImageView)vista.findViewById(R.id.imgvAyuda);
        imgvAyuda.setOnClickListener(this);

        imgvContratarCredito = (ImageButton)vista.findViewById(R.id.imgvContratarCredito);

        edtMontoVisible    = (LabelMontoTotalCredit)vista.findViewById(R.id.edtMontoVisible);

        txtvDescProd        = (TextView)vista.findViewById(R.id.txtvDescProd);
        txtvMontoMinimo     = (TextView)vista.findViewById(R.id.txtvMontoMinimo);
        txtvMontoMaximo     = (TextView)vista.findViewById(R.id.txtvMontoMaximo);
        txtvCat             = (TextView)vista.findViewById(R.id.txtvCat);
        txtvOpcion          = (TextView)vista.findViewById(R.id.txtvTipo);
        txtvPlazo           = (TextView)vista.findViewById(R.id.txtPlazo);
        txtvTasa            = (TextView)vista.findViewById(R.id.txtvTasa);
        txtvTipo            = (TextView)vista.findViewById(R.id.txtvTipo);
        txtvMasInfo         = (TextView)vista.findViewById(R.id.txtvMasInfo);
        txtvPagoMMax        = (TextView)vista.findViewById(R.id.txtvPagoMMax);
        txtvMontoMMax       = (TextView)vista.findViewById(R.id.txtvMontoMMax);

        lnlDetalleProd      = (LinearLayout)vista.findViewById(R.id.lnlDetalleProd);
        lnlTipo             = (LinearLayout)vista.findViewById(R.id.lnlTipo);
        lnlTipo.setOnClickListener(this);

        lnlPlazo            = (LinearLayout)vista.findViewById(R.id.lnlPlazo);
        lnlPlazo.setOnClickListener(this);

        lnlPlazoTipo        = (LinearLayout)vista.findViewById(R.id.lnlPlazoTipo);
        lnlPagoMMaximo      = (LinearLayout)vista.findViewById(R.id.lnlPagoMMaximo);
        lnlPagoMMaximo2     = (LinearLayout)vista.findViewById(R.id.lnlPagoMMaximo2);

        seekBarSimulador    = (SeekBar)vista.findViewById(R.id.seekBarSimulador);

    }

    @Override
    public void onClick(View v) {

        if (v == lnlPlazo){

        }

        if (v == lnlTipo){

        }

        if (v == imgvAyuda) {
            PopUpAyudaViewController.mostrarPopUp((MenuPrincipalActivitySeekBar)getActivity(),getProducto().getCveProd());
        }

        if(v == imgvShowDetalle){

            ((MenuPrincipalActivitySeekBar)getActivity()).isOnomS = true;
            ((MenuPrincipalActivitySeekBar)getActivity()).validateTxtvColor(this);

            setColor(false);

            if(isSimulated){ //si ya fue simulado, muestra detalle
                Log.i("simular","producto simulado");
                validateLayoutToShow();
            }else{
                Log.w("simular","producto no simulado");
                isFromBar = false;
                isSimulated = true;
                validateProducSimulated();
            }

        }

    }

    public void setColor(boolean isNormal){
        if(isNormal)
            edtMontoVisible.setTextColor(getResources().getColor(R.color.nuevaimagen_tx5));
        else
            edtMontoVisible.setTextColor(getResources().getColor(R.color.naranja));
    }

    public void validateLayoutToShow(){

        //oculta detalle
        if(isShowDetalle()) {
            setIsShowDetalle(false);
            lnlDetalleProd.setVisibility(View.GONE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_masdetalle);

        }else{

            ((MenuPrincipalActivitySeekBar)getActivity()).isOnomDetail = true;
            ((MenuPrincipalActivitySeekBar)getActivity()).validateIsShowAnyDetail(this);

            setIsShowDetalle(true);
            lnlDetalleProd.setVisibility(View.VISIBLE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_menosdetalle);
            lnlPlazoTipo.setVisibility(View.VISIBLE);
            lnlPlazo.setVisibility(View.VISIBLE);
            lnlPagoMMaximo2.setVisibility(View.VISIBLE);
            txtvTipo.setVisibility(View.INVISIBLE);
            imgvContratarCredito.setVisibility(View.VISIBLE);

            setDetailProduct();
        }

    }

    private void setDetailProduct(){
        try {
            //        ArrayList<Producto>detail = Session.getInstance().getAuxCreditos().getProductos();
            Producto newProducto = Tools.getProductByCve(ConstantsCredit.CREDITO_NOMINA, Session.getInstance().getCreditos().getProductos());

            String clveMesQuincena, cat, tasa, totalMesQuincena;

            clveMesQuincena = newProducto.getSubproducto().get(0).getCveSubp();
            tasa = newProducto.getTasaS();
            cat = newProducto.getCATS();
            totalMesQuincena = newProducto.getPagMProdS();


            txtvPagoMMax.setText(clveMesQuincena.equalsIgnoreCase(ConstantsCredit.CODIGO_MENSUAL) ?
                    getResources().getString(R.string.detalle_adelanto_nomina_pago_mensual) :
                    getResources().getString(R.string.detalle_adelanto_nomina_pago_quincenal)+ " ");

            txtvTasa.setText("Tasa de interés: "+tasa+" %");
            txtvCat.setText("CAT: "+cat+"% sin iva");
            txtvMontoMMax.setText(GuiTools.getMoneyString(totalMesQuincena));

        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    private int calculatePogress(int progress, int min, int progresFinal, int mmax){

        int auxMin = min;

        if (progress == 0){
            return min;
        }else {

            for (int i = 1; i<=progress; i++) {
                auxMin = auxMin + INCREMENTAL;
                if (progress == progresFinal){
                    auxMin = mmax;
                }
            }
        }

        return auxMin;
    }

    public void reset() {
        this.isResetting = true;
        this.typedString=new StringBuffer();
        this.isResetting = false;
    }

    private void setFormattedText(){

        isSettingText = true;
        String text = typedString.toString();

        edtMontoVisible.setText(Tools.formatUserAmount(text));
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public String getMontoMax() {
        return montoMax;
    }

    public void setMontoMax(String montoMax) {
        this.montoMax = montoMax;
    }

    public String getMontMin() {
        return montMin;
    }

    public void setMontMin(String montMin) {
        this.montMin = montMin;
    }

    public boolean isIndSimProd() {
        return indSimProd;
    }

    public void setIndSimProd(boolean indSimProd) {
        this.indSimProd = indSimProd;
    }

    public boolean isShowDetalle() {
        return isShowDetalle;
    }

    public void setIsShowDetalle(boolean isShowDetalle) {
        this.isShowDetalle = isShowDetalle;
    }

    public String getAuxFinalMax() {
        return auxFinalMax;
    }

    public void setAuxFinalMax(String auxFinalMax) {
        this.auxFinalMax = auxFinalMax;
    }

}