package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import bancomer.api.common.timer.TimerController;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.ListaDatosResumenController;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.ResumenDelegate;
import bbvacredit.bancomer.apicredit.models.CreditoContratado;
import bbvacredit.bancomer.apicredit.models.Producto;
import tracking.TrackingHelperCredit;

import bbvacredit.bancomer.apicredit.R;

public class ResumenActivity extends BaseActivity implements OnClickListener {

	private ResumenDelegate delegate;
	private Button enviarMailBtn;
	private Button regresarBtn;
	private TextView condicionesCreditosBtn1;
	private TextView condicionesCreditosBtn2;
	// scroll
	private ScrollView parentScroll;
	private Session session;
	
	private double totalPago;
	private boolean simulaContratados;
	private boolean simulaOfertados;
	private MainController parentManager = MainController.getInstance();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState,SHOW_HEADER|SHOW_TITLE|DONTSHOW_HEADERBUTTON, R.layout.activity_resumen);
		isOnForeground=true;

		TrackingHelperCredit.trackState("creditresumenSim", parentManager.estados);

		MainController.getInstance().setCurrentActivity(this);
		
		init();
	}


	private void init() {
		// Tomamos el que hemos guardado en el login si existe
		session = Tools.getCurrentSession();
		delegate = new ResumenDelegate();
		delegate.llenarTablas(this);
		scaleToCurrentScreen();
		mapearBotones();

		TextView textView = (TextView)findViewById(R.id.lblDespliegaListaCredSim);
		textView.setText(GuiTools.underlineText(textView.getText().toString()));
		
		TextView textView2 = (TextView)findViewById(R.id.lblDespliegaListaCredCont);
		textView2.setText(GuiTools.underlineText(textView2.getText().toString()));
		
		
	}

	private void mapearBotones() {
		// TODO Auto-generated method stub
		enviarMailBtn = (Button)findViewById(R.id.btnEnvioEmail);
		enviarMailBtn.setOnClickListener(this);
		
		regresarBtn = (Button)findViewById(R.id.btnRegresarResumen);
		regresarBtn.setOnClickListener(this);
		
		condicionesCreditosBtn1 = (TextView)findViewById(R.id.lblDespliegaListaCredSim);
		condicionesCreditosBtn1.setOnClickListener(this);
		
		condicionesCreditosBtn2 = (TextView)findViewById(R.id.lblDespliegaListaCredCont);
		condicionesCreditosBtn2.setOnClickListener(this);
		
	}

	private void scaleToCurrentScreen() {
		// TODO Auto-generated method stub
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.pResumenScrollLayout));
		guiTools.scale(findViewById(R.id.pResumenMainLayout));
		guiTools.scale(findViewById(R.id.pResumenRelativeScroll));
		guiTools.scale(findViewById(R.id.pResumenLinearScroll));
		guiTools.scale(findViewById(R.id.lblResumen),true);
		guiTools.scale(findViewById(R.id.imgResumen));
		guiTools.scale(findViewById(R.id.imgResumen2));
		guiTools.scale(findViewById(R.id.lblTituloCredSimulados),true);
		guiTools.scale(findViewById(R.id.lblDespliegaListaCredSim),true);
		guiTools.scale(findViewById(R.id.lblTituloCredContratados),true);
		guiTools.scale(findViewById(R.id.lblDespliegaListaCredCont),true);
										
		guiTools.scale(findViewById(R.id.imgFondo));

		guiTools.scale(findViewById(R.id.imgBarraGris1));
		guiTools.scale(findViewById(R.id.imgBarraGris2));
		guiTools.scale(findViewById(R.id.imgBarraGrisClaro1));
		guiTools.scale(findViewById(R.id.imgBarraGrisClaro4));
		guiTools.scale(findViewById(R.id.lblProducto),true);
		guiTools.scale(findViewById(R.id.lblProducto2),true);
		guiTools.scale(findViewById(R.id.lblPagoMensual),true);
		guiTools.scale(findViewById(R.id.lblPagoMensual2),true);
		guiTools.scale(findViewById(R.id.btnEnvioEmail));
		guiTools.scale(findViewById(R.id.btnRegresarResumen));
		
		
		guiTools.scale(findViewById(R.id.lblTituloCredOfer),true);
		guiTools.scale(findViewById(R.id.imgBarraGrisClaro7));
		guiTools.scale(findViewById(R.id.lblProducto3),true);
		guiTools.scale(findViewById(R.id.lblPagoMensual3),true);
		guiTools.scale(findViewById(R.id.imgBarraGrisClaro8));	
		
		guiTools.scale(findViewById(R.id.resumenCreditosContratados));
		guiTools.scale(findViewById(R.id.resumenCreditosOfertados));
		guiTools.scale(findViewById(R.id.resumenCreditosSimulados));
		guiTools.scale(findViewById(R.id.resumenSubTitle),true);
		
		guiTools.scale(findViewById(R.id.lblPagoMensualTitle),true);
		guiTools.scale(findViewById(R.id.lblPagoMensualTotal),true);
		guiTools.scale(findViewById(R.id.lblPagoMensualAdvertencia),true);
		
		
	}
	
	public void pintarTotalMensual(){
		TextView txtTotal = (TextView)findViewById(R.id.lblPagoMensualTotal);
		
		txtTotal.setText(GuiTools.getMoneyString(String.valueOf(totalPago)));
		
		//TextView txtAdvertencia = (TextView)findViewById(R.id.lblPagoMensualAdvertencia);
		//txtAdvertencia.setText(GuiTools.underlineText(txtAdvertencia.getText().toString()));
	}
	
	public void pintarTablaProductos(){

		Integer cont = 0;
		
		ArrayList<Producto> lista = delegate.getProductos();
		Iterator<Producto> it = lista.iterator();
	    
		ListaDatosResumenController listaController;
		while(it.hasNext()){
			Producto p = it.next();
	
			listaController = new ListaDatosResumenController((LinearLayout)findViewById(R.id.resumenCreditosOfertados),  this, R.id.resumenCreditosOfertados,this,p);
			listaController.addElement(p.getDesProd(), GuiTools.getMoneyString(p.getSubproducto().get(0).getMonMax().toString()), false);
			
			++cont;
		}
	}
	
	private void ocultaCreditosContratados(){
		((ImageView)findViewById(R.id.imgResumen2)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.lblTituloCredContratados)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.lblDespliegaListaCredCont)).setVisibility(View.GONE);

		((ImageView)findViewById(R.id.imgBarraGris2)).setVisibility(View.GONE);

		((TextView)findViewById(R.id.lblProducto2)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.lblPagoMensual2)).setVisibility(View.GONE);

		((ImageView)findViewById(R.id.imgBarraGrisClaro4)).setVisibility(View.GONE);
		
		((LinearLayout)findViewById(R.id.resumenCreditosContratados)).setVisibility(View.GONE);
		
		
		((TextView)findViewById(R.id.lblTituloCredOfer)).setVisibility(View.GONE);
		((ImageView)findViewById(R.id.imgBarraGrisClaro7)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.lblProducto3)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.lblPagoMensual3)).setVisibility(View.GONE);
		((ImageView)findViewById(R.id.imgBarraGrisClaro8)).setVisibility(View.GONE);
		((LinearLayout)findViewById(R.id.resumenCreditosOfertados)).setVisibility(View.GONE);
	   
	}
	
	public Integer pintarTablaContratados(){

		Integer cont = 0;
				
		if(delegate.getHaySimulacionCont()){
			ArrayList<CreditoContratado> lista = delegate.getCreditos();
			Iterator<CreditoContratado> it = lista.iterator();
		
			ListaDatosResumenController listaController;
			while(it.hasNext()){
				CreditoContratado p = it.next();
				
				if(p.getIndicadorSim()){
					
					Producto producto = delegate.getProductos().get(cont);
					listaController = new ListaDatosResumenController((LinearLayout)findViewById(R.id.resumenCreditosContratados),  this, R.id.resumenCreditosContratados,this,producto);
					listaController.addElement(p.getDesProd(), GuiTools.getMoneyString(p.getMontCre().toString()), false);
					
					totalPago = totalPago + p.getMontCre();
				
					
					++cont;
					
				}
			}
			if(cont>0)
				setSimulaContratados(true);
			else
				setSimulaContratados(false);
		}else{
			ocultaCreditosContratados();
		}
		
		return cont;
	}
	
	private void ocultaSimulados(){
		((ImageView)findViewById(R.id.imgResumen)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.lblTituloCredSimulados)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.lblDespliegaListaCredSim)).setVisibility(View.GONE);

		((ImageView)findViewById(R.id.imgBarraGris1)).setVisibility(View.GONE);

		((TextView)findViewById(R.id.lblProducto)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.lblPagoMensual)).setVisibility(View.GONE);

		((ImageView)findViewById(R.id.imgBarraGrisClaro1)).setVisibility(View.GONE);
		
		((LinearLayout)findViewById(R.id.resumenCreditosSimulados)).setVisibility(View.GONE);
	}
	
	public void pintarTablaSimulados(){		

		if(delegate.getHaySimulacion()){
			ArrayList<Producto> lista = delegate.getProductos();
			Iterator<Producto> it = lista.iterator();
			Integer cont = 0;
			
			ListaDatosResumenController listaController;
			
			while(it.hasNext())
			{
				Producto p = it.next();
				
				if(p.getIndSimBoolean())
				{
					if(p.getCveProd().equals("0TDC")|| p.getCveProd().equals("0ILC"))
					{
						listaController = new ListaDatosResumenController((LinearLayout)findViewById(R.id.resumenCreditosSimulados),  this, R.id.resumenCreditosSimulados,this,p);
						listaController.addElement(p.getCveProd(),p.getDesProd(), GuiTools.getMoneyString(p.getMontoDeseS()), false);
						
						//totalPago = totalPago + p.getSubproducto().get(0).getMonMax();
		
					}else
					{
						listaController = new ListaDatosResumenController((LinearLayout)findViewById(R.id.resumenCreditosSimulados),  this, R.id.resumenCreditosSimulados,this,p);
						listaController.addElement(p.getCveProd(),p.getDesProd(), GuiTools.getMoneyString(p.getPagMProdS()), false);
						
						totalPago = totalPago + Double.parseDouble(p.getPagMProdS());
						++cont;
						
					}
						
				}
			}
			if(cont>0)
				setSimulaOfertados(true);
			else
				setSimulaOfertados(false);
		}else{
			ocultaSimulados();
		}
	}
	
	
	public void showMailAlert(String texto, ResumenActivity act){
		final ResumenActivity activity = act;
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setMessage("BBVA Bancomer,S.A Universidad 1200, Col.Xoco,Deleg.Benito Juarez,03339,DF recaba sus datos para verificar su\nidentidad.El aviso de privacidad integral actualizado est�\nencualquier sucursal y en www.bancomer.com\n\n\nTe enviaremos el resumen \nde tu simulación");

		final EditText input = new EditText(this);
		input.setText(texto);
		alert.setView(input);

		alert.setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	         String srt = input.getEditableText().toString();
	        } 
		});
     
     
		alert.setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
		        String srt = input.getEditableText().toString();
				Map<String,Object> click_envio = new HashMap<String, Object>();
				click_envio.put("events","event17");
				click_envio.put("eVar17","simulador;simulador:envio email");
				TrackingHelperCredit.trackInicioOperacion(click_envio);
				delegate.sendEmail(activity, srt);
		    } 
     });
     	
     AlertDialog dialog = alert.show();
     
     TextView messageView = (TextView)dialog.findViewById(android.R.id.message);
     messageView.setGravity(Gravity.CENTER);
	}

	@Override
	public void onClick(View v) {
		isOnForeground=false;
		// TODO Auto-generated method stub
		if(v.getId()==R.id.btnEnvioEmail)
		{
			// Datos de correo electronico de usuario
			delegate.getCorreo(this);
		//this.showMailAlert(session.getEmail(),this);
		}
		if(v.getId()==R.id.btnRegresarResumen){
			delegate.redirectToView(OldMenuPrincipalActivity.class);
		}
		if(v.getId()==R.id.lblDespliegaListaCredSim)
		{
			delegate.redirectToView(CreditosSimuladosContratadosActivity.class);
		}
		if(v.getId()==R.id.lblDespliegaListaCredCont)
		{
			delegate.redirectToView(CreditosSimuladosContratadosActivity.class);
		}
		
	}
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(OldMenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }


	public boolean isSimulaContratados() {
		return simulaContratados;
	}

	public void setSimulaContratados(boolean simulaContratados) {
		this.simulaContratados = simulaContratados;
	}

	public boolean isSimulaOfertados() {
		return simulaOfertados;
	}

	public void setSimulaOfertados(boolean simulaOfertados) {
		this.simulaOfertados = simulaOfertados;
	}
}
