package bbvacredit.bancomer.apicredit.gui.delegates;

import android.util.Log;

import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.apiilc.models.OfertaILC;

import java.util.Hashtable;

import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.models.OfertaConsumo;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.ILCSeekBarViewController;
import bbvacredit.bancomer.apicredit.gui.activities.ILCViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.DetalleAlternativa;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by OOROZCO on 12/2/15.
 */


public class DetalleDeAlternativaDelegate extends BaseDelegateOperacion {

    // ---------- Attributes Region ------- //

	private String cliente;
	private String ium;
	private String numeroCelular;
	private String producto;
    private String fechaCat;
	
	
	private OfertaILC ofertaILC;
	private Promociones promocion;
	private OfertaConsumo ofertaConsumo;
    private Producto objetoProducto;

    private Producto oData;

    private ILCViewController ilcViewController;

    private ILCSeekBarViewController ilcSeekBarViewController;

    public ILCViewController getIlcViewController() {
        return ilcViewController;
    }

    public String getFechaCat() {
        return fechaCat;
    }

    public OfertaILC getOfertaILC() {
        return ofertaILC;
    }


    public DetalleDeAlternativaDelegate(String cliente, String ium, String numeroCelular, String producto, Producto objetoProducto) {
        this.objetoProducto = objetoProducto;
		this.cliente = cliente;
		this.ium = ium;
		this.numeroCelular = numeroCelular;
		this.producto = producto;
        ilcViewController = null;
	}

    public void setIlcViewController(ILCViewController ilcViewController) {
        this.ilcViewController = ilcViewController;
    }

    public void setIlcViewController(ILCSeekBarViewController ilcViewController) {
        this.ilcSeekBarViewController = ilcViewController;
    }

    @Override
	protected String getCodigoOperacion() {
		return Server.DETALLE_ALTERNATIVA;
	}
	
	
		
	public void consultaDetalleAlternativasTask(Producto oProducto){ //recibe objeto del producto a contratar

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		paramTable.put(ServerConstantsCredit.OPERACION_PARAM, getCodigoOperacion());
		paramTable.put(ServerConstantsCredit.CLIENTE_PARAM, cliente);
		paramTable.put(ServerConstantsCredit.IUM_PARAM, ium);
		paramTable.put(ServerConstantsCredit.NUMERO_CEL_PARAM, numeroCelular);
		paramTable.put(ServerConstantsCredit.PRODUCTO_PARAM, oProducto.getCveProd());

		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.consultaDetalleAlternativa(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2,true,new DetalleAlternativa(),MainController.getInstance().getContext());
	}
	
	public void consultaDetalleAlternativaTDCTask(Producto oData){
        Log.i("tdc","consultaDetalleAlternativaTDCTask");
        this.oData = oData;

        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        paramTable.put(ServerConstantsCredit.OPERACION_PARAM, Server.DETALLE_ALTERNATIVA_TDC);
        paramTable.put(ServerConstantsCredit.CLIENTE_PARAM, cliente);
        paramTable.put(ServerConstantsCredit.IUM_PARAM, ium);
        paramTable.put(ServerConstantsCredit.NUMERO_CEL_PARAM, numeroCelular);
        paramTable.put(ServerConstantsCredit.PRODUCTO_PARAM, producto);

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.consultaDetalleAlternativa(paramTable);
        this.doNetworkOperation(Server.DETALLE_ALTERNATIVA_TDC, paramTable2,true,new DetalleAlternativa(),MainController.getInstance().getContext());
    }
    
    public void analyzeResponse(String operationId, ServerResponse response) {

        if(Server.DETALLE_ALTERNATIVA_TDC.equals(operationId)){
            if((response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)||(response.getStatus() == ServerResponse.OPERATION_OPTIONAL_UPDATE)) {

                Log.i("tdc", "DetalleTDC, analyze");

                String montoSol = oData.getMontoDeseS();

                Log.i("contrataTDC","detalleAlternativaDelegat, montoSolcitado: "+montoSol);

                if(montoSol.contains(".")) {
                    montoSol = montoSol.replace(".", "");
                    oData.setMontoDeseS(montoSol);
                }

                DetalleAlternativa data = (DetalleAlternativa) response.getResponse();

                promocion = new Promociones();
                promocion.setCveCamp(data.getIdCampania());
                promocion.setCarrusel("");
                promocion.setDesOferta(data.getIdCampania());
                promocion.setMonto(montoSol);
                Log.i("TDC","iniciaFlujoTDC");
                iniciaFlujoTDC(promocion);
            }
        }else if(Server.DETALLE_ALTERNATIVA.equals(operationId)){
    		if((response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)||(response.getStatus() == ServerResponse.OPERATION_OPTIONAL_UPDATE)){
    			DetalleAlternativa data = (DetalleAlternativa) response.getResponse();
				promocion = new Promociones();
				promocion.setCveCamp(data.getIdCampania());
				promocion.setDesOferta("");

                if(producto.equals(ConstantsCredit.ADELANTO_NOMINA)){
                    android.util.Log.w("Detalle", "AnalizeResponse ANOM DetalleAlternativa");

                    promocion.setCveCamp(data.getIdCamCRM());

                    ofertaConsumo = new OfertaConsumo();

                    ofertaConsumo.setImporte(data.getImporte());
                    ofertaConsumo.setPlazoDes(data.getPlazoDes());
                    String bscPagar = new String(data.getBscPagar());
                    ofertaConsumo.setTipoSeg(bscPagar.length()>0?bscPagar.substring(0, 16):"");
                    ofertaConsumo.setTotalPagos(data.getTotalPagos());
                    ofertaConsumo.setDescDiasPago(data.getDescDiasPago());
                    ofertaConsumo.setFolioUG(data.getFolioUG());
                    ofertaConsumo.setProducto(data.getProducto());
                    ofertaConsumo.setCat(data.getCAT());
                    ofertaConsumo.setCuentaVinc(data.getCtaVinc());
                    ofertaConsumo.setTasaAnual(data.getTasaAnual());
                    ofertaConsumo.setTasaMensual(data.getTasaMensual());
                    ofertaConsumo.setPagoMenFijo(data.getPagoMenFijo());
                    ofertaConsumo.setFechaCat(data.getFechCat());

                    ofertaConsumo.setPlazo(data.getPlazo());
                    ofertaConsumo.setEstatusOferta(data.getEstatusOferta());
                    ofertaConsumo.setImpSegSal(data.getImpSegSal());

                    ofertaConsumo.setCveProd(producto);



                    iniciaFlujoConsumo();

                }else if(producto.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
					ofertaILC = new OfertaILC();
					promocion.setMonto(data.getMonDese());
					ofertaILC.setCat(data.getCAT());
					ofertaILC.setFechaCat(data.getFechCat());
                    promocion.setCveCamp(data.getIdCamCRM());

					ofertaILC.setImporte((int) (Float.parseFloat(data.getMonDese())) + "");
    				ofertaILC.setLineaActual((int) (Float.parseFloat(data.getLinCredAct())) + "");
    				ofertaILC.setLineaFinal((int) (Float.parseFloat(data.getLinCredFinal())) + "");
    				ofertaILC.setContrato("");
    				
    				Account cuenta = new Account();
    				cuenta.setNumber(ilcSeekBarViewController.getProducto().getNumTDCS());
    				cuenta.setAlias("");
                    Log.i("numeroTarjeta","numeroTarjeta: "+cuenta.getNumber());
                    Log.i("numeroTarjeta","numero tarjeta: "+ilcSeekBarViewController.getProducto().getNumTDCS());
    				ofertaILC.setILCAccount(cuenta);

                    //formatoFechaCat();
//                    ilcViewController.showIncrementarILC(this);
                    ilcSeekBarViewController.setDelegateOp(this);
                    ilcSeekBarViewController.setOfertaILC(ofertaILC);

                    ilcSeekBarViewController.setSimulation();
                    ilcSeekBarViewController.isSimulated = true;

                    if(ilcSeekBarViewController.isShowDetalle()){
                        ilcSeekBarViewController.setDetailProduct();

                    }else if (!ilcSeekBarViewController.isFromBar) {
                        ilcSeekBarViewController.setIsShowDetalle(false);
                        ilcSeekBarViewController.validateLayoutToShow();
                    }


    				//iniciarFlujoOneClickILC();
    			}
    			else {
					ofertaConsumo = new OfertaConsumo();

					ofertaConsumo.setImporte(data.getImporte());
					ofertaConsumo.setPlazoDes(data.getPlazoDes());
					String bscPagar = new String(data.getBscPagar());
					ofertaConsumo.setTipoSeg(bscPagar.length()>0?bscPagar.substring(0, 16):"");
					ofertaConsumo.setTotalPagos(data.getTotalPagos());
					ofertaConsumo.setDescDiasPago(data.getDescDiasPago());
					ofertaConsumo.setFolioUG(data.getFolioUG());
					ofertaConsumo.setProducto(data.getProducto());
    				ofertaConsumo.setCat(data.getCAT());
    				ofertaConsumo.setCuentaVinc(data.getCtaVinc());
					ofertaConsumo.setTasaAnual(data.getTasaAnual());
					ofertaConsumo.setTasaMensual(data.getTasaMensual());
					ofertaConsumo.setPagoMenFijo(data.getPagoMenFijo());
					ofertaConsumo.setFechaCat(data.getFechCat());

    				ofertaConsumo.setPlazo(data.getPlazo());
					ofertaConsumo.setEstatusOferta(data.getEstatusOferta());
					ofertaConsumo.setImpSegSal(data.getImpSegSal());
                    ofertaConsumo.setCveProd(producto);

                    if(objetoProducto.getIndRev() != null){
                        android.util.Log.i("revire","detalleAD, PPINOMINA, si trae revire");
                        if(objetoProducto.getIndRev().equalsIgnoreCase("S")){
                            ofertaConsumo.setTasa(objetoProducto.getTasaS());
                            ofertaConsumo.setTasaOri(objetoProducto.getTasaOri());
                            ofertaConsumo.setIndRev(objetoProducto.getIndRev());
                        }else{
                            ofertaConsumo.setTasa(objetoProducto.getTasaS());
                            ofertaConsumo.setTasaOri(objetoProducto.getTasaOri());
                            ofertaConsumo.setIndRev("");
                        }
                    }else{
                        ofertaConsumo.setTasa(objetoProducto.getTasaS());
                        ofertaConsumo.setTasaOri(objetoProducto.getTasaOri());
                        ofertaConsumo.setIndRev("");
                    }



                    promocion.setCveCamp(data.getIdCamCRM()); //clave

					iniciarFlujoOneClickConsumo();

    			}
    			
    		}else{
        		MainController.getInstance().ocultaIndicadorActividad();
        	}
    	}
    }

    public void contratacionILC() {
		MainController mainCntr = MainController.getInstance();
        mainCntr.getActivityController().getCurrentActivity().setActivityChanging(true);
		InitILC init = InitILC.getInstance(mainCntr.getActivityController().getCurrentActivity(),
                promocion, mainCntr.getDelegateOTP(), mainCntr.getSession(), mainCntr.getAutenticacion(), mainCntr.isSofTokenStatus(), mainCntr.getClient(), mainCntr.getHostInstance(),mainCntr.getListaCuentas(), Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
        init.isILCFromCredit(true);
        init.engagementFromCredit(ofertaILC);
    }

    private void iniciarFlujoOneClickConsumo(){ //PPI y Credito Nomina
        MainController mainCntr = MainController.getInstance();
        mainCntr.getActivityController().getCurrentActivity().setActivityChanging(true);
		InitConsumo init = InitConsumo.getInstance(mainCntr.getActivityController().getCurrentActivity(), promocion, mainCntr.getDelegateOTP(),
                mainCntr.getSession(), mainCntr.getAutenticacion(), mainCntr.isSofTokenStatus(), mainCntr.getClient(), mainCntr.getHostInstance(),
                true, Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
		init.setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
		init.showDetalleConsumo(ofertaConsumo, promocion,false);
    }

    public void iniciaFlujoConsumo(){ //anom
        MainController mainCntr = MainController.getInstance();
        mainCntr.getActivityController().getCurrentActivity().setActivityChanging(true);
        InitConsumo init = InitConsumo.getInstance(mainCntr.getActivityController().getCurrentActivity(),
                null,mainCntr.getDelegateOTP(),
                mainCntr.getSession(),mainCntr.getAutenticacion(), mainCntr.isSofTokenStatus(),
                mainCntr.getClient(), mainCntr.getHostInstance(),
                true, Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
        init.setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
        init.showContratoANViewController(ofertaConsumo, promocion);

        init.setResOfertaConsumo(ofertaConsumo);
        init.setRespPromociones(promocion);

    }
	
	public void iniciaFlujoTDC(Promociones oPromocion){
    

        MainController mainCntr = MainController.getInstance();
        InitVentaTDC initVentaTDC = InitVentaTDC.getInstance(mainCntr.getActivityController().getCurrentActivity(),
                promocion,mainCntr.getDelegateOTP(),
                mainCntr.getSession(), mainCntr.getAutenticacion(), mainCntr.isSofTokenStatus(), mainCntr.getNumeroCuenta(),mainCntr.getClient(),
                mainCntr.getHostInstance(),Server.DEVELOPMENT,Server.SIMULATION,Server.EMULATOR,Server.ALLOW_LOG);
        mainCntr.getActivityController().getCurrentActivity().setActivityChanging(true);
        initVentaTDC.startVentaTDC();

    }

    public void formatoFechaCat(){
        try{
            String date = ofertaILC.getFechaCat();
            String[] auxiliar = date.split("-");

            int month = Integer.valueOf(auxiliar[1]);
            switch (month)
            {
                case 1: fechaCat= auxiliar[2]+ " de " + "Enero" + " de "+ auxiliar[0];
                    break;
                case 2: fechaCat= auxiliar[2]+ " de " + "Febrero" + " de "+ auxiliar[0];
                    break;
                case 3: fechaCat= auxiliar[2]+ " de " + "Marzo" + " de "+ auxiliar[0];
                    break;
                case 4: fechaCat= auxiliar[2]+ " de " + "Abril" + " de "+ auxiliar[0];
                    break;
                case 5: fechaCat= auxiliar[2]+ " de " + "Mayo" + " de "+ auxiliar[0];
                    break;
                case 6: fechaCat= auxiliar[2]+ " de " + "Junio" + " de "+ auxiliar[0];
                    break;
                case 7: fechaCat= auxiliar[2]+ " de " + "Julio" + " de "+ auxiliar[0];
                    break;
                case 8: fechaCat= auxiliar[2]+ " de " + "Agosto" + " de "+ auxiliar[0];
                    break;
                case 9: fechaCat= auxiliar[2]+ " de " + "Septiembre" + " de "+ auxiliar[0];
                    break;
                case 10: fechaCat= auxiliar[2]+ " de " + "Octubre" + " de "+ auxiliar[0];
                    break;
                case 11: fechaCat= auxiliar[2]+ " de " + "Noviembre" + " de "+ auxiliar[0];
                    break;
                case 12: fechaCat= auxiliar[2]+ " de " + "Diciembre" + " de "+ auxiliar[0];
                    break;
            }

            /*
            try{
            String[] meses = { "enero", "febrero", "marzo", "abril", "mayo",
                    "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };
            String fechaCatM= ofertaILC.getFechaCat();
            int mesS=Integer.parseInt(fechaCatM.substring(5,7));
            String dia= fechaCatM.substring(fechaCatM.length()-2);
            String anio= fechaCatM.substring(0,4);

            fechaCat= dia+" de "+meses[mesS-1].toString()+ " de "+anio;

            }catch(Exception ex){
            if(Server.ALLOW_LOG) Log.e("","error formato"+ex);
            }
            */

        }catch(Exception ex){
            if(com.bancomer.apiilc.io.Server.ALLOW_LOG) Log.e("","error formato"+ex);
        }
    }
}