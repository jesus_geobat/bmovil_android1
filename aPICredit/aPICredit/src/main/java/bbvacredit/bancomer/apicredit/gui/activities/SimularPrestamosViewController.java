package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;


public class SimularPrestamosViewController extends Fragment {
    private Button btnVerPrestamos;
	
	public SimularPrestamosViewController() {
		// TODO Auto-generated constructor stub
	}
 
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_credit_simula_prestamos, container, false);

        if (rootView != null) {
            findViews(rootView);
            btnVerPrestamos.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MainController.getInstance().showScreen(PrestamosContratadosViewController.class);
                    ((MenuPrincipalActivity)getActivity()).isCreditShown(false);
                }
            });
        }

        return  rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {

        }
    }

    private void findViews(View rootView) {
        btnVerPrestamos = (Button) rootView.findViewById(R.id.btnVerPrestamos);

       Session session = Session.getInstance();
//       if(session.getDataFromCreditos().getCreditosContratados().size()==0)
           if(!Tools.getCurrentSession().getShowCreditosContratados())
               btnVerPrestamos.setVisibility(View.GONE);

    }
}
