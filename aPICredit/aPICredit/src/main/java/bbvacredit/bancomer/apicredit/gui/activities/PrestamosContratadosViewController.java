package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.PrestamosContratadosDelegate;
import bbvacredit.bancomer.apicredit.models.CreditoContratado;
import tracking.TrackingHelperCredit;
import com.viewpagerindicator.CirclePageIndicator;
//import android.app.Fragment;
//import android.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.support.v4.app.FragmentActivity;
import com.viewpagerindicator.PageIndicator;

/**
 * Created by SDIAZ on 25/11/15.
 */
public class PrestamosContratadosViewController extends BaseActivity implements ViewPager.OnPageChangeListener {

    private LinearLayout lytArriba;
    private LinearLayout lytTextoArriba;
    private int itemSelected;

    ContratadosFragmentAdapter mAdapter;
    public static ViewPager mPager;
    public static PageIndicator mIndicator;

    Fragment fragmentArriba;
    PrestamosContratadosDelegate delegate;

    public PrestamosContratadosViewController() {
        // TODO Auto-generated constructor stub
        fragmentArriba = null;
        delegate = new PrestamosContratadosDelegate();
        delegate.setActivityContratados(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER |SHOW_HEADER, R.layout.activity_credit_prestamos_simulados_contratados);

        setActivityChanging(false);

        imgEncabezado.setImageResource(R.drawable.txt_contratados);
        MainController.getInstance().setContext(this);
        MainController.getInstance().setCurrentActivity(this);

        fragmentArriba = new ContratadosDescripcion();
        ((ContratadosDescripcion)fragmentArriba).setDelegate(delegate);

        imgBtnEliminar.setVisibility(View.INVISIBLE);
        //Validamos si el fragment no es nulo
        if (fragmentArriba != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.superior_contratados, fragmentArriba).commit();

        } else {
            //Si el fragment es nulo mostramos un mensaje de error.
            Log.e("Error  ", "Error al cargar el fragment arriba prestamos contratados");
        }

        mAdapter = new ContratadosFragmentAdapter(getSupportFragmentManager(), delegate);

        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);

        lytArriba = (LinearLayout) findViewById(R.id.superior_contratados);

        //mPager.setOnPageChangeListener(this);
        mIndicator.setOnPageChangeListener(this);
    }

    public static ViewPager getMPager(){
        return mPager;
    }

    public static PageIndicator getMIndicator(){
        return mIndicator;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.cambiaValores();
        delegate.setArriba(false);
    }

    public void botonArriba() {

        itemSelected = PrestamosContratadosViewController.getMPager().getCurrentItem();
        Log.e("ITEM botón arriba--- ", String.valueOf(itemSelected));

        delegate.setArriba(true);

        CreditoContratado pruebaLiquidacion = (CreditoContratado) delegate.getCreditosContratados().get(itemSelected);
        delegate.eliminarSimulacionLiquidacionCredito(pruebaLiquidacion);

    }

    public void botonAbajo() {

        lytTextoArriba = (LinearLayout) findViewById(R.id.textoPago);

        lytTextoArriba.setVisibility(View.VISIBLE);
        ImageView botonArriba = (ImageView) findViewById(R.id.imgDespliegueArriba);
        ImageView botonAbajo = (ImageView) findViewById(R.id.imgDespliegueAbajo);

        botonArriba.setVisibility(View.VISIBLE);
        botonAbajo.setVisibility(View.GONE);
        Log.d("click Arriba", "Parte superior desactivada");

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        params.weight = 1f;
        lytArriba.setLayoutParams(params);

        delegate.setArriba(false);

        mAdapter.botonAbajo();
        Log.d("click Arriba", "Parte inferior activada");
    }

    public void muestraSimLiquidacion() {
        lytTextoArriba = (LinearLayout) findViewById(R.id.textoPago);

        lytTextoArriba.setVisibility(View.GONE);
        ImageView botonArriba = (ImageView) findViewById(R.id.imgDespliegueArriba);
        ImageView botonAbajo = (ImageView) findViewById(R.id.imgDespliegueAbajo);

        botonArriba.setVisibility(View.GONE);
        botonAbajo.setVisibility(View.VISIBLE);
        Log.d("click Arriba", "Parte superior desactivada");

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.weight = 0f;
        lytArriba.setLayoutParams(params);

        mAdapter.botonArriba(itemSelected);
        Log.d("click Arriba", "Parte inferior activada");
    }

    public void onPageSelected(int position) {

        Log.d("onPageSelected", "El viewpager ha cambiado al número de vista: " + position);
        if(delegate.getArriba())
            botonArriba();
    }

    public void onPageScrollStateChanged(int state) {

    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }
}
