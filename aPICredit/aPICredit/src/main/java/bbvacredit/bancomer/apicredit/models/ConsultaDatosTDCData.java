package bbvacredit.bancomer.apicredit.models;

import java.io.IOException;
import java.util.Date;


import bbvacredit.bancomer.apicredit.io.Parser;
import bbvacredit.bancomer.apicredit.io.ParsingException;
import bbvacredit.bancomer.apicredit.io.ParsingHandler;


public class ConsultaDatosTDCData  implements ParsingHandler {
	
	private String PE;
	
	private String SC;
	
	private Integer PM;
	
	private Integer FL;
	
	private Date FE;
	
	private Integer OC;
	
	//TODO: duda abierta
	private Date FE2;
	
	private Integer IM;
	
	private String DE;
	
	private Date AU;
	
	private Date AP;
	
	public String getPE() {
		return PE;
	}



	public void setPE(String pE) {
		PE = pE;
	}



	public String getSC() {
		return SC;
	}



	public void setSC(String sC) {
		SC = sC;
	}



	public Integer getPM() {
		return PM;
	}



	public void setPM(Integer pM) {
		PM = pM;
	}



	public Integer getFL() {
		return FL;
	}



	public void setFL(Integer fL) {
		FL = fL;
	}



	public Date getFE() {
		return FE;
	}



	public void setFE(Date fE) {
		FE = fE;
	}



	public Integer getOC() {
		return OC;
	}



	public void setOC(Integer oC) {
		OC = oC;
	}



	public Date getFE2() {
		return FE2;
	}



	public void setFE2(Date fE2) {
		FE2 = fE2;
	}



	public Integer getIM() {
		return IM;
	}



	public void setIM(Integer iM) {
		IM = iM;
	}



	public String getDE() {
		return DE;
	}



	public void setDE(String dE) {
		DE = dE;
	}



	public Date getAU() {
		return AU;
	}



	public void setAU(Date aU) {
		AU = aU;
	}



	public Date getAP() {
		return AP;
	}



	public void setAP(Date aP) {
		AP = aP;
	}



	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		PE = parser.parseNextValue("PE");
		SC = parser.parseNextValue("SC");
	}
}
