package bbvacredit.bancomer.apicredit.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.CreditJSONAble;

import java.io.IOException;
import java.util.ArrayList;

import bbvacredit.bancomer.apicredit.io.ParsingHandlerJSON;
import bbvacredit.bancomer.apicredit.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

public class AceptaOfertaEFI implements ParsingHandlerJSON, CreditJSONAble{
	String tipoCuenta;
	String numeroTarjeta;
	String impDispuesto;
	String numeroCuenta;
	String plazoMeses;
	String nombreTitular;
	String fechaOperacion;
	String horaOperacion;
	String comisionEFIPorcentaje;
	String comisionEFIMonto;
	String pagoMensual;
	String fechaCat;
	String email;
	String Cat;
	String folioAST;
	String ofertaCruzada;
	Promociones promocion;
	Promociones[] promociones;
	String PM;
	String tasaMensual;
	
	
	
	public String getTipoTarjeta() {
		return tipoCuenta;
	}

	public void setTipoTarjeta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	
	
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getImpDispuesto() {
		return impDispuesto;
	}

	public void setImpDispuesto(String impDispuesto) {
		this.impDispuesto = impDispuesto;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getPlazoMeses() {
		return plazoMeses;
	}

	public void setPlazoMeses(String plazoMeses) {
		this.plazoMeses = plazoMeses;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getHoraOperacion() {
		return horaOperacion;
	}

	public void setHoraOperacion(String horaOperacion) {
		this.horaOperacion = horaOperacion;
	}

	public String getComisionEFIPorcentaje() {
		return comisionEFIPorcentaje;
	}

	public void setComisionEFIPorcentaje(String comisionEFIPorcentaje) {
		this.comisionEFIPorcentaje = comisionEFIPorcentaje;
	}

	public String getComisionEFIMonto() {
		return comisionEFIMonto;
	}

	public void setComisionEFIMonto(String comisionEFIMonto) {
		this.comisionEFIMonto = comisionEFIMonto;
	}

	public String getPagoMensual() {
		return pagoMensual;
	}

	public void setPagoMensual(String pagoMensual) {
		this.pagoMensual = pagoMensual;
	}

	public String getFechaCat() {
		return fechaCat;
	}

	public void setFechaCat(String fechaCat) {
		this.fechaCat = fechaCat;
	}

	public String getCat() {
		return Cat;
	}

	public void setCat(String cat) {
		Cat = cat;
	}

	public String getFolioAST() {
		return folioAST;
	}

	public void setFolioAST(String folioAST) {
		this.folioAST = folioAST;
	}

	public String getOfertaCruzada() {
		return ofertaCruzada;
	}

	public void setOfertaCruzada(String ofertaCruzada) {
		this.ofertaCruzada = ofertaCruzada;
	}

	public Promociones getPromocion() {
		return promocion;
	}

	public void setPromocion(Promociones promocion) {
		this.promocion = promocion;
	}

	public Promociones[] getPromociones() {
		return promociones;
	}

	public void setPromociones(Promociones[] promociones) {
		this.promociones = promociones;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPM() {
		return PM;
	}

	public void setPM(String pM) {
		PM = pM;
	}
	
	public String getTasaMensual() {
		return tasaMensual;
	}

	public void setTasaMensual(String tasaMensual) {
		this.tasaMensual = tasaMensual;
	}

	@Override
	public void fromJSON(String jsonString) {

	}

	@Override
	public String toJSON() {
		return null;
	}

	@Override
	public void processJSON(String jsonString) throws JSONException {
		JSONObject obj = new JSONObject(jsonString);

		numeroTarjeta=obj.getString("numeroTarjeta");
		impDispuesto=obj.getString("impDispuesto");
		numeroCuenta=obj.getString("numeroCuenta");
		plazoMeses=obj.getString("plazoMeses");
		nombreTitular=obj.getString("nombreTitular");
		fechaOperacion=obj.getString("fechaOperacion");
		comisionEFIPorcentaje=obj.getString("comisionEFIPorcentaje");
		comisionEFIMonto=obj.getString("comisionEFIMonto");
		email=obj.getString("email");
		pagoMensual=obj.getString("pagoMensual");
		fechaCat=obj.getString("fechaCat");
		Cat=obj.getString("Cat");
		tasaMensual=obj.getString("tasaMensual");
		folioAST=obj.getString("folioAST");
		ofertaCruzada=obj.getString("ofertaCruzada");
		PM=obj.getString("PM");
		try {
			if(ofertaCruzada!=null){
				if(!ofertaCruzada.equals("NO")){
					JSONObject jsonObject= new JSONObject(obj.getString("LO"));
					promocion= new Promociones();
					String cveCamp=jsonObject.getString("cveCamp");
					promocion.setCveCamp(cveCamp);
					String desOferta= jsonObject.getString("desOferta");
					promocion.setDesOferta(desOferta);
					String monto=jsonObject.getString("monto");
					promocion.setMonto(monto);
				}
			}
			if(PM.equals("SI")){
				JSONObject jsonObjectPromociones= new JSONObject(obj.getString("LP"));
				JSONArray arrPromocionesJson =jsonObjectPromociones.getJSONArray("campanias");
				ArrayList<Promociones> arrPromociones = new ArrayList<Promociones>();
				for (int i = 0; i < arrPromocionesJson.length(); i++) {
					JSONObject jsonPromocion = (JSONObject) arrPromocionesJson.get(i);
					Promociones promo = new Promociones();
					promo.setCveCamp(jsonPromocion.getString("cveCamp"));
					promo.setDesOferta(jsonPromocion.getString("desOferta"));
					promo.setMonto(jsonPromocion.getString("monto"));
					arrPromociones.add(promo);
				}
				this.promociones = arrPromociones.toArray(new Promociones[arrPromociones.size()]);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			if(Server.ALLOW_LOG) e.printStackTrace();
		}

	}
}
