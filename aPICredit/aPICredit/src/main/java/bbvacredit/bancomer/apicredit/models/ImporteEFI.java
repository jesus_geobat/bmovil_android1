package bbvacredit.bancomer.apicredit.models;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.CreditJSONAble;

import java.io.IOException;

import bbvacredit.bancomer.apicredit.io.ParsingHandlerJSON;

public class ImporteEFI implements ParsingHandlerJSON, CreditJSONAble {
	String pagoMensual;
	String comisionEFIMonto;
	
	
	public String getPagoMensual() {
		return pagoMensual;
	}

	public void setPagoMensual(String pagoMensual) {
		this.pagoMensual = pagoMensual;
	}

	public String getComisionEFIMonto() {
		return comisionEFIMonto;
	}

	public void setComisionEFIMonto(String comisionEFIMonto) {
		this.comisionEFIMonto = comisionEFIMonto;
	}

	@Override
	public void fromJSON(String jsonString) {

	}

	@Override
	public String toJSON() {
		return null;
	}

	@Override
	public void processJSON(String jsonString) throws JSONException {
		JSONObject obj = new JSONObject(jsonString);

		pagoMensual=obj.getString("pagoMensual");
		comisionEFIMonto=obj.getString("ComisionEFIMonto");
	}
}
