package bbvacredit.bancomer.apicredit.gui.delegates;

import android.util.Log;

import java.util.Hashtable;

import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.AutoSeekBarViewController;
import bbvacredit.bancomer.apicredit.gui.activities.HipotecarioSeekBarViewController;
import bbvacredit.bancomer.apicredit.gui.activities.ILCSeekBarViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;

/**
 * Created by FHERNANDEZ on 28/12/16.
 */
public class AutoSeekBarDelegate extends BaseDelegateOperacion {

    private AutoSeekBarViewController controller;
    private Producto producto;
    private ObjetoCreditos data;
    private ObjetoCreditos auxData;
    private Session session;
    private boolean isDeleteOperation = false; //si es true, borrar simulación
    private boolean productoConPlazo;
    private boolean updateData = false;
    private String indSim, cveSub, cvePze;

    public AutoSeekBarDelegate(AutoSeekBarViewController controller){
        this.controller = controller;
        session = Tools.getCurrentSession();
    }


    public void analyzeResponse(String operationId, ServerResponse response) {
        if(getCodigoOperacion().equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                MainController.getInstance().ocultaIndicadorActividad();
                session.setAuxCreditos(data);
                setProducto(Tools.getProductByCve("AUTO", data.getProductos()));
                controller.updateData(false);

                indSim = getProducto().getIndSim();
                controller.setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);

                controller.setSimulation();
                controller.isSimulated = true;

                //Si el detalle está visible, manda nuevos datos
                if(controller.isShowDetalle()){
                    controller.setDetailProduct();

                }else if (!controller.isFromBar) {
                    controller.setIsShowDetalle(false);
                    controller.validateLayoutToShow();
                }

            }
        }else if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

                CalculoData respuesta = (CalculoData) response.getResponse();

                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                MainController.getInstance().ocultaIndicadorActividad();
                session.setAuxCreditos(data);  //se guarda por separado
//                session.setCreditos(data);
//                Log.i("update","calc oper, maximo auto: "+Tools.getProductByCve("AUTO",data.getProductos()).getMontoMaxS());

                setProducto(Tools.getProductByCve("AUTO", data.getProductos()));
                indSim = getProducto().getIndSim();
                controller.setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);

                if(isDeleteOperation){
                    controller.updateData(true);
                    isDeleteOperation = false;
                    controller.isSimulated = false;

                    if (updateData) {
                        Log.i("update","actualiza ahora si");
                        updateData(cveSub, cvePze, false);
                    }else {
                        Log.i("update","Se fue a recalculo");
                        doRecalculo();
                    }

                }else if(updateData) { //se actualizo el plazo o el tipo

                    updateData = false;
                    controller.updateData(false);

                    //si actualizó montos máximos, pintarlos
                    setCleanedValues();
//                    controller.setValuesProduct();
                    Log.i("update","entro a update data");
                    Log.i("update","auto, monto máximo. "+getProducto().getMontoMaxS());

                    doRecalculo();
                }
            }
        }
    }

    private void setCleanedValues(){
        controller.setProducto(getProducto());
        controller.initView(controller.getView());
    }


    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {
        Log.i("update","eliminando");

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(Server.CALCULO_ALTERNATIVAS_OPERACION, ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        if (isSavingOperation) {
            addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = false;
        } else {
            addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = true;
        }

        Hashtable<String, String> paramTable2 = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    public void doRecalculo(){

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
        Producto ccr = controller.getProducto();

        String montoSel = "";
        montoSel = controller.edtMontoVisible.getText().toString();
        montoSel = montoSel.replace("$", "");
        montoSel = montoSel.replace(",", "");

        String plazoSel = controller.getCvePlazo();

        String cveHipotecaSel = getCvSubPByValue(ccr, controller.getTipo());
        String cvePlazoSel = getCvPlazoByValue(ccr, plazoSel);

        addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM,paramTable);
        addParametro(cveHipotecaSel, ServerConstantsCredit.CVESUBP_PARAM,paramTable);
        addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM,paramTable);
        addParametro(cvePlazoSel, ServerConstantsCredit.CVEPLAZO_PARAM,paramTable);

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    public void updateData(String cveSub, String cvePzoE, boolean isUpdate){
        Log.i("update","se actualiza dato, true borra, false calcula con 5: "+isUpdate);
        updateData = true;

        this.cveSub = cveSub;
        this.cvePze = cvePzoE;

        if (isUpdate) {
            Log.i("update","primero elimina");
            saveOrDeleteSimulationRequest(false);

        }else {

            Hashtable<String, String> paramTable = new Hashtable<String, String>();

            addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
            addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
            addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
            addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

            addParametroObligatorio(ConstantsCredit.OP_AJUSTE_PLAZOS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);

            Producto ccr = controller.getProducto();

            addParametroObligatorio(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM, paramTable);
            addParametroObligatorio(cvePzoE, ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
            addParametroObligatorio("", ServerConstantsCredit.MON_DESE_PARAM, paramTable);
            addParametroObligatorio(cveSub, ServerConstantsCredit.CVESUBP_PARAM, paramTable);

            Hashtable<String, String> paramTable2 = AuxConectionFactoryCredit.calculo(paramTable);
            this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
        }
    }


    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    protected String getCodigoOperacion() {
        return Server.CALCULO_OPERACION;
    }
}
