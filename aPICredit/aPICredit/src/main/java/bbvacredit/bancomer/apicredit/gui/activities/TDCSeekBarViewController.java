package bbvacredit.bancomer.apicredit.gui.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.LabelMontoTotalCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.AdelantoNominaSeekBarDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.TDCDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.TDCSeekBarDelegate;
import bbvacredit.bancomer.apicredit.models.OfertaTDC;
import bbvacredit.bancomer.apicredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by FHERNANDEZ on 30/12/16.
 *
 * setMonMax y setMonMin serán seteados sin formato.
 * ejemplo. un monto de $9,000.00 será 9000
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class TDCSeekBarViewController extends Fragment implements View.OnClickListener{

    private Producto producto;

    private ImageView imgvProd;
    private ImageView imgvShowDetalle;
    private ImageView imgvAyuda;

    private ImageButton imgvContratarCredito;

    private ImageView imgvTipoTarjeta;

    public LabelMontoTotalCredit edtMontoVisible;

    private TextView txtvDescProd;
    private TextView txtvMontoMinimo;
    private TextView txtvMontoMaximo;
    private TextView txtvCat;
    private TextView txtvTasa;
    private TextView txtvTextSaldoTotal;
    private TextView txtvSaldoTotalPagar;
    private TextView txtvOpcion;
    private TextView txtvPlazo;
    private TextView txtvTipo;
    private TextView txtvMasInfo;
    private TextView txtvPagoMMax;
    private TextView txtvMontoMMax;
    private TextView txtvdetalleTDC_ILC;
    private TextView txtvResultadoDetalleTDCILC;

    private LinearLayout lnlDetalleProd;
    private LinearLayout lnlTipo;
    private LinearLayout lnlPlazo;
    private LinearLayout lnlPlazoTipo;
    private LinearLayout lnlPagoMMaximo;

    private SeekBar seekBarSimulador;

    private String montoMax = "";
    private String montMin = "";
    private StringBuffer typedString= new StringBuffer();
    private String amountString=null;
    private String nombreTarjeta;
    private boolean indSimProd;
    private String auxFinalMax;
    public String montoSeleccionado;

    private String [] clavePlat     = {"AP"};
    private String [] claveOro      = {"AV"};
    private String [] claveEduc     = {"TE"};
    private String [] claveAzul     = {"AC"};
    private String [] claveRayados  = {"AR"};
    private String [] claveUnam     = {"UM"};
    private String [] claveIPN      = {"PN"};
    private String [] claveCongelada= {"AG"};
    private String [] claveVive     = {"XA"};
    private String [] claveInfitine = {"V4"};

    private boolean isSettingText = false;
    private boolean isResetting = false;
    private boolean mAcceptCents=false;
    private boolean flag=true;
    public boolean isShowDetalle;
    public boolean isSimulated = false;
    public boolean isFromBar;
    public boolean isTDCAMP;
    public boolean isBackProd = true;

    public boolean initalCampain = false;

    public boolean auxisCamp = false;

    private Promociones promocionTDC;

    private TDCSeekBarDelegate delegate;


    private TDCSeekBarViewController controller;

    private final int INCREMENTAL = 1000;

    public OfertaTDC ofertaTDC;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_credit_item_listview_creditos,container,false);

        if(rootView != null){
            delegate = new TDCSeekBarDelegate(this);
//            Log.i("tdc","inicia, cve prod: "+getProducto().getCveProd()+", montmin: "+getProducto().getMontoMinS()+", monMax: "+getProducto().getMontoMaxS());

            controller = this;
            initView(rootView);
            setIsShowDetalle(false);
        }

        return rootView;
    }

    public void initView(View rootView){
        isTDCAMP = ((MenuPrincipalActivitySeekBar)getActivity()).isTDCCamp;

        if(isTDCAMP)
            delegate.setPromociones(getPromocionTDC());
        findViews(rootView);
        validateSO();
        setValuesProduct();
        setValuesSeekBar();
        setVisibleMont(rootView);
        validateCampainOfert();
    }

    public void validateCampainOfert(){
        //si solo trae campañas de oneclick
        if (isTDCAMP && getProducto()==null){
            edtMontoVisible.setEnabled(false);
            seekBarSimulador.setEnabled(false);
        }
    }

    public void dissableProd(){
        edtMontoVisible.setEnabled(false);
        seekBarSimulador.setEnabled(false);
        imgvShowDetalle.setEnabled(false);
        edtMontoVisible.setText("$0");
        txtvMontoMinimo.setVisibility(View.GONE);
        txtvMontoMaximo.setText("$0");
    }

    public void eneableProd(){
        edtMontoVisible.setEnabled(true);
        seekBarSimulador.setEnabled(true);
        imgvShowDetalle.setEnabled(true);
        txtvMontoMinimo.setVisibility(View.VISIBLE);
    }

    private void setVisibleMont(final View view){
        edtMontoVisible.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Log.i("edt","limpia monto, "+event);
                if(event.getAction() == MotionEvent.ACTION_UP){
                    Log.i("edt","action up, limpia texto");

                    ((MenuPrincipalActivitySeekBar)getActivity()).validateEdtMontoVisible();
                    ((MenuPrincipalActivitySeekBar)getActivity()).isTDCEditable = true;
                    ((MenuPrincipalActivitySeekBar)getActivity()).isTDCEditableS = edtMontoVisible.getText().toString();

                    reset();
                    mAcceptCents = true;
                    isSettingText = true;
                    edtMontoVisible.setText("");
                    return false;
                }else{
                    Log.w("edt","otro evento, no limpia");
                }

                return false;
            }
        });

        edtMontoVisible.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                String amountField = edtMontoVisible.getText().toString();
                if (!isSettingText) {
                    amountString = amountField;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String amountField = edtMontoVisible.getText().toString();
                isBackProd = false;
                if (s.toString().equals(".") || s.toString().equals("0")) {
                    edtMontoVisible.setText("");
                } else {
                    if (!isSettingText && !isResetting) {
                        if (!flag) {
                            String aux;
                            try {
                                aux = edtMontoVisible.getText().toString();//.substring(0, edtMontoVisible.length() - 3);
                                typedString = new StringBuffer();

                                if (aux.charAt(0) == '0')
                                    aux = aux.substring(1, aux.length());
                            } catch (Exception ex) {
                                aux = "";

                            }
                            for (int i = 0; i < aux.length(); i++) {
                                if (aux.charAt(i) != ',')
                                    typedString.append(aux.charAt(i));
                            }
                            flag = true;

                            setFormattedText();
                            amountField = edtMontoVisible.getText().toString();
                            amountString = amountField;
                        } else {
                            try {
                                if (edtMontoVisible.length() < amountString.length()) {
                                    reset();
                                } else if (edtMontoVisible.length() > amountString.length()) {

                                    int newCharIndex = edtMontoVisible.getSelectionEnd() - 1;
                                    //there was no selection in the field
                                    if (newCharIndex == -2) {
                                        newCharIndex = edtMontoVisible.length() - 1;
                                    }

                                    char num = amountField.charAt(newCharIndex);
                                    if (!(num == '0' && typedString.length() == 0)) {
                                        typedString.append(num);
                                    }
                                    setFormattedText();
                                }
                            } catch (StringIndexOutOfBoundsException ex) {
                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                isSettingText = false;
                edtMontoVisible.setSelection(edtMontoVisible.length());
            }
        });

        edtMontoVisible.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager manager = (InputMethodManager) ((MenuPrincipalActivitySeekBar) getActivity()).getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (manager != null) {
                        manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                        String auxMon = edtMontoVisible.getText().toString();
                        String respuesta = "";

                        if (edtMontoVisible.getText().toString().equals("")) {
                            edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                            updateGraph(montMin);
                        } else {
                            respuesta = Tools.validateMont(edtMontoVisible.getMontoMax(),
                                    edtMontoVisible.getMontoMin(), auxMon, null);
                            try {
                                if (respuesta.equals(ConstantsCredit.MONTO_MAYOR)) {
                                    edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                                    updateGraph(montoMax);
                                } else if (respuesta.equals(ConstantsCredit.MONTO_MENOR)) {
                                    edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montMin)));
                                    updateGraph(montMin);
                                } else if (respuesta.equals(ConstantsCredit.MONTO_EN_RANGO)) {
                                    String auxLbl = edtMontoVisible.getText().toString();
                                    if (!auxLbl.startsWith("$"))
                                        edtMontoVisible.setText("$" + auxLbl);
                                    updateGraph(auxLbl);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                    edtMontoVisible.clearFocus();
                }


                return false;
            }
        });

        edtMontoVisible.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    String amount = edtMontoVisible.getText().toString();

                    if (!amount.equals("") || amount != null) {
                        if (amount.contains(","))
                            amount = amount.replace(",", "");

                        if (amount.contains("$"))
                            amount = amount.replace("$", "");

                        amount = amount.substring(0, amount.length() - 1);

                        edtMontoVisible.setText(Tools.formatUserAmount(amount));

                    }
                }

                return false;
            }
        });
    }

    public void updateGraph(String amount){
        isFromBar = true;

        //Indica que fue modificado y que si se contrata, se lanzará por simulador.
        //si se encuentra en false, indicará que no fue modificada, por lo que se lanzará por oneclick
        if (isTDCAMP) {
            if (!auxisCamp) {
//                Log.i("campaña","updateGraph cambia bandera a true");
                auxisCamp = true;
            }
        }

        double newAmount = 0.0, finalmmax = 0.0;
        int finalP = 0, auxMonMin = 0, auxAmount2 = 0;
        String auxAmount = Tools.clearAmounr(amount);

        if(auxAmount.equals(getMontMin())){
            seekBarSimulador.setProgress(0);

        }else if(auxAmount.equals(getMontoMax())){
            finalmmax = Double.parseDouble(getAuxFinalMax());
            seekBarSimulador.setProgress((int) finalmmax);

        }else {

            auxMonMin = Integer.parseInt(getMontMin());
            auxAmount2 = Integer.parseInt(auxAmount);

            finalP = auxAmount2-auxMonMin;
            newAmount = finalP / INCREMENTAL;

            seekBarSimulador.setProgress((int)newAmount);
        }

        validateProducSimulated();
    }

    private void updateGraph2(String amount){

        double newAmount = 0.0, finalmmax = 0.0, auxAmount2 = 0.0,  auxMonMin = 0;
        double finalP = 0;

        String auxAmount = Tools.clearAmounr(amount);

        if(getProducto()!=null) {
            auxMonMin = Double.parseDouble(getMontMin());
            auxAmount2 = Double.parseDouble(auxAmount);

            finalP = auxAmount2 - auxMonMin;
            newAmount = finalP / INCREMENTAL;
        }else{
            auxAmount2 = Double.parseDouble(auxAmount);
            newAmount = auxAmount2 /INCREMENTAL;
        }

        seekBarSimulador.setProgress((int) newAmount);

    }

    private void setValuesSeekBar() {

        if (getProducto() != null) {
            final double mmax = Double.parseDouble(Tools.validateMont(getMontoMax(), false));
            final double mmin = Double.parseDouble(Tools.validateMont(getMontMin(), false));
            double auxMmax = mmax - mmin;

            auxMmax = auxMmax / INCREMENTAL;

            final double finalAuxMmax = auxMmax;

            setAuxFinalMax(String.valueOf(finalAuxMmax));

            seekBarSimulador.setMax((int) finalAuxMmax);
            seekBarSimulador.setProgress((int) finalAuxMmax);

            seekBarSimulador.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    String visibleMont;

                    final int progressChanged = progress;
                    final int progresFinal = calculatePogress(progressChanged, (int) mmin, (int) finalAuxMmax, (int) mmax);

                    visibleMont = Tools.formatUserAmount(Integer.toString(progresFinal));

                    //si es campaña, la primera vez que entra, setea monto de campaña
                    if (isTDCAMP) {
                        if (!initalCampain) {
                            initalCampain = true;
                            String gtMonto = getPromocionTDC().getMonto();

                            Log.i("tdc","monto promocion: "+gtMonto);

                            gtMonto = gtMonto.substring(0, gtMonto.length() - 2);
                            gtMonto = Tools.formatUserAmount(gtMonto);

                            edtMontoVisible.setText("");
                            edtMontoVisible.setText(gtMonto);

                        } else
                            edtMontoVisible.setText(visibleMont);
                    } else
                        edtMontoVisible.setText(visibleMont);

                    edtMontoVisible.setCursorVisible(false);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    isFromBar = true;
                    Log.i("simulated","onStopBar, lanza petición" );
                    if (isTDCAMP) {
                        if (!auxisCamp) {
                            auxisCamp = true;
                        }
                    }
//
                    validateProducSimulated();

                }
            });
        }

        String auxMonCamp, decimales;

        if(isTDCAMP) {
            if(getProducto()==null) {
                //Si no tiene oferta por simulador, no es necesario calcular el progreso de la barra, dado que está llena
                //Si tuviera oneclick y simulador, si necesita calcularse el progreso
                seekBarSimulador.setMax(1);
                seekBarSimulador.setProgress(1);
            }else {

                auxMonCamp = getPromocionTDC().getMonto();
                auxMonCamp = auxMonCamp.substring(0, auxMonCamp.length() - 2);
                decimales = auxMonCamp.substring(auxMonCamp.length() - 2, auxMonCamp.length());

                auxMonCamp = Tools.formatUserAmount(auxMonCamp) + "." + decimales;

                updateGraph2(auxMonCamp);
            }
        }
    }

//    para mostrar detalle TDC
    private void validateProducSimulated(){

        if(isTDCAMP) {
            if (!auxisCamp) { //si es campaña y no ha sido modificado, lanza detalle por oneclick
                Log.i("simulated", "TDc, lanza detalle de oneclick");
                delegate.consultaDetalleTDCOneClick();
            } else if (isIndSimProd()) { //si fue modificado, valida si ha sido simulado
                Log.i("simulated","TDC, simulado, elimina y recalcula");
                delegate.saveOrDeleteSimulationRequest(false); //true guarda, false elimina
            } else {
                Log.i("simulated", "TDC, recalcula");
                delegate.doRecalculo();
            }

        }else if (isIndSimProd()) { //Si solo es oferta, valida si ha sido simulado
            Log.i("simulated","TDC, simulado, elimina y recalcula");
            delegate.saveOrDeleteSimulationRequest(false); //true guarda, false elimina
        } else {
            Log.i("simulated", "TDC, recalcula");
            delegate.doRecalculo();
        }
    }



    private void setValuesProduct(){

        String montoMaximo, auxMMax, montoMinimo, auxMMin,
                montoFlow, cveProd, indSim;

        int imgProducto,descProd;


        if (getProducto()!=null) {
            cveProd = getProducto().getCveProd();
            montoMaximo = getProducto().getMontoMaxS();
            montoMinimo = getProducto().getMontoMinS();
            indSim = getProducto().getIndSim();
            imgProducto = GuiTools.getCreditIcon(cveProd, false);
            descProd = GuiTools.getSelectedLabel(cveProd);
            montoFlow = getProducto().getMontoMaxS();   //valor que se tomará como monto máximo para saber si es mayor a 0.

            auxMMin = Tools.formatUserAmount(Tools.validateMont(montoMinimo, true));
            auxMMax = Tools.formatUserAmount(Tools.validateMont(montoMaximo,true));

            if(auxMMin.contains("$"))
                auxMMin = auxMMin.replace("$","");
            if(auxMMax.contains("$"))
                auxMMax = auxMMax.replace("$", "");

            setMontoMax(Tools.clearAmounr(auxMMax));
            setMontMin(Tools.clearAmounr(auxMMin));


            Log.i("simulated","indicador de simulador TDC: "+indSim);
            setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);
            txtvMontoMinimo.setText(auxMMin);
            edtMontoVisible.setMontoMin(getMontMin());
            edtMontoVisible.setText(Tools.formatUserAmount(getMontoMax()));
            edtMontoVisible.setCveProd(producto.getCveProd());
        }else{
            cveProd = getPromocionTDC().getCveCamp();
            imgProducto = GuiTools.getCreditIcon(cveProd, false);
            descProd = GuiTools.getSelectedLabel(cveProd);
            Log.i("tdc","montoPromocion: "+getPromocionTDC().getMonto());
            montoMaximo = getPromocionTDC().getMonto();
            montoMaximo = montoMaximo.substring(0, montoMaximo.length() - 2);
            montoMaximo = Tools.formatUserAmount(montoMaximo);
            Log.i("tdc","montoPromocionFormat: "+montoMaximo);
            auxMMax = montoMaximo.replace("$", "");
            setMontoMax(Tools.clearAmounr(auxMMax));
            txtvMontoMinimo.setVisibility(View.GONE);

            edtMontoVisible.setText(montoMaximo);
        }


        imgvProd.setImageResource(imgProducto);
        txtvDescProd.setText(descProd);
        txtvMontoMaximo.setText(auxMMax);
        edtMontoVisible.setMontoMax(getMontoMax());
    }

    private void validateSO(){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH){
            seekBarSimulador.setThumb(getResources().getDrawable(R.drawable.seek_bar_thumb));
            seekBarSimulador.setProgressDrawable(getResources().getDrawable(R.drawable.custom_seek_bar));
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setThumbOffset(0);
        }
    }

    private void findViews(View vista){
        imgvProd        = (ImageView)vista.findViewById(R.id.imgvProd);
        imgvShowDetalle = (ImageView)vista.findViewById(R.id.imgvShowDetalle);
        imgvShowDetalle.setOnClickListener(this);

        imgvAyuda       = (ImageView)vista.findViewById(R.id.imgvAyuda);
        imgvAyuda.setOnClickListener(this);

        imgvContratarCredito = (ImageButton)vista.findViewById(R.id.imgvContratarCredito);
        imgvContratarCredito.setOnClickListener(this);

        imgvTipoTarjeta      = (ImageView)vista.findViewById(R.id.imgvTipoTarjeta);

        edtMontoVisible    = (LabelMontoTotalCredit)vista.findViewById(R.id.edtMontoVisible);
//        edtMontoVisible.setOnClickListener(this);

        txtvDescProd        = (TextView)vista.findViewById(R.id.txtvDescProd);
        txtvMontoMinimo     = (TextView)vista.findViewById(R.id.txtvMontoMinimo);
        txtvMontoMaximo     = (TextView)vista.findViewById(R.id.txtvMontoMaximo);
        txtvCat             = (TextView)vista.findViewById(R.id.txtvCat);
        txtvOpcion          = (TextView)vista.findViewById(R.id.txtvTipo);
        txtvPlazo           = (TextView)vista.findViewById(R.id.txtPlazo);
        txtvTasa            = (TextView)vista.findViewById(R.id.txtvTasa);
        txtvTipo            = (TextView)vista.findViewById(R.id.txtvTipo);
        txtvMasInfo         = (TextView)vista.findViewById(R.id.txtvMasInfo);
        txtvPagoMMax        = (TextView)vista.findViewById(R.id.txtvPagoMMax);
        txtvMontoMMax       = (TextView)vista.findViewById(R.id.txtvMontoMMax);
        txtvdetalleTDC_ILC  = (TextView)vista.findViewById(R.id.txtvdetalleTDC_ILC);
        txtvResultadoDetalleTDCILC  = (TextView)vista.findViewById(R.id.txtvResultadoDetalleTDCILC);

        lnlDetalleProd      = (LinearLayout)vista.findViewById(R.id.lnlDetalleProd);
        lnlTipo             = (LinearLayout)vista.findViewById(R.id.lnlTipo);
        lnlTipo.setOnClickListener(this);

        lnlPlazo            = (LinearLayout)vista.findViewById(R.id.lnlPlazo);
        lnlPlazo.setOnClickListener(this);

        lnlPlazoTipo        = (LinearLayout)vista.findViewById(R.id.lnlPlazoTipo);
        lnlPagoMMaximo      = (LinearLayout)vista.findViewById(R.id.lnlPagoMMaximo);

        seekBarSimulador    = (SeekBar)vista.findViewById(R.id.seekBarSimulador);

        imgvContratarCredito.setVisibility(View.VISIBLE);
        lnlPlazoTipo.setVisibility(View.GONE);
        lnlPagoMMaximo.setVisibility(View.GONE);
        txtvdetalleTDC_ILC.setVisibility(View.VISIBLE);
        txtvResultadoDetalleTDCILC.setVisibility(View.VISIBLE);

    }

    @Override
    public void onClick(View v) {

        if (v ==  imgvContratarCredito ){
            if(isTDCAMP) {
                if (!auxisCamp) {
//                lanza peticion contratar de oneclcikc
                    Log.i("campaña", "TDC por one click, "+auxisCamp);
                    delegate.iniciaFlujoTDC();
                } else {
                    Log.i("campaña", "TDC por simulador, "+auxisCamp);
                    contratarTDC();
                }
            }else{
                contratarTDC();
            }
        }

        if (v == imgvAyuda) {
            if(getProducto()!=null)
                PopUpAyudaViewController.mostrarPopUp((MenuPrincipalActivitySeekBar)getActivity(),getProducto().getCveProd());
            else
                PopUpAyudaViewController.mostrarPopUp((MenuPrincipalActivitySeekBar)getActivity(),getPromocionTDC().getCveCamp());
        }

        if(v == imgvShowDetalle){

            if(isSimulated){ //si ya fue simulado, muestra detalle
                Log.i("campaña","producto simulado");
                validateLayoutToShow();
            }else{
                Log.w("campaña","producto no simulado");
                isFromBar = false;
                validateProducSimulated();
            }

        }

    }

    public void contratarTDC(){
        delegate.saveOrDeleteSimulationRequest(true);

    }

    public void setSimulation(){
        ((MenuPrincipalActivitySeekBar) getActivity()).isTDCS = true;
        ((MenuPrincipalActivitySeekBar) getActivity()).validateTxtvColor(controller);

        setColor(false);
    }

    public void setColor(boolean isNormal){
        if(isNormal)
            edtMontoVisible.setTextColor(getResources().getColor(R.color.nuevaimagen_tx5));
        else
            edtMontoVisible.setTextColor(getResources().getColor(R.color.naranja));
    }

    public void validateLayoutToShow(){

        //oculta detalle
        if(isShowDetalle()) { //si true, oculta, si false muestra detalle
            setIsShowDetalle(false);
            lnlDetalleProd.setVisibility(View.GONE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_masdetalle);

        }else{

            ((MenuPrincipalActivitySeekBar)getActivity()).isTDCS = true;
            ((MenuPrincipalActivitySeekBar)getActivity()).validateTxtvColor(this);

            ((MenuPrincipalActivitySeekBar)getActivity()).isTDCDetail = true;
            ((MenuPrincipalActivitySeekBar)getActivity()).validateIsShowAnyDetail(this);

            setIsShowDetalle(true);
            lnlDetalleProd.setVisibility(View.VISIBLE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_menosdetalle);


            setDetailProduct();
        }

    }

    public void setDetailProduct(){
        try {
            Producto newProducto = delegate.getProducto();

            String cat, tasa,cveSubProd;

            if(isTDCAMP && !auxisCamp){//si se fue por oneclick
                tasa = getOfertaTDC().getTazaPonderada();
                cat = getOfertaTDC().getCat();
                cveSubProd = getOfertaTDC().getProducto();
            }else {
                tasa = newProducto.getTasaS();
                cat = newProducto.getCATS();
                cveSubProd = newProducto.getSubproducto().get(0).getCveSubp();
            }

            txtvdetalleTDC_ILC.setText(getResources().getString(R.string.seekbar_detalle_tdc));
            imgvTipoTarjeta.setImageDrawable(getTipoTarjeta(cveSubProd));
            txtvResultadoDetalleTDCILC.setText(nombreTarjeta);
            txtvTasa.setText("Tasa de interés: " + tasa + " %");
            txtvCat.setText("CAT: "+cat+"% sin iva");
            imgvTipoTarjeta.setVisibility(View.VISIBLE);

        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public Drawable getTipoTarjeta(String nombre){

        Drawable tipo = null;

        for (int i = 0; i<clavePlat.length; i++){
            if(clavePlat[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_platinum);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Platinum);
                break;
            }
        }

        for (int i = 0; i<claveOro.length; i++){
            if(claveOro[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_oro);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Oro);
                break;
            }
        }

        for (int i = 0; i<claveEduc.length; i++){
            if(claveEduc[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_educacion);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Educacion);
                break;
            }
        }

        for (int i = 0; i<claveAzul.length; i++){
            if(claveAzul[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_azul);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Azul);
                break;
            }
        }

        for (int i = 0; i<claveRayados.length; i++){
            if(claveRayados[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_rayados);
                nombreTarjeta = getResources().getString(R.string.tarjeta_rayados);
                break;
            }
        }

        for (int i = 0; i<claveUnam.length; i++){
            if(claveUnam[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_unam);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Unam);
                break;
            }
        }

        for (int i = 0; i<claveIPN.length; i++){
            if(claveIPN[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_ipn);
                nombreTarjeta = getResources().getString(R.string.tarjeta_IPN);
                break;
            }
        }

        for (int i = 0; i<claveCongelada.length; i++){
            if(claveCongelada[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_congelada);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Congelada);
                break;
            }
        }

        for(int i = 0; i<claveVive.length; i++){
            if(claveVive[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_vive);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Vive);
                break;
            }
        }

        for(int i = 0; i<claveInfitine.length; i++){
            if(claveInfitine[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_infinitie);
                nombreTarjeta = getResources().getString(R.string.tarjeta_infinitie);
                break;
            }
        }

        return tipo;
    }

    public Promociones getPromocionTDC() {
        return promocionTDC;
    }

    public void setPromocionTDC(Promociones promocionTDC) {
        this.promocionTDC = promocionTDC;
    }

    private int calculatePogress(int progress, int min, int progresFinal, int mmax){

        int auxMin = min;

        if (progress == 0){
            return min;
        }else {

            for (int i = 1; i<=progress; i++) {
                auxMin = auxMin + INCREMENTAL;
                if (progress == progresFinal){
                    auxMin = mmax;
                }
            }
        }

        return auxMin;
    }

    public void reset() {
        this.isResetting = true;
        this.typedString=new StringBuffer();
        this.isResetting = false;
    }

    private void setFormattedText(){

        isSettingText = true;
        String text = typedString.toString();

        edtMontoVisible.setText(Tools.formatUserAmount(text));
    }

    public void updateData(boolean isOperationDelete){
        Log.i("updatedata","se manda actualizar productos desde tdc");
        ((MenuPrincipalActivitySeekBar)getActivity()).updateAllProductData(this,isOperationDelete);
    }

    public String getMontoSeleccionado() {
        return montoSeleccionado;
    }

    public void setMontoSeleccionado(String montoSeleccionado) {
        this.montoSeleccionado = montoSeleccionado;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public OfertaTDC getOfertaTDC() {
        return ofertaTDC;
    }

    public void setOfertaTDC(OfertaTDC ofertaTDC) {
        this.ofertaTDC = ofertaTDC;
    }

    public String getMontoMax() {
        return montoMax;
    }

    public void setMontoMax(String montoMax) {
        this.montoMax = montoMax;
    }

    public String getMontMin() {
        return montMin;
    }

    public void setMontMin(String montMin) {
        this.montMin = montMin;
    }

    public boolean isIndSimProd() {
        return indSimProd;
    }

    public void setIndSimProd(boolean indSimProd) {
        this.indSimProd = indSimProd;
    }

    public boolean isShowDetalle() {
        return isShowDetalle;
    }

    public void setIsShowDetalle(boolean isShowDetalle) {
        this.isShowDetalle = isShowDetalle;
    }

    public String getAuxFinalMax() {
        return auxFinalMax;
    }

    public void setAuxFinalMax(String auxFinalMax) {
        this.auxFinalMax = auxFinalMax;
    }

}