package bbvacredit.bancomer.apicredit.gui.delegates;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Hashtable;

import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.gui.activities.PrestamosContratadosViewController;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CreditoContratado;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.Producto;

/**
 * Created by Carlos Garcia on 08/12/2015.
 */
public class PrestamosContratadosDelegate extends BaseDelegateOperacion {
    Context activityContratados;
    private Session session;
    private ArrayList<CreditoContratado> creditosContratados;
    CreditoContratado credito;
    String operationDelivered;
    boolean eliminarLiquidacion;
    private CalculoData data;
    private ArrayList<Producto> arrayProductos;
    boolean isUpper;

    public PrestamosContratadosDelegate() {
        session = Tools.getCurrentSession();
        creditosContratados = session.getCreditos().getCreditosContratados();
    }

    public void setActivityContratados(Context activityContratados) {
        this.activityContratados = activityContratados;
    }

    public Context getActivityContratados() {
        return activityContratados;
    }

    public void botonArriba() {
        ((PrestamosContratadosViewController)getActivityContratados()).botonArriba();
    }

    public void botonAbajo() {
        ((PrestamosContratadosViewController)getActivityContratados()).botonAbajo();
    }

    public ArrayList<CreditoContratado> getCreditosContratados() {
        return creditosContratados;
    }

    public ArrayList<Producto> getArrayProductos() {
        return arrayProductos;
    }

    public void eliminarSimulacionLiquidacionCredito(CreditoContratado aCredito) {
        Log.d("PrestamosContratados","eliminarSimulacionLiquidacionCredito");
        this.credito = aCredito;

        //operationDelivered = Server.CALCULO_ALTERNATIVAS_OPERACION;

        eliminarLiquidacion = true;

        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        // Mapeamos el IUM tomado de la sesion
        addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
        // Mapeamos el numeroCelular tomado de la sesion
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
        // Mapeamos el id de cliente tomado de la sesion
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        // Mapeamos el id de cliente tomado de la sesion
        addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
        // Mapeamos el tipo de operación, se indica que se va a eliminar la simulación
        addParametro(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    public void simularCredito() {
        Log.d("PrestamosContratados","simularCredito");

        eliminarLiquidacion = false;

        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        // Mapeamos el número de contrato
        addParametro(credito.getContrato(), ServerConstantsCredit.CONTRATO_PARAM, paramTable);
        // Mapeamos el tipo de operación, se indica calculo
        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
        // Mapeamos el codigo de operacion
        addParametro(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        // Mapeamos el numeroCelular tomado de la sesion
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
        // Mapeamos el id de cliente tomado de la sesion
        addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        // Mapeamos el IUM tomado de la sesion
        addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
        // Mapeamos el Pago Mensual
        addParametroObligatorio(credito.getPagoMen(), ServerConstantsCredit.PAGO_MENSUAL_PARAM, paramTable);

        Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(Server.CALCULO_OPERACION, paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    @Override
    protected String getCodigoOperacion() {
        // TODO Auto-generated method stub
        return Server.CALCULO_ALTERNATIVAS_OPERACION;
    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
        }
    }


    public void analyzeResponse(String operationId, ServerResponse response) {

        if ((response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)) {

            Log.d("analyzeResponse", "Respuesta de petición Liquidaciones");

            if (Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId) && eliminarLiquidacion) {
                Log.d("PrestamosContratados","hay que simular crédito");
                simularCredito();
            } else if (Server.CALCULO_OPERACION.equals(operationId) && !eliminarLiquidacion) {
                Log.d("PrestamosContratados","crédito simulado");
                data = (CalculoData) response.getResponse();

                if (data == null)
                    Log.e("Error", "No existe información");

                refrescarObjetoSessionConCalculoResult(data);

                this.arrayProductos = data.getProductos();

                for(Producto p:arrayProductos) {
                    String codigoProducto = p.getCveProd();

                    if(codigoProducto.equalsIgnoreCase(ConstantsCredit.TARJETA_CREDITO))
                        p.setDesProd(ConstantsCredit.TARJETA_CREDITO_LBL);
                    else if(codigoProducto.equalsIgnoreCase(ConstantsCredit.INCREMENTO_LINEA_CREDITO))
                        p.setDesProd(ConstantsCredit.INCREMENTO_LINEA_CREDITO_LBL);
                    else if(codigoProducto.equalsIgnoreCase(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO))
                        p.setDesProd(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO_LBL);
                    else if(codigoProducto.equalsIgnoreCase(ConstantsCredit.CREDITO_NOMINA))
                        p.setDesProd(ConstantsCredit.CREDITO_NOMINA_LBL);
                    else if(codigoProducto.equalsIgnoreCase(ConstantsCredit.CREDITO_AUTO))
                        p.setDesProd(ConstantsCredit.CREDITO_AUTO_LBL);
                    else if(codigoProducto.equalsIgnoreCase(ConstantsCredit.CREDITO_HIPOTECARIO) ||
                            codigoProducto.equalsIgnoreCase("HIPO"))
                        p.setDesProd(ConstantsCredit.CREDITO_HIPOTECARIO_LBL);
                    /*switch(claveProd) {
                        case "0TDC":
                            Log.d("responseSimular", "Producto: TDC");
                            p.setDesProd(ConstantsCredit.TARJETA_CREDITO_LBL);
                            break;
                        case "0ILC":
                            Log.d("responseSimular", "Producto: ILC");
                            p.setDesProd(ConstantsCredit.INCREMENTO_LINEA_CREDITO_LBL);
                            break;
                        case "0PPI":
                            Log.d("responseSimular","Producto: PPI");
                            p.setDesProd(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO_LBL);
                            break;
                        case "0NOM":
                            Log.d("responseSimular","Producto: Nomina");
                            p.setDesProd(ConstantsCredit.CREDITO_NOMINA_LBL);
                            break;
                        case "AUTO":
                            Log.d("responseSimular","Producto: Automotriz");
                            p.setDesProd(ConstantsCredit.CREDITO_AUTO_LBL);
                            break;
                        case "0HIP":

                        case "HIPO":
                            Log.d("responseSimular","Producto: Hipotecario");
                            p.setDesProd(ConstantsCredit.CREDITO_HIPOTECARIO_LBL);
                            break;
                    }*/

                }

                ((PrestamosContratadosViewController)activityContratados).muestraSimLiquidacion();
            }
        }
    }

    private void refrescarObjetoSessionConCalculoResult(CalculoData calculoData) {
        session.getCreditos().setMontotSol(calculoData.getMontotSol());
        session.getCreditos().setPorcTotal(calculoData.getPorcTotal());
        session.getCreditos().setPagMTot(calculoData.getPagMTot());

        //ArrayList<Producto> listaProductosTemp = session.getCreditos().getProductos();

        session.getCreditos().setProductos(calculoData.getProductos());

        //Se mantiene el valor del tick, aunque me venga desactivado
        //for (Producto producto:listaProductosTemp) {
            //obtenerProductoOfertadoDeSession(producto.getCveProd()).set
        //}

    }

    /*private Producto obtenerProductoOfertadoDeSession(String cveProducto) {
        Producto productoEncontrado = null;

        for(Producto producto: session.getCreditos().getProductos()) {
            if(producto.getCveProd().equalsIgnoreCase(cveProducto))
                productoEncontrado = producto;
        }

        return productoEncontrado;
    }*/

    public void botonIzquierda() {
        Log.d("Contratados", "PrestamosContratadosDelegate - botonIzquierda");
        if(isUpper)
            botonArriba();
    }

    public void botonDerecha() {
        Log.d("Contratados", "PrestamosContratadosDelegate - botonDerecha");
        if(isUpper)
            botonArriba();
    }

    public void setArriba(boolean arriba) {
        isUpper = arriba;
    }

    public boolean getArriba(){
        return isUpper;
    }
}
