package bbvacredit.bancomer.apicredit.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.CreditJSONAble;

import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.io.ParsingHandlerJSON;

public class ConsultaCorreoData implements ParsingHandlerJSON, CreditJSONAble {
	
	private String estado;
	
	private String correoclie;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCorreoclie() {
		return correoclie;
	}


	public void setCorreoclie(String correoclie) {
		this.correoclie = correoclie;
	}

	// OLD
	@Override
	public void processJSON(String jsonString) throws JSONException {
		JSONObject obj = new JSONObject(jsonString);
		
		this.estado = Tools.getJSONChecked(obj,"estado", String.class);//obj.getString("estado");
		this.correoclie = Tools.getJSONChecked(obj,"correoclie", String.class);//obj.getString("correoclie");
	}

	@Override
	public void fromJSON(String jsonString) {
		// TODO Auto-generated method stub
		try{
			JSONObject obj = new JSONObject(jsonString);
			
			this.estado = Tools.getJSONChecked(obj,"estado", String.class);//obj.getString("estado");
			this.correoclie = Tools.getJSONChecked(obj,"correoclie", String.class);//obj.getString("correoclie");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e(this.getClass().toString(), "Error en fromJSON");
			e.printStackTrace();
		}
	}

	@Override
	public String toJSON() {
		// TODO Auto-generated method stub
		return null;
	}

}
