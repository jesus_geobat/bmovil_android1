package bbvacredit.bancomer.apicredit.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.CreditJSONAble;

import java.util.ArrayList;

import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.io.ParsingHandlerJSON;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;

public class CalculoData  implements ParsingHandlerJSON, CreditJSONAble {
	
	private String idRev;

	private String estado;
	
	private String montotSol;
	
	private Integer porcTotal;
	
	private String pagMTot;
	
	private String indAceptar;
	
	private ArrayList<Producto> productos;

	private ArrayList<Credito> creditos;

	private ArrayList<CreditoContratado> creditosContratados;

	public CalculoData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CalculoData(String estado, String montotSol, Integer porcTotal,
			String pagMTot, String indAceptar, ArrayList<Producto> productos,
			ArrayList<Credito> creditos, ArrayList<CreditoContratado> creditosContratados) {
		super();
		this.estado = estado;
		this.montotSol = montotSol;
		this.porcTotal = porcTotal;
		this.pagMTot = pagMTot;
		this.indAceptar = indAceptar;
		this.productos = productos;
		this.creditos = creditos;
		this.creditosContratados = creditosContratados;
	}

	public String getIdRev() {
		return idRev;
	}

	public void setIdRev(String idRev) {
		this.idRev = idRev;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMontotSol() {
		return montotSol;
	}

	public void setMontotSol(String montotSol) {
		this.montotSol = montotSol;
	}

	public Integer getPorcTotal() {
		return porcTotal;
	}

	public void setPorcTotal(Integer porcTotal) {
		this.porcTotal = porcTotal;
	}

	public String getPagMTot() {
		return pagMTot;
	}

	public void setPagMTot(String pagMTot) {
		this.pagMTot = pagMTot;
	}

	public String getIndAceptar() {
		return this.indAceptar;
	}

	public void setIndAceptar(String indAceptar) {
		this.indAceptar = indAceptar;
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}

	public ArrayList<Credito> getCreditos() {
		return creditos;
	}

	public void setCreditos(ArrayList<Credito> creditos) {
		this.creditos = creditos;
	}

	private ArrayList<Plazo> parsePlazos(JSONArray plazoJArray){
		ArrayList<Plazo> lista = new ArrayList<Plazo>();
				
		try {
			JSONObject item;
			Plazo p;
			for (int i = 0; i < plazoJArray.length(); i++) {
					item = plazoJArray.getJSONObject(i);
					
					p = new Plazo();
					p.setCvePlazo(Tools.getJSONChecked(item, "CvePlazo", String.class));
					p.setDesPlazo(Tools.getJSONChecked(item, "DesPlazo", String.class));
					lista.add(p);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lista;
	}
	
	private ArrayList<Subproducto> parseSubproductos(JSONArray subproductosJArray){
		ArrayList<Subproducto> lista = new ArrayList<Subproducto>();
		
		try {
			JSONObject item;
			Subproducto pr;
			for (int i = 0; i < subproductosJArray.length(); i++) {
					item = subproductosJArray.getJSONObject(i);
					
					pr = new Subproducto();
					pr.setCveSubp(Tools.getJSONChecked(item, "CveSubp", String.class));
					pr.setDesSubp(Tools.getJSONChecked(item, "DesSubp", String.class));
					pr.setNumTDC(Tools.getJSONChecked(item, "NumTDC", String.class));
					pr.setMonMax(Tools.getJSONChecked(item, "MonMax", Integer.class));
					pr.setMonMin(Tools.getJSONChecked(item, "MonMin", Integer.class));
					pr.setTasa(Tools.getJSONChecked(item, "Tasa", Integer.class));
					pr.setCAT(Tools.getJSONChecked(item, "CAT", Double.class));
					pr.setFechCat(Tools.getJSONChecked(item, "FechCat", String.class));

					JSONArray plazoJArray = Tools.getJSONChecked(item,"Plazo", JSONArray.class);
					pr.setPlazo(parsePlazos(plazoJArray));
					
					lista.add(pr);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lista;
		
	}
	
	private ArrayList<Producto> parseProductos(JSONArray productosJArray){

		ArrayList<Producto> lista = null;

		if(productosJArray != null){
			try {
				lista = new ArrayList<Producto>();
				JSONObject item;
				Producto pr;
				String cveProd, auxMontMin, auxMontMax;
				for (int i = 0; i < productosJArray.length(); i++) {
						item = productosJArray.getJSONObject(i);
						
					pr = new Producto();
				   	cveProd = Tools.getJSONChecked(item,"CveProd",String.class);

					pr.setCveProd(Tools.getJSONChecked(item, "CveProd", String.class));
					pr.setCveProd(cveProd);
					pr.setDesProd(Tools.getJSONChecked(item, "DesProd", String.class));
					pr.setParainfo(Tools.getJSONChecked(item, "ParaInfo", String.class));
					pr.setSubprodE(Tools.getJSONChecked(item, "SubprodE", String.class));
					pr.setDessubpE(Tools.getJSONChecked(item, "DessubpE", String.class));
					pr.setCvePzoE(Tools.getJSONChecked(item, "CvePzoE", String.class));
					pr.setDesPlazoE(Tools.getJSONChecked(item, "DesPlazoE", String.class));
					pr.setMontoMaxS(Tools.getJSONChecked(item, "MontoMaxS", String.class));
					pr.setMontoMinS(Tools.getJSONChecked(item, "MontoMinS", String.class));
					pr.setNumTDCS(Tools.getJSONChecked(item, "NumTDCS", String.class));
					pr.setMontoDeseS(Tools.getJSONChecked(item, "MontoDeseS", String.class));
					pr.setPagMProdS(Tools.getJSONChecked(item, "PagMProdS", String.class));
					pr.setPorcProdS(Tools.getJSONChecked(item, "PorcProdS", String.class));
					pr.setTasaS(Tools.getJSONChecked(item, "TasaS", String.class));
					pr.setCATS(Tools.getJSONChecked(item, "CATS", String.class));
					pr.setFechaCatS(Tools.getJSONChecked(item, "fechaCatS", String.class));
					pr.setIndSim(Tools.getJSONChecked(item, "IndSim", String.class));
					pr.setIndInfo(Tools.getJSONChecked(item, "IndInfo", String.class));
					pr.setMensaje(Tools.getJSONChecked(item, "Mensaje", String.class));
					pr.setIndRev(Tools.getJSONChecked(item, "IdRev", String.class));
					pr.setTasa(Tools.getJSONChecked(item, "Tasa", String.class));
					pr.setTasaOri(Tools.getJSONChecked(item, "TASAORIS",String.class));
					pr.setComision(Tools.getJSONChecked(item, "comision",String.class));

					JSONArray subProductosJArray = Tools.getJSONChecked(item, "Subproducto", JSONArray.class);
					pr.setSubproducto(parseSubproductos(subProductosJArray));

					if(cveProd.equalsIgnoreCase(ConstantsCredit.ADELANTO_NOMINA)) {
						auxMontMin = Integer.toString(pr.getSubproducto().get(0).getMonMin());
						auxMontMax = Integer.toString(pr.getSubproducto().get(0).getMonMax());

						Log.e("adelantos","montoMin: "+auxMontMin);
						Log.e("adelantos","montoMin: "+auxMontMax);
						//si monto máximo es mayor a 200 y monto min menor a 200, nuevo valor de monto min será 200
						if(Float.parseFloat(auxMontMax)>=200) {
							pr.getSubproducto().get(0).setMonMin(200);
							pr.setMontoMinS("200.00");
							Log.i("adelantos","adelanto, montomax > 200, y monto min <200, montmin = 200");
							lista.add(pr);
						}else{
							pr.getSubproducto().get(0).setMonMin(0);
							pr.setMontoMinS("0.00");
							lista.add(pr);
							Log.i("adelantos","anom no visible");
						}

						Log.e("adelantos", "CveProd: " + cveProd);
						Log.e("adelantos", "montomin: " + auxMontMin);
					}

					if(!cveProd.equalsIgnoreCase(ConstantsCredit.ADELANTO_NOMINA))
						lista.add(pr);
				}
				System.out.println("Mario Lista: "+lista);
				Log.i("MArio Lista: ", lista.toString());
	
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return lista;
	}
	
	private ArrayList<Credito> parseCreditos(JSONArray creditosJArray){

		ArrayList<Credito> lista = null;

		if(creditosJArray != null){
			try {
				lista = new ArrayList<Credito>();
				JSONObject item;
				Credito cr;
				for (int i = 0; i < creditosJArray.length(); i++) {
						item = creditosJArray.getJSONObject(i);
						
						cr = new Credito();
						cr.setContrato(Tools.getJSONChecked(item,"Contrato",String.class));
						
						lista.add(cr);
				}
	
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return lista;
	}

	// OLD
	@Override
	public void processJSON(String jsonString) throws JSONException {
		
		JSONObject obj = new JSONObject(jsonString);
		
		this.estado =  obj.getString(ServerConstantsCredit.JSON_ESTADO_RESPUESTA);
		this.montotSol = obj.getString("MontotSol");
		this.porcTotal = obj.getInt("PorcTotal");
		this.pagMTot = obj.getString("PagMTot"); 
		this.indAceptar = obj.getString("IndAceptar");
		
		JSONArray productosJArray = obj.getJSONArray("Productos");
		this.productos = parseProductos(productosJArray);
		
		JSONArray creditosJArray = obj.getJSONArray("Creditos");
		this.creditos = parseCreditos(creditosJArray);
		
		// TODO: Duda
	}

	@Override
	public void fromJSON(String jsonString) {
		
		JSONObject obj;
		try {
			
			obj = new JSONObject(jsonString);
			this.estado =  Tools.getJSONChecked(obj, ServerConstantsCredit.JSON_ESTADO_RESPUESTA,String.class); //obj.getString(ServerConstants.JSON_ESTADO_RESPUESTA);
			this.montotSol = Tools.getJSONChecked(obj, "MontotSol", String.class); //obj.getDouble("MontotSol");
			this.porcTotal = Tools.getJSONChecked(obj, "PorcTotal", Integer.class); //obj.getInt("PorcTotal");
			this.pagMTot = Tools.getJSONChecked(obj,"PagMTot", String.class); //obj.getDouble("PagMTot");
			this.indAceptar = Tools.getJSONChecked(obj, "IndAceptar",String.class); //obj.getString("IndAceptar");
			
			JSONArray productosJArray = Tools.getJSONChecked(obj,"Productos", JSONArray.class); //obj.getJSONArray("Productos");
			this.productos = parseProductos(productosJArray);
			
			JSONArray creditosJArray = Tools.getJSONChecked(obj,"Creditos", JSONArray.class); //obj.getJSONArray("Creditos");
			this.creditos = parseCreditos(creditosJArray);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public String toJSON() {
		// TODO Auto-generated method stub
		return null;
	}

}
