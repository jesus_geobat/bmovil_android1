package bbvacredit.bancomer.apicredit.common;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.bouncycastle.crypto.CreditDigest;
import org.bouncycastle.crypto.digests.CreditMD5Digest;
import org.bouncycastle.util.encoders.CreditHex;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import bancomer.api.common.commons.ICommonSession;
import bancomer.api.consumo.io.Server;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.MenuPrincipalActivity;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;


public class Tools {

	public final static int TAM_FRAGMENTO_TEXTO = 1000;

	/**
	 * Builds the MD5 code for the user.
	 * @param username the username
	 * @param pass the user password
	 * @return the MD5 code for the user
	 */
	public static String buildMD5Pass(String username, String pass) {

		StringBuffer sb;

		sb = new StringBuffer(username);
		sb.append(pass);

		String input = sb.toString();

		CreditDigest digest = new CreditMD5Digest();
		byte[]  resBuf = new byte[digest.getDigestSize()];
		byte[]  bytes = input.getBytes();
		digest.update(bytes, 0, bytes.length);
		digest.doFinal(resBuf, 0);

		String output = new String(CreditHex.encode(resBuf)).toUpperCase();

		return output;

	}

	//BBVA CREDIT INTEGRATION

	/** Modificacion 50986
	 * Los siguientes metodos ya no son necesarios ya que en la respuesta de DetalleAlternativa
	 * ya vienen estipulados los datos que se obtenian con estos metodos.
	 */

    /*
	public static String formatterForBmovil(String importe)
	{
		StringBuffer buffer = new StringBuffer();
		boolean flag=false;
		int counter = -1;
		for(int i=0;i<importe.length();i++)
		{
			if(importe.charAt(i)!='.')	
				buffer.append(importe.charAt(i));
			else
				flag=true;
			if(flag)
				counter++;
		}
		switch(counter)
		{
		case -1: buffer.append("00");
		break;
		case 1:	buffer.append("0");
		break;
		}
		return buffer.toString();
	}
    */


	//public static ArrayList<ConsumoCredit> getJsonCreditParsed()
	//{

	/**
	 * Created: April 22th, 2015.
	 * Author: Omar Orozco Silverio.
	 *
	 * This was the original Json for Consumo Task but it has no "CveSubp" value which matches
	 * with CveSubP value from DetalleArlternativa Response.

	 final String JsonConsumo = "{\"Consumo\":["
	 + "{\"producto\":\"PBCCMNOM02\",\"CveSubp\":\"CN61\",\"pagoMilS\":\"11.88\"},"
	 + "{\"producto\":\"PBCCMNOM01\",\"CveSubp\":\"CU87\",\"pagoMilS\":\"16.24\"},"
	 + "{\"producto\":\"PBCCMPPI02\",\"CveSubp\":\"CP95\",\"pagoMilS\":\"11.68\"},"
	 + "{\"producto\":\"PBCCMPPI02\",\"CveSubp\":\"CP45\",\"pagoMilS\":\"16.24\"}"
	 + "]}";

	 */

        /*
		final String JsonConsumo = "{\"Consumo\":["
				+ "{\"producto\":\"PBCCMNOM02\",\"CveSubp\":\"CN61\",\"pagoMilS\":\"11.88\"},"
				+ "{\"producto\":\"PBCCMNOM01\",\"CveSubp\":\"CU86\",\"pagoMilS\":\"16.24\"},"
				+ "{\"producto\":\"PBCCMPPI02\",\"CveSubp\":\"CP95\",\"pagoMilS\":\"11.68\"},"
				+ "{\"producto\":\"PBCCMPPI02\",\"CveSubp\":\"CP45\",\"pagoMilS\":\"16.24\"}"
				+ "]}";

		try {
			JSONObject mainJson = new JSONObject(JsonConsumo);
			JSONArray productos = mainJson.getJSONArray(ConstantsCredit.JSON_CONSUMO_TAG);
			ArrayList<ConsumoCredit> productsList = new ArrayList<ConsumoCredit>();


			ConsumoCredit auxInstance;
			JSONObject auxJson;


			for(int i=0;i<productos.length();i++)
			{
				auxInstance = new ConsumoCredit();
				auxJson = productos.getJSONObject(i);

				auxInstance.setCveSubP(auxJson.getString(ConstantsCredit.CVESUBP));
				auxInstance.setPagMilS(auxJson.getString(ConstantsCredit.PAGOMILS));
				auxInstance.setProducto(auxJson.getString(ConstantsCredit.PRODUCTO));

				productsList.add(auxInstance);
			}

			return productsList;

		} catch (JSONException e) {			
			Log.e("Problem caused when Creating Json, reason:\n",e.getLocalizedMessage());
		}


		return null;
	}
    */
	/*
	public static String getImporteDelSeguro(String pagoMilS, String MonDeseS)
	{
		// Created: April 22th, 2015. Author:OOS.
		return String.valueOf(Float.valueOf(pagoMilS)*Float.valueOf(MonDeseS)/1000);
	}
	*/


    /*
	public static String getTotalPagos(String desPlazoE)
	{
		// Created: April 22th, 2015. Author:OOS.
		StringBuffer buffer = new StringBuffer();

		loop : for(int i=0;i<desPlazoE.length();i++)
		{
			switch(desPlazoE.charAt(i))
			{
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				buffer.append(desPlazoE.charAt(i));
				break;
			default :
				break loop;
			}
		}
		return buffer.toString();
	}
	*/
	//END REGION


	/**
	 * Mask the username (the telephone number), with a mask character and only.
	 * letting visible a certain number of digits
	 * @param username the telephone number
	 * @return a text containing the fist digits of the number masked with a
	 * character, and the last digits in clear
	 */
	public static String hideUsername(String username) {
		String result = "";
		if ((username != null) && (username.length() > ConstantsCredit.VISIBLE_NUMBER_CHARCOUNT)) {
			StringBuffer sb = new StringBuffer();
			// mask the first characters of the username with "*" and leave visible the
			// VISIBLE_USERNAME_CHARCOUNT last characters
			for (int i = 0; i < username.length() - ConstantsCredit.VISIBLE_NUMBER_CHARCOUNT; i++) {
				sb.append("*");
			}
			sb.append(username.substring(username.length() - ConstantsCredit.VISIBLE_NUMBER_CHARCOUNT));
			result = sb.toString();
		}
		return result;
	}

	/**
	 * Hide part of the account number, showing the first digits of the account as
	 * an asterisk (*), and only letting visible a certain number of digits. Example:
	 * Account 12345678901234567890 would turn into *7890
	 * @param account the account number
	 * @return a string with the masked account number
	 */
	public static String hideAccountNumber(String account) {
		String result = "";
		if ((account != null) && (account.length() > ConstantsCredit.VISIBLE_NUMBER_ACCOUNT)) {
			StringBuffer sb = new StringBuffer("*");
			sb.append(account.substring(account.length() - ConstantsCredit.VISIBLE_NUMBER_ACCOUNT));
			result = sb.toString();
		} else {
			if (account.length() == ConstantsCredit.VISIBLE_NUMBER_ACCOUNT) {
				result = account;
			}
		}

		return result;
	}

	/**
	 * Determines if a string is empty or null
	 * @param text the string object to evaluate
	 * @return true if the string object has no real value
	 */
	public static boolean isEmptyOrNull(String text){
		return text==null || text.trim().length() == 0;
	}

	/**
	 *
	 * Parsea una fecha a Date dado un formato
	 *
	 * @param inDate
	 * @param format
	 * @return
	 * @throws ParseException
	 */

	public static Date getDate(String inDate, String format) throws ParseException {
		SimpleDateFormat s = new SimpleDateFormat(format);
		return s.parse(inDate);
	}

	public static String getFormatedDate(Date inDate, String format) throws ParseException {
		SimpleDateFormat s = new SimpleDateFormat(format);
		return s.format(inDate);
	}

	public static void trazaTexto(String titulo, String mensaje) {
		Log.d(titulo, "");
		for (int i = 0; i < (mensaje.length() / TAM_FRAGMENTO_TEXTO) + 1; i++) {
			Log.d("", mensaje.substring(i * TAM_FRAGMENTO_TEXTO, Math.min((i + 1) * TAM_FRAGMENTO_TEXTO, mensaje.length())));
		}
	}

	/**
	 * Build the IUM (unique identifier of the installation).
	 * @param username the username
	 * @param seed the random seed
	 * @param applicationContext application context to retrieve phone data
	 * @return the IUM as a hexadecimal string
	 */
	public static String buildIUM(String username, long seed, Context applicationContext) {

		StringBuffer sb;

		if(username != null)
			sb = new StringBuffer(username);
		else
			sb = new StringBuffer();

		sb.append(seed);
		String imei = getImei(applicationContext);
		if (imei != null) {
			sb.append(imei);
		}
		String imsi = getImsi(applicationContext);
		if (imsi != null) {
			sb.append(imsi);
		}

		String props = getSystemProperties();
		if (props != null) {
			sb.append(props);
		}

		String input = sb.toString();

		CreditDigest digest = new CreditMD5Digest();
		byte[]  resBuf = new byte[digest.getDigestSize()];
		byte[]  bytes = input.getBytes();
		digest.update(bytes, 0, bytes.length);
		digest.doFinal(resBuf, 0);

		String output = new String(CreditHex.encode(resBuf)).toUpperCase();

//		return output;
		return "868236F841A444FBEC54848E7404859F";  //5528790201

	}

	/**
	 * Try to obtain the IMEI from the telephone.
	 * @param applicationContext application context to retrieve phone data
	 * @return the IMEI from the telephone, or null if it cannot be obtained
	 */
	private static String getImei(Context applicationContext) {
		TelephonyManager telephonyManager =
				(TelephonyManager) applicationContext.getSystemService(Context.TELEPHONY_SERVICE);
		String imei = telephonyManager.getDeviceId();
		if (TextUtils.isEmpty(imei)) {
			imei = "";
		}
		return imei;
	}

	// TODO: PENDING, Must check in real device -- folvera
	/**
	 * Try to obtain the IMSI from the telephone SIM card.
	 * @param applicationContext application context to retrieve phone data
	 * @return the IMSI from the telephone SIM card, or null if it cannot be obtained
	 */
	private static String getImsi(Context applicationContext)  {
		TelephonyManager telephonyManager =
				(TelephonyManager) applicationContext.getSystemService(Context.TELEPHONY_SERVICE);
		String imsi = telephonyManager.getSubscriberId();
		if (imsi != null) {
			return "misdn:" + imsi;
		} else {
			return null;
		}
	}

	/**
	 * .
	 * @return SOMETHING.
	 */
	private static String getSystemProperties() {
		StringBuffer result = new StringBuffer();
		try {
			result.append(System.getProperty("java.runtime.name"));
		} catch (Throwable th) { };
		try {
			result.append(System.getProperty("os.version"));
		} catch (Throwable th) { };
		try {
			result.append(System.getProperty("java.vm.name"));
		} catch (Throwable th) { };
		try {
			result.append(System.getProperty("java.runtime.version"));
		} catch (Throwable th) { };
		try {
			result.append(System.getProperty("java.vm.version"));
		} catch (Throwable th) { };


		System.out.println("System Properties = " + result.toString());

		return result.toString();
	}

	/**
	 * Si es un campo del json, devuelve el parseo a la clase, sino se devuelve null
	 *
	 * @param json
	 * @param campo
	 * @param clase
	 * @return
	 * @throws JSONException
	 */
	public static <T> T getJSONChecked(JSONObject json, String campo, Class<T> clase) throws JSONException {
		if(json.has(campo)){
			T ret = null;
			if(clase.equals(String.class)){
				// String
				ret = (T) json.getString(campo);
			}else if(clase.equals(Double.class)){
				// Double
				ret = (T) Double.valueOf(json.getDouble(campo));
			}else if(clase.equals(Integer.class)){
				// Integer
				ret = (T) Integer.valueOf(json.getInt(campo));
			}else if(clase.equals(JSONArray.class)){
				// JSONArray
				ret = (T) json.getJSONArray(campo);
			}else if(clase.equals(Boolean.class)){
				// Boolean
				ret = (T) Boolean.valueOf(json.getBoolean(campo));
			}

			return ret;
		}else{
			if(clase.equals(Boolean.class)){
				// Boolean
				return (T) Boolean.valueOf(false);
			}else{
				return null;
			}
		}
	}

	/**
	 * Return the current session
	 * @return
	 */
	public static Session getCurrentSession(){
		return Session.getInstance(MainController.getInstance().getContext());
	}

	public static void setIsContratacionPreference(Boolean isContratacion){
		SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
		Editor editor = sp.edit();
		editor.putBoolean(ConstantsCredit.SHARED_IS_CONTRATACION, isContratacion);
		editor.commit();
	}

	public static Boolean getIsContratacionPreference(){
		SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
		return sp.getBoolean(ConstantsCredit.SHARED_IS_CONTRATACION, false);
	}


	//agm BBVA Credit
	public static float getPosicionBarraInferiorS(ImageButton button)
	{


		float h=button.getHeight();
		float y=button.getY();
		float s=(float) ((h*1.5)+y);

		Log.d("posicicion", " entero  " + s);
		return s;


	}


	public static float getPosicionBarraInferiorB(LinearLayout button)
	{
		float h=button.getHeight();
		float y=button.getY();
		float b=y+h;
		return b;
	}



	public static String getCurrency(float amount)
	{
		NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.getDefault());
		return formatter.format(amount);
	}

	private static String formatPositiveIntegerAmount(String amount) {
		StringBuffer result = new StringBuffer();
		if (amount != null) {
			int size = amount.length();
			int remaining = size % 3;
			if (remaining == 0) {
				remaining = 3;
			}
			int start = 0;
			for (int end = remaining; end <= size; end += 3) {
				result.append(amount.substring(start, end));
				if (end < size) {
					result.append(',');
				}
				start = end;
			}
		}
		return result.toString();
	}


	public static String formatAmount(String amount, boolean negative) {
		amount = amount + "00";
		StringBuffer result = new StringBuffer();
		if (amount != null) {
			result.append('$');
			if (negative) {
				if(amount.startsWith("-"))
					amount = amount.substring(1);
				result.append('-');
			}
			int size = amount.length();
			switch (size) {
				case 0:
					result.append("0.00");
					break;
				case 1:
					result.append("0.0");
					result.append(amount);
					break;
				case 2:
					result.append("0.");
					result.append(amount);
					break;
				default:
					String intPart = amount.substring(0, size - 2);
					String decPart = amount.substring(size - 2);
					result.append(formatPositiveIntegerAmount(intPart));
					result.append('.');
					result.append(decPart);
					break;
			}
		}
		return result.toString();
	}

	public static String formatAmount2(String monto) {
		DecimalFormat formateador = new DecimalFormat("##,###,###.00");
		String resultado = "$" + formateador.format(Double.parseDouble(monto));
		return resultado;
	}

	public static String formatCredit(String monto){
		String format = "";
		int aux = monto.length();
		if(aux <= 8) {
			format = monto.substring(0, (aux - 5))
					+ ","
					+ monto.substring((aux - 5), (aux - 2))
					+ "."
					+ monto.substring((aux - 2), monto.length());
		} else if( aux > 8){
			format = monto.substring(0, (aux - 8))
					+ ","
					+ monto.substring((aux - 8) , (aux - 5))
					+ ","
					+ monto.substring((aux - 5), (aux - 2))
					+ "."
					+ monto.substring((aux - 2), aux);
		}
		return format;
	}

	public static String formatAmount(String monto){
		String format = "";
		int aux = monto.length();
		format = monto.substring( 0, (aux - 2))
				+ "."
				+ monto.substring((aux - 2), monto.length());
		return format;
	}

	public static String formatSplitAmount(String monto){
		String format = "";
		int aux = monto.length();
		String[] parts = monto.split(",");
		if(aux <= 6){
			format = parts[0];
		} else if(aux > 6 && aux <= 10) {
			format = parts[0] + parts[1];
		} else if( aux > 10){
			format = parts[0]+parts[1]+parts[2];
		}

		return format;
	}


	public static String formatSplitReverse(String monto){
		String format = "000";
		String parts[] = monto.split("\\.");
		format = parts[0]+parts[1];



		return format;
	}

	public static String formatCreditPoc(String monto){
		String format = "";

		if(monto.startsWith("$"))
			monto = monto.substring(1,monto.length());

		int aux = monto.length();
		String[] dec = monto.split("\\.");

		String[] parts = dec[0].split(",");
		if(aux <= 6){
			format = parts[0];
		} else if(aux > 6 && aux <= 10) {
			format = parts[0] + parts[1];
		} else if( aux > 10){
			format = parts[0]+parts[1]+parts[2];
		}

		return format;
	}

	public static String validateMont(String max, String min, String mont, Context context){
		String value = "";

		if(max.contains(","))
			max = max.replace(",", "");

		if(min.contains(","))
			min = min.replace(",","");

		if(mont.contains(","))
			mont = mont.replace(",","");

		if (mont.contains("$"))
			mont = mont.replace("$","");


		try {
			Log.i("compare","mont: "+mont);
			Log.i("compare","montoMax: "+max);
			Log.i("compare","montoMin: "+min);
			if(Integer.parseInt(mont) > Integer.parseInt(max)) //si es mayor
				value = ConstantsCredit.MONTO_MAYOR;
			else if (Integer.parseInt(mont)< Integer.parseInt(min))
				value = ConstantsCredit.MONTO_MENOR;
			else
				value = ConstantsCredit.MONTO_EN_RANGO;

		}catch (Exception ex){
			ex.printStackTrace();
		}

		return value;
	}

	public static void showAlert(String mensaje, Context context){
		new AlertDialog.Builder(context)
				.setTitle("Alert")
				.setMessage(mensaje)
				.setPositiveButton("Seleccionar", new DialogInterface.OnClickListener() {
					@TargetApi(11)
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				}).show();
	}

	public static String validateValueMonto(String monto){
		String auxMonto = monto;

		if(auxMonto.contains(",")){
			auxMonto = auxMonto.replace(",","");
			Log.i("monto","auxMonto: "+auxMonto);
		}

		if(auxMonto.endsWith(".00")) {
			auxMonto = auxMonto.replace(".00", "");
			Log.i("monto", "auxMonto2: " + auxMonto);
		}

		if(auxMonto.contains(".")){
			auxMonto = auxMonto.replace(".","");
		}

		return auxMonto;
	}

	public static boolean readURL(String url) {
		boolean resp = true;
		Log.i("opinator","readURLNOMINA");

		Session session = getCurrentSession();
		String numCel = session.getNumCelular();
//		String url = Server.BASE_OPINATOR+Server.OPINATOR_ID+"="+numCel+"&"+Server.OPINATOR_DESIGNATOR+"="+Server.CONSUMO_ILC_TDC_EXITO;

		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();

		HttpGet httpGet = new HttpGet(url);

		android.util.Log.d("opinator", "urlConsultaOpinator: " + url);

		try {
			HttpResponse response = client.execute(httpGet);

			android.util.Log.i("opinator","response: "+response);

			StatusLine statusLine = response.getStatusLine();

			int statusCode = statusLine.getStatusCode();

			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));


				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				MenuPrincipalActivity.getInstance().setRespuestaOpinator(builder.toString());
			} else {
				android.util.Log.e("opinator", "Failed to download data");
				resp =  false;
			}


		} catch (ClientProtocolException e) {
			e.printStackTrace();
			resp = false;
		} catch (IOException e) {
			e.printStackTrace();
			resp =  false;
		}

		return resp;
	}

	/**
	 * format an amount entered by the user to show the decimal and thousand separators.
	 * Examples:
	 *  22 returns 22.00
	 * 22.3 returns 22.30
	 * 1230 returns 1,230.00
	 * 12345677.8 returns 1,234,677.80
	 * @param amount the amount as entered by the user
	 * @return the formatted amount
	 */
	public static String formatUserAmount(String amount) {
		if ((amount == null) || (amount.length() == 0)) {
			return "0.00";
		}
//		StringBuffer decimal = new StringBuffer();
		StringBuffer integer = new StringBuffer();

		// format the decimal part
		int decimalPos = amount.indexOf(".");
//		if ((decimalPos < 0) || (decimalPos == amount.length() - 1)) {
//			// there is not decimal point or it is the last character of the string
//			decimal.append("00");
//		} else {
//			// get the decimal positions
//			String auxDecimal = amount.substring(decimalPos + 1);
//			if (auxDecimal.length() >= 2) {
//				decimal.append(auxDecimal.substring(0, 2));
//			} else {
//				// add 0s to complete 2 decimals
//				decimal.append(auxDecimal);
//				for (int i = decimal.length(); i < 2; i++) {
//					decimal.append("0");
//				}
//			}
//		}

		// format the integer part
		String auxInteger = amount;
		if (decimalPos == 0) {
			auxInteger = "0";
		} else if (decimalPos > 0) {
			auxInteger = amount.substring(0, decimalPos);
		}
		int curPos;
		for (int i = auxInteger.length(); i > 0; i = i - 3) {
			curPos = i - 3;
			if (curPos < 0) {
				curPos = 0;
			}
			if (curPos > 0) {
				integer = new StringBuffer(",").
						append(auxInteger.substring(curPos, i)).
						append(integer);
			} else {
				integer = new StringBuffer(auxInteger.
						substring(0, i)).append(integer);
			}
		}
//		StringBuffer result = integer.append(".").append(decimal.toString());
		return "$"+integer.toString();
	}

	/**
	 *
	 * @param monto
	 * @return
	 */
	public static String validateMont(String monto, boolean deleteDecimal){
		String auxMonto = monto;
		int longitud = monto.length();

		if(auxMonto.contains("$"))
			auxMonto = auxMonto.replace("$","");
		if(!deleteDecimal) {
			if (auxMonto.endsWith(".00"))
				auxMonto = auxMonto.replace(".00", "");
		}else{

			auxMonto = auxMonto.substring(0, longitud-3);
		}
		if(auxMonto.contains(","))
			auxMonto = auxMonto.replace(",", "");

		return auxMonto;
	}

	public static String clearAmounr(String amount){

		String auxAmount = amount;

		if (auxAmount.contains("$"))
			auxAmount = auxAmount.replace("$","");

		if(auxAmount.contains(","))
			auxAmount = auxAmount.replace(",","");

		return auxAmount;

	}

//	public static void orderProducts(boolean isSaveAuxList){
//		Session oSession = Tools.getCurrentSession();
//
//		ArrayList<Producto> productosSession;
//
//		if (!isSaveAuxList){
//			productosSession = oSession.getCreditos().getProductos();
//		}else {
//			productosSession = oSession.getAuxCreditos().getProductos();
//		}
//
//		ArrayList<Producto> auxProduct = new ArrayList<Producto>(productosSession.size());
//
//		for(int j = 0; j<ConstantsCredit.ordenProductos.length; j++){
//
//			for (int i = 0; i<productosSession.size(); i++){
//				final String cveProd = productosSession.get(i).getCveProd();
//
//				if(ConstantsCredit.ordenProductos[j].equalsIgnoreCase(cveProd)){
//					auxProduct.add(productosSession.get(i));
//					break;
//				}else{
//					Log.e("burbuja","no encontro producto: "+cveProd);
//				}
//			}
//		}
//
//		if(!isSaveAuxList) {
//			//guardando en sesión el nuevo arreglo de productos ahora ordenados.
//			oSession.setCreditos(new ObjetoCreditos(oSession.getCreditos().getEstado(),
//					oSession.getCreditos().getMontotSol(), oSession.getCreditos().getPorcTotal(),
//					oSession.getCreditos().getPagMTot(), auxProduct, oSession.getCreditos().getCreditos(),
//					oSession.getCreditos().getCreditosContratados()));
//		}else{
//			oSession.setAuxCreditos(new ObjetoCreditos(oSession.getCreditos().getEstado(),
//					oSession.getCreditos().getMontotSol(), oSession.getCreditos().getPorcTotal(),
//					oSession.getCreditos().getPagMTot(), auxProduct, oSession.getCreditos().getCreditos(),
//					oSession.getCreditos().getCreditosContratados()));
//		}
//
//	}

	//return a product by its cveProd from session array
	public static Producto getProductByCve(String cveProd, ArrayList<Producto>productosSession){
//		ArrayList<Producto>productosSession = Session.getInstance().getCreditos().getProductos();
		Producto producto = null;

		for (int i = 0; i < productosSession.size(); i++){
			final String auxCveProd = productosSession.get(i).getCveProd();

			if(auxCveProd.equalsIgnoreCase(cveProd)){
				producto = productosSession.get(i);
				break;
			}
		}
		return producto;
	}

	/**
	 * Format a decimal amount, reserving the last two digits for the decimals.
	 * Examples:
	 * 1200 will return 12.00
	 * 1230 will return 12.30
	 * 12.35 will return 1235
	 *
	 * @param amount amount to format
	 * @return the formatted amount
	 */
	public static String formatAmountFromServer(String amount) {

		if (amount.length() == 0) {
			return "0.00";
		} else if (amount.length() == 1) {
			return "0.0" + amount;
		} else if (amount.length() == 2) {
			return "0." + amount;
		} else {
			return formatPositiveIntegerAmount(amount.substring(0, amount.length() - 2))
					+ "." + amount.substring(amount.length() - 2);
		}

	}

}
