package bbvacredit.bancomer.apicredit.gui.delegates;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.ServerConstants;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.MenuPrincipalActivity;
import bbvacredit.bancomer.apicredit.gui.activities.ResumenEmailViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CreditoContratado;
import bbvacredit.bancomer.apicredit.models.EnvioCorreoData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;

/**
 * Created by Carlos Garcia on 10/12/2015.
 */
public class EnviarCorreoDelegate extends BaseDelegate {
    int operationDelivered;
//    Producto ps1=null; //Nomina
//    Producto ps2=null;  //PPI
//    Producto ps3=null;  //TDC
//    Producto ps4=null;  //ILC
//    Producto ps5=null;  //AUTO
//    Producto ps6=null;  //HP
//    Producto ps7=null;  //ANOM
    String src; //Todos si no hay N si si S
    String san; //Si hay Nomina S si no N
    String sap; //Si hay PPI S si no N
    String sai; //Si hay ILC S si no N
    String sah; //Si hay CH S si no N
    String saa; //Si hay Auto S si no N
    String sat; //Si hay TDC S si no N
    String sad; //Si hay NOM S si no N

    CreditoContratado pc1;
    CreditoContratado pc2;
    CreditoContratado pc3;
    CreditoContratado pc4;
    CreditoContratado pc5;
    CreditoContratado pc6;
    CreditoContratado pc7;

    String titulo1;
    String titulo2;
    String titulo3;
    String titulo4;
    String titulo5;
    String titulo6;
    String titulo7;

    boolean mailPendiente;
    private Session session;
    private ObjetoCreditos data;
    private Producto producto;
    private ResumenEmailViewController act;

    public EnviarCorreoDelegate() {
        SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
        session = Tools.getCurrentSession();
        // Datos de sesion
        data = session.getCreditos();
        producto = Session.getInstance().getCreditos().getProductos().get(sp.getInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, 0));
    }

    public void enviarCorreoConIndicador(String indCorreo, String direccionEnvioEmail, ResumenEmailViewController act) {
        Log.i("EnviarCorreoDelegate", "enviarCorreoConIndicador");
        mailPendiente = true;
        this.act = act;

        MainController.getInstance().muestraIndicadorActividad("Operación", "conectando");

        int downloadId = 0;

        ArrayList<Producto> creditsSimulated = getCreditsSimulated();
        ArrayList<CreditoContratado> creditsContratados = getCreditsContratados();

        if (!creditsSimulated.isEmpty()) {
            src = "N";
            san = "N";
            sap = "N";
            sai = "N";
            sah = "N";
            saa = "N";
            sat = "N";
        } else {
            src = "S";
        }

        for (Producto creditSimulated: creditsSimulated) {
            Producto auxProducto1 = creditSimulated;
            Producto auxProducto = new Producto();

            auxProducto.setCveProd(auxProducto1.getCveProd());

//            if(auxProducto1.getIndSim().equalsIgnoreCase("S")) {
//                float montoDeseS = Float.parseFloat(auxProducto1.getMontoDeseS());
//                auxProducto.setMontoDeseS(Tools.getCurrency(montoDeseS)); // verificar formato
//                Log.w("email", "montoDeseS: " + Tools.getCurrency(montoDeseS));
//
//                if(!auxProducto1.getCveProd().equals(ConstantsCredit.TARJETA_CREDITO)) { //tdc no tiene pago mensual ni pago máximoO
//                    float pagMProdS = Float.parseFloat(auxProducto1.getPagMProdS());
//                    auxProducto.setPagMProdS(Tools.getCurrency(pagMProdS)); // verificar formato
//                    Log.w("email", "pagMProdS: " + Tools.getCurrency(pagMProdS));
//
//                    float montoMaxS = Float.parseFloat(auxProducto1.getMontoMaxS());
//                    auxProducto.setMontoMaxS(Tools.getCurrency(montoMaxS)); // verificar formato
//                    Log.w("email", "montoMaxS: " + Tools.getCurrency(montoMaxS));
//                }
//            }else{
//                auxProducto.setMontoDeseS(auxProducto1.getMontoDeseS()); // verificar formato
//                auxProducto.setPagMProdS(auxProducto1.getPagMProdS());
//                auxProducto.setMontoMaxS(auxProducto1.getMontoMaxS());
//            }

            auxProducto.setNumTDCS(auxProducto1.getNumTDCS());
            auxProducto.setDesPlazoE(auxProducto1.getDesPlazoE());
            auxProducto.setTasaS(auxProducto1.getTasaS());
            auxProducto.setIndSim(auxProducto1.getIndSim());
            auxProducto.setCATS(auxProducto1.getCATS());
            auxProducto.setFechaCatS(auxProducto1.getFechaCatS());
            if(auxProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.CREDITO_NOMINA)) {
//                ps1 = auxProducto;
                san = "S";
            } else if(auxProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)) {
//                ps2 = auxProducto;
                sap = "S";
            } else if(auxProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.TARJETA_CREDITO)) {
//                ps3 = auxProducto;
                sat = "S";
            } else if(auxProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.INCREMENTO_LINEA_CREDITO)) {

                sai = "S";
            } else if(auxProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.CREDITO_AUTO)) {
//                ps5 = auxProducto;
                saa = "S";
            } else if(auxProducto.getCveProd().equalsIgnoreCase("0HIP") ||
                    auxProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.CREDITO_HIPOTECARIO)) {
//                ps6 = auxProducto;
                sah = "S";
            }
//            else if (auxProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.ADELANTO_NOMINA)) {
//                ps7 = auxProducto;
//                sad = "S";
//            }
        }

        verificaIndicadores();

        if(creditsContratados != null) {
            for (int i = 0; i < creditsContratados.size(); i++) {
                CreditoContratado auxProducto1 = creditsContratados.get(i);
                CreditoContratado auxProducto = new CreditoContratado();

                auxProducto.setCveProd(auxProducto1.getCveProd());

                //int pagoMen = Integer.parseInt(Tools.getCurrency(auxProducto1.getPagoMen()));
                //auxProducto.setPagoMen(pagoMen);  // Verificar formato
                auxProducto.setPagoMen(auxProducto1.getPagoMen());

                //int saldo = Integer.parseInt(Tools.getCurrency(auxProducto1.getSaldo()));
                //auxProducto.setSaldo(saldo);   // Verificar formato
                auxProducto.setSaldo(auxProducto1.getSaldo());

                auxProducto.setIndicadorSim(auxProducto1.getIndicadorSim());

                if (i == 0) {
                    pc1 = auxProducto;
                    titulo1 = asignarTitulo(pc1.getCveProd());
                } else if (i == 1) {
                    pc2 = auxProducto;
                    titulo2 = asignarTitulo(pc2.getCveProd());
                } else if (i == 2) {
                    pc3 = auxProducto;
                    titulo3 = asignarTitulo(pc3.getCveProd());
                } else if (i == 3) {
                    pc4 = auxProducto;
                    titulo4 = asignarTitulo(pc4.getCveProd());
                } else if (i == 4) {
                    pc5 = auxProducto;
                    titulo5 = asignarTitulo(pc5.getCveProd());
                } else if (i == 5) {
                    pc6 = auxProducto;
                    titulo6 = asignarTitulo(pc6.getCveProd());
                } else if (i == 6){
                    pc7 = auxProducto;
                    titulo7 = asignarTitulo(pc7.getCveProd());
                }

            }
        }

        verificacionProductosLiquidacion();


        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        // Mapeamos el codigo de operacion
        addParametro(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);

        // Mapeamos el id de cliente tomado de la sesion
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);

        // Mapeamos el IUM tomado de la sesion
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);

        // Mapeamos el numeroCelular tomado de la sesion
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);


        //Parámetro para Adelanto de sueldo
//        addParametroObligatorio(ps7.getIndSim(), ServerConstantsCredit.SPPI, paramTable);
//        addParametro(ps7.getMontoDeseS(), ServerConstantsCredit.PP_IMPORTE, paramTable);
//        addParametro(ps7.getDesPlazoE(), ServerConstantsCredit.PP_PLAZO,paramTable);
//        addParametro(ps7.getPagMProdS(), ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL,paramTable);

        String pagoMensualMaxS = ""; //pago mensual maximo
        String montoMaximoS  = "";  //monto máximo
        String montoDeseadoS  = "";  //monto deseado

        //Se obtienen los valores que se mandan para el envío de correo
        for(int i = 0; i<getCreditsSimulated().size(); i++){
            final Producto oProducto = getCreditsSimulated().get(i);

            montoMaximoS = Tools.getCurrency(Float.parseFloat(oProducto.getMontoMaxS()));
            montoDeseadoS = Tools.getCurrency(Float.parseFloat(oProducto.getMontoDeseS()));

            if(oProducto.getIndSim().equalsIgnoreCase("S")) {
                Log.w("email", "cveProd: " + oProducto.getCveProd());
                Log.i("email", "montoMS: " + montoMaximoS);
                Log.i("email", "montoDS: " + montoDeseadoS);
            }

            if(oProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.CREDITO_AUTO)
                    && oProducto.getIndSim().equalsIgnoreCase("S")) {

                pagoMensualMaxS = Tools.getCurrency(Float.parseFloat(oProducto.getPagMProdS()));

                addParametroObligatorio(oProducto.getIndSim(), ServerConstantsCredit.SCA, paramTable);
                addParametro(montoDeseadoS, ServerConstantsCredit.CA_IMPORTE, paramTable);
                addParametro(oProducto.getTasaS(), ServerConstantsCredit.CA_TASA_ANUAL, paramTable);
                addParametro(oProducto.getDesPlazoE(), ServerConstantsCredit.CA_PLAZO, paramTable);
                addParametro(pagoMensualMaxS, ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL, paramTable);

                addParametro(pagoMensualMaxS, ServerConstantsCredit.CA, paramTable);

            } else if (oProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)
                    && oProducto.getIndSim().equalsIgnoreCase("S")) {

                pagoMensualMaxS = Tools.getCurrency(Float.parseFloat(oProducto.getPagMProdS()));

                addParametroObligatorio(oProducto.getIndSim(), ServerConstantsCredit.SPPI, paramTable);
                addParametro(montoDeseadoS, ServerConstantsCredit.PP_IMPORTE, paramTable);
                addParametro(oProducto.getTasaS(), ServerConstantsCredit.PP_TASA_ANUAL, paramTable);
                addParametro(oProducto.getDesPlazoE(), ServerConstantsCredit.PP_PLAZO, paramTable);
                addParametro(pagoMensualMaxS, ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL, paramTable);

                addParametro(montoMaximoS, ServerConstantsCredit.PP, paramTable);

            }if(oProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.INCREMENTO_LINEA_CREDITO)
                    && oProducto.getIndSim().equalsIgnoreCase("S")) {

                oProducto.setNumTDCS(Tools.hideAccountNumber(oProducto.getNumTDCS()));
                if(oProducto.getNumTDCS().length() > 15)
                    oProducto.setNumTDCS(oProducto.getNumTDCS().substring(oProducto.getNumTDCS().length() - 6, 5));

                addParametroObligatorio(oProducto.getIndSim(), ServerConstantsCredit.SILDC, paramTable);
                addParametro(oProducto.getNumTDCS(), ServerConstantsCredit.IL_TARJETA_DE_CREDITO, paramTable);
                addParametro(montoMaximoS, ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA, paramTable);
                addParametro(montoDeseadoS, ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA, paramTable);

                addParametro(montoMaximoS, ServerConstantsCredit.IL, paramTable);

            }else if(oProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.CREDITO_NOMINA)
                    && oProducto.getIndSim().equalsIgnoreCase("S")) {

                pagoMensualMaxS = Tools.getCurrency(Float.parseFloat(oProducto.getPagMProdS()));

                addParametroObligatorio(oProducto.getIndSim(), ServerConstantsCredit.SCN, paramTable);
                addParametro(montoDeseadoS, ServerConstantsCredit.CN_IMPORTE, paramTable);
                addParametro(oProducto.getTasaS(), ServerConstantsCredit.CN_TASA_ANUAL, paramTable);
                addParametro(oProducto.getDesPlazoE(), ServerConstantsCredit.CN_PLAZO, paramTable);
                addParametro(pagoMensualMaxS, ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL, paramTable);

                addParametro(montoMaximoS, ServerConstantsCredit.CN, paramTable);

            }else if(oProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.TARJETA_CREDITO)
                    && oProducto.getIndSim().equalsIgnoreCase("S")) {

                addParametroObligatorio(oProducto.getIndSim(), ServerConstantsCredit.STDC, paramTable);
                addParametro(montoMaximoS, ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO, paramTable);
                addParametro(montoDeseadoS, ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO, paramTable);
                addParametro(montoMaximoS, ServerConstantsCredit.TC, paramTable);

            }else if(oProducto.getCveProd().equalsIgnoreCase(ConstantsCredit.CREDITO_HIPOTECARIO)
                    && oProducto.getIndSim().equalsIgnoreCase("S")){

                pagoMensualMaxS = Tools.getCurrency(Float.parseFloat(oProducto.getPagMProdS()));

                addParametroObligatorio(getCreditsSimulated().get(i).getIndSim(), ServerConstantsCredit.SCH, paramTable);
                addParametro(montoDeseadoS, ServerConstantsCredit.CH_IMPORTE, paramTable);
                addParametro(oProducto.getTasaS(), ServerConstantsCredit.CH_TASA_ANUAL, paramTable);
                addParametro(oProducto.getDesPlazoE(), ServerConstantsCredit.CH_PLAZO, paramTable);
                addParametro(pagoMensualMaxS, ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL, paramTable);
                addParametro(oProducto.getCATS(), ServerConstantsCredit.CON_CAT, paramTable);  // Verificar porque viene nulo
                addParametro("", ServerConstantsCredit.CON_CAT, paramTable);
                addParametro(oProducto.getFechaCatS(), ServerConstantsCredit.CON_FECHA_CALCULO, paramTable); // Verificar porque viene nulo
                addParametro("", ServerConstantsCredit.CON_FECHA_CALCULO, paramTable);

                addParametro(pagoMensualMaxS, ServerConstantsCredit.CH, paramTable);
            }
        }


        addParametroObligatorio(pc1.getIndicadorSim(), ServerConstantsCredit.SCNL, paramTable);
        addParametro(titulo1, ServerConstantsCredit.CN_DESC, paramTable);
        addParametro(pc1.getSaldo(), ServerConstantsCredit.CN_CANT, paramTable);
        addParametroObligatorio(pc2.getIndicadorSim(), ServerConstantsCredit.SPPIL, paramTable);
        addParametro(titulo2, ServerConstantsCredit.PP_DESC, paramTable);
        addParametro(pc2.getSaldo(), ServerConstantsCredit.PP_CANT, paramTable);
        addParametroObligatorio(pc3.getIndicadorSim(), ServerConstantsCredit.SCHL, paramTable);
        addParametro(titulo2, ServerConstantsCredit.CH_DESC, paramTable);
        addParametro(pc3.getSaldo(), ServerConstantsCredit.CH_CANT, paramTable);
        addParametroObligatorio(pc4.getIndicadorSim(), ServerConstantsCredit.SCAL, paramTable);
        addParametro(titulo4, ServerConstantsCredit.CA_DESC, paramTable);
        addParametro(pc4.getSaldo(), ServerConstantsCredit.CA_CANT, paramTable);
        addParametroObligatorio(pc5.getIndicadorSim(), ServerConstantsCredit.SP5, paramTable);
        addParametro(titulo5, ServerConstantsCredit.P5_DESC, paramTable);
        addParametro(pc5.getSaldo(), ServerConstantsCredit.P5_CANT, paramTable);
        addParametroObligatorio(pc6.getIndicadorSim(), ServerConstantsCredit.SP6, paramTable);
        addParametro(titulo6, ServerConstantsCredit.P6_DESC, paramTable);
        addParametro(pc6.getSaldo(), ServerConstantsCredit.P6_CANT, paramTable);
//        addParametro(titulo7, ServerConstantsCredit.P7_DESC,paramTable);
//        addParametro(pc7.getSaldo(), ServerConstantsCredit.P7_CANT,paramTable);

        addParametroObligatorio(src, ServerConstantsCredit.SCR, paramTable);
        addParametroObligatorio(san, ServerConstantsCredit.SAN, paramTable);
        addParametroObligatorio(sap, ServerConstantsCredit.SAP, paramTable);
        addParametroObligatorio(sai, ServerConstantsCredit.SAI, paramTable);
        addParametroObligatorio(sah, ServerConstantsCredit.SAH, paramTable);
        addParametroObligatorio(saa, ServerConstantsCredit.SAA, paramTable);
        addParametroObligatorio(sat, ServerConstantsCredit.SAT, paramTable);
//        addParametro(ps3.getMontoMaxS(), ServerConstantsCredit.TC, paramTable);
//        addParametroObligatorio(sad,ServerConstantsCredit.);

        addParametroObligatorio(indCorreo, ServerConstantsCredit.IND_CORREO, paramTable);
        addParametroObligatorio(direccionEnvioEmail, ServerConstantsCredit.email, paramTable);

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.envioCorreo(paramTable);
        this.doNetworkOperation(Server.ENVIO_CORREO_OPERACION, paramTable2,true,new EnvioCorreoData(),MainController.getInstance().getContext());

        Log.i("EnviarCorreoDelegate", "Se ha invocado doNetworkOperation envío de correo");

    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    public void analyzeResponse(String operationId, ServerResponse response) {
        Log.i("EnviarCorreoDelegate", "analyzeResponse, se ha obtenido respuesta");

        if(getCodigoOperacion().equals(operationId)) {
            if ((response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)) {
                if(act != null)
                    MainController.getInstance().ocultaIndicadorActividad();
                    //act.showInformationAlert("Su mensaje ha sido enviado con éxito");
                act.showInformationAlert("Su mensaje ha sido enviado con éxito", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainController.getInstance().showScreen(MenuPrincipalActivity.class);
                    }
                });
            } else {
                // Mostrar mensaje de error
                String error = response.getMessageText();
                act.showErrorMessage(error);
            }
        }
    }

    protected String getCodigoOperacion() {
        return Server.ENVIO_CORREO_OPERACION;
    }

    private ArrayList<Producto> getCreditsSimulated() {
        return data.getProductos();
    }

    private ArrayList<CreditoContratado> getCreditsContratados(){
        return data.getCreditosContratados();
    }

    private void verificaIndicadores() {
        Producto p = new Producto();
        p.setMontoDeseS("");
        p.setIndSim("N");
        p.setTasaS("");
        p.setPagMProdS("");
        p.setDesPlazoE("");
        p.setNumTDCS("");
        p.setMontoMaxS("");
        p.setCATS("");
        p.setFechaCatS("");

        if(san == null || san.equals("N")) {
            san = "N";
//            ps1 = p;
        }
        if(sap == null || sap.equals("N")) {
            sap = "N";
//            ps2 = p;
        }
        if(sat == null || sat.equals("N")) {
            sat = "N";
//            ps3 = p;
        }
        if(sai == null || sai.equals("N")) {
            sai = "N";
//            ps4 = p;
        }
        if(saa == null || saa.equals("N")) {
            saa = "N";
//            ps5 = p;
        }
        if(sah == null || sah.equals("N")) {
            sah = "N";
//            ps6 = p;
        }
    }

    private String asignarTitulo(String clave) {

        String respuesta;
        // Falta el título para Tarjeta de crédito y para oferta preaprobada
        if(clave.equals(ConstantsCredit.CREDITO_NOMINA))
            respuesta = "SIMULACION CREDITO NOMINA";
        else if(clave.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO))
            respuesta = "SIMULACION PRESTAMO PERSONAL INMEDIATO";
        else if(clave.equals("0HIP") || clave.equals(ConstantsCredit.CREDITO_HIPOTECARIO))
            respuesta = "SIMULACION CREDITO HIPOTECARIO";
        else if(clave.equals(ConstantsCredit.CREDITO_AUTO))
            respuesta = "SIMULACION CREDITO DE AUTO";
        else if(clave.equals(ConstantsCredit.ADELANTO_NOMINA))
            respuesta = "SIMULACION DE ADELANTO DE SUELDO";
        else
            respuesta = "";

        return respuesta;
    }

    private void verificacionProductosLiquidacion() {

        CreditoContratado creditoVacio = new CreditoContratado("", "", "", 0, 0, 0, true); // verificar el valor de indicadorSim, debe ser true o false?

        if(pc1 == null) {
            pc1 = creditoVacio;
            titulo1 = "";
        }
        if(pc2 == null) {
            pc2 = creditoVacio;
            titulo2 = "";
        }
        if(pc3 == null) {
            pc3 = creditoVacio;
            titulo3 = "";
        }
        if(pc4 == null) {
            pc4 = creditoVacio;
            titulo4 = "";
        }
        if(pc5 == null) {
            pc5 = creditoVacio;
            titulo5 = "";
        }
        if(pc6 == null) {
            pc6 = creditoVacio;
            titulo6 = "";
        }
    }

    public String getEmail(){
        return session.getEmail();
    }

}
