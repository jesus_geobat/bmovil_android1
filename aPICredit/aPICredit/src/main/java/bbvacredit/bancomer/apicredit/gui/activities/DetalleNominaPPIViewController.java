package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.NominaPPIDelegate;
import tracking.TrackingHelperCredit;

/**
 * Created by OOROZCO on 04/12/15.
 */
public class DetalleNominaPPIViewController extends Fragment {


    // Parameters Opening //

    private ImageButton btnRecalcularSimulacion;
    private ImageButton btnSolicitarSimulacion;
    private TextView lblResultadoPago;
    private TextView lblResultadoPlazo;
    private TextView lblResultadoTasa;
    private TextView lblCat;
    private TextView lblPagoMensual;
    private Button btnVerResumenPagos;
    private NominaPPIDelegate delegate;
    private boolean isPPI;
    private Session session;
    private Integer getProdIndex = 0;

    // Parameters Closing //

    public void setPPI(boolean isPPI) {
        this.isPPI = isPPI;
    }

    public void setDelegate(NominaPPIDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (isPPI)
            TrackingHelperCredit.trackState("creditdetallePPICont", MainController.getInstance().estados);
        else
            TrackingHelperCredit.trackState("creditdetalleCreditoNominaCont", MainController.getInstance().estados);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_credit_resultado_simulacion, container, false);

        findViews(rootView);

        ((MenuPrincipalActivity) getActivity()).setIsSimulate(true);
        ((MenuPrincipalActivity) getActivity()).setoFragment(this);

        delegate.setDetalleNomina(this);

        validaRevire();

        return  rootView;
    }

    private void validaRevire(){
        String tasaNueva = "";
        String tasaVieja = "";

        if(delegate.getProducto().getIndRev()!=null) {
                Log.i("revire", "indRev no nulo");
                String isShow = delegate.getProducto().getIndRev();

            if (isShow.equalsIgnoreCase("S")) {
                Log.i("revire", "valida revire");
                tasaNueva = delegate.getProducto().getTasaS();
                tasaVieja = delegate.getProducto().getTasaOri();

                ((MenuPrincipalActivity) getActivity()).setCintilloRevire(tasaNueva, tasaVieja, true);
            }
        }else{
            Log.e("revire","indRev es nulo");
        }

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        session = Tools.getCurrentSession();
        getProdIndex = 0;

        if(delegate.isPPI())
            getProdIndex = session.getProductoIndexByCve(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO);
        else
            getProdIndex = session.getProductoIndexByCve(ConstantsCredit.CREDITO_NOMINA);

        Log.i("opinator", "guardando cveProd: " + delegate.getData().getProductos().get(getProdIndex).getCveProd());

        String tasa= getActivity().getString(R.string.lblTasaNomina) + " ";
        String cat;
        DecimalFormat form = new DecimalFormat("0.00");

        try {
            float tasa2 = Float.parseFloat(delegate.getData().getProductos().get(getProdIndex).getTasaS());
            Log.i("tasa", "valor tasa: " + tasa2);
            tasa2 = tasa2 / 12;

            Log.i("tasa", "valor tasa: " + tasa2);

            cat = getActivity().getString(R.string.lblCatPPI1) + " "+
                    delegate.getData().getProductos().get(getProdIndex).getCATS() + getActivity().getString(R.string.lblCatPPI2) + " " +
                    delegate.getData().getProductos().get(getProdIndex).getFechaCatS() + ".";

            String setTasa = tasa + form.format(tasa2)+getActivity().getResources().getString(R.string.lblResultadoTasaSinIva);
            Typeface boldTypeface = Typeface.defaultFromStyle(Typeface.BOLD);

            lblPagoMensual.setText(getActivity().getResources().getString(R.string.lblPagoMensual_pago_maximo));
            lblResultadoPago.setText(GuiTools.getMoneyString(delegate.getProducto().getPagMProdS()));
            lblResultadoPlazo.setText(delegate.getData().getProductos().get(getProdIndex).getDesPlazoE());

            lblResultadoTasa.setText(setTasa + "\n" + getActivity().getResources().getString(R.string.lblPagoMensual_iva));
            lblResultadoTasa.setTypeface(boldTypeface);
            lblCat.setText(cat);

        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    private void findViews(View root) {
        btnRecalcularSimulacion = (ImageButton) root.findViewById(R.id.btnRecalcularSimulacion);
        btnSolicitarSimulacion = (ImageButton) root.findViewById(R.id.btnContratarANOM);
        btnSolicitarSimulacion.setImageResource(R.drawable.txt_contratar);
        btnVerResumenPagos = (Button) root.findViewById(R.id.btnVerResumenPagos);

        lblResultadoPago = (TextView) root.findViewById(R.id.lblResultadoPago);
        lblResultadoPlazo = (TextView) root.findViewById(R.id.lblResultadoPlazo);
        lblResultadoTasa = (TextView) root.findViewById(R.id.lblResultadoTasa);
        lblPagoMensual = (TextView)root.findViewById(R.id.lblPagoMensual);
        lblCat = (TextView) root.findViewById(R.id.lblCat);

        btnRecalcularSimulacion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //delegate.saveOrDeleteSimulationRequest(false);

                delegate.saveOrDeleteSimulationRequest(false); //false para eliminar la operacion

                Map<String, Object> click_paso2_operacion = new HashMap<String, Object>();
                click_paso2_operacion.put("inicio", "event45");
                if (isPPI) {
                    click_paso2_operacion.put("&&products", "simulador;simulador:simulador PPI");
                    click_paso2_operacion.put("eVar12", "seleccion recalcular PPI");
                } else {
                    click_paso2_operacion.put("&&products", "simulador;simulador:simulador credito de nomina");
                    click_paso2_operacion.put("eVar12", "seleccion recalcular credito de nomina");
                }

            }
        });

        btnSolicitarSimulacion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                contratarAction();
            }
        });

        btnVerResumenPagos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainController.getInstance().showScreen(ResumenViewController.class);
                MainController.getInstance().setFlowFragment(DetalleNominaPPIViewController.this);
            }
        });

        btnSolicitarSimulacion.setImageResource(R.drawable.txt_contratar);

    }

    public void contratarAction() {
        realizaAccion(isPPI);
        delegate.oneClickILC();
    }

    private void realizaAccion(boolean isPPI){
        Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
        click_paso2_operacion.put("evento_realizada","event52");
        if (isPPI)
            click_paso2_operacion.put("&&products","simulador;simulador:solicitar credito nomina");
        else
            click_paso2_operacion.put("&&products","simulador;simulador:solicitar prestamo personal inmediato");

        click_paso2_operacion.put("eVar12", "solicitud realizada");
    }

    public void simpleUpdateMenu(String cveProd){

        if(((MenuPrincipalActivity)getActivity()) == null)
            Log.i("eliminar", "Menu es nulo");

        Log.i("eliminar", "actualizaMenu Nomina PPI, cveProd: " + cveProd);
        ((MenuPrincipalActivity)getActivity()).updateMenu(cveProd);
        getFragmentManager().beginTransaction().remove(((MenuPrincipalActivity) getActivity()).getActiveFragment()).commit();
        ((MenuPrincipalActivity)getActivity()).setCintilloRevire("","",false);
        showPPIorNomina();
    }

    public void showPPIorNomina(){

        if(delegate.isPPI()) {
            Log.i("eliminar","es ppi");
            ((MenuPrincipalActivity) getActivity()).showPPI();
        }else {
            Log.i("eliminar","es nomina");
            ((MenuPrincipalActivity) getActivity()).showNomina();
        }
    }

}
