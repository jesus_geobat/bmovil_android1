package bbvacredit.bancomer.apicredit.common;

import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.gui.activities.LineaCreditoActivity;
import bbvacredit.bancomer.apicredit.gui.activities.OldMenuPrincipalActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaCreditoAutoActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaCreditoAutoContratoActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaCreditoHipotecarioActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaCreditoHipotecarioContratoActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaCreditoNominaContratoActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaCreditoNominaLiquidacionActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaPPIActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaPPILiquidacionActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaTarjetaCreditoActivity;


/**
 * Clase utilitaria para el escalmiento de los elementos visuales, 
 * el escalmiento se basa en el ancho de la pantalla para respetar la relaci�n de aspecto de los elementos visuales. 
 */
public class GuiTools {
	/**
	 * Ancho de la pantalla base.
	 */
	private static double BaseWidth = 320.0;
	
	/**
	 * Factor de escala para la pantalla.
	 */
	private static double ScaleFactor = 1.0;
	
	/**
	 * Bandera de inicializaci�n.
	 */
	private static boolean initialized = false;
	
	/**
	 * Unused.
	 */
	private static boolean virtualButtonsEnabled = false;
	
	/**
	 * M�tricas de la pantalla actual.
	 */
	private static DisplayMetrics metrics = null;
	
	/**
	 * La �nica instancia de la clase.
	 */
	private static GuiTools current = null;
	
	/**
	 * Obtiene la �nica instancia de la clase.
	 * @return La instancia.
	 */
	public static GuiTools getCurrent() {
		if(null == current)
			current = new GuiTools();
		return current;
	}
	
	/**
	 * Contructor privado para respetar el patron Singleton.
	 */
	private GuiTools() {
		BaseWidth = 320.0;
		ScaleFactor = 1.0;
	}
	
	/**
	 * Inicializacion de la instancia para detectar el factor de escala correspondiente.
	 * @param manager El manejador de pantallas de donde se obtienen las caracteristicas de la pantalla.
	 */
	public void init(WindowManager manager) {
		if(null == manager)
			return;
		
		metrics = new DisplayMetrics();
		manager.getDefaultDisplay().getMetrics(metrics);
		ScaleFactor = ((double)metrics.widthPixels) / BaseWidth;
		initialized = true;
		
		ScreenWidth = metrics.widthPixels;
		ScreenHeigth = metrics.heightPixels;
		
		Log.d("GuiTools", "Screen resolution (WxH): " + metrics.widthPixels + "X" + metrics.heightPixels);
		Log.d("GuiTools", "Scale Factor: " + ScaleFactor);
	}
	
	public static SpannableString underlineText(String str){
		SpannableString content = new SpannableString(str);
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		return content;
	}
	
	/**
	 * Obtiene la bandera que indica si el elemento esta inicializado.
	 * @return True si la instancia ha sido inicializada, False de otro modo.
	 */
	public static boolean isInitialized() {
		return initialized;
	}
	
	/**
	 * Obtiene el factor de escala para pantalla actual.
	 * @return El factor de escala.
	 */
	public static double getScaleFactor() {	
		return ScaleFactor;
	}
	
	/**
	 * Obtiene la equivalencia en pixeles para la pantalla actual.
	 * @param baseMeasure La medida inicial.
	 * @return La medida tras aplicr el factor de escala correspondiente.
	 */
	public int getEquivalenceInPixels(double baseMeasure) {
		if(baseMeasure < 0.0)
			return 0;
		return (int)(baseMeasure * ScaleFactor);
	}
	
	/**
	 * Obtiene la equivalencia en pixeles para la pantalla actual.
	 * @param baseMeasure La medida inicial.
	 * @return La medida tras aplicr el factor de escala correspondiente.
	 */
	public int getEquivalenceInPixels(int baseMeasure) {
		if(baseMeasure < 0.0)
			return 0;
		return (int)((double)baseMeasure * ScaleFactor);
	}
	
	/**
	 * Obtiene la equivalencia en pixeles para la pantalla actual.
	 * @param baseMeasure La medida inicial.
	 * @return La medida tras aplicr el factor de escala correspondiente.
	 */
	public int getEquivalenceFromScaledPixels(double baseMeasure) {
		if(baseMeasure < 0.0)
			return 0;
		return getEquivalenceInPixels(pxToDip((int)baseMeasure));
	}
	
	/**
	 * Obtiene la equivalencia en pixeles para la pantalla actual.
	 * @param baseMeasure La medida inicial.
	 * @return La medida tras aplicr el factor de escala correspondiente.
	 */
	public int getEquivalenceFromScaledPixels(int baseMeasure) {
		if(baseMeasure < 0.0)
			return 0;
		return getEquivalenceInPixels(pxToDip(baseMeasure));
	}

	/**
	 * Escala y establece cada valor de relleno(padding) del elemento.
	 * @param view La vista a escalar.
	 */
	public void scalePaddings(View view){
		if(null == view)
			return;
		
		int[] paddings = new int[4];
		
		paddings[0] = getEquivalenceInPixels(pxToDip(view.getPaddingLeft()));
		paddings[1] = getEquivalenceInPixels(pxToDip(view.getPaddingTop()));
		paddings[2] = getEquivalenceInPixels(pxToDip(view.getPaddingRight()));
		paddings[3] = getEquivalenceInPixels(pxToDip(view.getPaddingBottom()));
		
		view.setPadding(paddings[0], paddings[1], paddings[2], paddings[3]);
	}
	
	/**
	 * Escala y establece los parametros LayoutParams de la vista.
	 * @param view La vista a escalar.
	 */
	public void scaleLayoutParams(View view) {
		if(null == view)
			return;
		
		ViewGroup.LayoutParams params = view.getLayoutParams();
		
		if(params instanceof LinearLayout.LayoutParams) {
			LinearLayout.LayoutParams linearLayoutParams = (LinearLayout.LayoutParams)params;
			scaleLinearLayoutParams(view, linearLayoutParams);
		} else if(params instanceof FrameLayout.LayoutParams) {
			FrameLayout.LayoutParams frameLayoutParams = (FrameLayout.LayoutParams)params;
			scaleFrameLayoutParams(view, frameLayoutParams);
		} else if(params instanceof RelativeLayout.LayoutParams) {
			RelativeLayout.LayoutParams relativeLayoutParams = (RelativeLayout.LayoutParams)params;
			scaleRelativeLayoutParams(view, relativeLayoutParams);
		}
	}
	
	/**
	 * Escala y establece un objeto LayoutParams para un LinearLayout.
	 * @param params Los parametros de layout del elemento a escalar.
	 */
	private void scaleLinearLayoutParams(View view, LinearLayout.LayoutParams params) {
		if(null == view || null == params || !initialized)
			return;
		
		params.leftMargin 	= getEquivalenceInPixels(pxToDip(params.leftMargin));
		params.rightMargin 	= getEquivalenceInPixels(pxToDip(params.rightMargin));
		params.topMargin 	= getEquivalenceInPixels(pxToDip(params.topMargin));
		params.bottomMargin = getEquivalenceInPixels(pxToDip(params.bottomMargin));
		
		if(metrics.widthPixels != params.width && -1 != params.width && -2 != params.width)
			params.width = getEquivalenceInPixels(pxToDip(params.width));
		if(metrics.heightPixels != params.height && -1 != params.height && -2 != params.height)
			params.height = getEquivalenceInPixels(pxToDip(params.height));
		
		view.setLayoutParams(params);
	}
	
	/**
	 * Escala y establece un objeto LayoutParams para un RelativeLayout.
	 * @param params Los parametros de layout del elemento a escalar.
	 */
	private void scaleRelativeLayoutParams(View view, RelativeLayout.LayoutParams params) {
		if(null == view || null == params || !initialized)
			return;
		
		params.leftMargin 	= getEquivalenceInPixels(pxToDip(params.leftMargin));
		params.rightMargin 	= getEquivalenceInPixels(pxToDip(params.rightMargin));
		params.topMargin 	= getEquivalenceInPixels(pxToDip(params.topMargin));
		params.bottomMargin = getEquivalenceInPixels(pxToDip(params.bottomMargin));
		
		if(metrics.widthPixels != params.width && -1 != params.width && -2 != params.width)
			params.width = getEquivalenceInPixels(pxToDip(params.width));
		if(metrics.heightPixels != params.height && -1 != params.height && -2 != params.height)
			params.height = getEquivalenceInPixels(pxToDip(params.height));
		
		view.setLayoutParams(params);
	}
	
	/**
	 * Escala y establece un objeto LayoutParams para un FrameLayout. 
	 * @param params Los parametros de layout del elemento a escalar.
	 */
	private void scaleFrameLayoutParams(View view, FrameLayout.LayoutParams params) {
		if(null == view || null == params || !initialized)
			return;
		
		params.leftMargin 	= getEquivalenceInPixels(pxToDip(params.leftMargin));
		params.rightMargin 	= getEquivalenceInPixels(pxToDip(params.rightMargin));
		params.topMargin 	= getEquivalenceInPixels(pxToDip(params.topMargin));
		params.bottomMargin = getEquivalenceInPixels(pxToDip(params.bottomMargin));
		
		if(metrics.widthPixels != params.width && -1 != params.width && -2 != params.width)
			params.width = getEquivalenceInPixels(pxToDip(params.width));
		if(metrics.heightPixels != params.height && -1 != params.height && -2 != params.width)
			params.height = getEquivalenceInPixels(pxToDip(params.height));
		
		view.setLayoutParams(params);
	}
	
	/**
	 * Convierte una medida en pixeles a dip.
	 * @param px Medida en pixeles.
	 * @return Equivalente en dip.
	 */
	private double pxToDip(int px) {
		double dblPx = (double)px;
		double equivalent = dblPx / metrics.density;
		return equivalent;
		//return ((double)px / metrics.density);
	}
	
	/**
	 * Escala el tama�o de letra de un elemento TextView.
	 * @param view El TextView a escalar.
	 */
	public void tryScaleText(TextView view) {
		try {
			double textScaleChangeFactor = 1.0;
			//double textScaleChangeFactor = 1.0 / ScaleFactor;
			//view.setScaleX((float)ScaleFactor);
			//view.setScaleY((float)ScaleFactor);
			view.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float)(view.getTextSize() * (ScaleFactor * textScaleChangeFactor)));
			//view.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float)(view.getTextSize() * (1.0)));
		}catch(Exception ex) {
			Log.e("GuiTools", "Trying to scale text on an invalid view.", ex);
		}
	}
	
	/**
	 * Escala los parametros de posicionamiento (LayoutParams y Padings).
	 * @param view La vista a escalar.
	 */
	public void scale(View view) {
		//scale(view, false);
		if(null == view)
			return;
		
		scaleLayoutParams(view);
		scalePaddings(view);
		
		if(view instanceof TextView)
			tryScaleText((TextView)view);
	}
	
	/**
	 * Escala los parametros de posicionamiento (LayoutParams y Padings) y tama�o de texto si es necesario.
	 * @param view La vista a escalar.
	 * @param isTextView Bandera que indica si la vista hereda o es una instancia de TextView y se debe escalar el tama�o de texto.
	 */
	public void scale(View view, boolean isTextView) {
		if(null == view)
			return;
		
		scaleLayoutParams(view);
		scalePaddings(view);
		
		if(isTextView)
			tryScaleText((TextView)view);
	}
	
	public void scaleAll(ViewGroup viewGroup) {
		if(null == viewGroup)
			return;
		
		
		int childCount = viewGroup.getChildCount();
		
		
		for(int counter = 0; counter < childCount; counter++) {
			View view = viewGroup.getChildAt(counter);
			
			scale(view);
			
			if(view instanceof ViewGroup)
				scaleAll((ViewGroup)view);
		}
	}
	
	
	/*public static int getCreditIcon(String code){
		int ret;
		if(code.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
			ret = R.drawable.icon_menu_linea_credito;
		}else if(code.equals(ConstantsCredit.TARJETA_CREDITO)){
			ret = R.drawable.icon_menu_tarjeta_credito;
		}else if(code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)||code.equals("HIPO")){
			ret = R.drawable.icon_menu_hipoteca;
		}else if(code.equals(ConstantsCredit.CREDITO_AUTO)){
			ret = R.drawable.icon_menu_auto;
		}else if(code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
			ret = R.drawable.icon_menu_ppi;
		}else if(code.equals(ConstantsCredit.CREDITO_NOMINA)){
			ret = R.drawable.icon_menu_ppi;
		}else{
			ret = 0;
		}
		
		return ret;
	}*/


    //Modified on November 27th, by OOS.

    public static int getCreditIcon(String code){
        if(code.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
//            return R.drawable.ic_detalleagregartara;
			return R.drawable.nvo_icon_ilc;
        }else if(code.equals(ConstantsCredit.TARJETA_CREDITO)){
            return R.drawable.ic_detalletdca;
        }else if(code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
            return R.drawable.ic_detallehipotecarioa;
        }else if(code.equals(ConstantsCredit.CREDITO_AUTO)){
            return R.drawable.ic_detalleautoa;
        }else if(code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
            return  R.drawable.ic_detallepersonala;
        }else if(code.equals(ConstantsCredit.CREDITO_NOMINA)){
            return R.drawable.ic_detallenominaa;
        } else if(code.equals(ConstantsCredit.ADELANTO_NOMINA))
			return R.drawable.ic_detalleadelantosueldoa;
        return 0;
    }


	public static int getCreditIcon(String code, boolean isHelp){

		if(code.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO) || code.startsWith(ConstantsCredit.CLAVE_ILC)){
			if(!isHelp)
				return R.drawable.nvo_icon_ilc;
			else
				return R.drawable.nvo_icon_ayuda_ilc;

		}else if(code.equals(ConstantsCredit.TARJETA_CREDITO) || code.startsWith(ConstantsCredit.CLAVE_TDC)){
			if(!isHelp)
				return R.drawable.nvo_icon_tdc;
			else
				return R.drawable.nvo_icon_ayuda_tdc;

		}else if(code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
			if(!isHelp)
				return R.drawable.nvo_icon_credito_hipo;
			else
				return R.drawable.nvo_icon_ayuda_hipo;

		}else if(code.equals(ConstantsCredit.CREDITO_AUTO)){
			if(!isHelp)
				return R.drawable.nvo_icon_credito_auto;
			else
				return R.drawable.nvo_icon_ayuda_hipo;

		}else if(code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO) || code.startsWith(ConstantsCredit.CLAVE_CONSUMO)){
			if(!isHelp)
				return R.drawable.nvo_icon_credito_ppi;
			else
				return R.drawable.nvo_icon_ayuda_ppi;

		}else if(code.equals(ConstantsCredit.CREDITO_NOMINA)){ //pendiente por ícono
			if(!isHelp)
				return R.drawable.nvo_icon_onom;
			else
				return R.drawable.nvo_icon_ayuda_onom;

		} else if(code.equals(ConstantsCredit.ADELANTO_NOMINA)) {
			if (!isHelp)
				return R.drawable.nvo_icon_anom;
			else
				return R.drawable.nvo_icon_ayuda_anom;

		}else if(code.startsWith(ConstantsCredit.CLAVE_EFI)){
			if (!isHelp)
				return R.drawable.nvo_icon_efi;
			else
				return R.drawable.nvo_icon_ayuda_efi;
		}

		return 0;
	}

    public static int getResumenCreditIcon(String code){
        if(code.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
            return R.drawable.ic_res_ilc;
        }else if(code.equals(ConstantsCredit.TARJETA_CREDITO)){
            return R.drawable.ic_res_tdc;
        }else if(code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
            return R.drawable.ic_res_hipo;
        }else if(code.equals(ConstantsCredit.CREDITO_AUTO)){
            return R.drawable.ic_res_auto;
        }else if(code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
            return  R.drawable.ic_res_ppi;
        }else if(code.equals(ConstantsCredit.CREDITO_NOMINA)){
            return R.drawable.ic_res_nom;
        }else if(code.equals(ConstantsCredit.ADELANTO_NOMINA)){
            return R.drawable.ic_res_adelanto;
        }
        return 0;
    }

    public static int getSelectedCreditIcon(String code){
        if(code.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
            return R.drawable.ic_detalleagregartar;
        }else if(code.equals(ConstantsCredit.TARJETA_CREDITO)){
            return R.drawable.ic_detalletdc;
        }else if(code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
            return R.drawable.ic_detallehipotecario;
        }else if(code.equals(ConstantsCredit.CREDITO_AUTO)){
            return R.drawable.ic_detalleauto;
        }else if(code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
            return  R.drawable.ic_detallepersonal;
        }else if(code.equals(ConstantsCredit.CREDITO_NOMINA)){
            return R.drawable.ic_detallenomina;
        }else if(code.equals(ConstantsCredit.ADELANTO_NOMINA)){
            return R.drawable.ic_detalleadelantosueldon;
        }
        return 0;
    }

    public static int getSelectedLabel(String code){
        if(code.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO) || code.startsWith(ConstantsCredit.CLAVE_ILC)){
            return R.string.seekbar_producto_ilc;
        }else if(code.equals(ConstantsCredit.TARJETA_CREDITO) || code.startsWith(ConstantsCredit.CLAVE_TDC)){
			return R.string.seekbar_producto_tdc;
        }else if(code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
			return R.string.seekbar_producto_hipo;
        }else if(code.equals(ConstantsCredit.CREDITO_AUTO)){
			return R.string.seekbar_producto_auto;
        }else if(code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO) || code.startsWith(ConstantsCredit.CLAVE_CONSUMO)){
			return R.string.seekbar_producto_ppi;
        }else if(code.equals(ConstantsCredit.CREDITO_NOMINA)){
			return R.string.seekbar_producto_onom;
        }else if(code.equals(ConstantsCredit.ADELANTO_NOMINA)){
			return R.string.seekbar_producto_anom;
        }else if(code.equalsIgnoreCase(ConstantsCredit.CLAVE_EFI)){
			return R.string.seekbar_producto_efi;
		}
        return 0;
    }

	public static int getSelectedLabelUper(String code){
		if(code.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO) || code.startsWith(ConstantsCredit.CLAVE_ILC)){
			return R.string.seekbar_producto_ilc_uper;
		}else if(code.equals(ConstantsCredit.TARJETA_CREDITO) || code.startsWith(ConstantsCredit.CLAVE_TDC)){
			return R.string.seekbar_producto_tdc_uper;
		}else if(code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
			return R.string.seekbar_producto_hipo_uper;
		}else if(code.equals(ConstantsCredit.CREDITO_AUTO)){
			return R.string.seekbar_producto_auto_uper;
		}else if(code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO) || code.startsWith(ConstantsCredit.CLAVE_CONSUMO)){
			return R.string.seekbar_producto_ppi_uper;
		}else if(code.equals(ConstantsCredit.CREDITO_NOMINA)){
			return R.string.seekbar_producto_onom_upe;
		}else if(code.equals(ConstantsCredit.ADELANTO_NOMINA)){
			return R.string.seekbar_producto_anom_uper;
		}else if(code.equalsIgnoreCase(ConstantsCredit.CLAVE_EFI)){
			return R.string.seekbar_producto_efi_uper;
		}
		return 0;
	}

	public static int getSelectedDescriptionProduct(String cvProd){
		if(cvProd.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)|| cvProd.startsWith(ConstantsCredit.CLAVE_ILC) ){
			return R.string.popup_ayudacreditos_ilc_mensaje;
		}else if(cvProd.equals(ConstantsCredit.TARJETA_CREDITO) || cvProd.startsWith(ConstantsCredit.CLAVE_TDC)){
			return R.string.popup_ayudacreditos_tdc_mensaje;
		}else if(cvProd.equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
			return R.string.popup_ayudacreditos_hipo_mensaje;
		}else if(cvProd.equals(ConstantsCredit.CREDITO_AUTO)){
			return R.string.popup_ayudacreditos_auto_mensaje;
		}else if(cvProd.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO) || cvProd.startsWith(ConstantsCredit.CLAVE_CONSUMO)){
			return R.string.popup_ayudacreditos_ppi_mensaje;
		}else if(cvProd.equals(ConstantsCredit.CREDITO_NOMINA)){
			return R.string.popup_ayudacreditos_onom_mensaje;
		}else if(cvProd.equals(ConstantsCredit.ADELANTO_NOMINA)){
			return R.string.popup_ayudacreditos_anom_mensaje;
		}else if(cvProd.startsWith(ConstantsCredit.CLAVE_EFI)){
			return R.string.popup_ayudacreditos_efi_mensaje;
		}
		return 0;

	}
	
	public static int getCreditColor(String code){
		int ret;
		if(code.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
			ret = R.color.anaranjado;
		}else if(code.equals(ConstantsCredit.TARJETA_CREDITO)){
			ret = R.color.turquesa2;
		}else if(code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)||code.equals("HIPO")){
			ret = R.color.turquesa3;
		}else if(code.equals(ConstantsCredit.CREDITO_AUTO)){
			ret = R.color.naranja2;
		}else if(code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
			ret = R.color.verde2;
		}else if(code.equals(ConstantsCredit.CREDITO_NOMINA)){
			ret = R.color.verde2;
		}else{
			ret = 0;
		}
		
		return ret;
	}
	
	public static int getCreditTitle(String code){
		int ret;
		if(code.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
			ret = R.string.posicionGlobal_label_incLineaCredito;
		}else if(code.equals(ConstantsCredit.TARJETA_CREDITO)){
			ret = R.string.posicionGlobal_label_tarjCredito;
		}else if(code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)||code.equals("HIPO")){
			ret = R.string.posicionGlobal_label_credHipotecario;
		}else if(code.equals(ConstantsCredit.CREDITO_AUTO)){
			ret = R.string.posicionGlobal_label_credAuto;
		}else if(code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
			ret = R.string.posicionGlobal_label_ppi;
		}else if(code.equals(ConstantsCredit.CREDITO_NOMINA)){
			ret = R.string.posicionGlobal_label_prestNomina;
		}else if(code.equals(ConstantsCredit.ADELANTO_NOMINA)) {
			ret = R.string.posicionGlobal_label_adelantoNomina;
		}else{
			ret = 0;
		}
		
		return ret;
	}
	
	public static Class<?> getCreditRedirect(String code, Boolean esOfertado){
		Class<?> ret;
		
		if(esOfertado){
			if(code.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
				ret = LineaCreditoActivity.class;
			}else if(code.equals(ConstantsCredit.TARJETA_CREDITO)){
				ret = SimulaTarjetaCreditoActivity.class;
			}else if(code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
				ret = SimulaCreditoHipotecarioContratoActivity.class;
			}else if(code.equals(ConstantsCredit.CREDITO_AUTO)){
				ret = SimulaCreditoAutoContratoActivity.class;
			}else if(code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
				ret = SimulaPPIActivity.class;
			}else if(code.equals(ConstantsCredit.CREDITO_NOMINA)){
				ret = SimulaCreditoNominaContratoActivity.class;
			}else{
				ret = OldMenuPrincipalActivity.class;
			}
		}else{
			if(code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)||code.equals("HIPO")){
				ret = SimulaCreditoHipotecarioActivity.class;
			}else if(code.equals(ConstantsCredit.CREDITO_AUTO)){
				ret = SimulaCreditoAutoActivity.class;
			}else if(code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
				ret = SimulaPPILiquidacionActivity.class;
			}else if(code.equals(ConstantsCredit.CREDITO_NOMINA)){
				ret = SimulaCreditoNominaLiquidacionActivity.class;
			}else{
				ret = OldMenuPrincipalActivity.class;
			}
		}
		
		return ret;
	}
	/*
	public static String getMoneyString(String data){
		if(data != null){
			BigDecimal val = new BigDecimal(0);
			try{
				val = new BigDecimal(data);
			}catch(NumberFormatException e){
				Log.e("GuiTools", "Numberformat");
			}
			
			DecimalFormat myFormatter = new DecimalFormat("$###,##0.00");
			return myFormatter.format(val);
		}else{
			return "";
		}
	}*/
	
	
	public static String getMoneyString(String amount) {

		if ((amount == null) || (amount.length() == 0)) {

		return "0.00";

		}

		StringBuffer decimal = new StringBuffer();

		StringBuffer integer = new StringBuffer();



		// format the decimal part

		int decimalPos = amount.indexOf(".");

		if ((decimalPos < 0) || (decimalPos == amount.length() - 1)) {

		// there is not decimal point or it is the last character of the string

		decimal.append("00");

		} else {

		// get the decimal positions

		String auxDecimal = amount.substring(decimalPos + 1);

		if (auxDecimal.length() >= 2) {

		decimal.append(auxDecimal.substring(0, 2));

		} else {

		// add 0s to complete 2 decimals

		decimal.append(auxDecimal);

		for (int i = decimal.length(); i < 2; i++) {

		decimal.append("0");

		}

		}

		}



		// format the integer part

		String auxInteger = amount;

		if (decimalPos == 0) {

		auxInteger = "0";

		} else if (decimalPos > 0) {

		auxInteger = amount.substring(0, decimalPos);

		}

		int curPos;

		for (int i = auxInteger.length(); i > 0; i = i - 3) {

		curPos = i - 3;

		if (curPos < 0) {

		    curPos = 0;

		}

		if (curPos > 0) {

		integer = new StringBuffer(",").

		  append(auxInteger.substring(curPos, i)).

		 	append(integer);

		} else {

		integer = new StringBuffer(auxInteger.

		  substring(0, i)).append(integer);

		}

		}

		StringBuffer result = integer.append(".").append(decimal.toString());


		return "$"+result.toString();

		}
	
	public static String getTDCString(String data){
		if(data != null){
			if(data.length()>=5){
				Integer index = data.length()-5;
				
				return "*"+data.substring(index);
			}else{
				return "*"+data;
			}
		}else{
			return "";
		}
	}
	
	public static String getTDCString2Asteriscos(String data){
		if(data != null){
			if(data.length()>=5){
				Integer index = data.length()-4;
				
				return "*"+data.substring(index);
			}else{
				return "*"+data;
			}
		}else{
			return "";
		}
	}
	
	public static Integer getAmountThFormatted(Integer data){
		Integer min = 1000;
		String ret = min.toString();
		
		if(data > min){
			ret = data.toString();
			ret = ret.substring(0, (ret.length()-3));
			ret = ret+"000";
		}
		
		return Integer.valueOf(ret);
	}
	
	public int ScreenWidth = 0;
	
	public int ScreenHeigth = 0;

}

