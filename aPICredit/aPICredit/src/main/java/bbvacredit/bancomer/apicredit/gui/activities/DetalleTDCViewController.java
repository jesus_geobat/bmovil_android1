package bbvacredit.bancomer.apicredit.gui.activities;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.ICommonSession;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.TDCDelegate;
import bbvacredit.bancomer.apicredit.models.Producto;
import bbvacredit.bancomer.apicredit.models.Subproducto;
import tracking.TrackingHelperCredit;

public class DetalleTDCViewController extends Fragment implements View.OnClickListener {
    private ImageButton btnContratarTDC;
    private ImageButton btnRecalcularSimulacion;
    private Button btnVerResumen;
    private ImageView imgTDC;
    private ImageView imgAvisoTDC;
    private TextView lblMontoTotalTDC;
    private TextView lblNombreTDC;
    private TextView lblTipoTDC;
    private TextView lblQuiero;

    private TDCDelegate delegate;
    private Producto producto;
    private ArrayList<Subproducto> subproductoList;

    private String cveProd;
    private String desProd;
    private Integer montoMax;
    private Integer montoMin;
    private Integer tasa;
    private Double cat;
    private String fechaCat;
    private String nombreTarjeta;

    private Session session;

    private String [] clavePlat     = {"AP"};
    private String [] claveOro      = {"AV"};
    private String [] claveEduc     = {"TE"};
    private String [] claveAzul     = {"AC"};
    private String [] claveRayados  = {"AR"};
    private String [] claveUnam     = {"UM"};
    private String [] claveIPN      = {"PN"};
    private String [] claveCongelada= {"AG"};
    private String [] claveVive     = {"XA"};

    public DetalleTDCViewController() {
        // TODO Auto-generated constructor stub
    }

    public String getMonto(){
        return Integer.toString(producto.getSubproducto().get(0).getMonMax());
    }

    public TDCDelegate getDelegate() {
        return delegate;
    }

    public void setDelegate(TDCDelegate delegate) {
        this.delegate = delegate;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_credit_simula_tarjeta_credito, container, false);

        delegate.setDetalleTDC(this);
        if (rootView != null) {
            findViews(rootView);
        }

        ((MenuPrincipalActivity) getActivity()).setIsSimulate(true);
        ((MenuPrincipalActivity) getActivity()).setoFragment(this);

        return  rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        session = Tools.getCurrentSession();
        Integer getProdIndex = session.getProductoIndexByCve(ConstantsCredit.TARJETA_CREDITO);

        try {
            lblQuiero.setVisibility(View.GONE);
            Log.i("tdc","Monto deseado: "+delegate.getProducto().getMontoDeseS());
            Log.i("tdc","imagen: "+delegate.getProducto().getSubproducto().get(0).getCveSubp());
            lblMontoTotalTDC.setText(GuiTools.getMoneyString(delegate.getProducto().getMontoDeseS()));
            imgTDC.setImageDrawable(getTipoTarjeta(delegate.getProducto().getSubproducto().get(0).getCveSubp()));
            lblNombreTDC.setText(getResources().getString(R.string.title_nombre_tarjeta_credito) + " " + nombreTarjeta);

            Log.i("Credit", "nombreTarjeta: " + nombreTarjeta);

        }catch (Exception ex){
            ex.printStackTrace();
        }


    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public Drawable getTipoTarjeta(String nombre){

        Drawable tipo = null;

        for (int i = 0; i<clavePlat.length; i++){
            if(clavePlat[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_platinum);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Platinum);
                break;
            }
        }

        for (int i = 0; i<claveOro.length; i++){
            if(claveOro[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_oro);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Oro);
                break;
            }
        }

        for (int i = 0; i<claveEduc.length; i++){
            if(claveEduc[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_educacion);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Educacion);
                break;
            }
        }

        for (int i = 0; i<claveAzul.length; i++){
            if(claveAzul[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_azul);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Azul);
                break;
            }
        }

        for (int i = 0; i<claveRayados.length; i++){
            if(claveRayados[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_rayados);
                nombreTarjeta = getResources().getString(R.string.tarjeta_rayados);
                break;
            }
        }

        for (int i = 0; i<claveUnam.length; i++){
            if(claveUnam[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_unam);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Unam);
                break;
            }
        }

        for (int i = 0; i<claveIPN.length; i++){
            if(claveIPN[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_ipn);
                nombreTarjeta = getResources().getString(R.string.tarjeta_IPN);
                break;
            }
        }

        for (int i = 0; i<claveCongelada.length; i++){
            if(claveCongelada[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_congelada);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Congelada);
                break;
            }
        }

        for(int i = 0; i<claveVive.length; i++){
            if(claveCongelada[i].equals(nombre)){
                tipo = getResources().getDrawable(R.drawable.tdc_vive);
                nombreTarjeta = getResources().getString(R.string.tarjeta_Vive);
                break;
            }
        }

        return tipo;
    }


    private void findViews(View rootView) {
        btnRecalcularSimulacion = (ImageButton) rootView.findViewById(R.id.btnRecalcularSimulacion);
        btnRecalcularSimulacion.setOnClickListener(this);
        btnContratarTDC        = (ImageButton) rootView.findViewById(R.id.btnContratarTDC);
        btnContratarTDC.setOnClickListener(this);
        btnVerResumen           = (Button) rootView.findViewById(R.id.btnVerResumenPagos);
        btnVerResumen.setOnClickListener(this);
        imgTDC              = (ImageView) rootView.findViewById(R.id.imgTDC);
        imgAvisoTDC         = (ImageView) rootView.findViewById(R.id.imgAvisoTDC);
        imgAvisoTDC.setOnClickListener(this);
        lblMontoTotalTDC    = (TextView) rootView.findViewById(R.id.lblMontoTotalTDC);
        lblNombreTDC        = (TextView) rootView.findViewById(R.id.lblNombreTDC);
        lblTipoTDC          = (TextView) rootView.findViewById(R.id.lblTipoTDC);
        lblQuiero           = (TextView) rootView.findViewById(R.id.lblQuiero);
    }

    @Override
    public void onClick(View v) {

        if(v == btnContratarTDC){
           // solicitarAction();
            //preferences();
            contratarTDC(); //detalle alternativa y detalle TDC
            //Toast.makeText(getActivity(), "Solicitar Simulación", Toast.LENGTH_SHORT).show();

        }
        if(v == btnRecalcularSimulacion){
//            ((MenuPrincipalActivity) getActivity()).showTDC();
            delegate.saveOrDeleteSimulationRequest(false); //se manda false para eliminar

        }

        if(v == imgAvisoTDC){
            MenuPrincipalActivity.getInstance().showPopUpInforma(DetalleTDCViewController.this);
            Log.w("imagen","imagen nueva");
        }

        if(v == btnVerResumen){
            MainController.getInstance().showScreen(ResumenViewController.class);
            MainController.getInstance().setFlowFragment(DetalleTDCViewController.this);
        }
    }

    public void contratarTDC(){
        Log.i("tdc","DetalleTDCV, contratar");
        delegate.doDetalleAlternativasTDC();
    }


    public void preferences(){
        SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(ConstantsCredit.SHARED_IS_RESUMEN, false);
        editor.commit();
    }

    public void solicitarAction(){
        String params = "";
        ICommonSession sesion = MainController.getInstance().getSession();
        params = "nombredepila=" + sesion.getNombreCliente().split(" ")[0] + "&segmento=0TDC&credito="
                + getMonto() + "&idpagina=simulacion";

        Map<String,Object> click_paso2_operacion = new HashMap<String,Object>();
        click_paso2_operacion.put("inicio","event45");
        click_paso2_operacion.put("&&products","simulador;simulador:simulador tarjeta de credito");
        click_paso2_operacion.put("eVar12", "seleccion contactame");

        TrackingHelperCredit.trackPaso1Operacion(click_paso2_operacion);

    }

    public void simpleUpdateMenu(String cveProd){

        if(((MenuPrincipalActivity)getActivity()) == null)
            Log.i("eliminar", "Menu es nulo");

        Log.i("eliminar", "actualizaMenu AUTO, cveProd: " + cveProd);
        ((MenuPrincipalActivity)getActivity()).updateMenu(cveProd);
        getFragmentManager().beginTransaction().remove(((MenuPrincipalActivity) getActivity()).getActiveFragment()).commit();
        ((MenuPrincipalActivity)getActivity()).showTDC();
    }
}
