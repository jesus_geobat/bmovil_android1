package bbvacredit.bancomer.apicredit.models;

public class PromocionesCampania {
	
	private String cveCamp;
	
	private String desOferta;
	
	private String monto;

	public PromocionesCampania(String cveCamp, String desOferta, String monto) {
		super();
		this.cveCamp = cveCamp;
		this.desOferta = desOferta;
		this.monto = monto;
	}

	public String getCveCamp() {
		return cveCamp;
	}

	public void setCveCamp(String cveCamp) {
		this.cveCamp = cveCamp;
	}

	public String getDesOferta() {
		return desOferta;
	}

	public void setDesOferta(String desOferta) {
		this.desOferta = desOferta;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

}
