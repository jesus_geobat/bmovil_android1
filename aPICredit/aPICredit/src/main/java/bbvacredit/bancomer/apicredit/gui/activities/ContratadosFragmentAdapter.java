package bbvacredit.bancomer.apicredit.gui.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.gui.delegates.PrestamosContratadosDelegate;
import bbvacredit.bancomer.apicredit.models.CreditoContratado;

/**
 * Created by Carlos Garcia on 08/12/2015.
 */
public class ContratadosFragmentAdapter extends FragmentPagerAdapter {

    private int numeroFragmentos;
    private ArrayList<ContratadosFragment> listaFragments;
    PrestamosContratadosDelegate delegate;
    ArrayList<CreditoContratado> creditosContratados;

    //public ContratadosFragmentAdapter(FragmentManager fm, ArrayList<CreditoContratado> creditosContratados) {
    public ContratadosFragmentAdapter(FragmentManager fm, PrestamosContratadosDelegate delegate) {
        super(fm);

        this.delegate = delegate;

        listaFragments = new ArrayList<ContratadosFragment>();
        creditosContratados = delegate.getCreditosContratados();
        numeroFragmentos = creditosContratados.size();

        for (CreditoContratado credito: creditosContratados) {
            ContratadosFragment contratadoFragment = new ContratadosFragment();
            contratadoFragment.setCveProd(credito.getCveProd());
            contratadoFragment.setSaldo(credito.getSaldo());
            contratadoFragment.setDelegate(delegate);
            listaFragments.add(contratadoFragment);
        }

    }

    @Override
    public Fragment getItem(int position) {
        return listaFragments.get(position);
    }

    @Override
    public int getCount() {
        return numeroFragmentos;
    }

    public void botonArriba(int itemSelected) {
        ContratadosFragment fragmentSelected = (ContratadosFragment) getItem(itemSelected);
        fragmentSelected .showContratar(delegate.getArrayProductos());
    }

    public void botonAbajo() {
        for(ContratadosFragment fragment : listaFragments) {
            fragment.hideContratar();
        }
    }

    public void cambiaValores() {
        for (ContratadosFragment contratadoFragment: listaFragments) {
            contratadoFragment.cambiaValores();
        }
    }
}
