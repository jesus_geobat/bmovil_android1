package bbvacredit.bancomer.apicredit.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.CreditJSONAble;

import bbvacredit.bancomer.apicredit.io.ParsingHandlerJSON;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;


public class DetalleAlternativa implements ParsingHandlerJSON, CreditJSONAble {

	private String estado;

	//ILC
	private String monDese;
	
	private String idCamCRM;

	private String cveCanal;

	private String bin;

	private String cuenta;

	private String cnlCRM;

	private String linCredAct;

	private String linCredFinal;

	private String codProd;

	private String CAT;

	private String fechCat;


	//CONSUMO
	private String plazo;

	private String importe;

	private String ctaVinc;

	private String plazoDes;

	private String pagoMenFijo;

	private String estatusOferta;

	private String folioUG;

	private String totalPagos;

	private String descDiasPago;

	private String bscPagar;

	private String impSegSal;

	private String tasaAnual;

	private String tasaMensual;

	private String producto;

	private String libre;

	private String idCampania;


	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMonDese() {
		return monDese;
	}

	public void setMonDese(String monDese) {
		this.monDese = monDese;
	}

	public String getIdCamCRM() {
		return idCamCRM;
	}

	public void setIdCamCRM(String idCamCRM) {
		this.idCamCRM = idCamCRM;
	}

	public String getCveCanal() {
		return cveCanal;
	}

	public void setCveCanal(String cveCanal) {
		this.cveCanal = cveCanal;
	}

	public String getBin() {return bin;	}

	public void setBin(String bin) {this.bin = bin;	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getCnlCRM() {
		return cnlCRM;
	}

	public void setCnlCRM(String cnlCRM) {
		this.cnlCRM = cnlCRM;
	}

	public String getLinCredAct() {
		return linCredAct;
	}

	public void setLinCredAct(String linCredAct) {
		this.linCredAct = linCredAct;
	}

	public String getLinCredFinal() {
		return linCredFinal;
	}

	public void setLinCredFinal(String linCredFinal) {
		this.linCredFinal = linCredFinal;
	}

	public String getCodProd() {
		return codProd;
	}

	public void setCodProd(String codProd) {
		this.codProd = codProd;
	}

	public String getCAT() {
		return CAT;
	}

	public void setCAT(String cAT) {
		CAT = cAT;
	}

	public String getFechCat() {
		return fechCat;
	}

	public void setFechCat(String fechCat) {
		this.fechCat = fechCat;
	}

	public String getPlazo() {
		return plazo;
	}

	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getCtaVinc() {
		return ctaVinc;
	}

	public void setCtaVinc(String ctaVinc) {
		this.ctaVinc = ctaVinc;
	}

	public String getPlazoDes() {
		return plazoDes;
	}

	public void setPlazoDes(String plazoDes) {
		this.plazoDes = plazoDes;
	}

	public String getPagoMenFijo() {
		return pagoMenFijo;
	}

	public void setPagoMenFijo(String pagoMenFijo) {
		this.pagoMenFijo = pagoMenFijo;
	}

	public String getEstatusOferta() {
		return estatusOferta;
	}

	public void setEstatusOferta(String estatusOferta) {
		this.estatusOferta = estatusOferta;
	}

	public String getFolioUG() {
		return folioUG;
	}

	public void setFolioUG(String folioUG) {
		this.folioUG = folioUG;
	}

	public String getTotalPagos() {
		return totalPagos;
	}

	public void setTotalPagos(String totalPagos) {
		this.totalPagos = totalPagos;
	}

	public String getDescDiasPago() {
		return descDiasPago;
	}

	public void setDescDiasPago(String descDiasPago) {
		this.descDiasPago = descDiasPago;
	}

	public String getBscPagar() {
		return bscPagar;
	}

	public void setBscPagar(String bscPagar) {
		this.bscPagar = bscPagar;
	}

	public String getImpSegSal() {
		return impSegSal;
	}

	public void setImpSegSal(String impSegSal) {
		this.impSegSal = impSegSal;
	}

	public String getTasaAnual() {
		return tasaAnual;
	}

	public void setTasaAnual(String tasaAnual) {
		this.tasaAnual = tasaAnual;
	}

	public String getTasaMensual() {
		return tasaMensual;
	}

	public void setTasaMensual(String tasaMensual) {
		this.tasaMensual = tasaMensual;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getLibre() {
		return libre;
	}

	public void setLibre(String libre) {
		this.libre = libre;
	}

	public String getIdCampania() {
		return idCampania;
	}

	public void setIdCampania(String idCampania) {
		this.idCampania = idCampania;
	}


	//private String cvePlazo;

	//private String cveProd;
	
	//private String cveSubp;
	
	//private String numTDC;
	
	//private String tasa;

	//private String pagMens;
	
	//private  String contrato;
	
	//private String fechInicio;
	
	//private String fechFin;

	//private String destino;

	//private String moneda;
	
	//private String fechSol;

	//private String fechLim;
	
	//private String numTarj;
	
	//private String actEcon;
	
	//private String ctaDepos;
	
	//private String desFechCat;



   /*
	public String getCveProd() {return cveProd;	}

	public void setCveProd(String cveProd) {this.cveProd = cveProd;	}

	public String getCveSubp() {
		return cveSubp;
	}

	public void setCveSubp(String cveSubp) {
		this.cveSubp = cveSubp;
	}

	public String getNumTDC() {
		return numTDC;
	}

	public void setNumTDC(String numTDC) {
		this.numTDC = numTDC;
	}

	public String getCvePlazo() {return cvePlazo; }

	public void setCvePlazo(String cvePlazo) { this.cvePlazo = cvePlazo; }

	public String getTasa() {return tasa;}

	public void setTasa(String tasa) {this.tasa = tasa;	}

	public String getPagMens() {
		return pagMens;
	}

	public void setPagMens(String pagMens) {
		this.pagMens = pagMens;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getFechInicio() {
		return fechInicio;
	}

	public void setFechInicio(String fechInicio) {
		this.fechInicio = fechInicio;
	}

	public String getFechFin() {
		return fechFin;
	}

	public void setFechFin(String fechFin) {
		this.fechFin = fechFin;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getMoneda() {	return moneda; }

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getFechSol() { return fechSol; }

	public void setFechSol(String fechSol) { this.fechSol = fechSol; }

	public String getCtaVinc() {
		return ctaVinc;
	}

	public void setCtaVinc(String ctaVinc) {
		this.ctaVinc = ctaVinc;
	}

	public String getFechLim() {
		return fechLim;
	}

	public void setFechLim(String fechLim) {
		this.fechLim = fechLim;
	}

	public String getNumTarj() {
		return numTarj;
	}

	public void setNumTarj(String numTarj) {
		this.numTarj = numTarj;
	}

	public String getActEcon() {
		return actEcon;
	}

	public void setActEcon(String actEcon) {
		this.actEcon = actEcon;
	}

	public String getCtaDepos() { return ctaDepos; 	}

	public void setCtaDepos(String ctaDepos) {	this.ctaDepos = ctaDepos; }

	public String getDesFechCat() {
		return desFechCat;
	}

	public void setDesFechCat(String desFechCat) {
		this.desFechCat = desFechCat;
	}

	*/
	
	
	public DetalleAlternativa(){
		
	}
	
	
	
	
	@Override
	public void fromJSON(String jsonString) {
		//Modificacion 50986
		JSONObject obj;
		try {
			
			obj = new JSONObject(jsonString);
			this.estado =  Tools.getJSONChecked(obj, ServerConstantsCredit.JSON_ESTADO_RESPUESTA,String.class);
			//this.monDese = Tools.getJSONChecked(obj, "monDese", String.class);
			//this.idCamCRM = Tools.getJSONChecked(obj, "idCamCRM", String.class);

			if(Session.getInstance().getCveCamp().equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO))
			{

				this.monDese = Tools.getJSONChecked(obj, "monDese", String.class);
				//this.idCamCRM = Tools.getJSONChecked(obj, "idCamCRM", String.class);
				this.idCampania = Tools.getJSONChecked(obj, "idCampania", String.class);
				this.cveCanal = Tools.getJSONChecked(obj, "cveCanal", String.class);
				this.bin = Tools.getJSONChecked(obj, "bin", String.class);
				this.cuenta = Tools.getJSONChecked(obj, "cuenta", String.class);
				this.cnlCRM = Tools.getJSONChecked(obj, "cnlCRM", String.class);
				this.linCredAct = Tools.getJSONChecked(obj, "linCredAct", String.class);
				this.linCredFinal = Tools.getJSONChecked(obj, "linCredFinal", String.class);
				this.codProd = Tools.getJSONChecked(obj, "codProd", String.class);
				this.CAT = Tools.getJSONChecked(obj, "cat", String.class);
				this.fechCat = Tools.getJSONChecked(obj, "fechaCat", String.class);

			}
			else
			{
				if(obj.has("plazo"))
					this.plazo = Tools.getJSONChecked(obj, "plazo", String.class);
				if(obj.has("importe"))
					this.importe = Tools.getJSONChecked(obj, "importe", String.class);
				if(obj.has("cuentaVinc"))
					this.ctaVinc = Tools.getJSONChecked(obj, "cuentaVinc", String.class);
				if(obj.has("plazoDes"))
					this.plazoDes = Tools.getJSONChecked(obj, "plazoDes", String.class);
				if(obj.has("Cat"))
					this.CAT = Tools.getJSONChecked(obj, "Cat", String.class);
				if(obj.has("fechaCat"))
					this.fechCat = Tools.getJSONChecked(obj, "fechaCat", String.class);
				if(obj.has("pagoMenFijo"))
					this.pagoMenFijo = Tools.getJSONChecked(obj, "pagoMenFijo", String.class);
				if(obj.has("estatusOferta"))
					this.estatusOferta = Tools.getJSONChecked(obj, "estatusOferta", String.class);
				if(obj.has("folioUG"))
					this.folioUG = Tools.getJSONChecked(obj, "folioUG", String.class);
				if(obj.has("totalPagos"))
					this.totalPagos = Tools.getJSONChecked(obj, "totalPagos", String.class);
				if(obj.has("descDiasPago"))
					this.descDiasPago = Tools.getJSONChecked(obj, "descDiasPago", String.class);
				if(obj.has("bscPagar"))
					this.bscPagar = Tools.getJSONChecked(obj, "bscPagar", String.class);
				if(obj.has("impSegSal"))
					this.impSegSal = Tools.getJSONChecked(obj, "impSegSal", String.class);
				if(obj.has("tasaAnual"))
					this.tasaAnual = Tools.getJSONChecked(obj, "tasaAnual", String.class);
				if(obj.has("tasaMensual"))
					this.tasaMensual = Tools.getJSONChecked(obj, "tasaMensual", String.class);
				if(obj.has("producto"))
					this.producto = Tools.getJSONChecked(obj, "producto", String.class);
				//if(obj.has("idCamCRM"))
					//this.idCamCRM = Tools.getJSONChecked(obj, "idCamCRM", String.class);
				if(obj.has("libre"))
					this.libre = Tools.getJSONChecked(obj, "libre", String.class);
				if(obj.has("idCampania"))
					this.idCampania = Tools.getJSONChecked(obj,"idCampania", String.class);

				/*
				if(obj.has("cveProd"))
					this.cveProd = Tools.getJSONChecked(obj, "cveProd", String.class);
				if(obj.has("cveSubp"))
					this.cveSubp = Tools.getJSONChecked(obj, "cveSubp", String.class);
				if(obj.has("numTDC"))
					this.numTDC = Tools.getJSONChecked(obj, "numTDC", String.class);
				if(obj.has("cvePlazo"))
					this.cvePlazo = Tools.getJSONChecked(obj, "cvePlazo", String.class);
				if(obj.has("tasa"))
					this.tasa = Tools.getJSONChecked(obj, "tasa", String.class);
				if(obj.has("cat"))
					this.CAT = Tools.getJSONChecked(obj, "cat", String.class);
				if(obj.has("pagMens"))
					this.pagMens = Tools.getJSONChecked(obj, "pagMens", String.class);
				if(obj.has("contrato"))
					this.contrato = Tools.getJSONChecked(obj, "contrato", String.class);
				if(obj.has("fechInicio"))
					this.fechInicio = Tools.getJSONChecked(obj, "fechInicio", String.class);
				if(obj.has("fechFin"))
					this.fechFin = Tools.getJSONChecked(obj, "fechFin", String.class);
				if(obj.has("destino"))
					this.destino = Tools.getJSONChecked(obj, "destino", String.class);
				if(obj.has("moneda"))
					this.moneda = Tools.getJSONChecked(obj, "moneda", String.class);
				if(obj.has("fechSol"))
					this.fechSol = Tools.getJSONChecked(obj, "fechSol", String.class);
				if(obj.has("ctaVinc"))
					this.ctaVinc = Tools.getJSONChecked(obj, "ctaVinc", String.class);
				if(obj.has("fechLim"))
					this.fechLim = Tools.getJSONChecked(obj, "fechLim", String.class);
				if(obj.has("numTarj"))
					this.numTarj = Tools.getJSONChecked(obj, "numTarj", String.class);
				if(obj.has("actEcon"))	
					this.actEcon = Tools.getJSONChecked(obj, "actEcon", String.class);
				if(obj.has("ctaDepos"))
					this.ctaDepos = Tools.getJSONChecked(obj, "ctaDepos", String.class);
				if(obj.has("fechCat"))
					this.fechCat = Tools.getJSONChecked(obj, "fechCat", String.class);
				if(obj.has("desFechCat"))
					this.desFechCat = Tools.getJSONChecked(obj, "desFechCat", String.class);
				*/
			}			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("Error caused by", e.getMessage());
			e.printStackTrace();
		}
			
		
	}


	@Override
	public void processJSON(String jsonString) throws JSONException {
        //Modificacion 50986
		JSONObject obj;
		try {
			
			obj = new JSONObject(jsonString);
			this.estado =  Tools.getJSONChecked(obj, ServerConstantsCredit.JSON_ESTADO_RESPUESTA,String.class);
			this.monDese = Tools.getJSONChecked(obj, "monDese", String.class);
			this.idCamCRM = Tools.getJSONChecked(obj, "idCamCRM", String.class);
			
			if(Session.getInstance().getCveCamp().equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO))
			{
				Log.e("Entra parseo ILC", "ok");
				
				this.cveCanal = Tools.getJSONChecked(obj, "cveCanal", String.class);
				this.bin = Tools.getJSONChecked(obj, "bin", String.class);
				this.cuenta = Tools.getJSONChecked(obj, "cuenta", String.class);
				this.cnlCRM = Tools.getJSONChecked(obj, "cnlCRM", String.class);
				this.linCredAct = Tools.getJSONChecked(obj, "linCredAct", String.class);
				this.linCredFinal = Tools.getJSONChecked(obj, "linCredFinal", String.class);
				this.codProd = Tools.getJSONChecked(obj, "codProd", String.class);
				this.CAT = Tools.getJSONChecked(obj, "cat", String.class);
				this.fechCat = Tools.getJSONChecked(obj, "fechaCat", String.class);
			}
			else
			{
				
				Log.e("Entra parseo Consumo", "ok");

				if(obj.has("plazo"))
					this.plazo = Tools.getJSONChecked(obj, "plazo", String.class);
				if(obj.has("importe"))
					this.importe = Tools.getJSONChecked(obj, "importe", String.class);
				if(obj.has("cuentaVinc"))
					this.ctaVinc = Tools.getJSONChecked(obj, "cuentaVinc", String.class);
				if(obj.has("plazoDes"))
					this.plazoDes = Tools.getJSONChecked(obj, "plazoDes", String.class);
				if(obj.has("Cat"))
					this.CAT = Tools.getJSONChecked(obj, "Cat", String.class);
				if(obj.has("fechaCat"))
					this.fechCat = Tools.getJSONChecked(obj, "fechaCat", String.class);
				if(obj.has("pagoMenFijo"))
					this.pagoMenFijo = Tools.getJSONChecked(obj, "pagoMenFijo", String.class);
				if(obj.has("estatusOferta"))
					this.estatusOferta = Tools.getJSONChecked(obj, "estatusOferta", String.class);
				if(obj.has("folioUG"))
					this.folioUG = Tools.getJSONChecked(obj, "folioUG", String.class);
				if(obj.has("totalPagos"))
					this.totalPagos = Tools.getJSONChecked(obj, "totalPagos", String.class);
				if(obj.has("descDiasPago"))
					this.descDiasPago = Tools.getJSONChecked(obj, "descDiasPago", String.class);
				if(obj.has("bscPagar"))
					this.bscPagar = Tools.getJSONChecked(obj, "bscPagar", String.class);
				if(obj.has("impSegSal"))
					this.impSegSal = Tools.getJSONChecked(obj, "impSegSal", String.class);
				if(obj.has("tasaAnual"))
					this.tasaAnual = Tools.getJSONChecked(obj, "tasaAnual", String.class);
				if(obj.has("tasaMensual"))
					this.tasaMensual = Tools.getJSONChecked(obj, "tasaMensual", String.class);
				if(obj.has("producto"))
					this.producto = Tools.getJSONChecked(obj, "producto", String.class);
				if(obj.has("idCamCRM"))
					this.idCamCRM = Tools.getJSONChecked(obj, "idCamCRM", String.class);
				if(obj.has("libre"))
					this.libre = Tools.getJSONChecked(obj, "libre", String.class);

				/*
				if(obj.has("cveProd"))
					this.cveProd = Tools.getJSONChecked(obj, "cveProd", String.class);
				if(obj.has("cveSubp"))
					this.cveSubp = Tools.getJSONChecked(obj, "cveSubp", String.class);
				if(obj.has("numTDC"))
					this.numTDC = Tools.getJSONChecked(obj, "numTDC", String.class);
				if(obj.has("cvePlazo"))
					this.cvePlazo = Tools.getJSONChecked(obj, "cvePlazo", String.class);
				if(obj.has("tasa"))
					this.tasa = Tools.getJSONChecked(obj, "tasa", String.class);
				if(obj.has("cat"))
					this.CAT = Tools.getJSONChecked(obj, "cat", String.class);
				if(obj.has("pagMens"))
					this.pagMens = Tools.getJSONChecked(obj, "pagMens", String.class);
				if(obj.has("contrato"))
					this.contrato = Tools.getJSONChecked(obj, "contrato", String.class);
				if(obj.has("fechInicio"))
					this.fechInicio = Tools.getJSONChecked(obj, "fechInicio", String.class);
				if(obj.has("fechFin"))
					this.fechFin = Tools.getJSONChecked(obj, "fechFin", String.class);
				if(obj.has("destino"))
					this.destino = Tools.getJSONChecked(obj, "destino", String.class);
				if(obj.has("moneda"))
					this.moneda = Tools.getJSONChecked(obj, "moneda", String.class);
				if(obj.has("fechSol"))
					this.fechSol = Tools.getJSONChecked(obj, "fechSol", String.class);
				if(obj.has("ctaVinc"))
					this.ctaVinc = Tools.getJSONChecked(obj, "ctaVinc", String.class);
				if(obj.has("fechLim"))
					this.fechLim = Tools.getJSONChecked(obj, "fechLim", String.class);
				if(obj.has("numTarj"))
					this.numTarj = Tools.getJSONChecked(obj, "numTarj", String.class);
				if(obj.has("actEcon"))	
					this.actEcon = Tools.getJSONChecked(obj, "actEcon", String.class);
				if(obj.has("ctaDepos"))
					this.ctaDepos = Tools.getJSONChecked(obj, "ctaDepos", String.class);
				if(obj.has("fechCat"))
					this.fechCat = Tools.getJSONChecked(obj, "fechCat", String.class);
				if(obj.has("desFechCat"))
					this.desFechCat = Tools.getJSONChecked(obj, "desFechCat", String.class);
				*/
			}			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("Error caused by", e.getMessage());
			e.printStackTrace();
		}
			
			
	}

	@Override
	public String toJSON() {
		// TODO Auto-generated method stub
		return null;
	}



}
