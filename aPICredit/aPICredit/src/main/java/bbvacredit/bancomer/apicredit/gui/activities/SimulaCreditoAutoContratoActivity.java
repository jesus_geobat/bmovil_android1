package bbvacredit.bancomer.apicredit.gui.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.timer.TimerController;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.SimulaCreditoAutoDelegate;
import bbvacredit.bancomer.apicredit.models.Producto;
import bbvacredit.bancomer.apicredit.R;
import tracking.TrackingHelperCredit;

public class SimulaCreditoAutoContratoActivity extends BaseActivity implements OnClickListener, OnSeekBarChangeListener {
	
	private SimulaCreditoAutoDelegate delegate;
	private ImageButton backButton;
	private ImageButton btnSimular;
	
	// Menu Button
	private Button resumenBtn;
	private Button btnContacto;
	
	// Botones Seekbar
	private Button seekPlus;
	private Button seekMinus;
	private SeekBar seekbar;
	private Spinner spinnerEstado;
	private Spinner spinnerPlazo;
	
	private boolean isOcultaInfoEnabled;
	
	private String plazoSel = "";
	private int estado=0;
	private String estadoSel = "";
	
	//Label monto disponible
	private TextView disponible;

	private MainController init = MainController.getInstance();
	
	public SeekBar getSeekbar() {
		return seekbar;
	}

	public void setSeekbar(SeekBar seekbar) {
		this.seekbar = seekbar;
	}

	public Spinner getSpinnerEstado() {
		return spinnerEstado;
	}

	public void setSpinnerEstado(Spinner spinnerEstado) {
		this.spinnerEstado = spinnerEstado;
	}

	public Spinner getSpinnerPlazo() {
		return spinnerPlazo;
	}

	public void setSpinnerPlazo(Spinner spinnerPlazo) {
		this.spinnerPlazo = spinnerPlazo;
	}

	private MainController parentManager = MainController.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState,SHOW_HEADER|SHOW_TITLE, R.layout.activity_simula_credito_auto_contrato);
		isOnForeground=true;
		isOcultaInfoEnabled=false;
		TrackingHelperCredit.trackState("creditsimAutoCont", parentManager.estados);
		MainController.getInstance().setCurrentActivity(this);
		init();
	}
	
	private void init(){
		Tools.setIsContratacionPreference(true);
		ocultaInfoNC();
		Tools.getCurrentSession().setAutoCAct(this);
		delegate = new SimulaCreditoAutoDelegate();
		inicializaBotones();
		mapearBotones();	
		
		if((delegate.getProducto()!=null) && delegate.getProducto().getIndSimBoolean()) this.pintarInfoTabla();
				
		scaleToCurrentScreen();
		
		setEventToSpinner();
		//Tools.getCurrentSession().setVisibilityButton(this, R.id.simCAutoCBottomMenuRC);
	}
	
	private void inicializaBotones(){
		
		//backButton = (ImageButton)findViewById(R.id.headerRefresh);
		btnSimular = (ImageButton)findViewById(R.id.simCAutoBtnSimular);
		resumenBtn = (Button)findViewById(R.id.simCAutoCBottomMenuRC);
		btnContacto = (Button)findViewById(R.id.simCAutoCBottomContacto);
		seekPlus = (Button)findViewById(R.id.simCAutoCSeekbarMin);
		seekMinus = (Button)findViewById(R.id.simCAutoCSeekbarPlus);
		seekbar = (SeekBar)findViewById(R.id.simCAutoCSeekBar);
		spinnerEstado = (Spinner)findViewById(R.id.simCAutoCSpinner);
		spinnerPlazo = (Spinner)findViewById(R.id.simCAutoCSpinner2);
		disponible = (TextView)findViewById(R.id.simCAutoCLMon);
		
		
	}
	
	private void mapearBotones(){		
		//backButton = (ImageButton)findViewById(R.id.headerRefresh);
		backButton.setOnClickListener(this);
		
		btnSimular = (ImageButton)findViewById(R.id.simCAutoBtnSimular);
		btnSimular.setOnClickListener(this);
		
		resumenBtn = (Button)findViewById(R.id.simCAutoCBottomMenuRC);
		resumenBtn.setOnClickListener(this);
		
		btnContacto = (Button)findViewById(R.id.simCAutoCBottomContacto);
		btnContacto.setOnClickListener(this);
				
		seekPlus = (Button)findViewById(R.id.simCAutoCSeekbarMin);
		seekPlus.setOnClickListener(this);
		
		seekMinus = (Button)findViewById(R.id.simCAutoCSeekbarPlus);
		seekMinus.setOnClickListener(this);
		
		seekbar = (SeekBar)findViewById(R.id.simCAutoCSeekBar);
		seekbar.setMax(delegate.getMontoMax());
		seekbar.setProgress(delegate.getMontoProgress());
		seekbar.setOnSeekBarChangeListener(this);		
		seekbar.refreshDrawableState();
		
		TextView valueSeekBar =(TextView) findViewById(R.id.simCAutoCSeekbarText2);
		valueSeekBar.setText(GuiTools.getMoneyString(delegate.getMontoProgress().toString()));
		
		//TextView disponible = (TextView)findViewById(R.id.simCAutoCLMon);
		if(delegate.getProducto() != null){
			disponible.setText(GuiTools.getMoneyString(String.valueOf(delegate.getProducto().getSubproducto().get(0).getMonMax())));
		}else{
			disponible.setText(GuiTools.getMoneyString(String.valueOf(0)));
		}
		
		spinnerEstado = (Spinner)findViewById(R.id.simCAutoCSpinner);
		ArrayAdapter<String> dataAdapterEstado =
				new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, delegate.getEstados());
		dataAdapterEstado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerEstado.setAdapter(dataAdapterEstado);
		spinnerPlazo = (Spinner)findViewById(R.id.simCAutoCSpinner2);
		ArrayAdapter<String> dataAdapterPlazos =
				new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, delegate.getPlazos());
		dataAdapterPlazos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerPlazo.setAdapter(dataAdapterPlazos);
		
		/*Nuevo*/
				
		spinnerEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){
				if(spinnerEstado.getId() == R.id.simCAutoCSpinner)
			     {
					disponible.setText(getMontoMaximo());
					estado++;
					if(estado>1 && delegate.getProducto().getIndSimBoolean() ){
			    	delegate.cargarPlazosOnCode(spinnerEstado.getSelectedItem().toString());
					setPlazos();
					}else if (!delegate.getProducto().getIndSimBoolean()){
						delegate.cargarPlazosOnCode(spinnerEstado.getSelectedItem().toString());
						setPlazos();
					}
			     }

			}


			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		/*fin nuevo*/



	}

	private String getMontoMaximo() {
		Producto p = delegate.getProducto();
		for(int i=0;i<p.getSubproducto().size();i++){
			if(p.getSubproducto().get(i).getDesSubp().equalsIgnoreCase(spinnerEstado.getSelectedItem().toString())){
				//if(p.getSubproducto().get(i).getMonMax()<seekbar.getMax() && seekbar.getProgress()>p.getSubproducto().get(i).getMonMax()){
				seekbar.setMax(p.getSubproducto().get(i).getMonMax());
					seekbar.setProgress(p.getSubproducto().get(i).getMonMax());
					delegate.setMontoMax(p.getSubproducto().get(i).getMonMax());
					delegate.setMontoMin(p.getSubproducto().get(i).getMonMin());
				//}
				//seekbar.setMax(p.getSubproducto().get(i).getMonMax());
				return GuiTools.getMoneyString(String.valueOf(p.getSubproducto().get(i).getMonMax()));
			}
		}
		return GuiTools.getMoneyString(String.valueOf(p.getSubproducto().get(0).getMonMax()));

	}

    private void ocultaInfoNC(){

		TextView info = (TextView)findViewById(R.id.simCAutoCInfo);
		info.setVisibility(View.INVISIBLE);

		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCAutoCLinearLayoutTexto);
		tlayout.setVisibility(View.INVISIBLE);

		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.simCAutoCCatInfo);
		tableLayout.setVisibility(View.INVISIBLE);

		btnContacto = (Button)findViewById(R.id.simCAutoCBottomContacto);
		btnContacto.setVisibility(View.INVISIBLE);
       //  subir();


	}

	public void subir()
	{

		LinearLayout p=(LinearLayout)findViewById(R.id.simCAutoCBottomLayout);
		ImageButton bn=(ImageButton)findViewById(R.id.simCAutoBtnSimular);
		p.setY(Tools.getPosicionBarraInferiorS(bn));



	}
	public void bajar()
	{
		LinearLayout d=(LinearLayout)findViewById(R.id.simCHipoCBodyLayout);
		LinearLayout p=(LinearLayout)findViewById(R.id.simCAutoCBottomLayout);
		LinearLayout p1=(LinearLayout)findViewById(R.id.simCAutoCCatInfo);

		p1.setY(p1.getY()+5);
		p.setY(Tools.getPosicionBarraInferiorB(d));


	}


	private void muestraInfoNC(){


		TextView info = (TextView)findViewById(R.id.simCAutoCInfo);
		info.setVisibility(View.VISIBLE);

		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCAutoCLinearLayoutTexto);
		tlayout.setVisibility(View.VISIBLE);

		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.simCAutoCCatInfo);
		tableLayout.setVisibility(View.VISIBLE);

		btnContacto = (Button)findViewById(R.id.simCAutoCBottomContacto);
		btnContacto.setVisibility(View.VISIBLE);
		//bajar();
	}

	public void mockData(){
		Spinner spinner = (Spinner) findViewById(R.id.simCAutoCSpinner);
		Spinner spinner2 = (Spinner) findViewById(R.id.simCAutoCSpinner2);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.estado_vehiculo_array, android.R.layout.simple_spinner_item);
		ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.plazo_array, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);
		spinner2.setAdapter(adapter2);
	}
	/**
	 * Escala los elementos de la pantalla para la resoluci?n actual.
	 */
	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scale(findViewById(R.id.simCAutoBtnSimular));
		guiTools.scale(findViewById(R.id.simCHipoCLayout));
		guiTools.scale(findViewById(R.id.simCHipoCBodyLayout));
		guiTools.scale(findViewById(R.id.simCAutoCTitle),true);
		guiTools.scale(findViewById(R.id.simCAutoCLayoutSubTitle));
		guiTools.scale(findViewById(R.id.simCAutoCSubTitleTCImg));
		guiTools.scale(findViewById(R.id.simCAutoCSubTitleMiddleTxt),true);
		guiTools.scale(findViewById(R.id.simCAutoCLinearLayout));
		guiTools.scale(findViewById(R.id.simCAutoCLText),true);
		guiTools.scale(findViewById(R.id.simCAutoCLMon),true);
		guiTools.scale(findViewById(R.id.simCAutoCMTextSL),true);
		guiTools.scale(findViewById(R.id.simCAutoCLineaSeparacion));
		guiTools.scale(findViewById(R.id.simCAutoCEstado),true);
		guiTools.scale(findViewById(R.id.simCAutoCSpinner));
		guiTools.scale(findViewById(R.id.simCAutoCPlazo),true);
		guiTools.scale(findViewById(R.id.simCAutoCSpinner2));
		guiTools.scale(findViewById(R.id.simCAutoCBottomLayout));
		guiTools.scale(findViewById(R.id.simCAutoCBottomMenuRC),true);
		guiTools.scale(findViewById(R.id.simCAutoCBottomContacto),true);
		guiTools.scale(findViewById(R.id.simCAutoClblResultadoTasa),true);
		guiTools.scale(findViewById(R.id.simCAutoClblResultadoSimulacion2),true);
		guiTools.scale(findViewById(R.id.simCAutoClblResultadoPago),true);
		guiTools.scale(findViewById(R.id.simCAutoClblResultadoSimulacion),true);
		guiTools.scale(findViewById(R.id.simCAutoCLinearLayoutTexto));

		guiTools.scale(findViewById(R.id.simCAutoCInfo),true);
		guiTools.scale(findViewById(R.id.simCAutoCCatFecha),true);
		guiTools.scale(findViewById(R.id.simCAutoCCatText),true);
		guiTools.scale(findViewById(R.id.simCAutoCCatPorc),true);
		guiTools.scale(findViewById(R.id.simCAutoCCat),true);
		guiTools.scale(findViewById(R.id.simCAutoCCatInfo));

		guiTools.scale(findViewById(R.id.simCAutoCSeekbarText),true);
		guiTools.scale(findViewById(R.id.simCAutoCSeekbarText2),true);
		guiTools.scale(findViewById(R.id.simCAutoCSeekBar));
		guiTools.scale(findViewById(R.id.simCAutoCSeekbarLayout));
		guiTools.scale(findViewById(R.id.simCAutoCSeekbarPlus));
		guiTools.scale(findViewById(R.id.simCAutoCSeekbarMin));
	}

	@Override
	public void onClick(View v) {
		isOnForeground = false;
		/*if(v.getId() == R.id.headerRefresh){
			delegate.setRet();
			delegate.redirectToView(OldMenuPrincipalActivity.class);
		}else*/if(v.getId() == R.id.simCAutoBtnSimular)
		{
			isOcultaInfoEnabled=false;
		//	delegate.doRecalculoContract(this);
Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
			click_paso2_operacion.put("evento_realizada","event52");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador credito auto");
			click_paso2_operacion.put("eVar12","simulacion realizada");
			TrackingHelperCredit.trackSimulacionRealizada(click_paso2_operacion);
			deleteSimulation();
		}else if(v.getId() == R.id.simCAutoCBottomMenuRC){
			delegate.redirectToView(ResumenActivity.class);
		}else if(v.getId() == R.id.simCAutoCBottomContacto)
		{
			isOnForeground = false;
			String params = "";
			ICommonSession sesion = init.getSession();
			Log.d("Credit", "params = " + params);
			params = "nombredepila=" + sesion.getNombreCliente().split(" ")[0] + "&segmento=AUTO&credito=" + seekbar.getProgress() + "&idpagina=simulacion";
			Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
			click_paso2_operacion.put("inicio","event45");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador credito auto");
			click_paso2_operacion.put("eVar12", "seleccion contactame");
			TrackingHelperCredit.trackPaso1Operacion(click_paso2_operacion);
			PopUpContactameActivity.muestraPop(this, params);
			//JQH PopUpContactameActivity.muestraPop(this);
			//Log.e("Selecciono contactar","....");
		}else if(v.getId() == R.id.simCAutoCSeekbarPlus){
			seekbar = (SeekBar)findViewById(R.id.simCAutoCSeekBar);
			if(seekbar.getProgress() < seekbar.getMax()){
				Integer val = seekbar.getMax() - seekbar.getProgress();
				if(val>=1000){
					seekbar.setProgress(seekbar.getProgress()+1000);
				}else{
					seekbar.setProgress(seekbar.getProgress()+val);
				}
				if(seekbar.getProgress()<=delegate.getMontoMax()){
					delegate.doRecalculoContract(this);
					}
			}
		}else if(v.getId() == R.id.simCAutoCSeekbarMin){
			seekbar = (SeekBar)findViewById(R.id.simCAutoCSeekBar);
			if(seekbar.getProgress() > delegate.getMontoMin()){
				if((seekbar.getProgress() >= 1000)&&(delegate.getMontoMin() <= (seekbar.getProgress()-1000))){
					seekbar.setProgress(seekbar.getProgress()-1000);
				}else{
					seekbar.setProgress(delegate.getMontoMin());
				}
				if(seekbar.getProgress()>=delegate.getMontoMin()){
					delegate.doRecalculoContract(this);
					}
			}
		}
	}


	public void pintarInfoTabla(){

		//Quitar bloqueo
		MainController.getInstance().ocultaIndicadorActividad();

		ocultaInfoNC();

		TextView porcentajeCat = (TextView)findViewById(R.id.simCAutoCCatPorc);
		TextView fechaCat = (TextView)findViewById(R.id.simCAutoCCatFecha);
		TextView resultadoPago = (TextView)findViewById(R.id.simCAutoClblResultadoPago);
		TextView resultadoTasa = (TextView)findViewById(R.id.simCAutoClblResultadoTasa);

		Producto p = delegate.getProducto();

		//seekbar.setProgress(Integer.parseInt(p.getMontoDeseS()));
		if ( (delegate.getProducto()!=null) && delegate.getProducto().getIndSimBoolean() ){
			estadoSel = p.getDessubpE();
			spinnerEstado.setSelection(getPosSelectedSpinner(spinnerEstado, estadoSel));
			//setPlazos();
			plazoSel = p.getDesPlazoE();	//movida
			spinnerPlazo.setSelection(getPosSelectedSpinner(spinnerPlazo, plazoSel));

		}
		if(spinnerEstado != null && spinnerEstado.getSelectedItem() != null){
			estadoSel = spinnerEstado.getSelectedItem().toString();
		}
		if(spinnerPlazo!= null && spinnerPlazo.getSelectedItem() != null){
			plazoSel = spinnerPlazo.getSelectedItem().toString();
		}



		String textoPorcCat = p.getCATS();
		porcentajeCat.setText(textoPorcCat);
		fechaCat.setText("Fecha de cálculo al "+p.getFechaCatS().toLowerCase());

		resultadoPago.setText(GuiTools.getMoneyString(String.valueOf(p.getPagMProdS())));
		disponible.setText(GuiTools.getMoneyString(String.valueOf(p.getSubproducto().get(0).getMonMax())));
		resultadoTasa.setText(p.getTasaS()+"%");

		muestraInfoNC();
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		TextView text = (TextView)findViewById(R.id.simCAutoCSeekbarText2);
		text.setText(GuiTools.getMoneyString(String.valueOf(seekbar.getProgress())));
		ocultaInfoNC();

		if(seekbar.getProgress() == delegate.getMontoMax()){
			seekbar.setProgress(seekbar.getProgress());
		}else{
			seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
		}

//		if(seekbar.getProgress()>=delegate.getMontoMin()) {
//			delegate.doRecalculoContract(this);
//		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
		if(seekbar.getProgress()>=delegate.getMontoMin()){
			//delegate.doRecalculoContract(this);
		}else{
			seekbar.setProgress(delegate.getMontoMin());
			//delegate.doRecalculoContract(this);
		}
	}

	//Nuevo
	public void setPlazos(){
		ArrayAdapter<String> dataAdapter =
				new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, delegate.getPlazos());
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerPlazo.setAdapter(dataAdapter);
	}
	//Nuevo
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

	}

	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(OldMenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}
    }

	private void setEventToSpinner()
	{
		spinnerPlazo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
		    	if(isOcultaInfoEnabled)
		    		ocultaInfoNC();
		    	isOcultaInfoEnabled=true;
		    } 

		    public void onNothingSelected(AdapterView<?> adapterView) {
		        return;
		    } 
		}); 
	}
	
	
	/**
	 * Created June 8th, 2015,
	 */
	
	private void deleteSimulation()
	{
		delegate.deleteSimulationRequest(this);
	}
	
	public void doRecalculo()
	{
		Log.d("Elimina Simulacion", "ok");
		delegate.doRecalculoContract(this);
	}
	
	
}
