package bbvacredit.bancomer.apicredit.gui.delegates;

import android.content.SharedPreferences;
import android.util.Log;

import java.util.Hashtable;
import java.util.Iterator;

import bancomer.api.consumo.implementations.InitConsumo;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.AdelantoNominaViewController;
import bbvacredit.bancomer.apicredit.gui.activities.DetalleAdelantoNomina;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ConsultaDatosTDCData;
import bbvacredit.bancomer.apicredit.models.DetalleAlternativa;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;

public class AdelantoNominaDelegate extends BaseDelegateOperacion {

    private Producto producto = null;
    private Integer productIndex = 0;
    private ObjetoCreditos data;
    private AdelantoNominaViewController controller;
    private DetalleAdelantoNomina dealleANOM;
    private Session session;

    private boolean isFromAdelantoNomina;

    public DetalleAdelantoNomina getDealleANOM() {
        return dealleANOM;
    }

    public void setDealleANOM(DetalleAdelantoNomina dealleANOM) {
        this.dealleANOM = dealleANOM;
    }


    // ----------- Attributes Region ----------------- //
    private boolean isSavingOperation = false;
    private boolean isDeleteOperation = false; //si es true, borrar simulación

    public Producto getProducto() {
        return producto;
    }

    public void setProducto() {
        SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
        productIndex = sp.getInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, 0);
        producto = data.getProductos().get(productIndex);
    }

    public void setController(AdelantoNominaViewController controller) {
        this.controller = controller;
    }

    public Producto returnProducto(){
        return producto;

    }

    public AdelantoNominaDelegate(){
        session = Tools.getCurrentSession();

        MainController.getInstance().muestraIndicadorActividad("Operación", "Conectando");

        data = session.getDataFromCreditos();

        setProducto();
       /* if(producto != null)
            if(!producto.getIndSimBoolean())
                doPeticionTDC();
*/

        MainController.getInstance().ocultaIndicadorActividad();
    }

    public ObjetoCreditos getData() {
        return data;
    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    public void consultaDetalleAlternativa(Producto oProducto){

        Session session = Tools.getCurrentSession();
        Integer getProdIndex = session.getProductoIndexByCve(ConstantsCredit.ADELANTO_NOMINA);

        String ium = session.getIum();
        String numeroCelular = session.getNumCelular();

        String cveCamp = this.getData().getProductos().get(getProdIndex).getCveProd();
        String cliente = session.getIdUsuario();

        Session.getInstance().setCveCamp(cveCamp);

        DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(cliente,ium,numeroCelular,cveCamp,getProducto());
        delegate.consultaDetalleAlternativasTask(oProducto);
    }

    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(Server.CALCULO_ALTERNATIVAS_OPERACION, ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        if(isSavingOperation) {
            addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = false;
        }else {
            addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = true;
        }

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    public void analyzeResponse(String operationId, ServerResponse response) {
        if(getCodigoOperacion().equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                productIndex = session.getProductoIndexByCve(ConstantsCredit.ADELANTO_NOMINA);
                this.producto = data.getProductos().get(productIndex);

                Log.i("revire","IndRev: "+respuesta.getProductos().get(productIndex).getIndRev());

                if(data.getProductos().get(productIndex).getCveProd().equals(ConstantsCredit.ADELANTO_NOMINA)){
                    data.getProductos().get(productIndex).setIndSimBoolean(true);
                }else{
                    setSimLooking4Cve(ConstantsCredit.ADELANTO_NOMINA);
                }

                MainController.getInstance().ocultaIndicadorActividad();
                //At this point I gotta update the credits.
                session.setCreditos(data);
                isSavingOperation =true;

                setProducto();

                saveOrDeleteSimulationRequest(true);
            }
        }else if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                if(isDeleteOperation){
                    isDeleteOperation = false;

                    CalculoData respuesta = (CalculoData) response.getResponse();

                    data = null;
                    data = new ObjetoCreditos();

                    data.setCreditos(respuesta.getCreditos());
                    data.setEstado(respuesta.getEstado());
                    data.setMontotSol(respuesta.getMontotSol());
                    data.setPagMTot(respuesta.getPagMTot());
                    data.setPorcTotal(respuesta.getPorcTotal());
                    data.setProductos(respuesta.getProductos());
                    data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                    productIndex = session.getProductoIndexByCve(ConstantsCredit.ADELANTO_NOMINA);
                    producto = data.getProductos().get(productIndex);

                    Log.i("simulador","AdelantoNominaDelegate, indSim"+data.getProductos().get(productIndex).getIndSim());

                    MainController.getInstance().ocultaIndicadorActividad();
                    //At this point I gotta update the credits.
                    session.setCreditos(data);
                    setProducto();

                    getDealleANOM().simpleUpdateMenu(producto.getCveProd());

                }else{
                    controller.showDetalleAdelantoNomina();
                }
            }
        }
    }

    private void setSimLooking4Cve(String cve){
        Iterator<Producto> it = data.getProductos().iterator();

        while(it.hasNext()){
            Producto pr = it.next();
            if(pr.getCveProd().equals(cve)) pr.setIndSimBoolean(true);
        }
    }

    public void doRecalculo(){
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);
        Producto ccr = data.getProductos().get(productIndex);

        Log.i("anom","crr.cvePRod: "+ccr.getCveProd());

        // String plazoSel = controller.getPlazo();
        int montoSel = Integer.valueOf(controller.getMonto());
        //String cvePlazoSel = getCvPlazoByValue(producto, plazoSel);

        addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM, paramTable);
        addParametro(ccr.getSubproducto().get(0).getCveSubp(), ServerConstantsCredit.CVESUBP_PARAM, paramTable);
        //addParametro(cvePlazoSel, ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        addParametro("", ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM, paramTable);

        Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }


    @Override
    protected String getCodigoOperacion() {
        // TODO Auto-generated method stub
        return Server.CALCULO_OPERACION;
    }

}
