package bbvacredit.bancomer.apicredit.io;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.CreditConstantsPropertyList;
import bbvacredit.bancomer.apicredit.common.NameValuePair;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ConsultaAlternativasData;
import bbvacredit.bancomer.apicredit.models.ConsultaCorreoData;
import bbvacredit.bancomer.apicredit.models.ConsultaDatosTDCData;
import bbvacredit.bancomer.apicredit.models.DetalleAlternativa;
import bbvacredit.bancomer.apicredit.models.EnvioCorreoData;
import bbvacredit.bancomer.apicredit.models.LoginData;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;

/**
 * Created by RPEREZ on 25/09/15.
 */
public class Server {

    public static CreditPropertyList oReadDocumentCredit = new CreditPropertyList();
    /**
     * Defines if the application show logs
     */
    public static boolean ALLOW_LOG = true;

    /**
     * Defines if the application is running on the emulator or in the device.
     */
    public static boolean EMULATOR = false;

    /**
     * Indicates simulation usage.
     */
    public static boolean SIMULATION = false;

    /**
     * Indicates if the base URL points to development server or production
     */
    public static boolean DEVELOPMENT = false;
    // public static boolean DEVELOPMENT = true; //TODO remove for TEST

    /**
     * Indicates simulation usage.
     */
    public static final long SIMULATION_RESPONSE_TIME = 5;
    /**
     * Bancomer Infrastructure.
     */
    public static final int BANCOMER = 0;

    public static int PROVIDER = BANCOMER; // TODO remove for TEST


    /**
     * Operation data parameter.
     */
    public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";
    /**
     * Operation code parameter.
     */
    public static final String OPERATION_CODE_PARAMETER = "OPERACION";
    /**
     * Operation data parameter.
     */
    public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";
    // public static boolean DEVELOPMENT = true; //TODO remove for TEST
    /**
     * Operation data parameter.
     */
    public static final String OPERATION_LOCALE_VALUE = "es_ES";


    /**
     * BBVACredit
     * Reference to HttpInvoker, which performs the network connection
     */
    private HttpInvoker httpClientCredit = null;
    /**
     * Default constructor.
     * Default constructor.
     */
    public Server() {
        httpClientCredit = new HttpInvoker();
    }

    /**
     * Base URL for providers.
     */
    public static String[] BASE_URL_CREDIT;
    /**
     *  ZERO
     */
    private static final String ZERO = "0";

    /** OPERACIONES */
    public static final String LOGIN_OPERACION = "login";
    public static final String CALCULO_OPERACION = "calculo";
    //Modified June 8th,2015,
    public static final String CALCULO_ALTERNATIVAS_OPERACION="calculoAlternativas";
//    public static final String CONSULTA_CORREO_OPERACION = "consultaCorreo";
    public static final String ENVIO_CORREO_OPERACION = "envioCorreo";
    public static final String CONSULTA_ALTERNATIVAS = "consultaAlternativas";
    public static final String CONSULTA_TDC = "consultaTDC";
    public static final String DETALLE_ALTERNATIVA = "detalleAlternativa";
    public static final String DETALLE_ALTERNATIVA_TDC = "detalleAlternativaTDC";
    //Modificacion 50870
    public static final String CONTRATA_ALTERNATIVA_CONSUMO = "contrataAlternativaConsumo ";

    public static final String CONSULTA_TABLA_AMORTIZACION ="consultaTablaAmortizacion";

    //envio de correo
    public static final String ENVIO_CORREO_HIPO_AUTO = "envioCorreoHipoAuto";
    public static final String ENVIO_CORREO_CONSUMO  = "envioCorreoConsumo";
    public static final String ENVIO_CORREO_TDC_ILC = "envioCorreoTDCILC";

    public static final String CONSULTA_DETALLE_EFI = "consultaDetalleOfertaEfi";
    public static final String CONSULTA_DETALLE_ILC= "consultaDetalleOferta";
    public static final String CONSULTA_DETALLE_TDC= "consultaDetalleOfertaTDC";
    public static final String CONSULTA_DETALLE_CONSUMO= "consultaDetalleOfertaConsumo";


    //One CLick
    /**
     * Operation codes.
     */
    public static final int CONSULTA_DETALLE_OFERTA_TDC=154;
    /** consulta detalle ofertas ilc*/
    public static final int CONSULTA_DETALLE_OFERTA_ILC=71;//67;//66
    public static final int ACEPTACION_OFERTA=72;//68;//67
    public static final int EXITO_OFERTA=73;//69;//68
    public static final int RECHAZO_OFERTA=74;//70;//69
    /**consulta detalle ofertas EFI*/
    public static final int CONSULTA_DETALLE_OFERTA_EFI=75;//71;//70
    public static final int ACEPTACION_OFERTA_EFI=76;//71
    public static final int EXITO_OFERTA_EFI=77;//72
    public static final int SIMULADOR_EFI=78;//73
    /** consulta detalle consumo*/
    public static final int CONSULTA_DETALLE_OFERTA_CONSUMO=79;
    public static final int POLIZA_OFERTA_CONSUMO=80;
    public static final int TERMINOS_OFERTA_CONSUMO=81;
    public static final int EXITO_OFERTA_CONSUMO=82;
    public static final int DOMICILIACION_OFERTA_CONSUMO=83;
    public static final int CONTRATO_OFERTA_CONSUMO=84;
    public static final int RECHAZO_OFERTA_CONSUMO=85;
    public static final int SMS_OFERTA_CONSUMO=86;
    public static final int OP_CONSULTA_INTERBANCARIOS =87;

    //one Click
    /**
     * clase de enum para cambio de url;
     */
    public enum isJsonValueCode{
        ONECLICK, NONE,CONSUMO,DEPOSITOS,PAPERLESS;
    }

    public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
    //Termina One CLick

    //ids migracion a apiServer
    public static final int CALCULO_ALTERNATIVAS_OPERACION_ID=142;
//    public static final int CONSULTA_CORREO_OPERACION_ID = 143;
    public static final int ENVIO_CORREO_OPERACION_ID = 144;
    public static final int CONSULTA_ALTERNATIVAS_ID = 145;
    public static final int CONSULTA_TDC_ID = 146;
    public static final int DETALLE_ALTERNATIVA_ID = 147;
    public static final int CONTRATA_ALTERNATIVA_CONSUMO_ID =148;
    public static final int DETALLE_ALTERNATIVA_TDC_ID =157;
    //Tabla de amortización

    public static final int TABLA_AMORTIZACION = 213;
    //ENVIO DE CORREO NUEVA IMAGEN
    public static final int ENVIO_CORREO_HIPO_AUTO_OP = 211;
    public static final int ENVIO_CORREO_CONSUMO_OP = 210;
    public static final int ENVIO_CORREO_TDC_ILC_OP = 212;
    /**
     * url de consumo para popup contactame
     */
    public static final String URL_CONTACTAME = "https://www.bancomermovil.com/mbank/mbank/Descargas/BBVACredit/TLM/webview24042015Prod.html";//"https://www.bancomermovil.net:11443/mbank/mbank/Descargas/BBVACredit/form.html";
    /**
     * Operation codes.
     */
    public static final Hashtable<String, String> OPERATION_CODES_CREDIT = new Hashtable<String, String>();


    /** Simulaciones de error */
    public static boolean SIMULACION_RESPUESTA_ERROR_LOGIN = false;
    public static boolean SIMULACION_RESPUESTA_ERROR_CALCULO = false;
    public static boolean SIMULACION_RESPUESTA_ERROR_CONSULTA_CORREO = false;
    public static boolean SIMULACION_RESPUESTA_ERROR_ENVIO_CORREO = false;
    public static boolean SIMULACION_RESPUESTA_ERROR_CONSULTA_ALTERNATIVAS = false;
    public static boolean SIMULACION_RESPUESTA_ERROR_CONSULTA_TDC = false;
    public static boolean SIMULACION_RESPUESTA_ERROR_CONSULTA_DETALLE_ALTERNATIVAS = false;

    private static void setupOperationCodesCredit(){
        OPERATION_CODES_CREDIT.put(LOGIN_OPERACION, "103");
        OPERATION_CODES_CREDIT.put(CALCULO_OPERACION, "calculoAlternativas");
//        OPERATION_CODES_CREDIT.put(CONSULTA_CORREO_OPERACION, "consultaCorreo");
        OPERATION_CODES_CREDIT.put(ENVIO_CORREO_OPERACION, "envioCorreo");
        OPERATION_CODES_CREDIT.put(CONSULTA_ALTERNATIVAS, "consultaAlternativas");
        OPERATION_CODES_CREDIT.put(CONSULTA_TDC, "104");
        OPERATION_CODES_CREDIT.put(DETALLE_ALTERNATIVA, "detalleAlternativa");
        //Modificacion 50870
        OPERATION_CODES_CREDIT.put(CONTRATA_ALTERNATIVA_CONSUMO, "contrataAlternativaConsumo");
        OPERATION_CODES_CREDIT.put(DETALLE_ALTERNATIVA_TDC,"detalleAlternativaTDC");
    }

    /**
     * Path table for provider/operation URLs.
     */
    public static Hashtable<String, PathsOperacion> OPERATIONS_PATH_CREDIT = new Hashtable<String, PathsOperacion>();

    /**
     * Operation URLs map.
     */
    public static Hashtable<String, String> OPERATIONS_MAP_CREDIT = new Hashtable<String, String>();


    /**
     * Operation providers map.
     */
    public static Hashtable<String, Integer> OPERATIONS_PROVIDER_CREDIT = new Hashtable<String, Integer>();



    /**
     * Setup operation for a provider.
     *
     * @param operation
     *            the operation
     * @param provider
     *            the provider
     */

    private static void setupOperationCredit(String operation, int provider) {
        OPERATIONS_MAP_CREDIT.put(
                OPERATION_CODES_CREDIT.get(operation),
                new StringBuffer(BASE_URL_CREDIT[provider]).append(((PathsOperacion) OPERATIONS_PATH_CREDIT.get(operation)).getPathProveedor(provider)).toString());
        OPERATIONS_PROVIDER_CREDIT.put(operation, Integer.valueOf(provider));
    }

    /**
     * Setup operations for a provider
     *
     * @param provider the provider
     */

    private static void setupOperationsCredit(int provider) {
        setupOperationCredit(LOGIN_OPERACION, provider);
        setupOperationCredit(CALCULO_OPERACION, provider);
//        setupOperationCredit(CONSULTA_CORREO_OPERACION, provider);
        setupOperationCredit(ENVIO_CORREO_OPERACION, provider);
        setupOperationCredit(CONSULTA_ALTERNATIVAS, provider);
        setupOperationCredit(CONSULTA_TDC, provider);
        setupOperationCredit(DETALLE_ALTERNATIVA, provider);
        //Modificacion 50870
        setupOperationCredit(CONTRATA_ALTERNATIVA_CONSUMO, provider);
        setupOperationCredit(DETALLE_ALTERNATIVA_TDC, provider);
    }

    /**
     * Setup production server paths
     */
    private static void setupProductionServerCredit() {
        BASE_URL_CREDIT = new String[]{
                "https://www.bancomermovil.com",
                "http://10.0.3.10:8080/servermobile/Servidor",
                "http://movilok.net"
        };

        OPERATIONS_PATH_CREDIT.put(LOGIN_OPERACION, new PathsOperacion("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(CALCULO_OPERACION, new PathsOperacion("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

//        OPERATIONS_PATH_CREDIT.put(CONSULTA_CORREO_OPERACION, new PathsOperacion("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(ENVIO_CORREO_OPERACION, new PathsOperacion("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(CONSULTA_ALTERNATIVAS, new PathsOperacion("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(CONSULTA_TDC, new PathsOperacion("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(DETALLE_ALTERNATIVA, new PathsOperacion("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        //Modificacion 50870
        OPERATIONS_PATH_CREDIT.put(CONTRATA_ALTERNATIVA_CONSUMO, new PathsOperacion("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(DETALLE_ALTERNATIVA_TDC, new PathsOperacion("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));
    }

    /**
     * Setup production server paths
     */
    private static void setupDevelopmentServerCredit() {
        BASE_URL_CREDIT = new String[]{
                "https://www.bancomermovil.net:11443",
                "http://10.0.3.10:8080/servermobile/Servidor",
                "http://movilok.net"
        };

        OPERATIONS_PATH_CREDIT.put(LOGIN_OPERACION, new PathsOperacion("/eexd_mx_web/servlet/ServletOperacionWeb", "/eexd_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(CALCULO_OPERACION, new PathsOperacion("/eexd_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

//        OPERATIONS_PATH_CREDIT.put(CONSULTA_CORREO_OPERACION, new PathsOperacion("/eexd_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(ENVIO_CORREO_OPERACION, new PathsOperacion("/eexd_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(CONSULTA_ALTERNATIVAS, new PathsOperacion("/eexd_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(CONSULTA_TDC, new PathsOperacion("/eexd_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(DETALLE_ALTERNATIVA, new PathsOperacion("/eexd_mx_web/servlet/ServletOperacionWeb", "/eexd_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        //Modificacion 50870
        OPERATIONS_PATH_CREDIT.put(CONTRATA_ALTERNATIVA_CONSUMO, new PathsOperacion("/eexd_mx_web/servlet/ServletOperacionWeb", "/eexd_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

        OPERATIONS_PATH_CREDIT.put(DETALLE_ALTERNATIVA_TDC, new PathsOperacion("/eexd_mx_web/servlet/ServletOperacionWeb", "/eexd_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));
    }

    /**
     * Get the operation URL.
     *
     * @param operation
     *            the operation id
     * @return the operation URL
     */
    public static String getOperationUrlCredit(String operation) {
        String url = (String) OPERATIONS_MAP_CREDIT.get(operation);
        return url;
    }

    /** PROVEEDORES */
    public static final int MOVILOK = 2;


    public static void setEnviroment(){
        if (DEVELOPMENT) {
            setupDevelopmentServerCredit();
        } else {
            setupProductionServerCredit();
        }


        setupOperationCodesCredit();
        setupOperationsCredit(PROVIDER);
    }

    private static void logNetworkOperation(String operationTag, String requestParams, String responseMessage) {
        if (ALLOW_LOG) {
            Log.d(operationTag + " request params", requestParams);
            Tools.trazaTexto(operationTag + " response", responseMessage);
        }
    }

    /**
     * inicia metodos BBVACredit
     *
     */

    /**
     * INTEGRACION BBVACredit
     * perform the network operation identifier by operationId with the
     * parameters passed
     *
     * @param operationId the identifier of the operation
     * @param params Hashtable with the parameters as key-value pairs.
     * @return the response from the server
     * @throws IOException
     * @throws ParsingException
     * @throws URISyntaxException
     */
    public ServerResponse doNetworkOperation(String operationId, Hashtable<String, ?> params) throws IOException, ParsingException, JSONException, ParsingException, URISyntaxException {
        System.out.println("doNetworkOperation - operationId = " + operationId);
        ServerResponse response = null;
        long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);

        Integer provider = (Integer) OPERATIONS_PROVIDER_CREDIT.get(operationId);
        if (provider != null) {
            PROVIDER = provider.intValue();
        }

        if (LOGIN_OPERACION.equals(operationId)) {
            System.out.println("Operacion de login");
            response = loginCredit(params);
        }else if(CALCULO_OPERACION.equals(operationId)){
            System.out.println("Operacion de posicion global");
            response = calculo(params);
        }/*else if(CONSULTA_CORREO_OPERACION.equals(operationId)){
            System.out.println("Operacion de consulta de correo");
            response = consultaCorreo(params);
        }*/else if(ENVIO_CORREO_OPERACION.equals(operationId)){
            System.out.println("Operacion de envio de correo");
            response = envioCorreo(params);
        }else if(CONSULTA_ALTERNATIVAS.equals(operationId)){
            Log.e("cve Operation",operationId);
            System.out.println("Operacion de consulta alternativas");
            response = consultaAlternativas(params);
        }else if(CONSULTA_TDC.equals(operationId)){
            System.out.println("Operacion de consulta TDC");
            response = consultaTDC(params);
        }else if(DETALLE_ALTERNATIVA.equals(operationId)){
            System.out.println("Operacion detalle alternativa");
            response = consultaDetalleAlternativa(params);
        }else if(CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)){
            Log.e("Invoking ","ok");
            response=guardarEliminarSimulacion(params);
        }else if(DETALLE_ALTERNATIVA_TDC.equals(operationId)){
            System.out.println("Operacion detalle alternativa TDC");
            response = consultaDetalleAlternativaTDC(params);
        }

        if (sleep > 0) {
            //#debug debug
            System.out.println("Sleeping extra time: " + sleep);
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
        }

        System.out.println("End of doNetworkOperation");
        return response;
    }

    /**
     * Realiza la operacion de login.
     *
     * @param params un mapa de parametros para la operacion
     * @return la respuesta del servidor
     * @throws IOException            control de excepciones de entrada/salida
     * @throws ParsingException control de excepciones de parseo
     * @throws JSONException
     * @throws URISyntaxException
     */
    private ServerResponse loginCredit(Hashtable<String, ?> params) throws IOException, ParsingException, JSONException, URISyntaxException {

        // Se devuelve los distintos estados de respuesta en funcion de la version del sistema - VM
        LoginData data = new LoginData();
        ServerResponse response = new ServerResponse(data);

        if (SIMULATION) {

            if (SIMULACION_RESPUESTA_ERROR_LOGIN) {
                this.httpClientCredit.simulateOperation("ESERROR*COCNE0011*MECLAVE DE ACCESO INCORRECTA", response);

            } else {

                String versionVM = (String) params.get(ServerConstantsCredit.VERSION_VM);
                String versionAU = (String) params.get(ServerConstantsCredit.VERSION_AU);
                String mensaje = "";

                if (versionVM.equals(ConstantsCredit.VM500)) {
                    mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM500);
                } else if (versionVM.equals(ConstantsCredit.VM300)) {
                    mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM300);
                } else if (versionVM.equals(ConstantsCredit.VM350)) {
                    mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM350);
                } else if (versionVM.equals(ConstantsCredit.VM411)) {
                    if (versionAU.equals(ZERO)) {
                        mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM411_0);
                    } else if (((Boolean) params.get(ServerConstantsCredit.VERSION_HC))) {
                        mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM411_HC);
                    } else {
                        mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM411);
                    }
                } else if (versionVM.equals(ConstantsCredit.VM910)) {
                    mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM910);
                }

                logNetworkOperation("loginCredit", params.toString(), mensaje);
                this.httpClientCredit.simulateOperation(mensaje, response);
            }

        } else {
            NameValuePair[] parameters = new NameValuePair[14];
            parameters[0] = new NameValuePair(ServerConstantsCredit.USERNAME_PARAM, (String) params.get(ServerConstantsCredit.USERNAME_PARAM));
            parameters[1] = new NameValuePair(ServerConstantsCredit.PASSWORD_PARAM, (String) params.get(ServerConstantsCredit.PASSWORD_PARAM));
            parameters[2] = new NameValuePair(ServerConstantsCredit.IUM_ETIQUETA, (String) params.get(ServerConstantsCredit.IUM_ETIQUETA));
            parameters[3] = new NameValuePair(ServerConstantsCredit.VERSION_VM, (String) params.get(ServerConstantsCredit.VERSION_VM));
            parameters[4] = new NameValuePair(ServerConstantsCredit.VERSION_C1, (String) params.get(ServerConstantsCredit.VERSION_C1));
            parameters[5] = new NameValuePair(ServerConstantsCredit.VERSION_C4, (String) params.get(ServerConstantsCredit.VERSION_C4));
            parameters[6] = new NameValuePair(ServerConstantsCredit.VERSION_C5, (String) params.get(ServerConstantsCredit.VERSION_C5));
            parameters[7] = new NameValuePair(ServerConstantsCredit.VERSION_C8, (String) params.get(ServerConstantsCredit.VERSION_C8));
            parameters[8] = new NameValuePair(ServerConstantsCredit.MARCA_MODELO, (String) params.get(ServerConstantsCredit.MARCA_MODELO));
            parameters[9] = new NameValuePair(ServerConstantsCredit.VERSION_TA, (String) params.get(ServerConstantsCredit.VERSION_TA));
            parameters[10] = new NameValuePair(ServerConstantsCredit.VERSION_DM, (String) params.get(ServerConstantsCredit.VERSION_DM));
            parameters[11] = new NameValuePair(ServerConstantsCredit.VERSION_SV, (String) params.get(ServerConstantsCredit.VERSION_SV));
            parameters[12] = new NameValuePair(ServerConstantsCredit.VERSION_MS, (String) params.get(ServerConstantsCredit.VERSION_MS));
            parameters[13] = new NameValuePair(ServerConstantsCredit.VERSION_AU, (String) params.get(ServerConstantsCredit.VERSION_AU));
//					parameters[13] = new NameValuePair(ServerConstants.VERSION_VM, Constants.APPLICATION_VERSION);

            this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(LOGIN_OPERACION), parameters, response);
        }
        return response;

    }

    /**
     * Realiza la operacion de consulta TDC.
     *
     * @param params un mapa de parametros para la operacion
     * @return la respuesta del servidor
     * @throws IOException            control de excepciones de entrada/salida
     * @throws ParsingException control de excepciones de parseo
     * @throws JSONException
     * @throws URISyntaxException
     */
    private ServerResponse consultaTDC(Hashtable<String, ?> params) throws IOException, ParsingException, JSONException, URISyntaxException {

        // Se devuelve los distintos estados de respuesta en funcion de la version del sistema - VM
        ConsultaDatosTDCData data = new ConsultaDatosTDCData();
        ServerResponse response = new ServerResponse(data);

        if (SIMULATION) {

            if (SIMULACION_RESPUESTA_ERROR_CONSULTA_TDC) {
                String mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_TDC_ERROR);

                logNetworkOperation("consultaTDC", params.toString(), mensaje);
                this.httpClientCredit.simulateOperation(mensaje, response);

            } else {
                String mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_TDC);
                Log.d("Consulta TDC request", params.toString());
                Tools.trazaTexto("Consulta TDC response", mensaje);

                this.httpClientCredit.simulateOperation(mensaje, response);
						/*
						NameValuePair[] parameters = new NameValuePair[6];
						parameters[0] = new NameValuePair(ServerConstantsCredit.USERNAME_PARAM, (String) params.get(ServerConstantsCredit.USERNAME_PARAM));
						parameters[1] = new NameValuePair(ServerConstantsCredit.PASSWORD_PARAM, (String) params.get(ServerConstantsCredit.PASSWORD_PARAM));
						parameters[2] = new NameValuePair(ServerConstantsCredit.IUM_ETIQUETA, (String) params.get(ServerConstantsCredit.IUM_ETIQUETA));
						parameters[3] = new NameValuePair(ServerConstantsCredit.TP, (String) params.get(ServerConstantsCredit.TP));
						parameters[4] = new NameValuePair(ServerConstantsCredit.AS, (String) params.get(ServerConstantsCredit.AS));
						if(params.containsKey(ServerConstantsCredit.PE))
							parameters[5] = new NameValuePair(ServerConstantsCredit.PE, (String) params.get(ServerConstantsCredit.PE));

						this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CONSULTA_TDC), parameters, response);
				        */

            }

        } else {
            NameValuePair[] parameters = new NameValuePair[6];
            parameters[0] = new NameValuePair(ServerConstantsCredit.USERNAME_PARAM, (String) params.get(ServerConstantsCredit.USERNAME_PARAM));
            parameters[1] = new NameValuePair(ServerConstantsCredit.PASSWORD_PARAM, (String) params.get(ServerConstantsCredit.PASSWORD_PARAM));
            parameters[2] = new NameValuePair(ServerConstantsCredit.IUM_ETIQUETA, (String) params.get(ServerConstantsCredit.IUM_ETIQUETA));
            parameters[3] = new NameValuePair(ServerConstantsCredit.TP, (String) params.get(ServerConstantsCredit.TP));
            parameters[4] = new NameValuePair(ServerConstantsCredit.AS, (String) params.get(ServerConstantsCredit.AS));
            if (params.containsKey(ServerConstantsCredit.PE))
                parameters[5] = new NameValuePair(ServerConstantsCredit.PE, (String) params.get(ServerConstantsCredit.PE));

            this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CONSULTA_TDC), parameters, response);
        }
        return response;

    }

    /**
     * Realiza la operacion de calculo.
     *
     * @param params un mapa de parametros para la operacion
     * @return la respuesta del servidor
     * @throws IOException            control de excepciones de entrada/salida
     * @throws ParsingException control de excepciones de parseo
     * @throws JSONException
     * @throws URISyntaxException
     */
    private ServerResponse calculo(Hashtable<String, ?> params) throws IOException, ParsingException, JSONException, URISyntaxException {
        CalculoData data = new CalculoData();
        ServerResponse response = new ServerResponse(data);

        if (SIMULATION) {
            String mensaje = "";
            if (SIMULACION_RESPUESTA_ERROR_CALCULO) {
                mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CALCULO_ERROR);
                this.httpClientCredit.simulateOperation(mensaje, response, true);
            } else {

                //Creditos ofertados 6
                mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CALCULO);

                logNetworkOperation("calculo", params.toString(), mensaje);
                this.httpClientCredit.simulateOperation(mensaje, response, true);
            }
        } else {
            JSONObject json = new JSONObject();

            // Se distingue entre los par�metros que vayamos a meter ? para diferenciar las peticiones de liquidaci�n y contrataci�n; por tanto no meter par�metros vacios
            if (params.containsKey(ServerConstantsCredit.CVESUBP_PARAM) || params.containsKey(ServerConstantsCredit.CVEPROD_PARAM) || params.containsKey(ServerConstantsCredit.CVEPLAZO_PARAM)) {
                // Contrataci�n
                json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION));
                json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));
                json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
                json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));
                json.put(ServerConstantsCredit.CVEPROD_PARAM, params.get(ServerConstantsCredit.CVEPROD_PARAM));
                json.put(ServerConstantsCredit.CVESUBP_PARAM, params.get(ServerConstantsCredit.CVESUBP_PARAM));
                json.put(ServerConstantsCredit.CVEPLAZO_PARAM, params.get(ServerConstantsCredit.CVEPLAZO_PARAM));
                json.put(ServerConstantsCredit.MON_DESE_PARAM, params.get(ServerConstantsCredit.MON_DESE_PARAM));
                json.put(ServerConstantsCredit.TIPO_OP_PARAM, params.get(ServerConstantsCredit.TIPO_OP_PARAM));

            } else {
                // Liquidacion
                json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION));
                json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));
                json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
                json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));
                json.put(ServerConstantsCredit.CONTRATO_PARAM, params.get(ServerConstantsCredit.CONTRATO_PARAM));
                json.put(ServerConstantsCredit.PAGO_MENSUAL_PARAM, params.get(ServerConstantsCredit.PAGO_MENSUAL_PARAM));
                json.put(ServerConstantsCredit.TIPO_OP_PARAM, params.get(ServerConstantsCredit.TIPO_OP_PARAM));

            }

            this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CALCULO_OPERACION), json, response);
        }

        return response;
    }

    /**
     * Realiza la operacion de consulta de correo.
     *
     * @param params un mapa de parametros para la operacion
     * @return la respuesta del servidor
     * @throws IOException            control de excepciones de entrada/salida
     * @throws ParsingException control de excepciones de parseo
     * @throws JSONException
     * @throws URISyntaxException
     */
    /*private ServerResponse consultaCorreo(Hashtable<String, ?> params) throws IOException, ParsingException, JSONException, URISyntaxException {
        ConsultaCorreoData data = new ConsultaCorreoData();
        ServerResponse response = new ServerResponse(data);

        if (SIMULATION) {
            String mensaje = "";
            if (SIMULACION_RESPUESTA_ERROR_CONSULTA_CORREO) {
                mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_CORREO_ERROR);
            } else {
                mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_CORREO);
            }

            logNetworkOperation("consultaCorreo", params.toString(), mensaje);
            this.httpClientCredit.simulateOperation(mensaje, response, true);

        } else {
            JSONObject json = new JSONObject();

            json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CONSULTA_CORREO_OPERACION));
            json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));
            json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));

            this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CONSULTA_CORREO_OPERACION), json, response);
        }

        return response;
    }*/

    /**
     * Realiza la operacion de envio de correo.
     *
     * @param params un mapa de parametros para la operacion
     * @return la respuesta del servidor
     * @throws IOException            control de excepciones de entrada/salida
     * @throws ParsingException control de excepciones de parseo
     * @throws JSONException
     * @throws URISyntaxException
     */
    private ServerResponse envioCorreo(Hashtable<String, ?> params) throws IOException, ParsingException, JSONException,  URISyntaxException {
        EnvioCorreoData data = new EnvioCorreoData();
        ServerResponse response = new ServerResponse(data);

        if (SIMULATION) {
            String mensaje = "";
            if (SIMULACION_RESPUESTA_ERROR_ENVIO_CORREO) {
                //mensaje = "{\"estado\":\"ERROR\"}";
                mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.ENVIO_CORREO_ERROR);
            } else {
                mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.ENVIO_CORREO);
            }

            logNetworkOperation("envioCorreo", params.toString(), mensaje);
            this.httpClientCredit.simulateOperation(mensaje, response, true);
        } else {
            JSONObject json = new JSONObject();

            json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.ENVIO_CORREO_OPERACION));
            json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));
            json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));

            String indicador = (String) params.get(ServerConstantsCredit.IND_CORREO);

            if (indicador.equals("C")) {
                json.put(ServerConstantsCredit.SCN, params.get(ServerConstantsCredit.SCN));
                json.put(ServerConstantsCredit.CN_IMPORTE, params.get(ServerConstantsCredit.CN_IMPORTE));
                json.put(ServerConstantsCredit.CN_TASA_ANUAL, params.get(ServerConstantsCredit.CN_TASA_ANUAL));
                json.put(ServerConstantsCredit.CN_PLAZO, params.get(ServerConstantsCredit.CN_PLAZO));
                json.put(ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL, params.get(ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL));

                json.put(ServerConstantsCredit.SPPI, params.get(ServerConstantsCredit.SPPI));
                json.put(ServerConstantsCredit.PP_IMPORTE, params.get(ServerConstantsCredit.PP_IMPORTE));
                json.put(ServerConstantsCredit.PP_TASA_ANUAL, params.get(ServerConstantsCredit.PP_TASA_ANUAL));
                json.put(ServerConstantsCredit.PP_PLAZO, params.get(ServerConstantsCredit.PP_PLAZO));
                json.put(ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL, params.get(ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL));

                json.put(ServerConstantsCredit.STDC, params.get(ServerConstantsCredit.STDC));
                json.put(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO, params.get(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO));
                json.put(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO, params.get(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO));

                json.put(ServerConstantsCredit.SILDC, params.get(ServerConstantsCredit.SILDC));
                json.put(ServerConstantsCredit.IL_TARJETA_DE_CREDITO, params.get(ServerConstantsCredit.IL_TARJETA_DE_CREDITO));
                json.put(ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA, params.get(ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA));
                json.put(ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA, params.get(ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA));

                json.put(ServerConstantsCredit.SCA, params.get(ServerConstantsCredit.SCA));
                json.put(ServerConstantsCredit.CA_IMPORTE, params.get(ServerConstantsCredit.CA_IMPORTE));
                json.put(ServerConstantsCredit.CA_TASA_ANUAL, params.get(ServerConstantsCredit.CA_TASA_ANUAL));
                json.put(ServerConstantsCredit.CA_PLAZO, params.get(ServerConstantsCredit.CA_PLAZO));
                json.put(ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL, params.get(ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL));

                json.put(ServerConstantsCredit.SCH, params.get(ServerConstantsCredit.SCH));
                json.put(ServerConstantsCredit.CH_IMPORTE, params.get(ServerConstantsCredit.CH_IMPORTE));
                json.put(ServerConstantsCredit.CH_TASA_ANUAL, params.get(ServerConstantsCredit.CH_TASA_ANUAL));
                json.put(ServerConstantsCredit.CH_PLAZO, params.get(ServerConstantsCredit.CH_PLAZO));
                json.put(ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL, params.get(ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL));

                json.put(ServerConstantsCredit.CON_CAT, params.get(ServerConstantsCredit.CON_CAT));
                json.put(ServerConstantsCredit.CON_FECHA_CALCULO, params.get(ServerConstantsCredit.CON_FECHA_CALCULO));

            } else if (indicador.equals("L")) {

                //liquidacion
                json.put(ServerConstantsCredit.SCNL, params.get(ServerConstantsCredit.SCNL));
                json.put(ServerConstantsCredit.CN_DESC, params.get(ServerConstantsCredit.CN_DESC));
                json.put(ServerConstantsCredit.CN_CANT, params.get(ServerConstantsCredit.CN_CANT));

                json.put(ServerConstantsCredit.SPPIL, params.get(ServerConstantsCredit.SPPIL));
                json.put(ServerConstantsCredit.PP_DESC, params.get(ServerConstantsCredit.PP_DESC));
                json.put(ServerConstantsCredit.PP_CANT, params.get(ServerConstantsCredit.PP_CANT));

                json.put(ServerConstantsCredit.SCHL, params.get(ServerConstantsCredit.SCHL));
                json.put(ServerConstantsCredit.CH_DESC, params.get(ServerConstantsCredit.CH_DESC));
                json.put(ServerConstantsCredit.CH_CANT, params.get(ServerConstantsCredit.CH_CANT));

                json.put(ServerConstantsCredit.SCAL, params.get(ServerConstantsCredit.SCAL));
                json.put(ServerConstantsCredit.CA_DESC, params.get(ServerConstantsCredit.CA_DESC));
                json.put(ServerConstantsCredit.CA_CANT, params.get(ServerConstantsCredit.CA_CANT));

                json.put(ServerConstantsCredit.SP5, params.get(ServerConstantsCredit.SP5));
                json.put(ServerConstantsCredit.P5_DESC, params.get(ServerConstantsCredit.P5_DESC));
                json.put(ServerConstantsCredit.P5_CANT, params.get(ServerConstantsCredit.P5_CANT));

                json.put(ServerConstantsCredit.SP6, params.get(ServerConstantsCredit.SP6));
                json.put(ServerConstantsCredit.P6_DESC, params.get(ServerConstantsCredit.P6_DESC));
                json.put(ServerConstantsCredit.P6_CANT, params.get(ServerConstantsCredit.P6_CANT));


                //alternativas
                json.put(ServerConstantsCredit.SCR, params.get(ServerConstantsCredit.SCR));

                json.put(ServerConstantsCredit.SAN, params.get(ServerConstantsCredit.SAN));
                json.put(ServerConstantsCredit.CN, params.get(ServerConstantsCredit.CN));

                json.put(ServerConstantsCredit.SAP, params.get(ServerConstantsCredit.SAP));
                json.put(ServerConstantsCredit.PP, params.get(ServerConstantsCredit.PP));

                json.put(ServerConstantsCredit.SAI, params.get(ServerConstantsCredit.SAI));
                json.put(ServerConstantsCredit.IL, params.get(ServerConstantsCredit.IL));

                json.put(ServerConstantsCredit.SAH, params.get(ServerConstantsCredit.SAH));
                json.put(ServerConstantsCredit.CH, params.get(ServerConstantsCredit.CH));

                json.put(ServerConstantsCredit.SAA, params.get(ServerConstantsCredit.SAA));
                json.put(ServerConstantsCredit.CA, params.get(ServerConstantsCredit.CA));

                json.put(ServerConstantsCredit.SAT, params.get(ServerConstantsCredit.SAT));
                json.put(ServerConstantsCredit.TC, params.get(ServerConstantsCredit.TC));

                //YA NO SON NECESARIOS
                //json.put(ServerConstantsCredit.LIQ_CAT, params.get(ServerConstantsCredit.LIQ_CAT));
                //json.put(ServerConstantsCredit.LIQ_FECHA_CALCULO, params.get(ServerConstantsCredit.LIQ_FECHA_CALCULO));
            }

            json.put(ServerConstantsCredit.IND_CORREO, indicador);
            json.put(ServerConstantsCredit.email, params.get(ServerConstantsCredit.email));

            this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(ENVIO_CORREO_OPERACION), json, response);
        }

        return response;
    }

    /**
     * Realiza la operacion de envio de correo.
     *
     * @param params un mapa de parametros para la operacion
     * @return la respuesta del servidor
     * @throws IOException            control de excepciones de entrada/salida
     * @throws ParsingException control de excepciones de parseo
     * @throws JSONException
     * @throws URISyntaxException
     * @throws ParsingException
     */
    private ServerResponse consultaAlternativas(Hashtable<String, ?> params) throws IOException, ParsingException, JSONException, URISyntaxException {
        ConsultaAlternativasData data = new ConsultaAlternativasData();
        ServerResponse response = new ServerResponse(data);

        Log.e("apiServer","nuevo tipo de petición por apiserver");

        if (SIMULATION) {
            Log.e("consultaAlternativas", "Simulacion OK");
            String mensaje = "";
            if (SIMULACION_RESPUESTA_ERROR_CONSULTA_ALTERNATIVAS) {
                Log.e("consultaAlternativas", "Error");
                mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_ALTERNATIVAS_ERROR);

            } else {
                Log.e("consultaAlternativas", "OK");

                //Todas las posibles opciones para creditos simulados y contratados en diccionario
                mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_ALTERNATIVAS);
            }

            logNetworkOperation("consultaAlternativas", params.toString(), mensaje);
            this.httpClientCredit.simulateOperation(mensaje, response, true);

        } else {
/*
            ParametersTO parametersTO = new ParametersTO();

            Hashtable<String,String> paramsApiServer = new Hashtable<String, String>();

            paramsApiServer.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION));

            paramsApiServer.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CONSULTA_ALTERNATIVAS));
            paramsApiServer.put(ServerConstantsCredit.CLIENTE_PARAM, (String) params.get(ServerConstantsCredit.CLIENTE_PARAM));

            paramsApiServer.put(ServerConstantsCredit.IUM_PARAM, (String) params.get(ServerConstantsCredit.IUM_PARAM));
            paramsApiServer.put(ServerConstantsCredit.NUMERO_CEL_PARAM, (String) params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));

            parametersTO.setParameters(paramsApiServer);
            parametersTO.setSimulation(Server.SIMULATION);
            parametersTO.setDevelopment(Server.DEVELOPMENT);
            parametersTO.setJson(true);
            parametersTO.setIsJsonValue();
            parametersTO.setOperationId(130);

            CommServiceProxy serverProxy = new CommServiceProxy(MainController.getInstance().getContext());

            IResponseService resultado= serverProxy.request(parametersTO,new Object().getClass());

            Log.i("apiServer","resultado.getMessageCode"+resultado.getMessageCode());
            Log.i("apiServer","resultado.getMessageText"+resultado.getMessageText());
            Log.i("apiServer","resultado.getResponseString"+resultado.getResponseString());
            Log.i("apiServer","resultado.getResponse"+resultado.getResponse());
            Log.i("apiServer","resultado.getStatus"+resultado.getStatus());
            Log.i("apiServer","resultado.getUpdateURL"+resultado.getUpdateURL());
            Log.i("apiServer","resultado.getObjResponse"+resultado.getObjResponse());

*/
            JSONObject json = new JSONObject();

            json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CONSULTA_ALTERNATIVAS));
            json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));

            json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));



            //Gotta look for flags using into MainController.

            //////////////////////// Focus /////////////////////////

            //Starts BBVA CREDIT Cookies.
            /*if (!Session.getInstance(SuiteApp.appContext).areCookiesSet()) {
                this.httpClientCredit.setCookies(Session.getInstance(SuiteApp.appContext).getCookieManager());
                Session.getInstance(SuiteApp.appContext).setCookiesFlag(true);
            }*/

            //Log.d("INVOKER CERDIT", this.httpClientCredit.getHttpClient().toString());
            this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CONSULTA_ALTERNATIVAS), json, response);

        }

        return response;
    }

    private ServerResponse consultaDetalleAlternativa(Hashtable<String, ?> params) throws IOException, ParsingException, JSONException, URISyntaxException {
        DetalleAlternativa data = new DetalleAlternativa();
        ServerResponse response = new ServerResponse(data);

        Log.e("Llega a server method", "ok");
        Log.e("valor bandera", String.valueOf(SIMULATION));

        if (SIMULATION) {
            Log.e("ANOM", "getCveCamp: "+Session.getInstance().getCveCamp());
            String mensaje = "";
            if (SIMULACION_RESPUESTA_ERROR_CONSULTA_DETALLE_ALTERNATIVAS) {
                mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.DETALLE_ALTERNATIVA_ERROR);
            } else {

                if (Session.getInstance().getCveCamp().equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)) {
                    Log.e("Entra ILC", "ok");
                    //ILC
                    mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.DETALLE_ALTERNATIVA_ILC);

                } else if(Session.getInstance().getCveCamp().equals(ConstantsCredit.ADELANTO_NOMINA)){
                    //Adelanto Nomina
                    Log.e("Entra ANOM", "ok");
                    mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.DETALLE_ALTERNATIVA_ANOM);

                }else{
                    Log.e("Entra Consumo", "ok");
                    //CONSUMO
                    mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.DETALLE_ALTERNATIVA_CONSUMO);
                }
            }

            logNetworkOperation("consultaDetalleAlternativa", params.toString(), mensaje);
            this.httpClientCredit.simulateOperation(mensaje, response, true);

        } else {
            JSONObject json = new JSONObject();

            json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.DETALLE_ALTERNATIVA));
            json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));

            json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));
            json.put(ServerConstantsCredit.PRODUCTO_PARAM, params.get(ServerConstantsCredit.PRODUCTO_PARAM));

            this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(DETALLE_ALTERNATIVA), json, response);
        }

        return response;
    }

    /**
     * Created June 8th,2015,
     *
     * @throws URISyntaxException
     */


    private ServerResponse guardarEliminarSimulacion(Hashtable<String, ?> params) throws IOException, ParsingException, JSONException, URISyntaxException {
        CalculoData data = new CalculoData();
        ServerResponse response = new ServerResponse(data);
        Log.d("Entra a proceso de Server:", "Guardar o Eliminar Simulación");


        if (SIMULATION) {
            String mensaje = "";
            if (SIMULACION_RESPUESTA_ERROR_CONSULTA_DETALLE_ALTERNATIVAS) {
                mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CALCULO_ALTERNATIVAS_ERROR);
            } else {
                mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CALCULO_ALTERNATIVAS);
            }

            logNetworkOperation("guardarEliminarSimulacion", params.toString(), mensaje);
            this.httpClientCredit.simulateOperation(mensaje, response, true);

        } else {
            JSONObject json = new JSONObject();

            json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION));
            json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));

            json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));
            json.put(ServerConstantsCredit.TIPO_OP_PARAM, params.get(ServerConstantsCredit.TIPO_OP_PARAM));

            if(params.get(ServerConstantsCredit.TIPO_OP_PARAM).equals("5")){
                Log.w("URL","Agregar Montos");
                json.put(ServerConstantsCredit.CVEPROD_PARAM, params.get(ServerConstantsCredit.CVEPROD_PARAM));
                json.put(ServerConstantsCredit.CVESUBP_PARAM, params.get(ServerConstantsCredit.CVESUBP_PARAM));
                json.put(ServerConstantsCredit.MON_DESE_PARAM, params.get(ServerConstantsCredit.MON_DESE_PARAM));
                json.put(ServerConstantsCredit.CVEPLAZO_PARAM, params.get(ServerConstantsCredit.CVEPLAZO_PARAM));
            }


            this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CALCULO_OPERACION), json, response);
        }

        return response;
    }

    private ServerResponse consultaDetalleAlternativaTDC(Hashtable<String, ?> params) throws IOException, ParsingException, JSONException, URISyntaxException {
        DetalleAlternativa data = new DetalleAlternativa();
        ServerResponse response = new ServerResponse(data);

        Log.i("detalletdc","dopeticion");

        JSONObject json = new JSONObject();

        json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.DETALLE_ALTERNATIVA_TDC));
        json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));
        json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
        json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));
        json.put(ServerConstantsCredit.PRODUCTO_PARAM, params.get(ServerConstantsCredit.PRODUCTO_PARAM));

        this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(DETALLE_ALTERNATIVA_TDC), json, response);


        return response;
    }



}
