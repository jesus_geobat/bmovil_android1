package bbvacredit.bancomer.apicredit.gui.activities;


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.common.timer.TimerController;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.ResumenAdapter;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.AutoDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.ResumenDelegate;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.models.CreditoContratado;
import bbvacredit.bancomer.apicredit.models.Producto;

/**
 * Created by OOROZCO on 08/12/15.
 */

public class ResumenViewController extends BaseActivity implements View.OnClickListener, CallBackModule{

    private ImageButton btnEnvioEmail;
    private ImageView imgvDivision2;

    private TextView btnSolicitarPrestamo;
    private TextView lblPagoMensual;
    private TextView lblMontoPagoMensual;
    private TextView txtEnviarCorreo;
    private TextView txtEditarCorreo;
    private TextView txtvCorreoSession;

    public EditText edtCorreoNuevo;

    private ListView lstContratados;

    private LinearLayout linearFooter;
    private LinearLayout llContent;
    private LinearLayout lyt_saldo;
    private LinearLayout linear_enviar_correo;
    private LinearLayout linear_mostrar_correo;
    private LinearLayout linear_editar_correo;

    private Button btnCancelarEnvioCorreo;
    private Button btnAceptarEnvioCorreo;
    private Button btnCancelarEditarCorreo;
    private Button btnEnviarEditarCorreo;
    private Button btnLimpiarCorreo;

    private int [] imageId;
    private String[] listSaldo;
    private String[] listMensual;
    private int total=0;
    private ResumenDelegate delegate;
    private Producto producto;
    public TextView textLayout;
    public boolean dataTDC = false;

    public ResumenViewController me;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_TITLE| SHOW_HEADER, R.layout.activity_credit_resumen);
        Log.i("resumenviewcontroller", "ResumenViewC getIsFrom: " + MenuPrincipalActivity.getInstance().isFromWB());
        imgBtnEliminar.setVisibility(View.INVISIBLE);
        setActivityChanging(false);

        me = this;
        MainController.getInstance().setContext(this);
        MainController.getInstance().setCurrentActivity(this);

        delegate = new ResumenDelegate();
        delegate.setController(this);
        initView();

        if(!Tools.getCurrentSession().getShowCreditosContratados()){ //Si no tiene creditos contratados
            Log.i("resumen", "no muestra labels");
            lblMontoPagoMensual.setVisibility(View.GONE);
            lblPagoMensual.setVisibility(View.GONE);
            ViewGroup.LayoutParams params = linearFooter.getLayoutParams();
            ((ViewGroup.MarginLayoutParams) params).topMargin = 300;
            linearFooter.setLayoutParams(params);
            linearFooter.requestLayout();
        }

        this.textLayout = (TextView) findViewById(R.id.lblPagoMensualTDC);
        if(dataTDC == true){
            this.textLayout.setText("Más el pago mínimo mensual para no generar intereses en tu Tarjeta.");
        }
    }

    private void initView(){
        findViews();
        muestraCorreoSession();
        setTable();
        setTextToView();
    }

    private void scaleView(){
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(btnSolicitarPrestamo);
        guiTools.scale(lyt_saldo);

    }

    private void findViews() {
        lblPagoMensual = (TextView) findViewById(R.id.lblPagoMensual);
        txtEnviarCorreo = (TextView) findViewById(R.id.txtEnviarCorreo);
        lblMontoPagoMensual = (TextView) findViewById(R.id.lblMontoPagoMensual);
        btnSolicitarPrestamo = (TextView) findViewById(R.id.btnSolicitarPrestamo);
        txtEditarCorreo = (TextView)findViewById(R.id.txtEditarCorreo);
        txtEditarCorreo.setOnClickListener(this);
        txtvCorreoSession = (TextView)findViewById(R.id.txtvCorreoSession);

        edtCorreoNuevo = (EditText)findViewById(R.id.edtCorreoNuevo);
        edtCorreoNuevo.setMovementMethod(new ScrollingMovementMethod());

        imgEncabezado.setImageResource(R.drawable.txt_pagos);
        btnEnvioEmail = (ImageButton) findViewById(R.id.btnEnvioEmail);
        btnEnvioEmail.setOnClickListener(this);

        lstContratados = (ListView) findViewById(R.id.lstContratados);

        llContent = (LinearLayout) findViewById(R.id.llContent);
        imgvDivision2 = (ImageView) findViewById(R.id.imgvDivision2);
        linearFooter = (LinearLayout)findViewById(R.id.linearFooter);
        lyt_saldo = (LinearLayout)findViewById(R.id.lyt_saldo);
        linear_enviar_correo = (LinearLayout)findViewById(R.id.linear_enviar_correo);
        linear_enviar_correo.setOnClickListener(this);
        linear_mostrar_correo = (LinearLayout)findViewById(R.id.linear_mostrar_correo);
        linear_editar_correo = (LinearLayout)findViewById(R.id.linear_editar_correo);

        btnCancelarEnvioCorreo = (Button)findViewById(R.id.btnCancelarEnvioCorreo);
        btnCancelarEnvioCorreo.setOnClickListener(this);
        btnAceptarEnvioCorreo = (Button)findViewById(R.id.btnAceptarEnvioCorreo);
        btnAceptarEnvioCorreo.setOnClickListener(this);
        btnCancelarEditarCorreo = (Button) findViewById(R.id.btnCancelarEditarCorreo);
        btnCancelarEditarCorreo.setOnClickListener(this);
        btnEnviarEditarCorreo = (Button) findViewById(R.id.btnEnviarEditarCorreo);
        btnEnviarEditarCorreo.setOnClickListener(this);
        btnLimpiarCorreo = (Button)findViewById(R.id.btnLimpiarCorreo);
        btnLimpiarCorreo.setOnClickListener(this);
    }

    private void setTextToView(){
        String enviarPorCorreo = "<u>"+getResources().getString(R.string.resumen_enviar_correo)+"</u>";
        String stringCorreo = "<font color=\"#094fa4\"><b>"+Tools.getCurrentSession().getEmail() + " "+"</b></font>";
        String correoSession = getResources().getString(R.string.resumen_correo_session)
                + stringCorreo;

        String stringEditarCorreo = "<u>"+getResources().getString(R.string.resumen_editar_correo)+"</u>";


        txtEditarCorreo.setText(Html.fromHtml(stringEditarCorreo), TextView.BufferType.SPANNABLE);
        txtEnviarCorreo.setText(Html.fromHtml(enviarPorCorreo), TextView.BufferType.SPANNABLE);
        txtvCorreoSession.setText(Html.fromHtml(correoSession), TextView.BufferType.SPANNABLE);
    }

    private void setTable() {
        SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
        TextView pagoMensual;
        if (sp.getBoolean(ConstantsCredit.IS_SHOWN, false)) {
            producto = Session.getInstance().getCreditos().getProductos().get(sp.getInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, 0));
            delegate.setProducto(producto);
            ((ImageView) findViewById(R.id.imgIconoUltimaSimulacion)).setImageResource(GuiTools.getResumenCreditIcon(producto.getCveProd()));
            ((TextView) findViewById(R.id.lblValorSaldo)).setText(GuiTools.getMoneyString(producto.getMontoDeseS()));
            ((TextView)findViewById(R.id.lblPagoMensualSimulacion)).setText(getResources().getString(R.string.lblResumenMensualPromo));

            pagoMensual = (TextView) findViewById(R.id.lblValorPagoMensual);
            pagoMensual.setText(GuiTools.getMoneyString(producto.getPagMProdS()));

            String code = producto.getCveProd();

            if (code.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)) {
                pagoMensual.setText(" ");
                btnSolicitarPrestamo.setText(getResources().getString(R.string.resumen_btn_solicitar));
                btnSolicitarPrestamo.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        aceptarIncremento();
                    }
                });

            } else if (code.equals(ConstantsCredit.TARJETA_CREDITO)) {
                this.dataTDC = true;
                pagoMensual.setText(" ");
                ((TextView)findViewById(R.id.lblPagoMensualSimulacion)).setVisibility(View.GONE);
                btnSolicitarPrestamo.setText(getResources().getString(R.string.resumen_btn_solicitar));
                btnSolicitarPrestamo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((DetalleTDCViewController) MainController.getInstance().getFlowFragment()).contratarTDC();
                    }
                });
            } else if (code.equals(ConstantsCredit.CREDITO_HIPOTECARIO)) {
                total += Double.parseDouble(producto.getPagMProdS());
                btnSolicitarPrestamo.setVisibility(View.GONE);
                btnSolicitarPrestamo.setBackground(getResources().getDrawable(R.drawable.btn_solicitar));
                btnSolicitarPrestamo.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ((DetalleHipotecarioViewController) MainController.getInstance().getFlowFragment()).solicitarAction();
                    }
                });

                SharedPreferences.Editor editor = sp.edit();
                editor.putBoolean(ConstantsCredit.SHARED_IS_RESUMEN, true);
                editor.commit();

            } else if (code.equals(ConstantsCredit.CREDITO_AUTO)) {
                total += Double.parseDouble(producto.getPagMProdS());
                btnSolicitarPrestamo.setText(getResources().getString(R.string.resumen_btn_solicitar));

                btnSolicitarPrestamo.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ((DetalleAutoViewController) MainController.getInstance().getFlowFragment()).solicitarAction();
                    }
                });

                btnSolicitarPrestamo.setVisibility(View.GONE);//JQH Oculta botón Auto
                SharedPreferences.Editor editor = sp.edit();
                editor.putBoolean(ConstantsCredit.SHARED_IS_RESUMEN, true);
                editor.commit();

            } else if (code.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)) {
                total += Double.parseDouble(producto.getPagMProdS());
                btnSolicitarPrestamo.setText(getResources().getString(R.string.resumen_btn_contratar));
                btnSolicitarPrestamo.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ((DetalleNominaPPIViewController) MainController.getInstance().getFlowFragment()).contratarAction();
                    }
                });
            } else if (code.equals(ConstantsCredit.CREDITO_NOMINA)) {
                total += Double.parseDouble(producto.getPagMProdS());
                btnSolicitarPrestamo.setText(getResources().getString(R.string.resumen_btn_contratar));
                btnSolicitarPrestamo.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ((DetalleNominaPPIViewController) MainController.getInstance().getFlowFragment()).contratarAction();
                    }
                });
            } else if (code.equals(ConstantsCredit.ADELANTO_NOMINA)) {
                if (producto.getCveProd().equalsIgnoreCase(ConstantsCredit.CODIGO_QUINCENAL)) {
                    total += Double.parseDouble(producto.getPagMProdS()) * 2;
                    pagoMensual.setText(GuiTools.getMoneyString("" + (Double.parseDouble(producto.getPagMProdS()) * 2)));
                } else {
                    ((TextView)findViewById(R.id.lblPagoMensualSimulacion)).setText(getResources().getString(R.string.lblResumenQuincenalPromo));
                    total += Double.parseDouble(producto.getPagMProdS());
                }

                btnSolicitarPrestamo.setText(getResources().getString(R.string.resumen_btn_contratar));
                btnSolicitarPrestamo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((DetalleAdelantoNomina) MainController.getInstance().getFlowFragment()).adquiereANOM();
                    }
                });
            }
        } else {
            findViewById(R.id.linearPromo).setVisibility(View.GONE);
            findViewById(R.id.imgDivisionPromo).setVisibility(View.GONE);
            btnSolicitarPrestamo.setVisibility(View.GONE);
        }

        Session session = Session.getInstance();
        ArrayList<CreditoContratado> creditos = session.getDataFromCreditos().getCreditosContratados();
        String getPagMTot = session.getDataFromCreditos().getPagMTot();

        //validación que, en caso de no parsear los créditos o el objeto venga vacío, eviatará que truene la app
        if (creditos == null) {
            Log.i("session", "creditos null ");
        } else {
            Log.i("session", "creditos not null: " + creditos);

            if (creditos != null && creditos.size() > 0) {
                imageId = new int[creditos.size()];
                listSaldo = new String[creditos.size()];
                listMensual = new String[creditos.size()];

                for (int i = 0; i < creditos.size(); i++) {
                    imageId[i] = GuiTools.getCreditIcon(creditos.get(i).getCveProd());
                    listSaldo[i] = String.valueOf(creditos.get(i).getSaldo());
                    listMensual[i] = String.valueOf(creditos.get(i).getPagoMen());
                    total += creditos.get(i).getPagoMen();
                }

                lstContratados.setAdapter(new ResumenAdapter(this, listSaldo, listMensual, imageId));
                ((TextView) findViewById(R.id.lblMontoPagoMensual)).setText(Tools.formatAmount(String.valueOf(total), false));

            } else {
                lstContratados.setVisibility(View.GONE);
                ((TextView) findViewById(R.id.lblMontoPagoMensual)).setText(Tools.formatAmount2(getPagMTot));
            }

            if (creditos.size() == 0) {
                llContent.setVisibility(View.GONE);
                imgvDivision2.setVisibility(View.GONE);
                Log.i("creditos", "CreditosContratados, IF");
            } else {
                llContent.setVisibility(View.VISIBLE);
                imgvDivision2.setVisibility(View.VISIBLE);
                Log.i("creditos", "CreditosContratados, ELSE");
            }
        }

        ((ScrollView)findViewById(R.id.body_layout)).smoothScrollTo(0, 0);
    }

    public void muestraCorreoSession(){
        linear_enviar_correo.setVisibility(View.VISIBLE);
        linear_mostrar_correo.setVisibility(View.GONE);
        linear_editar_correo.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        if(v == btnEnvioEmail){
            MainController.getInstance().showScreen(ResumenEmailViewController.class);
            setActivityChanging(true);
        }

        if(v == linear_enviar_correo){  //primer layout, envio de correo, se muestra enseguida el layout que tiene el correo de session
            //enviar correo
            linear_enviar_correo.setVisibility(View.GONE);
            linear_mostrar_correo.setVisibility(View.VISIBLE);

        }

        if(v == txtEditarCorreo){  //muestra layot de editar correo
            linear_mostrar_correo.setVisibility(View.GONE);
            linear_editar_correo.setVisibility(View.VISIBLE);
        }

        if(v == btnCancelarEnvioCorreo){ //regresa al layout de enviar correo
            linear_mostrar_correo.setVisibility(View.GONE);
            linear_enviar_correo.setVisibility(View.VISIBLE);
        }

        if(v == btnAceptarEnvioCorreo){
            delegate.enviarCorreo(true);
        }

        if(v == btnCancelarEditarCorreo){
            linear_editar_correo.setVisibility(View.GONE);
            linear_mostrar_correo.setVisibility(View.VISIBLE);
            edtCorreoNuevo.setText("");
        }

        if(v == btnEnviarEditarCorreo){
            if(delegate.isEmailValid(edtCorreoNuevo.getText().toString())){
                //método de envio de correo con el modificado
                delegate.enviarCorreo(false);
            }else{
                this.showInformationAlert("Formato de correo electrónico no válido, favor de verificarlo",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        edtCorreoNuevo.setEnabled(true);
                        edtCorreoNuevo.requestFocus();
                        edtCorreoNuevo.setText("");
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(edtCorreoNuevo, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }

        }

        if(v == btnLimpiarCorreo){
            edtCorreoNuevo.setText("");
        }

    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            try {

                Log.i("update", "Viene de DetalleAdelantoNomina getFlowFragment: " + MainController.getInstance().getFlowFragment());
                //
                Fragment oFragmeng = Session.getInstance().getoMenuPA().getActiveFragment();
                Log.i("resumenviewcontroller","ResumenVew.onKeyDw: "+MenuPrincipalActivity.getInstance().isFromWB());
                boolean isFromWebView = MenuPrincipalActivity.getInstance().isFromWB();

                if(isFromWebView) {
                    MenuPrincipalActivity.getInstance().setIsFromWB(false);
                    Log.i("resumenviewcontroller", "viene de Webview, manda a menuprincipala");
                    MainController.getInstance().showScreen(MenuPrincipalActivity.class);

                }else if(oFragmeng instanceof DetalleAdelantoNomina || oFragmeng instanceof DetalleTDCViewController || oFragmeng instanceof DetalleILCViewController
                        || oFragmeng instanceof DetalleNominaPPIViewController || oFragmeng instanceof  DetalleAutoViewController
                        || oFragmeng instanceof DetalleHipotecarioViewController){
                    Log.i("resumenviewcontroller", "MenuPrincipalActivity.getInstance().getActivefragment: " + MenuPrincipalActivity.getInstance().getActiveFragment());
                    Log.i("resumenviewcontroller", "MenuPrincipalActivity.getInstance(): " + MenuPrincipalActivity.getInstance());
                    Session.getInstance().getoMenuPA().getActiveFragment();

                }else {
                    Log.i("resumenviewcontroller","viene de otra cosa");
                    MainController.getInstance().showScreen(MenuPrincipalActivity.class);
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }


        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void returnToPrincipal() {
        MainController.getInstance().showScreen(MenuPrincipalActivity.class);
    }

    @Override
    public void returnDataFromModule(String s, ServerResponse serverResponse) {

    }

    public void aceptarIncremento(){AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Aviso");
        dialogo1.setMessage("¿Al autorizar esta oferta incrementará tu línea de crédito.?");
        dialogo1.setCancelable(false);
        dialogo1.setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                ((DetalleILCViewController)MainController.getInstance().getFlowFragment()).incrementarAction();
            }
        });
        dialogo1.setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {

            }
        });
        dialogo1.show();
    }

    public String getEdtCorreNuevo(){
        return edtCorreoNuevo.getText().toString();
    }

}
