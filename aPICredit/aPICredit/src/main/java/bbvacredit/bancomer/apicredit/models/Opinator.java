package bbvacredit.bancomer.apicredit.models;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.CreditJSONAble;

import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ParsingHandlerJSON;

/**
 * Created by FHERNANDEZ on 26/08/16.
 */
public class Opinator implements ParsingHandlerJSON, CreditJSONAble {

    private boolean responseObjet;
    private String responseMsg;
    private String responseMessage;

    public boolean isResponseObjet() {
        return responseObjet;
    }

    public void setResponseObjet(boolean responseObjet) {
        this.responseObjet = responseObjet;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    @Override
    public void fromJSON(String jsonString) {

        try {

            JSONObject obj = new JSONObject(jsonString);
            setResponseMessage(obj.getString(ConstantsCredit.LBL_RESPONSE_MESSAGE));
            setResponseMsg(obj.getString(ConstantsCredit.LBL_RESPONSE_MSG));
            android.util.Log.i("opinator","responseObj: "+obj.getString(ConstantsCredit.LBL_RESPONSE_OBJET));
            setResponseObjet(obj.getString(ConstantsCredit.LBL_RESPONSE_OBJET).equalsIgnoreCase("true") ? true : false);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void clearGetSet(){
        setResponseObjet(false);
        setResponseMsg("");
        setResponseMessage("");
    }

    @Override
    public String toJSON() {
        return null;
    }

    @Override
    public void processJSON(String jsonString) throws JSONException {

    }
}
