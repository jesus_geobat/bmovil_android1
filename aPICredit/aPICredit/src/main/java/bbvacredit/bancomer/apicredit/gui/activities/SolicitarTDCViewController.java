package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ScrollView;

import bbvacredit.bancomer.apicredit.R;

/**
 * Created by SDIAZ on 26/11/15.
 */
public class SolicitarTDCViewController extends Activity {
    private ScrollView body;

    public SolicitarTDCViewController() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_credit_base);
        body = (ScrollView) findViewById(R.id.body_layout);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.activity_credit_solicitar_tdc, body, true);
        super.onCreate(savedInstanceState);

        //findViews();

        //and then?
    }
}
