package bbvacredit.bancomer.apicredit.gui.delegates;

import bbvacredit.bancomer.apicredit.models.Plazo;
import bbvacredit.bancomer.apicredit.models.Producto;
import bbvacredit.bancomer.apicredit.models.Subproducto;

public abstract class BaseDelegateOperacion extends BaseDelegate {
	
	protected abstract String getCodigoOperacion();
	
	protected String getCvPlazoByValue(Producto producto, String plazoSel){
		String cvPlazo = null;
		
		boolean cont = true;
		for (int s=0; s<producto.getSubproducto().size() && cont; s++) {
			Subproducto subP = producto.getSubproducto().get(s);
			for (int p=0; p<subP.getPlazo().size() && cont; p++) {
				Plazo plazo = subP.getPlazo().get(p);
				if (plazo.getDesPlazo().equals(plazoSel)){
					cvPlazo = plazo.getCvePlazo();
					cont = false;
				}
			}
		}		
		
		return cvPlazo;
	}
	
	protected String getCvSubPByValue(Producto producto, String subPSel){
		String cvSubP = "";
		
		boolean cont = true;
		for (int s=0; s<producto.getSubproducto().size() && cont; s++) {
			if (producto.getSubproducto().get(s).getDesSubp().equalsIgnoreCase(subPSel)){
				cvSubP = producto.getSubproducto().get(s).getCveSubp();
				cont = false;
			}
			
		}		
		
		return cvSubP;
	}


}
