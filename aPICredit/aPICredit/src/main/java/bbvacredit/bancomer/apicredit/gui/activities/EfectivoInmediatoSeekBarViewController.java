package bbvacredit.bancomer.apicredit.gui.activities;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.LabelMontoTotalCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.gui.delegates.AdelantoNominaSeekBarDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.EfectivoInmediatoSeekBarDelegate;
import bbvacredit.bancomer.apicredit.models.OfertaEFI;
import bbvacredit.bancomer.apicredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by FHERNANDEZ on 30/12/16.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class EfectivoInmediatoSeekBarViewController extends Fragment implements View.OnClickListener{

    private Promociones promocion;

    private ImageView imgvProd;
    private ImageView imgvShowDetalle;
    private ImageView imgvAyuda;

    private ImageButton imgvContratarCredito;

    private LabelMontoTotalCredit edtMontoVisible;

    private TextView txtvDescProd;
    private TextView txtvMontoMinimo;
    private TextView txtvMontoMaximo;
    private TextView txtvCat;
    private TextView txtvTasa;
    private TextView txtvTextSaldoTotal;
    private TextView txtvSaldoTotalPagar;
    private TextView txtvOpcion;
    private TextView txtvPlazo;
    private TextView txtvTipo;
    private TextView txtvMasInfo;
    private TextView txtvPagoMMax;
    private TextView txtvMontoMMax;

    private LinearLayout lnlDetalleProd;
    private LinearLayout lnlTipo;
    private LinearLayout lnlPlazo;
    private LinearLayout lnlPlazoTipo;
    private LinearLayout lnlPagoMMaximo;

    private SeekBar seekBarSimulador;

    private String montoMax = "";
    private String montMin = "";
    private StringBuffer typedString= new StringBuffer();
    private String amountString=null;
    private boolean indSimProd;
    private String auxFinalMax;

    private boolean isSettingText = false;
    private boolean isResetting = false;
    private boolean mAcceptCents=false;
    private boolean flag=true;
    public boolean isShowDetalle;
    private boolean isSimulated = false;
    public boolean isFromBar;


    private EfectivoInmediatoSeekBarDelegate delegate;
    private EfectivoInmediatoSeekBarViewController controller;

    private final int INCREMENTAL = 100;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_credit_item_listview_creditos,container,false);

        if(rootView != null){
            delegate = new EfectivoInmediatoSeekBarDelegate(this);
            controller = this;
            initView(rootView);
            delegate.setPromocion(getPromocion());
            setIsShowDetalle(false);
        }

        return rootView;
    }

    private void initView(View rootView){
        findViews(rootView);
        validateSO();
        setValuesProduct();
        setValuesSeekBar();
    }

    private void setValuesSeekBar(){

        final double mmax = Double.parseDouble(Tools.validateMont(getMontoMax(), false));
//        final double mmin = Double.parseDouble(Tools.validateMont(getMontMin(), false));

        Log.i("efi","mmax: "+mmax);
        Log.i("efi","mmax: "+(int)mmax);

        seekBarSimulador.setMax(1);
        seekBarSimulador.setProgress((int) mmax);

        seekBarSimulador.setEnabled(false);
        edtMontoVisible.setEnabled(false);

    }

    private void setValuesProduct(){
        String montoMaximo, auxMMax, montoMinimo, auxMMin, cveProd;

        int imgProducto, descProd;

        cveProd         = getPromocion().getCveCamp().substring(0, 4);
        montoMaximo     = getPromocion().getMonto();
//        montoMinimo     = getPromocion().getMonto();
        imgProducto     = GuiTools.getCreditIcon(cveProd, false);
        descProd        = GuiTools.getSelectedLabel(cveProd);

        Log.i("efi", "montoMax: " + montoMaximo);

        imgvProd.setImageResource(imgProducto);
        txtvDescProd.setText(descProd);

        montoMaximo = montoMaximo.substring(0, montoMaximo.length() - 2);

        auxMMax = Tools.formatUserAmount(Tools.validateMont(montoMaximo,false));

        if(auxMMax.contains("$"))
            auxMMax = auxMMax.replace("$","");

        setMontoMax(auxMMax);

        txtvMontoMinimo.setVisibility(View.GONE);
        txtvMontoMaximo.setText(auxMMax);
        edtMontoVisible.setMontoMax(getMontoMax());
        edtMontoVisible.setCveProd(getPromocion().getCveCamp().substring(0, 4));

        edtMontoVisible.setText(Tools.formatUserAmount(montoMaximo));
    }

    private void validateSO(){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH){
            seekBarSimulador.setThumb(getResources().getDrawable(R.drawable.seek_bar_thumb));
            seekBarSimulador.setProgressDrawable(getResources().getDrawable(R.drawable.custom_seek_bar));
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setThumbOffset(0);
        }
    }

    private void findViews(View vista){
        imgvProd        = (ImageView)vista.findViewById(R.id.imgvProd);
        imgvShowDetalle = (ImageView)vista.findViewById(R.id.imgvShowDetalle);
        imgvShowDetalle.setOnClickListener(this);

        imgvAyuda       = (ImageView)vista.findViewById(R.id.imgvAyuda);
        imgvAyuda.setOnClickListener(this);

        imgvContratarCredito = (ImageButton)vista.findViewById(R.id.imgvContratarCredito);
        imgvContratarCredito.setOnClickListener(this);

        edtMontoVisible    = (LabelMontoTotalCredit)vista.findViewById(R.id.edtMontoVisible);

        txtvDescProd        = (TextView)vista.findViewById(R.id.txtvDescProd);
        txtvMontoMinimo     = (TextView)vista.findViewById(R.id.txtvMontoMinimo);
        txtvMontoMinimo.setVisibility(View.GONE);
        txtvMontoMaximo     = (TextView)vista.findViewById(R.id.txtvMontoMaximo);
        txtvCat             = (TextView)vista.findViewById(R.id.txtvCat);
        txtvOpcion          = (TextView)vista.findViewById(R.id.txtvTipo);
        txtvPlazo           = (TextView)vista.findViewById(R.id.txtPlazo);
        txtvTasa            = (TextView)vista.findViewById(R.id.txtvTasa);
        txtvTipo            = (TextView)vista.findViewById(R.id.txtvTipo);
        txtvMasInfo         = (TextView)vista.findViewById(R.id.txtvMasInfo);
        txtvPagoMMax        = (TextView)vista.findViewById(R.id.txtvPagoMMax);
        txtvMontoMMax       = (TextView)vista.findViewById(R.id.txtvMontoMMax);

        lnlDetalleProd      = (LinearLayout)vista.findViewById(R.id.lnlDetalleProd);
        lnlTipo             = (LinearLayout)vista.findViewById(R.id.lnlTipo);
        lnlTipo.setOnClickListener(this);

        lnlPlazo            = (LinearLayout)vista.findViewById(R.id.lnlPlazo);
        lnlPlazo.setOnClickListener(this);

        lnlPlazoTipo        = (LinearLayout)vista.findViewById(R.id.lnlPlazoTipo);
        lnlPagoMMaximo      = (LinearLayout)vista.findViewById(R.id.lnlPagoMMaximo);

        seekBarSimulador    = (SeekBar)vista.findViewById(R.id.seekBarSimulador);

    }

    @Override
    public void onClick(View v) {

        if (v == imgvAyuda) {
            PopUpAyudaViewController.mostrarPopUp((MenuPrincipalActivitySeekBar)getActivity(),getPromocion().getCveCamp().substring(0,4));
        }

        if(v == imgvContratarCredito){
            delegate.contratarEFI();
        }

        if(v == imgvShowDetalle){


            if(isSimulated){ //si ya fue simulado, muestra detalle
                Log.i("efi","producto simulado");
                validateLayoutToShow();
            }else{
                Log.w("efi","producto no simulado");
                isFromBar = false;
                simulateEFI();
            }
        }
    }

    public void setSimulation(){
        ((MenuPrincipalActivitySeekBar) getActivity()).isTDCS = true;
        ((MenuPrincipalActivitySeekBar) getActivity()).validateTxtvColor(controller);

        setColor(false);
    }

    private void simulateEFI(){
        delegate.realizaOperacionEFI();
    }

    public void setColor(boolean isNormal){
        if(isNormal)
            edtMontoVisible.setTextColor(getResources().getColor(R.color.nuevaimagen_tx5));
        else
            edtMontoVisible.setTextColor(getResources().getColor(R.color.naranja));
    }

    public void validateLayoutToShow(){

        //oculta detalle
        if(isShowDetalle()) {
            setIsShowDetalle(false);
            lnlDetalleProd.setVisibility(View.GONE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_masdetalle);

        }else{
            ((MenuPrincipalActivitySeekBar)getActivity()).isEFIDetail = true;
            ((MenuPrincipalActivitySeekBar)getActivity()).validateIsShowAnyDetail(this);

            setIsShowDetalle(true);
            lnlDetalleProd.setVisibility(View.VISIBLE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_menosdetalle);
            imgvContratarCredito.setVisibility(View.VISIBLE);
            lnlPlazoTipo.setVisibility(View.GONE);
            lnlPagoMMaximo.setVisibility(View.VISIBLE);

            setDetailProduct();
        }

    }

    private void setDetailProduct(){
        try {

            OfertaEFI ofertaEFI = delegate.getOfertaEFI();

            String clveMesQuincena, cat, tasa, totalMesQuincena;

            tasa = ofertaEFI.getTasaAnual();
            cat = ofertaEFI.getCat();
            totalMesQuincena = ofertaEFI.getPagoMensualSimulador();


            txtvPagoMMax.setText(getResources().getString(R.string.detalle_adelanto_nomina_pago_mensual));

            txtvTasa.setText("Tasa de interés: "+tasa+" %");
            txtvCat.setText("CAT: "+cat+"% sin iva");
            Log.i("efi", "efiAmount: " + totalMesQuincena.substring(0, totalMesQuincena.length() - 2));
            Log.i("efi", "efiFormatAmount: "+Tools.formatUserAmount(totalMesQuincena.substring(0,totalMesQuincena.length()-2)));
            txtvMontoMMax.setText(Tools.formatUserAmount(totalMesQuincena.substring(0,totalMesQuincena.length()-2)));

        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    private int calculatePogress(int progress, int min, int progresFinal, int mmax){

        int auxMin = min;

        if (progress == 0){
            return min;
        }else {

            for (int i = 1; i<=progress; i++) {
                auxMin = auxMin + INCREMENTAL;
                if (progress == progresFinal){
                    auxMin = mmax;
                }
            }
        }

        return auxMin;
    }

    public void reset() {
        this.isResetting = true;
        this.typedString=new StringBuffer();
        this.isResetting = false;
    }

    private void setFormattedText(){

        isSettingText = true;
        String text = typedString.toString();

        edtMontoVisible.setText(Tools.formatUserAmount(text));
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public void setPromocion(Promociones promocion) {
        this.promocion = promocion;
    }

    public String getMontoMax() {
        return montoMax;
    }

    public void setMontoMax(String montoMax) {
        this.montoMax = montoMax;
    }

    public String getMontMin() {
        return montMin;
    }

    public void setMontMin(String montMin) {
        this.montMin = montMin;
    }

    public boolean isIndSimProd() {
        return indSimProd;
    }

    public void setIndSimProd(boolean indSimProd) {
        this.indSimProd = indSimProd;
    }

    public boolean isShowDetalle() {
        return isShowDetalle;
    }

    public void setIsShowDetalle(boolean isShowDetalle) {
        this.isShowDetalle = isShowDetalle;
    }

    public String getAuxFinalMax() {
        return auxFinalMax;
    }

    public void setAuxFinalMax(String auxFinalMax) {
        this.auxFinalMax = auxFinalMax;
    }

}
