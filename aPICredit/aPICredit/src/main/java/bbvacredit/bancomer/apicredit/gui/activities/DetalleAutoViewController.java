package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.ICommonSession;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.AutoDelegate;
import tracking.TrackingHelperCredit;

/**
 * Created by OOROZCO on 12/9/15.
 */
public class DetalleAutoViewController extends Fragment {

    // ------------- Attributes Region ----------- //

    private ImageButton btnRecalcularSimulacion;
    private ImageButton btnSolicitarSimulacion;
    private TextView lblResultadoPago;
    private TextView lblResultadoPlazo;
    private TextView lblResultadoTasa;
    private TextView lblCat;
    private Button btnVerResumenPagos;
    private DetalleAutoViewController detalleAuto;
    private Session session;
    private MainController parentManager = MainController.getInstance();


    private AutoDelegate delegate;


    public void setDelegate(AutoDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_credit_resultado_simulacion, container, false);

        TrackingHelperCredit.trackState("creditsimAutoCont:detalle", parentManager.estados);

        detalleAuto = this;
        delegate.setDetalleAuto(this);
        findViews(rootView);

        return  rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TrackingHelperCredit.trackState("creditsimNominaCont", parentManager.estados);

        session = Tools.getCurrentSession();
        Integer getProdIndex = session.getProductoIndexByCve(ConstantsCredit.CREDITO_AUTO);

        Log.i("opinator", "guardando cveProd: " + delegate.getData().getProductos().get(getProdIndex).getCveProd());

        String tasa= getActivity().getString(R.string.lblTasaNomina);
        String cat= getActivity().getString(R.string.lblAutoCat1) + " " + delegate.getData().getProductos().get(getProdIndex).getCATS() + getActivity().getString(R.string.lblAutoCat2)+ " " + delegate.getProducto().getFechaCatS() + ".";

        ((MenuPrincipalActivity) getActivity()).setIsSimulate(true);
        ((MenuPrincipalActivity) getActivity()).setoFragment(this);


        lblResultadoPago.setText(GuiTools.getMoneyString(delegate.getData().getProductos().get(getProdIndex).getPagMProdS()));
        lblResultadoPlazo.setText(delegate.getData().getProductos().get(getProdIndex).getDesPlazoE());
        lblResultadoTasa.setText(tasa+ " " +delegate.getData().getProductos().get(getProdIndex).getTasaS()+ "%");
        lblCat.setText(cat);

    }

    private void findViews(View root) {
        btnRecalcularSimulacion = (ImageButton) root.findViewById(R.id.btnRecalcularSimulacion);
        btnSolicitarSimulacion = (ImageButton) root.findViewById(R.id.btnContratarANOM);
        btnSolicitarSimulacion.setImageResource(R.drawable.btn_solicitar); //Here my doubts.
        btnSolicitarSimulacion.setVisibility(View.GONE);
        btnVerResumenPagos = (Button) root.findViewById(R.id.btnVerResumenPagos);

        lblResultadoPago = (TextView) root.findViewById(R.id.lblResultadoPago);
        lblResultadoPlazo = (TextView) root.findViewById(R.id.lblResultadoPlazo);
        lblResultadoTasa = (TextView) root.findViewById(R.id.lblResultadoTasa);
        lblCat = (TextView) root.findViewById(R.id.lblCat);

        btnRecalcularSimulacion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                delegate.saveOrDeleteSimulationRequest(false); //se manda false para eliminar
                //((MenuPrincipalActivity) getActivity()).showAuto();
            }
        });

        btnSolicitarSimulacion.setOnClickListener(new View.OnClickListener() {
                                                      public void onClick(View v) {

                                                          SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
                                                          SharedPreferences.Editor editor = sp.edit();
                                                          editor.putBoolean(ConstantsCredit.SHARED_IS_RESUMEN, false);
                                                          editor.commit();
                                                          solicitarAction();
                                                      }
                                                  }
        );

        btnVerResumenPagos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Session.getInstance().setoDelegate(delegate);
                MainController.getInstance().showScreen(ResumenViewController.class);
                MainController.getInstance().setFlowFragment(DetalleAutoViewController.this);


            }
        });

    }

    public void solicitarAction() {
        String params = "";
        ICommonSession sesion = MainController.getInstance().getSession();
        params = "nombredepila=" + sesion.getNombreCliente().split(" ")[0] + "&segmento=AUTO&credito=" + delegate.getController().getMonto() + "&idpagina=simulacion";

        Map<String,Object> click_paso2_operacion = new HashMap<String,Object>();
        click_paso2_operacion.put("inicio","event45");
        click_paso2_operacion.put("&&products", "simulador;simulador:simulador credito auto");
        click_paso2_operacion.put("eVar12", "seleccion contactame");
        TrackingHelperCredit.trackPaso1Operacion(click_paso2_operacion);

        WebViewController.setParams(params);
        WebViewController.setIsAuto(true);

        MainController.getInstance().showScreen(WebViewController.class);
    }

    public void simpleUpdateMenu(String cveProd){

        if(((MenuPrincipalActivity)getActivity()) == null)
            Log.i("eliminar", "Menu es nulo");

        Log.i("eliminar", "actualizaMenu AUTO, cveProd: " + cveProd);
        ((MenuPrincipalActivity)getActivity()).updateMenu(cveProd);
        getFragmentManager().beginTransaction().remove(((MenuPrincipalActivity) getActivity()).getActiveFragment()).commit();
        ((MenuPrincipalActivity)getActivity()).showAuto();
    }
}
