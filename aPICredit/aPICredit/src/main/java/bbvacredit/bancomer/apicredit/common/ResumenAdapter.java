package bbvacredit.bancomer.apicredit.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import bbvacredit.bancomer.apicredit.R;

/**
 * Created by OOROZCO on 12/10/15.
 */
public class ResumenAdapter  extends BaseAdapter {


    private Context context;
    private int [] imageId;
    private String[] listSaldo;
    private String[] listMensual;
    private ImageView productIcon;
    private TextView lblSaldo;
    private TextView lblMensual;


    private static LayoutInflater inflater=null;

    public ResumenAdapter(Context context, String [] listSaldo, String [] listMensual, int [] imageId ) {
        this.context=context;
        this.imageId = imageId;
        this.listSaldo = listSaldo;
        this.listMensual = listMensual;

        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return listSaldo.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView;

        rowView = inflater.inflate(R.layout.activity_credit_custom_item, null);
        productIcon = (ImageView) rowView.findViewById(R.id.productIcon);
        lblSaldo = (TextView) rowView.findViewById(R.id.lblSaldo);
        lblMensual = (TextView) rowView.findViewById(R.id.lblMensual);

        productIcon.setImageResource(imageId[position]);
        lblSaldo.setText(Tools.formatAmount(listSaldo[position],false));
        lblMensual.setText(Tools.formatAmount(listMensual[position],false));

        return rowView;
    }

}
