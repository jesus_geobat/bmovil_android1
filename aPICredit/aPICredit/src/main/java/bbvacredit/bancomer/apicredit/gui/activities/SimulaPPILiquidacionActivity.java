package bbvacredit.bancomer.apicredit.gui.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

import bancomer.api.common.timer.TimerController;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.ListaDatosController;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.SimulaPPIDelegate;
import bbvacredit.bancomer.apicredit.models.Producto;

public class SimulaPPILiquidacionActivity extends BaseActivity implements OnClickListener {

	private SimulaPPIDelegate delegate;
	private ImageButton refreshBtn;
	private Button resumenLiquidacionBtn;
	
	//private ImageView simulaBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.activity_simula_ppiliquidacion);
		isOnForeground = true;
		MainController.getInstance().setCurrentActivity(this);
		init();
		//delegate.doRecalculoNC(this);
		deleteSimulation();
	}
	
	private void init(){
		// le indicamos al delegate que no es contratacion
		Tools.setIsContratacionPreference(false);
		delegate = new SimulaPPIDelegate();
		setSaldo();
		// Ocultamos la info si es necesario
		if(delegate.getOcultaInfo()){
			ocultaInfoNC();
		}else{
			pintarInfoTabla();
		}
		scaleToCurrentScreen();
		mapearBotones();
		Tools.getCurrentSession().setVisibilityButton(this, R.id.simPPILBottomMenuRC);
	}
	
	private void setSaldo(){
		TextView saldo = (TextView)findViewById(R.id.simPPILILMtxt);
		
		saldo.setText(GuiTools.getMoneyString(delegate.getSaldo().toString()));
	}
	
	// Oculta Info
	private void ocultaInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCAuto);
		tlayout.setVisibility(View.INVISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		tableLayout.setVisibility(View.INVISIBLE);
	}
	
	private void muestraInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCAuto);
		tlayout.setVisibility(View.VISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		tableLayout.setVisibility(View.VISIBLE);
	}
	// End Oculta Info
	
	private void mapearBotones(){
		//refreshBtn = (ImageButton)findViewById(R.id.headerRefresh);
		refreshBtn.setOnClickListener(this);
		
		resumenLiquidacionBtn = (Button)findViewById(R.id.simPPILBottomMenuRC);
		resumenLiquidacionBtn.setOnClickListener(this);
		
		//simulaBtn = (ImageView)findViewById(R.id.simPPILHeaderLogo);
		//simulaBtn.setOnClickListener(this);
	}
	
	private void scaleToCurrentScreen(){
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(findViewById(R.id.simPPILLayout));
		guiTools.scale(findViewById(R.id.simPPILBodyLayout));
		guiTools.scale(findViewById(R.id.simPPILTitle),true);
		guiTools.scale(findViewById(R.id.simPPILSubTitleTCImg));
		//guiTools.scale(findViewById(R.id.simPPILLText),true);
		guiTools.scale(findViewById(R.id.simPPILILMtxt),true);
		//guiTools.scale(findViewById(R.id.simPPILHeaderLogo));
		guiTools.scale(findViewById(R.id.simPPILBottomLayout));
		guiTools.scale(findViewById(R.id.simPPILBottomMenuRC),true);

		guiTools.scale(findViewById(R.id.simCAuto));
		guiTools.scale(findViewById(R.id.simCAutoLlblAuxiliar),true);
		
		//guiTools.scale(findViewById(R.id.simCAutoLblInfo),true);
		guiTools.scale(findViewById(R.id.RelativeLayoutText1));
	}
	
	private void ocultaCamposTabla(){
		LinearLayout layout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		layout.removeAllViews();
		
	}
	
	public void pintarInfoTabla(){
		ocultaCamposTabla();
		
		muestraInfoNC();
		
		ArrayList<Producto> lista = delegate.getProductTable();
		Iterator<Producto> it = lista.iterator();
		Integer cont = 0;

		ListaDatosController listaController;
		 String prodDesc ="";
		while(it.hasNext()){
			Producto p = it.next();
			
			if(p.getCveProd().equals("0NOM") || p.getCveProd().equals("0PPI")) 
			{
	  prodDesc = "PRESTAMO PERSONAL";
			                       } 
			else {
			                               prodDesc = p.getDesProd();
			                       }

			listaController = new ListaDatosController((LinearLayout)findViewById(R.id.RelativeLayoutText1), this, this, R.id.RelativeLayoutText1);
			listaController.addElement(prodDesc, GuiTools.getMoneyString(p.getSubproducto().get(0).getMonMax().toString()), false);
			++cont;
		}
	}

	@Override
	public void onClick(View v) {
		isOnForeground = false;
		/*if(v.getId() == R.id.headerRefresh){
			delegate.setRet();
			delegate.redirectToView(OldMenuPrincipalActivity.class);
		}else*/ if(v.getId() == R.id.simPPILBottomMenuRC){
			delegate.redirectToView(ResumenActivity.class);
		//}else if(v.getId() == R.id.simPPILHeaderLogo){
			//delegate.doRecalculoNC(this);
			
		}
	}
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(OldMenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }

	/**
	 * June 8th,2015,
	 */
	
	private void deleteSimulation()
	{
		delegate.deleteSimulationRequest(this,true);
	}
	
	public void doRecalculo()
	{
		Log.d("Elimina Simulacion", "ok");
		delegate.doRecalculoNC(this);
	}
	
	
}
