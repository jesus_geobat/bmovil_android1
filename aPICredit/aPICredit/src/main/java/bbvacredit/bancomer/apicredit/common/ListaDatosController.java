package bbvacredit.bancomer.apicredit.common;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bbvacredit.bancomer.apicredit.R;



public class ListaDatosController {
	
	private LinearLayout layout;
	
	private Context cnt;
	
	private Activity act;
	
	private int idLayout;
	
	public ListaDatosController(){
		
	}
	
	public ListaDatosController(LinearLayout layout, Context cnt, Activity act, int idLayout) {
		this.layout = layout;
		this.cnt = cnt;
		this.idLayout = idLayout;
		this.act = act;
	}

	public void addElement(String descripcion, String valor, Boolean mostrarBarra){
		LinearLayout myLo = (LinearLayout) LayoutInflater.from(cnt).inflate(R.layout.lista_datos_template_credit, null);
		TextView desc = ((TextView)myLo.findViewById(R.id.textView1));
		TextView val = ((TextView)myLo.findViewById(R.id.textView2));
		ImageView barraUp = (ImageView)myLo.findViewById(R.id.imageView2);
		ImageView barraDown = (ImageView)myLo.findViewById(R.id.imageView1);
		
		desc.setText(descripcion);
		val.setText(valor);
		
		if(!mostrarBarra) barraUp.setVisibility(View.GONE);
		
		layout.addView(myLo);
		
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(act.getWindowManager());
		guiTools.scale(act.findViewById(idLayout));
		guiTools.scale(desc,true);
		guiTools.scale(val,true);
		guiTools.scale(barraUp);
		guiTools.scale(barraDown);
		guiTools.scale(myLo);
		
		layout.requestLayout(); 
	}
	
	public void addCreditoContratadoElement(String descripcion, String valor, Boolean mostrarBarra, Boolean isTitle){
		LinearLayout myLo = (LinearLayout) LayoutInflater.from(cnt).inflate(R.layout.lista_datos_template_credit, null);
		TextView desc = ((TextView)myLo.findViewById(R.id.textView1));
		TextView val = ((TextView)myLo.findViewById(R.id.textView2));
		ImageView barraUp = (ImageView)myLo.findViewById(R.id.imageView2);
		ImageView barraDown = (ImageView)myLo.findViewById(R.id.imageView1);
		
		desc.setText(descripcion);
		val.setText(valor);
		
		if(isTitle) val.setTextColor(act.getResources().getColor(R.color.tercer_azul));
		
		if(!mostrarBarra) barraUp.setVisibility(View.GONE);
		
		layout.addView(myLo);
		
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(act.getWindowManager());
		guiTools.scale(act.findViewById(idLayout));
		guiTools.scale(desc,true);
		guiTools.scale(val,true);
		guiTools.scale(barraUp);
		guiTools.scale(barraDown);
		guiTools.scale(myLo);
		
		layout.requestLayout(); 
	}

}
