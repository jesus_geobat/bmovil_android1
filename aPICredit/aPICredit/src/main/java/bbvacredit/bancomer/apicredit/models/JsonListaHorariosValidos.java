package bbvacredit.bancomer.apicredit.models;

import java.util.List;

public class JsonListaHorariosValidos {
	
	private List<JsonHorarioOperacion> horariosOperacion;

	public JsonListaHorariosValidos(List<JsonHorarioOperacion> horariosOperacion) {
		super();
		this.horariosOperacion = horariosOperacion;
	}

	public List<JsonHorarioOperacion> getHorariosOperacion() {
		return horariosOperacion;
	}

	public void setHorariosOperacion(List<JsonHorarioOperacion> horariosOperacion) {
		this.horariosOperacion = horariosOperacion;
	}

}
