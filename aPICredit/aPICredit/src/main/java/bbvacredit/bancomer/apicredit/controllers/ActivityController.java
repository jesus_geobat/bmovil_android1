package bbvacredit.bancomer.apicredit.controllers;

import android.content.Intent;

import bbvacredit.bancomer.apicredit.gui.activities.BaseActivity;


public class ActivityController {
	private static ActivityController theInstance = null;
	
	private BaseActivity currentActivity;

	public BaseActivity getCurrentActivity() {
		return currentActivity;
	}

	void setCurrentActivity(BaseActivity currentActivity) {
		this.currentActivity = currentActivity;
	}
	
	void showScreen(Class<?> activity) {
        currentActivity.setActivityChanging(true);
		Intent intent = new Intent(currentActivity, activity);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		currentActivity.startActivity(intent);
	}
}
