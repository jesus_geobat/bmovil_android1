package bbvacredit.bancomer.apicredit.gui.delegates;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Hashtable;
import java.util.Iterator;

import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.AutoViewController;
import bbvacredit.bancomer.apicredit.gui.activities.DetalleAutoViewController;
import bbvacredit.bancomer.apicredit.gui.activities.HipotecarioViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;

/**
 * Created by OOROZCO on 12/8/15.
 */
public class AutoDelegate extends BaseDelegateOperacion {

    // Attributes Region //

    private Session session;
    private Integer productIndex = 0;
    private Producto producto = null;
    private ObjetoCreditos data;
    private AutoViewController controller;
    private boolean updateData = false;
    private boolean isDeleteOperation = false; //si es true, borrar simulación
    public DetalleAutoViewController detalleAuto;

    public AutoViewController getController() {
        return controller;
    }

    public DetalleAutoViewController getDetalleAuto() {
        return detalleAuto;
    }

    public void setDetalleAuto(DetalleAutoViewController detalleAuto) {
        this.detalleAuto = detalleAuto;
    }

    public void setProducto() {
        SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
        productIndex = sp.getInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, 0);
        producto = data.getProductos().get(productIndex);
    }

    public ObjetoCreditos getData() {
        return data;
    }

    public Producto getProducto() {
        return producto;
    }

    public AutoDelegate(AutoViewController controller) {

        session = Tools.getCurrentSession();
        data = session.getCreditos();
        this.controller = controller;

        MainController.getInstance().muestraIndicadorActividad("Operación", "Conectando");
        setProducto();
        MainController.getInstance().ocultaIndicadorActividad();

    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    public void doRecalculo(){

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);

        Producto ccr = data.getProductos().get(productIndex);

        String plazoSel = controller.getCvePlazo();
        int montoSel = Integer.valueOf(controller.getMonto());

        String cveHipotecaSel = getCvSubPByValue(producto, controller.getOption());
        String cvePlazoSel = getCvPlazoByValue(producto, plazoSel);

        Log.i("sim","Auto - plazoSeleccionado: "+plazoSel);
        Log.i("sim","Auto - montoSeleccionado: "+montoSel);
        Log.i("sim","Auto - cveHipotecaSel: "+cveHipotecaSel);
        Log.i("sim","Auto - cvePlazoSel: "+cvePlazoSel);

        addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM,paramTable);
        addParametro(cveHipotecaSel, ServerConstantsCredit.CVESUBP_PARAM,paramTable);
        addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM,paramTable);
        addParametro(cvePlazoSel, ServerConstantsCredit.CVEPLAZO_PARAM,paramTable);

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    public void updateData(String cveSub, String cvePzoE){

        updateData = true;

        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        addParametroObligatorio(ConstantsCredit.OP_AJUSTE_PLAZOS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);

        Producto ccr = data.getProductos().get(productIndex);

        addParametroObligatorio(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM, paramTable);
        addParametroObligatorio(cvePzoE, ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        addParametroObligatorio("", ServerConstantsCredit.MON_DESE_PARAM, paramTable);
        addParametroObligatorio(cveSub, ServerConstantsCredit.CVESUBP_PARAM, paramTable);

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    private void setSimLooking4Cve(String cve){
        Iterator<Producto> it = data.getProductos().iterator();
        while(it.hasNext()){
            Producto pr = it.next();
            if(pr.getCveProd().equals(cve)) pr.setIndSimBoolean(true);
        }
    }

    public void analyzeResponse(String operationId, ServerResponse response) {
        if(getCodigoOperacion().equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

                CalculoData respuesta = (CalculoData) response.getResponse();

                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                session.setCreditos(data);
                productIndex = session.getProductoIndexByCve(ConstantsCredit.CREDITO_AUTO);
                producto = data.getProductos().get(productIndex);

                if(data.getProductos().get(productIndex).getCveProd().equals(ConstantsCredit.CREDITO_AUTO))
                    data.getProductos().get(productIndex).setIndSimBoolean(true);
                else
                   setSimLooking4Cve(ConstantsCredit.CREDITO_AUTO);

                setProducto();

                saveOrDeleteSimulationRequest(true);

            }
        }else if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

                CalculoData respuesta = (CalculoData) response.getResponse();

                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                session.setCreditos(null);
                session.setCreditos(data);
                productIndex = session.getProductoIndexByCve(ConstantsCredit.CREDITO_AUTO);
                producto = data.getProductos().get(productIndex);

                Log.i("simulador","autoDelegate, indSim: "+data.getProductos().get(productIndex).getIndSim());

                setProducto();

                if(isDeleteOperation){
                    isDeleteOperation = false;
                    getDetalleAuto().simpleUpdateMenu(producto.getCveProd());

                }else if(updateData) {

                    updateData = false;
                    controller.updateMenu();
                    doRecalculo();
//                    Log.i("update","updateData, auto: "+producto.getMontoMaxS());
//
//                    controller.updateMenu();
                }else
                    controller.showDetalleAuto();
            }

        }
    }

    @Override
    protected String getCodigoOperacion() {
        // TODO Auto-generated method stub
        return Server.CALCULO_OPERACION;
    }

    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {
        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        addParametroObligatorio(Server.CALCULO_ALTERNATIVAS_OPERACION, ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        if(isSavingOperation) {
            addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = false;
        }else {
            addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = true;
        }
        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
    }


}
