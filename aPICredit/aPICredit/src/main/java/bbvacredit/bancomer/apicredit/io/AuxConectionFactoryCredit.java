package bbvacredit.bancomer.apicredit.io;

import java.util.Hashtable;

import bancomer.api.apiventatdc.commons.ConstantsVentaTDC;
import bancomer.api.common.commons.Constants;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;

/**
 * Created by smontesdeoca on 11/05/2016.
 */
public class AuxConectionFactoryCredit {


    public static Hashtable<String, String> consultaCorreo(Hashtable<String, ?> params)  {
        // ConsultaCorreoData data = new ConsultaCorreoData();

        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CONSULTA_CORREO_OPERACION)));
        paramNew.put(ServerConstantsCredit.CLIENTE_PARAM, validaNull(params.get(ServerConstantsCredit.CLIENTE_PARAM)));
        paramNew.put(ServerConstantsCredit.IUM_PARAM, validaNull(params.get(ServerConstantsCredit.IUM_PARAM)));
        paramNew.put(ServerConstantsCredit.NUMERO_CEL_PARAM, validaNull(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));

        return paramNew;

    }

    public static Hashtable<String, String> realizaOPEfi(Hashtable<String, ?> params)  {
        Hashtable<String, String> paramNew = new Hashtable<String, String>();

//
//        params.put("numeroCelular", user);
//        params.put("cveCamp", promocion.getCveCamp());
//        params.put("IUM", ium);
//
//
        paramNew.put(ServerConstantsCredit.IUM_PARAM, validaNull(params.get(ServerConstantsCredit.IUM_PARAM)));
        paramNew.put(ServerConstantsCredit.NUMERO_CEL_PARAM, validaNull(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));
        paramNew.put("cveCamp", validaNull(params.get("cveCamp")));

        return paramNew;
    }


    public static Hashtable<String, String> guardarEliminarSimulacion(Hashtable<String, ?> params)  {
        //Log.d("Entra a proceso de Server:", "Guardar o Eliminar Simulación");

        Hashtable<String, String> paramNew = new Hashtable<String, String>();

        //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION)));
        paramNew.put(ServerConstantsCredit.CLIENTE_PARAM, validaNull(params.get(ServerConstantsCredit.CLIENTE_PARAM)));

        paramNew.put(ServerConstantsCredit.IUM_PARAM, validaNull(params.get(ServerConstantsCredit.IUM_PARAM)));
        paramNew.put(ServerConstantsCredit.NUMERO_CEL_PARAM, validaNull(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));
        paramNew.put(ServerConstantsCredit.TIPO_OP_PARAM, validaNull(params.get(ServerConstantsCredit.TIPO_OP_PARAM)));

        return paramNew;
    }

    public static Hashtable<String, String> calculo(Hashtable<String, ?> params) {

        Hashtable<String, String> json = new Hashtable<String, String>();

        // Se distingue entre los par�metros que vayamos a meter ? para diferenciar las peticiones de liquidaci�n y contrataci�n; por tanto no meter par�metros vacios
        if (params.containsKey(ServerConstantsCredit.CVESUBP_PARAM) || params.containsKey(ServerConstantsCredit.CVEPROD_PARAM) || params.containsKey(ServerConstantsCredit.CVEPLAZO_PARAM)) {
            // Contrataci�n
            //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION)));
            json.put(ServerConstantsCredit.CLIENTE_PARAM, validaNull(params.get(ServerConstantsCredit.CLIENTE_PARAM)));
            json.put(ServerConstantsCredit.IUM_PARAM, validaNull(params.get(ServerConstantsCredit.IUM_PARAM)));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, validaNull(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));
            json.put(ServerConstantsCredit.CVEPROD_PARAM, validaNull(params.get(ServerConstantsCredit.CVEPROD_PARAM)));
            json.put(ServerConstantsCredit.CVESUBP_PARAM, validaNull(params.get(ServerConstantsCredit.CVESUBP_PARAM)));
            json.put(ServerConstantsCredit.CVEPLAZO_PARAM, validaNull(params.get(ServerConstantsCredit.CVEPLAZO_PARAM)));
            json.put(ServerConstantsCredit.MON_DESE_PARAM, validaNull(params.get(ServerConstantsCredit.MON_DESE_PARAM)));
            json.put(ServerConstantsCredit.TIPO_OP_PARAM, validaNull(params.get(ServerConstantsCredit.TIPO_OP_PARAM)));

        } else {
            // Liquidacion
            //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION)));
            json.put(ServerConstantsCredit.CLIENTE_PARAM, validaNull(params.get(ServerConstantsCredit.CLIENTE_PARAM)));
            json.put(ServerConstantsCredit.IUM_PARAM, validaNull(params.get(ServerConstantsCredit.IUM_PARAM)));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, validaNull(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));
            json.put(ServerConstantsCredit.CONTRATO_PARAM, validaNull(params.get(ServerConstantsCredit.CONTRATO_PARAM)));
            json.put(ServerConstantsCredit.PAGO_MENSUAL_PARAM, validaNull(params.get(ServerConstantsCredit.PAGO_MENSUAL_PARAM)));
            json.put(ServerConstantsCredit.TIPO_OP_PARAM, validaNull(params.get(ServerConstantsCredit.TIPO_OP_PARAM)));

        }

        return json;
    }

    public static Hashtable<String, String> envioCorreoNuevaImagen(Hashtable<String, ?> params, String cveProd) {
        Hashtable<String, String> json = new Hashtable<String, String>();

        json.put(ServerConstantsCredit.CEL_PARAM, validaNull(params.get(ServerConstantsCredit.CEL_PARAM)));
        json.put(ServerConstantsCredit.CVE_PROD, validaNull(params.get(ServerConstantsCredit.CVE_PROD)));
        json.put(ServerConstantsCredit.CAT_PARAM, validaNull(params.get(ServerConstantsCredit.CAT_PARAM)));
        json.put(ServerConstantsCredit.NOMBRE_PARAM, validaNull(params.get(ServerConstantsCredit.NOMBRE_PARAM)));
        json.put(ServerConstantsCredit.OPERACION_PARAM, validaNull(params.get(ServerConstantsCredit.OPERACION_PARAM)));

        if(cveProd.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
//simulado y actual
            json.put(ServerConstantsCredit.IUM_PARAM, validaNull(params.get(ServerConstantsCredit.IUM_PARAM)));
            json.put(ServerConstantsCredit.CAT_FECHA_PARAM, validaNull(params.get(ServerConstantsCredit.CAT_FECHA_PARAM)));
            json.put(ServerConstantsCredit.EMAIL_PARAM, validaNull(params.get(ServerConstantsCredit.EMAIL_PARAM)));
            json.put(ServerConstantsCredit.LIMITE_NUEVO_PARAM, validaNull(params.get(ServerConstantsCredit.LIMITE_NUEVO_PARAM)));
            json.put(ServerConstantsCredit.LIMITE_SIMULADO_PARAM, validaNull(params.get(ServerConstantsCredit.LIMITE_SIMULADO_PARAM)));
            json.put(ServerConstantsCredit.LIMITE_ACTUAL_PARAM, validaNull(params.get(ServerConstantsCredit.LIMITE_ACTUAL_PARAM)));
            json.put(ServerConstantsCredit.TASA_PARAM, validaNull(params.get(ServerConstantsCredit.TASA_PARAM)));
            json.put(ServerConstantsCredit.DES_SUBP_PARAM, validaNull(params.get(ServerConstantsCredit.DES_SUBP_PARAM)));
            json.put(ServerConstantsCredit.TDC_PARAM, validaNull(params.get(ServerConstantsCredit.TDC_PARAM)));

        }else if(cveProd.equals("TDC")){

            json.put(ServerConstantsCredit.NOMBRE_PARAM, validaNull(params.get(ServerConstantsCredit.NOMBRE_PARAM)));
            json.put(ServerConstantsCredit.TDC_PARAM, validaNull(params.get(ServerConstantsCredit.TDC_PARAM)));
            json.put(ServerConstantsCredit.IUM_PARAM, validaNull(params.get(ServerConstantsCredit.IUM_PARAM)));
            json.put(ServerConstantsCredit.CAT_FECHA_PARAM, validaNull(params.get(ServerConstantsCredit.CAT_FECHA_PARAM)));
            json.put(ServerConstantsCredit.EMAIL_PARAM, validaNull(params.get(ServerConstantsCredit.EMAIL_PARAM)));
            json.put(ServerConstantsCredit.LIMITE_NUEVO_PARAM, validaNull(params.get(ServerConstantsCredit.LIMITE_NUEVO_PARAM)));
            json.put(ServerConstantsCredit.TASA_PARAM, validaNull(params.get(ServerConstantsCredit.TASA_PARAM)));
            json.put(ServerConstantsCredit.DES_SUBP_PARAM, validaNull(params.get(ServerConstantsCredit.DES_SUBP_PARAM)));

        }else if(cveProd.equals(ConstantsCredit.CREDITO_AUTO) || cveProd.equals("HIPO")){

            json.put(ServerConstantsCredit.IUM2_PARAM, validaNull(params.get(ServerConstantsCredit.IUM2_PARAM)));
            json.put(ServerConstantsCredit.CORREO_PARAM, validaNull(params.get(ServerConstantsCredit.CORREO_PARAM)));
            json.put(ServerConstantsCredit.FECHA_CAT_PARAM, validaNull(params.get(ServerConstantsCredit.FECHA_CAT_PARAM)));
            json.put(ServerConstantsCredit.MONTO_CALCULADO_PARAM, validaNull(params.get(ServerConstantsCredit.MONTO_CALCULADO_PARAM)));
            json.put(ServerConstantsCredit.PLAZO_PARAM, validaNull(params.get(ServerConstantsCredit.PLAZO_PARAM)));
            json.put(ServerConstantsCredit.PAGO_MENSUAL2_PARAM, validaNull(params.get(ServerConstantsCredit.PAGO_MENSUAL2_PARAM)));
            json.put(ServerConstantsCredit.DESC_SUBP_PARAM, validaNull(params.get(ServerConstantsCredit.DESC_SUBP_PARAM)));
            json.put(ServerConstantsCredit.TASA_PARAM, validaNull(params.get(ServerConstantsCredit.TASA_PARAM)));


        }else { //consumo

            json.put(ServerConstantsCredit.PAGO_MENSUAL2_PARAM, validaNull(params.get(ServerConstantsCredit.PAGO_MENSUAL2_PARAM)));
            json.put(ServerConstantsCredit.IUM2_PARAM, validaNull(params.get(ServerConstantsCredit.IUM2_PARAM)));
            json.put(ServerConstantsCredit.CORREO_PARAM, validaNull(params.get(ServerConstantsCredit.CORREO_PARAM)));
            json.put(ServerConstantsCredit.FECHA_CAT_PARAM, validaNull(params.get(ServerConstantsCredit.FECHA_CAT_PARAM)));
            json.put(ServerConstantsCredit.IMPORTE_PARAM, validaNull(params.get(ServerConstantsCredit.IMPORTE_PARAM)));
            json.put(ServerConstantsCredit.TIPO_SEGURO_PARAM, validaNull(params.get(ServerConstantsCredit.TIPO_SEGURO_PARAM)));
            json.put(ServerConstantsCredit.PAGO_SEGURO_PARAM, validaNull(params.get(ServerConstantsCredit.PAGO_SEGURO_PARAM)));

            json.put(ServerConstantsCredit.IND_REVIRE_PARAM, validaNull(params.get(ServerConstantsCredit.IND_REVIRE_PARAM)));
            json.put(ServerConstantsCredit.PLAZO_PARAM, validaNull(params.get(ServerConstantsCredit.PLAZO_PARAM)));

            if(params.get(ServerConstantsCredit.IND_REVIRE_PARAM).toString().equalsIgnoreCase("S")) {
                if(params.get(ServerConstantsCredit.TASA_REVIRE_PARAM)!=null){
                    json.put(ServerConstantsCredit.TASA_REVIRE_PARAM, validaNull(params.get(ServerConstantsCredit.TASA_REVIRE_PARAM)));
                }
            }

            if(cveProd.equals(ConstantsCredit.ADELANTO_NOMINA)){
                json.put(ServerConstantsCredit.COMISION_PARAM, validaNull(params.get(ServerConstantsCredit.COMISION_PARAM)));
                json.put(ServerConstantsCredit.TASA_ANUAL_PARAM, validaNull(params.get(ServerConstantsCredit.TASA_ANUAL_PARAM)));
            }else
                json.put(ServerConstantsCredit.TASA_MENSUAL_PARAM, validaNull(params.get(ServerConstantsCredit.TASA_MENSUAL_PARAM)));
        }

        return json;

    }

    public static Hashtable<String, String> envioCorreo(Hashtable<String, ?> params) {

        Hashtable<String, String> json = new Hashtable<String, String>();

        //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.ENVIO_CORREO_OPERACION)));
        json.put(ServerConstantsCredit.CLIENTE_PARAM, validaNull(params.get(ServerConstantsCredit.CLIENTE_PARAM)));
        json.put(ServerConstantsCredit.IUM_PARAM, validaNull(params.get(ServerConstantsCredit.IUM_PARAM)));
        json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, validaNull(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));

        String indicador = validaNull(params.get(ServerConstantsCredit.IND_CORREO));

        if (indicador.equals("C")) {
            json.put(ServerConstantsCredit.SCN, validaNull(params.get(ServerConstantsCredit.SCN)));
            json.put(ServerConstantsCredit.CN_IMPORTE, validaNull(params.get(ServerConstantsCredit.CN_IMPORTE)));
            json.put(ServerConstantsCredit.CN_TASA_ANUAL, validaNull(params.get(ServerConstantsCredit.CN_TASA_ANUAL)));
            json.put(ServerConstantsCredit.CN_PLAZO, validaNull(params.get(ServerConstantsCredit.CN_PLAZO)));
            json.put(ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL, validaNull(params.get(ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL)));

            json.put(ServerConstantsCredit.SPPI, validaNull(params.get(ServerConstantsCredit.SPPI)));
            json.put(ServerConstantsCredit.PP_IMPORTE, validaNull(params.get(ServerConstantsCredit.PP_IMPORTE)));
            json.put(ServerConstantsCredit.PP_TASA_ANUAL, validaNull(params.get(ServerConstantsCredit.PP_TASA_ANUAL)));
            json.put(ServerConstantsCredit.PP_PLAZO, validaNull(params.get(ServerConstantsCredit.PP_PLAZO)));
            json.put(ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL, validaNull(params.get(ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL)));

            json.put(ServerConstantsCredit.STDC, validaNull(params.get(ServerConstantsCredit.STDC)));
            json.put(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO, validaNull(params.get(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO)));
            json.put(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO, validaNull(params.get(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO)));

            json.put(ServerConstantsCredit.SILDC, validaNull(params.get(ServerConstantsCredit.SILDC)));
            json.put(ServerConstantsCredit.IL_TARJETA_DE_CREDITO, validaNull(params.get(ServerConstantsCredit.IL_TARJETA_DE_CREDITO)));
            json.put(ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA, validaNull(params.get(ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA)));
            json.put(ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA, validaNull(params.get(ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA)));

            json.put(ServerConstantsCredit.SCA, validaNull(params.get(ServerConstantsCredit.SCA)));
            json.put(ServerConstantsCredit.CA_IMPORTE, validaNull(params.get(ServerConstantsCredit.CA_IMPORTE)));
            json.put(ServerConstantsCredit.CA_TASA_ANUAL, validaNull(params.get(ServerConstantsCredit.CA_TASA_ANUAL)));
            json.put(ServerConstantsCredit.CA_PLAZO, validaNull(params.get(ServerConstantsCredit.CA_PLAZO)));
            json.put(ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL, validaNull(params.get(ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL)));

            json.put(ServerConstantsCredit.SCH, validaNull(params.get(ServerConstantsCredit.SCH)));
            json.put(ServerConstantsCredit.CH_IMPORTE, validaNull(params.get(ServerConstantsCredit.CH_IMPORTE)));
            json.put(ServerConstantsCredit.CH_TASA_ANUAL, validaNull(params.get(ServerConstantsCredit.CH_TASA_ANUAL)));
            json.put(ServerConstantsCredit.CH_PLAZO, validaNull(params.get(ServerConstantsCredit.CH_PLAZO)));
            json.put(ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL, validaNull(params.get(ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL)));

            json.put(ServerConstantsCredit.CON_CAT, validaNull(params.get(ServerConstantsCredit.CON_CAT)));
            json.put(ServerConstantsCredit.CON_FECHA_CALCULO, validaNull(params.get(ServerConstantsCredit.CON_FECHA_CALCULO)));

        } else if (indicador.equals("L")) {

            //liquidacion
            json.put(ServerConstantsCredit.SCNL, validaNull(params.get(ServerConstantsCredit.SCNL)));
            json.put(ServerConstantsCredit.CN_DESC, validaNull(params.get(ServerConstantsCredit.CN_DESC)));
            json.put(ServerConstantsCredit.CN_CANT, validaNull(params.get(ServerConstantsCredit.CN_CANT)));

            json.put(ServerConstantsCredit.SPPIL, validaNull(params.get(ServerConstantsCredit.SPPIL)));
            json.put(ServerConstantsCredit.PP_DESC, validaNull(params.get(ServerConstantsCredit.PP_DESC)));
            json.put(ServerConstantsCredit.PP_CANT, validaNull(params.get(ServerConstantsCredit.PP_CANT)));

            json.put(ServerConstantsCredit.SCHL, validaNull(params.get(ServerConstantsCredit.SCHL)));
            json.put(ServerConstantsCredit.CH_DESC, validaNull(params.get(ServerConstantsCredit.CH_DESC)));
            json.put(ServerConstantsCredit.CH_CANT, validaNull(params.get(ServerConstantsCredit.CH_CANT)));

            json.put(ServerConstantsCredit.SCAL, validaNull(params.get(ServerConstantsCredit.SCAL)));
            json.put(ServerConstantsCredit.CA_DESC, validaNull(params.get(ServerConstantsCredit.CA_DESC)));
            json.put(ServerConstantsCredit.CA_CANT, validaNull(params.get(ServerConstantsCredit.CA_CANT)));

            json.put(ServerConstantsCredit.SP5, validaNull(params.get(ServerConstantsCredit.SP5)));
            json.put(ServerConstantsCredit.P5_DESC, validaNull(params.get(ServerConstantsCredit.P5_DESC)));
            json.put(ServerConstantsCredit.P5_CANT, validaNull(params.get(ServerConstantsCredit.P5_CANT)));

            json.put(ServerConstantsCredit.SP6, validaNull(params.get(ServerConstantsCredit.SP6)));
            json.put(ServerConstantsCredit.P6_DESC, validaNull(params.get(ServerConstantsCredit.P6_DESC)));
            json.put(ServerConstantsCredit.P6_CANT, validaNull(params.get(ServerConstantsCredit.P6_CANT)));


            //alternativas
            json.put(ServerConstantsCredit.SCR, validaNull(params.get(ServerConstantsCredit.SCR)));

            json.put(ServerConstantsCredit.SAN, validaNull(params.get(ServerConstantsCredit.SAN)));
            json.put(ServerConstantsCredit.CN, validaNull(params.get(ServerConstantsCredit.CN)));

            json.put(ServerConstantsCredit.SAP, validaNull(params.get(ServerConstantsCredit.SAP)));
            json.put(ServerConstantsCredit.PP, validaNull(params.get(ServerConstantsCredit.PP)));

            json.put(ServerConstantsCredit.SAI, validaNull(params.get(ServerConstantsCredit.SAI)));
            json.put(ServerConstantsCredit.IL, validaNull(params.get(ServerConstantsCredit.IL)));

            json.put(ServerConstantsCredit.SAH, validaNull(params.get(ServerConstantsCredit.SAH)));
            json.put(ServerConstantsCredit.CH, validaNull(params.get(ServerConstantsCredit.CH)));

            json.put(ServerConstantsCredit.SAA, validaNull(params.get(ServerConstantsCredit.SAA)));
            json.put(ServerConstantsCredit.CA, validaNull(params.get(ServerConstantsCredit.CA)));

            json.put(ServerConstantsCredit.SAT, validaNull(params.get(ServerConstantsCredit.SAT)));
            json.put(ServerConstantsCredit.TC, validaNull(params.get(ServerConstantsCredit.TC)));

            //YA NO SON NECESARIOS
            //json.put(ServerConstantsCredit.LIQ_CAT, validaNull(params.get(ServerConstantsCredit.LIQ_CAT)));
            //json.put(ServerConstantsCredit.LIQ_FECHA_CALCULO, validaNull(params.get(ServerConstantsCredit.LIQ_FECHA_CALCULO)));
        }

        json.put(ServerConstantsCredit.IND_CORREO, indicador);
        json.put(ServerConstantsCredit.email, validaNull(params.get(ServerConstantsCredit.email)));


        return json;
    }

    public static Hashtable<String, String> consultaDetalleAlternativa(Hashtable<String, ?> params) {

        Hashtable<String, String> json = new Hashtable<String, String>();

        //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.DETALLE_ALTERNATIVA)));
        json.put(ServerConstantsCredit.CLIENTE_PARAM, validaNull(params.get(ServerConstantsCredit.CLIENTE_PARAM)));

        json.put(ServerConstantsCredit.IUM_PARAM, validaNull(params.get(ServerConstantsCredit.IUM_PARAM)));
        json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, validaNull(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));
        json.put(ServerConstantsCredit.PRODUCTO_PARAM, validaNull(params.get(ServerConstantsCredit.PRODUCTO_PARAM)));

        return json;
    }


    public static Hashtable<String, String> consultaDetalleTDC(Hashtable<String, ?> params) {
        Hashtable<String, String> json = new Hashtable<String, String>();

        json.put(ConstantsVentaTDC.NUMERO_CELULAR_TAG, validaNull(params.get(ConstantsVentaTDC.NUMERO_CELULAR_TAG)));
        json.put(ConstantsVentaTDC.CVE_CAMP_TAG, validaNull(params.get(ConstantsVentaTDC.CVE_CAMP_TAG)));
        json.put(ConstantsVentaTDC.IUM_TAG, validaNull(params.get(ConstantsVentaTDC.IUM_TAG)));
        json.put(ConstantsVentaTDC.OFICINA_TAG, validaNull(params.get(ConstantsVentaTDC.OFICINA_TAG)));

        return json;
    }


    public static Hashtable<String, String> consultaAlternativas(Hashtable<String, ?> params) {
        //ConsultaAlternativasData data = new ConsultaAlternativasData();
        Hashtable<String, String> json = new Hashtable<String, String>();

        //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CONSULTA_ALTERNATIVAS)));
        json.put(ServerConstantsCredit.CLIENTE_PARAM, validaNull(params.get(ServerConstantsCredit.CLIENTE_PARAM)));

        json.put(ServerConstantsCredit.IUM_PARAM, validaNull(params.get(ServerConstantsCredit.IUM_PARAM)));
        json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, validaNull(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));

        return json;
    }

    private static String validaNull(Object valor){
        if(null==valor){
            return "";
        }else{
            return String.valueOf(valor);
        }
    }



}