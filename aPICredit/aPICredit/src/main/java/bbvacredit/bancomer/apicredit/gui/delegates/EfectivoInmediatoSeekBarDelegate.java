package bbvacredit.bancomer.apicredit.gui.delegates;

import android.util.Log;

import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.EfectivoInmediatoSeekBarViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.OfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by FHERNANDEZ on 28/12/16.
 */
public class EfectivoInmediatoSeekBarDelegate extends BaseDelegateOperacion {

    private EfectivoInmediatoSeekBarViewController controller;
//    private Producto producto;
    private Promociones promocion;
    private ObjetoCreditos data;
    private ObjetoCreditos auxData;
    private Session session;
    private boolean isDeleteOperation = false; //si es true, borrar simulación
    private boolean productoConPlazo;

    private BaseViewController viewController;

    private OfertaEFI ofertaEFI;

    public EfectivoInmediatoSeekBarDelegate(EfectivoInmediatoSeekBarViewController controller){
        this.controller = controller;
        session = Tools.getCurrentSession();
    }


    public void analyzeResponse(String operationId, ServerResponse response) {

        if(Server.CONSULTA_DETALLE_EFI.equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                Log.i("efi","consulta exitosa de efi: "+response.getResponse());

                ofertaEFI = (OfertaEFI)response.getResponse();
                setOfertaEFI(ofertaEFI);

                Log.i("efi","ofertaefiCat: "+ofertaEFI.getCat());
                Log.i("efi","ofertaefiComision: "+ofertaEFI.getComisionEFIMonto());
                Log.i("efi","ofertaefidispon: "+ofertaEFI.getDisponibleTDC());
                Log.i("efi","ofertaefiPlazoMeses: "+ofertaEFI.getPlazoMeses());
                Log.i("efi","ofertaefiImporte: "+ofertaEFI.getImporte());
                Log.i("efi","ofertaefiMMax: "+ofertaEFI.getMontoMaximo());
                Log.i("efi","ofertaefiPMSim: "+ofertaEFI.getPagoMensualSimulador());

                if (!controller.isFromBar)
                    controller.validateLayoutToShow();
            }

        }else if(getCodigoOperacion().equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                MainController.getInstance().ocultaIndicadorActividad();
//                session.setAuxCreditos(data);
                session.setCreditos(data);

//                saveOrDeleteSimulationRequest(true);

            }
        }else if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                if(isDeleteOperation){
                    isDeleteOperation = false;

                    CalculoData respuesta = (CalculoData) response.getResponse();

                    data = null;
                    data = new ObjetoCreditos();

                    data.setCreditos(respuesta.getCreditos());
                    data.setEstado(respuesta.getEstado());
                    data.setMontotSol(respuesta.getMontotSol());
                    data.setPagMTot(respuesta.getPagMTot());
                    data.setPorcTotal(respuesta.getPorcTotal());
                    data.setProductos(respuesta.getProductos());
                    data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                    MainController.getInstance().ocultaIndicadorActividad();
//                    session.setAuxCreditos(data);  //se guarda por separado
                    session.setCreditos(data);

//                    doRecalculo();

                }else{
//                    Log.i("detalle", "showDetalle, getProd: " + getProducto().getCveProd());
//                    Tools.orderProducts(true); //true to save array from recalculo
//                    if (!controller.isFromBar)
//                        controller.validateLayoutToShow();
                }
            }
        }
    }

    public void contratarEFI(){
//        DetalleOfertaEFIDelegate delegateEFI = new DetalleOfertaEFIDelegate();
//        delegateEFI.setOfertaEFI(ofertaEFI);
//        ((BmovilViewsController)viewController.getParentViewsController()).showDetalleEFI(ofertaEFI, promocion);
//        (())
//        ((suitebancomer.aplicaciones.bmovil.classes.gui.controllers.token.BmovilViewsController)viewController.getParentViewsController()).showD();
//        l
//        MainController.getInstance().getActivityController().getCurrentActivity().

        Session.getInstance().setOfertaEFI(ofertaEFI);
        Session.getInstance().setPromociones(controller.getPromocion());
//        MainController.getInstance().showScreen(DetalleEFIViewControllerCredit.class);

    }

    public void realizaOperacionEFI() {

        Log.i("efi","realiza operacion efi");

        Session session = Session.getInstance();
        String user = session.getNumCelular();
        String ium= session.getIum();
        Hashtable<String, String> params = new Hashtable<String, String>();
        // params.put("operacion", "cDetalleOfertaBMovil");
        params.put("numeroCelular", user);
        params.put("cveCamp", promocion.getCveCamp());
        params.put("IUM", ium);

        Hashtable<String, String> paramTable2 = AuxConectionFactoryCredit.realizaOPEfi(params);

//        doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA_EFI, params, true, new OfertaEFI(), Server.isJsonValueCode.ONECLICK, MainController.getInstance().getContext());
        this.doNetworkOperation(Server.CONSULTA_DETALLE_EFI, paramTable2,true, new OfertaEFI(),MainController.getInstance().getContext());

    }


    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    public OfertaEFI getOfertaEFI() {
        return ofertaEFI;
    }

    public void setOfertaEFI(OfertaEFI ofertaEFI) {
        this.ofertaEFI = ofertaEFI;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public void setPromocion(Promociones promocion) {
        this.promocion = promocion;
    }

    @Override
    protected String getCodigoOperacion() {
        return Server.CALCULO_OPERACION;
    }
}
