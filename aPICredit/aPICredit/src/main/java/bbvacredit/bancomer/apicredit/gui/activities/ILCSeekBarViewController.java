package bbvacredit.bancomer.apicredit.gui.activities;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bancomer.apiilc.models.OfertaILC;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.LabelMontoTotalCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.AdelantoNominaSeekBarDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.DetalleDeAlternativaDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.ILCSeekBarDelegate;
import bbvacredit.bancomer.apicredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by FHERNANDEZ on 30/12/16.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ILCSeekBarViewController extends Fragment implements View.OnClickListener{

    private Producto producto;

    private ImageView imgvProd;
    private ImageView imgvShowDetalle;
    private ImageView imgvAyuda;

    private ImageButton imgvContratarCredito;

    public LabelMontoTotalCredit edtMontoVisible;

    private TextView txtvDescProd;
    private TextView txtvMontoMinimo;
    private TextView txtvMontoMaximo;
    private TextView txtvCat;
    private TextView txtvTasa;
    private TextView txtvTextSaldoTotal;
    private TextView txtvSaldoTotalPagar;
    private TextView txtvOpcion;
    private TextView txtvPlazo;
    private TextView txtvTipo;
    private TextView txtvMasInfo;
    private TextView txtvPagoMMax;
    private TextView txtvMontoMMax;
    private TextView txtvdetalleTDC_ILC;
    private TextView txtvResultadoDetalleTDCILC;

    private LinearLayout lnlDetalleProd;
    private LinearLayout lnlTipo;
    private LinearLayout lnlPlazo;
    private LinearLayout lnlPlazoTipo;
    private LinearLayout lnlPagoMMaximo;

    private SeekBar seekBarSimulador;

    private String montoMax = "";
    private String montMin = "";
    private StringBuffer typedString= new StringBuffer();
    private String amountString=null;
    private boolean indSimProd;
    private String auxFinalMax;

    private boolean isSettingText = false;
    private boolean isResetting = false;
    private boolean mAcceptCents=false;
    private boolean flag=true;
    public boolean isShowDetalle;
    public boolean isSimulated = false;
    public boolean isFromBar;
    public boolean isILCCamp;
    public boolean initalCampain = false;
    public boolean isBackProd = true;

    public OfertaILC ofertaILC;
    public bbvacredit.bancomer.apicredit.models.OfertaILC ofertaILCC;

    //variable que indica si tenía oferta y esta fue modificada por el simulador
    public boolean auxIsCam = false;

    private Promociones promocionILC;

    private ILCSeekBarDelegate delegate;
    private DetalleDeAlternativaDelegate delegateOp;

    private ILCSeekBarViewController controller;

    private final int INCREMENTAL = 1000;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_credit_item_listview_creditos,container,false);

        if(rootView != null){
            delegate = new ILCSeekBarDelegate(this);
            controller = this;
            initView(rootView);
            delegate.setProducto(getProducto());
            setIsShowDetalle(false);
        }

        return rootView;
    }

    public void initView(View rootView){
        isILCCamp = ((MenuPrincipalActivitySeekBar)getActivity()).isILCCamp;
        if (delegate == null)
            delegate = new ILCSeekBarDelegate(this);
        if(isILCCamp)
            delegate.setPromocion(getPromocionILC());
        findViews(rootView);
        validateSO();
        setValuesProduct();
        setValuesSeekBar();
        setVisibleMont(rootView);
        validateCampainOfert();
    }

    public void validateCampainOfert(){
        //si solo trae campañas de oneclick
        if (isILCCamp && getProducto()==null){
            edtMontoVisible.setEnabled(false);
            seekBarSimulador.setEnabled(false);
            txtvMontoMinimo.setVisibility(View.GONE);
        }
    }

    public void dissableProd(){
        edtMontoVisible.setEnabled(false);
        seekBarSimulador.setEnabled(false);
        imgvShowDetalle.setEnabled(false);
        edtMontoVisible.setText("$0");
        txtvMontoMinimo.setVisibility(View.GONE);
        txtvMontoMaximo.setText("$0");
    }

    public void enableProd(){
        edtMontoVisible.setEnabled(true);
        seekBarSimulador.setEnabled(true);
        imgvShowDetalle.setEnabled(true);
        txtvMontoMinimo.setVisibility(View.VISIBLE);
    }

    private void setVisibleMont(final View view){

        edtMontoVisible.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.i("edt", "limpia monto, " + event);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Log.i("edt", "action up, limpia texto");

                    ((MenuPrincipalActivitySeekBar) getActivity()).validateEdtMontoVisible();
                    ((MenuPrincipalActivitySeekBar) getActivity()).isILCEditable = true;
                    ((MenuPrincipalActivitySeekBar) getActivity()).isILCEditableS = edtMontoVisible.getText().toString();

                    reset();
                    mAcceptCents = true;
                    isSettingText = true;
                    edtMontoVisible.setText("");
                    return false;
                } else {
                    Log.w("edt", "otro evento, no limpia");
                }

                return false;
            }
        });

        edtMontoVisible.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                String amountField = edtMontoVisible.getText().toString();
                if (!isSettingText) {
                    amountString = amountField;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String amountField = edtMontoVisible.getText().toString();
                isBackProd = false;

                if (s.toString().equals(".") || s.toString().equals("0")) {
                    edtMontoVisible.setText("");
                } else {

                    if (!isSettingText && !isResetting) {
                        if (!flag) {
                            String aux;
                            try {
                                aux = edtMontoVisible.getText().toString();//.substring(0, edtMontoVisible.length() - 3);
                                typedString = new StringBuffer();

                                if (aux.charAt(0) == '0')
                                    aux = aux.substring(1, aux.length());
                            } catch (Exception ex) {
                                aux = "";

                            }
                            for (int i = 0; i < aux.length(); i++) {
                                if (aux.charAt(i) != ',')
                                    typedString.append(aux.charAt(i));
                            }
                            flag = true;

                            setFormattedText();
                            amountField = edtMontoVisible.getText().toString();
                            amountString = amountField;
                        } else {
                            try {
                                if (edtMontoVisible.length() < amountString.length()) {
                                    reset();
                                } else if (edtMontoVisible.length() > amountString.length()) {

                                    int newCharIndex = edtMontoVisible.getSelectionEnd() - 1;
                                    //there was no selection in the field
                                    if (newCharIndex == -2) {
                                        newCharIndex = edtMontoVisible.length() - 1;
                                    }

                                    char num = amountField.charAt(newCharIndex);
                                    if (!(num == '0' && typedString.length() == 0)) {
                                        typedString.append(num);
                                    }
                                    setFormattedText();
                                }
                            } catch (StringIndexOutOfBoundsException ex) {
                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                isSettingText = false;
            }
        });

        edtMontoVisible.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager manager = (InputMethodManager) ((MenuPrincipalActivitySeekBar) getActivity()).getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (manager != null) {
                        manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                        String auxMon = edtMontoVisible.getText().toString();
                        String respuesta = "";

                        if (edtMontoVisible.getText().toString().equals("")) {
                            edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                            updateGraph(montMin);
                        } else {
                            respuesta = Tools.validateMont(edtMontoVisible.getMontoMax(),
                                    edtMontoVisible.getMontoMin(), auxMon, null);
                            try {
                                if (respuesta.equals(ConstantsCredit.MONTO_MAYOR)) {
                                    edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                                    updateGraph(montoMax);
                                } else if (respuesta.equals(ConstantsCredit.MONTO_MENOR)) {
                                    edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montMin)));
                                    updateGraph(montMin);
                                } else if (respuesta.equals(ConstantsCredit.MONTO_EN_RANGO)) {
                                    String auxLbl = edtMontoVisible.getText().toString();
                                    if (!auxLbl.startsWith("$"))
                                        edtMontoVisible.setText("$" + auxLbl);
                                    updateGraph(auxLbl);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                    edtMontoVisible.clearFocus();
                }


                return false;
            }
        });

        edtMontoVisible.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    String amount = edtMontoVisible.getText().toString();

                    if (!amount.equals("") || amount != null) {
                        if (amount.contains(","))
                            amount = amount.replace(",", "");

                        if (amount.contains("$"))
                            amount = amount.replace("$", "");

                        amount = amount.substring(0, amount.length() - 1);

                        edtMontoVisible.setText(Tools.formatUserAmount(amount));

                    }
                }

                return false;
            }
        });
    }

    public void updateGraph(String amount){
        isFromBar = true;

        if (isILCCamp) {
            if (!auxIsCam) {
                Log.i("ilc","updateGraph cambia bandera a true");
                auxIsCam = true;
            }
        }


        double newAmount = 0.0, finalmmax = 0.0;
        int finalP = 0, auxMonMin = 0, auxAmount2 = 0;
        String auxAmount = Tools.clearAmounr(amount);

        if(auxAmount.equals(getMontMin())){
            seekBarSimulador.setProgress(0);

        }else if(auxAmount.equals(getMontoMax())){
            finalmmax = Double.parseDouble(getAuxFinalMax());
            seekBarSimulador.setProgress((int) finalmmax);

        }else {

            auxMonMin = Integer.parseInt(getMontMin());
            auxAmount2 = Integer.parseInt(auxAmount);

            finalP = auxAmount2-auxMonMin;
            newAmount = finalP / INCREMENTAL;

            seekBarSimulador.setProgress((int)newAmount);
        }

        validateProducSimulated();
    }

    private void updateGraph2(String amount){

        double newAmount = 0.0, finalmmax = 0.0, auxAmount2 = 0.0,  auxMonMin = 0;
        double finalP = 0;

        String auxAmount = Tools.clearAmounr(amount);

        if(getProducto()!=null) {
            auxMonMin = Double.parseDouble(getMontMin());
            auxAmount2 = Double.parseDouble(auxAmount);

            finalP = auxAmount2 - auxMonMin;
            newAmount = finalP / INCREMENTAL;
        }else{
            auxAmount2 = Double.parseDouble(auxAmount);
            newAmount = auxAmount2 /INCREMENTAL;
        }

        seekBarSimulador.setProgress((int) newAmount);

    }

    private void setValuesSeekBar(){

        if(getProducto() != null) {
            final double mmax = Double.parseDouble(Tools.validateMont(getMontoMax(), false));
            final double mmin = Double.parseDouble(Tools.validateMont(getMontMin(), false));
            double auxMmax = mmax - mmin;

            auxMmax = auxMmax / INCREMENTAL;

            final double finalAuxMmax = auxMmax;

            setAuxFinalMax(String.valueOf(finalAuxMmax));

            seekBarSimulador.setMax((int) finalAuxMmax);
            seekBarSimulador.setProgress((int) finalAuxMmax);

            seekBarSimulador.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    String visibleMont;

                    final int progressChanged = progress;
                    final int progresFinal = calculatePogress(progressChanged, (int) mmin, (int) finalAuxMmax, (int) mmax);

                    visibleMont = Tools.formatUserAmount(Integer.toString(progresFinal));

                    if (isILCCamp) {
                        if (!initalCampain) {
                            initalCampain = true;
                            String gtMonto = getPromocionILC().getMonto();

                            gtMonto = gtMonto.substring(0, gtMonto.length() - 2);
                            gtMonto = Tools.formatUserAmount(gtMonto);

                            Log.i("detalleilc", "gtMonto: " + gtMonto);
                            edtMontoVisible.setText("");
                            edtMontoVisible.setText(gtMonto);

                        } else {
                            Log.i("detalleilc", "else1 visibleMont: " + visibleMont);
                            edtMontoVisible.setText(visibleMont);
                        }
                    } else {
                        Log.i("detalleilc", "else2 visibleMont: " + visibleMont);
                        edtMontoVisible.setText(visibleMont);
                    }

                    edtMontoVisible.setCursorVisible(false);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
//                    isSimulated = true;
                    isFromBar = true;
                    if (isILCCamp) {
                        if (!auxIsCam) {
                            Log.i("ilc", "onProgressBar, cambia bandera a true");
                            auxIsCam = true;
                        }
                    }

                    validateProducSimulated();
                }
            });
        }

        String auxMonCamp, decimales;

        if(isILCCamp) {
            if(getProducto()==null) {
                //Si no tiene oferta por simulador, no es necesario calcular el progreso de la barra, dado que está llena
                //Si tuviera oneclick y simulador, si necesita calcularse el progreso
                seekBarSimulador.setMax(1);
                seekBarSimulador.setProgress(1);
            }else {

                auxMonCamp = getPromocionILC().getMonto();
                auxMonCamp = auxMonCamp.substring(0, auxMonCamp.length() - 2);
                decimales = auxMonCamp.substring(auxMonCamp.length() - 2, auxMonCamp.length());

                auxMonCamp = Tools.formatUserAmount(auxMonCamp) + "." + decimales;

                Log.i("ilc", "auxMonCamp: " + auxMonCamp);

                updateGraph2(auxMonCamp);
            }
        }

    }

    private void validateProducSimulated(){

         if (isIndSimProd()){ //si ya fue simulado, elimina y recalcula
            delegate.saveOrDeleteSimulationRequest(false); //true guarda, false elimina

        }else{
            Log.i("ilc","doRecalculo");
            delegate.doRecalculo();
        }
    }

    private void setValuesProduct(){

        String montoMaximo, auxMMax, montoMinimo, auxMMin,
                montoFlow, cveProd, indSim;

        int imgProducto,descProd;


        if (getProducto()!=null) {
            cveProd = getProducto().getCveProd();
            montoMaximo = getProducto().getMontoMaxS();
            montoMinimo = getProducto().getMontoMinS();
            indSim = getProducto().getIndSim();
            imgProducto = GuiTools.getCreditIcon(cveProd, false);
            descProd = GuiTools.getSelectedLabel(cveProd);
            montoFlow = getProducto().getMontoMaxS();   //valor que se tomará como monto máximo para saber si es mayor a 0.

            auxMMin = Tools.formatUserAmount(Tools.validateMont(montoMinimo, true));
            auxMMax = Tools.formatUserAmount(Tools.validateMont(montoMaximo,true));

            if(auxMMin.contains("$"))
                auxMMin = auxMMin.replace("$","");
            if(auxMMax.contains("$"))
                auxMMax = auxMMax.replace("$", "");

            setMontoMax(Tools.clearAmounr(auxMMax));
            setMontMin(Tools.clearAmounr(auxMMin));

            setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);

            Log.i("detalleilc", "getProducto != null: " + Tools.formatUserAmount(getMontoMax()));
            edtMontoVisible.setText(Tools.formatUserAmount(getMontoMax()));

            edtMontoVisible.setCveProd(producto.getCveProd());
            edtMontoVisible.setMontoMin(getMontMin());
            txtvMontoMinimo.setText(auxMMin);
        }else{
            cveProd = getPromocionILC().getCveCamp();
            imgProducto = GuiTools.getCreditIcon(cveProd, false);
            descProd = GuiTools.getSelectedLabel(cveProd);
            montoMaximo = getPromocionILC().getMonto();
            montoMaximo = montoMaximo.substring(0, montoMaximo.length() - 2);

            montoMaximo = Tools.formatUserAmount(montoMaximo);
            auxMMax = montoMaximo.replace("$","");

            setMontoMax(Tools.clearAmounr(auxMMax));

            edtMontoVisible.setText(montoMaximo);
        }


        imgvProd.setImageResource(imgProducto);
        txtvDescProd.setText(descProd);

        txtvMontoMaximo.setText(auxMMax);
        edtMontoVisible.setMontoMax(getMontoMax());
    }

    private void validateSO(){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH){
            seekBarSimulador.setThumb(getResources().getDrawable(R.drawable.seek_bar_thumb));
            seekBarSimulador.setProgressDrawable(getResources().getDrawable(R.drawable.custom_seek_bar));
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setThumbOffset(0);
        }
    }

    private void findViews(View vista){
        imgvProd        = (ImageView)vista.findViewById(R.id.imgvProd);
        imgvShowDetalle = (ImageView)vista.findViewById(R.id.imgvShowDetalle);
        imgvShowDetalle.setOnClickListener(this);

        imgvAyuda       = (ImageView)vista.findViewById(R.id.imgvAyuda);
        imgvAyuda.setOnClickListener(this);

        imgvContratarCredito = (ImageButton)vista.findViewById(R.id.imgvContratarCredito);
        imgvContratarCredito.setOnClickListener(this);

        edtMontoVisible    = (LabelMontoTotalCredit)vista.findViewById(R.id.edtMontoVisible);

        txtvDescProd        = (TextView)vista.findViewById(R.id.txtvDescProd);
        txtvMontoMinimo     = (TextView)vista.findViewById(R.id.txtvMontoMinimo);
        txtvMontoMaximo     = (TextView)vista.findViewById(R.id.txtvMontoMaximo);
        txtvCat             = (TextView)vista.findViewById(R.id.txtvCat);
        txtvOpcion          = (TextView)vista.findViewById(R.id.txtvTipo);
        txtvPlazo           = (TextView)vista.findViewById(R.id.txtPlazo);
        txtvTasa            = (TextView)vista.findViewById(R.id.txtvTasa);
        txtvTipo            = (TextView)vista.findViewById(R.id.txtvTipo);
        txtvMasInfo         = (TextView)vista.findViewById(R.id.txtvMasInfo);
        txtvPagoMMax        = (TextView)vista.findViewById(R.id.txtvPagoMMax);
        txtvMontoMMax       = (TextView)vista.findViewById(R.id.txtvMontoMMax);
        txtvdetalleTDC_ILC  = (TextView)vista.findViewById(R.id.txtvdetalleTDC_ILC);
        txtvResultadoDetalleTDCILC  = (TextView)vista.findViewById(R.id.txtvResultadoDetalleTDCILC);

        lnlDetalleProd      = (LinearLayout)vista.findViewById(R.id.lnlDetalleProd);
        lnlTipo             = (LinearLayout)vista.findViewById(R.id.lnlTipo);
        lnlTipo.setOnClickListener(this);

        lnlPlazo            = (LinearLayout)vista.findViewById(R.id.lnlPlazo);
        lnlPlazo.setOnClickListener(this);

        lnlPlazoTipo        = (LinearLayout)vista.findViewById(R.id.lnlPlazoTipo);
        lnlPagoMMaximo      = (LinearLayout)vista.findViewById(R.id.lnlPagoMMaximo);

        seekBarSimulador    = (SeekBar)vista.findViewById(R.id.seekBarSimulador);

    }

    @Override
    public void onClick(View v) {
        if (v == imgvContratarCredito){
            confirmarIncremento(getActivity());
        }

        if (v == imgvAyuda) {
            PopUpAyudaViewController.mostrarPopUp((MenuPrincipalActivitySeekBar)getActivity(),getProducto().getCveProd());
        }

        if(v == imgvShowDetalle){

            if(isSimulated){ //si ya fue simulado, muestra detalle
                validateLayoutToShow();

            }else{
                isFromBar = false;

                //si es campaña y no ha sido modificado
                Log.i("ilc","isILC: "+isILCCamp);
                Log.i("ilc","initalCampain: "+auxIsCam);
                if(isILCCamp && !auxIsCam){
                    //lanza petición de ilc
                    Log.i("ilc","no modificado, lanza oneclick");
                    delegate.doOneClickILC();
                }else {
                    Log.i("ilc","modificado, lanza simulador");
                    validateProducSimulated();
                }
            }
        }
    }

    public void setSimulation(){
        ((MenuPrincipalActivitySeekBar) getActivity()).isILCS = true;
        ((MenuPrincipalActivitySeekBar) getActivity()).validateTxtvColor(controller);

        setColor(false);
    }

    public void setColor(boolean isNormal){
        if(isNormal)
            edtMontoVisible.setTextColor(getResources().getColor(R.color.nuevaimagen_tx5));
        else
            edtMontoVisible.setTextColor(getResources().getColor(R.color.naranja));
    }

    public void setDelegateOp(DetalleDeAlternativaDelegate delegateOp){
        this.delegateOp = delegateOp;
    }

    public void validateLayoutToShow(){

        //oculta detalle
        if(isShowDetalle()) {
            setIsShowDetalle(false);
            lnlDetalleProd.setVisibility(View.GONE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_masdetalle);

        }else{

            ((MenuPrincipalActivitySeekBar)getActivity()).isILCdetail = true;
            ((MenuPrincipalActivitySeekBar)getActivity()).validateIsShowAnyDetail(this);

            setIsShowDetalle(true);
            lnlDetalleProd.setVisibility(View.VISIBLE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_menosdetalle);
            imgvContratarCredito.setVisibility(View.VISIBLE);
            lnlPlazoTipo.setVisibility(View.GONE);
            lnlPagoMMaximo.setVisibility(View.GONE);
            txtvResultadoDetalleTDCILC.setVisibility(View.VISIBLE);
            txtvdetalleTDC_ILC.setVisibility(View.VISIBLE);
            txtvTasa.setVisibility(View.GONE);
            txtvCat.setVisibility(View.GONE);
            setDetailProduct();
        }

    }

    public void setDetailProduct(){
        try {

            String lineaFinal = "";

            if(!auxIsCam && isILCCamp) {
                Log.i("ilc","linea final oneclick: "+getOfertaILCC().getLineaFinal());
                Log.i("ilc","linea actual oneclick: "+getOfertaILCC().getLineaActual());

                lineaFinal = getOfertaILCC().getLineaFinal();
            }else{
                Log.i("ilc","linea final simulador: "+getOfertaILC().getLineaFinal());
                Log.i("ilc","linea actual simulador: "+getOfertaILC().getLineaActual());

                lineaFinal = getOfertaILC().getLineaFinal();

            }


            Log.i("ilc", "linea final ilc: " + lineaFinal);

            txtvdetalleTDC_ILC.setText(getResources().getString(R.string.seekbar_detalle_ilc));
            txtvResultadoDetalleTDCILC.setText(Tools.formatUserAmount(lineaFinal));

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void updateData(boolean isOperationDelete){
        Log.i("updatedata","se manda actualizar productos desde tdc");
        ((MenuPrincipalActivitySeekBar)getActivity()).updateAllProductData(this,isOperationDelete);
    }

    private int calculatePogress(int progress, int min, int progresFinal, int mmax){

        int auxMin = min;

        if (progress == 0){
            return min;
        }else {

            for (int i = 1; i<=progress; i++) {
                auxMin = auxMin + INCREMENTAL;
                if (progress == progresFinal){
                    auxMin = mmax;
                }
            }
        }

        return auxMin;
    }

    public void confirmarIncremento(Context context){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(context);
        dialogo1.setTitle("Aviso");
        dialogo1.setMessage("¿Al autorizar esta oferta incrementará tu línea de crédito.?");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {

                if(isILCCamp) {
                    if (!auxIsCam){
                        Log.i("ilc","ilc, es campaña, se manda por oneclick");
                        delegate.initAPIILC(getOfertaILC());
                    }else{
                        Log.i("ilc", "ilc, es campaña se lanza por simulador");
                        delegate.saveOrDeleteSimulationRequest(true);
                    }
                }else{ //si no era campaña, contrata por simulador
                    Log.i("ilc","ilc, no trae campaaña, se lanza por simulador");
                    delegate.saveOrDeleteSimulationRequest(true);
                }

            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {

            }
        });
        dialogo1.show();
    }

    public void contratarILC(){
        delegateOp.contratacionILC();
    }

    public OfertaILC getOfertaILC() {
        return ofertaILC;
    }

    public void setOfertaILC(OfertaILC ofertaILC) {
        this.ofertaILC = ofertaILC;
    }

    public bbvacredit.bancomer.apicredit.models.OfertaILC getOfertaILCC() {
        return ofertaILCC;
    }

    public void setOfertaILCC(bbvacredit.bancomer.apicredit.models.OfertaILC ofertaILCC) {
        this.ofertaILCC = ofertaILCC;
    }

    private void doILCOneClick(){
        delegate.oneClickILC();
    }

    public Promociones getPromocionILC() {
        return promocionILC;
    }

    public void setPromocionILC(Promociones promocionILC) {
        this.promocionILC = promocionILC;
    }

    public void reset() {
        this.isResetting = true;
        this.typedString=new StringBuffer();
        this.isResetting = false;
    }

    private void setFormattedText(){

        isSettingText = true;
        String text = typedString.toString();

        edtMontoVisible.setText(Tools.formatUserAmount(text));
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public String getMontoMax() {
        return montoMax;
    }

    public void setMontoMax(String montoMax) {
        this.montoMax = montoMax;
    }

    public String getMontMin() {
        return montMin;
    }

    public void setMontMin(String montMin) {
        this.montMin = montMin;
    }

    public boolean isIndSimProd() {
        return indSimProd;
    }

    public void setIndSimProd(boolean indSimProd) {
        this.indSimProd = indSimProd;
    }

    public boolean isShowDetalle() {
        return isShowDetalle;
    }

    public void setIsShowDetalle(boolean isShowDetalle) {
        this.isShowDetalle = isShowDetalle;
    }

    public String getAuxFinalMax() {
        return auxFinalMax;
    }

    public void setAuxFinalMax(String auxFinalMax) {
        this.auxFinalMax = auxFinalMax;
    }

}