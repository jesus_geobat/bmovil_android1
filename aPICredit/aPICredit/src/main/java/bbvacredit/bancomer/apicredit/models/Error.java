package bbvacredit.bancomer.apicredit.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.CreditJSONAble;

import bbvacredit.bancomer.apicredit.io.ParsingHandlerJSON;
import bbvacredit.bancomer.apicredit.common.Tools;

public class Error implements ParsingHandlerJSON, CreditJSONAble {

	private String estado;
	
	private String codigoMensaje;
	
	private String descripcionMensaje;

	public Error() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Error(String estado, String codigoMensaje, String descripcionMensaje) {
		super();
		this.estado = estado;
		this.codigoMensaje = codigoMensaje;
		this.descripcionMensaje = descripcionMensaje;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigoMensaje() {
		return codigoMensaje;
	}

	public void setCodigoMensaje(String codigoMensaje) {
		this.codigoMensaje = codigoMensaje;
	}

	public String getDescripcionMensaje() {
		return descripcionMensaje;
	}

	public void setDescripcionMensaje(String descripcionMensaje) {
		this.descripcionMensaje = descripcionMensaje;
	}

	@Override
	public void fromJSON(String jsonString) {
		// TODO Auto-generated method stub
		try{
			JSONObject obj = new JSONObject(jsonString);
		
			this.estado = Tools.getJSONChecked(obj,"estado", String.class); //obj.getString("estado");
			this.codigoMensaje = Tools.getJSONChecked(obj,"codigoMensaje", String.class);
			this.descripcionMensaje = Tools.getJSONChecked(obj,"descripcionMensaje", String.class);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e(this.getClass().toString(), "Error en fromJSON");
			e.printStackTrace();
		}
	}

	@Override
	public String toJSON() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void processJSON(String jsonString) throws JSONException {
		// TODO Auto-generated method stub
		
	}
	
}
