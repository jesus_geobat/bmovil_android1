package bbvacredit.bancomer.apicredit.gui.activities;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.LabelMontoTotalCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.gui.delegates.AdelantoNominaSeekBarDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.AutoSeekBarDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.HipotecarioSeekBarDelegate;
import bbvacredit.bancomer.apicredit.models.Plazo;
import bbvacredit.bancomer.apicredit.models.Producto;
import bbvacredit.bancomer.apicredit.models.Subproducto;

/**
 * Created by FHERNANDEZ on 30/12/16.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AutoSeekBarViewController extends Fragment implements View.OnClickListener{

    private Producto producto;
    private Producto productoAux;

    private ImageView imgvProd;
    private ImageView imgvShowDetalle;
    private ImageView imgvAyuda;
    private ImageView imgvTipo;

    private ImageButton imgvContratarCredito;

    public LabelMontoTotalCredit edtMontoVisible;

    private TextView txtvDescProd;
    private TextView txtvMontoMinimo;
    private TextView txtvMontoMaximo;
    private TextView txtvCat;
    private TextView txtvTasa;
    private TextView txtvTextSaldoTotal;
    private TextView txtvSaldoTotalPagar;
    private TextView txtvOpcion;
    private TextView txtvPlazo;
    private TextView txtvTipo;
    private TextView txtvMasInfo;
    private TextView txtvPagoMMax;
    private TextView txtvMontoMMax;

    private LinearLayout lnlDetalleProd;
    private LinearLayout lnlTipo;
    private LinearLayout lnlPlazo;
    private LinearLayout lnlPlazoTipo;
    private LinearLayout lnlPagoMMaximo;
    private LinearLayout lnlPagoMMaximo2;

    private SeekBar seekBarSimulador;

    private String montoMax = "";
    private String montMin = "";
    private StringBuffer typedString= new StringBuffer();
    private String amountString=null;
    private boolean indSimProd;
    private String auxFinalMax;
    private String plazo;
    private String tipo;

    public String cvePlazo;
    public String cveSubProd;

    private String termCreditList[];
    private String termCreditLisNew[];
    private String optionList[];
    private Subproducto auxiliarOptionList[];

    public DialogInterface oDialog;

    public boolean buscarPlazo = true;
    public boolean buscarOpcion = false;
    private boolean isSettingText = false;
    private boolean isResetting = false;
    private boolean mAcceptCents=false;
    private boolean flag=true;
    public boolean isShowDetalle;
    public boolean isSimulated = false;
    public boolean isFromBar;
    private boolean isListSet=false;
    public boolean isVisible = false;
    public boolean isBackProd = true;

    private AutoSeekBarDelegate delegate;
    private AutoSeekBarViewController controller;

    private final int INCREMENTAL = 1000;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_credit_item_listview_creditos,container,false);

        if(rootView != null){
            delegate = new AutoSeekBarDelegate(this);
            controller = this;
            initView(rootView);
//            delegate.setProducto(getProducto());
            Log.w("update", "auto,getmax: " + getProducto().getMontoMaxS());
            setIsShowDetalle(false);

            Producto auxP = Tools.getProductByCve("AUTO",Session.getInstance().getCreditos().getProductos());
            Log.i("update","getByCve: "+auxP.getMontoMaxS());
        }

        return rootView;
    }

    public void initView(View rootView){
        findViews(rootView);
        validateSO();
        setValuesProduct();
        setValuesSeekBar();
        setVisibleMont(rootView);
        loadLists();
    }

    public void dissableProd(){
        edtMontoVisible.setEnabled(false);
        seekBarSimulador.setEnabled(false);
        imgvShowDetalle.setEnabled(false);
        edtMontoVisible.setText("$0");
        txtvMontoMinimo.setVisibility(View.GONE);
        txtvMontoMaximo.setText("$0");
    }

    public void enableProd(){
        edtMontoVisible.setEnabled(true);
        seekBarSimulador.setEnabled(true);
        imgvShowDetalle.setEnabled(true);
        txtvMontoMinimo.setVisibility(View.VISIBLE);
    }

    private void setVisibleMont(final View view){

        edtMontoVisible.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.i("edt", "limpia monto, " + event);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Log.i("edt", "action up, limpia texto");

                    ((MenuPrincipalActivitySeekBar) getActivity()).validateEdtMontoVisible();
                    ((MenuPrincipalActivitySeekBar) getActivity()).isAUTOEditable = true;
                    ((MenuPrincipalActivitySeekBar) getActivity()).isAUTOEditableS = edtMontoVisible.getText().toString();

                    reset();
                    mAcceptCents = true;
                    isSettingText = true;
                    edtMontoVisible.setText("");
                    return false;
                } else {
                    Log.w("edt", "otro evento, no limpia");
                }

                return false;
            }
        });

        edtMontoVisible.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                String amountField = edtMontoVisible.getText().toString();
                if (!isSettingText) {
                    amountString = amountField;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String amountField = edtMontoVisible.getText().toString();
                isBackProd = false;

                if (s.toString().equals(".") || s.toString().equals("0")) {
                    edtMontoVisible.setText("");
                } else {

                    if (!isSettingText && !isResetting) {
                        if (!flag) {
                            String aux;
                            try {
                                aux = edtMontoVisible.getText().toString();//.substring(0, edtMontoVisible.length() - 3);
                                typedString = new StringBuffer();

                                if (aux.charAt(0) == '0')
                                    aux = aux.substring(1, aux.length());
                            } catch (Exception ex) {
                                aux = "";

                            }
                            for (int i = 0; i < aux.length(); i++) {
                                if (aux.charAt(i) != ',')
                                    typedString.append(aux.charAt(i));
                            }
                            flag = true;

                            setFormattedText();
                            amountField = edtMontoVisible.getText().toString();
                            amountString = amountField;
                        } else {
                            try {
                                if (edtMontoVisible.length() < amountString.length()) {
                                    reset();
                                } else if (edtMontoVisible.length() > amountString.length()) {

                                    int newCharIndex = edtMontoVisible.getSelectionEnd() - 1;
                                    //there was no selection in the field
                                    if (newCharIndex == -2) {
                                        newCharIndex = edtMontoVisible.length() - 1;
                                    }

                                    char num = amountField.charAt(newCharIndex);
                                    if (!(num == '0' && typedString.length() == 0)) {
                                        typedString.append(num);
                                    }
                                    setFormattedText();
                                }
                            } catch (StringIndexOutOfBoundsException ex) {
                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                isSettingText = false;
            }
        });

        edtMontoVisible.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager manager = (InputMethodManager) ((MenuPrincipalActivitySeekBar) getActivity()).getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (manager != null) {
                        manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                        String auxMon = edtMontoVisible.getText().toString();
                        String respuesta = "";

                        if (edtMontoVisible.getText().toString().equals("")) {
                            edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                            updateGraph(montMin);
                        } else {
                            respuesta = Tools.validateMont(edtMontoVisible.getMontoMax(),
                                    edtMontoVisible.getMontoMin(), auxMon, null);
                            try {
                                if (respuesta.equals(ConstantsCredit.MONTO_MAYOR)) {
                                    edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                                    updateGraph(montoMax);
                                } else if (respuesta.equals(ConstantsCredit.MONTO_MENOR)) {
                                    edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montMin)));
                                    updateGraph(montMin);
                                } else if (respuesta.equals(ConstantsCredit.MONTO_EN_RANGO)) {
                                    String auxLbl = edtMontoVisible.getText().toString();
                                    if (!auxLbl.startsWith("$"))
                                        edtMontoVisible.setText("$" + auxLbl);
                                    updateGraph(auxLbl);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                    edtMontoVisible.clearFocus();
                }

                return false;
            }
        });

        edtMontoVisible.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    String amount = edtMontoVisible.getText().toString();

                    if (!amount.equals("") || amount != null) {
                        if (amount.contains(","))
                            amount = amount.replace(",", "");

                        if (amount.contains("$"))
                            amount = amount.replace("$", "");

                        amount = amount.substring(0, amount.length() - 1);

                        edtMontoVisible.setText(Tools.formatUserAmount(amount));

                    }
                }

                return false;
            }
        });
    }

    public void updateGraph(String amount){
        isFromBar = true;

        double newAmount = 0.0, finalmmax = 0.0;
        int finalP = 0, auxMonMin = 0, auxAmount2 = 0;
        String auxAmount = Tools.clearAmounr(amount);

        if(auxAmount.equals(getMontMin())){
            seekBarSimulador.setProgress(0);

        }else if(auxAmount.equals(getMontoMax())){
            finalmmax = Double.parseDouble(getAuxFinalMax());
            seekBarSimulador.setProgress((int) finalmmax);

        }else {

            auxMonMin = Integer.parseInt(getMontMin());
            auxAmount2 = Integer.parseInt(auxAmount);

            finalP = auxAmount2-auxMonMin;
            newAmount = finalP / INCREMENTAL;

            seekBarSimulador.setProgress((int)newAmount);
        }

        validateProducSimulated();
    }

    private void setValuesSeekBar(){

        final double mmax = Double.parseDouble(Tools.validateMont(getMontoMax(), false));
        final double mmin = Double.parseDouble(Tools.validateMont(getMontMin(), false));
        double auxMmax = mmax-mmin;

        auxMmax = auxMmax/INCREMENTAL;

        final double finalAuxMmax = auxMmax;

        setAuxFinalMax(String.valueOf(finalAuxMmax));

        seekBarSimulador.setMax((int) finalAuxMmax);
        seekBarSimulador.setProgress((int) finalAuxMmax);

        seekBarSimulador.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String visibleMont;

                final int progressChanged = progress;
                final int progresFinal = calculatePogress(progressChanged, (int) mmin, (int) finalAuxMmax, (int) mmax);

                visibleMont = Tools.formatUserAmount(Integer.toString(progresFinal));
                edtMontoVisible.setText(visibleMont);
                edtMontoVisible.setCursorVisible(false);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                isFromBar = true;
                validateProducSimulated();
            }
        });
    }

    private void validateProducSimulated(){
        Log.i("auto","indSim: "+isIndSimProd());
        if (isIndSimProd()){ //si ya fue simulado, elimina y recalcula
            delegate.saveOrDeleteSimulationRequest(false); //true guarda, false elimina
        }else{
            setCvePlazo(txtvPlazo.getText().toString());
            setCveSubProd(txtvTipo.getText().toString());
            delegate.doRecalculo();
        }

    }

    public void setValuesProduct(){
        String montoMaximo, auxMMax, montoMinimo, auxMMin,
                montoFlow, cveProd, indSim;

        int imgProducto, descProd;

        cveProd         = getProducto().getCveProd();
        montoMaximo     = getProducto().getMontoMaxS();
        montoMinimo     = getProducto().getMontoMinS();
        indSim          = getProducto().getIndSim();
        imgProducto     = GuiTools.getCreditIcon(cveProd, false);
        descProd        = GuiTools.getSelectedLabel(cveProd);
        montoFlow       = getProducto().getMontoMaxS();   //valor que se tomará como monto máximo para saber si es mayor a 0.

        imgvProd.setImageResource(imgProducto);
        txtvDescProd.setText(descProd);

        auxMMax = Tools.formatUserAmount(Tools.validateMont(montoMaximo,true));
        auxMMin = Tools.formatUserAmount(Tools.validateMont(montoMinimo, true));

        if(auxMMax.contains("$"))
            auxMMax = auxMMax.replace("$","");
        if(auxMMin.contains("$"))
            auxMMin = auxMMin.replace("$","");

        setMontoMax(Tools.clearAmounr(auxMMax));
        setMontMin(Tools.clearAmounr(auxMMin));

        setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);

        txtvMontoMaximo.setText(auxMMax);
        txtvMontoMinimo.setText(auxMMin);


        edtMontoVisible.setMontoMax(getMontoMax());
        edtMontoVisible.setMontoMin(getMontMin());
        edtMontoVisible.setCveProd(producto.getCveProd());

        edtMontoVisible.setText(Tools.formatUserAmount(getMontoMax()));
    }

    private void validateSO(){
        Log.i("version","version android device: "+Build.VERSION.SDK_INT);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH){
            Log.i("version","version android es menor");
            seekBarSimulador.setThumb(getResources().getDrawable(R.drawable.seek_bar_thumb));
            seekBarSimulador.setProgressDrawable(getResources().getDrawable(R.drawable.custom_seek_bar));
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setThumbOffset(0);
        }
    }

    private void findViews(View vista){
        imgvProd        = (ImageView)vista.findViewById(R.id.imgvProd);
        imgvTipo        = (ImageView)vista.findViewById(R.id.imgvTipo);
        imgvShowDetalle = (ImageView)vista.findViewById(R.id.imgvShowDetalle);
        imgvShowDetalle.setOnClickListener(this);

        imgvAyuda       = (ImageView)vista.findViewById(R.id.imgvAyuda);
        imgvAyuda.setOnClickListener(this);

        imgvContratarCredito = (ImageButton)vista.findViewById(R.id.imgvContratarCredito);

        edtMontoVisible    = (LabelMontoTotalCredit)vista.findViewById(R.id.edtMontoVisible);

        txtvDescProd        = (TextView)vista.findViewById(R.id.txtvDescProd);
        txtvMontoMinimo     = (TextView)vista.findViewById(R.id.txtvMontoMinimo);
        txtvMontoMaximo     = (TextView)vista.findViewById(R.id.txtvMontoMaximo);
        txtvCat             = (TextView)vista.findViewById(R.id.txtvCat);
        txtvOpcion          = (TextView)vista.findViewById(R.id.txtvTipo);
        txtvPlazo           = (TextView)vista.findViewById(R.id.txtvPlazo);
        txtvTasa            = (TextView)vista.findViewById(R.id.txtvTasa);
        txtvTipo            = (TextView)vista.findViewById(R.id.txtvTipo);
        txtvMasInfo         = (TextView)vista.findViewById(R.id.txtvMasInfo);
        txtvPagoMMax        = (TextView)vista.findViewById(R.id.txtvPagoMMax);
        txtvMontoMMax       = (TextView)vista.findViewById(R.id.txtvMontoMMax);

        lnlDetalleProd      = (LinearLayout)vista.findViewById(R.id.lnlDetalleProd);
        lnlTipo             = (LinearLayout)vista.findViewById(R.id.lnlTipo);
        lnlTipo.setOnClickListener(this);

        lnlPlazo            = (LinearLayout)vista.findViewById(R.id.lnlPlazo);
        lnlPlazo.setOnClickListener(this);

        lnlPlazoTipo        = (LinearLayout)vista.findViewById(R.id.lnlPlazoTipo);
        lnlPagoMMaximo      = (LinearLayout)vista.findViewById(R.id.lnlPagoMMaximo);
        lnlPagoMMaximo2     = (LinearLayout)vista.findViewById(R.id.lnlPagoMMaximo2);

        seekBarSimulador    = (SeekBar)vista.findViewById(R.id.seekBarSimulador);

        lnlPlazoTipo.setVisibility(View.VISIBLE);
        lnlPagoMMaximo.setVisibility(View.VISIBLE);
        lnlPagoMMaximo2.setVisibility(View.GONE);
        lnlTipo.setVisibility(View.VISIBLE);
        lnlPlazo.setVisibility(View.VISIBLE);
        txtvTipo.setVisibility(View.VISIBLE);
        txtvMasInfo.setVisibility(View.VISIBLE);
        imgvContratarCredito.setVisibility(View.GONE);

    }

    private void loadLists(){

        int counter=0;
        optionList = new String[getProducto().getSubproducto().size()];

        auxiliarOptionList = new Subproducto[getProducto().getSubproducto().size()];

        for(Subproducto sb : getProducto().getSubproducto()) {
            this.productoAux = getProducto();

            optionList[counter] = sb.getDesSubp();
            auxiliarOptionList[counter] = sb;

            //JQH Oculta botón listar plazos
            if (sb.getPlazo().size()<=1)
                imgvTipo.setVisibility(View.GONE);


            //if(sb.getDesSubp().equalsIgnoreCase(ConstantsCredit.NUEVO_LBL)){
            termCreditLisNew = new String[sb.getPlazo().size()];
            int i=0;

            for(Plazo plazo : sb.getPlazo()) {
                termCreditLisNew[i]=plazo.getDesPlazo();
                i++;
            }

            if(!isListSet) {
                tipo = optionList[counter];
                plazo = termCreditLisNew.length>0? termCreditLisNew[0]:"";
                isListSet=true;
                termCreditList= termCreditLisNew;
            }
            counter++;
        }

        Log.w("plazo", "getIndSim: " + producto.getIndSim());


//        if(producto.getIndSim().equals("S")){ //si fue actualizada toma los valores guardados de respaldo
        //busca por clave y setea valores correctos

        txtvTipo.setText(getProducto().getDessubpE());
        txtvPlazo.setText(getProducto().getDesPlazoE());

    }

    private void pickerPlazo(){

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View DialogView = inflater.inflate(R.layout.activity_credit_picker, null);

        final NumberPicker dialogPicker = (NumberPicker)DialogView.findViewById(R.id.dialogPicker);
        dialogPicker.setMinValue(0);
        dialogPicker.setMaxValue(termCreditList.length - 1);
        dialogPicker.setDisplayedValues(termCreditList);
        dialogPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        dialogPicker.setValue(getPosition(termCreditList, txtvPlazo.getText().toString()));
        dialogPicker.setWrapSelectorWheel(false);

        new AlertDialog.Builder(getActivity()).setView(dialogPicker)
                .setTitle("Seleccione una opción")
                .setView(DialogView)
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }

                })
                .setPositiveButton("Seleccionar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        oDialog = dialog;
                        plazo = termCreditList[dialogPicker.getValue()];
                        txtvPlazo.setText(plazo); //setear cuando sea exitoso

                        String cvePlazo = buscarClave(txtvTipo.getText().toString(),plazo,buscarPlazo);
                        String cveSub   = buscarClave(txtvTipo.getText().toString(),"",buscarOpcion);

                        delegate.updateData(cveSub, cvePlazo, true);
                    }

                }).show();
    }

    private void pickerTipo(){

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View DialogView = inflater.inflate(R.layout.activity_credit_picker, null);

        final NumberPicker dialogPicker = (NumberPicker)DialogView.findViewById(R.id.dialogPicker);
        dialogPicker.setMinValue(0);
        dialogPicker.setMaxValue(optionList.length - 1);
        dialogPicker.setDisplayedValues(optionList);
        dialogPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        dialogPicker.setWrapSelectorWheel(false);
        dialogPicker.setValue(getPosition(optionList, txtvTipo.getText().toString()));

        new AlertDialog.Builder(getActivity()).setView(dialogPicker)
                .setTitle("Seleccione una opción")
                .setView(DialogView)
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }

                })
                .setPositiveButton("Seleccionar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        oDialog = dialog;
                        tipo = optionList[dialogPicker.getValue()];
                        txtvTipo.setText(tipo);
                        updateTermList(tipo);

                        String cveSub = buscarClave(tipo,"",buscarOpcion);
                        String cvePlazo = buscarClave(tipo, plazo, buscarPlazo);

                        Log.i("update","cveSub: "+cveSub+", cveplazo: "+cvePlazo);
                        delegate.updateData(cveSub,cvePlazo, true);

                    }

                }).show();
    }

    private void updateTermList(String code) {
        for(Subproducto sb : auxiliarOptionList) {

            if (sb.getDesSubp().equals(code)) {

                termCreditList = termCreditLisNew;
                plazo = termCreditLisNew.length>0? termCreditLisNew[0]:"";
                txtvPlazo.setText(plazo);
                break;
            }

        }
    }

    private int getPosition(String [] array, String code) {
        for(int i= 0; i< array.length;i++) {
            if(array[i].equalsIgnoreCase(code))
                return i;
        }
        return -1;
    }

    public String buscarClave(String desSub, String value, boolean cvePlazo){
        Producto aux = productoAux;
        String clave = "";

        for(Subproducto auxSP : aux.getSubproducto()) {

            if(desSub.equals(auxSP.getDesSubp())) {
                if (cvePlazo) { //busca plazo
                    for (int i = 0; i < auxSP.getPlazo().size(); i++) {
                        if (value.equals(auxSP.getPlazo().get(i).getDesPlazo())) {
                            clave = auxSP.getPlazo().get(i).getCvePlazo();
                        }
                    }
                }else{ //busca opcion
                    clave = auxSP.getCveSubp();
                }
            }
        }
        return clave;
    }

    @Override
    public void onClick(View v) {

        if (v == lnlPlazo){
            pickerPlazo();
        }

        if (v == lnlTipo){
            pickerTipo();
        }

        if (v == imgvAyuda) {
            PopUpAyudaViewController.mostrarPopUp((MenuPrincipalActivitySeekBar)getActivity(),getProducto().getCveProd());
        }

        if(v == imgvShowDetalle){


            if(isSimulated){ //si ya fue simulado, muestra detalle
                Log.i("auto","producto simulado");
                validateLayoutToShow();

            }else{
                Log.w("auto","producto no simulado");
                isFromBar = false;
//                isVisible = true;
                validateProducSimulated();
            }
        }
    }

    public void setSimulation(){
        ((MenuPrincipalActivitySeekBar)getActivity()).isAutoS = true;
        ((MenuPrincipalActivitySeekBar) getActivity()).validateTxtvColor(controller);

        setColor(false);
    }

    public void setColor(boolean isNormal){
        if(isNormal)
            edtMontoVisible.setTextColor(getResources().getColor(R.color.nuevaimagen_tx5));
        else
            edtMontoVisible.setTextColor(getResources().getColor(R.color.naranja));
    }

    public void validateLayoutToShow(){

        //oculta detalle
        if(isShowDetalle()) {
            setIsShowDetalle(false);
            lnlDetalleProd.setVisibility(View.GONE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_masdetalle);

        }else{
            setIsShowDetalle(true);
            ((MenuPrincipalActivitySeekBar)getActivity()).isAutoDetail = true;
            ((MenuPrincipalActivitySeekBar)getActivity()).validateIsShowAnyDetail(this);

            lnlDetalleProd.setVisibility(View.VISIBLE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_menosdetalle);

            setDetailProduct();
        }

    }

    public void setDetailProduct(){
        try {
            //        ArrayList<Producto>detail = Session.getInstance().getAuxCreditos().getProductos();
            Producto newProducto = delegate.getProducto();

            String clveMesQuincena, cat, tasa, totalMesQuincena;

            clveMesQuincena = newProducto.getSubproducto().get(0).getCveSubp();
            tasa = newProducto.getTasaS();
            cat = newProducto.getCATS();
            totalMesQuincena = newProducto.getPagMProdS();

            Log.i("auto","tasa: "+tasa);
            Log.i("auto","cat: "+cat);
            Log.i("auto","totalMesQuincena: "+totalMesQuincena);

            txtvPagoMMax.setText(getResources().getString(R.string.lblResumenMensual)+" ");

            txtvTasa.setText("Tasa de interés: "+tasa+" %");
            txtvCat.setText("CAT: "+cat+"% sin iva");
            txtvMontoMMax.setText(GuiTools.getMoneyString(totalMesQuincena));

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void updateData(boolean isOperationDelete){
        Log.i("updatedata","se manda actualizar productos desde auto");
        ((MenuPrincipalActivitySeekBar)getActivity()).updateAllProductData(this,isOperationDelete);
    }

    private int calculatePogress(int progress, int min, int progresFinal, int mmax){

        int auxMin = min;

        if (progress == 0){
            return min;
        }else {

            for (int i = 1; i<=progress; i++) {
                auxMin = auxMin + INCREMENTAL;
                if (progress == progresFinal){
                    auxMin = mmax;
                }
            }
        }

        return auxMin;
    }

    public void reset() {
        this.isResetting = true;
        this.typedString=new StringBuffer();
        this.isResetting = false;
    }

    private void setFormattedText(){

        isSettingText = true;
        String text = typedString.toString();

        edtMontoVisible.setText(Tools.formatUserAmount(text));
    }

    public String getTipo() {
        return tipo;
    }

    public String getCvePlazo() {
        return cvePlazo;
    }

    public void setCvePlazo(String cvePlazo) {
        this.cvePlazo = cvePlazo;
    }

    public String getCveSubProd() {
        return cveSubProd;
    }

    public void setCveSubProd(String cveSubProd) {
        this.cveSubProd = cveSubProd;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public String getMontoMax() {
        return montoMax;
    }

    public void setMontoMax(String montoMax) {
        this.montoMax = montoMax;
    }

    public String getMontMin() {
        return montMin;
    }

    public void setMontMin(String montMin) {
        this.montMin = montMin;
    }

    public boolean isIndSimProd() {
        return indSimProd;
    }

    public void setIndSimProd(boolean indSimProd) {
        this.indSimProd = indSimProd;
    }

    public boolean isShowDetalle() {
        return isShowDetalle;
    }

    public void setIsShowDetalle(boolean isShowDetalle) {
        this.isShowDetalle = isShowDetalle;
    }

    public String getAuxFinalMax() {
        return auxFinalMax;
    }

    public void setAuxFinalMax(String auxFinalMax) {
        this.auxFinalMax = auxFinalMax;
    }

}