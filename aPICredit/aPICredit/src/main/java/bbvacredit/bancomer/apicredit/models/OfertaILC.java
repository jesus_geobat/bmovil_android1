package bbvacredit.bancomer.apicredit.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.CreditJSONAble;

import java.io.IOException;
import java.util.ArrayList;

import bbvacredit.bancomer.apicredit.io.ParsingHandlerJSON;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;

public class OfertaILC implements ParsingHandlerJSON, CreditJSONAble {
	String importe;
	Account account= new Account();
	String contrato;
	String lineaActual;
	String lineaFinal;
	String fechaCat;
	String cat;
	public ArrayList<Account> accountILC;
	
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public ArrayList<Account> getAccountILC() {
		return accountILC;
	}

	public void setAccountILC(ArrayList<Account> accountILC) {
		this.accountILC = accountILC;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getLineaActual() {
		return lineaActual;
	}

	public void setLineaActual(String lineaActual) {
		this.lineaActual = lineaActual;
	}

	public String getLineaFinal() {
		return lineaFinal;
	}

	public void setLineaFinal(String lineaFinal) {
		this.lineaFinal = lineaFinal;
	}

	public String getFechaCat() {
		return fechaCat;
	}

	public void setFechaCat(String fechaCat) {
		this.fechaCat = fechaCat;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}	

	@Override
	public void fromJSON(String jsonString) {

		try {
			JSONObject obj = new JSONObject(jsonString);

			Log.i("ilc", "fromJSON,jsonobj: " + obj);

			importe = obj.getString("importe");
			contrato = obj.getString("numContrato");
			lineaActual = obj.getString("lineaActual");
			lineaFinal = obj.getString("lineaFinal");
			fechaCat = obj.getString("fechaCat");
			cat = obj.getString("Cat");
			account.setAlias(obj.getString("alias"));
			account.setNumber(obj.getString("numeroTarjeta"));

		}catch (JSONException ex){
			ex.printStackTrace();
		}
	}

	@Override
	public String toJSON() {
		return null;
	}

	@Override
	public void processJSON(String jsonString) throws JSONException {
		JSONObject obj = new JSONObject(jsonString);

		Log.i("ilc","jsonobj: "+obj);

		importe=obj.getString("importe");
		contrato=obj.getString("numContrato");
		lineaActual=obj.getString("lineaActual");
		lineaFinal=obj.getString("lineaFinal");
		fechaCat=obj.getString("fechaCat");
		cat=obj.getString("Cat");
		account.setAlias(obj.getString("alias"));
		account.setNumber(obj.getString("numeroTarjeta"));
	}
}
