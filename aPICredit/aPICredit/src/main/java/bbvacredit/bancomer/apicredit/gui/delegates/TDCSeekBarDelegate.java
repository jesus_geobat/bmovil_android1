package bbvacredit.bancomer.apicredit.gui.delegates;

import android.util.Log;

import org.apache.http.impl.client.DefaultHttpClient;

import java.util.Hashtable;

import bancomer.api.apiventatdc.commons.ConstantsVentaTDC;
import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.apiventatdc.models.OfertaTDC;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.AdelantoNominaSeekBarViewController;
import bbvacredit.bancomer.apicredit.gui.activities.MenuPrincipalActivitySeekBar;
import bbvacredit.bancomer.apicredit.gui.activities.TDCSeekBarViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by FHERNANDEZ on 28/12/16.
 */
public class TDCSeekBarDelegate extends BaseDelegateOperacion {

    private TDCSeekBarViewController controller;
    private Producto producto;
    private ObjetoCreditos data;
    private ObjetoCreditos auxData;
    private Session session;
    private boolean isDeleteOperation = false; //si es true, borrar simulación
    private boolean productoConPlazo;
    private Promociones promociones;
    private String indSim;

    public TDCSeekBarDelegate(TDCSeekBarViewController controller){
        this.controller = controller;
        session = Tools.getCurrentSession();
    }


    public void analyzeResponse(String operationId, ServerResponse response) {
        //CONSULTA_DETALLE_TDC one click
        if(Server.CONSULTA_DETALLE_TDC.equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
             Log.i("TDC", "consulta exitosa");
                MainController.getInstance().ocultaIndicadorActividad();

                bbvacredit.bancomer.apicredit.models.OfertaTDC ofertaTDC = (bbvacredit.bancomer.apicredit.models.OfertaTDC) response.getResponse();
                controller.setOfertaTDC(ofertaTDC);
                if (!controller.isFromBar)
                    controller.validateLayoutToShow();
            }

        } else if(getCodigoOperacion().equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                MainController.getInstance().ocultaIndicadorActividad();
                session.setAuxCreditos(data);
//                session.setCreditos(data);

                setProducto(Tools.getProductByCve("0TDC", data.getProductos()));

                //actualización de los objetos
                controller.updateData(false);

                indSim = getProducto().getIndSim();
//                Log.i("tdc", "cveProd: " + Tools.getProductByCve("0TDC", data.getProductos()).getCveProd() + ", indSim: " + Tools.getProductByCve("0TDC", data.getProductos()).getIndSim());
                controller.setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);

                controller.setSimulation();
                controller.isSimulated = true;

                if (controller.isShowDetalle()){
                    controller.setDetailProduct();

                }else if (!controller.isFromBar) {
                    controller.setIsShowDetalle(false);
                    controller.validateLayoutToShow();
                }

            }
        }else if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

                CalculoData respuesta = (CalculoData) response.getResponse();

                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

//                session.setCreditos(data);
                session.setAuxCreditos(data);

                setProducto(Tools.getProductByCve("0TDC", data.getProductos()));
                //actualización de los objetos
                controller.updateData(false);

                indSim = getProducto().getIndSim();
                controller.setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);


                if(isDeleteOperation){
                    controller.updateData(true);
                    Log.i("simulated","se ha eliminado, ahora calcula");
                    isDeleteOperation = false;
                    controller.isSimulated = false;
                    doRecalculo();

                }else{
                    Log.i("contrato","contrat TDC por consumo");
                    doDetalleAlternativasTDC();
                }
            }
        }
    }

    public void iniciaFlujoTDC(){

        MainController mainCntr = MainController.getInstance();
        InitVentaTDC initVentaTDC = InitVentaTDC.getInstance(mainCntr.getActivityController().getCurrentActivity(),
                getPromociones(),mainCntr.getDelegateOTP(),
                mainCntr.getSession(), mainCntr.getAutenticacion(), mainCntr.isSofTokenStatus(), mainCntr.getNumeroCuenta(),mainCntr.getClient(),
                mainCntr.getHostInstance(),Server.DEVELOPMENT,Server.SIMULATION,Server.EMULATOR,Server.ALLOW_LOG);
        mainCntr.getActivityController().getCurrentActivity().setActivityChanging(true);
        initVentaTDC.startVentaTDC();
    }

    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(Server.CALCULO_ALTERNATIVAS_OPERACION, ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        if (isSavingOperation) {
            addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = false;
        } else {
            addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = true;
        }

        Hashtable<String, String> paramTable2 = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
    }

    public void doRecalculo(){

        String plazoSel= "";
        String cvePlazoSel = "";
        String montoSel = "";

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
        Producto ccr = controller.getProducto();

        Log.i("detalle","cveProd doRecalc: "+ccr.getCveProd());
        montoSel = controller.edtMontoVisible.getText().toString();
        montoSel = montoSel.replace("$","");
        montoSel = montoSel.replace(",","");
        cvePlazoSel = ccr.getCvePzoE();

        addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM, paramTable);
        addParametro(ccr.getSubproducto().get(0).getCveSubp(), ServerConstantsCredit.CVESUBP_PARAM, paramTable);
        addParametro(cvePlazoSel, ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        addParametro("", ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM, paramTable);

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    public void consultaDetalleTDCOneClick(){

        MainController mainCntr = MainController.getInstance();
        InitVentaTDC initVentaTDC = InitVentaTDC.getInstance(mainCntr.getActivityController().getCurrentActivity(),
                getPromociones(),mainCntr.getDelegateOTP(),
                mainCntr.getSession(), mainCntr.getAutenticacion(), mainCntr.isSofTokenStatus(), mainCntr.getNumeroCuenta(),mainCntr.getClient(),
                mainCntr.getHostInstance(),Server.DEVELOPMENT,Server.SIMULATION,Server.EMULATOR,Server.ALLOW_LOG);
        mainCntr.getActivityController().getCurrentActivity().setActivityChanging(true);

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ConstantsVentaTDC.OPERACION_TAG, "detalleTDC");
        params.put(ConstantsVentaTDC.NUMERO_CELULAR_TAG, session.getNumCelular());
        params.put(ConstantsVentaTDC.CVE_CAMP_TAG, getPromociones().getCveCamp());
        params.put(ConstantsVentaTDC.IUM_TAG, session.getIum());
        params.put(ConstantsVentaTDC.OFICINA_TAG, initVentaTDC.getCuentaOneClick().substring(4,8));

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.consultaDetalleTDC(params);
        this.doNetworkOperation(Server.CONSULTA_DETALLE_TDC, paramTable2, true, new bbvacredit.bancomer.apicredit.models.OfertaTDC(), MainController.getInstance().getContext());
    }

    public void doDetalleAlternativasTDC(){


        Log.i("contrataTDC", "detalleTDCDelegate, data.montosolicitado: " + data.getMontotSol());
        Log.i("contrataTDC", "detalleTDCDelegate, producto.montosolicitado: " + producto.getMontoDeseS());

        String ium = session.getIum();
        String numeroCelular = session.getNumCelular();

        String producto = controller.getProducto().getCveProd();
        String cliente = session.getIdUsuario();
        Session.getInstance().setCveCamp(producto);

        DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(cliente,ium,numeroCelular,producto,null);
        delegate.consultaDetalleAlternativaTDCTask(this.producto);
    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Promociones getPromociones() {
        return promociones;
    }

    public void setPromociones(Promociones promociones) {
        this.promociones = promociones;
    }

    @Override
    protected String getCodigoOperacion() {
        return Server.CALCULO_OPERACION;
    }
}
