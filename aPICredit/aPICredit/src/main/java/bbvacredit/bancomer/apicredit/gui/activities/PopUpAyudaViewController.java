package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.GuiTools;

/**
 * Created by FHERNANDEZ on 28/12/16.
 */
public class PopUpAyudaViewController {


    public static void mostrarPopUp(final Activity activity, String cveProd) {

        final Dialog contactDialog = new Dialog(activity);
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View vista = inflater.inflate(R.layout.activity_popup_ayuda, null);
        final ImageView imgvIconProdAyuda = (ImageView)vista.findViewById(R.id.imgvIconProdAyuda);
        final TextView txtvPrdAyuda  = (TextView)vista.findViewById(R.id.txtvProdAyuda);
        final TextView txvDesCompletaProd = (TextView)vista.findViewById(R.id.txtvDesCompletaProd);

        contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // make dialog itself transparent
        contactDialog.setCancelable(true);
        contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(activity.getResources().getColor(android.R.color.transparent)));
        contactDialog.setContentView(vista);

        int imgProducto     = GuiTools.getCreditIcon(cveProd, false);
        int nombreProd   = GuiTools.getSelectedLabelUper(cveProd);
        int descProd        = GuiTools.getSelectedDescriptionProduct(cveProd);

        imgvIconProdAyuda.setImageResource(imgProducto);
        txtvPrdAyuda.setText(nombreProd);
        txvDesCompletaProd.setText(descProd);

        ((ImageView)vista.findViewById(R.id.imgvEntendido)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDialog.dismiss();
            }
        });


        int original_width = activity.getResources().getDisplayMetrics().widthPixels;
        final int width = (int)(activity.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = 0;
        if(original_width > 500) {
            height = (int) (activity.getResources().getDisplayMetrics().heightPixels * 0.80);
        }else{
            height = (int) (activity.getResources().getDisplayMetrics().heightPixels * 0.90);
        }
        contactDialog.getWindow().setLayout(width, height);

        final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(contactDialog.getWindow().getWindowManager());
        guiTools.scale(imgvIconProdAyuda, false);
        guiTools.scale(txtvPrdAyuda, true);
        guiTools.scale(txvDesCompletaProd, true);

        contactDialog.show();

    }
}
