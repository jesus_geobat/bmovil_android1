package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.EnviarCorreoDelegate;

/**
 * Created by SDIAZ on 25/11/15.
 */
public class  ResumenEmailViewController extends BaseActivity implements View.OnClickListener{

    private Button lblAvisoPrivacidad;

    private ImageView imgSobre;
    private ImageView imgEditarEmail;

    private ImageButton btnCancelar;
    private Button btnAceptar;

    private TextView lblMensajeEnvioEmail;
    private EditText lblEnvioEmail;
    private TextView lblEnvioEmailInformativo;
    private TextView lblEnvioEmailInformativo2;

    private ScrollView body;
    private EnviarCorreoDelegate delegate;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_TITLE | SHOW_HEADER, R.layout.activity_credit_resumen_correo);

        imgBtnEliminar.setVisibility(View.INVISIBLE);

        setActivityChanging(false);

        MainController.getInstance().setContext(this);
        MainController.getInstance().setCurrentActivity(this);

        findViews();
        init();
    }

    public void verAviso(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("BBVA Bancomer, S.A. Av. Paseo de la Reforma No. 510, Col Juárez\n" +
                "Del. Cuauhtémoc, C.P. 06600, México, D.F. recaba sus datos para verificar su identidad. El\n" +
                "Aviso de Privacidad Integral actualizado está en cualquier\n" +
                "sucursal y en www.bancomer.com")
                .setTitle("AVISO")
                .setCancelable(false)
                .setNeutralButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void init() {
        delegate = new EnviarCorreoDelegate();
        lblEnvioEmail.setText(delegate.getEmail());
//        lblEnvioEmail.setText("albertofrancisco.hernandez.contractor@bbva.com");
    }

    private void findViews() {
        imgEncabezado.setImageResource(R.drawable.txt_pagos);
        lblAvisoPrivacidad = (Button) findViewById(R.id.lblAvisoPrivacidad);

        imgSobre = (ImageView) findViewById(R.id.imgSobre);
        imgSobre.requestFocus();
        imgEditarEmail = (ImageView) findViewById(R.id.imgEditarEmail);

        imgEditarEmail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                lblEnvioEmail.setEnabled(true);
                lblEnvioEmail.requestFocus();
                lblEnvioEmail.setText("");
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(lblEnvioEmail, InputMethodManager.SHOW_IMPLICIT);
            }
        });


        btnCancelar = (ImageButton) findViewById(R.id.btnCancelar);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                android.util.Log.i("resumen","btn cancelar");
                MainController.getInstance().showScreen(MenuPrincipalActivity.class);
                setActivityChanging(true);
            }

        });


        btnAceptar = (Button) findViewById(R.id.btnAceptarRS);
        btnAceptar.setOnClickListener(this);

        lblMensajeEnvioEmail = (TextView) findViewById(R.id.lblMensajeEnvioEmail);
        lblEnvioEmail = (EditText) findViewById(R.id.lblEnvioEmail);

        lblEnvioEmailInformativo = (TextView) findViewById(R.id.lblEnvioEmailInformativo);
        lblEnvioEmailInformativo2 = (TextView) findViewById(R.id.lblEnvioEmailInformativo2);

        lblEnvioEmail.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == 66) {

                    InputMethodManager manager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (manager != null)
                        manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    lblEnvioEmail.setEnabled(false);


                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(isEmailValid(lblEnvioEmail.getText().toString()))
            delegate.enviarCorreoConIndicador("C", lblEnvioEmail.getText().toString(), this);
        else
        {
            this.showInformationAlert("Formato de correo electrónico no válido, favor de verificarlo",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    lblEnvioEmail.setEnabled(true);
                    lblEnvioEmail.requestFocus();
                    lblEnvioEmail.setText("");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(lblEnvioEmail, InputMethodManager.SHOW_IMPLICIT);
                }
            });
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
            MainController.getInstance().showScreen(MenuPrincipalActivity.class);
        return super.onKeyDown(keyCode, event);
    }
}
