package bbvacredit.bancomer.apicredit.gui.delegates;

import android.util.Log;

import java.util.Hashtable;
import java.util.Iterator;

import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.OfertasListViewAdapter;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;

/**
 * Created by FHERNANDEZ on 28/12/16.
 */
public class OfertasListViewDelegate extends BaseDelegateOperacion {

    private OfertasListViewAdapter controller;
    private Producto producto;
    private ObjetoCreditos data;
    private ObjetoCreditos auxData;
    private Session session;
    private boolean isDeleteOperation = false; //si es true, borrar simulación
    private boolean productoConPlazo;

    public OfertasListViewDelegate(OfertasListViewAdapter controller){
        this.controller = controller;
        session = Tools.getCurrentSession();
    }


    public void analyzeResponse(String operationId, ServerResponse response) {
        if(getCodigoOperacion().equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                MainController.getInstance().ocultaIndicadorActividad();
//                session.setAuxCreditos(data);
                session.setCreditos(data);

                saveOrDeleteSimulationRequest(true);

            }
        }else if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                if(isDeleteOperation){
                    isDeleteOperation = false;

                    CalculoData respuesta = (CalculoData) response.getResponse();

                    data = null;
                    data = new ObjetoCreditos();

                    data.setCreditos(respuesta.getCreditos());
                    data.setEstado(respuesta.getEstado());
                    data.setMontotSol(respuesta.getMontotSol());
                    data.setPagMTot(respuesta.getPagMTot());
                    data.setPorcTotal(respuesta.getPorcTotal());
                    data.setProductos(respuesta.getProductos());
                    data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                    MainController.getInstance().ocultaIndicadorActividad();
//                    session.setAuxCreditos(data);  //se guarda por separado
                    session.setCreditos(data);

                    doRecalculo();

                }else{
                    Log.i("detalle","showDetalle, getProd: "+getProducto().getCveProd());
                    controller.setDetalleProducto(getProducto().getCveProd());
                }
            }
        }
    }


    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(Server.CALCULO_ALTERNATIVAS_OPERACION, ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        if (isSavingOperation) {
            addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = false;
        } else {
            addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = true;
        }

        Hashtable<String, String> paramTable2 = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    public void doRecalculo(){

        String plazoSel= "";
        String cvePlazoSel = "";
        float montoSel = 0;

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
        Producto ccr = getProducto();

        Log.i("detalle","cveProd doRecalc: "+ccr.getCveProd());
        montoSel = Float.valueOf(ccr.getMontoDeseS());

        if(isProductoConPlazo()) {
            plazoSel = ccr.getDesPlazoE();
            cvePlazoSel = getCvPlazoByValue(producto, plazoSel);
        }

        addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM, paramTable);
        addParametro(ccr.getSubproducto().get(0).getCveSubp(), ServerConstantsCredit.CVESUBP_PARAM, paramTable);
        addParametro(cvePlazoSel, ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        addParametro("", ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM, paramTable);

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    private void setSimLooking4Cve(String cve){
        Iterator<Producto> it = data.getProductos().iterator();

        while(it.hasNext()){
            Producto pr = it.next();
            if(pr.getCveProd().equals(cve)) pr.setIndSimBoolean(true);
        }
    }

    public Producto getAuxProductoByCveProd(String cveProd){
//        ObjetoCreditos obj = session.getAuxCreditos();
        ObjetoCreditos obj = session.getCreditos();
        Producto productoSim = null;

        for(int i = 0; i<obj.getProductos().size(); i++){
            Log.i("buscando","buscando producto simulado: "+cveProd);
            if (cveProd.equalsIgnoreCase(obj.getProductos().get(i).getCveProd())) {
                productoSim = obj.getProductos().get(i);
                Log.w("buscando", "producto simulado: "+productoSim.getCveProd());
            }
        }

        return productoSim;
    }

    public ObjetoCreditos getAuxData() {
//        session.getAuxCreditos();
        return auxData;
    }

    public boolean isProductoConPlazo() {
        return productoConPlazo;
    }

    public void setProductoConPlazo(boolean productoConPlazo) {
        this.productoConPlazo = productoConPlazo;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    protected String getCodigoOperacion() {
        return Server.CALCULO_OPERACION;
    }
}
