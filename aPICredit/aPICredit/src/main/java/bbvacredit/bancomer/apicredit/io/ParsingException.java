package bbvacredit.bancomer.apicredit.io;

public class ParsingException extends Exception {
	
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	
	/**
     * Default constructor.
     * @param message the message
     */
    public ParsingException(String message) {
        super(message);
    }
}
