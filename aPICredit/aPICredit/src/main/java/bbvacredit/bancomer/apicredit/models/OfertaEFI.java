package bbvacredit.bancomer.apicredit.models;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.CreditJSONAble;

import java.util.ArrayList;

import bbvacredit.bancomer.apicredit.io.ParsingHandlerJSON;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;

public class OfertaEFI implements ParsingHandlerJSON, CreditJSONAble {
	
	String importe;
	String fecFinOferta;
	String comisionEFIPorcentaje;
	String comisionEFIMonto;
	String plazoMeses;
	String tasaAnual;
	//String montoUtilizado;
	String montoDisponible;
	String montoDisponibleFijo; //Nuevo monto
	//String montoDisposicion;
	//String pagoMensual;
	String pagoMensualSimulador;
	String disponibleTDC;
	String celula;
	String edicion;
	Account accountDestino= new Account();
	Account accountOrigen= new Account();
	String numContrato;
	String fechaCat;
	String cat;
	String montoMaximo;
	Account accountTipoCuenta= new Account(); //Tipo de cuenta
	public ArrayList<Account> accountEFI=new ArrayList<Account>();
	
	
//Tipo de cuenta
	public Account getTipoCuenta() {
		return accountTipoCuenta;
	}


	public void setTipoCuenta(Account tipocuenta) {
		this.accountTipoCuenta = tipocuenta;
	}

//End Tipo de cuenta	
	
	
	
	public String getImporte() {
		return importe;
	}



	public void setImporte(String importe) {
		this.importe = importe;
	}



	public String getFecFinOferta() {
		return fecFinOferta;
	}



	public void setFecFinOferta(String fecFinOferta) {
		this.fecFinOferta = fecFinOferta;
	}



	public String getComisionEFIPorcentaje() {
		return comisionEFIPorcentaje;
	}



	public void setComisionEFIPorcentaje(String comisionEFIPorcentaje) {
		this.comisionEFIPorcentaje = comisionEFIPorcentaje;
	}



	public String getComisionEFIMonto() {
		return comisionEFIMonto;
	}



	public void setComisionEFIMonto(String comisionEFIMonto) {
		this.comisionEFIMonto = comisionEFIMonto;
	}



	public String getPlazoMeses() {
		return plazoMeses;
	}



	public void setPlazoMeses(String plazoMeses) {
		this.plazoMeses = plazoMeses;
	}



	public String getTasaAnual() {
		return tasaAnual;
	}



	public void setTasaAnual(String tasaAnual) {
		this.tasaAnual = tasaAnual;
	}


	public String getMontoDisponible() {
		return montoDisponible;
	}



	public void setMontoDisponible(String montoDisponible) {
		this.montoDisponible = montoDisponible;
	}

	
	//Modificacion
	public String getMontoDisponibleFijo() {
		return montoDisponibleFijo;
	}



	public void setMontoDisponibleFijo(String montoDisponibleFijo) {
		this.montoDisponibleFijo = montoDisponibleFijo;
	}

	public String getPagoMensualSimulador() {
		return pagoMensualSimulador;
	}



	public void setPagoMensualSimulador(String pagoMensualSimulador) {
		this.pagoMensualSimulador = pagoMensualSimulador;
	}


	public String getCelula() {
		return celula;
	}



	public void setCelula(String celula) {
		this.celula = celula;
	}



	public String getEdicion() {
		return edicion;
	}



	public void setEdicion(String edicion) {
		this.edicion = edicion;
	}



	public Account getAccountDestino() {
		return accountDestino;
	}



	public void setAccountDestino(Account accountDestino) {
		this.accountDestino = accountDestino;
	}



	public Account getAccountOrigen() {
		return accountOrigen;
	}



	public void setAccountOrigen(Account accountOrigen) {
		this.accountOrigen = accountOrigen;
	}



	public String getNumContrato() {
		return numContrato;
	}



	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}



	public String getFechaCat() {
		return fechaCat;
	}



	public void setFechaCat(String fechaCat) {
		this.fechaCat = fechaCat;
	}



	public String getCat() {
		return cat;
	}



	public void setCat(String cat) {
		this.cat = cat;
	}
	
	

	public String getMontoMaximo() {
		return montoMaximo;
	}



	public void setMontoMaximo(String montoMaximo) {
		this.montoMaximo = montoMaximo;
	}



	public ArrayList<Account> getAccountEFI() {
		return accountEFI;
	}



	public void setAccountEFI(ArrayList<Account> accountEFI) {
		this.accountEFI = accountEFI;
	}



	public String getDisponibleTDC() {
		return disponibleTDC;
	}



	public void setDisponibleTDC(String disponibleTDC) {
		this.disponibleTDC = disponibleTDC;
	}


	@Override
	public void processJSON(String jsonString) throws JSONException {
		JSONObject obj = new JSONObject(jsonString);

		importe=obj.getString("importe");
		fecFinOferta=obj.getString("fecFinOferta");
		comisionEFIPorcentaje=obj.getString("comisionEFIPorcentaje");
		comisionEFIMonto=obj.getString("comisionEFIMonto");
		plazoMeses=obj.getString("plazoMeses");
		tasaAnual=obj.getString("tasaAnual");
		//montoUtilizado=parser.parseNextValue("montoUtilizado");
		montoDisponible=obj.getString("montoDisponible");
		montoMaximo=obj.getString("montoDisponible");
		montoDisponibleFijo=obj.getString("montoDisponible");
		//montoDisposicion=parser.parseNextValue("montoDisposicion");
		//pagoMensual=parser.parseNextValue("pagoMensual")+"00";
		pagoMensualSimulador=obj.getString("pagoMensualSimulador");
		disponibleTDC=obj.getString("disponibleTDC");
		celula=obj.getString("celula");
		edicion=obj.getString("edicion");
		fechaCat=obj.getString("fechaCat");
		cat=obj.getString("Cat");
		accountOrigen.setAlias(obj.getString("alias"));
		accountOrigen.setNumber(obj.getString("numeroTarjeta"));
		numContrato=obj.getString("numContrato");
		//	accountDestino.setNumber(parser.parseNextValue("cuentaEje"));

		accountEFI.add(accountOrigen);
	}

	@Override
	public void fromJSON(String jsonString) {

	}

	@Override
	public String toJSON() {
		return null;
	}
}
