package bbvacredit.bancomer.apicredit.gui.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

import bancomer.api.common.timer.TimerController;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.ListaDatosController;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.SimulaCreditoNominaDelegate;
import bbvacredit.bancomer.apicredit.models.Producto;

public class SimulaCreditoNominaLiquidacionActivity extends BaseActivity implements OnClickListener {
	
	private SimulaCreditoNominaDelegate delegate;
	private ImageButton backButton;
	
	// Menu Button
	private Button resumenBtn;

	//private ImageView simulaBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState,SHOW_HEADER|SHOW_TITLE, R.layout.activity_simula_credito_nomina_liquidez);
		isOnForeground = true;
		MainController.getInstance().setCurrentActivity(this);
		init();
		//delegate.doRecalculoNC(this);
		deleteSimulation();
	}
	
	private void init(){
		// le indicamos al delegate que no es contratacion
		Tools.setIsContratacionPreference(false);
		delegate = new SimulaCreditoNominaDelegate();
		setSaldo();
		// Ocultamos la info si es necesario
		if(delegate.getOcultaInfo()){
			ocultaInfoNC();
		}else{
			pintarInfoTabla();
		}
		scaleToCurrentScreen();
		mapearBotones();
		
		Tools.getCurrentSession().setVisibilityButton(this, R.id.simCNomBottomMenuRC);
	}
	
	private void mapearBotones(){		
		//backButton = (ImageButton)findViewById(R.id.headerRefresh);
		backButton.setOnClickListener(this);
		
		resumenBtn = (Button)findViewById(R.id.simCNomBottomMenuRC);
		resumenBtn.setOnClickListener(this);
		
		//simulaBtn = (ImageView)findViewById(R.id.simCNomHeaderLogo);
		//simulaBtn.setOnClickListener(this);
	}

	private void setSaldo(){
		TextView saldo = (TextView)findViewById(R.id.simCNomILMtxt);
		
		saldo.setText(GuiTools.getMoneyString(delegate.getSaldo().toString()));
	}
	
	// Oculta Info
	private void ocultaInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCAuto);
		tlayout.setVisibility(View.INVISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		tableLayout.setVisibility(View.INVISIBLE);
	}
	
	private void muestraInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCAuto);
		tlayout.setVisibility(View.VISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		tableLayout.setVisibility(View.VISIBLE);
	}
	// End Oculta Info
	/**
	 * Escala los elementos de la pantalla para la resoluci�n actual.
	 */
	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		
		guiTools.scale(findViewById(R.id.simCNomLayout));
		guiTools.scale(findViewById(R.id.simCNomBodyLayout));
		guiTools.scale(findViewById(R.id.simCNomTitle),true);
		guiTools.scale(findViewById(R.id.simCNomSubTitleTCImg));
		//guiTools.scale(findViewById(R.id.simCNomLText),true);
		guiTools.scale(findViewById(R.id.simCNomILMtxt),true);
		//guiTools.scale(findViewById(R.id.simCNomHeaderLogo));
		guiTools.scale(findViewById(R.id.simCNomBottomLayout));
		guiTools.scale(findViewById(R.id.simCNomBottomMenuRC),true);
		
		guiTools.scale(findViewById(R.id.simCAuto));
		guiTools.scale(findViewById(R.id.simCAutoLlblAuxiliar),true);
		
		//guiTools.scale(findViewById(R.id.simCAutoLblInfo),true);
		guiTools.scale(findViewById(R.id.RelativeLayoutText1));
	}
	
	private void ocultaCamposTabla(){
		LinearLayout layout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		layout.removeAllViews();
		
	}
	
	public void pintarInfoTabla(){
		ocultaCamposTabla();
		
		muestraInfoNC();
		
		ArrayList<Producto> lista = delegate.getProductTable();
		Iterator<Producto> it = lista.iterator();
		Integer cont = 0;

		ListaDatosController listaController;
		String prodDesc="";
		while(it.hasNext()){
			Producto p = it.next();
			if(p.getCveProd().equals("0NOM") || p.getCveProd().equals("0PPI")) 
			{
			                               prodDesc = "PRESTAMO PERSONAL";
			                       } else {
			                               prodDesc = p.getDesProd();
			                       }


			listaController = new ListaDatosController((LinearLayout)findViewById(R.id.RelativeLayoutText1), this, this, R.id.RelativeLayoutText1);
			listaController.addElement(prodDesc, GuiTools.getMoneyString(p.getSubproducto().get(0).getMonMax().toString()), false);
			++cont;
		}
	}
	
	@Override
	public void onClick(View v) {
		isOnForeground = false;
		
		/*if(v.getId() == R.id.headerRefresh){
			delegate.setRet();
			delegate.redirectToView(OldMenuPrincipalActivity.class);
		}else */if(v.getId() == R.id.simCNomBottomMenuRC){
			delegate.redirectToView(ResumenActivity.class);
		//}else if(v.getId() == R.id.simCNomHeaderLogo){
			//delegate.doRecalculoNC(this);
			
		}
	}
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(OldMenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }

	/**
	 * Created June 8th, 2015,
	 */
	
	private void deleteSimulation()
	{
		delegate.deleteSimulationRequest(this,true);
	}
	
	public void doRecalculo()
	{
		Log.d("Eliminó Simulación", "ok");
		delegate.doRecalculoNC(this);
	}
	
	
}
