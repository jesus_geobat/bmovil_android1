package bbvacredit.bancomer.apicredit.models;

import java.util.List;

public class ListaConvenioPagoServicios {
	
	private String versionCatalogo;
	
	private Integer ocurrencia;
	
	private List<ConvenioPagoServicios> lista;

	public ListaConvenioPagoServicios(String versionCatalogo,
			Integer ocurrencia, List<ConvenioPagoServicios> lista) {
		super();
		this.versionCatalogo = versionCatalogo;
		this.ocurrencia = ocurrencia;
		this.lista = lista;
	}

	public ListaConvenioPagoServicios() {
		// TODO Auto-generated constructor stub
	}

	public String getVersionCatalogo() {
		return versionCatalogo;
	}

	public void setVersionCatalogo(String versionCatalogo) {
		this.versionCatalogo = versionCatalogo;
	}

	public Integer getOcurrencia() {
		return ocurrencia;
	}

	public void setOcurrencia(Integer ocurrencia) {
		this.ocurrencia = ocurrencia;
	}

	public List<ConvenioPagoServicios> getLista() {
		return lista;
	}

	public void setLista(List<ConvenioPagoServicios> lista) {
		this.lista = lista;
	}

}
