package bbvacredit.bancomer.apicredit.controllers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.opengl.EGLExt;
import android.util.Log;

import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import bancomer.api.common.commons.IAutenticacion;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.DelegateBaseOperacion;
import bancomer.api.common.gui.delegates.MainViewHostDelegate;
import bancomer.api.common.model.HostAccounts;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.AbstractSuitePowerManager;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.SuiteViewReleaser;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.gui.activities.BaseActivity;
import bbvacredit.bancomer.apicredit.gui.delegates.BaseDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.MenuPrincipalCreditDelegate;
import bbvacredit.bancomer.apicredit.io.ConnectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.NetworkOperation;
import bbvacredit.bancomer.apicredit.io.ParsingException;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;

import android.app.Fragment;

import com.bancomer.base.callback.CallbackBmovil;


public class MainController {

    private static MainController theInstance = null;

    private ActivityController activityController;
    private Server server;
    private Context context;
    private Vector<BaseActivity> screenStack;
    private String numeroCuenta;
    private CallbackBmovil callbackBmovil;
    private Promociones[] promociones;
    private suitebancomer.aplicaciones.bmovil.classes.model.Account[] acoountBmovil;

    private BaseViewController baseViewController;

    private String operacion;

    public ArrayList<String> estados = new ArrayList<String>();

    private DelegateBaseOperacion delegateOTP;

    private ICommonSession session;

    private IAutenticacion autenticacion;

    private boolean sofTokenStatus;

    private MainViewHostDelegate hostInstance;

    private static DefaultHttpClient clientCredit;

    private Account[] listaCuentas;

    public Account[] getListaCuentas() {
        return listaCuentas;
    }

    public static DefaultHttpClient getClient() {
        return clientCredit;
    }

    public DelegateBaseOperacion getDelegateOTP() {
        return delegateOTP;
    }

    public IAutenticacion getAutenticacion() {
        return autenticacion;
    }

    public boolean isSofTokenStatus() {
        return sofTokenStatus;
    }

    public MainViewHostDelegate getHostInstance() {
        return hostInstance;
    }

    public ICommonSession getSession() {
        return session;
    }

    private static boolean habilitarVista;


    public void setHabilitarVista(boolean habilitarVista) {
        this.habilitarVista = habilitarVista;
    }

    //BBVA INTEGRATION
    private SuiteViewReleaser menuSuiteController;

    /**
     * Pending network operations.
     */
    protected Hashtable<String, NetworkOperation> networkOperationsTable = null;

    /**
     * Reference to a pending network operation, in case that an
     * operation launched a session expired error. This operation
     * will be relaunched after a successful login
     */
    protected NetworkOperation pendingOperation = null;

    /**
     * A progress dialog for long waiting processes.
     */
    protected ProgressDialog mProgressDialog;

    private Fragment flowFragment;

    public Fragment getFlowFragment() {
        return flowFragment;
    }

    public void setFlowFragment(Fragment flowFragment) {
        this.flowFragment = flowFragment;
    }

    public static MainController getInstance(DelegateBaseOperacion delegateOTP, ICommonSession session,IAutenticacion autenticacion,boolean softTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance,Account[] listaCuentas, boolean dev, boolean sim, boolean emulator, boolean log)
    {
        if(theInstance == null)
            theInstance = new MainController(delegateOTP,session,autenticacion,softTokenStatus,client, hostInstance, listaCuentas, dev,  sim,  emulator, log);

        habilitarVista = true;

        return theInstance;
    }

    private MainController(Boolean value){
        setHabilitarVista(value);
    }

    private MainController(DelegateBaseOperacion delegateOTP, ICommonSession session, IAutenticacion autenticacion, boolean sofTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance, Account[] listaCuentas, boolean dev, boolean sim, boolean emulator, boolean log) {
        setServerParams(dev, sim, emulator, log);
        Server.setEnviroment();
        this.delegateOTP = delegateOTP;
        this.session = session;
        this.autenticacion = autenticacion;
        this.sofTokenStatus = sofTokenStatus;
        this.hostInstance = hostInstance;
        clientCredit = client;
        this.listaCuentas = listaCuentas;

        activityController = new ActivityController();
        networkOperationsTable = new Hashtable<String, NetworkOperation>();
        server = new Server();
        screenStack = new Vector<BaseActivity>();

    }

    //public void setServerParams(boolean dev, boolean sim, boolean emulator, boolean log,CommonSession session)
    public void setServerParams(boolean dev, boolean sim, boolean emulator, boolean log)
    {
        Server.DEVELOPMENT = dev;
        Server.SIMULATION = sim;
        Server.EMULATOR = emulator;
        Server.ALLOW_LOG = log;
        //this.session = session;
        //Server.setEnviroment();

    }

    private MainController() {
        activityController = new ActivityController();
        networkOperationsTable = new Hashtable<String, NetworkOperation>();
        server = new Server();
        screenStack = new Vector<BaseActivity>();
    }

    public void setPromociones(Promociones[] promociones){
        this.promociones = promociones;
    }

    public Promociones[] getPromociones(){
        return this.promociones;
    }

    public BaseViewController getBaseViewController() {
        return baseViewController;
    }

    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }

    public static MainController getInstance() {
        if (theInstance == null) {
            theInstance = new MainController();
        }

        return theInstance;
    }

    public Account[] getAcoountBmovil() {
        return acoountBmovil;
    }

    public void setAcoountBmovil(Account[] acoountBmovil) {
        this.acoountBmovil = acoountBmovil;
    }

    public void numCuenta(String numC){
        this.numeroCuenta = numC;
    }

    public String getNumeroCuenta(){
        return this.numeroCuenta;
    }


    public void startCredit(String userNumber,String password,String ium, String user,String mail,String
            operacion)
    {
        this.operacion = operacion;
        MenuPrincipalCreditDelegate delegateMP = new MenuPrincipalCreditDelegate();
        delegateMP.setSessionByBmovil(userNumber, password, ium, user, mail, operacion);
        Tools.getCurrentSession().setIsClearOnly(true);
        delegateMP.doCalculo();

    }

    public String getOperacion(){
        return this.operacion;
    }

    public void startCredit(String userNumber,String password,String ium, String user,String mail)
    {
        MenuPrincipalCreditDelegate delegateMP = new MenuPrincipalCreditDelegate();
        delegateMP.setSessionByBmovil(userNumber, password, ium, user, mail, "");
        Tools.getCurrentSession().setIsClearOnly(true);
        delegateMP.doCalculo();

    }

    public void setCurrentActivity(BaseActivity currentActivity) {
        screenStack.add(currentActivity);
        activityController.setCurrentActivity(currentActivity);
    }

    public void goBack() {
        if (screenStack.size() > 0) {
            screenStack.removeElementAt(screenStack.size() - 1);
        }

        if (screenStack.size() > 0) {
            activityController.setCurrentActivity(screenStack.elementAt(screenStack.size() - 1));
        } else {
            activityController.setCurrentActivity(null);
        }
    }

    public void showScreen(Class<?> activity) {
        activityController.showScreen(activity);
    }


    /**
     * These methods are going to  be used to call methods from suite Bancomer.
     * */

    public void setMenuSuiteController(SuiteViewReleaser controller)
    {
        menuSuiteController=controller;
    }

    public SuiteViewReleaser getMenuSuiteController()
    {
        return menuSuiteController;
    }



    //End of region.


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Server getServer() {
        return server;
    }

    public ActivityController getActivityController() {
        return activityController;
    }

    /**
     * Determines if the requested operation is not being called already
     * @param opId the operation id
     * @return true if the operation isn't already waiting for a response
     */
    private boolean isNotAlreadyCalling(String opId){
        return !this.networkOperationsTable.containsKey(opId);
    }

    /**
     * Places a progress dialog, for long waiting processes.
     *
     * @param strTitle the dialog title
     * @param strMessage the message to show in the while
     */
    public void muestraIndicadorActividad(String strTitle, String strMessage) {
        if(mProgressDialog != null){
            ocultaIndicadorActividad();
        }
//		mProgressDialog = ProgressDialog.show(getContext(), strTitle, 
//    			strMessage, true);
        mProgressDialog = ProgressDialog.show(this.getActivityController().getCurrentActivity(), strTitle, strMessage, true);
        mProgressDialog.setCancelable(false);
    }

    /**
     * Hides the progress dialog.
     */
    public void ocultaIndicadorActividad() {
        if(mProgressDialog != null){
            mProgressDialog.dismiss();
        }
    }

	/**
	 * metodo para llamar el api server
	 * @param operationId
	 * @param params
	 * @param caller
	 * @param progressLabel
	 * @param progressMessage
	 * @param isJson
	 * @param hadler
	 * @param contex
	 */
	public void invokeNetworkOperation(final String operationId, final Hashtable<String, ?> params,
			  final BaseDelegate caller, 
			  String progressLabel, 
			  String progressMessage,
		   final Boolean isJson, final Object hadler, final Context contex) {
              
		
		Log.e("Código en invoke", operationId);
             		
		if (isNotAlreadyCalling(operationId)) {

			try {

				final NetworkOperation operation = new NetworkOperation(operationId, params, caller);
				this.networkOperationsTable.put(operationId, operation);
				this.pendingOperation = operation;
				
				if (caller != null) {
					if(AbstractSuitePowerManager.getSuitePowerManager().isScreenOn())
						// TODO: Fix para no bloquear dos veces el login. Corregir.
						if(!operationId.equals(Server.CONSULTA_ALTERNATIVAS)) muestraIndicadorActividad(progressLabel, progressMessage);
					else
						Log.d(this.getClass().getSimpleName(), "La aplicaci�n estaba bloqueada.");
				}

				Thread thread = new Thread(new Runnable() {
					public void run() {
						try {
							//ServerResponseCredit response = getServer().doNetworkOperationCredit(operationId, params);
                            Log.i("efi","oppID: "+operationId);
							int opId=0;
							if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId) || Server.CALCULO_OPERACION.equals(operationId) ){
								opId=Server.CALCULO_ALTERNATIVAS_OPERACION_ID;
							}/*else if(Server.CONSULTA_CORREO_OPERACION.equals(operationId)){
								opId=Server.CONSULTA_CORREO_OPERACION_ID;
							}*/else if(Server.ENVIO_CORREO_OPERACION.equals(operationId)){
								opId=Server.ENVIO_CORREO_OPERACION_ID;
							}else if(Server.CONSULTA_ALTERNATIVAS.equals(operationId)){
								opId=Server.CONSULTA_ALTERNATIVAS_ID;
							}else if(Server.CONSULTA_TDC.equals(operationId)){
								opId=Server.CONSULTA_TDC_ID;
							}else if(Server.DETALLE_ALTERNATIVA.equals(operationId)){
								opId=Server.DETALLE_ALTERNATIVA_ID;
							}else if(Server.CONTRATA_ALTERNATIVA_CONSUMO.equals(operationId)){
								opId=Server.CONTRATA_ALTERNATIVA_CONSUMO_ID;
							}else if(Server.DETALLE_ALTERNATIVA_TDC.equals(operationId)){
                                opId = Server.DETALLE_ALTERNATIVA_TDC_ID;
                            }else if(Server.CONSULTA_TABLA_AMORTIZACION.equals(operationId)){
                                opId = Server.TABLA_AMORTIZACION;
                            }else if(Server.ENVIO_CORREO_HIPO_AUTO.equals(operationId)){
                                opId = Server.ENVIO_CORREO_HIPO_AUTO_OP;
                            }else if(Server.ENVIO_CORREO_TDC_ILC.equals(operationId)){
                                opId = Server.ENVIO_CORREO_TDC_ILC_OP;
                            }else if(Server.ENVIO_CORREO_CONSUMO.equals(operationId)){
                                opId = Server.ENVIO_CORREO_CONSUMO_OP;
                            }/*else if (Server.CONSULTA_DETALLE_EFI.equals(operationId)){
                                opId = Server.CONSULTA_DETALLE_OFERTA_EFI;
                            }*/ else if (Server.CONSULTA_DETALLE_ILC.equals(operationId)){
                                opId = Server.CONSULTA_DETALLE_OFERTA_ILC;
                            }else if (Server.CONSULTA_DETALLE_TDC.equals(operationId)){
                                opId = Server.CONSULTA_DETALLE_OFERTA_TDC;
                            }else if (Server.CONSULTA_DETALLE_CONSUMO.equals(operationId)){
                                opId = Server.CONSULTA_DETALLE_OFERTA_CONSUMO;
                            }/*else if (Server.MODIFICAR_IMPORTE_EFI.equals(operationId)){
                                opId = Server.SIMULADOR_EFI;
                            }else if (Server.ACEPTA_EFI.equals(operationId)){
                                Log.i("efi","se asigna op 76");
                                opId = Server.ACEPTACION_OFERTA_EFI;
                            }*/

                            Log.i("efi","opId "+opId);

							ServerResponse responseNew=null;
							ParametersTO parameters=new ParametersTO();
							parameters.setSimulation(Server.SIMULATION);
							parameters.setDevelopment(Server.DEVELOPMENT);
							parameters.setOperationId(opId);
							parameters.setParameters(params.clone());
							parameters.setJson(isJson);
							IResponseService resultado = new ResponseServiceImpl() ;
							//Convirtiendo el Enum al esperado por API SERVER

                            suitebancomer.aplicaciones.commservice.commons.ApiConstants.isJsonValueCode isJsonTransformValue;

                            //se cambia tipo de operación para que sea consumo
                            if (operationId == Server.CONSULTA_DETALLE_CONSUMO){
                                isJsonTransformValue = ApiConstants.isJsonValueCode.CONSUMO; //SE CAMBIA PARA CONSUMO Y LAS LANZA POR CONSUMO

                            }else if (//operationId == Server.CONSULTA_DETALLE_EFI ||
                                    operationId == Server.CONSULTA_DETALLE_ILC /*||
                                    operationId == Server.MODIFICAR_IMPORTE_EFI ||
                                    operationId == Server.ACEPTA_EFI ||
                                    operationId == Server.CANCELA_EFI*/
                                    ){
                                isJsonTransformValue = ApiConstants.isJsonValueCode.ONECLICK; //SE CAMBIA PARA CONSUMO Y LAS LANZA POR CONSUMO

                            }else {
                                isJsonTransformValue = ApiConstants.isJsonValueCode.NONE; //SE CAMBIA PARA CONSUMO Y LAS LANZA POR CONSUMO
                            }

							Hashtable<String, ?> parameters2 = ((Hashtable<String, ?>)parameters.getParameters());
							ConnectionFactoryCredit cf= new ConnectionFactoryCredit(parameters.getOperationId(), parameters2, parameters.isJson(), hadler,contex,isJsonTransformValue);
							resultado=cf.processConnectionWithParams();
							responseNew=cf.parserConnection(resultado, hadler);

							returnFromNetworkOperation(operationId, responseNew, caller, null);
						} catch (Throwable t) {
							returnFromNetworkOperation(operationId, null, caller, t);
						}
					}

				});

				// Starts the worker thread to perform network operation
				thread.start();

			} catch (Throwable th) {
				returnFromNetworkOperation(operationId, null, caller, th);
			}

		} else {
			// unable to process request, show error and return
			return;
		}
	}

	/**
	 * Invoke a network operation. It shows a popup indicating progress with
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 */
	public void invokeNetworkOperation(final String operationId, final Hashtable<String, ?> params,
			  final BaseDelegate caller,
			  String progressLabel,
			  String progressMessage) {


        Log.e("Código en invoke", operationId);

        if (isNotAlreadyCalling(operationId)) {

            try {

                NetworkOperation operation = new NetworkOperation(operationId, params, caller);
                this.networkOperationsTable.put(operationId, operation);
                this.pendingOperation = operation;

                if (caller != null) {
                    if(AbstractSuitePowerManager.getSuitePowerManager().isScreenOn())
                        // TODO: Fix para no bloquear dos veces el login. Corregir.
                        if(!operationId.equals(Server.CONSULTA_ALTERNATIVAS))
                            muestraIndicadorActividad(progressLabel, progressMessage);
                        else
                            Log.d(this.getClass().getSimpleName(), "La aplicaci�n estaba bloqueada.");
                }

                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        try {
                            ServerResponse response = getServer().doNetworkOperation(operationId, params);
                            returnFromNetworkOperation(operationId, response, caller, null);
                        } catch (Throwable t) {
                            returnFromNetworkOperation(operationId, null, caller, t);
                        }
                    }

                });

                // Starts the worker thread to perform network operation
                thread.start();

            } catch (Throwable th) {
                returnFromNetworkOperation(operationId, null, caller, th);
            }

        } else {
            // unable to process request, show error and return
            return;
        }
    }

    /**
     * Called internally after a network operation has returned. It ends the progress
     * popup and invokes method to analyze the result
     * @param operationId network operation identifier. See Server class.
     * @param response the ServerResponse instance returned from the server.
     * @param throwable the throwable received.
     * @param callerHandlesError boolean flag indicating wheter the caller is capable
     * of handling an error message from the server or method should handle in the default
     * implementation
     */
    protected void returnFromNetworkOperation(final String operationId,
                                              final ServerResponse response,
                                              final BaseDelegate caller,
                                              final Throwable throwable) {

        if (caller != null) {
            //TODO: Fix para no bloquear dos veces el login. Corregir.
            if (operationId.equals(Server.CONSULTA_ALTERNATIVAS)) {
                ocultaIndicadorActividad();
                //Pending
                // MainController.getInstance().getMenuSuiteController().habilitarVista();
            } else if(!operationId.equals(Server.LOGIN_OPERACION)) ocultaIndicadorActividad();
            getActivityController().getCurrentActivity().runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        analyzeNetworkResponse(operationId, response, throwable, caller);
                    } catch (Throwable t) {
                        Log.d(this.getClass().getSimpleName(), "Exception on analyzeNetworkResponse.", t);
                        if (caller != null) {
                            String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
                            getActivityController().getCurrentActivity().showErrorMessage(message);
                        }
                    }
                }
            });
        } else { //TODO session timer expired, go to login screen
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    try {
                        analyzeNetworkResponse(operationId, response, throwable, null);
                    } catch (Throwable t) {
                        String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
                        Log.d(this.getClass().getSimpleName() + " :: returnFromNetworkOperation", message);
                    }
                }
            });
            thread.run();
        }
    }

    /**
     * Get the operation error message
     * @param throwable the throwable
     * @return the operation error message
     */
    private String getErrorMessage(Throwable throwable) {
        StringBuffer sb = new StringBuffer();
        if (throwable != null) {
            if (throwable instanceof NumberFormatException) {
                sb.append(this.getContext().getString(R.string.error_format));
            } else if (throwable instanceof ParsingException) {
                sb.append(this.getContext().getString(R.string.error_format));
            } else if (throwable instanceof IOException) {
                sb.append(this.getContext().getString(R.string.error_communications));
            } else {
                sb.append(this.getContext().getString(R.string.error_communications));
            }
        }
        return sb.toString();
    }

    /**
     * Analyze the network response obtained from the server
     * @param operationId network identifier. See Server class.
     * @param response the ServerResponse instance built from the server response. It
     * contains the type of result (success, failure), and the real content for
     * the application business.
     * @param operationId network operation identifier. See Server class.
     * @param response the ServerResponse instance returned from the server.
     * @param throwable the throwable received.
     * @param callerHandlesError boolean flag indicating wheter the caller is capable
     * of handling an error message or the network flux should handle in the default
     * implementation
     */
    protected void analyzeNetworkResponse(String operationId,
                                          ServerResponse response,
                                          Throwable throwable,
                                          final BaseDelegate caller) {
        // get the caller
        NetworkOperation operation = this.networkOperationsTable.get(operationId);
        if (operation != null) {
            // remove the operation id from the table
            this.networkOperationsTable.remove(operationId);

            if (operation.isActive()) {
                if (response != null) {

                    BaseDelegate baseDelegate = operation.getCaller();
                    int resultCode = response.getStatus();
                    android.util.Log.i("mensajes","MainController, analyzeError, resultCode: "+resultCode);
                    switch (resultCode) {
                        case ServerResponse.OPERATION_SUCCESSFUL:
                        case ServerResponse.OPERATION_WARNING:
                            baseDelegate.analyzeResponse(operationId, response);

                            break;
                        case ServerResponse.OPERATION_OPTIONAL_UPDATE:
                            baseDelegate.analyzeResponse(operationId, response);
                            break;
                        case ServerResponse.OPERATION_ERROR:
                            this.pendingOperation = null;
                            String errorCode = response.getMessageCode();

                            android.util.Log.i("mensajes","MainController, analyzeError, errorCode: "+errorCode);

//						if ((operationError == Server.LOGIN_OPERATION) && (Constants.DEACTIVATION_ERROR_CODE.equals(errorCode))) {
//							Session session = Session.getInstance(SuiteApp.appContext);
//							String username = session.getUsername();
//							String password = session.getPassword();
//							Session.getInstance(suiteApp.getApplicationContext()).setValidity(Session.UNSET_STATUS);
//
//							Bundle params = new Bundle();
//							params.putString(Server.USERNAME_PARAM, username);
//							params.putString(Server.PASSWORD_PARAM, password);
//
//						} else {
                            //valida, si hay ofertas pero no hay alternativas, mostrará sólo las alternativas sin mensajes de error
                            //Si tiene una alternativa, y al limpiar (opcion 4) se genera un error, se debe de mostrar el mensaje de error.
                            if((getPromociones()!= null || getPromociones().length>0) && Tools.getCurrentSession().getCreditos()==null){
                                Log.i("mensajes", "Si hay promociones");
                                baseDelegate.analyzeResponse(operationId, response);

                            }else if (this.getActivityController().getCurrentActivity(). getLocalClassName().equals(ConstantsCredit.CLASS_START_BMOVIL_IN_BACK)){
                                this.getActivityController().getCurrentActivity().showErrorMessage(errorCode + "\n" + response.getMessageText(), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getActivityController().getCurrentActivity().finish();
                                        System.exit(0);
                                    }
                                });
                            }
                            else {
                                this.getActivityController().getCurrentActivity().showErrorMessage(errorCode + "\n" + response.getMessageText());
                                baseDelegate.analyzeResponse(operationId, response);
                            }//						}
                            break;
                        case ServerResponse.OPERATION_SESSION_EXPIRED:
                            this.pendingOperation = operation;
//						Session.getInstance(this.getContext()).setValidity(Session.INVALID_STATUS);
//						suiteApp.getSuiteViewsController().showMenuSuite(true);
                            break;
                        default:
                            ocultaIndicadorActividad();
                            this.getActivityController().getCurrentActivity().showErrorMessage(R.string.error_format);
                            break;
                    }
                } else {
                    Log.i("mensajes","else switch errores");
                    if (throwable != null) {
                        ocultaIndicadorActividad();
                        //this.getActivityController().getCurrentActivity().showErrorMessage("error");
                        this.getActivityController().getCurrentActivity().showErrorMessage(getErrorMessage(throwable));
                    } else {
                        ocultaIndicadorActividad();
                        this.getActivityController().getCurrentActivity().showErrorMessage(R.string.error_communications);
                    }
                    if(habilitarVista) {
                        MainController.getInstance().getMenuSuiteController().habilitarVista();
                        theInstance = null;

                    }

                }
            }
        } else {
            Log.i("mensajes","else operation is active");
            ocultaIndicadorActividad();
            this.getActivityController().getCurrentActivity().showErrorMessage(R.string.error_unexpected);
            if(habilitarVista) {
                MainController.getInstance().getMenuSuiteController().habilitarVista();
                theInstance = null;
            }
        }
    }

    public static void resetInstace(){theInstance = null;}

    public CallbackBmovil getCallbackBmovil() {
        return callbackBmovil;
    }

    public void setCallbackBmovil(CallbackBmovil callbackBmovil) {
        this.callbackBmovil = callbackBmovil;
    }

}


