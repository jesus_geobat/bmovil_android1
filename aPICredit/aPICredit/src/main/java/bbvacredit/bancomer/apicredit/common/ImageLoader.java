package bbvacredit.bancomer.apicredit.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.os.Handler;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;



public class ImageLoader {
    
//	final int stub_id = R.drawable.ic_launcher;
    static MemoryCache memoryCache = new MemoryCache();
    static FileCache fileCache;
    
    private static Map<ImageView, String> imageViews=Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    ExecutorService executorService;
    Handler handler=new Handler();//handler to display images in UI thread
    
    public ImageLoader(Context context){
        fileCache=new FileCache(context);
        executorService=Executors.newFixedThreadPool(5);
    }
    
    
    public void DisplayImage(String url, ImageView imageView)
    {
        imageViews.put(imageView, url);
        Bitmap bitmap=memoryCache.get(url);
        if(bitmap!=null){
            imageView.setImageBitmap(bitmap);
        	
        }else{
        	
        	try{
	            queuePhoto(url, imageView);
	        	}
        		catch(OutOfMemoryError ex){
        			Log.w("Error","display out: "+ ex);
        		}
        		catch(RuntimeException re){
        			Log.w("Error","display run: "+ re);
        		}
        }      
    }
        
    private void queuePhoto(String url, ImageView imageView)
    {
    	try{
        PhotoToLoad p=new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p));
    	}
    	catch(OutOfMemoryError ex){
    		Log.w("Error","queuePhoto out: "+ ex);
    	}
    	catch(RuntimeException re){
    		Log.w("Error","queuePhoto run: "+ re);
    	}
    }
    
    private Bitmap getBitmap(String url) 
    {
        File f=fileCache.getFile(url);
        
        Bitmap b = decodeFile(f);
        if(b!=null)
            return b;
        
        Bitmap bitmap = null;
        URL imageUrl = null;
        HttpURLConnection conn = null;
        InputStream is = null;
        OutputStream os = null;
        
        try {    
            imageUrl = new URL(url);
            conn = (HttpURLConnection)imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            is = conn.getInputStream();
            os = new FileOutputStream(f);
            CopyStream(is, os);
            os.close();
            conn.disconnect();
            bitmap = decodeFile(f);
            memoryCache.clear();
            return bitmap;
        } catch (Throwable ex){
           if(ex instanceof OutOfMemoryError)
               memoryCache.clear();
           
           System.gc();
           return null;
        }
        finally
        {
        	if(conn != null){
        		conn.disconnect();
        		conn = null;
        	}
        	
        	if(is != null)
        	{
        		try {
					is.close();
					is = null;
				} catch (IOException e) {
					
				}
        	}
        	
        	if(os != null)
        	{
        		try {
					os.close();
					os = null;
				} catch (IOException e) {
					
				}
        	}
        }
        
    }

    private Bitmap decodeFile(File f){
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            
            FileInputStream stream1=new FileInputStream(f);
            BitmapFactory.decodeStream(stream1,null,o);
            stream1.close();
            
            stream1 = null;
            
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = 1;
            
            FileInputStream stream2=new FileInputStream(f);
            Bitmap bitmap=BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            stream2 = null;
            
            return bitmap;
        } catch (FileNotFoundException e) {
        } 
        catch (IOException e) {
        	System.gc();
            return null;
        }catch (OutOfMemoryError E) {
        	System.gc();
            return null;          
        }
        return null;
    }
    
    private class PhotoToLoad
    {
        public String url;
        public ImageView imageView;
        public PhotoToLoad(String u, ImageView i){
            url=u; 
            imageView=i;
        }
    }
    
    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;
        PhotosLoader(PhotoToLoad photoToLoad){
            this.photoToLoad=photoToLoad;
        }
        
        @Override
        public void run() {
            try{
                if(imageViewReused(photoToLoad))
                    return;
                Bitmap bmp=getBitmap(photoToLoad.url);
                memoryCache.put(photoToLoad.url, bmp);
                if(imageViewReused(photoToLoad))
                    return;
                BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad);
                handler.post(bd);
            }catch(Throwable th){
            }
        }
    }
    
    boolean imageViewReused(PhotoToLoad photoToLoad) throws IOException{
    	String tag=imageViews.get(photoToLoad.imageView);
        if(tag==null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }
    
    class BitmapDisplayer implements Runnable
    {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;
        public BitmapDisplayer(Bitmap b, PhotoToLoad p){bitmap=b;photoToLoad=p;}
        public void run()
        {
            try {
				if(imageViewReused(photoToLoad))
				    return;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            if(bitmap!=null)
                photoToLoad.imageView.setImageBitmap(bitmap);
        }
    }

    public void clearCache() {
    	imageViews.clear();
        memoryCache.clear();
        fileCache.clear();
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (;;) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }
}
