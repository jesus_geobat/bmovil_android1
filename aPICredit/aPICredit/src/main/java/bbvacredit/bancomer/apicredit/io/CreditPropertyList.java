package bbvacredit.bancomer.apicredit.io;

import android.util.Log;

import java.util.ResourceBundle;

public class CreditPropertyList {
	public String read(String propertyToRecover)
    {
		ResourceBundle myResources = ResourceBundle.getBundle("bbvacredit.io.creditPropertiesFile");
		String prop = myResources.getString(propertyToRecover);
		Log.w("DATOS PROPERTY", prop);
		return prop;
    }
}