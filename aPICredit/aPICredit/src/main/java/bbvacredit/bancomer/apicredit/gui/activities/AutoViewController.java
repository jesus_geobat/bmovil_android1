package bbvacredit.bancomer.apicredit.gui.activities;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.LabelMontoTotalCredit;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.AutoDelegate;
import bbvacredit.bancomer.apicredit.models.Plazo;
import bbvacredit.bancomer.apicredit.models.Producto;
import bbvacredit.bancomer.apicredit.models.Subproducto;
import tracking.TrackingHelperCredit;

public class AutoViewController extends Fragment {

// ----- SeekArc Components ------ //

    private SeekArc mSeekArc;
    private LabelMontoTotalCredit lblMontoTotalCredito;
    private String monto;


// ------- Layout Components -------//

    private TextView txtPlazo;
    private TextView txtOpcion;
    private ImageView imgCredito;
    private ImageButton btnPlazo;
    private ImageButton btnSimular;
    private ImageButton btnOpcion;

    private DetalleAutoViewController detalleAuto;

// ------- Data Components -------//
    private String plazo;
    private String termCreditList[];
    private String termCreditLisNew[];
    private String termCreditListUsed[];
    private Subproducto auxiliarOptionList[];
    private Producto producto;
    private String optionList[];
    private String option;
    private boolean isListSet=false;
    private AutoDelegate delegate;

    private boolean editable;
    private DecimalFormat df;
    private DecimalFormat dfnd;

    private String amountString=null;
    private StringBuffer typedString= new StringBuffer();
    private boolean isSettingText = false;
    private boolean isResetting = false;
    private boolean mAcceptCents=false;
    private boolean flag=true;

    private String montMax;
    private String montMin;

    public boolean buscarPlazo = true;
    public boolean buscarOpcion = false;

    private Producto productoAux;

    public DialogInterface oDialog;

    public String cvePlazo;
    public String cveSubProd;

    public String getCvePlazo() {
        return cvePlazo;
    }

    public void setCvePlazo(String cvePlazo) {
        this.cvePlazo = cvePlazo;
    }

    public String getCveSubProd() {
        return cveSubProd;
    }

    public void setCveSubProd(String cveSubProd) {
        this.cveSubProd = cveSubProd;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Producto getProducto() {
        return producto;
    }

    public String getMonto() {
        return monto;
    }

    public String getOption() {
        return option;
    }

    public String getPlazo() {
        return plazo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realizaAccionInicio();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        delegate = new AutoDelegate(this);

        montMax = getProducto().getMontoMaxS();
        montMin = getProducto().getMontoMinS();

        final View rootView = inflater.inflate(R.layout.circule_progress_bar_layout, container, false);

        if(rootView!=null){
            findViews(rootView);

            lblMontoTotalCredito.setMontoMin(montMin);
            lblMontoTotalCredito.setMontoMax(montMax);
            lblMontoTotalCredito.setLongClickable(false);

            lblMontoTotalCredito.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    reset();
                    mAcceptCents = true;
                    isSettingText = true;
                    lblMontoTotalCredito.setText("");
                    return false;
                }
            });

            lblMontoTotalCredito.addTextChangedListener(new TextWatcher() {

                /**
                 * Save the value of string before changing the text
                 */
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    String amountField = lblMontoTotalCredito.getText().toString();

                    if(amountField.equals(","))
                        amountField.replace(",","");
                    if(amountField.equals("."))
                        amountField.replace(".","");

                    if (!isSettingText) {
                        amountString = amountField;
                    }
                }

                /**
                 * When the value string has changed
                 */
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String amountField = lblMontoTotalCredito.getText().toString();
                    Log.i("eliminar","CharSequence: "+s);

                    if(s.toString().equals(".")) {
                        Log.w("eliminar", "se ingresó un un punto, eliminalo, toS");
                        lblMontoTotalCredito.setText("");
                    }else {
                        Log.w("eliminar", "Se ingresó bien el número");

                        if (!isSettingText && !isResetting) {
                            if (!flag) {
                                String aux;
                                try {
                                    aux = lblMontoTotalCredito.getText().toString().substring(0, lblMontoTotalCredito.length() - 3);
                                    typedString = new StringBuffer();

                                    if (aux.charAt(0) == '0')
                                        aux = aux.substring(1, aux.length());
                                } catch (Exception ex) {
                                    aux = "";

                                }
                                for (int i = 0; i < aux.length(); i++) {
                                    if (aux.charAt(i) != ',')
                                        typedString.append(aux.charAt(i));
                                }
                                flag = true;

                                setFormattedText();
                                amountField = lblMontoTotalCredito.getText().toString();
                                amountString = amountField;
                            } else {

                                try {
                                    if (lblMontoTotalCredito.length() < amountString.length()) {
                                        reset();
                                    } else if (lblMontoTotalCredito.length() > amountString.length()) {

                                        int newCharIndex = lblMontoTotalCredito.getSelectionEnd() - 1;
                                        //there was no selection in the field
                                        if (newCharIndex == -2) {
                                            newCharIndex = lblMontoTotalCredito.length() - 1;
                                        }

                                        char num = amountField.charAt(newCharIndex);
                                        if (!(num == '0' && typedString.length() == 0)) {
                                            typedString.append(num);
                                        }
                                        setFormattedText();
                                    }
                                } catch (StringIndexOutOfBoundsException ex) {
                                    //                                ex.printStackTrace();
                                }
                            }
                        }
                    }
                }

                public void afterTextChanged(Editable s) {
                    isSettingText = false;
                    lblMontoTotalCredito.setSelection(lblMontoTotalCredito.length());
                }
            });

            lblMontoTotalCredito.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View view, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        InputMethodManager manager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (manager != null) {
                            manager.hideSoftInputFromWindow(view.getWindowToken(), 0);


                            String auxMon = lblMontoTotalCredito.getText().toString();
                            String respuesta = "";

                            if (lblMontoTotalCredito.getText().toString().equals("")) {
                                lblMontoTotalCredito.setText(Tools.formatAmount(Tools.validateValueMonto(montMin), false));
                                actualizaGrafica(montMin);
                            } else {
                                respuesta = Tools.validateMont(lblMontoTotalCredito.getMontoMax(), lblMontoTotalCredito.getMontoMin(), auxMon, null);
                                try {
                                    if (respuesta.equals(ConstantsCredit.MONTO_MAYOR)) {
                                        lblMontoTotalCredito.setText(Tools.formatAmount(Tools.validateValueMonto(montMax), false));
                                        actualizaGrafica(montMax);
                                    } else if (respuesta.equals(ConstantsCredit.MONTO_MENOR)) {
                                        lblMontoTotalCredito.setText(Tools.formatAmount(Tools.validateValueMonto(montMin), false));
                                        actualizaGrafica(montMin);
                                    } else if (respuesta.equals(ConstantsCredit.MONTO_EN_RANGO)) {
                                        String auxLbl = lblMontoTotalCredito.getText().toString();
                                        if (!auxLbl.startsWith("$"))
                                            lblMontoTotalCredito.setText("$" + auxLbl);

                                        actualizaGrafica(auxLbl);
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    }

                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        if (lblMontoTotalCredito.getText().length() > 3 && (isFlag())) {
                            lblMontoTotalCredito.setSelection(lblMontoTotalCredito.length() - 3);
                            setFlag(false);
                        }
                    }
                    return false;
                }
            });

            mSeekArc.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekArc seekArc) {
                }

                @Override
                public void onStartTrackingTouch(SeekArc seekArc) {
                }

                @Override
                public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
                    monto = String.valueOf(progress);
                    Log.i("plazo", "monto onProgress: " + monto);
                    lblMontoTotalCredito.setText(Tools.formatAmount(monto, false));
                    lblMontoTotalCredito.setSelection(monto.length());
                    getActivity().findViewById(R.id.mainContainer).setVisibility(View.VISIBLE);
                    editable = false;

                    InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (manager != null)
                        manager.hideSoftInputFromWindow(lblMontoTotalCredito.getWindowToken(), 0);

                    lblMontoTotalCredito.setCursorVisible(false);
                }
            });

            btnPlazo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    pickerPlazo();
                }
            });

            txtPlazo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pickerPlazo();
                }
            });

            btnOpcion.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    pickerOpcion();
                }
            });

            txtOpcion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pickerOpcion();
                }
            });

            btnSimular.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    if (lblMontoTotalCredito.getText().toString().equals("")) {
                        Tools.showAlert("Favor de ingresar un monto", getActivity());
                    } else {
                        Log.i("monto", "valor respaldado de gráfica: " + lblMontoTotalCredito.getText().toString());

                        setCvePlazo(txtPlazo.getText().toString());
                        setCveSubProd(txtOpcion.getText().toString());

                        delegate.doRecalculo();
                    }
                }
            });
        }
        return  rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadLists();

        String snMax = getProducto().getMontoMaxS();
        String snMin = getProducto().getMontoMinS();

        if(snMax.endsWith(".00"))
            snMax = snMax.replace(".00","");
        if(snMax.contains(","))
            snMax = snMax.replace(",","");
        if(snMin.endsWith(".00"))
            snMin = snMin.replace(".00","");
        if(snMin.contains(","))
            snMin = snMin.replace(",", "");

        mSeekArc.mMax = Integer.parseInt(snMax);
        mSeekArc.mMin = Integer.parseInt(snMin);
    }

    private void updateTermList(String code) {
        for(Subproducto sb : auxiliarOptionList) {

            if (sb.getDesSubp().equals(code)) {

                //if(sb.getDesSubp().equalsIgnoreCase(ConstantsCredit.NUEVO_LBL))
                //{
                    termCreditList = termCreditLisNew;
                    plazo = termCreditLisNew.length>0? termCreditLisNew[0]:"";

                /*}
                else if(sb.getDesSubp().equalsIgnoreCase(ConstantsCredit.SEMI_LBL))
                {
                    termCreditList = termCreditListUsed;
                    plazo = termCreditListUsed.length>0? termCreditListUsed[0]:"";

                }*/
                txtPlazo.setText(plazo);
                break;
            }

        }
    }

    private void loadLists(){

        int counter=0;
        optionList = new String[producto.getSubproducto().size()];

        auxiliarOptionList = new Subproducto[producto.getSubproducto().size()];

        for(Subproducto sb : producto.getSubproducto()) {
            this.productoAux = producto;

            optionList[counter] = sb.getDesSubp();
            auxiliarOptionList[counter] = sb;

            //JQH Oculta botón listar plazos
            if (sb.getPlazo().size()<=1)
                btnPlazo.setVisibility(View.GONE);


            //if(sb.getDesSubp().equalsIgnoreCase(ConstantsCredit.NUEVO_LBL)){
                termCreditLisNew = new String[sb.getPlazo().size()];
                int i=0;

                for(Plazo plazo : sb.getPlazo()) {
                    termCreditLisNew[i]=plazo.getDesPlazo();
                    i++;
                }

                if(!isListSet) {
                    option = optionList[counter];
                    plazo = termCreditLisNew.length>0? termCreditLisNew[0]:"";
                    isListSet=true;
                    termCreditList= termCreditLisNew;
                }
            counter++;
        }

        Log.w("plazo", "getIndSim: " + producto.getIndSim());


//        if(producto.getIndSim().equals("S")){ //si fue actualizada toma los valores guardados de respaldo
            //busca por clave y setea valores correctos
        txtOpcion.setText(producto.getDessubpE());
        txtPlazo.setText(producto.getDesPlazoE());
        Log.w("plazo", "getSubprodE: " + producto.getSubprodE());
        Log.w("plazo", "getDesPlazoE: " + producto.getDesPlazoE());

        Log.w("plazo", "option: " + option);
        Log.w("plazo", "plazo: " + plazo);
        Log.w("plazo", "");


    }

    private void findViews(View rootView){
        mSeekArc = (SeekArc) rootView.findViewById(R.id.seekArc);
        lblMontoTotalCredito = (LabelMontoTotalCredit) rootView.findViewById(R.id.lblMontoTotalCredito);
        txtPlazo = (TextView) rootView.findViewById(R.id.txtPlazo);
        btnPlazo = (ImageButton) rootView.findViewById(R.id.btnPlazo);
        btnSimular = (ImageButton) rootView.findViewById(R.id.btn_simular);
        imgCredito = (ImageView) rootView.findViewById(R.id.imgCredito);
        btnOpcion = (ImageButton) rootView.findViewById(R.id.btnOpcion);
        txtOpcion = (TextView) rootView.findViewById(R.id.txtOpcion);


        rootView.findViewById(R.id.txtTarjeta).setVisibility(View.GONE);
        rootView.findViewById(R.id.txtTerminacion).setVisibility(View.GONE);

        imgCredito.setImageResource(R.drawable.ic_prestamoautoi);

        seekArcSettlement();
    }

    private void seekArcSettlement() {
        mSeekArc.setArcRotation(345);
        mSeekArc.setSweepAngle(330);
        mSeekArc.setArcWidth(40);
        mSeekArc.setProgressWidth(20);

        monto = String.valueOf(mSeekArc.mMax);
        Log.i("plazo", "monto seekArcSettL: " + monto);
        Log.i("plazo", "monto seekArcSettL. getMontoMax: " + montMax);
//        String auxMonMax = montMax;
//
//        if(auxMonMax.endsWith(".00"))
//            auxMonMax = auxMonMax.replace(".00","");
//        if(auxMonMax.contains(","))
//            auxMonMax = auxMonMax.replace(",","");

        lblMontoTotalCredito.setText(Tools.formatAmount(monto, false));
        mSeekArc.updateProgressText(Integer.parseInt(monto));
        Log.i("fragment", "seteo valor gráfica: " + monto);
        lblMontoTotalCredito.setSelection(String.valueOf(String.valueOf(monto)).length());
    }

    public void setSimulacionGrafica(String monto){

        lblMontoTotalCredito.setText(monto);
        actualizaGrafica(monto);
    }

    /**
     *
     * @param value valor a buscar, puede ser desPlazo o desSub (12 meses, nuevo)
     * @param cvePlazo si indica true buscará la clave de plazo, si indica false buscará la clave desSub
     * @return regresa la clave
     */
    public String buscarClave(String desSub, String value, boolean cvePlazo){
        Producto aux = productoAux;
        String clave = "";

        for(Subproducto auxSP : aux.getSubproducto()) {

            if(desSub.equals(auxSP.getDesSubp())) {
                if (cvePlazo) { //busca plazo
                    for (int i = 0; i < auxSP.getPlazo().size(); i++) {
                        if (value.equals(auxSP.getPlazo().get(i).getDesPlazo())) {
                            clave = auxSP.getPlazo().get(i).getCvePlazo();
                        }
                    }
                }else{ //busca opcion
                    clave = auxSP.getCveSubp();
                }
            }
        }

        return clave;
    }

    private void pickerPlazo(){
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View DialogView = inflater.inflate(R.layout.activity_credit_picker, null);

        final NumberPicker dialogPicker = (NumberPicker)DialogView.findViewById(R.id.dialogPicker);
        dialogPicker.setMinValue(0);
        dialogPicker.setMaxValue(termCreditList.length - 1);
        dialogPicker.setDisplayedValues(termCreditList);
        dialogPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        dialogPicker.setValue(getPosition(termCreditList, txtPlazo.getText().toString()));
        dialogPicker.setWrapSelectorWheel(false);

        new AlertDialog.Builder(getActivity()).setView(dialogPicker)
                .setTitle("Seleccione una opción")
                .setView(DialogView)
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }

                })
                .setPositiveButton("Seleccionar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        oDialog = dialog;
                        plazo = termCreditList[dialogPicker.getValue()];
                        txtPlazo.setText(plazo); //setear cuando sea exitoso

                        Log.w("claves","respaldando desde plazo");

                        String cvePlazo = buscarClave(txtOpcion.getText().toString(),plazo,buscarPlazo);
                        String cveSub   = buscarClave(txtOpcion.getText().toString(),"",buscarOpcion);

                        Log.i("claves","BuscarOpcion pickerPlazo: "+cveSub+ " Opcion: "+txtOpcion.getText());
                        Log.i("claves", "BuscarPlazo pickerPlazo: " + cvePlazo + " Plazo: "+plazo);

                        delegate.updateData(cveSub, cvePlazo);
                    }

                }).show();
    }

    private void pickerOpcion(){
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View DialogView = inflater.inflate(R.layout.activity_credit_picker, null);

        final NumberPicker dialogPicker = (NumberPicker)DialogView.findViewById(R.id.dialogPicker);
        dialogPicker.setMinValue(0);
        dialogPicker.setMaxValue(optionList.length - 1);
        dialogPicker.setDisplayedValues(optionList);
        dialogPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        dialogPicker.setWrapSelectorWheel(false);
        dialogPicker.setValue(getPosition(optionList, txtOpcion.getText().toString()));

        new AlertDialog.Builder(getActivity()).setView(dialogPicker)
                .setTitle("Seleccione una opción")
                .setView(DialogView)
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }

                })
                .setPositiveButton("Seleccionar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        oDialog = dialog;
                        option = optionList[dialogPicker.getValue()];
                        txtOpcion.setText(option);
                        updateTermList(option);

                        String cveSub = buscarClave(option,"",buscarOpcion);
                        String cvePlazo = buscarClave(option, plazo, buscarPlazo);

                        Log.i("update","cveSub: "+cveSub+", cveplazo: "+cvePlazo);
                        delegate.updateData(cveSub,cvePlazo);

                    }

                }).show();
    }

    private int getPosition(String [] array, String code) {
        for(int i= 0; i< array.length;i++) {
            if(array[i].equalsIgnoreCase(code))
                return i;
        }
        return -1;
    }

    public void updateMenu(){

        Log.i("update", "actualizaMenu, cveProd: " + producto.getCveProd());

        ((MenuPrincipalActivity)getActivity()).updateMenu(producto.getCveProd());
        getFragmentManager().beginTransaction().remove(((MenuPrincipalActivity) getActivity()).getActiveFragment()).commit();
        ((MenuPrincipalActivity)getActivity()).showAuto();

        oDialog.dismiss();
        oDialog = null;

    }

    public void showDetalleAuto(){

        ((MenuPrincipalActivity)getActivity()).updateMenu(producto.getCveProd());

        try {
            if (((MenuPrincipalActivity) getActivity()).getActiveFragment() != null) {
                getFragmentManager().beginTransaction().remove(((MenuPrincipalActivity) getActivity()).getActiveFragment()).commit();
            }
            ((MenuPrincipalActivity) getActivity()).setActiveFragment(null);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        detalleAuto = new DetalleAutoViewController();
        detalleAuto.setDelegate(delegate);
//        android.util.Log.w("update", "flowFragment: " + ((MenuPrincipalActivity) getActivity()).getoFragment());
        ((MenuPrincipalActivity) getActivity()).setActiveFragment(detalleAuto);
        getFragmentManager().beginTransaction().
                add(R.id.fragmentContainer, ((MenuPrincipalActivity) getActivity()).getActiveFragment()).commit();
    }

    private void setFormattedText(){
        isSettingText = true;
        String text = typedString.toString();

        if(mAcceptCents){
            text += "00";
        }

        lblMontoTotalCredito.setText(bancomer.api.common.commons.Tools.formatAmountFromServer(text));
    }

    public void reset() {
        this.isResetting = true;
        this.typedString=new StringBuffer();
        this.isResetting = false;
    }

    public void actualizaGrafica(String monto){
        Log.i("fragment", "actualizando gráfica, monto: " + monto);

        if(monto.startsWith("$"))
            monto = monto.replace("$","");

        mSeekArc.setArcRotation(345);
        mSeekArc.setSweepAngle(330);
        mSeekArc.setArcWidth(40);
        mSeekArc.setProgressWidth(20);

        mSeekArc.updateProgressText(Integer.parseInt(Tools.validateValueMonto(monto)));
    }

    private void realizaAccionInicio(){
        Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
        click_paso2_operacion.put("evento_paso1", "event45");
//        click_paso2_operacion.put("&&products", "simulador;simulador:simulador credito auto;;;;eVar12=inicio");
        click_paso2_operacion.put("&&products", "simulador;simulador:simulador credito auto;;;;");
        click_paso2_operacion.put("eVar12", "inicio");  //pendiente por definir
        TrackingHelperCredit.trackSimulacionRealizada(click_paso2_operacion);
    }

    private void realizaAccionSimular(){
        Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
        click_paso2_operacion.put("evento_paso1", "event46");
//        click_paso2_operacion.put("&&products", "simulador;simulador:simulador incremento linea;;;;eVar12=simulacion realizada");
        click_paso2_operacion.put("&&products", "simulador;simulador:simulador credito auto;;;;");
        click_paso2_operacion.put("eVar12", "simulacion realizada");  //pendiente por definir
        TrackingHelperCredit.trackSimulacionRealizada(click_paso2_operacion);
    }
}
