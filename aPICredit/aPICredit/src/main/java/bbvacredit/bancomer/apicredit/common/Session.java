package bbvacredit.bancomer.apicredit.common;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.bancomer.apiilc.models.OfertaILC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.consumo.models.OfertaConsumo;
import bbvacredit.bancomer.apicredit.gui.activities.MenuPrincipalActivity;
import bbvacredit.bancomer.apicredit.gui.delegates.BaseDelegateOperacion;
import suitebancomercoms.aplicaciones.bmovil.classes.model.AceptaOfertaEFI;;
import bbvacredit.bancomer.apicredit.models.Catalog;
import bbvacredit.bancomer.apicredit.models.ConsultaDatosTDCData;
import bbvacredit.bancomer.apicredit.models.ConvenioPagoServicios;
import bbvacredit.bancomer.apicredit.models.CreditoContratado;
import bbvacredit.bancomer.apicredit.models.DineroMovil;
import bbvacredit.bancomer.apicredit.models.JsonListaCuentas;
import bbvacredit.bancomer.apicredit.models.MantenimientoSPEI;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.OfertaEFI;
import bbvacredit.bancomer.apicredit.models.Producto;
import bbvacredit.bancomer.apicredit.models.PromocionesCampania;
import bbvacredit.bancomer.apicredit.models.Rapido;
import bbvacredit.bancomer.apicredit.models.TiempoAire;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.gui.activities.LineaCreditoActivity;
import bbvacredit.bancomer.apicredit.gui.activities.LoginActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaCreditoAutoContratoActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaCreditoHipotecarioContratoActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaCreditoNominaContratoActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaPPIActivity;
import bbvacredit.bancomer.apicredit.gui.activities.SimulaTarjetaCreditoActivity;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;


/**
 * 
 * @author Stefanini IT Solutions.
 * 
 *         Session stores values than can be used by different instances of the
 *         application, and which have validity during a user's session. It is
 *         encoded as a Singleton pattern.
 */

public class Session {

	/** The singleton instance */
	private static Session theInstance = null;
	
	////////////////////////////////////////////////////////////////

	private String operacion = null;

	/**
	 * Clave de campaña
	 */
	private String cveCamp;

	/** 
	 *  Username
	 */
	private String idUsuario;
	
	/**
	 *  Celular Number
	 */
	private String numCelular;

	/**
	 * Indicador Opinator
	 */
	private boolean showOpinator = false;
	/**
	 * Random seed to generate the IUM
	 */
	private long seed = 0;

	/**
	 * The password entered in login
	 */
	private String password = null;
	
	/**
	 * viene de regresar
	 */
	private Boolean regresar = false;
	
	/**
	 * 
	 */
	private String email = null;
	
	/**
	 *  Session context 
	 */
	private Context context;

	/**
	 * The unique identifier of the user
	 */
	private String ium = null;

	/**
	 * The catalog versions(initially value 0)
	 */	
	private HashMap<String, String> catalogVersions;

	/**
	 * Lista de r�pidos.
	 */
	private ArrayList<Rapido> rapidos = null;
	
	/**
	* Lista de promociones.
	*/
	private ArrayList<PromocionesCampania> promocion = null;

	/**
	 * The user accounts
	 */
	private JsonListaCuentas accounts = null;
	
	/**
	 * states
	 *
	 */
	public enum State {NORMAL, OK, DELETE};
	
	private HashMap<Integer,State> stateList = new HashMap<Integer,State>();

	/**
	 * The catalogs
	 */
	private ArrayList<Catalog> catalogs = new ArrayList<Catalog>();
	
	private TiempoAire catalogoTiempoAire = new TiempoAire();
	private DineroMovil catalogoDineroMovil = new DineroMovil(); 
	private ConvenioPagoServicios catalogoServicios = new ConvenioPagoServicios(); 
	private MantenimientoSPEI catalogoMantenimientoSPEI = new MantenimientoSPEI(); 

	////////////////////////////////////////////////////////////////
	
	/**
	 * Object for singleton synchronization
	 */
	private static final Object lock = new Object();
	
	/**
	 * Object for Creditos
	 */
	private ObjetoCreditos auxCreditos = null;

	private ObjetoCreditos creditos = null;
	
	private Boolean showCreditosContratados = null;
	
	private Boolean showCreditosOfertados = null;
	
	private LoginActivity activityLogin = null;
	
	private ConsultaDatosTDCData dataTDC;
	
	public ConsultaDatosTDCData getDataTDC() {
		return dataTDC;
	}


	public void setDataTDC(ConsultaDatosTDCData dataTDC) {
		this.dataTDC = dataTDC;
	}


	public LoginActivity getActivityLogin() {
		return activityLogin;
	}


	public void setActivityLogin(LoginActivity activityLogin) {
		this.activityLogin = activityLogin;
	}

	/**
	 * respaldo de datos al actualizar plazos;
	 */
	public String optionRespaldo;
	public String plazoRespaldo;
	public boolean isUpdated =false;
	public String montoActualizado;


	// Devuelve los datos de creditos, no el delegate completo. Se hace as� para en un futuro cambiar la instancia del delegate por dto simple.
	public ObjetoCreditos getDataFromCreditos(){
		if(creditos != null){
			return creditos;
		}else{
			return null;
		}
	}
	
	// Activities
	private LineaCreditoActivity lineaAct = null;
	private SimulaCreditoAutoContratoActivity autoCAct = null;
	private SimulaCreditoHipotecarioContratoActivity hipoCAct = null;
	private SimulaCreditoNominaContratoActivity nomCAct = null;
	private SimulaPPIActivity ppiCAct = null;
	private SimulaTarjetaCreditoActivity tdcAct = null;

	private MenuPrincipalActivity oMenuPA;
	private BaseDelegateOperacion oDelegate;

	public OfertaEFI ofertaEFI;
	public Promociones promociones;
	public AceptaOfertaEFI aceptaOfertaEFI;

//	public Promociones[] promocionesEFI;

	private boolean isClearOnly;

	public AceptaOfertaEFI getAceptaOfertaEFI() {
		return aceptaOfertaEFI;
	}

	public void setAceptaOfertaEFI(AceptaOfertaEFI aceptaOfertaEFI) {
		this.aceptaOfertaEFI = aceptaOfertaEFI;
	}

//	public Promociones[] getPromocionesEFI() {
//		return promocionesEFI;
//	}
//
//	public void setPromocionesEFI(Promociones[] promocionesEFI) {
//		this.promocionesEFI = promocionesEFI;
//	}

	public Promociones getPromociones() {
		return promociones;
	}

	public void setPromociones(Promociones promociones) {
		this.promociones = promociones;
	}

	public OfertaEFI getOfertaEFI() {
		return ofertaEFI;
	}

	public void setOfertaEFI(OfertaEFI ofertaEFI) {
		this.ofertaEFI = ofertaEFI;
	}

	public boolean isClearOnly() {
		return isClearOnly;
	}

	public void setIsClearOnly(boolean isClearOnly) {
		this.isClearOnly = isClearOnly;
	}

	private OfertaILC ofertaILC;

	public OfertaILC getOfertaILC() {
		return ofertaILC;
	}

	public void setOfertaILC(OfertaILC ofertaILC) {
		this.ofertaILC = ofertaILC;
	}

	public BaseDelegateOperacion getoDelegate() {
		return oDelegate;
	}

	public void setoDelegate(BaseDelegateOperacion oDelegate) {
		this.oDelegate = oDelegate;
	}

	public MenuPrincipalActivity getoMenuPA() {
		return oMenuPA;
	}

	public void setoMenuPA(MenuPrincipalActivity oMenuPA) {
		this.oMenuPA = oMenuPA;
	}

	public LineaCreditoActivity getLineaAct() {
		return lineaAct;
	}


	public void setLineaAct(LineaCreditoActivity lineaAct) {
		this.lineaAct = lineaAct;
	}


	public SimulaCreditoAutoContratoActivity getAutoCAct() {
		return autoCAct;
	}


	public void setAutoCAct(SimulaCreditoAutoContratoActivity autoCAct) {
		this.autoCAct = autoCAct;
	}


	public SimulaCreditoHipotecarioContratoActivity getHipoCAct() {
		return hipoCAct;
	}


	public void setHipoCAct(SimulaCreditoHipotecarioContratoActivity hipoCAct) {
		this.hipoCAct = hipoCAct;
	}


	public SimulaCreditoNominaContratoActivity getNomCAct() {
		return nomCAct;
	}


	public void setNomCAct(SimulaCreditoNominaContratoActivity nomCAct) {
		this.nomCAct = nomCAct;
	}


	public SimulaPPIActivity getPpiCAct() {
		return ppiCAct;
	}


	public void setPpiCAct(SimulaPPIActivity ppiCAct) {
		this.ppiCAct = ppiCAct;
	}


	public SimulaTarjetaCreditoActivity getTdcAct() {
		return tdcAct;
	}


	public void setTdcAct(SimulaTarjetaCreditoActivity tdcAct) {
		this.tdcAct = tdcAct;
	}


	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	/**
	 *  Getters and Setters
	 */

	public String getIdUsuario() {
		return idUsuario;
	}

	public HashMap<Integer, State> getStateList() {
		return stateList;
	}


	public void setStateList(HashMap<Integer, State> stateList) {
		this.stateList = stateList;
	}


	public Boolean getRegresar() {
		return regresar;
	}


	public void setRegresar(Boolean regresar) {
		this.regresar = regresar;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public static Object getLock() {
		return lock;
	}

	public ObjetoCreditos getCreditos() {
		return creditos;
	}

	public void setCreditos(ObjetoCreditos creditos) {
		this.creditos = creditos;
	}

	public ObjetoCreditos getAuxCreditos() {
		return auxCreditos;
	}

	public void setAuxCreditos(ObjetoCreditos auxCreditos) {
		this.auxCreditos = auxCreditos;
	}

	public boolean isShowOpinator() {
		return showOpinator;
	}

	public void setShowOpinator(boolean showOpinator) {
		this.showOpinator = showOpinator;
	}

	public String getNumCelular() {
		return numCelular;
	}

	public void setNumCelular(String numCelular) {
		this.numCelular = numCelular;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public String getIum() {
		return ium;
	}

	public void setIum(String ium) {
		this.ium = ium;
	}

	public long getSeed() {
		return seed;
	}

	public void setSeed(long seed) {
		this.seed = seed;
	}
	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public HashMap<String,String> getCatalogVersions() {
		return catalogVersions;
	}

	public void setCatalogVersions(HashMap<String,String> catalogVersions) {
		this.catalogVersions = catalogVersions;
	}

	public ArrayList<Rapido> getRapidos() {
		return rapidos;
	}

	public void setRapidos(ArrayList<Rapido> rapidos) {
		this.rapidos = rapidos;
	}

	public ArrayList<PromocionesCampania> getPromocion() {
		return promocion;
	}

	public void setPromocion(ArrayList<PromocionesCampania> promocion) {
		this.promocion = promocion;
	}

	public JsonListaCuentas getAccounts() {
		return accounts;
	}

	public void setAccounts(JsonListaCuentas accounts) {
		this.accounts = accounts;
	}

	public ArrayList<Catalog> getCatalogs() {
		return catalogs;
	}

	public void setCatalogs(ArrayList<Catalog> catalogs) {
		this.catalogs = catalogs;
	}

	public TiempoAire getCatalogoTiempoAire() {
		return catalogoTiempoAire;
	}

	public void setCatalogoTiempoAire(TiempoAire catalogoTiempoAire) {
		this.catalogoTiempoAire = catalogoTiempoAire;
	}

	public DineroMovil getCatalogoDineroMovil() {
		return catalogoDineroMovil;
	}

	public void setCatalogoDineroMovil(DineroMovil catalogoDineroMovil) {
		this.catalogoDineroMovil = catalogoDineroMovil;
	}

	public ConvenioPagoServicios getCatalogoServicios() {
		return catalogoServicios;
	}

	public void setCatalogoServicios(ConvenioPagoServicios catalogoServicios) {
		this.catalogoServicios = catalogoServicios;
	}

	public MantenimientoSPEI getCatalogoMantenimientoSPEI() {
		return catalogoMantenimientoSPEI;
	}

	public void setCatalogoMantenimientoSPEI(
			MantenimientoSPEI catalogoMantenimientoSPEI) {
		this.catalogoMantenimientoSPEI = catalogoMantenimientoSPEI;
	}
	
	/**
	 *  End Getters and Setters
	 */

	/**
	 * Default constructor
	 */
	private Session() {
		initVariables();
	}
	
	/**
	 * Public accessor method to the instance
	 * 
	 * @return the Session instance
	 */
	public static Session getInstance(Context context) {
		if (theInstance == null) {
			synchronized (lock) {
				if (theInstance == null) {
					theInstance = new Session(context);
				}
			}
		}

		/*
		 * Sometimes the SO frees the Session object when pausing the
		 * application, we must restore it
		 */
		if (theInstance.getIum() == null) {
			// TODO: cargar lo almacenado awesome pero con tiempo para hacerlo bien y tal ... 
			//theInstance.loadRecordStore();
			/*String ium = Tools.buildIUM(theInstance.getIdUsuario(), theInstance.getSeed(), context);
			theInstance.setIum(ium);*/
			theInstance.setIum(ConstantsCredit.IUM_ESTATICO);
		}

		return theInstance;
	}

	/**
	 * Default constructor
	 */
	private Session(Context context) {
		this.context = context;
		// TODO: cuando se haga la recarga de datos no ser� necesario a no ser que se haga por seguridad
		initVariables();
		// TODO: cada todo es un puto infierno, llamar a metodos para cargar los datos
		//loadRecordStore();
//		CatalogoAutenticacionFileManager.getCurrent().cargaCatalogoAutenticacion();
		//loadCatalogoCompaniasTelefonicas();
	}
	
	/**
	 * Public accessor method to the instance
	 * 
	 * @return the Session instance
	 */
	public static Session getInstance() {
		if (theInstance == null) {
			synchronized (lock) {
				if (theInstance == null) {
					theInstance = new Session();
				}
			}
		}
		return theInstance;
	}
	
	/**
	 * Inicializa las variables pertinentes
	 */
	private void initVariables(){
		// Map con las versiones de los cat�logos, un array asociativo siempre es bien
		catalogVersions = new HashMap<String, String>();
		catalogVersions.put(ServerConstantsCredit.VERSION_C1, "0");
		catalogVersions.put(ServerConstantsCredit.VERSION_C4, "0");
		catalogVersions.put(ServerConstantsCredit.VERSION_C5, "0");
		catalogVersions.put(ServerConstantsCredit.VERSION_C8, "0");
		catalogVersions.put(ServerConstantsCredit.VERSION_TA, "0");
		catalogVersions.put(ServerConstantsCredit.VERSION_DM, "0");
		catalogVersions.put(ServerConstantsCredit.VERSION_SV, "0");
		catalogVersions.put(ServerConstantsCredit.VERSION_MS, "0");
		catalogVersions.put(ServerConstantsCredit.VERSION_AU, "0");
	}


	public Boolean getShowCreditosContratados() {
		return showCreditosContratados;
	}


	public void setShowCreditosContratados(Boolean showCreditosContratados) {
		this.showCreditosContratados = showCreditosContratados;
	}


	public Boolean getShowCreditosOfertados() {
		return showCreditosOfertados;
	}


	public void setShowCreditosOfertados(Boolean showCreditosOfertados) {
		this.showCreditosOfertados = showCreditosOfertados;
	}
	
	public Integer getProductoIndexByCve(String cve){
		ObjetoCreditos obc = getDataFromCreditos();
		Integer index = 0;
		
		if(obc != null){
			for (int i = 0; i < obc.getProductos().size(); i++) {
				Producto pr = obc.getProductos().get(i);
				
				if(pr.getCveProd().equals(cve)){
					index = i;
				}
			}
		}
		
		return index;
	}
	
	public Integer getContSimCredits(){
		Integer cont = 0;
		ObjetoCreditos obc = getDataFromCreditos();
		
		if(obc != null){
			
			if(obc.getProductos() != null){
				Iterator<Producto> it = obc.getProductos().iterator();
				Producto p;
				while(it.hasNext()){
					p = it.next();
					if(p.getIndSimBoolean()) ++cont;
				}
			}
			
			if(obc.getCreditosContratados() != null){
				Iterator<CreditoContratado> it = obc.getCreditosContratados().iterator();
				CreditoContratado c;
				while(it.hasNext()){
					c = it.next();
					if(c.getIndicadorSim()) ++cont;
				}
			}
			
		}
		
		return cont;
	}
	
	public void setVisibilityButton(Activity act, int id){
		
		Button bresumen = (Button)act.findViewById(id);
		
		if(getContSimCredits() == 0){
			bresumen.setVisibility(View.INVISIBLE);
		}else{
			bresumen.setVisibility(View.VISIBLE);
		}
		
	}

	public String getCveCamp() {
		return cveCamp;
	}

	public void setCveCamp(String cveCamp) {
		this.cveCamp = cveCamp;
	}
}
