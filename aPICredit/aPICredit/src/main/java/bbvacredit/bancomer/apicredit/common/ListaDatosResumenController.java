package bbvacredit.bancomer.apicredit.common;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bancomer.api.common.commons.ICommonSession;
import bbvacredit.bancomer.apicredit.R;

import java.util.HashMap;
import java.util.Map;

import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.BaseActivity;
import bbvacredit.bancomer.apicredit.gui.activities.PopUpContactameActivity;
import bbvacredit.bancomer.apicredit.gui.delegates.DetalleDeAlternativaDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.ResumenDelegate;
import bbvacredit.bancomer.apicredit.models.Producto;


import tracking.TrackingHelperCredit;


public class ListaDatosResumenController implements OnClickListener {
	
	private LinearLayout layout;
	
	private Context cnt;
	
	private int idLayout;
	
	private String claveOneClick;
	
	private BaseActivity controller;
	
	private Producto producto;

	//Modificacion 50986
	private ResumenDelegate delegate = new ResumenDelegate();

	public ListaDatosResumenController(){
		
	}
	
	public ListaDatosResumenController(LinearLayout layout, Context cnt, int idLayout,BaseActivity controller,Producto producto) {
		this.layout = layout;
		this.cnt = cnt;
		this.idLayout = idLayout;
		this.controller=controller;
		this.producto=producto;
	}

	public void addElement(String clave, String descripcion, String valor, Boolean mostrarBarra){
			
		claveOneClick=clave;
		LinearLayout myLo = (LinearLayout) LayoutInflater.from(cnt).inflate(R.layout.lista_datos_template_resumen, null);
		TextView desc = ((TextView)myLo.findViewById(R.id.textView1));
		TextView val = ((TextView)myLo.findViewById(R.id.textView2));
		ImageView barraUp = (ImageView)myLo.findViewById(R.id.imageView2);
		ImageView barraDown = (ImageView)myLo.findViewById(R.id.imageView1);
		
		ImageButton btnAdquirir = (ImageButton)myLo.findViewById(R.id.btnAdquirir);
		btnAdquirir.setOnClickListener(this);
		ImageButton btnContactar = (ImageButton)myLo.findViewById(R.id.btnContactar);
		btnContactar.setOnClickListener(this);
		
		if(clave.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO) || clave.equals(ConstantsCredit.CREDITO_NOMINA) || clave.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO) ){
			//Modificacion 50986
			//if(clave.equals(ConstantsCredit.CREDITO_NOMINA) || clave.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO) )
			//	;
			//else
			btnAdquirir.setVisibility(View.VISIBLE);
		}
			
		else
		 btnContactar.setVisibility(View.VISIBLE);
		
		desc.setText(descripcion);
		val.setText(valor);
		
		if(!mostrarBarra) barraUp.setVisibility(View.GONE);
		
		layout.addView(myLo);
		
		GuiTools guiTools = GuiTools.getCurrent();		
		guiTools.init(controller.getWindowManager());
		guiTools.scale(controller.findViewById(idLayout));
		guiTools.scale(desc,true);
		guiTools.scale(val,true);
		guiTools.scale(barraUp);
		guiTools.scale(barraDown);
		guiTools.scale(myLo);
		guiTools.scale(btnAdquirir);
		guiTools.scale(btnContactar);
		
		layout.requestLayout(); 
	}
	
	
	public void addElement(String descripcion, String valor, Boolean mostrarBarra){
		
		LinearLayout myLo = (LinearLayout) LayoutInflater.from(cnt).inflate(R.layout.lista_datos_template_resumen, null);
		TextView desc = ((TextView)myLo.findViewById(R.id.textView1));
		TextView val = ((TextView)myLo.findViewById(R.id.textView2));
		ImageView barraUp = (ImageView)myLo.findViewById(R.id.imageView2);
		ImageView barraDown = (ImageView)myLo.findViewById(R.id.imageView1);

		desc.setText(descripcion);
		val.setText(valor);
		
		if(!mostrarBarra) barraUp.setVisibility(View.GONE);
		
		layout.addView(myLo);
		
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(controller.getWindowManager());
		guiTools.scale(controller.findViewById(idLayout));
		guiTools.scale(desc,true);
		guiTools.scale(val,true);
		guiTools.scale(barraUp);
		guiTools.scale(barraDown);
		guiTools.scale(myLo);
		
		layout.requestLayout(); 
	}
	
	
	public void addCreditoContratadoElement(String descripcion, String valor, Boolean mostrarBarra, Boolean isTitle){
		LinearLayout myLo = (LinearLayout) LayoutInflater.from(cnt).inflate(R.layout.lista_datos_template_resumen, null);
		TextView desc = ((TextView)myLo.findViewById(R.id.textView1));
		TextView val = ((TextView)myLo.findViewById(R.id.textView2));
		ImageView barraUp = (ImageView)myLo.findViewById(R.id.imageView2);
		ImageView barraDown = (ImageView)myLo.findViewById(R.id.imageView1);
		
		desc.setText(descripcion);
		val.setText(valor);
		
		if(isTitle) val.setTextColor(controller.getResources().getColor(R.color.tercer_azul));
		
		if(!mostrarBarra) barraUp.setVisibility(View.GONE);
		
		layout.addView(myLo);
		
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(controller.getWindowManager());
		guiTools.scale(controller.findViewById(idLayout));
		guiTools.scale(desc,true);
		guiTools.scale(val,true);
		guiTools.scale(barraUp);
		guiTools.scale(barraDown);
		guiTools.scale(myLo);
		
		layout.requestLayout(); 
	}

	@Override
	public void onClick(View v) {
		
		controller.isOnForeground=false;
		
		if(v.getId() == R.id.btnContactar)
		{
			String idProd = "";
			 if(claveOneClick.equals(ConstantsCredit.CREDITO_AUTO))
             {
				 idProd = "AUTO";
				 Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
				 click_paso2_operacion.put("inicio","event45");
				 click_paso2_operacion.put("&&products","simulador;simulador:simulador credito auto");
				 click_paso2_operacion.put("eVar12", "seleccion contactame");
				 TrackingHelperCredit.trackPaso1Operacion(click_paso2_operacion);
             }
             else if(claveOneClick.equals(ConstantsCredit.CREDITO_HIPOTECARIO))
             {
				 idProd = "HIPO";
				 Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();

				 click_paso2_operacion.put("inicio","event45");
				 click_paso2_operacion.put("&&products","simulador;simulador:simulador credito hipotecario");
				 click_paso2_operacion.put("eVar12", "seleccion contactame");
				 TrackingHelperCredit.trackPaso1Operacion(click_paso2_operacion);
             }
             
             else if(claveOneClick.equals(ConstantsCredit.TARJETA_CREDITO))
             {
				 idProd = "TDC";
				 Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
				 click_paso2_operacion.put("inicio","event45");
				 click_paso2_operacion.put("&&products","simulador;simulador:simulador tarjeta credito");
				 click_paso2_operacion.put("eVar12","seleccion contactame");
				 TrackingHelperCredit.trackPaso1Operacion(click_paso2_operacion);
             }
             
             //Log.e("Selecciono contactame","...");
			 String params = "";
			 ICommonSession sesion = MainController.getInstance().getSession();
			 params = "nombredepila=" + sesion.getNombreCliente().split(" ")[0] + "&segmento=&credito=" + this.producto.getMontoDeseS() + "&idpagina=resumen";
			 Log.d("Credit", "params = " + params);
			 PopUpContactameActivity.muestraPop(cnt, params);
     }
		else if(v.getId() == R.id.btnAdquirir)
		{
			Log.e("Selecciono adquirir", "...");
			if(claveOneClick.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO))
            {
				Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
				click_paso2_operacion.put("evento_paso1","event45");
				click_paso2_operacion.put("&&products","simulador;simulador:simulador prestamo personal");
				click_paso2_operacion.put("eVar12","seleccion adquirir");
				TrackingHelperCredit.trackPaso1Operacion(click_paso2_operacion);
                    /*
                    HashMap cdata = new HashMap<String, Object>();
                    cdata.put("ppi",150000.00);
                    Analytics.trackState("Homepage", cdata);
                    */
				//Modificacion 50986
                  //  consumoOneClick();
				saveSimulation(true);
            }
            else if(claveOneClick.equals(ConstantsCredit.CREDITO_NOMINA))
            {
				Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
				click_paso2_operacion.put("evento_paso1","event45");
				click_paso2_operacion.put("&&products","simulador;simulador:simulador credito nomina");
				click_paso2_operacion.put("eVar12","seleccion adquirir");
				TrackingHelperCredit.trackPaso1Operacion(click_paso2_operacion);
                    /*
                    HashMap cdata = new HashMap<String, Object>();
                    cdata.put("nom",210000.00);
                    Analytics.trackState("Homepage", cdata);
                    */
				//Modificacion 50986
                  //  consumoOneClick();
				saveSimulation(true);
            }
            else if(claveOneClick.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO))
            {
				Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
				click_paso2_operacion.put("evento_paso1","event45");
				click_paso2_operacion.put("&&products","simulador;simulador:simulador incremento linea credito");
				click_paso2_operacion.put("eVar12", "seleccion adquirir");
				TrackingHelperCredit.trackPaso1Operacion(click_paso2_operacion);
				//Modificacion 50986
				//oneClickILC();
				saveSimulation(false);
			}
		}
		
	}


	public void oneClickILC()
	{
		//Modificacion 50986
		//OK	
		//Promociones promocion = new Promociones();
		//OfertaILC oferta = new OfertaILC();

		//oferta.setCat(producto.getCATS());
		//oferta.setFechaCat(producto.getFechaCatS());
		
		//Bmovil Session variables to be used to determine which flow the "one click flow" is going to take.
		//suitebancomer.aplicaciones.bmovil.classes.common.Session.getInstance(MainController.getInstance().getContext()).setOfertaDelSimulador(true);
		Session.getInstance().setCveCamp(producto.getCveProd());
		
		Session session = Session.getInstance(MainController.getInstance().getContext());
		
		//DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(session.getIdUsuario(), session.getIum(), session.getNumCelular(), producto.getCveProd());
		//delegate.setOferta(oferta);
		//delegate.setPromocion(promocion);
		//delegate.consultaDetalleAlternativasTask();
	}
	
	
	public void consumoOneClick()
	{		
		//OK
		/** Modified: April 22th, 2015. Author:OOS.*/

		//Modificacion 50986
		//Promociones promocion = new Promociones();
		
		//Bmovil Session variables to be used to determine which flow the "one click flow" is going to take.
		//suitebancomer.aplicaciones.bmovil.classes.common.Session.getInstance(MainController.getInstance().getContext()).setOfertaDelSimulador(true);
		Session.getInstance().setCveCamp(producto.getCveProd());
				
		
		//OfertaConsumo oferta = new OfertaConsumo();
		
		//oferta.setImporte(Tools.formatterForBmovil(producto.getMontoDeseS()));
		//oferta.setPlazoDes(producto.getDesPlazoE());
		
		//String bscPagar = new String(producto.getDessubpE());
		//oferta.setTipoSeg(bscPagar.substring(0, 16));
		//oferta.setTotalPagos(Tools.getTotalPagos(producto.getDesPlazoE()));
		
		Session session = Session.getInstance();
		
		//DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(session.getIdUsuario(), session.getIum(), session.getNumCelular(), producto.getCveProd());
		//delegate.setOfertaConsumo(oferta);
		//delegate.setPromocion(promocion);
		//delegate.consultaDetalleAlternativasTask();
		
		
	}

	//Modificacion 50986
	private void saveSimulation(boolean isConsumo)
	{
		delegate.saveSimulationRequest(this, isConsumo);
	}

}
