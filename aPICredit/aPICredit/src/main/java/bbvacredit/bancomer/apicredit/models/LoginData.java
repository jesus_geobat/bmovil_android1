package bbvacredit.bancomer.apicredit.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;




import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.io.ParserJSON;
import bbvacredit.bancomer.apicredit.io.ParsingHandler;
import bbvacredit.bancomer.apicredit.io.Parser;
import bbvacredit.bancomer.apicredit.io.ParsingException;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;


public class LoginData implements ParsingHandler{
	

    /**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = -400496845278428572L;

	/**
     * The Bancomer token.
     */
    private String bancomerToken = null;
    
    /**
     * Vía 1 o 2
     */
    private String via = null;

    /**
     * The client identifier for session purposes.
     */
    private String clientNumber = null;

    /**
     * The server date.
     */
    private String serverDate = null;

    /**
     * The server time.
     */
    private String serverTime = null;

    /**
     * The user account list.
     */
    private JsonListaCuentas accounts = null;

    /**
     * The catalogs.
     */
    private Catalog[] catalogs = null;

    /**
     * The version of each catalog.
     */
    private HashMap<String, String> catalogVersions = null;

    /**
     * The optional message to show after a successful login.
     */
    private String message = null;

    /**
     * The message to show that is an update available for the application.
     */
    private String updateMessage = null;

    /**
     * URL for the case in that the response is an update message.
     */
    private String updateURL = null;
    
    /**
     * Nombre del cliente
     */
    private String nombreCliente;
    
    /**
     * Compa�ia a la que pertenece el tel�fono del cliente
     */
    private String companiaTelCliente;
    
    /**
     * Correo electr�nico del cliente
     */
    private String emailCliente;
    
    /**
     * Perfil del cliente
     */
    private String perfilCliente;
    
    /**
     * Tipo de instrumento de seguridad del cliente
     */
    private String insSeguridadCliente;
    
    /**
     * Estatus del servicio
     */
    private String estatusServicio;
    
    /**
     * The session timeout in minutes.
     */
    private int timeout = 1;
    
    private String estatusInstrumento;
    
    private ListaTiempoAire catalogoTA;
    
    private ListaDineroMovil catalogoDineroMovil;
    
    private ListaConvenioPagoServicios catalogoConvenios;
    
    private ListaMantenimientoSPEI catalogoMantenimientoSPEI;
    
    private JsonListaCuentas listaCuentas;
    
    private ArrayList<Rapido> listaRapidos;
    
    private String autenticacionJson;
    
    private String limiteOperacion;
    
    private String jsonRapidos;
    
    /**
     * Estatus de alertas (alta, modificacion o vacio).
     */
    private String estatusAlertas;
    
    /**
     * Fecha de contratacci�n de bancomer m�vil.
     */
    private String fechaContratacion;
    
    private String candidatoPromociones;
    private ArrayList<Campania> listaPromocionesJSON;
    private String contratoBBVAlink;
    private String marcaTendero;
    
    /**
     * Cat�logo de compa�ias telefonicas.
     */
    private String catTelefonicas;
  //One Click
    /** json de promociones
     */
    private String isPromocion;
	private String jSonPromociones;

    /**
     * Default constructor.
     */
    public LoginData() {
    }
    
    /**
	 * @return the emailCliente
	 */
	public String getEmailCliente() {
		return emailCliente;
	}

	/**
	 * @return the estatusInstrumento
	 */
	public String getEstatusInstrumento() {
		return estatusInstrumento;
	}

	/**
     * Get the client identifier for session purposes.
     * @return the client identifier for session purposes
     */
    public String getClientNumber() {
        return clientNumber;
    }

    /**
     * get the server date.
     * @return the server date
     */
    public String getServerDate() {
        return serverDate;
    }

    /**
     * Get the server time.
     * @return the server time
     */
    public String getServerTime() {
        return serverTime;
    }

    /**
     * Get via
     * @return via
     */
    public String getVia() {
        return via;
    }
    
    /**
     * Get the Bancomer token.
     * @return the Bancomer token
     */
    public String getBancomerToken() {
        return bancomerToken;
    }

    /**
     * get the user's accounts.
     * @return the user's accounts
     */
    public JsonListaCuentas getAccounts() {
        return accounts;
    }

    /**
     * Get the catalogs.
     * @return the catalogs
     */
    public Catalog[] getCatalogs() {
        return catalogs;
    }
    
    /**
     * Get the authentication JSON.
     * @return Authentication JSON string.
     */
    public String getAuthenticationJson() {
    	return this.autenticacionJson;
    }

    public ListaTiempoAire getCatalogoTA() {
		return catalogoTA;
	}

	public void setCatalogoTA(ListaTiempoAire catalogoTA) {
		this.catalogoTA = catalogoTA;
	}

	public ListaDineroMovil getCatalogoDineroMovil() {
		return catalogoDineroMovil;
	}

	public void setCatalogoDineroMovil(ListaDineroMovil catalogoDineroMovil) {
		this.catalogoDineroMovil = catalogoDineroMovil;
	}

	public ListaConvenioPagoServicios getCatalogoConvenios() {
		return catalogoConvenios;
	}

	public void setCatalogoConvenios(ListaConvenioPagoServicios catalogoConvenios) {
		this.catalogoConvenios = catalogoConvenios;
	}

	public ListaMantenimientoSPEI getCatalogoMantenimientoSPEI() {
		return catalogoMantenimientoSPEI;
	}

	public void setCatalogoMantenimientoSPEI(
			ListaMantenimientoSPEI catalogoMantenimientoSPEI) {
		this.catalogoMantenimientoSPEI = catalogoMantenimientoSPEI;
	}

	/**
     * Get the optional message.
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Get the updating message.
     * @return the updating message
     */
    public String getUpdateMessage() {
        return updateMessage;
    }
    /**
     * Get the updating URL.
     * @return the URL
     */
    public String getUpdateURL() {
        return updateURL;
    }

    /**
     * Get the session timeout.
     * @return the session timeout, in minutes
     */
    public int getTimeout() {
        return timeout;
    }
    
    public String getPerfiCliente() {
    	return perfilCliente;
    }
    
    public String getInsSeguridadCliente() {
    	return insSeguridadCliente;
    }
    
    public String getEstatusServicio() {
    	return estatusServicio;
    }
    
    public String getEstatusIS() {
    	return estatusInstrumento;
    }

    public String getCompaniaTelCliente() {
		return companiaTelCliente;
	}
    
    public String getJsonRapidos() {
    	return jsonRapidos;
    }

	/**
	 * @return the estatusAlertas
	 */
	public String getEstatusAlertas() {
		return estatusAlertas;
	}

	/**
	 * @return the fechaContratacion
	 */
	public String getFechaContratacion() {
		return fechaContratacion;
	}

	/**
	 * @return the catTelefonicas
	 */
	public String getCatTelefonicas() {
		return catTelefonicas;
	}

	/**
	 * @return the nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}
	//One click
		public String getIsPromocion() {
			return isPromocion;
		}

		public void setIsPromocion(String isPromocion) {
			this.isPromocion = isPromocion;
		}

	/**
     * Process the login response and store the attributes.
     * @param parser reference to the parser
     * @throws IOException on communication errors
     * @throws ParsingExceptionCredit on parsing errors
     */
    public void process(Parser parser) throws IOException, ParsingException {
    	//MODIFIED BY Michael Andrade
    	//Parsing new server response
    	// Parse >> numero de cliente
    	clientNumber = parser.parseNextValue("TE");
    	
    	// Parse >> fecha del sistema
/*    	serverDate = parser.parseNextValue("FE");
    	serverTime = parser.parseNextValue("HR");
    	
    	// Parse >> timeout
    	String timeoutStr = parser.parseNextValue("TO");
        try {
            timeout = Integer.parseInt(timeoutStr);
        } catch (Throwable th) {
            timeout = 1;
        }
        
        // Parse >> cuentas 
        // TODO: duda. Seg�n la respuesta a dicha duda se obvian todos los parametros de entrada as� que podemos pasar la lista vac�a que no pasar� nada ... 
        parseAccounts(parser, serverDate);
        
        catalogs = new Catalog[4];
        catalogVersions = new HashMap<String, String>();
        String next = parseCatalogs(parser,catalogs,catalogVersions);
        
        message = next.substring(2, next.length()); //MP value is required
        
        // Parse >> mensaje en caso de update
        updateMessage = parser.parseNextValue("ME", false);
        
        // Parse >> url update 
        updateURL = parser.parseNextValue("UR", false);
        
        // Parse >> via
        via = parser.parseNextValue("VA");
         
        estatusInstrumento = parser.parseNextValue("EI");
        companiaTelCliente = parser.parseNextValue("CM", false);
        emailCliente = parser.parseNextValue("EM", false);
        perfilCliente = parser.parseNextValue("PR", false);
        insSeguridadCliente = parser.parseNextValue("IS", false);
        
        // Parse >> rapidos 
        jsonRapidos = parser.parseNextValue("RP", false);
        if(jsonRapidos != null ) parseRapidos(jsonRapidos, listaRapidos);
        
        
        String tmpEntity;
        do {
        	tmpEntity = parser.parseNextEntity();
        } while(!tmpEntity.equals("TA") && !tmpEntity.contains("DM") && !tmpEntity.contains("SV") && !tmpEntity.contains("ST") && !tmpEntity.contains("MS"));
        
        if (tmpEntity.contains("ST")) {
        	estatusServicio = tmpEntity.substring(2);
        	parser.parseNextValue("ST", false);
        } else {
        	if (tmpEntity.contains("TA")) {
        		catalogoTA = parseCompanyCatalog(parser);
            	catalogVersions.put(ServerConstants.VERSION_TA, catalogoTA.getVersionCatalogo());
            	tmpEntity = parser.parseNextEntity();
        	}
        	
        	if (tmpEntity.contains("DM")) {
        		catalogoDineroMovil = parseDineroCatalog(parser);
        		catalogVersions.put(ServerConstants.VERSION_DM, catalogoDineroMovil.getVersionCatalogo());
        		tmpEntity = parser.parseNextEntity();
        	}
        	
        	if (tmpEntity.contains("SV")) {
        		catalogoConvenios = parseConvenioCatalog(parser);
        		catalogVersions.put(ServerConstants.VERSION_SV, catalogoConvenios.getVersionCatalogo());
        		tmpEntity = parser.parseNextEntity();
        	}
        	
        	if (tmpEntity.contains("MS")) {
        		catalogoMantenimientoSPEI = parseSpeiCatalog(parser);
        		catalogVersions.put(ServerConstants.VERSION_MS, catalogoMantenimientoSPEI.getVersionCatalogo());
        		tmpEntity = "";
        	}
    		
        	if (tmpEntity.equals("")) {
        		estatusServicio = parser.parseNextValue("ST");
        	} else if (tmpEntity.length() > 3){
        		estatusServicio = tmpEntity.substring(2, tmpEntity.length());
        	} else {
        		estatusServicio = "";
        	}
        }
    	
    	nombreCliente = parser.parseNextValue("NO", false);
    	autenticacionJson = parser.parseNextValue("AU", false);
    	limiteOperacion = parser.parseNextValue("LO");
    	
    	estatusAlertas = parser.parseNextValue("EA");
    	
    	fechaContratacion = parser.parseNextValue("FC");
    	if (null == fechaContratacion) fechaContratacion = "";
    	
    	// Parse >> telfonicas
    	if(!estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
    		catTelefonicas = parser.parseNextValue("TM", false);
    	}
    	
    	if(estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)){
    		candidatoPromociones = parser.parseNextValue("PM");
    		
//    		Si tiene promociones hacemos este amago de locura ?? why ? 
//    		if(candidatoPromociones.compareTo("SI")==0){ 
//    			jSonPromociones= parser.parseNextValue("LP", false);
//
//	            if(!Tools.isEmptyOrNull(jSonPromociones))
//	            	jSonPromociones = jSonPromociones.replaceAll("\":\"\"", "\":[]");
//	            
//	            Session.getInstance(MainController.getInstance().getContext()).parsePromocionesJson(jSonPromociones);
//    	    }
            
            String listaPromociones = parser.parseNextValue("LP", false);
            listaPromocionesJSON = parseListaPromociones(listaPromociones);
            
            contratoBBVAlink = parser.parseNextValue("LK");
            
            String stringMarcaTendero = parser.parseNextValue("TN", false);
            marcaTendero = parseMarcaTendero(stringMarcaTendero);
            
            String stringListaCuentas = parser.parseNextValue("CS", false);
            listaCuentas = parseListaCuentas(parser,stringListaCuentas);
            
    	}*/
    	
    }

    /**
     * Parse the account list.
     * @param parser reference to the parse
     * @param date the server date
     * @return the account list
     * @throws IOException on input output errors
     * @throws ParsingExceptionCredit on communication errors
     * @throws NumberFormatException on parsing errors
     */
    private static void parseAccounts(Parser parser, String date) throws IOException, NumberFormatException, ParsingException{
        String accountType;
        String currency;
        String accountNumber;
        String amount;
        String concept;
        String visible;
        String alias;
        parser.parseNextValue("CT");
        int accounts = Integer.parseInt(parser.parseNextValue("OC"));
        //Account[] result = new Account[accounts];
        //Account account;
        for (int i = 0; i < accounts; i++) {
            accountType = parser.parseNextValue("TP");
            alias = parser.parseNextValue("AL");
            currency = parser.parseNextValue("DV");
            accountNumber = parser.parseNextValue("AS");
            amount = parser.parseNextValue("IM");
            concept = parser.parseNextValue("CP");
            visible = parser.parseNextValue("VI");
            //account = new Account(accountNumber, Tools.getDoubleAmountFromServerString(amount), Tools.formatDate(date), "S".equals(visible), currency, accountType, concept, alias);
            //result[i] = account;
        }
        //return result;
    }
	/**
     * Parse the account list.
     * @param parser reference to the parse
     * @param date the server date
     * @return the account list
     * @throws IOException on input output errors
     * @throws ParsingExceptionCredit on communication errors
     * @throws NumberFormatException on parsing errors
     */
    private static JsonListaCuentas parseListaCuentas(Parser parser, String listaCuentasJSON) throws IOException, NumberFormatException, ParsingException {
        String currency = "cuenta";
        String objeto = "objeto";
        String numTarjeta = "numeroTarjeta";
        String saldo = "saldo";
        String visible = "visible";
        String moneda = "moneda";
        String tipoCuenta = "tipoCuenta";
        String concepto = "concepto";
        String alias = "alias";
        String celularAsociado = "celularAsociado";
        String codigoComp = "codigoCompania";
        String descripcionComp = "descripcionCompania";
        String fechaUM = "fechaUltimaModificacion";
        String indicadorSPEI = "indicadorSPEI";


        JSONObject mainObj;
        JsonListaCuentas ret = new JsonListaCuentas();
        try {
			 mainObj = new JSONObject(listaCuentasJSON);
			 JSONArray mainArray = (JSONArray) mainObj.get("cuentas");
			 
			 ArrayList<JsonCuenta> lista = new ArrayList<JsonCuenta>();
			 JsonCuenta c = new JsonCuenta();
			 JSONObject obj = null;
			 JSONObject cuentaObjeto = null;
			 for (int i = 0; i < mainArray.length(); i++) {
				 obj = mainArray.getJSONObject(i);
				 c.setOcurrencia(obj.getString(currency));
				 // Cada registro del array tiene un subarray en "Objeto"
				 cuentaObjeto = obj.getJSONObject(objeto);
				 
				 c.setNumeroTarjeta(cuentaObjeto.getString(numTarjeta));
				 c.setBalance(cuentaObjeto.getString(saldo));
				 c.setVisible(cuentaObjeto.getString(visible));
				 c.setMoneda(cuentaObjeto.getString(moneda));
				 c.setTipoCuenta(cuentaObjeto.getString(tipoCuenta));
				 c.setConcepto(cuentaObjeto.getString(concepto));
				 c.setAlias(cuentaObjeto.getString(alias));
				 c.setCelularAsociado(cuentaObjeto.getString(celularAsociado));
				 c.setCodigoCompania(cuentaObjeto.getString(codigoComp));
				 c.setDescripcionCompania(cuentaObjeto.getString(descripcionComp));
				 c.setFechaUltimaModificacion(cuentaObjeto.getString(fechaUM));
				 c.setIndicadorSPEI(cuentaObjeto.getString(indicadorSPEI));
				 
				 lista.add(c);
				 
			 }
			 
			 ret.setLista(lista);
			 
        } catch (JSONException e) {
			Log.d("JSON Login Data Rapidas", "Something is wrong here!");
		}
        
        return ret;
    }


	 private static void parseRapidos(String daJson, ArrayList<Rapido> listaRapidos){
		 String nombreCorto = "nombreCorto";
		 String cuentaOrigen = "cuentaOrigen";
		 String cuentaDestino = "cuentaDestino";
		 String telefonoDestino = "telefonoDestino";
		 String importe = "importe";
		 String nombreBeneficiario = "nombreBeneficiario";
		 String companiaCelular = "companiaCelular";
		 String tipoRapido = "tipoRapido";
		 String iDOperacion = "iDOperacion";
		 String concepto = "concepto";
		
		 JSONObject mainObj;
		 try {
			 mainObj = new JSONObject(daJson);
		
			 JSONArray mainArray = (JSONArray) mainObj.get("rapidas");
			 
			 Rapido r = new Rapido();
			 JSONObject obj = null;
			 listaRapidos = new ArrayList<Rapido>();
			 for (int i = 0; i < mainArray.length(); i++) {
				 obj = mainArray.getJSONObject(i);
				 r.setNombreCorto(obj.getString(nombreCorto));
				 r.setCuentaOrigen(obj.getString(cuentaOrigen));
				 r.setCuentaDestino(obj.getString(cuentaDestino));
				 r.setTelefonoDestino(obj.getString(telefonoDestino));
				 
				 String auxImporte = obj.getString(importe);
				 if(Tools.isEmptyOrNull(auxImporte)){
					 r.setImporte(0);
				 }else{
					 r.setImporte(Integer.valueOf(auxImporte));
				 }
				 
				 r.setBeneficiario(obj.getString(nombreBeneficiario));
				 r.setCompaniaCelular(obj.getString(companiaCelular));
				 r.setTipoRapido(obj.getString(tipoRapido));
				 r.setIdOperacion(obj.getString(iDOperacion));
				 r.setConcepto(obj.getString(concepto));
				 
				 listaRapidos.add(r);
			}
		} catch (JSONException e) {
			Log.d("JSON Login Data Rapidas", "Something is wrong here!");
		}
	}
	
    /**
     * Parse a catalog.
     * @param parser a reference to the parser
     * @return the catalog
     * @throws IOException on communication errors
     * @throws ParsingExceptionCredit on parsing errors
     * @throws NumberFormatException on format errors
     */
    private static Catalog parseCatalog(Parser parser) throws IOException, NumberFormatException, ParsingException {
        Catalog result = new Catalog();
        String entry;
        String code;
        String value;
        int entries = Integer.parseInt(parser.parseNextValue("OC"));
        for (int i = 0; i < entries; i++) {
            entry = parser.parseNextEntity(true);
            if (entry != null) {
                int pos = entry.indexOf('-');
                code = entry.substring(0, pos);
                value = entry.substring(pos + 1);
                result.add(code, value);
            }
        }
        return result;
    }

    /**
     * Parse a catalog.
     * @param parser a reference to the parser
     * @return the catalog
     * @throws IOException on communication errors
     * @throws ParsingExceptionCredit on parsing errors
     * @throws NumberFormatException on format errors
     */
    private static ListaTiempoAire parseCompanyCatalog(Parser parser) throws IOException, NumberFormatException, ParsingException {
        String nombre;
        String imagen;
        String clave;
        String montos;
        String orden;
        String version = parser.parseNextValue("VE");
        int entries = Integer.parseInt(parser.parseNextValue("OC"));
        

        ListaTiempoAire ta = new ListaTiempoAire();
        ta.setVersionCatalogo(version);
        ta.setOcurrencia(entries);
        
        ArrayList<TiempoAire> lista = new ArrayList<TiempoAire>();
        TiempoAire tiempoA;
        
        for (int i = 0; i < entries; i++) {
            nombre = parser.parseNextValue("OA");
            imagen = parser.parseNextValue("MG");
            clave = parser.parseNextValue("CV");
            montos = parser.parseNextValue("IM");
            orden = parser.parseNextValue("OR");
            
            tiempoA = new TiempoAire(nombre, imagen, clave, montos, orden);
            lista.add(tiempoA);
        }
        
        ta.setLista(lista);
        
        return ta;
    }
    
    /**
     * Parse a catalog.
     * @param parser a reference to the parser
     * @return the catalog
     * @throws IOException on communication errors
     * @throws ParsingExceptionCredit on parsing errors
     * @throws NumberFormatException on format errors
     */
    private static ListaDineroMovil parseDineroCatalog(Parser parser) throws IOException, NumberFormatException, ParsingException{
        String nombre;
        String imagen;
        String clave;
        String montos;
        String orden;
        String version = parser.parseNextValue("VE");
        int entries = Integer.parseInt(parser.parseNextValue("OC"));
        

        ListaDineroMovil dm = new ListaDineroMovil();
        dm.setVersionCatalogo(version);
        dm.setOcurrencia(entries);
        
        ArrayList<DineroMovil> lista = new ArrayList<DineroMovil>();
        DineroMovil dineroM;
        
        for (int i = 0; i < entries; i++) {
            nombre = parser.parseNextValue("OA");
            imagen = parser.parseNextValue("MG");
            clave = parser.parseNextValue("CV");
            montos = parser.parseNextValue("IM");
            orden = parser.parseNextValue("OR");
            
            dineroM = new DineroMovil(nombre, imagen, clave, montos, orden);
            lista.add(dineroM);
        }
        
        dm.setLista(lista);
        
        return dm;
    }
    
    /**
     * Parse a catalog.
     * @param parser a reference to the parser
     * @return the catalog
     * @throws IOException on communication errors
     * @throws ParsingExceptionCredit on parsing errors
     * @throws NumberFormatException on format errors
     */
    private static ListaConvenioPagoServicios parseConvenioCatalog(Parser parser) throws IOException, NumberFormatException, ParsingException{
        String nombre;
        String imagen;
        String clave;
        String orden;
        String version = parser.parseNextValue("VE");
        int entries = Integer.parseInt(parser.parseNextValue("OC"));
        
        ListaConvenioPagoServicios convenioPS = new ListaConvenioPagoServicios();
        convenioPS.setOcurrencia(entries);
        convenioPS.setVersionCatalogo(version);
        
        ArrayList<ConvenioPagoServicios> lista = new ArrayList<ConvenioPagoServicios>();
        ConvenioPagoServicios convenio;
        
        for (int i = 0; i < entries; i++) {
        	clave = parser.parseNextValue("CI");
            imagen = parser.parseNextValue("MG");
            nombre = parser.parseNextValue("NM");
            orden = parser.parseNextValue("OR");
            
            convenio = new ConvenioPagoServicios(nombre, imagen, clave, orden);
            lista.add(convenio);
        }
        convenioPS.setLista(lista);
        
        return convenioPS;
    }
    
    private ListaMantenimientoSPEI parseSpeiCatalog(Parser parser) throws IOException, NumberFormatException, ParsingException{
    	
    	String nombre;
        String imagen;
        String clave;
        String orden;
    	String version = parser.parseNextValue("VE");
    	int entries = Integer.parseInt(parser.parseNextValue("OC"));
    	
        ListaMantenimientoSPEI mantenimiento = new ListaMantenimientoSPEI();
        mantenimiento.setOcurrencia(entries);
        mantenimiento.setVersionCatalogo(version);
        
        ArrayList<MantenimientoSPEI> lista = new ArrayList<MantenimientoSPEI>();
        MantenimientoSPEI mant;
    	
    	for (int i = 0; i < entries; i++) {
    		nombre = parser.parseNextValue("OA");
    		imagen = parser.parseNextValue("MG");
    		clave = parser.parseNextValue("CV");
            orden = parser.parseNextValue("OR");
            
            mant = new MantenimientoSPEI(nombre, imagen, clave, orden);
            lista.add(mant);
        }
    	mantenimiento.setLista(lista);
    	
        return mantenimiento;
        
    }
    
    
    
    private static ArrayList<Campania> parseListaPromociones(String listaPromociones) {
    	
    	ArrayList<Campania> listaCampaniasJSON = null;
    	
    	if ((listaPromociones != null) && (!"".equals(listaPromociones))) {
    		listaCampaniasJSON = new ArrayList<Campania>();
    		JSONObject jsonObject = null;
        	
        	try {
        		jsonObject = new JSONObject(listaPromociones);
    		} catch (JSONException ex) {
    			jsonObject = null;
    			Log.e("LoginData", "Error while trying to parse Json.", ex);
    		}
        	
        	if (null != jsonObject){

        		try {
        			JSONArray arrayCampanias = jsonObject.getJSONArray("campanias");
    			
        			for (int i = 0; i < arrayCampanias.length(); i++) {
        				JSONObject campaniaJSON = arrayCampanias.getJSONObject(i);
        				String claveCampania = campaniaJSON.getString("cveCamp");
        				String descripcionOferta = campaniaJSON.getString("desOferta");
        				String monto = campaniaJSON.getString("monto");
    				
        				Campania campania = new Campania(claveCampania, descripcionOferta, monto);
        				listaCampaniasJSON.add(campania);
    				}
    			
        		} catch (JSONException ex) {
        			Log.e("LoginData", "Error while reading the JSON.", ex);
        		}
        	}
    	}
    	
    	return listaCampaniasJSON;
    }
    
    private String parseMarcaTendero(String stringTendero) {
    	JSONObject jsonObject = null;
    	String tendero = null;
    	
    	if ((stringTendero != null) && (!"".equals(stringTendero))) {
    		try {
        		jsonObject = new JSONObject(stringTendero);
        		tendero = jsonObject.getString("tendero");
    		} catch (JSONException ex) {
    			jsonObject = null;
    			Log.e("LoginData", "Error while trying to parse Json.", ex);
    		}
    	}
    	
    	return tendero;
    }
    
    /**
     * Parse the collection of catalogs.
     * @param parser a reference to the parser
     * @param catalogs (out) parsed catalogs
     * @param catalogVersions version of catalog
     * @return the optional message
     * @throws IOException on communication errors
     * @throws ParsingExceptionCredit on parsing errors
     */
    private static String parseCatalogs(Parser parser,
    		Catalog[] catalogs, HashMap<String, String> catalogVersions)
    		throws IOException, ParsingException {
        String next = null;
        String entity;
        boolean finished = false;
        do {
            entity = parser.parseNextEntity();
            if (entity == null) {
                next = null;
                finished = true;
            } else {
                if (entity.startsWith("C1")) {
                    catalogVersions.put(ServerConstantsCredit.VERSION_C1, entity.substring(2));
                    catalogs[0] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C1=" + catalogVersions[0]);
                } else if (entity.startsWith("C4")) {
                	catalogVersions.put(ServerConstantsCredit.VERSION_C4, entity.substring(2));
                    catalogs[1] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C2=" + catalogVersions[1]);
                } else if (entity.startsWith("C5")) {
                	catalogVersions.put(ServerConstantsCredit.VERSION_C5, entity.substring(2));
                    catalogs[2] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C3=" + catalogVersions[2]);
                } else if (entity.startsWith("C8")) {
                	catalogVersions.put(ServerConstantsCredit.VERSION_C8, entity.substring(2));
                    catalogs[3] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C4=" + catalogVersions[3]);
                } else if (entity.startsWith("TA")) {
                	catalogVersions.put(ServerConstantsCredit.VERSION_TA, entity.substring(2));
                    catalogs[4] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C5=" + catalogVersions[4]);
                } else if (entity.startsWith("DM")) {
                	catalogVersions.put(ServerConstantsCredit.VERSION_DM, entity.substring(2));
                    catalogs[5] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C6=" + catalogVersions[5]);
                } else if (entity.startsWith("SV")) {
                	catalogVersions.put(ServerConstantsCredit.VERSION_SV, entity.substring(2));
                    catalogs[6] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C7=" + catalogVersions[6]);
                } else if (entity.startsWith("MS")) {
                	catalogVersions.put(ServerConstantsCredit.VERSION_MS, entity.substring(2));
                	catalogs[7] = parseCatalog(parser);
                } else {
                    next = entity;
                    finished = true;
                }
            }
        } while (!finished);


        return next;
    }

	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
