package bbvacredit.bancomer.apicredit.gui.activities;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
//import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageButton;
import android.util.Log;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.gui.delegates.PrestamosContratadosDelegate;


public class ContratadosDescripcion extends Fragment implements View.OnClickListener{

    PrestamosContratadosDelegate delegate;
    private ImageButton btnDespliegueArriba;
    private ImageButton btnDespliegueAbajo;

    public ContratadosDescripcion() {
        // Required empty public constructor
    }

    public void setDelegate(PrestamosContratadosDelegate delegate) {
        this.delegate = delegate;
    }

    // TODO: Rename and change types and number of parameters
    public static ContratadosDescripcion newInstance() {
        ContratadosDescripcion fragment = new ContratadosDescripcion();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_arriba_prestamos_contratados, container, false);
        btnDespliegueArriba = (ImageButton) rootView.findViewById(R.id.imgDespliegueArriba);
        btnDespliegueArriba.setOnClickListener(this);

        btnDespliegueAbajo = (ImageButton) rootView.findViewById(R.id.imgDespliegueAbajo);
        btnDespliegueAbajo.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.imgDespliegueArriba) {
            Log.d("ContratadosDescripcion", "onClick Arriba");
            delegate.botonArriba();
        } else if (v.getId() == R.id.imgDespliegueAbajo) {
            Log.d("ContratadosDescripcion", "onClick Abajo");
            delegate.botonAbajo();
        }
    }

}
