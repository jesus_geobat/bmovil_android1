package bbvacredit.bancomer.apicredit.gui.activities;

import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.PrestamosContratadosDelegate;
import bbvacredit.bancomer.apicredit.models.Producto;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ContratadosFragment extends Fragment implements View.OnClickListener {

    private LinearLayout lytAbajo;
    int fragVal;
    ImageView iconoTipoPrestamo;
    int cambioIcono;
    Button resumen;
    private Button btnVerResumenPagos;
    private String cveProd;
    private String leyendaProd;
    private Integer saldo;
    TextView txtLeyendaTipoPrestamo;
    TextView txtLeyendaSaldo;
    boolean showContratar;
    ImageButton btnDerecha;
    ImageButton btnIzquierda;
    TextView lblTituloLiquidacion;
    RelativeLayout producto1Layout;
    RelativeLayout producto2Layout;
    RelativeLayout producto3Layout;
    RelativeLayout producto4Layout;
    RelativeLayout producto5Layout;
    RelativeLayout producto6Layout;
    boolean showProducto1;
    boolean showProducto2;
    boolean showProducto3;
    boolean showProducto4;
    boolean showProducto5;
    boolean showProducto6;
    int productosAMostrar;
    ImageView imgSimulado1;
    ImageView imgSimulado2;
    ImageView imgSimulado3;
    ImageView imgSimulado4;
    ImageView imgSimulado5;
    ImageView imgSimulado6;
    ArrayList<Producto> arrayProductos;
    TextView lblMontoCredito1;
    TextView lblMontoCredito2;
    TextView lblMontoCredito3;
    TextView lblMontoCredito4;
    TextView lblMontoCredito5;
    TextView lblMontoCredito6;
    PrestamosContratadosDelegate delegate;

    public ContratadosFragment() {
        cambioIcono = 0;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragVal = getArguments() != null ? getArguments().getInt("val") : 1;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_prestamos_contratados, container, false);

        lytAbajo = (LinearLayout) rootView.findViewById(R.id.deplegadoArribaPrestamosContratados);
        iconoTipoPrestamo = (ImageView) rootView.findViewById(R.id.iconoTipoPrestamo);
        txtLeyendaTipoPrestamo = (TextView) rootView.findViewById(R.id.leyendaTipoPrestamo);
        txtLeyendaSaldo = (TextView) rootView.findViewById(R.id.leyendaSaldo);

        if(cambioIcono > 0) {
            iconoTipoPrestamo.setImageResource(cambioIcono);
            txtLeyendaTipoPrestamo.setText(leyendaProd);
            txtLeyendaSaldo.setText(Tools.formatAmount(saldo.toString(), false));
        }
        if(showContratar)
            lytAbajo.setVisibility(View.VISIBLE);
        else
            lytAbajo.setVisibility(View.GONE);
        resumen = (Button) rootView.findViewById(R.id.btnVerResumenPagos);
        resumen.setOnClickListener(this);

        btnVerResumenPagos = (Button) rootView.findViewById(R.id.btnVerResumenPagos);
        btnVerResumenPagos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainController.getInstance().showScreen(ResumenViewController.class);

                SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
                SharedPreferences.Editor editor = sp.edit();
                editor.putBoolean(ConstantsCredit.IS_SHOWN, false);
                editor.commit();
            }
        });

        btnDerecha = (ImageButton) rootView.findViewById(R.id.btnIzquierda);
        btnDerecha.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                int item = PrestamosContratadosViewController.getMPager().getCurrentItem();
                Log.e("ITEM---", String.valueOf(item));
                PrestamosContratadosViewController.getMIndicator().setCurrentItem(item+1);
                delegate.botonDerecha();
            }
        });
        btnIzquierda = (ImageButton) rootView.findViewById(R.id.btnDerecha);
        btnIzquierda.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                int item = PrestamosContratadosViewController.getMPager().getCurrentItem();
                Log.e("ITEM---", String.valueOf(item));
                if(item > 0){
                    PrestamosContratadosViewController.getMIndicator().setCurrentItem(item-1);
                    delegate.botonIzquierda();
                }
            }
        });

        lblTituloLiquidacion = (TextView) rootView.findViewById(R.id.lblTituloLiquidacion);

        producto1Layout = (RelativeLayout) rootView.findViewById(R.id.producto1);
        producto2Layout = (RelativeLayout) rootView.findViewById(R.id.producto2);
        producto3Layout = (RelativeLayout) rootView.findViewById(R.id.producto3);
        producto4Layout = (RelativeLayout) rootView.findViewById(R.id.producto4);
        producto5Layout = (RelativeLayout) rootView.findViewById(R.id.producto5);
        producto6Layout = (RelativeLayout) rootView.findViewById(R.id.producto6);

        producto1Layout = (RelativeLayout) rootView.findViewById(R.id.producto1);

        imgSimulado1 = (ImageView) rootView.findViewById(R.id.imgSimulado1);
        imgSimulado2 = (ImageView) rootView.findViewById(R.id.imgSimulado2);
        imgSimulado3 = (ImageView) rootView.findViewById(R.id.imgSimulado3);
        imgSimulado4 = (ImageView) rootView.findViewById(R.id.imgSimulado4);
        imgSimulado5 = (ImageView) rootView.findViewById(R.id.imgSimulado5);
        imgSimulado6 = (ImageView) rootView.findViewById(R.id.imgSimulado6);

        lblMontoCredito1 = (TextView) rootView.findViewById(R.id.lblMontoCredito1);
        lblMontoCredito2 = (TextView) rootView.findViewById(R.id.lblMontoCredito2);
        lblMontoCredito3 = (TextView) rootView.findViewById(R.id.lblMontoCredito3);
        lblMontoCredito4 = (TextView) rootView.findViewById(R.id.lblMontoCredito4);
        lblMontoCredito5 = (TextView) rootView.findViewById(R.id.lblMontoCredito5);
        lblMontoCredito6 = (TextView) rootView.findViewById(R.id.lblMontoCredito6);

        return rootView;
    }

    public void showContratar(ArrayList<Producto> arrayProductos) {

        showContratar = true;
        if(lytAbajo != null)
            lytAbajo.setVisibility(View.VISIBLE);

        if(lblTituloLiquidacion != null)
            lblTituloLiquidacion.setVisibility(View.VISIBLE);

        this.arrayProductos = arrayProductos;

        productosAMostrar = arrayProductos.size();

        switch (productosAMostrar) {
            case 6:
                producto6Layout.setVisibility(View.VISIBLE);
            case 5:
                producto5Layout.setVisibility(View.VISIBLE);
            case 4:
                producto4Layout.setVisibility(View.VISIBLE);
            case 3:
                producto3Layout.setVisibility(View.VISIBLE);
            case 2:
                producto2Layout.setVisibility(View.VISIBLE);
            case 1:
                producto1Layout.setVisibility(View.VISIBLE);
                break;
        }

        for(int i=0; i<arrayProductos.size(); i++) {
            Producto prod = (Producto) arrayProductos.get(i);
            int resource = getResourceProduct(prod.getCveProd());
            String montoFormateado = Tools.formatAmount2(prod.getMontoMaxS());

            if(resource == 0)
                continue;

            switch (i) {
                case 0:
                    imgSimulado1.setImageResource(resource);
                    lblMontoCredito1.setText(montoFormateado);
                    break;
                case 1:
                    imgSimulado2.setImageResource(resource);
                    lblMontoCredito2.setText(montoFormateado);
                    break;
                case 2:
                    imgSimulado3.setImageResource(resource);
                    lblMontoCredito3.setText(montoFormateado);
                    break;
                case 3:
                    imgSimulado4.setImageResource(resource);
                    lblMontoCredito4.setText(montoFormateado);
                    break;
                case 4:
                    imgSimulado5.setImageResource(resource);
                    lblMontoCredito5.setText(montoFormateado);
                    break;
                case 5:
                    imgSimulado6.setImageResource(resource);
                    lblMontoCredito6.setText(montoFormateado);
                    break;
            }
        }

    }

    public void hideContratar() {
        showContratar = false;
        if(lytAbajo != null)
            lytAbajo.setVisibility(View.GONE);

        if(lblTituloLiquidacion != null)
            lblTituloLiquidacion.setVisibility(View.GONE);
    }

    public void cambiaIcono(int resourceIcono) {
        cambioIcono = resourceIcono;
    }

    @Override
    public void onClick(View v) {
        MainController.getInstance().showScreen(ResumenViewController.class);
    }

    public void setCveProd(String cveProd) {
        this.cveProd = cveProd;
    }

    public void setSaldo(Integer saldo) {
        this.saldo = saldo;
    }

    public void cambiaValores() {
        if(cveProd.equals(ConstantsCredit.CREDITO_NOMINA)) {
            leyendaProd = "Nómina";
            cambioIcono = R.drawable.ic_nominan;
        } else if(cveProd.equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)) {
            leyendaProd = "Personal";
            cambioIcono = R.drawable.ic_personaln;
        } else if(cveProd.equals("0HIP") || cveProd.equals(ConstantsCredit.CREDITO_HIPOTECARIO)) {
            leyendaProd = "Hipotecario";
            cambioIcono = R.drawable.ic_hipotecarion;
        } else if(cveProd.equals(ConstantsCredit.CREDITO_AUTO)) {
            leyendaProd = "Auto";
            cambioIcono = R.drawable.ic_auton;
        }
    }

    private int getResourceProduct(String codigoProducto) {
        int result = 0;

        if(codigoProducto.equalsIgnoreCase(ConstantsCredit.TARJETA_CREDITO))
            result = R.drawable.ic_tdc;
        else if(codigoProducto.equalsIgnoreCase(ConstantsCredit.INCREMENTO_LINEA_CREDITO))
            result = R.drawable.ic_ilc;
        else if(codigoProducto.equalsIgnoreCase(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO))
            result = R.drawable.ic_personal;
        else if(codigoProducto.equalsIgnoreCase(ConstantsCredit.CREDITO_NOMINA))
            result = R.drawable.ic_nomina;
        //else if(codigoProducto.equalsIgnoreCase(ConstantsCredit.CREDITO_AUTO))
        //TODO: verificar el elemento adecuado, este aparece muy grande
        else if(codigoProducto.equalsIgnoreCase(ConstantsCredit.CREDITO_HIPOTECARIO) ||
                codigoProducto.equalsIgnoreCase("HIPO"))
            result = R.drawable.ic_hipotecariog;

        return result;
    }

    public void setDelegate(PrestamosContratadosDelegate delegate) {
        this.delegate = delegate;
    }
}
