package bbvacredit.bancomer.apicredit.gui.delegates;

import android.content.SharedPreferences;
import android.util.Log;

import java.util.Hashtable;
import java.util.Iterator;

import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.DetalleILCViewController;
import bbvacredit.bancomer.apicredit.gui.activities.ILCViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ConsultaDatosTDCData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;

/**
 * Created by OOROZCO on 12/2/15.
 */
public class ILCDelegate extends BaseDelegateOperacion {


    // ----------- Attributes Region ----------------- //
    private Session session;
    private boolean isSavingOperation = false;
    private ObjetoCreditos data;
    private Producto producto = null;
    private Integer productIndex = 0;
    private boolean isDeleteOperation = false; //si es true, borrar simulación

    private ILCViewController controller;
    private DetalleILCViewController detalleILC;

    public DetalleILCViewController getDetalleILC() {
        return detalleILC;
    }

    public void setDetalleILC(DetalleILCViewController detalleILC) {
        this.detalleILC = detalleILC;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setController(ILCViewController controller) {
        this.controller = controller;
    }

    public ILCDelegate() {
        session = Tools.getCurrentSession();

        MainController.getInstance().muestraIndicadorActividad("Operación", "Conectando");

        data = session.getDataFromCreditos();

        setProducto();

        MainController.getInstance().ocultaIndicadorActividad();
    }

    public ObjetoCreditos getData() {
        return data;
    }

    private void setProducto(){
        SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
        productIndex = sp.getInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, 0);

        producto = data.getProductos().get(productIndex);
    }


    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }


    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(Server.CALCULO_ALTERNATIVAS_OPERACION,ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        if(isSavingOperation) {
            addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = false;
        }else {
            addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = true;
        }
        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    public void doRecalculoContract(){

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);
        Producto ccr = data.getProductos().get(productIndex);

        int montoSel = Integer.valueOf(controller.getMonto());
        Log.w("Valor del monto",controller.getMonto());

        addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM,paramTable);
        addParametro(ccr.getSubproducto().get(0).getCveSubp(), ServerConstantsCredit.CVESUBP_PARAM,paramTable);
        addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM,paramTable);
        addParametro("", ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    public void analyzeResponse(String operationId, ServerResponse response) {
        if(getCodigoOperacion().equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                productIndex = session.getProductoIndexByCve(ConstantsCredit.INCREMENTO_LINEA_CREDITO);
                producto = data.getProductos().get(productIndex);
                if(data.getProductos().get(productIndex).getCveProd().equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
                    data.getProductos().get(productIndex).setIndSimBoolean(true);
                }else{
                    setSimLooking4Cve(ConstantsCredit.INCREMENTO_LINEA_CREDITO);
                }

                MainController.getInstance().ocultaIndicadorActividad();
                //At this point I gotta update the credits.
                session.setCreditos(data);
                setProducto();
                saveOrDeleteSimulationRequest(true);
            }
        } else if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                if(isDeleteOperation){

                    CalculoData respuesta = (CalculoData) response.getResponse();
                    data = null;
                    data = new ObjetoCreditos();

                    data.setCreditos(respuesta.getCreditos());
                    data.setEstado(respuesta.getEstado());
                    data.setMontotSol(respuesta.getMontotSol());
                    data.setPagMTot(respuesta.getPagMTot());
                    data.setPorcTotal(respuesta.getPorcTotal());
                    data.setProductos(respuesta.getProductos());
                    data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                    productIndex = session.getProductoIndexByCve(ConstantsCredit.INCREMENTO_LINEA_CREDITO);
                    producto = data.getProductos().get(productIndex);

                    Log.i("simulador","ilcDelegate, getIndsim: "+data.getProductos().get(productIndex).getIndSim());

                    //At this point I gotta update the credits.
                    session.setCreditos(data);

                    Log.i("eliminar","se eliminó operacion ILC");
                    isDeleteOperation = false;
                    setProducto();
                    getDetalleILC().simpleUpdateMenu(producto.getCveProd());
                }
                else
                    oneClickILC();
            }
        }
    }

    private void setSimLooking4Cve(String cve){
        Iterator<Producto> it = data.getProductos().iterator();

        while(it.hasNext()){
            Producto pr = it.next();
            if(pr.getCveProd().equals(cve)) pr.setIndSimBoolean(true);
        }
    }

    @Override
    protected String getCodigoOperacion() {
        return Server.CALCULO_OPERACION;
    }

    public void oneClickILC() {
        Session.getInstance().setCveCamp(producto.getCveProd());
        Session session = Session.getInstance(MainController.getInstance().getContext());

        DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(session.getIdUsuario(), session.getIum(), session.getNumCelular(), producto.getCveProd(),null);
        delegate.setIlcViewController(controller);
        delegate.consultaDetalleAlternativasTask(getProducto());
    }


}
