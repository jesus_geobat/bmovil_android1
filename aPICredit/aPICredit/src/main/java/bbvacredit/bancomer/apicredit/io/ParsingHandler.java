package bbvacredit.bancomer.apicredit.io;

import java.io.IOException;

public interface ParsingHandler {
	/**
     * Process a server response
     * @param parser
     * @throws IOException
     * @throws ParsingException
     */
    public void process(Parser parser) throws IOException, ParsingException;
}
