package bbvacredit.bancomer.apicredit.gui.activities;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.AdelantoNominaDelegate;

/**
 * Created by Packo on 22/02/2016.
 */
public class DetalleAdelantoNomina extends Fragment {

    private AdelantoNominaDelegate delegate;

    // ------------- Attributes Region ----------- //

    private ImageButton btnRecalcularSimulacion;
    private ImageButton btnContratarANOM;
    private TextView lblResultadoPago;
    private TextView lblResultadoPlazo;
    private TextView lblResultadoTasa;
    private TextView lblCat;
    private TextView lblPagoMensual;
    private Button btnVerResumenPagos;
    private Session session;
    private boolean pagoMensual = true;
    private boolean pagoQuincenal = false;

    public void setDelegate(AdelantoNominaDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_credit_resultado_simulacion, container, false);
        delegate.setDealleANOM(this);
        findViews(rootView);

        ((MenuPrincipalActivity) getActivity()).setIsSimulate(true);
        ((MenuPrincipalActivity) getActivity()).setoFragment(this);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        session = Tools.getCurrentSession();
        Log.i("anom", "detalle, getCve: " + delegate.returnProducto().getCveProd());
//        Integer getProdIndex = session.getProductoIndexByCve(ConstantsCredit.ADELANTO_NOMINA);

        Log.i("opinator", "guardando cveProd: " + delegate.returnProducto().getCveProd());

        String cat2 = delegate.returnProducto().getCATS();
        String mesOquincena = delegate.returnProducto().getSubproducto().get(0).getCveSubp();
        //String pagoMQ = delegate.getData().getProductos().get(getProdIndex).getPagMProdS();
        String tasa = delegate.returnProducto().getTasaS();
        //android.util.Log.i("pago", "PagMPRodS: " + pagoMQ);
        String nota = "*El IVA puede variar en cada pago";

        String TotalMesOQuincena = delegate.returnProducto().getPagMProdS();
        TotalMesOQuincena = pagoMensualQuincenal(TotalMesOQuincena,
                mesOquincena.equals(ConstantsCredit.CODIGO_MENSUAL)?pagoMensual:pagoQuincenal);

        String totalPagar = totalAPagar(delegate.returnProducto().getPagMProdS());

        String cat= getActivity().getString(R.string.lblAnom1) + " " + totalPagar + " "+ //dato que aún no proporcionan
                getActivity().getString(R.string.lblAnom2) + " " +"0%" + " sin IVA. "+ //datos en duro
                getActivity().getString(R.string.lblAnom4) + " " + cat2 +
                getActivity().getString(R.string.lblAnom5) + " " + delegate.getProducto().getFechaCatS() + ".";

        //si cliente = CD02, pago mensual. Si cliente CD01, pago quincenal
        lblPagoMensual.setText(mesOquincena.equals(ConstantsCredit.CODIGO_MENSUAL) ?
                getResources().getString(R.string.detalle_adelanto_nomina_pago_mensual) :
                getResources().getString(R.string.detalle_adelanto_nomina_pago_quincenal));
        lblResultadoTasa.setText("Más comisión: "+tasa +"% sin IVA."+"\n"+nota);

        lblResultadoPago.setText(GuiTools.getMoneyString(TotalMesOQuincena));//GuiTools.getMoneyString(delegate.getData().getPagMTot()));
        lblResultadoPlazo.setText(mesOquincena.equals(ConstantsCredit.CODIGO_MENSUAL) ?
                getResources().getString(R.string.detalle_adelanto_nomina_plazo_mensual) :
                getResources().getString(R.string.detalle_adelanto_nomina_plazo_quincenal));//delegate.getProducto().getDesPlazoE());

        lblCat.setText(cat);
    }


    public void findViews(View root){
        btnRecalcularSimulacion = (ImageButton) root.findViewById(R.id.btnRecalcularSimulacion);
        btnContratarANOM  = (ImageButton) root.findViewById(R.id.btnContratarANOM);
        btnContratarANOM.setImageResource(R.drawable.txt_contratar); //Here my doubts.
        btnVerResumenPagos      = (Button) root.findViewById(R.id.btnVerResumenPagos);

        lblResultadoPago        = (TextView) root.findViewById(R.id.lblResultadoPago);
        lblResultadoPlazo       = (TextView) root.findViewById(R.id.lblResultadoPlazo);
        lblResultadoTasa        = (TextView) root.findViewById(R.id.lblResultadoTasa);
        lblPagoMensual          = (TextView) root.findViewById(R.id.lblPagoMensual);
        lblCat                  = (TextView) root.findViewById(R.id.lblCat);

        btnRecalcularSimulacion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                delegate.saveOrDeleteSimulationRequest(false);
            }
        });

        btnContratarANOM.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                adquiereANOM();
            }
        });

        btnVerResumenPagos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainController.getInstance().showScreen(ResumenViewController.class);
                MainController.getInstance().setFlowFragment(DetalleAdelantoNomina.this);
            }
        });
    }

    public String pagoMensualQuincenal(String valorPago, boolean mensualQincenal){
        double pagoMensualQuincenal = Double.parseDouble(valorPago);

        if(!mensualQincenal)
            pagoMensualQuincenal = pagoMensualQuincenal/2;

        return String.valueOf(pagoMensualQuincenal);

    }

    public String totalAPagar(String mensOQuin){ //true si es pago mensual, false si es quincenal
        String valorTotal = "";

        Log.w("anom","mensOQuin: "+mensOQuin);

        if(mensOQuin.equals("0") || mensOQuin.equals("0.0") || mensOQuin.equals("0.00")){
            valorTotal = "$" + mensOQuin;
        }else {
            String auxmensOQuin = mensOQuin;

            if (auxmensOQuin.contains(","))
                auxmensOQuin = auxmensOQuin.replace(",","");

            Log.i("anom","auxMensoquin: "+auxmensOQuin);

            double totalMensOQuinc = Double.parseDouble(auxmensOQuin);  //convierte a número el string
            String pagoTotal = "";

            totalMensOQuinc = totalMensOQuinc * 2;

            pagoTotal = String.valueOf(totalMensOQuinc);

            Log.i("decimal", "mensOQuin: " + mensOQuin);
            Log.i("decimal", "pagoTotal: " + pagoTotal);

            String auxDecimal = pagoTotal.substring(pagoTotal.length() - 3, pagoTotal.length());
            String auxEntero = pagoTotal.substring(0, pagoTotal.length() - 3);

            Log.i("decimal", "auxEntero: " + auxEntero);
            Log.i("decimal", "respaldoDecimal: " + auxDecimal);

            valorTotal = GuiTools.getMoneyString(auxEntero);

            Log.i("decimal", "valorTotal: " + valorTotal);

            if (valorTotal.endsWith(".00"))
                valorTotal = valorTotal.replace(".00", auxDecimal);

            Log.i("decimal", "valorTotal formateado: " + valorTotal);
        }

        return valorTotal;
    }

    public void adquiereANOM(){
        delegate.consultaDetalleAlternativa(delegate.getProducto());
    }

    public void simpleUpdateMenu(String cveProd){

        if(((MenuPrincipalActivity)getActivity()) == null)
            Log.i("eliminar", "Menu es nulo");

        Log.i("eliminar", "actualizaMenu AUTO, cveProd: " + cveProd);
        ((MenuPrincipalActivity)getActivity()).updateMenu(cveProd);
        getFragmentManager().beginTransaction().remove(((MenuPrincipalActivity) getActivity()).getActiveFragment()).commit();
        ((MenuPrincipalActivity)getActivity()).showAdelantoNomina();


    }

}

