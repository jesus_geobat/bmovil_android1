package bbvacredit.bancomer.apicredit.gui.delegates;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import org.xml.sax.helpers.ParserAdapter;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.commons.ServerConstants;
import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.ListaDatosResumenController;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.MenuPrincipalActivity;
import bbvacredit.bancomer.apicredit.gui.activities.ResumenActivity;
import bbvacredit.bancomer.apicredit.gui.activities.ResumenEmailViewController;
import bbvacredit.bancomer.apicredit.gui.activities.ResumenViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ConsultaCorreoData;
import bbvacredit.bancomer.apicredit.models.CreditoContratado;
import bbvacredit.bancomer.apicredit.models.EnvioCorreoData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;

public class ResumenDelegate  extends BaseDelegateOperacion{

	private final String CODE_OP_TDC_ILC = "envioCorreoTDCILC";
	private final String CODE_OP_AUTO_HIPO = "envioCorreoHipoAuto";
	private final String CODE_OP_CONSUMO = "envioCorreoConsumo";

	private Session session;
	
	private ObjetoCreditos data;
	
	private Boolean haySimulacion = false;
	
	private Boolean haySimulacionCont = false;
	
	private Boolean sendSecond = true;
	
	private ResumenActivity act = null;

	//Modificacion 50986
	private ListaDatosResumenController actListaResumen = null;

	private Boolean isConsumo;

	private ResumenViewController controller;

	private Producto producto;

	private String correoEnviado;

	
	private static final String EMAIL_PATTERN =
			"^([_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*){1,64}@"
			+ "([_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*(\\.[_A-Za-z0-9-]+)){1,255}$";
	
	public Boolean getHaySimulacionCont() {
		return haySimulacionCont;
	}

	public void setHaySimulacionCont(Boolean haySimulacionCont) {
		this.haySimulacionCont = haySimulacionCont;
	}

	public Boolean getHaySimulacion() {
		return haySimulacion;
	}

	public void setHaySimulacion(Boolean haySimulacion) {
		this.haySimulacion = haySimulacion;
	}
	
	public String getEmail(){
		return session.getEmail();
	}
	
	public ResumenDelegate() {
		session = Tools.getCurrentSession();
		// Datos de sesion
		//data = session.getCreditos();
		// Bloqueamos la pantalla
		//MainController.getInstance().muestraIndicadorActividad("operacion", "conectando");

	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public void setController(ResumenViewController controller) {
		this.controller = controller;
	}

	public ArrayList<Producto> getProductos(){
		return data.getProductos();
	}
	
	public ArrayList<CreditoContratado> getCreditos(){
		return data.getCreditosContratados();
	}
	
	private void checkForSim(){
		
		Iterator<Producto> it = data.getProductos().iterator();
		
		while(it.hasNext()){
			if(it.next().getIndSimBoolean()) haySimulacion = true;
		}
		
		
	}
	
	private void checkForSimContratados(){
		
		Iterator<CreditoContratado> it = data.getCreditosContratados().iterator();
		
		while(it.hasNext()){
			if(it.next().getIndicadorSim()) haySimulacionCont = true;
		}
		
		
	}
	
	public void llenarTablas(ResumenActivity act){
		Integer resumen = 0;
		
		checkForSim();
		checkForSimContratados();
		if(data.getCreditosContratados() != null){
			resumen = act.pintarTablaContratados();
		}
		if(data.getProductos() != null){
			if(resumen > 0 ) act.pintarTablaProductos();
			act.pintarTablaSimulados();
		}
		act.pintarTotalMensual();
		MainController.getInstance().ocultaIndicadorActividad();
	}
	
	/**
	 * Peticiones
	 */
	
	public Boolean validaEmail(String email){
		Pattern pattern;
		Matcher matcher;
		
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(email);
		
		return matcher.matches();
		
	}
	
	private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}else{
			paramTable.put(cnt, "");
		}
	}
	
	private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}
	}
	
	private Boolean containsProductOperation(String operation){
		Iterator<Producto> it = data.getProductos().iterator();
		Boolean ret = false;
		while(it.hasNext()){
			Producto pr = it.next();
			if((pr.getCveProd().equals(operation))&&(pr.getIndSimBoolean())) ret = true;
		}
		
		return ret;
	}
	
	private Producto getProducto(String operation){
		Iterator<Producto> it = data.getProductos().iterator();
		Producto ret = null;
		while(it.hasNext()){
			Producto pr = it.next();
			if(pr.getCveProd().equals(operation)) ret = pr;
		}
		
		return ret;
	}
	
	private Boolean containsProductCreditoContratado(String operation){
		Iterator<CreditoContratado> it = data.getCreditosContratados().iterator();
		Boolean ret = false;
		while(it.hasNext()){
			CreditoContratado pr = it.next();
			if(pr.getCveProd().equals(operation)) ret = true;
		}
		
		return ret;
	}
	
	private CreditoContratado getCreditoContratado(String operation){
		Iterator<CreditoContratado> it = data.getCreditosContratados().iterator();
		CreditoContratado ret = null;
		while(it.hasNext()){
			CreditoContratado pr = it.next();
			if((pr.getCveProd().equals(operation))&&pr.getIndicadorSim()) ret = pr;
		}
		
		return ret;
	}
	
	private void sendEmailContratacion(){		
		//Modified by: WRGC; May 6th,2015.
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		// Mapeamos el codigo de operacion
		addParametro(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
		
		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
		
		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
		
		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
		
		if(containsProductOperation(ConstantsCredit.CREDITO_NOMINA)){
			 
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getIndSim(),ServerConstantsCredit.SCN, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getMontoDeseS(),ServerConstantsCredit.CN_IMPORTE, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getMontoDeseS(),ServerConstantsCredit.CN_IMPORTE, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getTasaS(),ServerConstantsCredit.CN_TASA_ANUAL, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getDesPlazoE(),ServerConstantsCredit.CN_PLAZO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getPagMProdS(),ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCN, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CN_IMPORTE, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CN_TASA_ANUAL, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CN_PLAZO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
			addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getIndSim(),ServerConstantsCredit.SPPI, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getMontoDeseS(),ServerConstantsCredit.PP_IMPORTE, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getTasaS(),ServerConstantsCredit.PP_TASA_ANUAL, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getDesPlazoE(),ServerConstantsCredit.PP_PLAZO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getPagMProdS(),ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SPPI, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.PP_IMPORTE, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.PP_TASA_ANUAL, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.PP_PLAZO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.TARJETA_CREDITO)){
			addParametroObligatorio(getProducto(ConstantsCredit.TARJETA_CREDITO).getIndSim(),ServerConstantsCredit.STDC, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.TARJETA_CREDITO).getMontoMaxS(),ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.TARJETA_CREDITO).getMontoDeseS(),ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.STDC, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getIndSim(),ServerConstantsCredit.SILDC, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getNumTDCS(),ServerConstantsCredit.IL_TARJETA_DE_CREDITO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getMontoMaxS(),ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getMontoDeseS(),ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SILDC, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.IL_TARJETA_DE_CREDITO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.CREDITO_AUTO)){
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getIndSim(),ServerConstantsCredit.SCA, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getMontoDeseS(),ServerConstantsCredit.CA_IMPORTE, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getTasaS(),ServerConstantsCredit.CA_TASA_ANUAL, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getDesPlazoE(),ServerConstantsCredit.CA_PLAZO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getPagMProdS(),ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCA, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CA_IMPORTE, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CA_TASA_ANUAL, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CA_PLAZO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.CREDITO_HIPOTECARIO)){
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getIndSim(),ServerConstantsCredit.SCH, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getMontoDeseS(),ServerConstantsCredit.CH_IMPORTE, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getTasaS(),ServerConstantsCredit.CH_TASA_ANUAL, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getDesPlazoE(),ServerConstantsCredit.CH_PLAZO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getPagMProdS(),ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCH, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CH_IMPORTE, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CH_TASA_ANUAL, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CH_PLAZO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL, paramTable);
		}
		
		if((data.getProductos() != null) && (!data.getProductos().isEmpty())){
			addParametroObligatorio(data.getProductos().get(0).getCATS(),ServerConstantsCredit.CON_CAT, paramTable);
			try {
				addParametroObligatorio(java.net.URLEncoder.encode(data.getProductos().get(0).getFechaCatS(), "UTF-8").replace("+", "%20"),ServerConstantsCredit.CON_FECHA_CALCULO, paramTable);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				addParametroObligatorio(data.getProductos().get(0).getFechaCatS(),ServerConstantsCredit.CON_FECHA_CALCULO, paramTable);
				Log.e("EncodeURL", data.getProductos().get(0).getFechaCatS());
			}
		}else{
			addParametroObligatorio("",ServerConstantsCredit.CON_CAT, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CON_FECHA_CALCULO, paramTable);
		}
		
		addParametroObligatorio("C",ServerConstantsCredit.IND_CORREO, paramTable);
		addParametroObligatorio(session.getEmail(),ServerConstantsCredit.email, paramTable);
		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.envioCorreo(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2, true, new EnvioCorreoData(), MainController.getInstance().getContext());
	}
	
	private void sendEmailLiquidacion(){
		
		//Modified by: OOS,WRGC; May 6th,2015.
		boolean flagSCR=false;
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		// Mapeamos el codigo de operacion
		addParametro(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
		
		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
		
		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
		
		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
		

		if(containsProductCreditoContratado(ConstantsCredit.CREDITO_NOMINA)){
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_NOMINA).getIndicadorSim(),ServerConstantsCredit.SCNL, paramTable);
			//addParametroObligatorio(ServerConstantsCredit.CN_DESC_TITLE,ServerConstantsCredit.CN_DESC, paramTable);
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_NOMINA).getPagoMen(),ServerConstantsCredit.CN_CANT, paramTable);
			
			if(containsProductOperation(ConstantsCredit.CREDITO_NOMINA)){
				flagSCR=true;
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getMontoMaxS(),ServerConstantsCredit.CN, paramTable);
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getIndSim(),ServerConstantsCredit.SAN, paramTable);				
			}else{
				addParametroObligatorio("",ServerConstantsCredit.CN, paramTable);
				addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAN, paramTable);
			}
		}else{
			//addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCNL, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CN_DESC, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CN_CANT, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CN, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAN, paramTable);
		}

		if(containsProductCreditoContratado(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getIndicadorSim(),ServerConstantsCredit.SPPIL, paramTable);
			//addParametroObligatorio(ServerConstantsCredit.PP_DESC_TITLE,ServerConstantsCredit.PP_DESC, paramTable);
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getPagoMen(),ServerConstantsCredit.PP_CANT, paramTable);
			
			if(containsProductOperation(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
				flagSCR=true;
				addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getMontoMaxS(),ServerConstantsCredit.PP, paramTable);
				addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getIndSim(),ServerConstantsCredit.SAP, paramTable);
			}else{
				addParametroObligatorio("",ServerConstantsCredit.PP, paramTable);
				addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAP, paramTable);
			}
		}else{
			//addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SPPIL, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.PP_DESC, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.PP_CANT, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.PP, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAP, paramTable);
		}
		
		if(containsProductCreditoContratado(ConstantsCredit.CREDITO_HIPOTECARIO)){
		//	addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_HIPOTECARIO).getIndicadorSim(),ServerConstantsCredit.SCHL, paramTable);
			//addParametroObligatorio(ServerConstantsCredit.CH_DESC_TITLE,ServerConstantsCredit.CH_DESC, paramTable);
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_HIPOTECARIO).getPagoMen(),ServerConstantsCredit.CH_CANT, paramTable);
			
			if(containsProductOperation(ConstantsCredit.CREDITO_HIPOTECARIO)){
				flagSCR=true;
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getMontoMaxS(),ServerConstantsCredit.CH, paramTable);
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getIndSim(),ServerConstantsCredit.SAH, paramTable);
			}else{
				addParametroObligatorio("",ServerConstantsCredit.CH, paramTable);
				addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAH, paramTable);
			}
		}else{
			//addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCHL, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CH_DESC, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CH_CANT, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CH, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAH, paramTable);
		}

		if(containsProductCreditoContratado(ConstantsCredit.CREDITO_AUTO)){
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_AUTO).getIndicadorSim(),ServerConstantsCredit.SCAL, paramTable);
			//addParametroObligatorio(ServerConstantsCredit.CA_DESC_TITLE,ServerConstantsCredit.CA_DESC, paramTable);
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_AUTO).getPagoMen(),ServerConstantsCredit.CA_CANT, paramTable);
			
			if(containsProductOperation(ConstantsCredit.CREDITO_AUTO)){
				flagSCR=true;
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getMontoMaxS(),ServerConstantsCredit.CA, paramTable);
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getIndSim(),ServerConstantsCredit.SAA, paramTable);
			}else{
				addParametroObligatorio("",ServerConstantsCredit.CA, paramTable);
				addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAA, paramTable);
			}
		}else{
			//addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCAL, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CA_DESC, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CA_CANT, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CA, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAA, paramTable);
		}
		
		
		//New Region || Created by: OOS; May 6th,2015.
		
		
		Iterator<CreditoContratado> it = data.getCreditosContratados().iterator();
		String tituloCredito;
		int counterAux=0;
		
		
		while(it.hasNext()){
			CreditoContratado pr = it.next();

			if(pr.getCveProd().equals(ConstantsCredit.CREDITO_NOMINA))
				tituloCredito=ServerConstantsCredit.CN_DESC_TITLE;
			else if (pr.getCveProd().equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO))
				tituloCredito=ServerConstantsCredit.PP_DESC_TITLE;
			else if (pr.getCveProd().equals(ConstantsCredit.CREDITO_HIPOTECARIO))
				tituloCredito=ServerConstantsCredit.CH_DESC_TITLE;
			else
				tituloCredito=ServerConstantsCredit.CA_DESC_TITLE;

			switch(counterAux)
			{
			case 0:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SCNL, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.CN_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.CN_CANT, paramTable);
				break;
			case 1:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SPPIL, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.PP_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.PP_CANT, paramTable);
				break;
			case 2:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SCHL, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.CH_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.CH_CANT, paramTable);
				break;
			case 3:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SCAL, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.CA_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.CA_CANT, paramTable);
				break;
			case 4:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SP5, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.P5_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.P5_CANT, paramTable);
				break;
			case 5:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SP6, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.P6_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.P6_CANT, paramTable);
				break;
			}	
			counterAux++;
		}
		
		if(counterAux<5)
		{
			do
			{
				switch(counterAux)
				{
				case 0:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCNL, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CN_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CN_CANT, paramTable);
					break;
				case 1:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SPPIL, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.PP_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.PP_CANT, paramTable);
					break;
				case 2:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCHL, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CH_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CH_CANT, paramTable);
					break;
				case 3:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCAL, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CA_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CA_CANT, paramTable);
					break;
				case 4:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SP5, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.P5_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.P5_CANT, paramTable);
					break;
				case 5:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SP6, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.P6_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.P6_CANT, paramTable);
					break;
				}		
				counterAux++;
			}while(counterAux<6);
		}
	   //End Region
		
		
		
		if(containsProductOperation(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
			flagSCR=true;
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getMontoMaxS(),ServerConstantsCredit.IL, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getIndSim(),ServerConstantsCredit.SAI, paramTable);
		}else{
			addParametroObligatorio("",ServerConstantsCredit.IL, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAI, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.TARJETA_CREDITO)){
			flagSCR=true;
			addParametroObligatorio(getProducto(ConstantsCredit.TARJETA_CREDITO).getMontoMaxS(),ServerConstantsCredit.TC, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.TARJETA_CREDITO).getIndSim(),ServerConstantsCredit.SAT, paramTable);
		}else{
			addParametroObligatorio("",ServerConstantsCredit.TC, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAT, paramTable);
		}

		
		//Modified May 6th,2015.
		if(flagSCR)
			addParametroObligatorio(ServerConstantsCredit.IND_S,ServerConstantsCredit.SCR, paramTable);
		else
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCR, paramTable);
		//End Region.
		
		//YA NO SON NECESARIOS
		//addParametroObligatorio("",ServerConstantsCredit.LIQ_CAT, paramTable);
		//addParametroObligatorio("",ServerConstantsCredit.LIQ_FECHA_CALCULO, paramTable);

		addParametroObligatorio("L",ServerConstantsCredit.IND_CORREO, paramTable);
		addParametroObligatorio(session.getEmail(),ServerConstantsCredit.email, paramTable);
		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.envioCorreo(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2, true, new EnvioCorreoData(), MainController.getInstance().getContext());
	}

	public void getCorreo(ResumenActivity act){
		// Establecer activity controladora
		this.act = act;
		// Consulta para obtener de correo
	}
	
	
	public void sendEmail(ResumenActivity act,String email){
		if(validaEmail(email))
		{
			// Bloqueamos la pantalla
			MainController.getInstance().muestraIndicadorActividad("Operación", "Conectando");
			this.act = act;
			sendSecond = true;
			//agm
			session.setEmail(email);
			//
			if(act.isSimulaOfertados())
				sendEmailContratacion();
			else if (act.isSimulaContratados())
				sendEmailLiquidacion();

		}else{
			Log.d("validaEmail", "Email no v�lido");
		}
	}
	
	/****/
	
	public void analyzeResponse(String operationId, ServerResponse response) {
    	
    	if(getCodigoOperacion().equals(operationId)){
    		if((response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)){

				if((act.isSimulaContratados() && !act.isSimulaOfertados()) ||
						(!act.isSimulaContratados() && act.isSimulaOfertados()) ||
						!sendSecond){
        			// Desbloqueamos la pantalla
        			MainController.getInstance().ocultaIndicadorActividad();
    				act.showInformationAlert("Su mensaje ha sido enviado con �xito");
    			}

				if(sendSecond && act.isSimulaContratados()){
    				sendEmailLiquidacion();
    			}
    			
    			sendSecond = false;
    		}
    	}else if(getCodigoOperacionGuardar().equals(operationId)) {
			if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
				if(isConsumo)
				   actListaResumen.consumoOneClick();
				else
					actListaResumen.oneClickILC();
			}
		}else if(Server.ENVIO_CORREO_HIPO_AUTO.equals(operationId)){
			if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
				Log.i("correo","envio exitoso de auto-hipo");
				exito();
			}
		}else if(Server.ENVIO_CORREO_TDC_ILC.equals(operationId)){
			if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
				Log.i("correo","envio exitoso de tdc-ilc");
				exito();
			}
		}else if (Server.ENVIO_CORREO_CONSUMO.equals(operationId)){
			if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
				Log.i("correo","envio exitoso de consumo");
				exito();
			}

		}
	}

	private void exito() {
		muestraAlertExitoEnvioCorreo();
		controller.muestraCorreoSession();
	}

	private void muestraAlertExitoEnvioCorreo(){
		controller.showInformationAlert(controller.getResources().getString(R.string.resumen_exito_envio_correo) + " " + getCorreoEnviado(), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				controller.edtCorreoNuevo.setEnabled(true);
				controller.edtCorreoNuevo.requestFocus();
				controller.edtCorreoNuevo.setText("");
				InputMethodManager imm = (InputMethodManager) controller.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(controller.edtCorreoNuevo, InputMethodManager.SHOW_IMPLICIT);
			}
		});
	}

	public <T> void redirectToView(Class<T> c){
		MainController.getInstance().showScreen(c);
	}

	@Override
	protected String getCodigoOperacion() {
		// TODO Auto-generated method stub
		return Server.ENVIO_CORREO_OPERACION;
	}

	//Modificacion 50986
	public void saveSimulationRequest(ListaDatosResumenController actListaResumen, boolean isConsumo )
	{   // Mapeamos el activity
		this.actListaResumen = actListaResumen;
		this.isConsumo = isConsumo;

		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();

		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacionGuardar(),ServerConstantsCredit.OPERACION_PARAM, paramTable);

		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);

		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);

		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

		// Mapeamos el tipo de operacion -> 3
		addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
		this.doNetworkOperation(getCodigoOperacionGuardar(), paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
	}

	public void enviarCorreo(boolean isCorreoSession){

		Hashtable<String, String> paramTable = new Hashtable<String, String>();

		Session oSession = Tools.getCurrentSession();
		String correo ="";
		String operacion ="";
		String ium = oSession.getIum();
		String celular = oSession.getNumCelular();
		String cveProd = producto.getCveProd();
		String tasa = producto.getTasaS();
		String cat = producto.getCATS();

		String catFecha = producto.getFechaCatS();

		/**
		 "numeroCelular":"5530724692","operacion":"calculoAlternativas","IUM":"3ABBBAA6F7031F0B913CC9D2F8D5CF96","cliente":"D0061628","tipoOp":"3"}
		 */


//		Log.e("correo", "nombre cliente MP: " + MenuPrincipalActivity.getInstance().getNombreCliente());
		String auxNombre = MenuPrincipalActivity.getInstance().getNombreCliente();
		String  nombre ="";
		if(auxNombre.length()>20)
			nombre = auxNombre.substring(0,20);
		else
			nombre = MenuPrincipalActivity.getInstance().getNombreCliente();



		Log.i("correo","nombre recortado: "+nombre);
		Log.i("correo","nombre MP: "+auxNombre);
		Log.i("correo","nombre MP2: "+MenuPrincipalActivity.getInstance().getNombreCliente());

		String lNuevo    = "";
		String lActual   = "";
		String lSimulado = ""; //maximo ofertado

		String montoSimulado = "";
		String plazo = "";
		String pagoM = "";
		String descSub = "";

		String numTDC = "";

		String importe ="";
		String tipoSeguro = "";
		String pagoSeguro = "";
		String tasaMensual = "";
		String indRevire = "";
		String tasaRevire = "";
		String comision = "";

		if(isCorreoSession)
			correo = Tools.getCurrentSession().getEmail();
		else
			correo = controller.getEdtCorreNuevo();

		setCorreoEnviado(correo);

		Log.w("coreo", "_______");
		if(producto.getCveProd().equals(ConstantsCredit.CREDITO_AUTO) ||
				producto.getCveProd().equals(ConstantsCredit.CREDITO_HIPOTECARIO)){

			operacion = CODE_OP_AUTO_HIPO;
			montoSimulado = Tools.formatUserAmount(producto.getMontoDeseS());
			pagoM = Tools.formatUserAmount(producto.getPagMProdS());

			plazo = producto.getDesPlazoE();
			descSub = producto.getDessubpE();

			if(producto.getCveProd().equals(ConstantsCredit.CREDITO_HIPOTECARIO)){
				cveProd = "HIPO";
			}

			addParametroObligatorio(ium, ServerConstantsCredit.IUM2_PARAM, paramTable);
			addParametroObligatorio(catFecha, ServerConstantsCredit.FECHA_CAT_PARAM, paramTable);
			addParametroObligatorio(correo, ServerConstantsCredit.CORREO_PARAM, paramTable);
			addParametroObligatorio(descSub, ServerConstantsCredit.DESC_SUBP_PARAM, paramTable);

		}else if(producto.getCveProd().equals(ConstantsCredit.TARJETA_CREDITO)) {
			cveProd = "TDC";
			operacion = CODE_OP_TDC_ILC;
			numTDC = producto.getNumTDCS();
			lNuevo = Tools.formatUserAmount(producto.getMontoDeseS());
			descSub = producto.getDessubpE();

			Log.i("correo","tasaS: "+producto.getTasaS());
			Log.i("correo","tasa: "+tasa);
			Log.i("correo","tasa: "+producto.getTasa());
			Log.i("correo","tasa: "+producto.getTasaOri());
			Log.i("correo","numTDC: "+producto.getNumTDCS());
			Log.i("correo","descSub: "+producto.getDessubpE());

			addParametroObligatorio(descSub, ServerConstantsCredit.DES_SUBP_PARAM, paramTable);
			addParametroObligatorio(ium, ServerConstantsCredit.IUM_PARAM, paramTable);
			addParametroObligatorio(correo, ServerConstantsCredit.EMAIL_PARAM, paramTable);
			addParametroObligatorio(catFecha, ServerConstantsCredit.CAT_FECHA_PARAM, paramTable);

		}else if (producto.getCveProd().equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){

			operacion = CODE_OP_TDC_ILC;
			lNuevo = Tools.formatUserAmount(oSession.getOfertaILC().getLineaFinal());
			lActual = Tools.formatUserAmount(oSession.getOfertaILC().getLineaActual());
			lSimulado = Tools.formatUserAmount(producto.getMontoMaxS());

			numTDC = producto.getNumTDCS();
			numTDC = numTDC.substring(numTDC.length()-5, numTDC.length());

			addParametroObligatorio(ium, ServerConstantsCredit.IUM_PARAM, paramTable);
			addParametroObligatorio(correo, ServerConstantsCredit.EMAIL_PARAM, paramTable);
			addParametroObligatorio(catFecha, ServerConstantsCredit.CAT_FECHA_PARAM, paramTable);
			addParametroObligatorio(descSub		, ServerConstantsCredit.DES_SUBP_PARAM, paramTable);

		}else if (producto.getCveProd().equals(ConstantsCredit.ADELANTO_NOMINA) ||
				producto.getCveProd().equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO) ||
				producto.getCveProd().equals(ConstantsCredit.CREDITO_NOMINA)){

			String mesOQuincena = producto.getSubproducto().get(0).getCveSubp();
			tipoSeguro = "";
			pagoSeguro = "";
			operacion = CODE_OP_CONSUMO;
			importe = Tools.formatUserAmount(producto.getMontoDeseS());
			pagoM = Tools.formatUserAmount(producto.getPagMProdS());
			tasaMensual = producto.getTasaS();
			indRevire = producto.getIndRev();

			if(cveProd.equals(ConstantsCredit.ADELANTO_NOMINA)) {
				plazo = mesOQuincena.equals(ConstantsCredit.CODIGO_MENSUAL) ?
						controller.getResources().getString(R.string.detalle_adelanto_nomina_plazo_mensual) :
						controller.getResources().getString(R.string.detalle_adelanto_nomina_plazo_quincenal);
			}else{
				plazo = producto.getDesPlazoE();
			}

			Log.i("resumendelegate","plazo: "+plazo);

			if(producto.getIndRev().equalsIgnoreCase("S")) {
				tasaRevire = producto.getTasaS();
				tasaMensual = producto.getTasaOri();
				addParametroObligatorio(tasaRevire, ServerConstantsCredit.TASA_REVIRE_PARAM, paramTable);
			}

			if(cveProd.equals(ConstantsCredit.ADELANTO_NOMINA))
				addParametroObligatorio(tasaMensual	, ServerConstantsCredit.TASA_ANUAL_PARAM, paramTable);
			else
				addParametroObligatorio(tasaMensual	, ServerConstantsCredit.TASA_MENSUAL_PARAM,paramTable);

			comision = Integer.toString(producto.getSubproducto().get(0).getTasa());

			addParametroObligatorio(ium, ServerConstantsCredit.IUM2_PARAM, paramTable);
			addParametroObligatorio(catFecha, ServerConstantsCredit.FECHA_CAT_PARAM, paramTable);
			addParametroObligatorio(correo, ServerConstantsCredit.CORREO_PARAM, paramTable);
		}

		addParametroObligatorio(celular		, ServerConstantsCredit.CEL_PARAM, paramTable);
		addParametroObligatorio(cveProd, ServerConstantsCredit.CVE_PROD, paramTable);
		addParametroObligatorio(tasa		, ServerConstantsCredit.TASA_PARAM,paramTable);
		addParametroObligatorio(cat, ServerConstantsCredit.CAT_PARAM, paramTable);

		addParametroObligatorio(nombre		, ServerConstantsCredit.NOMBRE_PARAM, paramTable);
		addParametroObligatorio(montoSimulado, ServerConstantsCredit.MONTO_CALCULADO_PARAM, paramTable);
		addParametroObligatorio(plazo		, ServerConstantsCredit.PLAZO_PARAM, paramTable);
		addParametroObligatorio(pagoM		, ServerConstantsCredit.PAGO_MENSUAL2_PARAM, paramTable);

		addParametroObligatorio(lNuevo		, ServerConstantsCredit.LIMITE_NUEVO_PARAM, paramTable);
		addParametroObligatorio(lActual		, ServerConstantsCredit.LIMITE_ACTUAL_PARAM, paramTable);
		addParametroObligatorio(lSimulado	, ServerConstantsCredit.LIMITE_SIMULADO_PARAM, paramTable);
		addParametroObligatorio(operacion	, ServerConstantsCredit.OPERACION_PARAM, paramTable);

		addParametroObligatorio(numTDC		, ServerConstantsCredit.TDC_PARAM, paramTable);
		addParametroObligatorio(importe		, ServerConstantsCredit.IMPORTE_PARAM, paramTable);
		addParametroObligatorio(tipoSeguro	, ServerConstantsCredit.TIPO_SEGURO_PARAM, paramTable);
		addParametroObligatorio(pagoSeguro	, ServerConstantsCredit.PAGO_SEGURO_PARAM, paramTable);

		addParametroObligatorio(indRevire	, ServerConstantsCredit.IND_REVIRE_PARAM, paramTable);
		addParametroObligatorio(comision, ServerConstantsCredit.COMISION_PARAM, paramTable);


		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.envioCorreoNuevaImagen(paramTable, cveProd);
		this.doNetworkOperation(operacion,paramTable2,true,new EnvioCorreoData(),MainController.getInstance().getContext());
	}

	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	//Modificacion 50986
	protected String getCodigoOperacionGuardar()
	{
		return Server.CALCULO_ALTERNATIVAS_OPERACION;
	}

	public String getCorreoEnviado() {
		return correoEnviado;
	}

	public void setCorreoEnviado(String correoEnviado) {
		this.correoEnviado = correoEnviado;
	}
}
