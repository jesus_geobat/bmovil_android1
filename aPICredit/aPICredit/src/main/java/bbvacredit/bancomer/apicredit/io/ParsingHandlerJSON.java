package bbvacredit.bancomer.apicredit.io;

import org.json.JSONException;

import java.io.IOException;

public interface ParsingHandlerJSON {
	/**
     * Process a server response
     * @param parser
     * @throws IOException
     * @throws ParsingException
     */
//	public void process(ParserJSON parser) throws IOException, ParsingException;
	public void processJSON(String jsonString) throws JSONException;
}
