package bbvacredit.bancomer.apicredit.gui.activities;

/**
 * Created by OOROZCO on 02/12/15.
 */

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.LabelMontoTotalCredit;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.SeekArc.OnSeekArcChangeListener;
import bbvacredit.bancomer.apicredit.gui.delegates.DetalleDeAlternativaDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.ILCDelegate;
import bbvacredit.bancomer.apicredit.models.Producto;
import tracking.TrackingHelperCredit;

public class ILCViewController extends Fragment {

    // ------ SeekArc Attributes ---- //
    private SeekArc mSeekArc;
    private LabelMontoTotalCredit lblMontoTotalCredito;
    private String monto;

    // ------ User Information Attributes ---- //
    private TextView txtTarjeta;
    private ImageView imgCredito;

    // ------ Low Part Attributes ---- //
    private ImageButton btnSimular;
    private LinearLayout lytOpcion;
    private LinearLayout lytPlazo;

    private DetalleILCViewController detalleILCFragment;
    private ILCDelegate delegate;

    private boolean editable;
    private DecimalFormat df;
    private DecimalFormat dfnd;

    private String amountString=null;
    private StringBuffer typedString= new StringBuffer();
    private boolean isSettingText = false;
    private boolean isResetting = false;
    private boolean mAcceptCents=false;
    private boolean flag=true;

    private Producto producto;
    private String montMax;
    private String montMin;

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getMonto() {
        return monto;
    }

    public ILCDelegate getDelegate() {
        return delegate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realizaAccionInicio();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        delegate= new ILCDelegate();
        delegate.setController(this);

        montMax = getProducto().getMontoMaxS();
        montMin = getProducto().getMontoMinS();

        final View rootView = inflater.inflate(R.layout.circule_progress_bar_layout, container, false);

        if (rootView != null) {
            findViews(rootView);

            lblMontoTotalCredito.setMontoMin(montMin);
            lblMontoTotalCredito.setMontoMax(montMax);
            lblMontoTotalCredito.setLongClickable(false);

            lblMontoTotalCredito.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    reset();
                    mAcceptCents = true;
                    isSettingText = true;
                    lblMontoTotalCredito.setText("");
                    return false;
                }
            });

            lblMontoTotalCredito.addTextChangedListener(new TextWatcher() {
                /**
                 * Save the value of string before changing the text
                 */
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    String amountField = lblMontoTotalCredito.getText().toString();
                    if (!isSettingText) {
                        amountString = amountField;
                    }
                }

                /**
                 * When the value string has changed
                 */
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String amountField = lblMontoTotalCredito.getText().toString();

                    if(s.toString().equals(".")) {
                        Log.w("eliminar", "se ingresó un un punto, eliminalo, toS");
                        lblMontoTotalCredito.setText("");
                    }else {
                        Log.w("eliminar", "Se ingresó bien el número");

                        if (!isSettingText && !isResetting) {
                            if (!flag) {
                                String aux;
                                try {
                                    aux = lblMontoTotalCredito.getText().toString().substring(0, lblMontoTotalCredito.length() - 3);
                                    typedString = new StringBuffer();

                                    if (aux.charAt(0) == '0')
                                        aux = aux.substring(1, aux.length());
                                } catch (Exception ex) {
                                    aux = "";
                                }
                                for (int i = 0; i < aux.length(); i++) {
                                    if (aux.charAt(i) != ',')
                                        typedString.append(aux.charAt(i));
                                }
                                flag = true;

                                setFormattedText();
                                amountField = lblMontoTotalCredito.getText().toString();
                                amountString = amountField;
                            } else {
                                try {
                                    if (lblMontoTotalCredito.length() < amountString.length()) {
                                        reset();
                                    } else if (lblMontoTotalCredito.length() > amountString.length()) {
                                        int newCharIndex = lblMontoTotalCredito.getSelectionEnd() - 1;
                                        //there was no selection in the field
                                        if (newCharIndex == -2) {
                                            newCharIndex = lblMontoTotalCredito.length() - 1;
                                        }

                                        char num = amountField.charAt(newCharIndex);
                                        if (!(num == '0' && typedString.length() == 0)) {
                                            typedString.append(num);
                                        }
                                        setFormattedText();
                                    }
                                } catch (StringIndexOutOfBoundsException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    }
                }

                public void afterTextChanged(Editable s) {
                    isSettingText = false;
                    lblMontoTotalCredito.setSelection(lblMontoTotalCredito.length());
                }
            });

            lblMontoTotalCredito.setOnKeyListener(new View.OnKeyListener() {
                      public boolean onKey(View view, int keyCode, KeyEvent event) {
                          if (keyCode == KeyEvent.KEYCODE_ENTER) {
                              InputMethodManager manager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                              if (manager != null) {
                                  manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                                  String auxMon = lblMontoTotalCredito.getText().toString();
                                  String respuesta = "";

                                  if (lblMontoTotalCredito.getText().toString().equals("")) {
                                      lblMontoTotalCredito.setText(Tools.formatAmount(Tools.validateValueMonto(montMin), false));
                                      actualizaGrafica(montMin);
                                  } else {
                                      respuesta = Tools.validateMont(lblMontoTotalCredito.getMontoMax(), lblMontoTotalCredito.getMontoMin(), auxMon, null);
                                      try {
                                          if (respuesta.equals(ConstantsCredit.MONTO_MAYOR)) {
                                              lblMontoTotalCredito.setText(Tools.formatAmount(Tools.validateValueMonto(montMax), false));
                                              actualizaGrafica(montMax);
                                          } else if (respuesta.equals(ConstantsCredit.MONTO_MENOR)) {
                                              lblMontoTotalCredito.setText(Tools.formatAmount(Tools.validateValueMonto(montMin), false));
                                              actualizaGrafica(montMin);
                                          } else if (respuesta.equals(ConstantsCredit.MONTO_EN_RANGO)) {
                                              String auxLbl = lblMontoTotalCredito.getText().toString();
                                              if(!auxLbl.startsWith("$"))
                                                  lblMontoTotalCredito.setText("$"+auxLbl);

                                              actualizaGrafica(auxLbl);
                                          }
                                      } catch (Exception ex) {
                                          ex.printStackTrace();
                                      }
                                  }
                              }
                          }
                          if (keyCode == KeyEvent.KEYCODE_DEL) {
                              if (lblMontoTotalCredito.getText().length() > 3 && (isFlag())) {
                                  lblMontoTotalCredito.setSelection(lblMontoTotalCredito.length() - 3);
                                  setFlag(false);
                              }
                          }
                          return false;
                      }
                  }
            );

                mSeekArc.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
                        @Override
                        public void onStopTrackingTouch(SeekArc seekArc) {
                        }

                        @Override
                        public void onStartTrackingTouch(SeekArc seekArc) {
                        }

                        @Override
                        public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
                            monto = String.valueOf(progress);
                            lblMontoTotalCredito.setText(Tools.formatAmount(monto, false));
                            lblMontoTotalCredito.setSelection(monto.length());
                            getActivity().findViewById(R.id.mainContainer).setVisibility(View.VISIBLE);
                            editable = false;

                            InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            if (manager != null)
                                manager.hideSoftInputFromWindow(lblMontoTotalCredito.getWindowToken(), 0);

                            lblMontoTotalCredito.setCursorVisible(false);
                        }
                    }
                );

                btnSimular.setOnClickListener(new OnClickListener() {
                          public void onClick(View v) {

                              if (lblMontoTotalCredito.getText().toString().equals("")) {
                                  Tools.showAlert("Favor de ingresar un monto", getActivity());
                              } else {
                                  delegate.doRecalculoContract();
                              }
                          }
                      }

                );
            }

            return  rootView;
    }


    private void findViews(View rootView){

        mSeekArc = (SeekArc) rootView.findViewById(R.id.seekArc);
        lblMontoTotalCredito = (LabelMontoTotalCredit) rootView.findViewById(R.id.lblMontoTotalCredito);
        txtTarjeta = (TextView) rootView.findViewById(R.id.txtTarjeta);
        btnSimular = (ImageButton) rootView.findViewById(R.id.btn_simular);
        imgCredito = (ImageView) rootView.findViewById(R.id.imgCredito);
        lytOpcion = (LinearLayout) rootView.findViewById(R.id.lytOpcion);
        lytPlazo = (LinearLayout) rootView.findViewById(R.id.lytPlazo);


        // This module does not need the dialog pickers, thus they are going to be hidden.
            lytOpcion.setVisibility(View.GONE);
            lytPlazo.setVisibility(View.GONE);

        imgCredito.setImageResource(R.drawable.ic_agregartari);

        txtTarjeta.setText(Tools.hideAccountNumber(delegate.getProducto().getNumTDCS()));
        Log.i("ILC","numTarj: "+delegate.getProducto().getNumTDCS());
        seekArcSettlement();

    }
    public void showIncrementarILC(DetalleDeAlternativaDelegate delegate){

        ((MenuPrincipalActivity)getActivity()).updateMenu(this.delegate.getProducto().getCveProd());

        try {
            if (((MenuPrincipalActivity) getActivity()).getActiveFragment() != null) {
                getFragmentManager().beginTransaction().remove(((MenuPrincipalActivity) getActivity()).getActiveFragment()).commit();
            }
            ((MenuPrincipalActivity) getActivity()).setActiveFragment(null);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        detalleILCFragment = new DetalleILCViewController();
        detalleILCFragment.setDelegate(delegate);
        detalleILCFragment.setDelegateOp(this.delegate);
        ((MenuPrincipalActivity) getActivity()).setActiveFragment(detalleILCFragment);


        getFragmentManager().beginTransaction().add(R.id.fragmentContainer, ((MenuPrincipalActivity) getActivity()).getActiveFragment()).commit();

    }


    private void seekArcSettlement(){
        mSeekArc.setArcRotation(345);
        mSeekArc.setSweepAngle(330);
        mSeekArc.setArcWidth(40);
        mSeekArc.setProgressWidth(20);

        monto = String.valueOf(mSeekArc.mMax);
        lblMontoTotalCredito.setText(Tools.formatAmount(monto, false));
        mSeekArc.updateProgressText(Integer.parseInt(monto));
        lblMontoTotalCredito.setSelection(String.valueOf(String.valueOf(monto)).length());

    }

    private void setFormattedText(){
        isSettingText = true;
        String text = typedString.toString();

        if(mAcceptCents){
            text += "00";
        }

        lblMontoTotalCredito.setText(bancomer.api.common.commons.Tools.formatAmountFromServer(text));
    }

    public void reset() {
        this.isResetting = true;
        this.typedString=new StringBuffer();
        this.isResetting = false;
    }

    public void actualizaGrafica(String monto){
        monto = monto.replace("$","");
        mSeekArc.setArcRotation(345);
        mSeekArc.setSweepAngle(330);
        mSeekArc.setArcWidth(40);
        mSeekArc.setProgressWidth(20);


        mSeekArc.updateProgressText(Integer.parseInt(Tools.validateValueMonto(monto)));

    }

    private void realizaAccionInicio(){
        Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
        click_paso2_operacion.put("evento_paso1", "event45");
//        click_paso2_operacion.put("&&products", "simulador;simulador:simulador incremento linea credito;;;;eVar12=inicio");
        click_paso2_operacion.put("&&products", "simulador;simulador:simulador incremento linea credito;;;;");
        click_paso2_operacion.put("eVar12", "inicio");  //pendiente por definir
        TrackingHelperCredit.trackSimulacionRealizada(click_paso2_operacion);
    }

    private void realizaAccionSimular(){
        Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
        click_paso2_operacion.put("evento_paso1", "event46");
//        click_paso2_operacion.put("&&products", "simulador;simulador:simulador incremento linea;;;;eVar12=simulacion realizada");
        click_paso2_operacion.put("&&products", "simulador;simulador:simulador incremento linea;;;;");
        click_paso2_operacion.put("eVar12", "simulacion realizada");  //pendiente por definir
        TrackingHelperCredit.trackSimulacionRealizada(click_paso2_operacion);
    }


    public void setSimulacionGrafica(String monto){

        lblMontoTotalCredito.setText(monto);
        actualizaGrafica(monto);
    }
}
