package bbvacredit.bancomer.apicredit.gui.activities;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;

import bbvacredit.bancomer.apicredit.R;
import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.GuiTools;
import bbvacredit.bancomer.apicredit.common.LabelMontoTotalCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.delegates.AdelantoNominaSeekBarDelegate;
import bbvacredit.bancomer.apicredit.gui.delegates.PrestamoPersonalSeekBarDelegate;
import bbvacredit.bancomer.apicredit.models.OfertaConsumo;
import bbvacredit.bancomer.apicredit.models.Plazo;
import bbvacredit.bancomer.apicredit.models.Producto;
import bbvacredit.bancomer.apicredit.models.Subproducto;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by FHERNANDEZ on 30/12/16.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PrestamoPersonalSeekBarViewController extends Fragment implements View.OnClickListener{

    private Producto producto;
    private Producto productoAux;

    private ImageView imgvProd;
    private ImageView imgvShowDetalle;
    private ImageView imgvAyuda;

    private ImageButton imgvContratarCredito;

    public LabelMontoTotalCredit edtMontoVisible;

    private TextView txtvDescProd;
    private TextView txtvMontoMinimo;
    private TextView txtvMontoMaximo;
    private TextView txtvCat;
    private TextView txtvTasa;
    private TextView txtvTextSaldoTotal;
    private TextView txtvSaldoTotalPagar;
    private TextView txtvPlazo;
    private TextView txtvTipo;
    private TextView txtvMasInfo;
    private TextView txtvPagoMMax;
    private TextView txtvMontoMMax;

    private LinearLayout lnlDetalleProd;
    private LinearLayout lnlTipo;
    private LinearLayout lnlPlazo;
    private LinearLayout lnlPlazoTipo;
    private LinearLayout lnlPagoMMaximo;
    private LinearLayout lnlPagoMMaximo2;

    private SeekBar seekBarSimulador;

    private String montoMax = "";
    private String montMin = "";
    private StringBuffer typedString= new StringBuffer();
    private String amountString=null;
    private boolean indSimProd;
    private String auxFinalMax;

    private String plazo;
    private String termCreditList[];
    public String cvePlazo;

    private boolean isSettingText = false;
    private boolean isResetting = false;
    private boolean mAcceptCents=false;
    private boolean flag=true;
    public boolean isShowDetalle;
    public boolean isSimulated = false;
    public boolean isFromBar;
    public boolean isConsumoCamp;
    public boolean isppi = false;
    public boolean buscarPlazo = true;
    public boolean isBackProd = true;
    public boolean auxisCamp = false;
    public boolean initalCampain = false;
    public DialogInterface oDialog;

    public OfertaConsumo ofertaConsumo;

    private Promociones promocionConsumo;

    private PrestamoPersonalSeekBarDelegate delegate;
    private PrestamoPersonalSeekBarViewController controller;

    private final int INCREMENTAL = 1000;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_credit_item_listview_creditos,container,false);

        if(rootView != null){
            delegate = new PrestamoPersonalSeekBarDelegate(this);
            controller = this;
            initView(rootView);
            setIsShowDetalle(false);
        }

        return rootView;
    }

    public void initView(View rootView){
        Log.i("update","init view");
        isConsumoCamp = ((MenuPrincipalActivitySeekBar)getActivity()).isConsumoCamp;
        if(isConsumoCamp) {
            delegate.setPromociones(getPromocionConsumo());
            Log.i("consumo","getCveCamp: "+getPromocionConsumo().getCveCamp());
            Log.i("consumo","getMonto: "+getPromocionConsumo().getMonto());
        }
        findViews(rootView);
        validateSO();
        setValuesProduct();
        setValuesSeekBar();
        setVisibleMont(rootView);
        if(getProducto()!=null)
            loadList();
        validateCampainOfert();
    }

    private void validateCampainOfert(){
        if (isConsumoCamp && getProducto() == null){
            edtMontoVisible.setEnabled(false);
            seekBarSimulador.setEnabled(false);
            txtvMontoMinimo.setVisibility(View.GONE);
        }
    }

    public void dissableProd(){
        edtMontoVisible.setEnabled(false);
        seekBarSimulador.setEnabled(false);
        imgvShowDetalle.setEnabled(false);
        edtMontoVisible.setText("$0");
        txtvMontoMinimo.setVisibility(View.GONE);
        txtvMontoMaximo.setText("$0");
    }

    public void enableProd(){
        edtMontoVisible.setEnabled(true);
        seekBarSimulador.setEnabled(true);
        imgvShowDetalle.setEnabled(true);
        txtvMontoMinimo.setVisibility(View.VISIBLE);
    }

    private void loadList(){
        ArrayList<Plazo> auxiliar = producto.getSubproducto().get(0).getPlazo();
        termCreditList = new String[auxiliar.size()];
        this.productoAux = producto;

        //JQH Oculta botón listar plazos
        if (auxiliar.size()<=1) {
            lnlPlazo.setVisibility(View.GONE);
        }else{
            lnlPlazo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pickerPlazo();
                }
            });
        }

        int counter=0;

        for(Plazo plazo: auxiliar) {
            termCreditList[counter] = plazo.getDesPlazo();
            counter++;
        }
        plazo = termCreditList.length>0?termCreditList[0]:"";

        txtvPlazo.setText(getProducto().getDesPlazoE());
    }

    private void setVisibleMont(final View view){

        edtMontoVisible.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.i("edt", "limpia monto, " + event);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    Log.i("edt", "action up, limpia texto");

                    ((MenuPrincipalActivitySeekBar)getActivity()).validateEdtMontoVisible();
                    ((MenuPrincipalActivitySeekBar)getActivity()).isPPIEditable = true;
                    ((MenuPrincipalActivitySeekBar)getActivity()).isPPIEditableS = edtMontoVisible.getText().toString();

                    reset();
                    mAcceptCents = true;
                    isSettingText = true;
                    edtMontoVisible.setText("");
                    return false;
                } else {
                    Log.w("edt", "otro evento, no limpia");
                }

                return false;
            }
        });

        edtMontoVisible.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                String amountField = edtMontoVisible.getText().toString();
                if (!isSettingText) {
                    amountString = amountField;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String amountField = edtMontoVisible.getText().toString();
                isBackProd = false;

                if (s.toString().equals(".") || s.toString().equals("0")) {
                    edtMontoVisible.setText("");
                } else {

                    if (!isSettingText && !isResetting) {
                        if (!flag) {
                            String aux;
                            try {
                                aux = edtMontoVisible.getText().toString();//.substring(0, edtMontoVisible.length() - 3);
                                typedString = new StringBuffer();

                                if (aux.charAt(0) == '0')
                                    aux = aux.substring(1, aux.length());
                            } catch (Exception ex) {
                                aux = "";

                            }
                            for (int i = 0; i < aux.length(); i++) {
                                if (aux.charAt(i) != ',')
                                    typedString.append(aux.charAt(i));
                            }
                            flag = true;

                            setFormattedText();
                            amountField = edtMontoVisible.getText().toString();
                            amountString = amountField;
                        } else {
                            try {
                                if (edtMontoVisible.length() < amountString.length()) {
                                    reset();
                                } else if (edtMontoVisible.length() > amountString.length()) {

                                    int newCharIndex = edtMontoVisible.getSelectionEnd() - 1;
                                    //there was no selection in the field
                                    if (newCharIndex == -2) {
                                        newCharIndex = edtMontoVisible.length() - 1;
                                    }

                                    char num = amountField.charAt(newCharIndex);
                                    if (!(num == '0' && typedString.length() == 0)) {
                                        typedString.append(num);
                                    }
                                    setFormattedText();
                                }
                            } catch (StringIndexOutOfBoundsException ex) {
                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                isSettingText = false;
            }
        });

        edtMontoVisible.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager manager = (InputMethodManager) ((MenuPrincipalActivitySeekBar) getActivity()).getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (manager != null) {
                        manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                        String auxMon = edtMontoVisible.getText().toString();
                        String respuesta = "";

                        if (edtMontoVisible.getText().toString().equals("")) {
                            edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                            updateGraph(montMin);
                        } else {
                            respuesta = Tools.validateMont(edtMontoVisible.getMontoMax(),
                                    edtMontoVisible.getMontoMin(), auxMon, null);
                            Log.i("compare","aceptar, respuesta: "+respuesta);

                            try {
                                if (respuesta.equals(ConstantsCredit.MONTO_MAYOR)) {
                                    edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montoMax)));
                                    updateGraph(montoMax);
                                } else if (respuesta.equals(ConstantsCredit.MONTO_MENOR)) {
                                    edtMontoVisible.setText(Tools.formatUserAmount(Tools.validateValueMonto(montMin)));
                                    updateGraph(montMin);
                                } else if (respuesta.equals(ConstantsCredit.MONTO_EN_RANGO)) {
//                                    String auxLbl = edtMontoVisible.getText().toString();
                                    Log.i("compare","en rango: "+auxMon);
                                    String auxLbl = auxMon;
                                    updateGraph(auxLbl);

                                    if (!auxLbl.startsWith("$"))
                                        edtMontoVisible.setText("$" + auxLbl);
                                    else
                                        edtMontoVisible.setText(auxLbl);


                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                    edtMontoVisible.clearFocus();
                }


                return false;
            }
        });

        edtMontoVisible.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    String amount = edtMontoVisible.getText().toString();

                    if (!amount.equals("") || amount != null) {
                        if (amount.contains(","))
                            amount = amount.replace(",", "");

                        if (amount.contains("$"))
                            amount = amount.replace("$", "");

                        amount = amount.substring(0, amount.length() - 1);

                        edtMontoVisible.setText(Tools.formatUserAmount(amount));

                    }
                }

                return false;
            }
        });
    }

    public void updateGraph(String amount){

        isFromBar = true;

        //Indica que fue modificado y que si se contrata, se lanzará por simulador.
        //si se encuentra en false, indicará que no fue modificada, por lo que se l
        if (isConsumoCamp){
            if (!auxisCamp){
                auxisCamp = true;
            }

        }

        double newAmount = 0.0, finalmmax = 0.0;
        int finalP = 0, auxMonMin = 0, auxAmount2 = 0;
        String auxAmount = Tools.clearAmounr(amount);

        if(auxAmount.equals(getMontMin())){
            seekBarSimulador.setProgress(0);

        }else if(auxAmount.equals(getMontoMax())){
            finalmmax = Double.parseDouble(getAuxFinalMax());
            seekBarSimulador.setProgress((int) finalmmax);

        }else {

            auxMonMin = Integer.parseInt(getMontMin());
            auxAmount2 = Integer.parseInt(auxAmount);

            finalP = auxAmount2-auxMonMin;
            newAmount = finalP / INCREMENTAL;

            seekBarSimulador.setProgress((int)newAmount);
        }

        validateProducSimulated();
    }

    private void updateGraph2(String amount){

        double newAmount = 0.0, finalmmax = 0.0, auxAmount2 = 0.0,  auxMonMin = 0;
        double finalP = 0;

        String auxAmount = Tools.clearAmounr(amount);

        if(getProducto()!=null) {
            auxMonMin = Double.parseDouble(getMontMin());
            auxAmount2 = Double.parseDouble(auxAmount);

            finalP = auxAmount2 - auxMonMin;
            newAmount = finalP / INCREMENTAL;
        }else{
            auxAmount2 = Double.parseDouble(auxAmount);
            newAmount = auxAmount2 /INCREMENTAL;
        }

        seekBarSimulador.setProgress((int) newAmount);

    }

    private void setValuesSeekBar(){

        if(getProducto() != null) {
            final double mmax = Double.parseDouble(Tools.validateMont(getMontoMax(), false));
            final double mmin = Double.parseDouble(Tools.validateMont(getMontMin(), false));
            double auxMmax = mmax - mmin;

            auxMmax = auxMmax / INCREMENTAL;

            final double finalAuxMmax = auxMmax;

            setAuxFinalMax(String.valueOf(finalAuxMmax));

            seekBarSimulador.setMax((int) finalAuxMmax);
            seekBarSimulador.setProgress((int) finalAuxMmax);

            seekBarSimulador.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    String visibleMont;

                    final int progressChanged = progress;
                    final int progresFinal = calculatePogress(progressChanged, (int) mmin, (int) finalAuxMmax, (int) mmax);

                    visibleMont = Tools.formatUserAmount(Integer.toString(progresFinal));

                    if (isConsumoCamp) {
                        if (!initalCampain) {
                            initalCampain = true;
                            String gtMonto = getPromocionConsumo().getMonto();

                            gtMonto = gtMonto.substring(0, gtMonto.length() - 2);
                            gtMonto = Tools.formatUserAmount(gtMonto);

                            edtMontoVisible.setText("");
                            edtMontoVisible.setText(gtMonto);
                        } else
                            edtMontoVisible.setText(visibleMont);
                    } else
                        edtMontoVisible.setText(visibleMont);

                    edtMontoVisible.setCursorVisible(false);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    isFromBar = true;

                    if (isConsumoCamp) {
                        if (!auxisCamp)
                            auxisCamp = true;
                    }

                    validateProducSimulated();
                }
            });
        }
        String auxMonCamp, decimales;

        if(isConsumoCamp){
            if(getProducto()==null) {
                //Si no tiene oferta por simulador, no es necesario calcular el progreso de la barra, dado que está llena
                //Si tuviera oneclick y simulador, si necesita calcularse el progreso
                seekBarSimulador.setMax(1);
                seekBarSimulador.setProgress(1);
            }else {

                auxMonCamp = getPromocionConsumo().getMonto();
                auxMonCamp = auxMonCamp.substring(0, auxMonCamp.length() - 2);
                decimales = auxMonCamp.substring(auxMonCamp.length() - 2, auxMonCamp.length());

                auxMonCamp = Tools.formatUserAmount(auxMonCamp) + "." + decimales;

                Log.i("consumo", "auxMonCamp: " + auxMonCamp);

                updateGraph2(auxMonCamp);
            }
        }
    }

    private void validateProducSimulated(){

        if (isConsumoCamp){
            if (!auxisCamp){
                Log.i("consumo","consumo, se lanza por oneclick");

                delegate.consultaDetalleConsumoOneClick();

            }else if (isIndSimProd()){
                Log.i("consumo","simulado, elimina y recalcula");
                delegate.saveOrDeleteSimulationRequest(false); //true guarda, false elimina
            }else{
                Log.i("consumo","recalcula");
                setCvePlazo(txtvPlazo.getText().toString());
                delegate.doRecalculo();
            }

        }else if (isIndSimProd()){ //si ya fue simulado, elimina y recalcula
            delegate.saveOrDeleteSimulationRequest(false); //true guarda, false elimina
        }else{
            setCvePlazo(txtvPlazo.getText().toString());
            delegate.doRecalculo();
        }

    }

    private void setValuesProduct(){

        String montoMaximo, auxMMax, montoMinimo, auxMMin,
                montoFlow, cveProd, indSim;

        int imgProducto,descProd;

        if(getProducto()!= null){
            Log.i("update", "setValuesProduct, Monto máximo: " +getProducto().getMontoMaxS());
        }

        if (getProducto()!=null) {
            cveProd = getProducto().getCveProd();
            montoMaximo = getProducto().getMontoMaxS();
            montoMinimo = getProducto().getMontoMinS();
            indSim = getProducto().getIndSim();
            imgProducto = GuiTools.getCreditIcon(cveProd, false);
            descProd = GuiTools.getSelectedLabel(cveProd);
            montoFlow = getProducto().getMontoMaxS();   //valor que se tomará como monto máximo para saber si es mayor a 0.

            auxMMin = Tools.formatUserAmount(Tools.validateMont(montoMinimo, true));
            auxMMax = Tools.formatUserAmount(Tools.validateMont(montoMaximo,true));

            if(auxMMin.contains("$"))
                auxMMin = auxMMin.replace("$","");
            if(auxMMax.contains("$"))
                auxMMax = auxMMax.replace("$", "");

            setMontoMax(Tools.clearAmounr(auxMMax));
            setMontMin(Tools.clearAmounr(auxMMin));

            setIndSimProd(indSim.equalsIgnoreCase("S") ? true : false);
            txtvMontoMinimo.setText(auxMMin);
            edtMontoVisible.setMontoMin(getMontMin());
            edtMontoVisible.setCveProd(producto.getCveProd());
            edtMontoVisible.setText(Tools.formatUserAmount(getMontoMax()));
        }else{
            String decimales ="";
            cveProd = getPromocionConsumo().getCveCamp();
            imgProducto = GuiTools.getCreditIcon(cveProd, false);
            descProd = GuiTools.getSelectedLabel(cveProd);
            montoMaximo = getPromocionConsumo().getMonto();
            montoMaximo = montoMaximo.substring(0, montoMaximo.length() - 2);
            decimales = montoMaximo.substring(montoMaximo.length() - 2, montoMaximo.length());

            montoMaximo = Tools.formatUserAmount(montoMaximo);
            auxMMax = montoMaximo.replace("$", "");

            setMontoMax(Tools.clearAmounr(auxMMax));

            Log.i("montos","consumo nulo, setVisibleMont: "+montoMaximo);
            Log.i("montos","consumo nulo, edtMonMax: " + auxMMax);

            edtMontoVisible.setText(montoMaximo);
        }

        imgvProd.setImageResource(imgProducto);
        txtvDescProd.setText(descProd);

        txtvMontoMaximo.setText(auxMMax);
        edtMontoVisible.setMontoMax(getMontoMax());
    }

    private void validateSO(){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH){
            seekBarSimulador.setThumb(getResources().getDrawable(R.drawable.seek_bar_thumb));
            seekBarSimulador.setProgressDrawable(getResources().getDrawable(R.drawable.custom_seek_bar));
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setMinimumHeight(10);
            seekBarSimulador.setThumbOffset(0);
        }
    }

    private void findViews(View vista){
        imgvProd        = (ImageView)vista.findViewById(R.id.imgvProd);
        imgvShowDetalle = (ImageView)vista.findViewById(R.id.imgvShowDetalle);
        imgvShowDetalle.setOnClickListener(this);

        imgvAyuda       = (ImageView)vista.findViewById(R.id.imgvAyuda);
        imgvAyuda.setOnClickListener(this);

        imgvContratarCredito = (ImageButton)vista.findViewById(R.id.imgvContratarCredito);
        imgvContratarCredito.setOnClickListener(this);

        edtMontoVisible    = (LabelMontoTotalCredit)vista.findViewById(R.id.edtMontoVisible);

        txtvDescProd        = (TextView)vista.findViewById(R.id.txtvDescProd);
        txtvMontoMinimo     = (TextView)vista.findViewById(R.id.txtvMontoMinimo);
        txtvMontoMaximo     = (TextView)vista.findViewById(R.id.txtvMontoMaximo);
        txtvCat             = (TextView)vista.findViewById(R.id.txtvCat);
        txtvPlazo           = (TextView)vista.findViewById(R.id.txtvPlazo);
        txtvTasa            = (TextView)vista.findViewById(R.id.txtvTasa);
        txtvTipo            = (TextView)vista.findViewById(R.id.txtvTipo);
        txtvMasInfo         = (TextView)vista.findViewById(R.id.txtvMasInfo);
        txtvPagoMMax        = (TextView)vista.findViewById(R.id.txtvPagoMMax2);
        txtvMontoMMax       = (TextView)vista.findViewById(R.id.txtvMontoMMax2);

        lnlDetalleProd      = (LinearLayout)vista.findViewById(R.id.lnlDetalleProd);
        lnlTipo             = (LinearLayout)vista.findViewById(R.id.lnlTipo);
        lnlTipo.setOnClickListener(this);

        lnlPlazo            = (LinearLayout)vista.findViewById(R.id.lnlPlazo);

        lnlPlazoTipo        = (LinearLayout)vista.findViewById(R.id.lnlPlazoTipo);
        lnlPagoMMaximo      = (LinearLayout)vista.findViewById(R.id.lnlPagoMMaximo);
        lnlPagoMMaximo2     = (LinearLayout)vista.findViewById(R.id.lnlPagoMMaximo2);

        seekBarSimulador    = (SeekBar)vista.findViewById(R.id.seekBarSimulador);


        lnlPlazoTipo.setVisibility(View.VISIBLE);
        lnlPlazo.setVisibility(View.VISIBLE);
        lnlPagoMMaximo2.setVisibility(View.VISIBLE);
        lnlTipo.setVisibility(View.GONE);
        imgvContratarCredito.setVisibility(View.VISIBLE);

    }

    @Override
    public void onClick(View v) {

        if (v == imgvContratarCredito){
            if (isConsumoCamp){
                if (!auxisCamp){
                    Log.i("consumo","contrata consumo por oneclick");
                    delegate.iniciaflucjoOneClickConsumo();
                }else{
                    Log.i("consumo","contrata consumo por simulador");
                    delegate.saveOrDeleteSimulationRequest(true);
//                    delegate.contratarConsumoSimulador();
                }
            }else{
                delegate.saveOrDeleteSimulationRequest(true);
//                delegate.contratarConsumoSimulador();
            }
        }

        if (v == imgvAyuda) {
            Log.i("efi","cveCamp: "+getPromocionConsumo().getCveCamp());
            PopUpAyudaViewController.mostrarPopUp((MenuPrincipalActivitySeekBar)getActivity(),getPromocionConsumo().getCveCamp());
        }

        if(v == imgvShowDetalle){

            if(isSimulated){ //si ya fue simulado, muestra detalle
                Log.i("simular","producto simulado");
                validateLayoutToShow();
            }else{
                Log.w("simular","producto no simulado");
                isFromBar = false;
                validateProducSimulated();
            }
        }
    }

    public void setSimulation(){
        if(isppi)
            ((MenuPrincipalActivitySeekBar) getActivity()).isPPIS = true;
        else
            ((MenuPrincipalActivitySeekBar) getActivity()).isOnomS = true;

        ((MenuPrincipalActivitySeekBar) getActivity()).validateTxtvColor(controller);

        setColor(false);
    }

    public void setColor(boolean isNormal){
        if(isNormal)
            edtMontoVisible.setTextColor(getResources().getColor(R.color.nuevaimagen_tx5));
        else
            edtMontoVisible.setTextColor(getResources().getColor(R.color.naranja));
    }

    public void pickerPlazo(){
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View DialogView = inflater.inflate(R.layout.activity_credit_picker, null);

        final NumberPicker dialogPicker = (NumberPicker)DialogView.findViewById(R.id.dialogPicker);
        dialogPicker.setMinValue(0);
        dialogPicker.setMaxValue(termCreditList.length - 1);
        dialogPicker.setDisplayedValues(termCreditList);
        dialogPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        dialogPicker.setValue(getPosition(termCreditList, txtvPlazo.getText().toString()));
        dialogPicker.setWrapSelectorWheel(false);


        new AlertDialog.Builder(getActivity()).setView(dialogPicker)
                .setTitle("Seleccione una opción")
                .setView(DialogView)
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }

                })
                .setPositiveButton("Seleccionar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        oDialog = dialog;
                        plazo = termCreditList[dialogPicker.getValue()];
                        txtvPlazo.setText(plazo);

                        String cvePlazo = buscarClave(producto.getDessubpE(), plazo, buscarPlazo);
                        String cveSub = producto.getSubprodE();

                        delegate.updateData(cveSub, cvePlazo, true);

                    }

                }).show();
    }

    public void validateLayoutToShow(){

        //oculta detalle
        if(isShowDetalle()) {
            setIsShowDetalle(false);
            lnlDetalleProd.setVisibility(View.GONE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_masdetalle);

        }else{

            ((MenuPrincipalActivitySeekBar)getActivity()).isPPIetail = true;
            ((MenuPrincipalActivitySeekBar)getActivity()).validateIsShowAnyDetail(this);

            setIsShowDetalle(true);
            lnlDetalleProd.setVisibility(View.VISIBLE);
            imgvShowDetalle.setImageResource(R.drawable.nvo_icon_menosdetalle);

            setDetailProduct();
        }
    }

    public String validateDecimal(String value){
        String amountToValidate = value, decimal;

        if(amountToValidate.endsWith(".00")){
            amountToValidate = amountToValidate.substring(0,amountToValidate.length()-2);
        }else{
            decimal = amountToValidate.substring(amountToValidate.length()-2, amountToValidate.length());
            amountToValidate = GuiTools.getMoneyString(amountToValidate.substring(0, amountToValidate.length() - 2));
            amountToValidate = amountToValidate.substring(0, amountToValidate.length() - 2);

            amountToValidate = amountToValidate+decimal;
        }


        return amountToValidate;
    }

    public void setDetailProduct(){
        try {
            //        ArrayList<Producto>detail = Session.getInstance().getAuxCreditos().getProductos();
            Producto newProducto = delegate.getProducto();

            String cat, tasa, totalMesQuincena, plazo;

            //pinta detalle de oneclick
            if (isConsumoCamp && !auxisCamp){
                tasa = getOfertaConsumo().getTasaAnual();
                cat = getOfertaConsumo().getCat();
                totalMesQuincena = validateDecimal(getOfertaConsumo().getPagoMenFijo());
                txtvPlazo.setText(getOfertaConsumo().getPlazoDes());
                txtvMontoMMax.setText(totalMesQuincena);
            }else{
                //pinta detalle de simulador
                tasa = newProducto.getTasaS();
                cat = newProducto.getCATS();
                totalMesQuincena = newProducto.getPagMProdS();
                txtvMontoMMax.setText(GuiTools.getMoneyString(totalMesQuincena));
            }

            txtvPagoMMax.setText(getResources().getString(R.string.detalle_adelanto_nomina_pago_mensual));
            txtvTasa.setText("Tasa de interés: "+tasa+" %");
            txtvCat.setText("CAT: "+cat+"% sin iva");


        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void updateData(boolean isOperationDelete){
        Log.i("updatedata","se manda actualizar productos desde tdc");
        ((MenuPrincipalActivitySeekBar)getActivity()).updateAllProductData(this,isOperationDelete);
    }

    private int calculatePogress(int progress, int min, int progresFinal, int mmax){

        int auxMin = min;

        if (progress == 0){
            return min;
        }else {

            for (int i = 1; i<=progress; i++) {
                auxMin = auxMin + INCREMENTAL;
                if (progress == progresFinal){
                    auxMin = mmax;
                }
            }
        }

        return auxMin;
    }

    public void reset() {
        this.isResetting = true;
        this.typedString=new StringBuffer();
        this.isResetting = false;
    }

    private void setFormattedText(){

        isSettingText = true;
        String text = typedString.toString();

        edtMontoVisible.setText(Tools.formatUserAmount(text));
    }

    public String buscarClave(String desSub, String value, boolean cvePlazo){
        Producto aux = productoAux;
        String clave = "";

        for(Subproducto auxSP : aux.getSubproducto()) {
            if(desSub.equals(auxSP.getDesSubp())) {
                if (cvePlazo) { //busca plazo
                    for (int i = 0; i < auxSP.getPlazo().size(); i++) {
                        if (value.equals(auxSP.getPlazo().get(i).getDesPlazo())) {
                            clave = auxSP.getPlazo().get(i).getCvePlazo();
                        }
                    }
                }else{ //busca opcion
                    clave = auxSP.getCveSubp();
                }
            }
        }
        return clave;
    }

    private int getPosition(String [] array, String code) {
        for(int i= 0; i< array.length;i++)
        {
            if(array[i].equalsIgnoreCase(code))
                return i;
        }
        return -1;
    }

    public Promociones getPromocionConsumo() {
        return promocionConsumo;
    }

    public void setPromocionConsumo(Promociones promocionConsumo) {
        this.promocionConsumo = promocionConsumo;
    }

    public OfertaConsumo getOfertaConsumo() {
        return ofertaConsumo;
    }

    public void setOfertaConsumo(OfertaConsumo ofertaConsumo) {
        this.ofertaConsumo = ofertaConsumo;
    }

    public String getCvePlazo() {
        return cvePlazo;
    }

    public void setCvePlazo(String cvePlazo) {
        this.cvePlazo = cvePlazo;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public String getMontoMax() {
        return montoMax;
    }

    public void setMontoMax(String montoMax) {
        this.montoMax = montoMax;
    }

    public String getMontMin() {
        return montMin;
    }

    public void setMontMin(String montMin) {
        this.montMin = montMin;
    }

    public boolean isIndSimProd() {
        return indSimProd;
    }

    public void setIndSimProd(boolean indSimProd) {
        this.indSimProd = indSimProd;
    }

    public boolean isShowDetalle() {
        return isShowDetalle;
    }

    public void setIsShowDetalle(boolean isShowDetalle) {
        this.isShowDetalle = isShowDetalle;
    }

    public String getAuxFinalMax() {
        return auxFinalMax;
    }

    public void setAuxFinalMax(String auxFinalMax) {
        this.auxFinalMax = auxFinalMax;
    }

}