package bbvacredit.bancomer.apicredit.gui.delegates;


import bbvacredit.bancomer.apicredit.io.ServerResponse;

public interface OperationNetworkCallback {
	/**
     * Se procesa la respuesta obtenida del servidor.
     * @param operationId el identificador de la operaci�n
     * @param response la respuesta del servidor
     */
    public void analyzeResponse(String operationId, ServerResponse response);
}
