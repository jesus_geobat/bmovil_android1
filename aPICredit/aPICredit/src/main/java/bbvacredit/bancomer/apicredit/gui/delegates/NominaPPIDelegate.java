package bbvacredit.bancomer.apicredit.gui.delegates;

import android.content.SharedPreferences;
import android.util.Log;

import java.util.Hashtable;
import java.util.Iterator;

import bbvacredit.bancomer.apicredit.common.ConstantsCredit;
import bbvacredit.bancomer.apicredit.common.Session;
import bbvacredit.bancomer.apicredit.common.Tools;
import bbvacredit.bancomer.apicredit.controllers.MainController;
import bbvacredit.bancomer.apicredit.gui.activities.DetalleNominaPPIViewController;
import bbvacredit.bancomer.apicredit.gui.activities.NominaPPIViewController;
import bbvacredit.bancomer.apicredit.io.AuxConectionFactoryCredit;
import bbvacredit.bancomer.apicredit.io.Server;
import bbvacredit.bancomer.apicredit.io.ServerConstantsCredit;
import bbvacredit.bancomer.apicredit.io.ServerResponse;
import bbvacredit.bancomer.apicredit.models.CalculoData;
import bbvacredit.bancomer.apicredit.models.ObjetoCreditos;
import bbvacredit.bancomer.apicredit.models.Producto;


/**
 * Created by OOROZCO on 12/3/15.
 */
public class NominaPPIDelegate extends BaseDelegateOperacion {

    private Session session;
    private Integer productIndex = 0;
    private Producto producto = null;
    private ObjetoCreditos data;
    private NominaPPIViewController controller;
    private boolean isSavingOperation = false;
    private boolean isPPI;
    private boolean updateData = false;
    public boolean isDeleteOperation= false;
    private DetalleNominaPPIViewController detalleNomina;

    public DetalleNominaPPIViewController getDetalleNomina() {
        return detalleNomina;
    }

    public void setDetalleNomina(DetalleNominaPPIViewController detalleNomina) {
        this.detalleNomina = detalleNomina;
    }

    public void setPPI(boolean isPPI) {
        this.isPPI = isPPI;
    }

    public boolean isPPI() {
        return isPPI;
    }

    public Producto getProducto() {
        return producto;
    }

    public ObjetoCreditos getData() {
        return data;
    }

    public NominaPPIDelegate(NominaPPIViewController controller) {
        session = Tools.getCurrentSession();
        data = session.getCreditos();
        MainController.getInstance().muestraIndicadorActividad("Operación", "Conectando");
        setProducto();
        MainController.getInstance().ocultaIndicadorActividad();
        this.controller = controller;
    }

    private void setProducto(){
        SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
        productIndex = sp.getInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, 0);
        producto = data.getProductos().get(productIndex);
    }

    private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }else{
            paramTable.put(cnt, "");
            Log.d(param.getClass().getName(), param.toString() + " empty or null");
        }
    }

    private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
        if(!Tools.isEmptyOrNull(param.toString())){
            paramTable.put(cnt, param.toString());
        }
    }

    public void doRecalculo(){
        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);

        Producto ccr = data.getProductos().get(productIndex);

        String plazoSel = controller.buscarClave(producto.getDessubpE(),controller.getCvePlazo(),controller.buscarPlazo);
        int montoSel = Integer.valueOf(controller.getMonto());

        addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM,paramTable);
        addParametro(ccr.getSubproducto().get(0).getCveSubp(), ServerConstantsCredit.CVESUBP_PARAM,paramTable);
        addParametro(plazoSel, ServerConstantsCredit.CVEPLAZO_PARAM,paramTable);
        addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM,paramTable);


        Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(getCodigoOperacion(), paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }

    private void setSimLooking4Cve(String cve){
        Iterator<Producto> it = data.getProductos().iterator();

        while(it.hasNext()){
            Producto pr = it.next();
            if(pr.getCveProd().equals(cve)) pr.setIndSimBoolean(true);
        }
    }

    public void analyzeResponse(String operationId, ServerResponse response) {
        if(getCodigoOperacion().equals(operationId)){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                //Set the new credits to session.
                session.setCreditos(data);
                productIndex = session.getProductoIndexByCve(isPPI ? ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO : ConstantsCredit.CREDITO_NOMINA);
                producto = data.getProductos().get(productIndex);

                if(data.getProductos().get(productIndex).getCveProd().equals(ConstantsCredit.CREDITO_NOMINA))
                   data.getProductos().get(productIndex).setIndSimBoolean(true);
                else
                   setSimLooking4Cve(isPPI? ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO: ConstantsCredit.CREDITO_NOMINA);

                setProducto();

                saveOrDeleteSimulationRequest(true);
            }
        }else if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId)) {
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

                CalculoData respuesta = (CalculoData) response.getResponse();
                data = null;
                data = new ObjetoCreditos();

                data.setCreditos(respuesta.getCreditos());
                data.setEstado(respuesta.getEstado());
                data.setMontotSol(respuesta.getMontotSol());
                data.setPagMTot(respuesta.getPagMTot());
                data.setPorcTotal(respuesta.getPorcTotal());
                data.setProductos(respuesta.getProductos());
                data.setCreditosContratados(session.getCreditos().getCreditosContratados());

                session.setCreditos(null);
                session.setCreditos(data);
                productIndex = session.getProductoIndexByCve(ConstantsCredit.CREDITO_AUTO);
                producto = data.getProductos().get(productIndex);

                setProducto();

                if(isDeleteOperation){
                    Log.i("simulador","se eliminó operacion NominaPPI");
                    isDeleteOperation = false;
                    getDetalleNomina().simpleUpdateMenu(producto.getCveProd());
                }
                else if(updateData) {
                    Log.i("simulador", "Datos actualizados, actualiza vista");
                    updateData = false;
                    controller.updateMenu();
                }else {
                    Log.i("revire","cveProd: "+producto.getCveProd());
                    Log.i("revire","indRev: "+producto.getIndRev());
                    Log.w("revire", "TasaOri: " + producto.getTasaOri());
                    Log.w("revire", "Tasa: " + producto.getTasaS());
                    controller.showDetalleNominaPPI();
                }
            }
        }
    }


    @Override
    protected String getCodigoOperacion() {
        // TODO Auto-generated method stub
        return Server.CALCULO_OPERACION;
    }


    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {
        this.isSavingOperation = isSavingOperation;
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        addParametroObligatorio(Server.CALCULO_ALTERNATIVAS_OPERACION, ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        if(isSavingOperation) {
            addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = false;
        }else {
            addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);
            isDeleteOperation = true;
        }

        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }


    public void oneClickILC() {
        Session.getInstance().setCveCamp(producto.getCveProd());
        Session session = Session.getInstance(MainController.getInstance().getContext());

        DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(session.getIdUsuario(),
                session.getIum(), session.getNumCelular(), producto.getCveProd(),getProducto());
        delegate.consultaDetalleAlternativasTask(getProducto());
    }

    public void updateData(String cveSub, String cvePzoE){

        updateData = true;
        isSavingOperation = false;

        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        addParametroObligatorio(this.getCodigoOperacion(), ServerConstantsCredit.OPERACION_PARAM, paramTable);
        addParametroObligatorio(session.getIum(), ServerConstantsCredit.IUM_PARAM, paramTable);
        addParametroObligatorio(session.getIdUsuario(), ServerConstantsCredit.CLIENTE_PARAM, paramTable);
        addParametroObligatorio(session.getNumCelular(), ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

        addParametroObligatorio(ConstantsCredit.OP_AJUSTE_PLAZOS, ServerConstantsCredit.TIPO_OP_PARAM, paramTable);

        Producto ccr = data.getProductos().get(productIndex);

        addParametroObligatorio(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM, paramTable);
        addParametroObligatorio(cvePzoE, ServerConstantsCredit.CVEPLAZO_PARAM, paramTable);
        addParametroObligatorio("", ServerConstantsCredit.MON_DESE_PARAM, paramTable);
        addParametroObligatorio(cveSub, ServerConstantsCredit.CVESUBP_PARAM, paramTable);
        Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
        this.doNetworkOperation(Server.CALCULO_ALTERNATIVAS_OPERACION, paramTable2,true, new CalculoData(),MainController.getInstance().getContext());
    }
}

