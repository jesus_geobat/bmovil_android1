package com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contactanos;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.commons.Constants;
import com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.commons.Tools;

import contactanosapi.mbanking.bancomer.com.contactanosapi.R;

/**
 * Created by eluna on 10/11/2015.
 */
public class ContactanosViewController {

    public void showContactanos(final Activity ctx) {
        if (Tools.appInstalled(ctx.getString(R.string.uri_linea_bancomer), ctx)) {
            appLineaBancomer(ctx);
        } else {
            PopUpImageView.mostrarPopUp(ctx);
        }
    }

    private void appLineaBancomer(final Context context) {
        Intent i = new Intent(Intent.ACTION_MAIN);
        final PackageManager manager = context.getPackageManager();
        i = manager.getLaunchIntentForPackage(context.getString(R.string.uri_linea_bancomer));
        i.setAction(Intent.ACTION_SEND);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        context.startActivity(i);
    }
}
