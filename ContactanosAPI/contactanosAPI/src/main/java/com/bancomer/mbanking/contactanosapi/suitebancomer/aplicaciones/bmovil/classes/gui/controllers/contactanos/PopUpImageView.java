package com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contactanos;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.commons.GuiTools;

import contactanosapi.mbanking.bancomer.com.contactanosapi.R;

/**
 * Created by andres.vicentelinare on 18/07/2016.
 */
public class PopUpImageView {

    public static void mostrarPopUp(final Activity activity) {

        final Dialog contactDialog = new Dialog(activity);
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View vista = inflater.inflate(R.layout.activity_popupimageview, null);
        contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // make dialog itself transparent
        contactDialog.setCancelable(true);
        contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(activity.getResources().getColor(android.R.color.transparent)));
        contactDialog.setContentView(vista);
        final ImageButton btnCancel = (ImageButton) vista.findViewById(R.id.imgCancelar);
        Button btnAceptar = (Button) vista.findViewById(R.id.btnAceptar);
        final RelativeLayout layoutCancel = (RelativeLayout) vista.findViewById(R.id.layout_cancelar);
        TextView textView = (TextView) vista.findViewById(R.id.tvMensaje);
        final TextView textTitulo = (TextView) vista.findViewById(R.id.titulo);

        String[] strings = new String[]{"¿Deseas ir a la ", "tienda", " para ", "\ndescargarla", "?"};
        String string = strings[0] + strings[1] + strings[2] + strings[3] + strings[4];
        SpannableString spannableString = new SpannableString(string);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), string.indexOf(strings[1]), string.indexOf(strings[1]) + strings[1].length(), 0);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), string.indexOf(strings[3]), string.indexOf(strings[3]) + strings[3].length(), 0);
        textView.append(spannableString);

        int original_width = activity.getResources().getDisplayMetrics().widthPixels;
        final int width = (int)(activity.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = 0;
        if(original_width > 500) {
            height = (int) (activity.getResources().getDisplayMetrics().heightPixels * 0.80);
        }else{
            height = (int) (activity.getResources().getDisplayMetrics().heightPixels * 0.90);
        }
        contactDialog.getWindow().setLayout(width, height);

        final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(contactDialog.getWindow().getWindowManager());
        guiTools.scale(textTitulo, true);
        guiTools.scale(textView, true);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDialog.dismiss();
            }
        });

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDialog.dismiss();

                aceptarClick(activity);
            }

        });

        layoutCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactDialog.dismiss();
            }
        });
        contactDialog.show();

    }

    private static void aceptarClick(Context context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.uri_prefix_app_market) + context.getString(R.string.uri_linea_bancomer))));
        } catch (ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.uri_prefix_app_market_web) + context.getString(R.string.uri_linea_bancomer))));

        }
    }
}
