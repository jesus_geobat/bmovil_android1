package bancomer.api.alertasdigitales.gui.delegates;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import bancomer.api.alertasdigitales.gui.controllers.AlertasDigitalesViewController;
import bancomer.api.alertasdigitales.implementations.BaseDelegateImpl;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.ParsingHandler;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.alertasdigitales.models.ConsultaCuentasAlertasData;
import bancomer.api.alertasdigitales.models.Cuenta;
import bancomer.api.alertasdigitales.models.DetalleAlertasDigitalesData;
import bancomer.api.alertasdigitales.models.DetalleCuentaAlertasData;
import bancomer.api.alertasdigitales.models.DetalleMensajesCuentaData;
import bancomer.api.common.commons.ServerConstants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.io.ServerResponse;


public class AlertasDigitalesDelegate extends BaseDelegateImpl {

	public final static long ALERTAS_DIGITALES_DELEGATE_ID = 0x4fd454ce67b39f5L;

	public static boolean banderaMostrarDetalle = false;

	private AlertasDigitalesViewController alertasDigitalesViewController;

	private ConsultaCuentasAlertasData consultaCuentasAlertasData;

	private DetalleMensajesCuentaData detalleMensajesCuentaData;

	private Cuenta cuentaSeleccionada;

	private DetalleAlertasDigitalesData detalleAlertasDigitalesData;

	public DetalleCuentaAlertasData getDetalleCuentaAlertasData() {
		return detalleCuentaAlertasData;
	}
	public DetalleCuentaAlertasData getDetalleCuentaAlertasData(int pos) {
		return cuenta.get(pos);
	}
	public void setDetalleCuentaAlertasData(DetalleCuentaAlertasData detalleCuentaAlertasData) {
		this.detalleCuentaAlertasData = detalleCuentaAlertasData;
	}

	private DetalleCuentaAlertasData detalleCuentaAlertasData;
	private boolean isMensajeAbono = false;

	/**
	 * Getters & Setters
	 */
	public ConsultaCuentasAlertasData getConsultaCuentasAlertasData() {
		return consultaCuentasAlertasData;
	}

	public Cuenta getCuentaSeleccionada() {
		return cuentaSeleccionada;
	}

	public void setCuentaSeleccionada(Cuenta cuentaSeleccionada) {
		this.cuentaSeleccionada = cuentaSeleccionada;
	}

	public void setConsultaCuentasAlertasData(
			ConsultaCuentasAlertasData consultaCuentasAlertasData) {
		this.consultaCuentasAlertasData = consultaCuentasAlertasData;
	}

	public AlertasDigitalesViewController getAlertasDigitalesViewController() {
		return alertasDigitalesViewController;
	}

	public void setAlertasDigitalesViewController(
			AlertasDigitalesViewController alertasDigitalesViewController) {

		if (Server.ALLOW_LOG)
			Log.i("[CGI-Configuracion-Obligatorio] >> ", "[AlertasDigitalesDelegate] Se setea el viewController para AlertasDigitalesViewController");
		this.alertasDigitalesViewController = alertasDigitalesViewController;
	}

	public DetalleAlertasDigitalesData getDetalleAlertasDigitalesData() {
		return detalleAlertasDigitalesData;
	}

	public void setDetalleAlertasDigitalesData(DetalleAlertasDigitalesData detalleAlertasDigitalesData) {
		this.detalleAlertasDigitalesData = detalleAlertasDigitalesData;
	}

	/**
	 * End Getters & Setters
	 */

	public void performAction(Object obj) {

		// Hacer show de la ventana de detalle
//		movimientoSeleccionadoCOC =  AlertasDigitalesViewController.getListaSeleccion().getOpcionSeleccionada();
		cuentaSeleccionada = consultaCuentasAlertasData.getListaCuentas().get(alertasDigitalesViewController.getListaSeleccion().getOpcionSeleccionada());
		if (isMensajeAbono == false) {
			if ("S".equals(cuentaSeleccionada.getIndAlertas())) {
				detalleAlertasDigitales();
				alertasDigitalesViewController.ocultarAyudaCuentaSinAlertas();
			} else {
				alertasDigitalesViewController.mostrarAyudaCuentaSinAlertas();
			}
		} else {
			detalleMenEnvPorCuenta();
		}

	}

	public void performAction(int i) {
		cuentaSeleccionada = consultaCuentasAlertasData.getListaCuentas().get(i);
		if (isMensajeAbono == false) {
			if ("S".equals(cuentaSeleccionada.getIndAlertas())) {
				detalleAlertasDigitales();
				alertasDigitalesViewController.ocultarAyudaCuentaSinAlertas();
			} else {
				alertasDigitalesViewController.mostrarAyudaCuentaSinAlertas();
			}
		} else {
			detalleMenEnvPorCuenta();
		}

	}

	public ArrayList<DetalleCuentaAlertasData> cuenta = new ArrayList<DetalleCuentaAlertasData>();

	public void consultaCuentasAlertasDigitales() {

		//alertasDigitalesViewController.getBaseViewController().muestraIndicadorActividad(alertasDigitalesViewController,
		//		alertasDigitalesViewController.getString(R.string.alert_operation),
		//		alertasDigitalesViewController.getString(R.string.alert_connecting));

		//prepare data
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = 0;
		operacion = Server.OP_CONSULTA_CUENTAS_ALERTAS;

		doNetworkOperation(operacion, paramTable, true, new ConsultaCuentasAlertasData(), alertasDigitalesViewController.getBaseViewController());
	}

	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, final boolean isJson,
								   final ParsingHandler handler, BaseViewController caller) {
		InitAlertasDigitales.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params, isJson, handler, caller, true);
	}

	public void analyzeResponse(int operationId, ServerResponse response) {
		if (operationId == Server.OP_DETALLE_CUENTA_ALERTAS) {
			if (Server.ALLOW_LOG) Log.d(">> CGI", "Entra por consulta Detalle cuentas alertas");
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if (Server.ALLOW_LOG) Log.d(">> CGI", "Consulta otros creditos >> Success");
				alertasDigitalesViewController.obtencion_detalles((DetalleCuentaAlertasData) response.getResponse());
				detalleCuentaAlertasData = (DetalleCuentaAlertasData) response.getResponse();
				setDetalleCuentaAlertasData(detalleCuentaAlertasData);
				InitAlertasDigitales.getInstance().getBaseViewController().ocultaIndicadorActividad();
				if (banderaMostrarDetalle) {
					banderaMostrarDetalle = false;
					InitAlertasDigitales.getInstance().showDetallesAlertasDigitales(detalleCuentaAlertasData, cuentaSeleccionada);
				}

				//alertasDigitalesViewController.cargaListaDatos(detalleCuentaAlertasData);

			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				if (Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Warning");

				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(alertasDigitalesViewController, response.getMessageText());

			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				if (Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Error");

				InitAlertasDigitales.getInstance().getBaseViewController().ocultaIndicadorActividad();
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(alertasDigitalesViewController, response.getMessageText(), (new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if (Server.ALLOW_LOG)
							Log.d(">> CGI", "Consulta otros creditos >> EX#2 Error en el servicio");
						alertasDigitalesViewController.getBaseViewController().goBack(alertasDigitalesViewController, alertasDigitalesViewController.getParentManager());
						alertasDigitalesViewController.irAtras();

					}
				}));

			}
		} else if (operationId == Server.OP_CONSULTA_CUENTAS_ALERTAS) {
			if (Server.ALLOW_LOG) Log.d(">> CGI", "Entra por consulta cuentas alertas");
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if (Server.ALLOW_LOG) Log.d(">> CGI", "Consulta cuentas alertas >> Success");
				consultaCuentasAlertasData = (ConsultaCuentasAlertasData) response.getResponse();
				alertasDigitalesViewController.obtencion_detalles(detalleCuentaAlertasData);
				//alertasDigitalesViewController.llenaListaSeleccion();
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				if (Server.ALLOW_LOG) Log.d(">> CGI", "Consulta cuentas alertas >> Warning");
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(alertasDigitalesViewController, response.getMessageText());

			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				if (Server.ALLOW_LOG) Log.d(">> CGI", "Consulta cuentas alertas >> Error");
				InitAlertasDigitales.getInstance().getBaseViewController().ocultaIndicadorActividad();
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(alertasDigitalesViewController, response.getMessageText(), (new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if (Server.ALLOW_LOG)
							Log.d(">> CGI", "Consulta cuentas alertas >> EX#1 Error en el servicio");
						alertasDigitalesViewController.getBaseViewController().goBack(alertasDigitalesViewController, alertasDigitalesViewController.getParentManager());
						alertasDigitalesViewController.irAtras();
					}
				}));
			}
		} else if (operationId == Server.OP_MENSAJES_ENVIADOS_ALERTAS) {
			if (Server.ALLOW_LOG) Log.d(">> CGI", "Entra por consulta cuentas alertas");
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if (Server.ALLOW_LOG)
					Log.d(">> CGI", "Consulta Operaciones Mensajes Abono>> Success");
				detalleMensajesCuentaData = (DetalleMensajesCuentaData) response.getResponse();

				InitAlertasDigitales.getInstance().showOperacionesAlertasDigitales(detalleMensajesCuentaData, cuentaSeleccionada);


			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				if (Server.ALLOW_LOG)
					Log.d(">> CGI", "Consulta Operaciones Mensajes Abono >> Warning");

				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(alertasDigitalesViewController, response.getMessageText());

			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				if (Server.ALLOW_LOG)
					Log.d(">> CGI", "Consulta Operaciones otros creditos >> Error");

				InitAlertasDigitales.getInstance().getBaseViewController().ocultaIndicadorActividad();
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(alertasDigitalesViewController, response.getMessageText(), (new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if (Server.ALLOW_LOG)
							Log.d(">> CGI", "Consulta Operaciones creditos >> EX#2 Error en el servicio");
						alertasDigitalesViewController.getBaseViewController().goBack(alertasDigitalesViewController, alertasDigitalesViewController.getParentManager());
						alertasDigitalesViewController.irAtras();

					}
				}));
			}
		}
	}

	private String getTipoCredito(String tipo) {
		String ret = tipo;
		if (tipo.equalsIgnoreCase("C")) {
			ret = "Consumo";
		} else if (tipo.equalsIgnoreCase("H")) {
			ret = "Hipotecario";
		}
		return ret;
	}

	/*public String getTipoCta(String key) {
        Map<String, String> mapTipoCta = new HashMap<String, String>();
		mapTipoCta.put("TC", "T. de crédito");
		mapTipoCta.put("LI", "Libretón");
		mapTipoCta.put("AH", "Cuenta");
		mapTipoCta.put("CE", "Cuenta express");
		mapTipoCta.put("TP", "T. Prepago");
		mapTipoCta.put("CH", "Cuenta");
		mapTipoCta.put("AD", "T. adicional");
		mapTipoCta.put("MI", "T. mini");

		if (mapTipoCta.containsKey(key)) {
			return mapTipoCta.get(key);
		}

		return "Cuenta";
	}*/

	public ArrayList<Object> getDatosTabla() {

		final ArrayList<Object> datos = new ArrayList<Object>();
		ArrayList<String> registro;

		if ((null == consultaCuentasAlertasData) || consultaCuentasAlertasData.getListaCuentas().isEmpty()) {
			registro = new ArrayList<String>();
			registro.add(null);
			registro.add("");
			registro.add("");
		} else {
			for (final Cuenta cuenta : consultaCuentasAlertasData.getListaCuentas()) {
				registro = new ArrayList<String>();
				registro.add(null);
				registro.add(cuenta.getNombreTipoCta());
				registro.add(Tools.hideAccountNumber(cuenta.getNumCuenta()));
				registro.add(cuenta.getIndAlertas());
				Log.i("ActivacionAlerta",cuenta.getIndAlertas());
				datos.add(registro);
			}
		}
		return datos;
	}

	public void detalleAlertasDigitales() {
		if (Server.ALLOW_LOG) Log.d(">> CGI", "Relleno petición Detalle Consulta otros Créditos");
		//ingresaDatosAlertaViewController.getBaseViewController().muestraIndicadorActividad(ingresaDatosAlertaViewController,
		//		ingresaDatosAlertaViewController.getString(R.string.alert_operation),
		//		ingresaDatosAlertaViewController.getString(R.string.alert_connecting));
		//InitAlertasDigitales.getInstance().getBaseViewController().muestraIndicadorActividad(ingresaDatosAlertaViewController,
		//		ingresaDatosAlertaViewController.getString(R.string.alert_operation),
		//		ingresaDatosAlertaViewController.getString(R.string.alert_connecting));
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = Server.OP_DETALLE_CUENTA_ALERTAS;
		paramTable.put(ServerConstants.NUM_CUENTA, cuentaSeleccionada.getNumCuenta());
		doNetworkOperation(operacion, paramTable, true, new DetalleCuentaAlertasData(), alertasDigitalesViewController.getBaseViewController());
	}

	public static String banderaingresa = null;

	public void detalleAlertasDigitales(String num_cuenta) {
		if (Server.ALLOW_LOG) Log.d(">> CGI", "Relleno petición Detalle Consulta otros Créditos");
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = Server.OP_DETALLE_CUENTA_ALERTAS;
		paramTable.put(ServerConstants.NUM_CUENTA, num_cuenta);
		doNetworkOperation(operacion, paramTable, true, new DetalleCuentaAlertasData(), alertasDigitalesViewController.getBaseViewController());
	}

	private void detalleMenEnvPorCuenta() {
		if (Server.ALLOW_LOG) Log.d(">> CGI", "Relleno petición Detalle Consulta otros Créditos");

		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = Server.OP_MENSAJES_ENVIADOS_ALERTAS;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		String fechaFin = sdf.format(c.getTime());
		String fechaInicio = sdf.format(calcularMes(c).getTime());

		paramTable.put(ServerConstants.NUMERO_TELEFONO, InitAlertasDigitales.getInstance().getConsultaSend().getUsername());
		paramTable.put(ServerConstants.NUM_CUENTA_REDUX, cuentaSeleccionada.getNumCuenta());
		paramTable.put(ServerConstants.FECHA_INICIO, fechaInicio);
		paramTable.put(ServerConstants.FECHA_FIN, fechaFin);

		doNetworkOperation(operacion, paramTable, true, new DetalleMensajesCuentaData(), alertasDigitalesViewController.getBaseViewController());
	}


	public void setIsMensajeAbono(boolean isMensajeAbono) {
		this.isMensajeAbono = isMensajeAbono;
	}

	private Calendar calcularMes(Calendar cal) {
		cal.add(Calendar.MONTH, -2);
		return cal;
	}
}
