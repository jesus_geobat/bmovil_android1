/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package bancomer.api.alertasdigitales.io;

import java.util.Hashtable;

import bancomer.api.common.session.CommonSession;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;


/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receive the network operation
 * requests and builds a network operation suitable for HttpInvoker class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponse object, so that the application logic can
 * perform high level checks and processes.
 * 
 * @author Stefanini IT Solutions.
 */
public class Server  extends  ServerCommons{


	/**
	 * Indicates simulation usage.
	 */
	public static final long SIMULATION_RESPONSE_TIME = 5;

	/**
	 * Bancomer Infrastructure.
	 */
	public static final int BANCOMER = 0;

	/**
	 * Stefanini Infrastructure.
	 */
	public static final int STEFANINI = 1;

	/**
	 * Last operation provider.
	 */

	// Change this value to bancomer for production setup
	// public static int PROVIDER = STEFANINI;
	public static int PROVIDER = BANCOMER; // TODO remove for TEST

	// Simulates profile change status
	public static String INSTRUMENT_TYPE = "";// Constants.IS_TYPE_DP270;

	public static String PROFILE_BASIC = "*PRMF01*IS";
	public static String PROFILE_ADVANCED_OCRA = "*PRMF03*IST6";
	public static String PROFILE_ADVANCED_DP270 = "*PRMF03*IST3";
	public static String PROFILE_ADVANCED_SOFTTOKEN = "*PRMF03*ISS1";
	public static String PROFILE_CHANGE_BASIC_TO_ADVANCED = "*PRMF01*IST3";
	public static String PROFILE_CHANGE_ADVANCED_TO_BASIC = "*PRMF03*ISD2";

	//public static String CURRENT_PROFILE_STRING = PROFILE_CHANGE_BASIC_TO_ADVANCED;
	public static String CURRENT_PROFILE_STRING = PROFILE_ADVANCED_SOFTTOKEN;//PROFILE_CHANGE_BASIC_TO_ADVANCED;


	// SIMULATES DIFFERENT ACCOUNT SETS
	public static String ACCOUNTS_ONE_EXPRESS = "ESOK*TED0026805*FE05012013*HR190752*TO2*CT*OC1*TPCE*AL*DVMX*AS00740036002753018236*IM0*CPC*VIS";
	public static String ACCOUNTS_ONE_ACCOUNT = "ESOK*TED0026805*FE05012013*HR190752*TO2*CT*OC1*TPLI*AL*DVMX*AS00743616002700628034*IM50000*CPC*VIS";
	public static String ACCOUNTS_TDC_IS_MAIN = "ESOK*TED0009006*FE07012013*HR085700*TO2*CT*OC3*TPTP*AL*DVMX*AS00740036002753594688*IM286504*CPC*VIN*TPTC*ALAZUL*DVMX*AS4931612268862560*IM000*CPC*VIN*TPTC*AL*DVMX*AS4931612501079881*IM000*CPC*VIS";
	public static String ACCOUNTS_MULTIPLE_ACCOUNTS_ZERO_BALANCE = "ESOK*TED0009006*FE05012013*HR085700*TO2*CT*OC5*TPTP*AL*DVMX*AS00740036002753594688*IM286504*CPC*VIS*TPLI*AL*DVMX*AS00740036002753938274*IM286504*CPC*VIN*TPCE*AL*DVMX*AS00740036002753018236*IM0*CPC*VIN*TPTC*ALAZUL*DVMX*AS4931612268862560*IM000*CPC*VIN*TPTC*AL*DVMX*AS4931612501079881*IM000*CPC*VIN";
	public static String ACCOUNTS_ALL_ACCOUNTS = "ESOK*TED0026805*FE27042015*HR062500*TO2*CT*OC8*TPLI*AL*DVMX*AS00743616002700628034*IM49999999998*CPC*VIS*TPAH*AL*DVMX*AS00743616002700142536*IM50000*CPC*VIN*TPCE*AL*DVMX*AS5520684849*IM0*CPC*VIN*TPCE*AL*DVMX*AS5520684850*IM0*CPC*VIN*TPTP*AL*DVMX*AS00743616002700846372*IM50000*CPC*VIN*TPCH*AL*DVMX*AS00743616002700894637*IM50000*CPC*VIN*TPTC*AL*DVMX*AS00743616002700374625*IM50000*CPC*VIN*TPTC*AL*DVMX*AS00743616002700374123*IM50000*CPC*VIN";
	public static String STATUS_NOT_A1 = "ESOK*TED0026805*FE19062013*HR171027*TO2*CT*OC0*VA*EI*STS4*LO1";
	public static String OPTIONAL_UPDATE = "ESAC*TED0027237*FE04072013*HR133027*TO2*CT*OC4*TPAH*AL*DVMX*AS00743616002900072907*IM4883862*CPC*VIS*TPCH*AL*DVMX*AS00743616000178463778*IM000*CPC*VIN*TPTC*AL*DVMX*AS4101810678996382*IM000*CPC*VIN*TPTC*AL*DVMX*AS4555456998072651*IM000*CPC*VIN";
	public static String MANDATORY_UPDATE = "ESERROR*COMBANK1111*MEBancomer móvil trae para tí la nueva opción de Dinero móvil, que te permitirá enviar dinero a quien tú quieras y cuando quieras, presiona Actualizar y comienza a utilizarla.*URhttps://www.bancomermovil.com/mbank/mbank/web/download.jsp?o=A";

	public static String CURRENT_ACCOUNT_SET = ACCOUNTS_ALL_ACCOUNTS;

	// Simulates diferent app status.
	// Simulate diferent fastpayment sets.
	public static String CODIGO_ESTATUS_APLICACION = "A1";
	public static String NO_FAST_PAYMENT = "*RP";
	// public static String NO_FAST_PAYMENT = "*RP{\"rapidas\":\"\"}";
	public static String FAST_PAYMENT_3 = "*RP{\"rapidas\":[{\"nombreCorto\":\"DOMO\",\"cuentaOrigen\":\"AH00741800000151951234\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"5000\",\"nombreBeneficiario\":\"\",\"companiaCelular\":\"Iusacell\",\"tipoRapido\":\"compraTiempoAire\",\"iDOperacion\":\"01\",\"concepto\":\"\"},{\"nombreCorto\":\"traspIvan\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"4152311034652888\",\"telefonoDestino\":\"\",\"importe\":\"15000\",\"nombreBeneficiario\":\"ARTURO ANTONIO ALVAREZ ALMAZAN\",\"companiaCelular\":\"\",\"tipoRapido\":\"otrosBancomer\",\"iDOperacion\":\"02\",\"concepto\":\"\"},{\"nombreCorto\":\"DineroM\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"9900000\",\"nombreBeneficiario\":\"Mario Mendez\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"dineroMovil\",\"iDOperacion\":\"03\",\"concepto\":\"pagarTelefono\"}]}";
	public static String FAST_PAYMENT_5 = "*RP{\"rapidas\":[{\"nombreCorto\":\"DOMO\",\"cuentaOrigen\":\"AH00741800000151951234\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"5000\",\"nombreBeneficiario\":\"\",\"companiaCelular\":\"Iusacell\",\"tipoRapido\":\"compraTiempoAire\",\"iDOperacion\":\"01\",\"concepto\":\"\"},{\"nombreCorto\":\"traspIvan\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"4152311034652888\",\"telefonoDestino\":\"\",\"importe\":\"15000\",\"nombreBeneficiario\":\"ARTURO ANTONIO ALVAREZ ALMAZAN\",\"companiaCelular\":\"\",\"tipoRapido\":\"otrosBancomer\",\"iDOperacion\":\"02\",\"concepto\":\"\"},{\"nombreCorto\":\"DineroM\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"9900000\",\"nombreBeneficiario\":\"Mario Mendez\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"dineroMovil\",\"iDOperacion\":\"03\",\"concepto\":\"pagarTelefono\"},{\"nombreCorto\":\"DOMO2\",\"cuentaOrigen\":\"AH00741800000151951234\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"5000\",\"nombreBeneficiario\":\"\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"compraTiempoAire\",\"iDOperacion\":\"01\",\"concepto\":\"\"},{\"nombreCorto\":\"traspIva2\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"4152311034652888\",\"telefonoDestino\":\"\",\"importe\":\"15000\",\"nombreBeneficiario\":\"ARTURO ANTONIO ALVAREZ ALMAZAN\",\"companiaCelular\":\"\",\"tipoRapido\":\"otrosBancomer\",\"iDOperacion\":\"02\",\"concepto\":\"\"}]}";
	public static String FAST_PAYMENT_6 = "*RP{\"rapidas\":[{\"nombreCorto\":\"DOMO\",\"cuentaOrigen\":\"AH00741800000151951234\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"5000\",\"nombreBeneficiario\":\"\",\"companiaCelular\":\"Iusacell\",\"tipoRapido\":\"compraTiempoAire\",\"iDOperacion\":\"01\",\"concepto\":\"\"},{\"nombreCorto\":\"traspIvan\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"4152311034652888\",\"telefonoDestino\":\"\",\"importe\":\"15000\",\"nombreBeneficiario\":\"ARTURO ANTONIO ALVAREZ ALMAZAN\",\"companiaCelular\":\"\",\"tipoRapido\":\"otrosBancomer\",\"iDOperacion\":\"02\",\"concepto\":\"\"},{\"nombreCorto\":\"DineroM\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"9900000\",\"nombreBeneficiario\":\"Mario Mendez\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"dineroMovil\",\"iDOperacion\":\"03\",\"concepto\":\"pagarTelefono\"},{\"nombreCorto\":\"DOMO2\",\"cuentaOrigen\":\"AH00741800000151951234\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"5000\",\"nombreBeneficiario\":\"\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"compraTiempoAire\",\"iDOperacion\":\"01\",\"concepto\":\"\"},{\"nombreCorto\":\"traspIva2\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"4152311034652888\",\"telefonoDestino\":\"\",\"importe\":\"15000\",\"nombreBeneficiario\":\"ARTURO ANTONIO ALVAREZ ALMAZAN\",\"companiaCelular\":\"\",\"tipoRapido\":\"otrosBancomer\",\"iDOperacion\":\"02\",\"concepto\":\"\"},{\"nombreCorto\":\"DineroM2\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"9900000\",\"nombreBeneficiario\":\"Mario Mendez\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"dineroMovil\",\"iDOperacion\":\"03\",\"concepto\":\"pagarTelefono\"}]}";

	public static String CURRENT_FAST_PAYMENT_SET = NO_FAST_PAYMENT;

	//-- OLD CATALOGS --
	//public static String OPERACIONES_CAT_AU_PREVIOUS = "{\"operaciones\":[{\"operacion\":\"contratacion\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"reactivacion\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"desbloqueo\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"login\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"transferirPropias\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"transferirBancomer\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"transferirBancomerR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"transferirInterbancaria\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"transferirInterbancariaR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"dineroMovil\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"pagoServicios\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosP\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"compraTiempoAire\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"compraTiempoAireR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"compraComercios\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"cancelarDineroMovil\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"altaFrecuente\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"altaRapido\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"bajaFrecuente\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"bajaRapido\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"cambioTelefono\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"cambioCuenta\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"cambioLimites\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"administrarAlertas\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"suspenderCancelar\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"cambioPerfil\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"configurarCorreo\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"configurarAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"quitarSuspension\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"contratacionAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"actualizacionAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}}],\"version\":\"7\"}";
	//public static String OPERACIONES_CAT_AU = "{\"operaciones\":[{\"operacion\":\"contratacion\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"reactivacion\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"desbloqueo\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"login\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"transferirPropias\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"transferirBancomer\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"transferirBancomerR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"transferirInterbancaria\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"transferirInterbancariaR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"dineroMovil\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"pagoServicios\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosP\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"compraTiempoAire\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"compraTiempoAireR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"compraComercios\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"cancelarDineroMovil\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"altaFrecuente\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"altaRapido\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"bajaFrecuente\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"bajaRapido\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"cambioTelefono\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"cambioCuenta\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"cambioLimites\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"administrarAlertas\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"suspenderCancelar\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"cambioPerfil\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"configurarCorreo\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"configurarAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"quitarSuspension\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"contratacionAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"actualizacionAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}}],\"version\":\"5\"}";
	//public static String OPERACIONES_ALL_TRUE = "{\"operaciones\":[{\"operacion\":\"contratacion\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"reactivacion\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"desbloqueo\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"login\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirPropias\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"dineroMovil\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"pagoServicios\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"compraComercios\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"altaRapido\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"bajaRapido\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"cambioLimites\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}}],\"version\":\"8\"}";
	
	//public static String OPERACIONES_CAT_AU = "{\"perfil\":[{\"recortado\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}}]},{\"basico\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}}]},{\"avanzado\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}}]}],\"version\":\"9\"}";
	
	public static String OPERACIONES_CAT_AU = "{\"perfil\":[{\"recortado\":[{\"operacion\":\"consultarCreditos\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"oneClickBmovilConsumo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"false\"}}]},{\"basico\":[{\"operacion\":\"consultarCreditos\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"oneClickBmovilConsumo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"false\"}}]},{\"avanzado\":[{\"operacion\":\"consultarCreditos\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"oneClickBmovilConsumo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}}]}],\"version\":\"12\"}";

	public static String LISTA_OPERACIONES = OPERACIONES_CAT_AU;

	public static String LISTA_TELEFONICAS = "[{\"companiaCelular\":\"TELCEL\",\"nombreImagen\":\"telcel.png\"},{\"companiaCelular\":\"MOVISTAR\",\"nombreImagen\":\"movistar.png\"},{\"companiaCelular\":\"IUSACELL\",\"nombreImagen\":\"iusacell.png\"},{\"companiaCelular\":\"UNEFON\",\"nombreImagen\":\"unefon.png\"}]";

	public static String CORREO_EJEMPLO = "ejemplo@gonet.us";

	// ///////////////////////////////////////////////////////////////////////////
	// Operations identifiers //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * Activation step 1 operation.
	 */
	public static final int ACTIVATION_STEP1_OPERATION = 1;

	/**
	 * Acivation step 2 operation.
	 */
	public static final int ACTIVATION_STEP2_OPERATION = 2;

	/**
	 * Login oepration.
	 */
	public static final int LOGIN_OPERATION = 3;

	/**
	 * Movements operation.
	 */
	public static final int MOVEMENTS_OPERATION = 4;

	/**
	 * Transfer money operation between the user's accounts.
	 */
	public static final int SELF_TRANSFER_OPERATION = 5;

	/**
	 * Transfer money operation to someone else's account in Bancomer.
	 */
	public static final int BANCOMER_TRANSFER_OPERATION = 6;

	/**
	 * External transfer operation.
	 */
	public static final int EXTERNAL_TRANSFER_OPERATION = 7;

	/**
	 * Nipper store purchase operation.
	 */
	public static final int NIPPER_STORE_PURCHASE_OPERATION = 8;

	/**
	 * Nipper airtime purchase operation.
	 */
	public static final int NIPPER_AIRTIME_PURCHASE_OPERATION = 9;

	/**
	 * Change password operation.
	 */
	public static final int CHANGE_PASSWORD_OPERATION = 10;

	/**
	 * Close session operation.
	 */
	public static final int CLOSE_SESSION_OPERATION = 11;

	/**
	 * Get the owner of a credit card.
	 */
	public static final int CARD_OWNER_OPERATION = 12;

	/**
	 * Calculate fee.
	 */
	public static final int CALCULATE_FEE_OPERATION = 13;

	/**
	 * Calculate fee 2.
	 */
	public static final int CALCULATE_FEE_OPERATION2 = 14;

	/**
	 * Service payment.
	 */
	public static final int SERVICE_PAYMENT_OPERATION = 15;

	/**
	 * Help image for service payment.
	 */
	public static final int HELP_IMAGE_OPERATION = 16;

	/**
	 * Request frequent service payment operation list.
	 */
	public static final int FAVORITE_PAYMENT_OPERATION = 18;

	/**
	 * Fast Payment service.
	 */
	public static final int FAST_PAYMENT_OPERATION = 19;

	/**
	 * Service enterprise name operation.
	 */
	public static final int SERVICE_NAME_OPERATION = 20;

	/**
	 * Alta de operaciones sin tarjeta
	 */
	public static final int ALTA_OPSINTARJETA = 23;

	/**
	 * Consulta de operaciones sin tarjeta
	 */
	public static final int CONSULTA_OPSINTARJETA = 24;

	/**
	 * Baja de operaciones sin tarjeta
	 */
	public static final int BAJA_OPSINTARJETA = 25;

	/**
	 * Consulta status del servicio
	 */
	public static final int CONSULTA_ESTATUSSERVICIO = 26;

	/**
	 * Cambia perfil del cliente
	 */
	public static final int CAMBIA_PERFIL = 27;

	/**
	 * Consulta comision para transferir interbancario
	 */
	public static final int CONSULTAR_COMISION_I = 28;

	/**
	 * Consulta comision para transferir interbancario
	 */
	public static final int CONSULTAR_CODIGO_PAGO_SERVICIOS = 29;

	/**
	 * Preregistro de pago de servicios
	 */
	public static final int PREREGISTRAR_PAGO_SERVICIOS = 30;

	/**
	 * Compra de tiempo aire
	 */
	public static final int COMPRA_TIEMPO_AIRE = 31;

	/**
	 * Alta de frecuente
	 */
	public static final int ALTA_FRECUENTE = 32;

	/**
	 * Consulta del beneficiario
	 */
	public static final int CONSULTA_BENEFICIARIO = 33;

	/**
	 * Baja de frecuente
	 */
	public static final int BAJA_FRECUENTE = 34; // OP144

	/**
	 * Baja de frecuente
	 */
	public static final int CONSULTA_CIE = 35;

	/**
	 * 
	 */
	public static final int ACTUALIZAR_FRECUENTE = 36;

	/**
	 * Actualizacion de preregistrado a Frecuente
	 */
	public static final int ACTUALIZAR_PREREGISTRO_FRECUENTE = 37;

	/**
	 * Cambio de telefono asociado
	 * 
	 */
	public static final int CAMBIO_TELEFONO = 38;

	/**
	 * Cambio de cuenta Asociada
	 */
	public static final int CAMBIO_CUENTA = 39;

	/**
	 * Suspencion Temporal
	 */
	public static final int SUSPENDER_CANCELAR = 40;

	/**
	 * Actualizacion de cuentas del usuario
	 */
	public static final int ACTUALIZAR_CUENTAS = 41;

	public static final int CONSULTA_TARJETA_OPERATION = 42;

	/**
	 * Consulta de limites de operacion
	 */
	public static final int CONSULTAR_LIMITES = 43;

	/**
	 * Cambio de limites de operacion
	 */
	public static final int CAMBIAR_LIMITES = 44;

	/**
	 * Consulta de estatus mantenimiento.
	 */
	public static final int CONSULTA_MANTENIMIENTO = 45;

	/**
	 * Desbloqueo de contraseñas
	 */
	public static final int DESBLOQUEO = 46;

	/**
	 * Quitar suspension
	 */
	public static final int QUITAR_SUSPENSION = 47;

	/**
	 * Reactivacion
	 */
	public static final int OP_ACTIVACION = 48;

	/**
	 * Operación de contratación final para bmovil.
	 */
	public static final int OP_CONTRATACION_BMOVIL_ALERTAS = 49;

	/**
	 * Operación de consulta de terminos y condiciones de uso.
	 */
	public static final int OP_CONSULTAR_TERMINOS = 50;

	public static final int OP_ENVIO_CLAVE_ACTIVACION = 51;

	public static final int OP_CONFIGURAR_CORREO = 52;

	public static final int OP_ENVIO_CORREO = 53;

	public static final int OP_VALIDAR_CREDENCIALES = 54;

	public static final int OP_FINALIZAR_CONTRATACION_ALERTAS = 55;

	/** Activacion softtoken - Consulta de tipo de solicitud. */
	public static final int CONSULTA_TARJETA_ST = 56;

	/** Activacion softtoken - Autenticacion */
	public static final int AUTENTICACION_ST = 57;

	/**
	 * Activacion softtoken
	 */
	public static final int EXPORTACION_SOFTTOKEN = 58;
	/**
	 * Activacion softtoken
	 */
	public static final int SINCRONIZACION_SOFTTOKEN = 59;

	/** Activacion softtoken - Contratacion enrolamiento. */
	public static final int CONTRATACION_ENROLAMIENTO_ST = 60;

	/** Activacion softtoken - Finalizar contratacion. */
	public static final int FINALIZAR_CONTRATACION_ST = 61;

	/** Activacion softtoken - Cambio telefono asociado. */
	public static final int CAMBIO_TELEFONO_ASOCIADO_ST = 62;

	/** Activacion softtoken - Solicitud. */
	public static final int SOLICITUD_ST = 63;

	/** Mantenimiento Alertas. */
	public static final int MANTENIMIENTO_ALERTAS = 64;
	
	/** Consulta de Terminos y Condiciones Sesion */
	
	public static final int OP_CONSULTAR_TERMINOS_SESION = 65;
	
	/** Solicitar Alertas */
	
	public static final int OP_SOLICITAR_ALERTAS = 66;
	
	//Empieza codigo de SPEI revisar donde se usan las constantes
		/* Identifier for the request spei accounts operation.*/
		public static final int SPEI_ACCOUNTS_REQUEST = 67;//66

		/**
		 * Identifier for the request spei terms and conditions.
		 */
		public static final int SPEI_TERMS_REQUEST = 68; //67
		
		/**
		 * The SPEI maintenance operation.
		 */
		public static final int SPEI_MAINTENANCE = 69;//68
		
		/**
		 * The request beneficiary account number operation.
		 */
		public static final int CONSULT_BENEFICIARY_ACCOUNT_NUMBER = 70;//69
	//Termina Codigo de SPEI	
		/** Consulta Interbancarios */
		
		

		
		//One CLick
		/**
		 * Operation codes.
		 */
		/** consulta detalle ofertas ilc*/
		public static final int CONSULTA_DETALLE_OFERTA=71;//67;//66
		public static final int ACEPTACION_OFERTA=72;//68;//67
		public static final int EXITO_OFERTA=73;//69;//68
		public static final int RECHAZO_OFERTA=74;//70;//69
		/**consulta detalle ofertas EFI*/
		public static final int CONSULTA_DETALLE_OFERTA_EFI=75;//71;//70
		public static final int ACEPTACION_OFERTA_EFI=76;//71
		public static final int EXITO_OFERTA_EFI=77;//72
		public static final int SIMULADOR_EFI=78;//73
		/** consulta detalle consumo*/
		public static final int CONSULTA_DETALLE_OFERTA_CONSUMO=79;
		public static final int POLIZA_OFERTA_CONSUMO=80;
		public static final int TERMINOS_OFERTA_CONSUMO=81;
		public static final int EXITO_OFERTA_CONSUMO=82;
		public static final int DOMICILIACION_OFERTA_CONSUMO=83;
		public static final int CONTRATO_OFERTA_CONSUMO=84;
		public static final int RECHAZO_OFERTA_CONSUMO=85;
		public static final int SMS_OFERTA_CONSUMO=86;
		public static final int OP_CONSULTA_INTERBANCARIOS = 87;
		
		/**
		 * Request frequent service payment operation list for BBVA accounts.
		 */
		public static final int FAVORITE_PAYMENT_OPERATION_BBVA = 88;
		
		//Depositos Movil
		public static final int CONSULTA_DEPOSITOS_CHEQUES = 89;//87
		public static final int CONSULTA_DEPOSITOS_EFECTIVO = 90; // Ya no se usa 88
		public static final int CONSULTA_PAGO_SERVICIOS = 91;//89
		public static final int CONSULTA_TRANSFERENCIAS_CUENTA_BBVA = 92;//90
		public static final int CONSULTA_TRANSFERENCIAS_MIS_CUENTAS = 93;//91
		public static final int CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS = 94;//92
		public static final int CONSULTA_TRANSFERENCIAS_CLIENTES_BANCOMER = 95; // Ya no se usa 93
		public static final int CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS = 96;//94
		public static final int CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO = 97;//95
		public static final int CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE = 98;//96
		public static final int CONSULTA_DEPOSITOS_CHEQUES_DETALLE = 99;//97

		/** consulta importes tarjeta de crédito **/
		public static final int OP_CONSULTA_TDC = 100;

		public static final int OP_CONSULTA_OTROS_CREDITOS = 101;

		public static final int OP_CONSULTA_DETALLE_OTROS_CREDITOS = 102;
		
		public static final int OP_CONSULTA_SMS_OTROS_CREDITOS = 103;

		public static final int OP_CONSULTA_CORREO_OTROS_CREDITOS = 104;
		
		public static final int OP_CONSULTA_CUENTAS_ALERTAS = 114;
		
		public static final int OP_DETALLE_CUENTA_ALERTAS = 115;
		
		public static final int OP_ALTA_MOD_SERVICIO_ALERTAS = 116;
		
		public static final int OP_BAJA_SERVICIO_ALERTAS = 117;

		public static final int OP_MENSAJES_ENVIADOS_ALERTAS = 118;

		public static final int OP_DETALLE_MENSAJE_ENV_ALERTAS = 119;
		
		//para simular errores de TDC
		public Boolean errorComunicaciones = false;
		public Boolean errorContrato = false;

	public static Integer controlCiclos = 0;



	/**
	 * Operation URLs map.
	 */
	public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

	/**
	 * Operation providers map.
	 */
	public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_VALUE = "BAN2O01";
	
	/**
	 * Operation code parameter for RECORTADO.
	 */
	public static final String OPERATION_CODE_VALUE_RECORTADO = "BREC001";

	/**
	 * Operation code parameter for BMOVIL MEJORADO.
	 */
	public static final String OPERATION_CODE_VALUE_BMOVIL_MEJORADO = "MJRS001";
	
	/**
	 * Operation code parameter for DEPOSITOS
	 */
	public static final String JSON_OPERATION_CODE_VALUE_DEPOSITOS = "O300001";

	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE = "BAN2O05";
	

	//one CLick
		/**
		 * Operation code parameter for new ops.
		 */
		public static final String JSON_OPERATION_CODE_VALUE_ONECLICK = "ONEC001";
		/**
		 * 
		 */
		
		//one CLick
		/**
		 * Operation code parameter for new ops.
		 */
		public static final String JSON_OPERATION_CODE_VALUE_CONSUMO = "ONEC002";
		/**
		 * 
		 */
	
	/**
	 * Operation code parameter for new ops.
	 */
//	public static final String JSON_OPERATION_SOLICITAR_ALERTAS_CODE_VALUE = "BREC001";
	public static final String JSON_OPERATION_RECORTADO_CODE_VALUE = "BREC001";

	/**
	 * Operation code for mejoras
	 */
	public static final String JSON_OPERATION_MEJORAS_CODE_VALUE = "MEJO001";
	
	/**
	 * Operation code for consultar otros creditos
	 */
	public static final String JSON_OPERATION_CONSULTAR_OTROS_CREDITOS = "CHIP001";

	/**
	 * Operation code for Alertas Digitales
	 */
	public static final String JSON_OPERATION_ALERTAS_DIGITALES = "ADGO001";
	
	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";

	/**
	 * Determines if operation is fast or frequent payment.
	 */
	public static final String FASTORFRECUENTOPERATION = "FastOrFrequentOperation";

	/**
	 * Charge account text.
	 */
	public static final String CUENTA_CARGO = "cuenta cargo";



	public static void setAllowLog(){
		CommonSession cs = CommonSession.getInstance();
		ALLOW_LOG = cs.getCommonSessionService().getAllowLog();
	}

	// ///////////////////////////////////////////////////////////////////////////
	// Parameter definition for application interface //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * The username.
	 */
	public static final String USERNAME_PARAM = "username";

	/**
	 * The password.
	 */
	public static final String PASSWORD_PARAM = "password";
	
	/**
	 * The IUM. SPEI
	 */
	public static final String IUM_PARAM = "ium";

	/**
	 * The version of catalog 1.
	 */
	public static final String VERSION_C1_PARAM = "version_c1";

	/**
	 * The version of catalog 4.
	 */
	public static final String VERSION_C4_PARAM = "version_c4";

	/**
	 * The version of catalog 5.
	 */
	public static final String VERSION_C5_PARAM = "version_c5";

	/**
	 * The version of catalog 8.
	 */
	public static final String VERSION_C8_PARAM = "version_c8";

	/**
	 * The version of catalog Tiempo aire.
	 */
	public static final String VERSION_TA_PARAM = "version_ta";

	/**
	 * The version of catalog Dinero movil.
	 */
	public static final String VERSION_DM_PARAM = "version_dm";
	
	
	/**
	 * The version of catalog Servicios.
	 */
	public static final String VERSION_SV_PARAM = "version_sv";

	public static final String VERSION_AU_PARAM = "version_au";
	//SPEI revisar
		/* The params for the SPEI catalog version.
		 */
		public static final String VERSION_MS_PARAM = "version_ms";
		//Termina SPEI

	/**
	 * Vía 1 o 2.
	 */
	public static final String VIA_PARAM = "via";
	/**
	 * The bancomer token.
	 */
	public static final String BANCOMER_TOKEN_PARAM = "sello_bancomer";

	/**
	 * The account.
	 */
	public static final String ACCOUNT_PARAM = "account";

	/**
	 * Origin account.
	 */
	public static final String ORIGIN_PARAM = "origin";

	/**
	 * Destination account.
	 */
	public static final String DESTINATION_PARAM = "destination";

	/**
	 * Money to transfer.
	 */
	public static final String AMOUNT_PARAM = "amount";

	/**
	 * Destination card number.
	 */
	public static final String CARDNUMBER_PARAM = "cardowner";

	/**
	 * Store.
	 */
	public static final String STORE_PARAM = "store";

	/**
	 * Cellular operator.
	 */
	public static final String OPERATOR_PARAM = "operator";
	
	//SPEI revisar

		/**
		 * Telephone number.
		 */
		public static final String PHONENUMBER_PARAM = "phonenumber";

		/**
		 * Old telephone number.
		 */
		public static final String OLD_PHONENUMBER_PARAM = "oldphonenumber";
	    //Termina SPEI

	/**
	 * Old password.
	 */
	public static final String OLD_PASSWORD_PARAM = "oldpassword";

	/**
	 * New password.
	 */
	public static final String NEW_PASSWORD_PARAM = "newpassword";

	/**
	 * Activation code.
	 */
	public static final String ACTIVATION_CODE_PARAM = "activation";

	/**
	 * User identifier.
	 */
	public static final String USER_ID_PARAM = "userid";

	/**
	 * Account type.
	 */
	public static final String ACCOUNT_TYPE_PARAM = "account_type";

	/**
	 * User identifier.
	 */
	public static final String BANK_PARAM = "userid2";

	/**
	 * Transfer receiver name identifier.
	 */
	public static final String RECEIVER_PARAM = "receiver";

	/**
	 * Transfer reference identifier.
	 */
	public static final String REFERENCE_PARAM = "reference";

	/**
	 * Transfer reason identifier.
	 */
	public static final String REASON_PARAM = "reason";

	/**
	 * Card type identifier.
	 */
	public static final String CARD_TYPE_PARAM = "card_type";

	/**
	 * Product type identifier.
	 */
	public static final String PRODUCT_TYPE_PARAM = "product_type";

	/**
	 * screen size identifier.
	 */
	public static final String SCREEN_SIZE_PARAM = "screen_size";

	/**
	 * Type of help image identifier.
	 */
	public static final String HELP_IMAGE_TYPE_PARAM = "help_image_type";

	/**
	 * Service provider identifier.
	 */
	public static final String SERVICE_PROVIDER_PARAM = "service_provider";

	/**
	 * CIE Agreement identifier.
	 */
	public static final String CIE_AGREEMENT_PARAM = "cie_agreement";

	/**
	 * Operation type.
	 */
	public static final String TYPE_OPER = "type_oper";

	/**
	 * Nick name.
	 */
	public static final String NICK_NAME_PARAM = "nickname";

	/**
	 * AP.
	 */
	public static final String IDNUMBER = "idNumber";

	/**
	 * CA.
	 */
	public static final String CUENTA_ABONO = "cuentaAbono";

	/**
	 * Payment operation code.
	 */
	public static final String PAYMENT_OPER_PARAM = "payment_oper";

	/**
	 * Payment ID.
	 */
	public static final String PAYMENT_ID_PARAM = "payment_id";

	/**
	 * Marca y modelo que se envía en Login, se concatenan las cadenas.
	 */
	public static final String MARCA_MODELO = "Marca Modelo";

	/**
	 * Plataforma en la que corre el midlet, en este caso es Android.
	 */
	public static final String PLATAFORMA = "Plataforma";

	/**
	 * Estatus de la operación sin tarjeta para efectivo m�vil, se utiliza en la
	 * consulta de Efectivo m�vil. Los estatus pueden ser VG=vigente,
	 * CN=Cancelada, CD=Expirada
	 */
	public static final String ESTATUS_OPSINTARJETA_PARAM = "estatus";

	/**
	 * Código de la operación de efectivo m�vil.
	 */
	public static final String FOLIO_OPERACION_PARAM = "FolioOperacion";

	/**
	 * Código del canal de efectivo m�vil.
	 */
	public static final String CODIGO_CANAL_PARAM = "CodigoCanal";

	/**
	 * Fecha de alta de la operación, efectivo m�vil.
	 */
	public static final String FECHA_OPERACION = "FechaAlta";

	public static final String NUMERO_CLIENTE_PARAM = "NumeroCliente";

	public static final String BANK_CODE_PARAM = "CodigoBanco";

	public static final String BENEFICIARIO_PARAM = "Beneficiario";

	public static final String MOTIVO_PAGO_PARAM = "MotivoPago";
	
	//SPEI Revisar
		public static final String OLD_COMPANY_NAME_PARAM = "companiaVieja";
		
		public static final String NEW_COMPANY_NAME_PARAM = "companiaNueva";
		//Termina SPEI

	public static final String REFERENCIA_NUMERICA_PARAM = "ReferenciaNumeria";
	public static final String NIP_PARAM = "Nip";
	public static final String CVV2_PARAM = "cvv2";
	public static final String VA_PARAM = "InstruccionValidacion";
	public static final String PAYMENT_CODE_PARAM = "QR";
	public static final String MARCA = "marca";
	public static final String MODELO = "modelo";

	public static final String DESCRIPCION_PARAM = "descripcion";
	public static final String CONCEPTO_PARAM = "concepto";
	public static final String TIPO_CONSULTA_PARAM = "tipoconsulta";
	public static final String OPERADORA_PARAM = "operadora";
	public static final String ID_TOKEN = "idToken";
	public static final String TIPO_COMISION_PARAM = "tipocomision";

	public static final String C1_PARAM = "C1";
	public static final String CN_PARAM = "CN";
	public static final String DE_PARAM = "DE";
	public static final String C2_PARAM = "C2";
	public static final String C3_PARAM = "C3";

	public static final String AUTH_CAT_PARAM = "authenticationVersion";
	public static final String COMPANIES_PARAM = "companiesVersion";

	public static final String ALERTSTATUS_PARAM = "estadoAlertas";

	/**
	 * Tags JSON
	 */
	public static final String J_NIP = "codigoNIP";
	public static final String J_CVV2 = "codigoCVV2";
	public static final String J_AUT = "cadenaAutenticacion";
	public static final String J_CUENTA_N = "cuentaNueva";
	public static final String J_CUENTA_A = "cuentaAnterior";
	public static final String J_CUENTA = "cuenta";
	
	//SPEI
	public static final String J_NUM_TELEFONO = "numeroTelefono";	
	public static final String J_NUM_CLIENTE = "numeroCliente";
	public static final String J_CVE_ACCESO = "cveAcceso";
	public static final String J_OTP = "codigoOTP";

	public static final String VOUCHER_TYPE_PARAM = "voucherType";
	public static final String SUBJECT_PARAM = "subject";
	public static final String CLIENTNAME_PARAM = "clientName";
	public static final String CELLPHONENUMBER_PARAM = "cellphoneNumber";
	public static final String OPERATION_DATE_PARAM = "operationDate";
	public static final String OPERATION_HOUR_PARAM = "operationHour";
	public static final String SECURITY_CODE_PARAM = "securityCode";
	public static final String REGISTER_DATE_PARAM = "registerDate";
	public static final String VALIDITY_DATE_PARAM = "validityDate";
	public static final String AGREEMENT_PARAM = "agreement";
	public static final String SERVICE_PARAM = "service";
	public static final String MSG_PARAM = "mensajeA";
	
	//SPEI revisar
		/**
		 * Last digists of the card for authentication prupose.
		 */
		public static final String LAST_CARD_DIGITS_PARAM = "tarjeta5Dig";
		/**
		 * The param for the maintenance type.
		 */
		public static final String SPEI_MAINTENANCE_TYPE_PARAM = "tipoMantenimientoSpei";
		
		//termina SPEI



	/**
	 * simulation option for the login.
	 */
	private static boolean simulateDeactivation = false;
	
	public static String JSON_OPERATION_CONSULTA_INTERBANCARIOS="BXCO001";




	/**
	 * Default constructor.
	 */
	public Server() {

	}

}