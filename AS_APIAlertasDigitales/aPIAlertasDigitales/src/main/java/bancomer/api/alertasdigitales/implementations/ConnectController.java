package bancomer.api.alertasdigitales.implementations;

import android.content.Context;

import com.bancomer.base.SuiteApp;

import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;

/**
 * Created by omar on 15/01/17.
 */
public class ConnectController {

    private static ConnectController INSTANCE= null;

    /*  -------- Attributes Region  ---------- */

    private Context context;
    private String seed;
    private String centro;
    private String userTelephoneNumber;
    private boolean hasKeyChainData;


    private static final String CENTRO_TAG="centro";

    /*  -------- Getters & Setters Region  ---------- */

    public void setContext(Context context) {
        this.context = context;
    }


    public static ConnectController getINSTANCE()
    {
        if(INSTANCE == null)
            INSTANCE = new ConnectController();
        return  INSTANCE;
    }



    public void setEnviroment()
    {
        SuiteApp.appContext = context;
        ValidacionEstatusST validacionEstatusST = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST();
        validacionEstatusST.validaEstatusSofttoken(context);
        checkKeyChain();
    }

    public boolean isSTActivated()
    {
        SuiteApp suiteApp = new SuiteApp();
        suiteApp.onCreate(context);

        suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST validacionEstatusST = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST();
        validacionEstatusST.validaEstatusSofttoken(context);

        return (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent()
                .getSofttokenService());
    }


    private void checkKeyChain()
    {
        KeyStoreWrapper kswrapper;

        try
        {

            kswrapper = KeyStoreWrapper.getInstance(context);
            userTelephoneNumber = kswrapper.getUserName();
            seed = kswrapper.getSeed();
            centro = kswrapper.fetchValueForKey(CENTRO_TAG);

            if(!seed.trim().isEmpty() && !seed.equals("0"))
            {
                if(!userTelephoneNumber.trim().isEmpty()) {
                    hasKeyChainData = true;
                    return;
                }
            }

            hasKeyChainData = false;

        }
        catch(NullPointerException nullException)
        {
            hasKeyChainData = false;
        }
        catch(Exception exception)
        {
            hasKeyChainData = false;
        }

    }
}
