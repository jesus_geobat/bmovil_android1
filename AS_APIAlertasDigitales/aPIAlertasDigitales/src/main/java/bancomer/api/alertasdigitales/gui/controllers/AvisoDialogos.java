package bancomer.api.alertasdigitales.gui.controllers;

/**
 * Created by leleyvah on 28/03/2016.
 */
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import bancomer.api.alertasdigitales.R;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;

/**
 * Created by leleyvah on 16/03/2016.
 */
public class AvisoDialogos extends DialogFragment {
    private Button aceptar;
    private String alerta;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        alerta=getActivity().getIntent().getStringExtra("alerta");
        View v = inflater.inflate(R.layout.api_alertas_digitales_ingresar_aviso, null);
        builder.setView(v);

        aceptar = (Button) v.findViewById(R.id.aceptar_aviso);
        aceptar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                }
        );
        return builder.create();
    }
}
