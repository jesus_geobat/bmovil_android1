package bancomer.api.alertasdigitales.gui.controllers;

/**
 * Created by leleyvah on 16/03/2016.
 */

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;

import bancomer.api.alertasdigitales.R;
import bancomer.api.alertasdigitales.gui.delegates.AlertasDigitalesDelegate;

public class ScreenSlidePageFragment extends Fragment {

    private static final String INDEX = "index";
    private static final String COUNT = "count";
    private static final String CUENTA_CON_ALERTAS="cuenta_con_alertas";
    private static final String POSICION = "posicion";

    private String Tipo_cuenta;
    private String Numero_cuenta;
    private String Alertas;
    private static String Numero_telefono_celular = "";
    private int posicionLayout;

    private String index;
    private String count;
    private String cuenta_con_alertas;
    private int posicion;
    private static AlertasDigitalesDelegate alertasdelegate;

    private RelativeLayout div_sup_uno;
    private RelativeLayout div_sup_dos;
    private RelativeLayout div_inf_uno;
    private RelativeLayout div_inf_dos;
    private RelativeLayout div_inf_tres;
    private TextView alias_uno;
    private TextView alias_dos;
    private TextView impor_abono;
    private TextView impor_cargo;
    private TextView impor_cargo_secundario;
    private ImageView compania_telefonica;

    private int imagen_compania;

    public static ScreenSlidePageFragment newInstance(String index, String count, String cuenta_con_alertas, AlertasDigitalesDelegate alerta, int pos) {

        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(INDEX, index);
        bundle.putString(COUNT, count);
        bundle.putString(CUENTA_CON_ALERTAS,cuenta_con_alertas);
        bundle.putInt(POSICION, pos);
        fragment.setArguments(bundle);
        fragment.setRetainInstance(true);
        alertasdelegate = alerta;
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.index = (getArguments() != null) ? getArguments().getString(INDEX) : null;
        this.count = (getArguments() != null) ? getArguments().getString(COUNT) : null;
        this.cuenta_con_alertas = (getArguments() != null) ? getArguments().getString(CUENTA_CON_ALERTAS) : null;
        this.posicion = (getArguments() != null) ? getArguments().getInt(POSICION) : null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    TextView numero_telefono;
    ImageButton editar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_screen_slide_page, container, false);
        /*=======================================================*/
        final TextView nombre_cuenta = (TextView) rootView.findViewById(R.id.nombre_cuenta);
        TextView numero_cuenta = (TextView) rootView.findViewById(R.id.numero_cuenta);
        numero_telefono = (TextView) rootView.findViewById(R.id.num_telefono);
        editar = (ImageButton) rootView.findViewById(R.id.editar_informacion);
        div_sup_dos = (RelativeLayout) rootView.findViewById(R.id.divisor_superior_dos);
        div_sup_uno = (RelativeLayout) rootView.findViewById(R.id.divisor_superior_uno);
        div_inf_uno = (RelativeLayout) rootView.findViewById(R.id.divisor_inferior_uno);
        div_inf_dos = (RelativeLayout) rootView.findViewById(R.id.divisor_inferior_dos);
        div_inf_tres = (RelativeLayout) rootView.findViewById(R.id.divisor_inferior_tres);
        alias_uno = (TextView) rootView.findViewById(R.id.pagos_escondidos);
        alias_dos = (TextView) rootView.findViewById(R.id.alias);
        impor_abono = (TextView) rootView.findViewById(R.id.textView16);
        impor_cargo = (TextView) rootView.findViewById(R.id.textView15);
        impor_cargo_secundario = (TextView) rootView.findViewById(R.id.cantidad);
        compania_telefonica = (ImageView) rootView.findViewById(R.id.companiatelefonica);

        Tipo_cuenta = String.valueOf(this.index);
        Numero_cuenta = String.valueOf(this.count);
        posicionLayout = Integer.valueOf(this.posicion);

        nombre_cuenta.setText(Tipo_cuenta);
        numero_cuenta.setText(Numero_cuenta);
        editar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertasdelegate.banderaMostrarDetalle = true;
                alertasdelegate.performAction(posicionLayout);
            }
        });
        /*=======================================================*/
        initView();
        return rootView;
    }

    public void initView() {

        if ("N".equals(this.cuenta_con_alertas)){
                div_inf_tres.setVisibility(View.VISIBLE);
                div_sup_uno.setVisibility(View.VISIBLE);
                div_sup_dos.setVisibility(View.GONE);
                div_inf_uno.setVisibility(View.GONE);
                div_inf_dos.setVisibility(View.GONE);
                editar.setVisibility(View.GONE);
        }else{
            if (alertasdelegate.getDetalleCuentaAlertasData(this.posicion) != null) {
                numero_telefono.setText(alertasdelegate.getDetalleCuentaAlertasData(this.posicion).getNumeroCelular());
                String ImpAbonoCap = alertasdelegate.getDetalleCuentaAlertasData(this.posicion).getImporteAbono();
                String Alias = alertasdelegate.getDetalleCuentaAlertasData(this.posicion).getAlias();
                String Importe_cargo = alertasdelegate.getDetalleCuentaAlertasData(this.posicion).getImporteCargo();
                String Compania_celular = alertasdelegate.getDetalleCuentaAlertasData(this.posicion).getTelefonicaDescr();
                if (Compania_celular.equals("TELCEL")) {
                    imagen_compania = R.drawable.telcel_blanco;
                }
                if (Compania_celular.equals("MOVISTAR")) {
                    imagen_compania = R.drawable.movistar_blanco;
                }
                if (Compania_celular.equals("NEXTEL")) {
                    imagen_compania = R.drawable.nextel_blanco;
                }
                if (Compania_celular.equals("UNEFON")) {
                    imagen_compania = R.drawable.unefon_blanco;
                }
                if (Compania_celular.equals("IUSACELL")) {
                    imagen_compania = R.drawable.iusacell_blanco;
                }
                compania_telefonica.setBackgroundResource(imagen_compania);

                if (!ImpAbonoCap.equals("")) {
                    div_sup_dos.setVisibility(View.VISIBLE);
                    div_inf_dos.setVisibility(View.VISIBLE);
                    div_sup_uno.setVisibility(View.GONE);
                    div_inf_uno.setVisibility(View.GONE);
                    div_inf_tres.setVisibility(View.GONE);
                    alias_uno.setText(Alias);
                    impor_abono.setText(formatoMonedaDeposito(alertasdelegate.getDetalleCuentaAlertasData(this.posicion).getImporteAbono()));
                    impor_cargo.setText(formatoMonedaDeposito(alertasdelegate.getDetalleCuentaAlertasData(this.posicion).getImporteCargo()));
                } else {
                    div_sup_dos.setVisibility(View.VISIBLE);
                    div_inf_uno.setVisibility(View.VISIBLE);
                    div_sup_uno.setVisibility(View.GONE);
                    div_inf_tres.setVisibility(View.GONE);
                    div_inf_dos.setVisibility(View.GONE);
                    impor_cargo_secundario.setText(formatoMonedaDeposito(Importe_cargo));
                    alias_dos.setText(Alias);
                }
            }
        }
    }

    public static String formatoMonedaDeposito(final String monto) {
        final DecimalFormat formato = new DecimalFormat("#,###.00");
        if (!monto.toString().matches("^\\$(\\d{1,3}(\\,\\d{3})*|(\\d+))(\\.\\d{2})?$")) {
            final String userInput = "" + monto.replaceAll("[^\\d]", "");
            final Float in = Float.parseFloat(monto);
            final float percen = in / 100;
            return ("$" + formato.format(percen));
        }
        return "00";
    }
}