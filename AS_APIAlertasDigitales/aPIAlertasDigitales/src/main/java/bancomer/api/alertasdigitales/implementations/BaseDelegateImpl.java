package bancomer.api.alertasdigitales.implementations;

import java.util.ArrayList;
import java.util.Hashtable;

import android.app.Activity;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.gui.delegates.BaseOperacionDelegate;
import bancomer.api.common.io.ServerResponse;

public class BaseDelegateImpl implements BaseDelegate, BaseOperacionDelegate{

	@Override
	public String getTextoTituloResultado() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTextoAyudaNIP() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTextoAyudaCVV() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void doNetworkOperation(int operationId,
			Hashtable<String, ?> params, BaseViewController caller) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void performAction(Object obj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public long getDelegateIdentifier() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean mostrarContrasenia() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean mostrarNIP() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mostrarCVV() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean mostrarCampoTarjeta(){
		return false;
	}

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void realizaOperacionConfirmacion(
			Activity confirmacionViewController, String contrasena, String nip,
			String asm, String tarjeta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getTituloConfirmacion() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getIconoConfirmacion() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String getNumeroCuentaParaRegistroOperacion() {
		return "00000000";
	}
	
}
