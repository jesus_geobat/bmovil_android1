package com.bancomer.mbanking.administracion;

import android.content.Context;
import android.view.View;

import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.base.callback.CallBackSession;

import suitebancomer.classes.gui.controllers.administracion.MenuSuiteViewController;
import suitebancomer.classes.gui.controllers.administracion.SuiteViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

public class SuiteAppAdmonApi extends com.bancomer.base.SuiteApp{
	public static boolean isSubAppRunning;
	public static Context appContext;
	private static SuiteAppAdmonApi me= new SuiteAppAdmonApi();
	private SuiteViewsController suiteViewsController;
	private static BaseViewControllerCommons intentMenuAdministrar;
	private static BaseViewControllerCommons intentMenuPrincipal;
	private static BaseViewControllerCommons intentConfirmacionAut;
	private static BaseViewControllerCommons intentMenuSuit;
	private static BaseViewControllerCommons cambioPerfil;

	private static CallBackSession callBackSession;

	private static CallBackBConnect callBackBConnect;

	public static BaseViewControllerCommons getIntentMenuAdministrar() {
		return intentMenuAdministrar;
	}

	public static void setIntentMenuAdministrar(
			final BaseViewControllerCommons intentMenuAdministrar) {
		SuiteAppAdmonApi.intentMenuAdministrar = intentMenuAdministrar;
	}

	public static BaseViewControllerCommons getIntentMenuPrincipal() {
		return intentMenuPrincipal;
	}

	public static void setIntentMenuPrincipal(
			final BaseViewControllerCommons intentMenuPrincipal) {
		SuiteAppAdmonApi.intentMenuPrincipal = intentMenuPrincipal;
	}
	
	public static BaseViewControllerCommons getIntentConfirmacionAut() {
		return intentConfirmacionAut;
	}

	public static void setIntentConfirmacionAut(
			final BaseViewControllerCommons intentConfirmacionAut) {
		SuiteAppAdmonApi.intentConfirmacionAut = intentConfirmacionAut;
	}

	public static void setIntentMenuSuit(final BaseViewControllerCommons intentMenuSuit){
		SuiteAppAdmonApi.intentMenuSuit=intentMenuSuit;	
	}
	public static BaseViewControllerCommons getIntentMenuSuit(){
		return SuiteAppAdmonApi.intentMenuSuit;	
	}
	
	public static BaseViewControllerCommons getCambioPerfil() {
		return cambioPerfil;
	}

	public static void setCambioPerfil(BaseViewControllerCommons cambioPerfil) {
		SuiteAppAdmonApi.cambioPerfil = cambioPerfil;
	}

	public void onCreate(final Context context) {
		super.onCreate(context);
		suiteViewsController = new SuiteViewsController();
		isSubAppRunning = false;
		
		appContext = context;
		me = this;
		
		startBmovilApp();
	};
	
	public void cierraAplicacionSuite() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public SuiteViewsController getSuiteViewsController() {
		return suiteViewsController;
	}

	public static SuiteAppAdmonApi getInstance() {
		return me;
	}
	
	// #region BmovilApp
	private BmovilApp bmovilApplication;
	
	public BmovilApp getBmovilApplication() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		return bmovilApplication;
	}
	
	public static boolean getBmovilStatus() {
		return PropertiesManager.getCurrent().getBmovilActivated();
	}
	
	public void startBmovilApp() {
		bmovilApplication = new BmovilApp(this);
		isSubAppRunning = true;
	}
	
	public void cierraAplicacionBmovil() {
		bmovilApplication.cierraAplicacion();
		bmovilApplication = null;
		isSubAppRunning = false;		
		//reiniciaAplicacionBmovil();
	}
	
	public void reiniciaAplicacionBmovil() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		bmovilApplication.reiniciaAplicacion();
		isSubAppRunning = true;
	}
	// #endregion
	
	// #region SofttokenApp
//	private SofttokenApp softtokenApp;
	
//	public SofttokenApp getSofttokenApplication() {
//		if(softtokenApp == null)
//			startSofttokenApp();
//		return softtokenApp;
//	}
	
	public static boolean getSofttokenStatus() {
		return PropertiesManager.getCurrent().getSofttokenActivated();
	}
	
//	public void startSofttokenApp() {
//		softtokenApp = new SofttokenApp(this);
//		isSubAppRunning = true;
//	}
	
//	public void closeSofttokenApp() {
//		if(null == softtokenApp)
//			return;
//		softtokenApp.cierraAplicacion();
//		softtokenApp = null;
//		isSubAppRunning = false;
//		
//		if(suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController)
//			((MenuSuiteViewController)suiteViewsController.getCurrentViewControllerApp()).setShouldHideLogin(true);
//		
//		suiteViewsController.showMenuSuite(true);
//	}
	// #endregion
	
	
	public void closeBmovilAppSession(){
		
		if(suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController)
			((MenuSuiteViewController)suiteViewsController.getCurrentViewControllerApp()).setShouldHideLogin(true);
		
		suiteViewsController.showMenuSuite(true);
		
		
		SuiteAppAdmonApi suiteApp = SuiteAppAdmonApi.getInstance();
		Session session = Session.getInstance(suiteApp.getApplicationContext());

		if(session.getValidity() == Session.VALID_STATUS){
			session.setValidity(Session.INVALID_STATUS);
		}
		
		bmovilApplication.closeBmovilAppSession(suiteViewsController.getCurrentViewControllerApp());
		
	}

	public static int getResourceId(final String nombre,final String tipo){
		return appContext.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}
	
	public static int getResourceId(final String nombre,final String tipo, final View vista){
		return vista.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}

	public void addDelegateToHashMap(final long id, final BaseDelegateCommons delegateCommons){
		suiteViewsController.addDelegateToHashMap(id,delegateCommons);
	}
	/*public void showViewController(Class<?> viewController, int flags, boolean inverted, String[] extrasKeys, Object[] extras,Activity currentActivity) {
		//getBmovilApplicationApi().getBmovilViewsController().showViewController( viewController,  flags,  inverted,  extrasKeys,  extras);
        suiteViewsController.setActivityContext(currentActivity);
		suiteViewsController.showViewController(viewController, flags, inverted, extrasKeys, extras);
	}*/

	public void showViewController(final Class<?> viewController, final int flags, final boolean inverted, final String[] extrasKeys, final Object[] extras) {
		//getBmovilApplicationApi().getBmovilViewsController().showViewController( viewController,  flags,  inverted,  extrasKeys,  extras);
		suiteViewsController.showViewController(viewController, flags, inverted, extrasKeys, extras);
	}

	public static CallBackSession getCallBackSession() {
		return callBackSession;
	}

	public static void setCallBackSession(final CallBackSession callBackSession) {
		SuiteAppAdmonApi.callBackSession = callBackSession;
	}

	public static CallBackBConnect getCallBackBConnect() {
		return callBackBConnect;
	}

	public static void setCallBackBConnect(final CallBackBConnect callBackBConnect) {
		SuiteAppAdmonApi.callBackBConnect = callBackBConnect;
	}

}
