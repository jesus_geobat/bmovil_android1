package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import suitebancomer.aplicaciones.bmovil.classes.common.administracion.AbstractContactMannager;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MantenimientoSpeiMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.administracion.SeleccionHorizontalViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.MantenimientoSpei.TiposOperacionSpei;

public class DetalleCuentaSpeiMovilViewController extends BaseViewController {
	//#region Class fileds.
	/**
	 * Activity request code for load a contact from the phone.
	 */
	public static final int CONTACTS_REQUEST_CODE = 1;
	
	/**
	 * The delegate for this screen.
	 */
	private MantenimientoSpeiMovilDelegate ownDelegate; 
	
	/**
	 * The origin account component.
	 */
	private CuentaOrigenViewController originAccount;
	
	/**
	 * The ListaSeleccion component to show the accounts list.
	 */
	private SeleccionHorizontalViewController companiesList;
	
	/**
	 * The text field to indicate the new phone number to associate.
	 */
	private EditText phoneNumberEdittext;
	
	/**
	 * The text field to indicate the new phone number to associate.
	 */
	private EditText confirmPhoneNumberEdittext;
	
	/**
	 * The linear layout for hold the accounts list.
	 */
	private LinearLayout companiesListLayout;
	
	/**
	 * Flag to indicate if the activity is loading contacts.
	 */
	private boolean loadingContact;
	//#endregion

	//AMZ
	private BmovilViewsController parentManager;
	
	//#region Life-cycle management.
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppAdmonApi.getResourceId("layout_bmovil_detalle_asociacion_cuenta_telefono_admon", "layout"));
		setTitle(R.string.bmovil_asociar_cuenta_telefono_alternative_title, R.drawable.icono_spei);
		/*
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("detalle de cuentas", parentManager.estados);
		*/
		SuiteApp.appContext=this;
		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((MantenimientoSpeiMovilDelegate)parentViewsController.getBaseDelegateForKey(MantenimientoSpeiMovilDelegate.DELEGATE_ID));
		ownDelegate = (MantenimientoSpeiMovilDelegate) getDelegate();
		ownDelegate.setOwnerController(this);
		
		loadingContact = false;
		
		findViews();
		scaleForCurrentScreen();
		initialize();	
	}

	/**
	 * Search the required views for the operation of this screen.
	 */
	private void findViews() {
//		speiToggleButton = (CheckBox)findViewById(R.id.speiAvailableToogleButton);
		phoneNumberEdittext = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("phoneEdittext", "id"));
		confirmPhoneNumberEdittext = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("phoneConfirmEdittext", "id"));
		companiesListLayout = (LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("companiesListLayout", "id"));
	}
	
	/**
	 * Scale the shown views to fit the current screen resolution.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("mainLayout", "id")));
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("accountLayout", "id")), false);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("companiesListTitle", "id")), true);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("companiesListLayout", "id")), false);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("phoneLabel", "id")), true);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("phoneEdittext", "id")), true);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("phoneConfirmLabel", "id")), true);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("phoneConfirmEdittext", "id")), true);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("searchContactsButton", "id")), false);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("confirmButton", "id")));
	}
	
	/**
	 * Initialize the screen values.
	 */
	private void initialize() {
		originAccount = ownDelegate.loadOriginAccountComponent();
		if(null != originAccount)
			((LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("accountLayout", "id"))).addView(originAccount);
		else
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while loading the origin account component.");
		
		companiesList = ownDelegate.loadCompaniesList();
		if(null != companiesList)
			companiesListLayout.addView(companiesList);
		else
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while loading the phone companies list.");
		
		if(ownDelegate.getSpeiModel().getTipoOperacion() == TiposOperacionSpei.Modify) {
			//phoneNumberEdittext.setText(ownDelegate.getSelectedSpeiAccount().getTelefonoAsociado());
			phoneNumberEdittext.setText(ownDelegate.getSelectedSpeiAccount().getCelularAsociado());
			//confirmPhoneNumberEdittext.setText(ownDelegate.getSelectedSpeiAccount().getTelefonoAsociado());
			confirmPhoneNumberEdittext.setText(ownDelegate.getSelectedSpeiAccount().getCelularAsociado());
		}
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		ownDelegate.setOwnerController(this);
		getParentViewsController().setCurrentActivityApp(this);
		if(!loadingContact)
			parentViewsController.consumeAccionesDeReinicio();
		
//		if(!ownDelegate.isAssociate())
//			onConfirmButtonClicked(null);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(!loadingContact)
			parentViewsController.consumeAccionesDePausa();
	}
	//#endregion
	
	//#region Network.
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		ownDelegate.analyzeResponse(operationId, response);
	}
	//#endregion
	
	//#region OnClick Handlers
	/**
	 * Procedure when the confirm button is clicked.
	 * @param sender The clicked view.
	 */
	public void onConfirmButtonClicked(final View sender) {
		ownDelegate.validaDatos(phoneNumberEdittext.getText().toString(), 
								confirmPhoneNumberEdittext.getText().toString(), 
								companiesList.getSelectedItem());
	}
	
	/**
	 * Procedure when the contacts button is clicked.
	 * @param sender The clicked view.
	 */
	public void onContactsButtonClicked(final View sender) {
		if(loadingContact)
			return;
		loadingContact = true;
		AbstractContactMannager.getContactManager().requestContactData(this);
	}
	//#endregion

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		if(requestCode == CONTACTS_REQUEST_CODE) {
			loadingContact = false;
			if(Activity.RESULT_OK == resultCode) {
				obtainContactData(data);
			}
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	/**
	 * Obtains and set the contact data.
	 * @param data The activity result data.
	 */

	private void obtainContactData(final Intent data) {
		AbstractContactMannager manager = AbstractContactMannager.getContactManager();
		manager.getContactData(this, data);
		
		if(manager.getNumeroDeTelefono().equalsIgnoreCase(""))
			return;

		phoneNumberEdittext.setText(manager.getNumeroDeTelefono());
		confirmPhoneNumberEdittext.setText(manager.getNumeroDeTelefono());
	}
	
	/**
	 * Updates the origin account component with the selected account.
	 * @param currentAccount The selected account.
	 */
	public void updateOriginAccount(final Account currentAccount) {
		if(originAccount.isSeleccionado()) {
			originAccount.setSeleccionado(false);
		}
		
		originAccount.getImgDerecha().setEnabled(true);
		originAccount.getImgIzquierda().setEnabled(true);
		originAccount.getVistaCtaOrigen().setEnabled(true);
		
		phoneNumberEdittext.setText(ownDelegate.getSpeiModel().getCelularAsociado());
		confirmPhoneNumberEdittext.setText(ownDelegate.getSpeiModel().getCelularAsociado());
		companiesList.setSelectedItem(ownDelegate.getSpeiModel().getCompany());
		
		phoneNumberEdittext.invalidate();
		confirmPhoneNumberEdittext.invalidate();
	}
}
