package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultarEstatusEnvioEC implements ParsingHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC> cuentas=null;



	public ArrayList<suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC> getCuentas() {
		return cuentas;
	}

	public void setCuentas(final ArrayList<suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC> cuentas) {
		this.cuentas = cuentas;
	}

	/**
	 * Default Construtor
	 */

	public ConsultarEstatusEnvioEC()
	{
		cuentas = new ArrayList<suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC>();
	}


	private ArrayList<suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC> parseCuentas(final ParserJSON parser)throws IOException, ParsingException
	{
		ArrayList<suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC> objetos =  null;
		JSONObject root=null;
		JSONArray elements=null;
		JSONObject actual=null;
		suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC auxiliar = null;

		root= parser.parserNextObject("arrCuentas");
		try {
			elements = root.getJSONArray("ocCuentas");
			objetos = new ArrayList<suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC>();
			for(int i=0;i<elements.length();i++){
				actual = elements.getJSONObject(i);
				auxiliar = new CuentaEC();
				auxiliar.setEstadoEnvio(actual.getString("estatusEnvio"));
				auxiliar.setNumeroCuenta(actual.getString("numeroCuenta"));
				auxiliar.setFlag(false);
				objetos.add(auxiliar);
			}
		} catch (JSONException e) {
			Log.e(this.getClass().getSimpleName(), "No se pudo interpretar el json.", e);
			return null;
		}


		return objetos;
		
	}
	
	
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		
		this.cuentas = parseCuentas(parser);
	}

}
