package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioPerfilDelegate;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.PopUpAvisoPrivacidadCommon;

public class BeneficiosViewController extends BaseViewController implements OnClickListener{

    private static final String TAG = "BeneficiosViewCtrl";
    private static final int INDICATORS = 3;
    private TextView texto1,texto2,texto3;
    private Button botonAceptar;
    private CheckBox acepto_terminos_checkbox;
    private ViewPager viewPager;
    private View[] indicators;
    //private boolean migraBasico = false;
    public boolean flujoLogin = false;

    private CambioPerfilDelegate cambioPerfilDelegate;

    /**
     * Textview clickeable que muestra el aviso de privacidad
     */
    private TextView consultaAvisoPrivacidad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.activity_beneficios_view_controller);

        setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());

        SuiteApp.appContext = this;
        SuiteAppAdmonApi.appContext=this;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            //migraBasico = (Boolean) bundle.get("realizaTransaccion");
            flujoLogin = (Boolean) bundle.get("flujoLogin");
        }

        cambioPerfilDelegate = (CambioPerfilDelegate)getParentViewsController().getBaseDelegateForKey(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
        findViews();
        scaleToScreenSize();

        botonAceptar.setOnClickListener(this);

        MyViewPagerAdapter adapter = new MyViewPagerAdapter();
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new WizardPageChangeListener());
        updateIndicators(0);

        CambioPerfilDelegate.cambioPerfilViewController = this;
        cambioPerfilDelegate.setNuevoPerfil(null);

    }

    public void aceptarCambioPerfil() {
        if (acepto_terminos_checkbox.isChecked()) {
            cambioPerfilDelegate.cambioPerfilAceptado();
        } else {
            showMensajeErrorTerminos();
        }
    }

    /*
	 * (non-Javadoc)
	 *
	 * @see android.app.Activity#onResume()
	 */
    @Override
    protected void onResume() {
        super.onResume();
        SuiteApp.appContext = this;
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }

        getParentViewsController().setCurrentActivityApp(this);
        cambioPerfilDelegate.setBaseViewController(this);
        cambioPerfilDelegate.setOwnerController(this);

        SharedPreferences preferences = getSharedPreferences(Constants.SHARED_PREFERENCES,MODE_PRIVATE);
        String type = preferences.getString(Constants.SHARED_PREFERENCES_FLAG_TYPE, Constants.SHARED_PREFERENCES_FLAG_DEFAULT);
        String activationCode = preferences.getString(Constants.SHARED_PREFERENCES_FLAG_CODE,Constants.SHARED_PREFERENCES_FLAG_DEFAULT);
        boolean existeArchivoP = SofttokenActivationBackupManager.getCurrent().existsABackup();

        if(ServerCommons.ALLOW_LOG)
            Log.i(TAG, "onCreate: cp_flag = " + type + " activationCode = " + activationCode);
        if(type.equals(Constants.FLAG_ST) && existeArchivoP){
            cambioPerfilDelegate.showActivacion();
            finish();
        }
        else if ((type.equals(Constants.FLAG_CP) || type.equals(Constants.FLAG_CP_REAL) || Constants.EMPTY_STRING.equals(type)) && !activationCode.equals(Constants.SHARED_PREFERENCES_FLAG_DEFAULT)){
            // cambio perfil
            startActivity(new Intent(this, ActivationPerfilController.class).putExtra(Constants.BUNDLE_FLAG_URL, activationCode));
            finish();
        }
        else if (type.equals(Constants.FLAG_CP) || type.equals(Constants.FLAG_CP_REAL)){
            startActivity(new Intent(this, ActivationPerfilController.class));
            finish();
        }
    }

    /*
	 * (non-Javadoc)
	 *
	 * @see android.app.Activity#onPause()
	 */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /*
	 * (non-Javadoc)
	 *
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#goBack()
	 */
    @Override
    public void goBack() {
        // Solo cuando el perfil del usuario es avanzado se le permite volver a la pantalla anterior
        if (Constants.Perfil.avanzado.equals(Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile())) {
            cambioPerfilDelegate.cambioPerfilCancelado();
        }
        if(!cambioPerfilDelegate.isCambioPerfilLogin()){
            super.goBack();
        }
    }

    /*
	 * (non-Javadoc)
	 *
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#
	 * processNetworkResponse(int,
	 * suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
    @Override
    public void processNetworkResponse(final int operationId, final ServerResponse response) {
        cambioPerfilDelegate.analyzeResponse(operationId, response);
    }

    /**
     * Inicializa las vistas de la pantalla.
     */
    private void findViews() {
        texto1 = (TextView) findViewById( R.id.aceptar_contratacion);
        texto2 = (TextView) findViewById(R.id.acepto_terminos_label);
        texto3 = (TextView) findViewById(R.id.acepto_terminos_link);
        texto3.setText(Html.fromHtml(getString(R.string.contratacion_autenticacion_acepto_terminos_y_condiciones_linkperfil)));
        texto3.setPaintFlags(texto3.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        botonAceptar = (Button) findViewById(R.id.contratacion_autenticacion_confirmar_button);
        acepto_terminos_checkbox = (CheckBox)findViewById(R.id.acepto_terminos_checkbox);
        LinearLayout layoutDots = (LinearLayout)findViewById(R.id.layoutDots);

        if (flujoLogin)
            texto1.setText(getString(R.string.contratacion_autenticacion_aceptar_terminos_y_condiciones_anuncio));
        else
            texto1.setText(getString(R.string.contratacion_autenticacion_aceptar_terminos_y_condiciones_pregunta));

        indicators = new View[INDICATORS];
        for (int i = 0; i < INDICATORS; i++){
            View view = new View(this);
            view.setBackgroundResource(R.drawable.cambio_perfil_slider_indicator_bg);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(30, 30);
            layoutParams.setMargins(4,4,4,4);
            view.setLayoutParams(layoutParams);
            layoutDots.addView(view);
            indicators[i] = view;
        }

        viewPager = (ViewPager) findViewById(R.id.fragment_slider);

        consultaAvisoPrivacidad = (TextView) findViewById(R.id.consulta_aviso_privacidad);
        consultaAvisoPrivacidad.setPaintFlags(consultaAvisoPrivacidad.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

    }

    /**
     * Escala el layout al tamaño de la pantalla.
     */
    private void scaleToScreenSize() {
        final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(findViewById(R.id.layouy_contenedor));
        guiTools.scale(texto1, true);
        guiTools.scale(texto2, true);
        guiTools.scale(texto3, true);
        guiTools.scale(botonAceptar);
        guiTools.scale(consultaAvisoPrivacidad, true);
    }

    private void scaleToScreenSize(View view) {
        final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(view, true);
    }

    @Override
    public void onClick(View v) {
        /*if(!acepto_terminos_checkbox.isChecked()){
            showMensajeErrorTerminos();
            return;
        }*/
        Intent intent = new Intent(this,FirmaAutografaViewController.class).putExtra("flujoLogin", flujoLogin);
        startActivity(intent);
        finish();
//        if (Constants.Perfil.basico.equals(Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile())){
//            if (flujoLogin) {
//                //*llamar a softtoken*//
//                SuiteAppApi apis = new SuiteAppApi();
//                apis.onCreate(this);
//                apis.setIntentToReturn(SuiteAppAdmonApi.getCallBackBConnect());
//                startActivity(new Intent(BeneficiosViewController.this, AutenticacionSTViewController.class));
//            }else {
//                /* llamar a cambio perfil */
//                aceptarCambioPerfil();
//            }
//        }else if (Constants.Perfil.recortado.equals(Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile())){
//            /* realiza peticion a migra*/
//            cambioPerfilDelegate.realizaTransaccionMigraBasico();
//        }
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
            // constructor para poder ser llamado
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.layout_bmovil_cambio_perfil_slider_page2, container, false);
            TextView title = (TextView)view.findViewById(R.id.title_slider);
            TextView content = (TextView)view.findViewById(R.id.content_slider);
            ImageView image = (ImageView)view.findViewById(R.id.image_slider);
            switch (position){
                case 0:
                    title.setText(getString(R.string.slider_title_1));
                    title.setTextColor(getResources().getColor(R.color.primer_azul));
                    image.setImageResource(R.drawable.im_preocupamos);
                    content.setText(getString(R.string.slider_body_1));
                    break;
                case 1:
                    title.setText(getString(R.string.slider_title_2));
                    title.setTextColor(getResources().getColor(R.color.naranja));
                    image.setImageResource(R.drawable.im_cuentas);
                    content.setText(getString(R.string.slider_body_2));
                    break;
                case 2:
                    title.setText(getString(R.string.slider_title_3));
                    title.setTextColor(getResources().getColor(R.color.verde));
                    image.setImageResource(R.drawable.im_dinero_2);
                    content.setText(getString(R.string.slider_body_3));
                    break;
            }
            content.setGravity(Gravity.CENTER);
            scaleToScreenSize(title);
            scaleToScreenSize(content);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return INDICATORS;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }

    }

    private class WizardPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int position) {
            // on page scroll state changed

        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            // on page scrolled

        }

        @Override
        public void onPageSelected(int position) {
            updateIndicators(position);
        }

    }

    public void updateIndicators(int position) {
        for (int i = 0; i < INDICATORS; i++){
            indicators[i].setBackgroundResource(R.drawable.cambio_perfil_slider_indicator_bg);
        }
        indicators[position].setBackgroundResource(R.drawable.cambio_perfil_slider_indicator_bg_blue);
    }

    /**
     * Muestra los terminos y condiciones.
     */
    /*public void onVerTerminosLinkClik(final View sernder) {
        final ContratacionAutenticacionDelegate delegate = new ContratacionAutenticacionDelegate(cambioPerfilDelegate);
        getParentViewsController().addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID,delegate);
        final ContratacionAutenticacionViewController caller = new ContratacionAutenticacionViewController();
        caller.setDelegate(delegate);
        caller.setParentViewsController(((BmovilViewsController) SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController()));
        delegate.setcontratacionAutenticacionViewController(caller);
        delegate.consultarTerminosDeUso();

        if(Server.ALLOW_LOG)
            Log.d(this.getClass().getSimpleName(), "Ver terminos y condiciones.");
    }*/

    public void showMensajeErrorTerminos(){
        showInformationAlert(getString(R.string.contratacion_autenticacion_aceptar_terminos_y_condiciones_falta));
    }

    /**
     * Muestra los terminos y condiciones.
     */
    public void onConsultaAvisoPrivacidadLinkClik(final View sernder) {
        Intent intent = new Intent();
        intent.setClass(this, PopUpAvisoPrivacidadCommon.class);
        this.startActivityForResult(intent, 1234);

    }


}