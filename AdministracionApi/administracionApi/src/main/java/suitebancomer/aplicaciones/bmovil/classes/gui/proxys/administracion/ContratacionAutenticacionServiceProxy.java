/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.gui.proxys.administracion;

import android.util.Log;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ContratacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ContratacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ContratacionDelegate;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.proxys.IContratacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.to.ParamTo;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtp;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtpBmovil;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * @author lbermejo
 *
 */
public class ContratacionAutenticacionServiceProxy 
implements IContratacionAutenticacionServiceProxy {
	
	/**
	 */
	private static final long serialVersionUID = 3925667201345924488L;
	private BaseDelegateCommons baseDelegateCommons;
	
	public ContratacionAutenticacionServiceProxy(final BaseDelegateCommons bdc ) {
		this.baseDelegateCommons = bdc;
	}
	
	@Override
	public ArrayList<Object> getListaDatos() {

		//ContratacionAutenticacionDelegate
		//tipo operacion ContratacionDelegate   o CambioPerfilDelegate 
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getListaDatos >> delegate");
		final ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate)baseDelegateCommons;
		final ArrayList<Object> list = delegate.consultaOperationsDelegate().getDatosTablaConfirmacion();
		return list;				
	}

	@Override
	public ConfirmacionViewTo showFields() {

		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy showFields >> delegate");
		final ConfirmacionViewTo to = new ConfirmacionViewTo();
		final ContratacionAutenticacionDelegate delegate = 
				(ContratacionAutenticacionDelegate)baseDelegateCommons;
		
		to.setShowContrasena(delegate.consultaDebePedirContrasena());
		to.setShowNip(delegate.consultaDebePedirNIP());
		to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
		to.setShowCvv(delegate.consultaDebePedirCVV());
		to.setShowTarjeta(delegate.mostrarCampoTarjeta() );
		
		to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
		to.setInstrumentoSeguridad(delegate.consultaTipoInstrumentoSeguridad());
		to.setTextoAyudaInsSeg(
				delegate.getTextoAyudaInstrumentoSeguridad(
						to.getInstrumentoSeguridad()));
		
		if(delegate.consultaOperationsDelegate() instanceof ContratacionDelegate){ //Llegamos desde contratación
			final ContratacionDelegate contratacionDelegate =
					(ContratacionDelegate)delegate.consultaOperationsDelegate();
			
			if(contratacionDelegate.isDeleteData())
			{	contratacionDelegate.deleteData(); }
		}
		/* 
		mostrarContrasena(contratacionAutenticacionDelegate.consultaDebePedirContrasena());
		mostrarNIP(contratacionAutenticacionDelegate.consultaDebePedirNIP());
		mostrarASM(contratacionAutenticacionDelegate.consultaInstrumentoSeguridad());
		mostrarCVV(contratacionAutenticacionDelegate.consultaDebePedirCVV());
		mostrarCampoTarjeta(contratacionAutenticacionDelegate.mostrarCampoTarjeta());	
		 */
		
		/*
		 * if ((contratacionAutenticacionDelegate.consultaOperationsDelegate().mostrarCVV())
				&& (!contratacionAutenticacionDelegate.consultaOperationsDelegate().mostrarNIP())) {
		 */
		return to;
	}
	
	@Override
	public Integer getMessageAsmError(final Constants.TipoInstrumento tipoInstrumento) {

		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getMessageAsmError >> delegate");
		int idMsg = 0;

		switch (tipoInstrumento) {
			case OCRA:
				idMsg = R.string.confirmation_ocra;
				break;
			case DP270:
				idMsg = R.string.confirmation_dp270;
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					idMsg = R.string.confirmation_softtokenActivado;
				} else {
					idMsg = R.string.confirmation_softtokenDesactivado;
				}
				break;
			default:
				break;
		}

		return idMsg;
		
		/*switch (tipoInstrumentoSeguridad) {
			case OCRA:
				mensaje += getEtiquetaCampoOCRA();
				break;
			case DP270:
				mensaje += getEtiquetaCampoDP270();
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					mensaje += getEtiquetaCampoSoftokenActivado();
				} else {
					mensaje += getEtiquetaCampoSoftokenDesactivado();
				}
				break;
			default:
				break;
		}
		*/ 
	}

	@Override
	public String loadOtpFromSofttoken(final Constants.TipoOtpAutenticacion tipoOtpAutenticacion) {

		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy loadOtpFromSofttoken >> delegate");
		final ContratacionAutenticacionDelegate delegate = 
				(ContratacionAutenticacionDelegate)baseDelegateCommons;
		final String res = delegate.loadOtpFromSofttoken(tipoOtpAutenticacion);
		return res;
	}

	@Override
	public Boolean doOperation(final ParamTo to) {

		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy doOperation >> delegate");
		if(ServerCommons.ALLOW_LOG)Log.d("APP","en 66.1");
		final ContratacionAutenticacionDelegate delegate =
				(ContratacionAutenticacionDelegate)baseDelegateCommons;

		final ConfirmacionViewTo params = (ConfirmacionViewTo)to;
		final ContratacionAutenticacionViewController caller = new ContratacionAutenticacionViewController();
		caller.setDelegate(delegate);
		caller.setParentViewsController(
				((BmovilViewsController) SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController()));
		delegate.setcontratacionAutenticacionViewController(caller);
		try {

			String newToken = null;
			suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST validacionEstatusST = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST();
			validacionEstatusST.validaEstatusSofttoken(SuiteApp.appContext);
			if(ServerCommons.ALLOW_LOG){
				Log.d("APP","el estatus del token es: "+ PropertiesManager.getCurrent().getSofttokenService());
			}
			if(params.getTokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno
					&& params.getInstrumentoSeguridad() == Constants.TipoInstrumento.SoftToken
					&& (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService())){

				if(SuiteAppCRApi.getSofttokenStatus()) {
					if(ServerCommons.ALLOW_LOG){
						Log.d("APP","softToken local");
					}
					newToken =loadOtpFromSofttoken(params.getTokenAMostrar());
				}
				else if(!SuiteAppCRApi.getSofttokenStatus() && PropertiesManager.getCurrent().getSofttokenService()) {
					if(ServerCommons.ALLOW_LOG){
						Log.d("APP","softoken compartido");
					}
					//suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST validacionEstatusST = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST();
					//validacionEstatusST.validaEstatusSofttoken(SuiteApp.appContext);
					GetOtpBmovil otp = new GetOtpBmovil(new GetOtp() {
						/**
						 *
						 * @param otp
						 */
						@Override
						public void setOtpRegistro(String otp) {

						}

						/**
						 *
						 * @param otp
						 */
						@Override
						public void setOtpCodigo(String otp) {
							if(ServerCommons.ALLOW_LOG) {
								Log.d("APP", "la otp en callback: " + otp);
							}
							if(null != otp)
							{	params.setAsm(otp); }

							Log.d("APP","el newToken 6.1 "+otp);
							//AMZ
							delegate.consultaOperationsDelegate().realizaOperacion(
									caller, params.getNip(), params.getAsm(), params.getCvv(),
									params.getContrasena(), params.getOkTerminos(), params.getTarjeta());
						}

						/**
						 *
						 * @param otp
						 */
						@Override
						public void setOtpCodigoQR(String otp) {

						}
					}, SuiteApp.appContext);

					otp.generateOtpCodigo();

					return Boolean.TRUE;
				}
			}
			if(null != newToken)
			{	params.setAsm(newToken); }

			Log.d("APP","el newToken 6 "+newToken);
			//AMZ
			delegate.consultaOperationsDelegate().realizaOperacion(
					caller, params.getNip(), params.getAsm(), params.getCvv(),
					params.getContrasena(), params.getOkTerminos(), params.getTarjeta());

			return Boolean.TRUE;
		}catch(Exception e){
			if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy doOperation >> delegate" );
			Log.e(getClass().getName(),">>proxy doOperation >> delegate",e);
			return Boolean.FALSE;
		}

	}
	
	//@Override
	public Integer consultaOperationsIdTextoEncabezado(){

		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy consultaOperationsIdTextoEncabezado >> delegate");
		//getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()
		final ContratacionAutenticacionDelegate delegate = 
								(ContratacionAutenticacionDelegate)baseDelegateCommons;
		final int res = delegate.consultaOperationsDelegate().getTextoEncabezado();
		
		return res;
	}
	
	public Constants.Perfil consultaClienteProfile(){

		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy consultaClienteProfile >> delegate");
		return Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
	}

	public void consultarTerminosDeUso(){

		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy consultarTerminosDeUso >> delegate");
		final ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate)baseDelegateCommons;
		final ContratacionAutenticacionViewController caller = new ContratacionAutenticacionViewController();
		caller.setDelegate(delegate);
		caller.setParentViewsController(
				((BmovilViewsController) SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController()));
		delegate.setcontratacionAutenticacionViewController(caller);
		delegate.consultarTerminosDeUso();
		
	}
	
	@Override
	public BaseDelegateCommons getDelegate() {
		return baseDelegateCommons;
	}

}
