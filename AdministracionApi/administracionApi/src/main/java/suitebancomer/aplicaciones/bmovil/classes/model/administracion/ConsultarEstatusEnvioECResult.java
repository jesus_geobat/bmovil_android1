package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultarEstatusEnvioECResult implements ParsingHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 /**
	     * The fecha
	     */
	    private static String fecha = null;
	    
	    /**
	     * The hora
	     */
	    private static String hora = null;
	    
	    /**
	     * The folio
	     */
	    private static String folio = null;
	    

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
	   setFecha(parser.parseNextValue("fecha"));
	   setHora(parser.parseNextValue("hora"));
	   setFolio(parser.parseNextValue("folio"));
	}

	public static String getFecha() {
	    return fecha;
	}

	public void setFecha(final String fecha) {
	    this.fecha = fecha;
	}

	public static String getHora() {
	    return hora;
	}

	public void setHora(final String hora) {
	    this.hora = hora;
	}

	public static String getFolio() {
	    return folio;
	}

	public void setFolio(final String folio) {
	    this.folio = folio;
	}


}
