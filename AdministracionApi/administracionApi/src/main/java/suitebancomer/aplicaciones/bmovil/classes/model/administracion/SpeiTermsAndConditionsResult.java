package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class SpeiTermsAndConditionsResult implements ParsingHandler {
	/**
	 * The terms and conditions.
	 */
	private String terms;
	
	/**
	 * @return The terms and conditions.
	 */
	public String getTerms() {
		return terms;
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		terms = null;
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		terms = parser.parseNextValue("textoTerminosCondiciones");
	}
}
