package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class BeneficiarioBancomerData implements ParsingHandler {
	private String nombre;
	private String aPaterno;
	private String aMaterno;
	
	public String getNombreBeneficiarioCompleto() {
		String nombreTmp = Tools.isEmptyOrNull(nombre)? "" : nombre;
		String aPatTmp = Tools.isEmptyOrNull(aPaterno)? "" : aPaterno;
		String aMatTmp = Tools.isEmptyOrNull(aMaterno)? "" : aMaterno;
		
		String nombreCompleto = nombreTmp;
		if (aPatTmp != null || !aPatTmp.equals("")) {
			nombreCompleto += " " + aPatTmp;
		}
		if (aMatTmp != null || !aMatTmp.equals("")) {
			nombreCompleto += " " + aMatTmp;
		}
		
		return nombreCompleto;
	}
	
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		nombre = parser.parseNextValue("NO");
		aPaterno = parser.parseNextValue("PP");
		aMaterno = parser.parseNextValue("PM");
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}
	
	
}
