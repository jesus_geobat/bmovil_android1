/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.common.administracion;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

/**
 * Implementation of the App power manager for api7 or greater.
 */
public final class EclairPowerManager extends AbstractSuitePowerManager {
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.common.AbstractSuitePowerManager#isScreenOn()
	 */
	@SuppressLint("NewApi")
	@Override
	public boolean isScreenOn() {
		try {
			PowerManager pm = (PowerManager) SuiteAppAdmonApi.appContext.getSystemService(Context.POWER_SERVICE);
			return pm.isScreenOn();
		}catch(Exception ex) {
			if(Server.ALLOW_LOG) 
				Log.wtf("EclairPowerManager", "Error while getting the state of the screen.",ex);
			return false;
		}
	}

}
