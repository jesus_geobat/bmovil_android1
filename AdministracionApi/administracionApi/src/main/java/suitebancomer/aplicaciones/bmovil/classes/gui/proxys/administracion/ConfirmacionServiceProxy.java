/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.gui.proxys.administracion;

import android.util.Log;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfirmacionDelegate;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.to.ParamTo;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * @author lbermejo
 *
 */
public class ConfirmacionServiceProxy implements IConfirmacionServiceProxy {
	
	/**
	 */
	private static final long serialVersionUID = 3925667201345924488L;
	private BaseDelegateCommons baseDelegateCommons;
	
	public ConfirmacionServiceProxy(final BaseDelegateCommons bdc ) {
		this.baseDelegateCommons = bdc;
	}
	
	@Override
	public ArrayList<Object> getListaDatos() {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getListaDatos >> delegate");

		final ConfirmacionDelegate delegate = (ConfirmacionDelegate)baseDelegateCommons;
		final ArrayList<Object> list = delegate.consultaOperationsDelegate().getDatosTablaConfirmacion();
		
		return list;
	}
	
	@Override
	public ConfirmacionViewTo showFields() {
		final ConfirmacionViewTo to = new ConfirmacionViewTo();
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy showFields >> delegate");

		final ConfirmacionDelegate delegate = (ConfirmacionDelegate)baseDelegateCommons;
		to.setShowContrasena(delegate.consultaDebePedirContrasena());
		to.setShowNip(delegate.consultaDebePedirNIP());
		to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
		to.setShowCvv(delegate.consultaDebePedirCVV());
		to.setShowTarjeta(delegate.mostrarCampoTarjeta() );

		to.setInstrumentoSeguridad(delegate.consultaTipoInstrumentoSeguridad());
		to.setTokenAMostrar(delegate.tokenAMostrar());

		to.setTextoAyudaInsSeg(
					delegate.getTextoAyudaInstrumentoSeguridad(
								to.getInstrumentoSeguridad()));
		/* 
			//mostrarContrasena(confirmacionDelegate.consultaDebePedirContrasena());
			//mostrarNIP(confirmacionDelegate.consultaDebePedirNIP());
			//mostrarASM(confirmacionDelegate.consultaInstrumentoSeguridad());
			//mostrarCVV(confirmacionDelegate.consultaDebePedirCVV());
			//mostrarCampoTarjeta(confirmacionDelegate.mostrarCampoTarjeta());  */
		return to;
	}

	@Override
	public Integer getMessageAsmError(final Constants.TipoInstrumento tipoInstrumento) {

		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getMessageAsmError >> delegate");

		int idMsg = 0;
		switch (tipoInstrumento) {
		case OCRA:
			idMsg = R.string.confirmation_ocra;
			break;
		case DP270:
			idMsg = R.string.confirmation_dp270;
			break;
		case SoftToken:
			if (SuiteAppAdmonApi.getSofttokenStatus()) {
				idMsg = R.string.confirmation_softtokenActivado;
			} else {
				idMsg = R.string.confirmation_softtokenDesactivado;
			}
			break;
		default:
			break;
		}
		
		return idMsg;
		
		/*switch (tipoInstrumentoSeguridad) {
			case OCRA:
				mensaje += getEtiquetaCampoOCRA();
				break;
			case DP270:
				mensaje += getEtiquetaCampoDP270();
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					mensaje += getEtiquetaCampoSoftokenActivado();
				} else {
					mensaje += getEtiquetaCampoSoftokenDesactivado();
				}
				break;
			default:
				break;
		}
		*/ 
	}

	@Override
	public String loadOtpFromSofttoken(final Constants.TipoOtpAutenticacion tipoOtpAutenticacion) {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy loadOtpFromSofttoken >> delegate");

		final ConfirmacionDelegate delegate = (ConfirmacionDelegate)baseDelegateCommons;
		final String res = delegate.loadOtpFromSofttoken(tipoOtpAutenticacion);
		return res;
	}

	@Override
	public Boolean doOperation(final ParamTo to) {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy doOperation >> delegate");

		final ConfirmacionDelegate delegate = (ConfirmacionDelegate)baseDelegateCommons;
		final ConfirmacionViewTo params = (ConfirmacionViewTo)to;
		final ConfirmacionViewController caller = new ConfirmacionViewController();
		caller.setDelegate(delegate);
		caller.setParentViewsController(((BmovilViewsController)
				SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController()));
		delegate.setConfirmacionViewController(caller);
		
		try {
			//if ((delegate.consultaOperationsDelegate() instanceof AltaRetiroSinTarjetaDelegate )
			//		|| (delegate.consultaOperationsDelegate() instanceof ConsultaRetiroSinTarjetaDelegate)) {
				
			//	delegate.consultaOperationsDelegate().realizaOperacion(
			//			caller, params.getContrasena(), params.getNip(), params.getAsm(),
			//			params.getTarjeta(), params.getCvv());
			//} else {
				
				delegate.consultaOperationsDelegate().realizaOperacion(
						caller, params.getContrasena(), params.getNip(),
						params.getAsm(), params.getTarjeta());
			//}
			return Boolean.TRUE;

			
		}catch(Exception e){
			//TODO Log
			return Boolean.FALSE;
		}
		
	}

	@Override
	public Integer consultaOperationsIdTextoEncabezado() {

		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy consultaOperationsIdTextoEncabezado >> delegate");
		//getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()
		final ConfirmacionDelegate delegate = (ConfirmacionDelegate)baseDelegateCommons;
		final int res = delegate.consultaOperationsDelegate().getTextoEncabezado();
		return res;
	}

	@Override
	public BaseDelegateCommons getDelegate() {
		return baseDelegateCommons;
	}

	

}
