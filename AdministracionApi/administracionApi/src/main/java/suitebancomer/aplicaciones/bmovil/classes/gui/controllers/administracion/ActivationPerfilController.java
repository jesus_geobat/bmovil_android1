package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ActivationPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioPerfilDelegate;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.UtilsSms;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;

public class ActivationPerfilController extends BaseViewController implements View.OnClickListener{

    private EditText editText;
    private TextView reenviarCodigo;
    private Button  btnConfirmar;
    private ActivationPerfilDelegate delegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.activity_activation_perfil);
        setTitle(R.string.softtoken_validacion_titulo, R.drawable.icono_st_activado);

        delegate = new ActivationPerfilDelegate(this);
        setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
        CambioPerfilDelegate.activacionPerfilController = this;

        SuiteApp.appContext = this;

        reenviarCodigo = (TextView)findViewById(R.id.lblReenviarCodigo);
        btnConfirmar = (Button)findViewById(R.id.btnConfirmar);
        scaleToCurrentScreen();

        SharedPreferences preferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
        String time = preferences.getString(Constants.SHARED_PREFERENCES_FLAG_TIME, "100000000");

        Bundle bundle = getIntent().getExtras();
        editText = (EditText)findViewById(R.id.tbClaveActivacion);
        if (bundle  != null ){
            String datos = bundle.getString(Constants.BUNDLE_FLAG_URL);

            if(datos.isEmpty()){
                showErrorMessage("Este codigo no es valido");
            }else{
                editText.setText(datos);
            }
            /*ArrayList<String> valueInfoSMS = UtilsSms.fetchInbox(time, this, datos);
            if (valueInfoSMS != null && valueInfoSMS.size() > 0 ) {
                editText.setText(datos);
            }else{
                showErrorMessage("Este codigo no es valido");
            }*/
        }

        reenviarCodigo.setOnClickListener(this);
        btnConfirmar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == reenviarCodigo){
            // se reenvia peticion de mensaje cambio de perfil
            delegate.cambioPerfilAceptado();
        }else if(v == btnConfirmar){
            delegate.confirmar(editText);
        }
    }

    private void scaleToCurrentScreen() {
        final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.rootLayout));
        guiTools.scale(findViewById(R.id.lblClave), true);
        guiTools.scale(findViewById(R.id.tbClaveActivacion), true);
        guiTools.scale(findViewById(R.id.lblInstrucciones), true);
        guiTools.scale(reenviarCodigo, true);
        guiTools.scale(findViewById(R.id.btnConfirmar));
    }

    public void processNetworkResponse(final int operationId, final ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

    @Override
    public void goBack() {
        SharedPreferences preferences = getSharedPreferences(Constants.SHARED_PREFERENCES,MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        finish();
    }

    @Override
    protected void onPause() {
        //camboPerfil = true;
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SuiteApp.appContext = this;
        SuiteAppCRApi.appContext = this;
        SuiteAppAdmonApi.appContext = this;
    }

    @Override
    protected void onStop() {
        //camboPerfil = true;
        super.onStop();
    }

}