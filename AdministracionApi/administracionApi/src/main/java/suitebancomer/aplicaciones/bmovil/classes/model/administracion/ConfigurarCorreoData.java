package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConfigurarCorreoData implements ParsingHandler {
	/**
	 * Folio de la arquitectura que devuelve la trasacci�n.
	 */
	private String folioArq;
	
	/**
	 * @return Folio de la arquitectura que devuelve la trasacci�n.
	 */
	public String getFolioArq() {
		return folioArq;
	}

	/**
	 * @param filioArq Folio de la arquitectura que devuelve la trasacci�n.
	 */
	public void setFilioArq(final String folioArq) {
		this.folioArq = folioArq;
	}

	public ConfigurarCorreoData() {
		folioArq = null;
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		folioArq = null;
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		folioArq = parser.parseNextValue("folioArq");
	}

}
