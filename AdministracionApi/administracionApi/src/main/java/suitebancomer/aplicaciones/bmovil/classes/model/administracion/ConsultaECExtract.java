/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;


//import suitebancomer.aplicaciones.bmovil.classes.common.Constants;

/**
 * ConsultaECExtract wraps an account periods, the list of the last 12 periods
 * 
 * @author Carlos Santoyo
 */
public class ConsultaECExtract  implements ParsingHandler {

    /**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 3272545845918202838L;

    /**
     * The periods
     */
    private ArrayList<Periodo> periodos = null;

    /**
     * Default constructor
     */
    public ConsultaECExtract() {
    	setPeriodos(new ArrayList<Periodo>());
    }

    /**
     * Constructor with parameters
 
     * @param periodos the periodos Array
     */
    public ConsultaECExtract(final ArrayList<Periodo> periodos) {

        this.setPeriodos(periodos);
        
    }


    /**
     * Parse periodos from parser data
     * @param parser the parser
     * @return the movements
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     * @throws NumberFormatException on format errors 
     */
    private ArrayList<Periodo> parsePeriodos(final ParserJSON parser) throws IOException, ParsingException {
		final ArrayList<Periodo>result = new ArrayList<Periodo>();
		final String ocPeriodos = parser.parseNextValue("arrPeriodosEC");
        JSONArray arrayPeriodos = new JSONArray();
        try {
			final JSONObject jsonObject = new JSONObject(ocPeriodos);
			arrayPeriodos = jsonObject.getJSONArray("ocPeriodos");
		} catch (JSONException e) {
			if(ServerCommons.ALLOW_LOG) e.printStackTrace();
		}
		final int numPeriodos = arrayPeriodos.length();
       for(int i = 0; i < numPeriodos; i++) {
    	    
			try {

				final JSONObject periodoObj = arrayPeriodos.getJSONObject(i);
				//String periodo = Tools.parsePeriodoFecha(periodoObj.getString("periodoEC"));
				final String periodo = periodoObj.getString("periodoEC");
				final String fechaCorte = periodoObj.getString("fechaCorte");
				final String referencia = periodoObj.getString("referencia");
				final Periodo objPeriodo = new Periodo(periodo,fechaCorte,referencia);
				
				result.add(objPeriodo);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				if(ServerCommons.ALLOW_LOG) e.printStackTrace();
			}
     
        }
		if(ServerCommons.ALLOW_LOG) Log.d("TEMA",result.size()+"");
        return result;
    }

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
        this.setPeriodos(parsePeriodos(parser));
	}

	public ArrayList<Periodo> getPeriodos() {
	    return periodos;
	}

	public void setPeriodos(final ArrayList<Periodo> periodos) {
	    this.periodos = periodos;
	}

    
}
