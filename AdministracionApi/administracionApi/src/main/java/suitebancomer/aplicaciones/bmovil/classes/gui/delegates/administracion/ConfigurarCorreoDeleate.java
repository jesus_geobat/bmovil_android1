package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.util.Log;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Pattern;

import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConfigurarCorreo;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;


public class ConfigurarCorreoDeleate extends DelegateBaseAutenticacion {
	//AMZ
		public boolean res = false;
	// #region Variables.
	/**
	 * Identificador �nico del delegado.
	 */
	public static long CONFIGURAR_CORREO_DELEGATE_ID = 845732282391197403L;
	
	/**
	 * Operación actual.
	 */
	private Operacion operacion;
	
	/**
	 * Controlador asociado al delegado.
	 */
	private BaseViewController ownerController;
	
	/**
	 * Email matching pattern regex.
	 */
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	/**
	 * Modelo para transacci�n a NACAR.
	 */
	private ConfigurarCorreo model;
	
	// #endregion
	
	// #region Getters y Setters.
	/**
	 * @return Controlador asociado al delegado.
	 */
	public BaseViewController getOwnerController() {
		return ownerController;
	}

	/**
	 * @param ownerController Controlador asociado al delegado.
	 */
	public void setOwnerController(final BaseViewController ownerController) {
		this.ownerController = ownerController;
	}

	@Override
	public long getDelegateIdentifier() {
		return CONFIGURAR_CORREO_DELEGATE_ID;
	}
	
	/**
	 * @return Operación actual.
	 */
	public Operacion getOperacion() {
		return operacion;
	}

	/**
	 * @return Modelo para transacci�n a NACAR.
	 */
	public ConfigurarCorreo getModel() {
		return model;
	}
	// #endregion

	public ConfigurarCorreoDeleate() {
		model = new ConfigurarCorreo();
		operacion = Operacion.configurarCorreo;
	}
	
	// #region Network.
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#doNetworkOperation(int, java.util.Hashtable, suitebancomer.classes.gui.controllers.BaseViewController)
	 */
	@Override
	public void doNetworkOperation(final int operationId,	final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		SuiteAppAdmonApi.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params,isJson,handler, caller);
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			Session.getInstance(SuiteAppAdmonApi.appContext).setEmail(model.getNuevoCorreo());
			((BmovilViewsController)ownerController.getParentViewsController()).showResultadosAutenticacionViewController(this, 
					R.string.bmovil_configurar_correo_titulo, 
					R.drawable.icono_conf_correo);
		} else {
			ownerController.showInformationAlert(response.getMessageText());
		}
	}
	// #endregion
	
	// #region Confirmacion.
	/**
	 * Valida los correos electronicos de manera que cumplan la siguiente regla:
	 * <br/>
	 * Se debe validar que contenga un texto de inicio "ejemplo1" seguido el s�mbolo arroba "@", 
	 * un texto de proveedor de servicio despues del "@", 
	 * un punto "." y un texto despues del punto.
	 * @param nuevoEmail Nuevo correo electrónico.
	 * @param confirmarEmail Confirmaci�n de correo electrónico.
	 */
	public void validarDatos(final String nuevoEmail, final String confirmarEmail) {
		if(null == nuevoEmail) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "No se registro un email.");
			return;
		} else if(null == confirmarEmail) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "No se registro una confirmación de email.");
			return;
		}
		
		if(Tools.isEmptyOrNull(nuevoEmail)) {
			ownerController.showInformationAlert(R.string.bmovil_configurar_correo_error_correo_vacio);
		} else if(!Pattern.compile(EMAIL_PATTERN).matcher(nuevoEmail).matches()) {
			ownerController.showInformationAlert(R.string.bmovil_configurar_correo_error_correo_invalido);
		} else if(Session.getInstance(SuiteAppAdmonApi.appContext).getEmail().equals(nuevoEmail)) {
			ownerController.showInformationAlert(R.string.bmovil_configurar_correo_error_correo_ya_asignado);
		} else if(Tools.isEmptyOrNull(confirmarEmail)) {
			ownerController.showInformationAlert(R.string.bmovil_configurar_correo_error_confirmacion_vacio);
		} else if(!nuevoEmail.equals(confirmarEmail)) {
			ownerController.showInformationAlert(R.string.bmovil_configurar_correo_error_confirmacion_diferente);
		} else {
			model.setNuevoCorreo(nuevoEmail);
			((BmovilViewsController)ownerController.getParentViewsController()).showConfirmacionAutenticacionViewController(this, 
					R.string.bmovil_configurar_correo_titulo, 
					R.drawable.icono_conf_correo, 0);
			//AMZ
			res = true;
		}
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseAutenticacion#realizaOperacion(suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void realizaOperacion(final ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController,
								 final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {
		ownerController = confirmacionAutenticacionViewController;
		Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		
		Hashtable<String,String> paramTable = new Hashtable<String, String>();
		
		paramTable.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		paramTable.put(ServerConstants.EMAIL, model.getNuevoCorreo());       
		paramTable.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());         
		paramTable.put("cveAcceso", Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);    
		paramTable.put("codigoNIP", Tools.isEmptyOrNull(nip) ? "" : nip);         
		paramTable.put("codigoCVV2", Tools.isEmptyOrNull(cvv) ? "" : cvv);        
		paramTable.put(ServerConstants.CODIGO_OTP, Tools.isEmptyOrNull(token) ? "" : token);         
		paramTable.put("cadenaAutenticacion", Autenticacion.getInstance().getCadenaAutenticacion(operacion, session.getClientProfile())); 
		paramTable.put("tarjeta5Dig", campoTarjeta == null ? "" : campoTarjeta);
		
		paramTable.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
		List<String> listaEncriptar = Arrays.asList("cveAcceso", "codigoNIP",
				"codigoCVV2");
		Encripcion.setContext(SuiteAppAdmonApi.appContext);
		Encripcion.encriptaCadenaAutenticacion(paramTable, listaEncriptar);
		
		//JAIG
		doNetworkOperation(Server.OP_CONFIGURAR_CORREO, paramTable,true,new suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConfigurarCorreoData(), confirmacionAutenticacionViewController);
		//doNetworkOperation(Server.OP_CONFIGURAR_CORREO, paramTable, confirmacionAutenticacionViewController);
		
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getDatosTablaConfirmacion()
	 */
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;

		String value = SuiteAppAdmonApi.appContext.getString(R.string.bmovil_configurar_correo_confirmacion_correo_electronico);
		fila = new ArrayList<String>();
		fila.add(value);
		fila.add(model.getNuevoCorreo());
		tabla.add(fila);
		
		return tabla;
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseAutenticacion#mostrarCVV()
	 */
	@Override
	public boolean mostrarCVV() {
		return Autenticacion.getInstance().mostrarCVV(operacion, Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile());
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#mostrarContrasenia()
	 */
	@Override
	public boolean mostrarContrasenia() {
		return Autenticacion.getInstance().mostrarContrasena(operacion, Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile());
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#tokenAMostrar()
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		return Autenticacion.getInstance().tokenAMostrar(operacion, Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile());
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#mostrarNIP()
	 */
	@Override
	public boolean mostrarNIP() {
		return Autenticacion.getInstance().mostrarNIP(operacion, Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile());
	}

	

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getNombreImagenEncabezado()
	 */
	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.icono_conf_correo;
	}

	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoEncabezado()
	 */
	@Override
	public int getTextoEncabezado() {
		return R.string.bmovil_configurar_correo_titulo;
	}
	// #endregion
	
	// #region Resultados.
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getOpcionesMenuResultados()
	 */
	@Override
	public int getOpcionesMenuResultados() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoPantallaResultados()
	 */
	@Override
	public String getTextoPantallaResultados() {
		return SuiteAppAdmonApi.appContext.getString(R.string.bmovil_configurar_correo_resultados_texto);
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoTituloResultado()
	 */
	@Override
	public String getTextoTituloResultado() {
		return SuiteAppAdmonApi.appContext.getString(R.string.bmovil_configurar_correo_resultados_titulo);
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getColorTituloResultado()
	 */
	@Override
	public int getColorTituloResultado() {
		return R.color.verde_limon;
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getDatosTablaResultados()
	 */
	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		return getDatosTablaConfirmacion();
	}
	// #endregion
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}
}
