package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import android.util.Log;

import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class CambioCuentaResult implements ParsingHandler{
	private String folio;
	private Account[] asuntos;
	
	public String getFolio() {
		return folio;
	};
	
	public Account[] getAsuntos() {
		return asuntos;
	}
	
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
	
		throw new ParsingException("Invalid process.");
		
	}
	
	@Override
public void process(final ParserJSON parser) throws IOException, ParsingException {
		if(ServerCommons.ALLOW_LOG) Log.w("Change","ok");

		
		folio = parser.parseNextValue("folioArq");
		final JSONArray arrayAsuntos = parser.parseNextValueWithArray("asuntos", false);
		final int numAsuntos = arrayAsuntos.length();
		asuntos = new Account[numAsuntos];
		for(int i = 0;i < numAsuntos; i++ ){
		try {
			final JSONObject asuntoObj = arrayAsuntos.getJSONObject(i);
			final String tipoCuenta = asuntoObj.getString("tipoCuenta");
			final String alias = asuntoObj.getString("alias");
			final String divisa = asuntoObj.getString("divisa");
			final String asunto = asuntoObj.getString("asunto");
			final String saldo = asuntoObj.getString("saldo");
			final String concepto = asuntoObj.getString("concepto");
			final String visible = asuntoObj.getString("visible");
			/*String celularAsociado = asuntoObj.getString("celularAsociado");
			String codigoCompania = asuntoObj.getString("codigoCompania");
			String descripcionCompania = asuntoObj.getString("descripcionCompania");
			String fechaUltimaModificacion = asuntoObj.getString("fechaUltimaModificacion");
			String indicadorSPEI = asuntoObj.getString("indicadorSPEI");
			*/
			String celularAsociado =""; 
			if(asuntoObj.has("celularAsociado"))
				celularAsociado = asuntoObj.getString("celularAsociado");
			String codigoCompania ="";
			if(asuntoObj.has("ocdigoCompania"))
				codigoCompania= asuntoObj.getString("codigoCompania");
			String descripcionCompania ="";
			if(asuntoObj.has("descripcionCompania"))
				descripcionCompania= asuntoObj.getString("descripcionCompania");
			String fechaUltimaModificacion ="";
			if(asuntoObj.has("fechaUltimaModificacion"))
				fechaUltimaModificacion = asuntoObj.getString("fechaUltimaModificacion");
			String indicadorSPEI ="";
			if(asuntoObj.has("indicadorSPEI"))
				indicadorSPEI = asuntoObj.getString("indicadorSPEI");
			final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
			final String date = Tools.dateForReference(session.getServerDate());
			asuntos[i] = new Account(asunto,
					Tools.getDoubleAmountFromServerString(saldo),
					Tools.formatDate(date), "S".equals(visible), divisa,
					tipoCuenta, concepto, alias, celularAsociado, 
					codigoCompania, descripcionCompania, fechaUltimaModificacion , indicadorSPEI);
				
		} catch (JSONException e) {
			//throw new ParsingException("Error formato");
			if(ServerCommons.ALLOW_LOG) throw new ParsingException("Esto es lo que se muestra en el alert");

		}
			
		}		
	}
	
}
