package suitebancomer.aplicaciones.bmovil.classes.common.administracion;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;

//import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.DineroMovilViewController;


/**
 * Clase que obtiene los datos de un contacto para la version 1.6 de Android (Api 4).
 */
@TargetApi(Build.VERSION_CODES.DONUT)
public class DonutContactManager extends AbstractContactMannager {
	/**
	 * Constructor por defecto.
	 */
	public DonutContactManager() {
		super();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void getContactData(final Activity callerActivity, final Intent data) {
		String telefono = "";
	    String contacto = "";
	    Cursor c = null;
		final Uri contactData = data.getData();
		
        c = callerActivity.managedQuery(contactData, null, null, null, null);
        callerActivity.startManagingCursor(c);
        if (c.moveToFirst()) {
        	contacto = c.getString(c.getColumnIndexOrThrow(android.provider.Contacts.People.NAME));  
        	telefono = c.getString(c.getColumnIndexOrThrow(android.provider.Contacts.People.NUMBER));
        }
        
        telefono = Tools.formatPhoneNumberFromContact(telefono);
	    
	    if(Constants.TELEPHONE_NUMBER_LENGTH == telefono.length()) {
	    	this.numeroDeTelefono = telefono;
	    	this.nombreContacto = contacto;
	    } else {
	    	this.numeroDeTelefono = "";
	    	this.nombreContacto = "";
	    }
	}

	@Override
	public void requestContactData(final Activity callerActivity) {
		Intent intent = new Intent(Intent.ACTION_PICK, android.provider.Contacts.People.CONTENT_URI);
//AYMB		callerActivity.startActivityForResult(intent, DineroMovilViewController.ContactsRequestCode);
	}

}
