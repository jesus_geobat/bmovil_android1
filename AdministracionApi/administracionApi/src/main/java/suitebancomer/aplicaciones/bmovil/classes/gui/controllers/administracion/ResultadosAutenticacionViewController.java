package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.DelegateBaseOperacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MantenimientoAlertasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ResultadosAutenticacionDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.ListaDatosViewController;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class ResultadosAutenticacionViewController extends BaseViewController implements View.OnClickListener {
	private ResultadosAutenticacionDelegate resultadosAutenticacionDelegate;
	
	private TextView tituloResultados;
	private TextView textoResultados;
	private TextView instruccionesResultados;
	private TextView textoEspecialResultados;
	private ImageButton menuButton;
	//Mejoras Bmovil
	private ImageButton compartirButton;
	//AMZ
	private BmovilViewsController parentManager;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppAdmonApi.getResourceId("layout_bmovil_resultados_admon", "layout"));
		SuiteApp.appContext = this;
		//AMZ
		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("resul",parentManager.estados);

		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((ResultadosAutenticacionDelegate)getParentViewsController().getBaseDelegateForKey(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID));
		
		resultadosAutenticacionDelegate = (ResultadosAutenticacionDelegate)getDelegate();
		resultadosAutenticacionDelegate.setResultadosViewController(this);
		
		findViews();
		scaleToScreenSize();
		
		
		
		menuButton.setOnClickListener(this);
		
		setTitulo();
		resultadosAutenticacionDelegate.consultaDatosLista();
		configuraPantalla();
		moverScroll();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if (!(resultadosAutenticacionDelegate
				.getOperationDelegate() instanceof MantenimientoAlertasDelegate)) {
			if (parentViewsController.consumeAccionesDeReinicio()) {
				return;
			}
		}
		
		parentViewsController.setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
    	if(resultadosAutenticacionDelegate.getmBroadcastReceiver() != null ){
    		unregisterReceiver(resultadosAutenticacionDelegate.getmBroadcastReceiver());
    		resultadosAutenticacionDelegate.setmBroadcastReceiver(null);
    	}
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(SuiteAppAdmonApi.getResourceId("menu_bmovil_resultados", "menu"), menu);
		//AMZ

		final int opc = parentManager.estados.size()-1;
		final int opc2 = parentManager.estados.size()-2;
		if(parentManager.estados.get(opc) == "opciones"){
			final String rem = parentManager.estados.remove(opc);
		}else if(parentManager.estados.get(opc2) == "sms" || parentManager.estados.get(opc2) == "correo" 
					|| parentManager.estados.get(opc2) == "alta frecuentes"){
			String rem = parentManager.estados.remove(opc2);
			 rem = parentManager.estados.remove(parentManager.estados.size()-1);
		}else{
			TrackingHelper.trackState("opciones", parentManager.estados);
		}
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(final Menu menu) {
		final int opcionesMenu = resultadosAutenticacionDelegate.getOpcionesMenuResultados();
		boolean showMenu = false;
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_SMS) != DelegateBaseOperacion.SHOW_MENU_SMS) {
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_sms_button", "id"));
		} else {
			showMenu = true;
		}
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_EMAIL) != DelegateBaseOperacion.SHOW_MENU_EMAIL) {
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_email_button", "id"));
		} else {
			showMenu = true;
		}
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_PDF) != DelegateBaseOperacion.SHOW_MENU_PDF) {
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_pdf_button", "id"));
		} else {
			showMenu = true;
		}
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_FRECUENTE) != DelegateBaseOperacion.SHOW_MENU_FRECUENTE) {
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_frecuente_button", "id"));
		} else {
			showMenu = true;
		}
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_RAPIDA) != DelegateBaseOperacion.SHOW_MENU_RAPIDA) {
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_rapida_button", "id"));
		} else {
			showMenu = true;
		}
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_BORRAR) != DelegateBaseOperacion.SHOW_MENU_BORRAR) {
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_borrar_button", "id"));
		} else {
			showMenu = true;
		}
		
		return showMenu;
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		//ARR
		final Map<String,Object> envioConfirmacionMap = new HashMap<String, Object>();
		final int itemId = item.getItemId();
		if (itemId == R.id.save_menu_sms_button) {
			resultadosAutenticacionDelegate.enviaSMS();
			//AMZ
			final int sms = parentManager.estados.size()-1;
			if(parentManager.estados.get(sms)=="resul"){
				String rem = parentManager.estados.remove(sms);
				rem = parentManager.estados.remove(parentManager.estados.size()-1);
			}
			TrackingHelper.trackState("sms", parentManager.estados);
			TrackingHelper.trackState("resul", parentManager.estados);
			if(getString(resultadosAutenticacionDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_miscuentas))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+mis cuentas");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosAutenticacionDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+otra cuenta bbva bancomer");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosAutenticacionDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+cuenta express");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosAutenticacionDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+otros bancos");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosAutenticacionDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+dinero movil");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			return true;
		} else if (itemId == R.id.save_menu_email_button) {
			resultadosAutenticacionDelegate.enviaEmail();
			//AMZ
			final int correo = parentManager.estados.size()-1;
			if(parentManager.estados.get(correo)=="resul"){
				String rem = parentManager.estados.remove(correo);
				rem = parentManager.estados.remove(parentManager.estados.size()-1);
			}
			if(getString(resultadosAutenticacionDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_miscuentas))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+mis cuentas");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosAutenticacionDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+otra cuenta bbva bancomer");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosAutenticacionDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+cuenta express");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosAutenticacionDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+otros bancos");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosAutenticacionDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+dinero movil");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			return true;
		} else if (itemId == R.id.save_menu_pdf_button) {
			resultadosAutenticacionDelegate.guardaPDF();
			return true;
		} else if (itemId == R.id.save_menu_frecuente_button) {
			resultadosAutenticacionDelegate.guardaFrecuente();
			//AMZ
			final int frec = parentManager.estados.size()-1;
			if(parentManager.estados.get(frec)=="resul"){	
				String rem = parentManager.estados.remove(frec);
				rem = parentManager.estados.remove(parentManager.estados.size()-1);
			}
			return true;
		} else if (itemId == R.id.save_menu_rapida_button) {
			resultadosAutenticacionDelegate.guardaRapido();
			return true;
		} else if (itemId == R.id.save_menu_borrar_button) {
			resultadosAutenticacionDelegate.borraRapido();
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void goBack() {
		//Se supone que el boton de atras no haga nada
		if(resultadosAutenticacionDelegate.isOperacionTransferirEnCurso()){
			super.goBack();
		}
	}
	
	public void setTitulo() {
		super.setTitle(resultadosAutenticacionDelegate.getTextoEncabezado(), 
						resultadosAutenticacionDelegate.getNombreImagenEncabezado());
	}
	
	
	public ResultadosAutenticacionDelegate getResultadosAutenticacionDelegate() {
		return resultadosAutenticacionDelegate;
	}

	public void setListaDatos(final ArrayList<Object> datos) {
		@SuppressWarnings("deprecation")
		final LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.rightMargin =  getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);

		final ListaDatosViewController listaDatos = new ListaDatosViewController(this, params, parentViewsController);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.showLista();
		final LinearLayout layoutListaDatos = (LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("resultados_lista_datos", "id"));
		layoutListaDatos.addView(listaDatos);
	}
	
	public void configuraPantalla() {
		final String titulo = resultadosAutenticacionDelegate.getTextoTituloResultado();
		final String texto = resultadosAutenticacionDelegate.getTextoPantallaResultados();
		final String instrucciones = resultadosAutenticacionDelegate.getTextoAyudaResultados();
		final String textoEspecial = resultadosAutenticacionDelegate.getTextoEspecialResultados();
		
		if (titulo.equals("")) {
			tituloResultados.setVisibility(View.GONE);
		} else {
			tituloResultados.setText(titulo);
			tituloResultados.setTextColor(getResources().getColor(resultadosAutenticacionDelegate.getColorTituloResultado()));
		}
		
		if (texto.equals("")) {
			textoResultados.setVisibility(View.GONE);
		} else {
			textoResultados.setText(texto);
		}
		
		if (instrucciones.equals("")) {
			instruccionesResultados.setVisibility(View.GONE);
		} else {
			instruccionesResultados.setText(instrucciones);
		}
		
		if (textoEspecial.equals("")) {
			textoEspecialResultados.setVisibility(View.GONE);
		} else {
			textoEspecialResultados.setText(textoEspecial);
		}
		
		//Mejoras Bmovil
		if(instruccionesResultados.getText() == ""){
			compartirButton.setVisibility(View.INVISIBLE);
		}
		
		/*
		ViewGroup contenido = (ViewGroup)this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
		contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float contentHeight = contenido.getMeasuredHeight();
		LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.resultados_lista_datos);
		layoutListaDatos.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float listaHeight = layoutListaDatos.getMeasuredHeight();
		textoResultados.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float textoHeight = textoResultados.getMeasuredHeight();
		instruccionesResultados.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float instruccionesHeight = instruccionesResultados.getMeasuredHeight();
		textoEspecialResultados.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float textoEspecialHeight = textoEspecialResultados.getMeasuredHeight();
		menuButton.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float buttonHeight = menuButton.getMeasuredHeight();
		
		float margin = getResources().getDimension(R.dimen.confirmacion_fields_initial_margin);
		
		float maximumSize = (contentHeight * 4) / 5;
		System.out.println("Altura maxima " +maximumSize);
		float elementsSize = listaHeight + textoHeight + instruccionesHeight + textoEspecialHeight + buttonHeight;
		System.out.println("Altura mixta " +elementsSize);
		float heightParaValidar = (contentHeight*3)/4;
		System.out.println("heightParaValidar " +contentHeight);
		
		RelativeLayout.LayoutParams botonLayout = (RelativeLayout.LayoutParams)menuButton.getLayoutParams();
		if (elementsSize <= contentHeight) {
			botonLayout.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		} else {
			botonLayout.addRule(RelativeLayout.BELOW, R.id.resultado_texto_especial);
		}
		*/
		menuButton.setBackgroundResource(resultadosAutenticacionDelegate.getImagenBotonResultados());
	}
	
	@Override
	public void onClick(final View v) {
		if (v == menuButton && !parentViewsController.isActivityChanging()) {
			/*if (!(resultadosAutenticacionDelegate
					.getOperationDelegate() instanceof MantenimientoAlertasDelegate)) {
				menuButton.setEnabled(false);
			}*/
			
			resultadosAutenticacionDelegate.accionBotonResultados();
//ARR Comprobacion de titulos
			
			if(getString(resultadosAutenticacionDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.bmovil_cambio_telefono_title))
			{
				//ARR
				final Map<String, Object> eventoSalir = new HashMap<String, Object>();
				eventoSalir.put("evento_salir", "event23");
				
				TrackingHelper.trackDesconexiones(eventoSalir);
			}
			
			//AMZ
			((BmovilViewsController)parentViewsController).touchMenu();
			
			//botonMenuClick();
		}
	}
	
//	public void botonMenuClick() {
//		parentViewsController.removeDelegateFromHashMap(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
//		parentViewsController.removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
//		((BmovilViewsController)parentViewsController).showMenuPrincipal(true);
//	}
	
	public BroadcastReceiver createBroadcastReceiver() {
    	
	     return new BroadcastReceiver() {
			@Override
		     public void onReceive(final Context ctx, final Intent intent) {
		    	 
		    	 String toastMessage;
		    	 
			     switch (getResultCode()) {
				     case Activity.RESULT_OK:
					     toastMessage = getString(R.string.sms_success);    
					     break;
				     case SmsManager.RESULT_ERROR_NO_SERVICE: //"SMS: No service"
				    	 toastMessage = getString(R.string.sms_error_noService);
				    	 break;
				     case SmsManager.RESULT_ERROR_NULL_PDU: //"SMS: Null PDU"
				    	 toastMessage = getString(R.string.sms_error_nullPdu);
				    	 break;
				     case SmsManager.RESULT_ERROR_RADIO_OFF: //"SMS: Radio off"
				    	 toastMessage = getString(R.string.sms_error_radioOff);
				    	 break;
				     case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				     default:
				    	 toastMessage = getString(R.string.sms_error);
				    	 break;
			     }
			     
			     ocultaIndicadorActividad();
			     Toast.makeText(getBaseContext(), toastMessage,Toast.LENGTH_SHORT).show();
		     }
	     };
	}
	
	private void findViews() {
		tituloResultados = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("resultado_titulo", "id"));
		textoResultados = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("resultado_texto", "id"));
		instruccionesResultados = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("resultado_instrucciones", "id"));
		textoEspecialResultados = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("resultado_texto_especial", "id"));
		menuButton = (ImageButton)findViewById(SuiteAppAdmonApi.getResourceId("resultados_menu_button", "id"));
		//Mejoras Bmovil
		compartirButton = (ImageButton)findViewById(SuiteAppAdmonApi.getResourceId("resultados_compartir", "id"));
	}
	
	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(tituloResultados, true);
		guiTools.scale(textoResultados, true);
		guiTools.scale(instruccionesResultados, true);
		guiTools.scale(textoEspecialResultados, true);
		guiTools.scale(menuButton);
	}
}
