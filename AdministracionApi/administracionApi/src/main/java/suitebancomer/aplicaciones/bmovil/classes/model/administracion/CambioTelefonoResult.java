/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * @author Francisco.Garcia
 * 
 */
public class CambioTelefonoResult implements ParsingHandler {

	private String folio;



	private String ivr;

	public String getFolio() {
		return folio;
	}

	/*
	 *
	 */
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		throw new UnsupportedOperationException(getClass().getName());
	}

	/**
	 * 
	 */
	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		folio = parser.parseNextValue("folioArq");
		ivr=parser.parseNextValue("ivr",false);
	}

	public String getIvr() {
		return ivr;
	}

	public void setIvr(final String ivr) {
		this.ivr = ivr;
	}
}
