package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MenuSuspenderCancelarDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.ListaSeleccionViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class MenuSuspenderCancelarViewController extends BaseViewController {

	LinearLayout contenedorPrincipal;
	ListaSeleccionViewController menuSC;
	MenuSuspenderCancelarDelegate delegate; 
	//AMZ
	private BmovilViewsController parentManager;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,
				SuiteAppAdmonApi.getResourceId("layout_bmovil_menu_suspender_cancelar_admon", "layout"));
		SuiteAppAdmonApi.appContext=this;
		setTitle(R.string.bmovil_suspender_cancelar_title, R.drawable.bmovil_suspender_cancelar_icono);
		//AMZ
		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();	

		
		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((MenuSuspenderCancelarDelegate)parentViewsController.getBaseDelegateForKey(MenuSuspenderCancelarDelegate.MENU_SUSPENDER_CANCELAR_ID));
		delegate = (MenuSuspenderCancelarDelegate) getDelegate();
		delegate.setViewController(this);
		findViews();
		scaleForCurrentScreen();
		initMenu();
	}
	
	/**
	 * 
	 */
	private void findViews() {
		contenedorPrincipal = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("menususpender_principal", "id"));
	}
	
	/**
	 * Escala la vistas en base a la pantalla donde se muestra
	 */
	private void scaleForCurrentScreen(){
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(contenedorPrincipal);
	}
	
	/**
	 * inicializa los items del menu
	 * Suspender
	 * Cancelar
	 */
	@SuppressWarnings("deprecation")
	private void initMenu() {
		final LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		menuSC = new ListaSeleccionViewController(this, params,parentViewsController);
		final ArrayList<Object> cuentasAMostrar = delegate.getListaMenu();
		menuSC.setDelegate(delegate);
		menuSC.setNumeroColumnas(1);
		menuSC.setLista(cuentasAMostrar);
		menuSC.setOpcionSeleccionada(-1);
		menuSC.setSeleccionable(false);
		menuSC.setAlturaFija(false);
		menuSC.setNumeroFilas(cuentasAMostrar.size());
		menuSC.setExisteFiltro(false);
		menuSC.cargarTabla();
		contenedorPrincipal.addView(menuSC);		
	}

	@Override
	protected void onPause() {
		parentViewsController.consumeAccionesDePausa();
		super.onPause();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	public void onBackPressed() {	
		parentViewsController.removeDelegateFromHashMap(MenuSuspenderCancelarDelegate.MENU_SUSPENDER_CANCELAR_ID);
		super.onBackPressed();
	}
	
	/**
	 * handler para obtener la opcion seleccionada e invocar la operacion.
	 * @param selected
	 */
	public void opcionSeleccionada(final String selected){
		if(selected.equals(Constants.SUSPENDER_OP)){
			//AMZ
			final Map<String,Object> Paso1OperacionMap = new HashMap<String, Object>();
			//AMZ
			Paso1OperacionMap.put("evento_paso1","event46");
			Paso1OperacionMap.put("&&products","operaciones;admin+suspender o cancelar");
			Paso1OperacionMap.put("eVar12","paso1:suspender");
			TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
			suspenderSelected();
		}else if(selected.equals(Constants.CANCELAR_OP)){
			//AMZ
			final Map<String,Object> Paso1OperacionMap = new HashMap<String, Object>();
			//AMZ
			Paso1OperacionMap.put("evento_paso1","event46");
			Paso1OperacionMap.put("&&products","operaciones;admin+suspender o cancelar");
			Paso1OperacionMap.put("eVar12","paso1:cancelar");
			TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
			cancelarSelected();
		}
	}
	
	/**
	 * Muestra la pantalla de suspension
	 */
	private void suspenderSelected(){
		//AMZ
				TrackingHelper.trackState("suspender", parentManager.estados);
		delegate.realizaSuspender();
	}
	
	/**
	 * Muestra la pantalla de cancelacion
	 */
	private void cancelarSelected(){
		
		//AMZ
		TrackingHelper.trackState("cancelar", parentManager.estados);
		delegate.realizaCancelar();	
	}
	
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}
	
}
