
/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.Serializable;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;


/**
 * Bean like class that represents a frequent payment.
 * This class is used by FrequentPaymentExtract parser to wrap individually 
 * each service of the response.
 *
 * @author 
 *
 */
public class Payment extends FrecuenteMulticanalData implements Serializable {
	
    /**
     * Operation nickname, assigned by client (NK)
     */
    private String strNK;

    /**
     * Charge account (CC)
     */
    private String strChargeAccount;

    /**
     *  Beneficiary account (CA)
     */
    private String strBeneficiaryAccount;

    /**
     *  Operation amount (IM)
     */
    private String strAmount;

    /**
     *  Beneficiary entity (BF)
     */
    private String strBeneficiary;
    
    /**
     *  Beneficiary entity (BF)
     */
    private String strOperadora;

    /**
     *  Reference (RF)
     */
    private String strReference;

    /**
     *  Concept (CP)
     */
    private String strConcept;

    /**
     *  Description (DE)
     */
    private String strDescription;

    /**
     *  Bank code (CB)
     */
    private String strBankCode;

    /**
     *  Operation code (TO)
     */
    private String strOperationCode;

    /**
     *  Application date (AP)
     */
    private String idNumber;
    
    /**
     * 
     */
    private String indicadorFrecuente;
    
    /**
     * 
     */
    private String idToken;
    
    private boolean esInterbancario;
    /**
     * Default constructor
     */
    public Payment() {
    }

    /**
     * Constructor with parameters
     * @param strNK                 NK
     * @param strChargeAccount      CC
     * @param strBeneficiaryAccount CA
     * @param strAmount             IM
     * @param strBeneficiary        BF
     * @param strReference          RF
     * @param strConcept            CP
     * @param strDescription        DE
     * @param strBankCode           CB
     * @param strOperationCode      TO
     * @param strApplicationDate    AP
     */
    public Payment(final String strNK, final String strChargeAccount, final String strBeneficiaryAccount,
                   final String strAmount, final String strBeneficiary, final String strReference, final String strConcept,
                   final String strDescription, final String strBankCode, final String strOperationCode,
                   final String idNumber) {

        this.strNK = strNK;
        this.strChargeAccount = strChargeAccount;
        this.strBeneficiaryAccount = strBeneficiaryAccount;
        this.strAmount = strAmount;
        this.strBeneficiary = strBeneficiary;
        this.strReference = strReference;
        this.strConcept = strConcept;
        this.strDescription = strDescription;
        this.strBankCode = strBankCode;
        this.strOperationCode = strOperationCode;
        this.idNumber = idNumber;
        esInterbancario =  strOperationCode.equals(Constants.tipoCFOtrosBancos);
        if(strOperationCode.equals(Constants.tipoCFOtrosBBVA ) || strOperationCode.equals(Constants.tipoCFCExpress ) || esInterbancario)
        	this.strBeneficiaryAccount = obtenerCuentaOrigen(strBeneficiaryAccount);
    }

    /**
     * Constructor with parameters
     * @param strNK                 NK
     * @param strChargeAccount      CC
     * @param strBeneficiaryAccount CA
     * @param strAmount             IM
     * @param strBeneficiary        BF
     * @param strOperadora			OA
     * @param strReference          RF
     * @param strConcept            CP
     * @param strDescription        DE
     * @param strBankCode           CB
     * @param strOperationCode      TO
     * @param strApplicationDate    AP
     */
    public Payment(final String strNK, final String strChargeAccount, final String strBeneficiaryAccount,
                   final String strAmount, final String strBeneficiary, final String strReference, final String strConcept,
                   final String strDescription, final String strBankCode, final String strOperationCode,
                   final String idNumber, final String strOperadora) {

        this.strNK = strNK;
        this.strChargeAccount = strChargeAccount;
        this.strBeneficiaryAccount = strBeneficiaryAccount;
        this.strAmount = strAmount;
        this.strBeneficiary = strBeneficiary;
        this.strReference = strReference;
        this.strConcept = strConcept;
        this.strDescription = strDescription;
        this.strBankCode = strBankCode;
        this.strOperationCode = strOperationCode;
        this.idNumber = idNumber;
        this.strOperadora = strOperadora;
        esInterbancario =  strOperationCode.equals(Constants.tipoCFOtrosBancos);
        if(strOperationCode.equals(Constants.tipoCFOtrosBBVA ) || strOperationCode.equals(Constants.tipoCFCExpress ) || esInterbancario)
        	this.strBeneficiaryAccount = obtenerCuentaOrigen(strBeneficiaryAccount);
    }

    /*
     * Returns all the payments in a string
     * For debugging purposes only.
     * This function CAN be deleted
     
    public String toString() {

        String strResult = "";
        strResult+= "NK: " + getNK();
        strResult+= "\nCharge account: " + getChargeAccount();
        strResult+= "\nBenef account: " + getBeneficiaryAccount();
        strResult+= "\nAmount: " + getAmount();
        strResult+= "\nBenef: " + getBeneficiary();
        strResult+= "\nRefer: " + getReference();
        strResult+= "\nConcept: " + getConcept();
        strResult+= "\nDesc: " + getDescription();
        strResult+= "\nBank Code: " + getBankCode();
        strResult+= "\nOp Code: " + getOperationCode();
        strResult+= "\nAp IdNumber: " + getIdNumber();
        strResult+= "\n";
        return strResult;
    }*/

    /* ATTRIBUTE GETTERS */

    public String getNickname() {
        return strNK;
    }

    public String getChargeAccount() {
        return strChargeAccount;
    }

    public String getBeneficiaryAccount() {
        return strBeneficiaryAccount;
    }

    public String getAmount() {
        return strAmount;
    }

    public String getBeneficiary() {
        return strBeneficiary;
    }
    
    public String getOperadora() {
        return strOperadora;
    }

    public String getReference() {
        return strReference;
    }

    public String getConcept() {
        return strConcept;
    }

    public String getDescription() {
        return strDescription;
    }

    public String getBankCode() {
        return strBankCode;
    }

    public String getOperationCode() {
        return strOperationCode;
    }

    public String getIdNumber() {
        return idNumber;
    }

    /* ATTRIBUTE SETTERS */

    public void setNK(final String strNK) {
        this.strNK = strNK;
    }

    public void setChargeAccount(final String strChargeAccount) {
        this.strChargeAccount = strChargeAccount;
    }

    public void setBeneficiaryAccount(final String strBeneficiaryAccount) {
        this.strBeneficiaryAccount = strBeneficiaryAccount;
    }

    public void setAmount(final String strAmount) {
        this.strAmount = strAmount;
    }

    public void setBeneficiary(final String strBeneficiary) {
        this.strBeneficiary = strBeneficiary;
    }
    
    public void setOperadora(final String strOperadora) {
        this.strOperadora = strOperadora;
    }

    public void setReference(final String strReference) {
        this.strReference = strReference;
    }

    public void setConcept(final String strConcept) {
        this.strConcept = strConcept;
    }

    public void setDescription(final String strDescription) {
        this.strDescription = strDescription;
    }

    public void setBankCode(final String strBankCode) {
        this.strBankCode = strBankCode;
    }

    public void setOperationCode(final String strOperationCode) {
        this.strOperationCode = strOperationCode;
    }

    public void setIdNumber(final String idNumber) {
        this.idNumber = idNumber;
    }
    
    public String toString(){
    	return (this.getNickname().trim().length() == 0)
    			? this.getConcept()
    			: this.getNickname().trim();
    }

	/**
	 * @return the indicadorFrecuente
	 */
	public String getIndicadorFrecuente() {
		return indicadorFrecuente;
	}

	/**
	 * @param indicadorFrecuente the indicadorFrecuente to set
	 */
	public void setIndicadorFrecuente(final String indicadorFrecuente) {
		this.indicadorFrecuente = indicadorFrecuente;
	}

	/**
	 * @return the idToken
	 */
	public String getIdToken() {
		return idToken;
	}

	/**
	 * @param idToken the idToken to set
	 */
	public void setIdToken(final String idToken) {
		this.idToken = idToken;
	}

	private String beneficiaryTypeAccount;
	
	public String getBeneficiaryTypeAccount() {
		return beneficiaryTypeAccount;
	}
	
	public void setBeneficiaryTypeAccount(final String beneficiaryTypeAccount) {
		this.beneficiaryTypeAccount = beneficiaryTypeAccount;
	}
	
	protected String obtenerCuentaOrigen(final String cuenta){
		this.beneficiaryTypeAccount = "";
		boolean esTC = false;
	if(cuenta != null){
		if ((cuenta.substring(0, 2).equals("10"))&&(cuenta.length() == 12)){
	           this.beneficiaryTypeAccount = "10";
	           return cuenta.substring(2);
	       }
		if((!Character.isDigit(cuenta.charAt(0)) && !Character.isDigit(cuenta.charAt(1)))
				||(esTC = cuenta.substring(0, 2).equals("20"))){
			if(esInterbancario)
			this.beneficiaryTypeAccount = esTC ? Constants.CREDIT_TYPE : cuenta.substring(0, 2);
			else
				this.beneficiaryTypeAccount = cuenta.substring(0, 2);
			return cuenta.substring(2);
		}
		else
			return cuenta;
	}
	return cuenta;
	}
	
    public Payment(final String strNK, final String strChargeAccount, final String strBeneficiaryAccount,
                   final String strAmount, final String strBeneficiary, final String strReference, final String strConcept,
                   final String strDescription, final String strBankCode, final String strOperationCode) {

        this.strNK = strNK;
        this.strChargeAccount = strChargeAccount;
        this.strBeneficiaryAccount = strBeneficiaryAccount;
        this.strAmount = strAmount;
        this.strBeneficiary = strBeneficiary;
        this.strReference = strReference;
        this.strConcept = strConcept;
        this.strDescription = strDescription;
        this.strBankCode = strBankCode;
        this.strOperationCode = strOperationCode;
        this.idNumber = "";
        this.strOperadora = "";
        esInterbancario =  strOperationCode.equals(Constants.tipoCFOtrosBancos);
        if(strOperationCode.equals(Constants.tipoCFOtrosBBVA ) || strOperationCode.equals(Constants.tipoCFCExpress ) || esInterbancario)
        	this.strBeneficiaryAccount = obtenerCuentaOrigen(strBeneficiaryAccount);
    }
	
	
	
}
