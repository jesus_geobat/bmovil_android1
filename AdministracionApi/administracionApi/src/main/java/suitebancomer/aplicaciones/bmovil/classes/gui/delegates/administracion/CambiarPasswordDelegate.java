package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.util.Log;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.CambiarPasswordResultadoViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.CambiarPasswordViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.CambiarPasswordData;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.delegates.administracion.BaseDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.validatePasswordPolicy1;
import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.validatePasswordPolicy2;

public class CambiarPasswordDelegate extends BaseDelegate {

	CambiarPasswordViewController cambiarPasswordViewController;
	CambiarPasswordResultadoViewController cambiarPasswordResultadoViewController;
	String tituloResultado;
	String mensajeResultado;
	String folioOperacion;
	
	public final static long CAMBIAR_PASSWORD_DELEGATE_ID = 0x1ed434c61ca109b1L;

	public void setCambiarPasswordViewController(final CambiarPasswordViewController cambiarPasswordViewController) {
		this.cambiarPasswordViewController = cambiarPasswordViewController;
	}
	
	
	
	public void setCambiarPasswordResultadoViewController(
			final CambiarPasswordResultadoViewController cambiarPasswordResultadoViewController) {
		this.cambiarPasswordResultadoViewController = cambiarPasswordResultadoViewController;
	}



	public void cambiarPassword(){
		//session context
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		
		//prepare data  
		final Hashtable<String,String> paramTable = new Hashtable<String,String>();
        paramTable.put("NT", session.getUsername());
        paramTable.put("NP", cambiarPasswordViewController.getContrasenaActualText().getText().toString()); 
        paramTable.put("IU", session.getIum());             
        paramTable.put("NN", cambiarPasswordViewController.getContrasenaNuevaText().getText().toString());
		paramTable.put(ServerConstants.PARAMS_TEXTO_EN, Constants.EMPTY_STRING);
		Encripcion.setContext(SuiteAppAdmonApi.appContext);
		final List<String> listaEncriptar3 = Arrays.asList("NP", "NN");
		Encripcion.encriptarPeticionString(paramTable, listaEncriptar3, "*",
				Constants.EMPTY_STRING,"*");
		paramTable.put("order", "NT*NP*IU*NN*EN");
        //prepare to server 
        //JAIG
		doNetworkOperation(Server.CHANGE_PASSWORD_OPERATION, paramTable, false, new CambiarPasswordData(), cambiarPasswordViewController);
        
		
	}
	//1
	public void doNetworkOperation(final int operationId, final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		((BmovilViewsController)cambiarPasswordViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler, caller);
	}
	//3
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			final CambiarPasswordData operacion = (CambiarPasswordData)response.getResponse();
			folioOperacion = operacion.getPage();
			tituloResultado = cambiarPasswordViewController.getString(R.string.administrar_cambiarpassword_operacion_exitosaTitle);
			mensajeResultado = cambiarPasswordViewController.getString(R.string.administrar_cambiarpassword_operacion_exitosaMsg);
			showResultado();
		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			tituloResultado = "";
			mensajeResultado = "";
			cambiarPasswordViewController.showInformationAlert(response.getMessageText());
		}
		
	}
	
	public boolean validaPasswordNuevo(){
		boolean valido = true;
		int msjError = 0;
		final String contrasenaActual = cambiarPasswordViewController.getContrasenaActualText().getText().toString();
		final String contrasenaNueva = cambiarPasswordViewController.getContrasenaNuevaText().getText().toString();
		final String contrasenaConfirmacion= cambiarPasswordViewController.getContrasenaConfirmacion().getText().toString();
	
		//Validar contrasenas
		if(contrasenaActual.length() != Constants.PASSWORD_LENGTH){//contrasena corta 
			valido = false;
			msjError = R.string.changePassword_OldPasswordTooShort;

		}else if(contrasenaNueva.length() != Constants.PASSWORD_LENGTH ){//contrasena corta 
			valido = false;
			msjError = R.string.changePassword_NewPasswordTooShort;

		}else if(contrasenaActual.equals(contrasenaNueva)){//contrasena actual == contrasena nueva
			valido = false;
			msjError = R.string.error_NewPasswordCannotEqualOld;

		}else if(!contrasenaNueva.equals(contrasenaConfirmacion)){//contrasenaactual != confirmacion
			valido = false;
			msjError = R.string.error_BadConfirmationPassword;
		}else if(!validatePasswordPolicy1(contrasenaNueva)){//contrasena nueva numeros repetidos
			valido = false;
			msjError = R.string.changePassword_NewPasswordCannotHaveMultipleRepeatedDigits;
			
		}else if(!validatePasswordPolicy2(contrasenaNueva)){//contrasena nueva num consecutivos
			valido = false;
			msjError = R.string.changePassword_NewPasswordCannotHaveMultipleConsecutiveDigits;
		}else {
			// operar
			if(Server.ALLOW_LOG) Log.d("CambiarPasswordDelegate", "contrasenas VALIDAS!!");			
		}

		if (!valido) {
			cambiarPasswordViewController.showInformationAlert(msjError);
		}
		return valido;
	}
	
    public String getTituloResultado() {
  		return tituloResultado;
	}

	public String getMensajeResultado() {
		return mensajeResultado;
	}

	public String getFolioOperacion() {
		return folioOperacion;
	}
	
	public void showMenu(){
		((BmovilViewsController) cambiarPasswordResultadoViewController.getParentViewsController()).showMenuPrincipal();
	}
	
	void showResultado(){
		((BmovilViewsController)cambiarPasswordViewController.getParentViewsController()).showCambiarPasswordResultado();
	}
    

    
    
}
