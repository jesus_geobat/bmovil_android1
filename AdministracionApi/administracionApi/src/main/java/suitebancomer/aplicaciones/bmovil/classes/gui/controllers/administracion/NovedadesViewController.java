package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MenuAdministrarDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.delegates.administracion.MenuSuiteDelegate;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.LoginDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoTdcDelegate;

public class NovedadesViewController extends BaseViewController {
	
	ScrollView vista;
	LinearLayout novedades1;
	private LinearLayout novedades2;
	LinearLayout novedades3;
	LinearLayout novedades4;
	
	TextView parrafo1;
	private TextView parrafo2;
	TextView parrafo3;
	TextView parrafo4;
	
	/**
	 * 
	 */
	private BmovilViewsController parentManager;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, SuiteAppAdmonApi.getResourceId("activity_novedades_view_controller_admon", "layout"));
		SuiteApp.appContext = this;
		setTitle(R.string.novedades_novedades_title, R.drawable.an_ic_aviso);

		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("novedades", parentManager.estados);
		
		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());

		if(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().getLastDelegateKey() != MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID){
			MenuSuiteDelegate delegate = (MenuSuiteDelegate)SuiteAppAdmonApi.getInstance().getSuiteViewsController().getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
			if (delegate == null) {
				delegate = new MenuSuiteDelegate();
			}
			setDelegate(delegate);
		}else{
			setDelegate((MenuAdministrarDelegate)parentViewsController.getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID));
			
		}
		
		init();
	}
	
	
	public void init(){
		findViews();
		scaleForCurrentScreen();	
	}
	
	
	private void findViews() {
		vista = (ScrollView)findViewById(SuiteAppAdmonApi.getResourceId("novelties_scroll", "id"));
		novedades1 = (LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("novelties_1_layout", "id"));
		//novedades2 = (LinearLayout)findViewById(R.id.novelties_2_layout);
		novedades3 = (LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("novelties_3_layout", "id"));
		novedades4 = (LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("novelties_4_layout", "id"));
		
		parrafo1 = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("paragraph_1_label", "id"));
		//parrafo2 = (TextView)findViewById(R.id.paragraph_2_label);
		parrafo3 = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("paragraph_3_label", "id"));
		parrafo4 = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("paragraph_4_label", "id"));
	}
	
	
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("novelties_layout", "id")));
		
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("novelties_1_layout", "id")));
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("paragraph_1_label", "id")), true);
		
		//guiTools.scale(findViewById(R.id.novelties_2_layout));
		//guiTools.scale(findViewById(R.id.paragraph_2_label), true);
		
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("novelties_3_layout", "id")));
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("paragraph_3_label", "id")), true);
		
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("novelties_4_layout", "id")));
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("paragraph_4_label", "id")), true);

	}

	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getLastDelegateKey() != MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID){

		    if (keyCode == KeyEvent.KEYCODE_BACK ) {
		    	
		    	MenuSuiteDelegate delegate = (MenuSuiteDelegate)SuiteApp.getInstance().getSuiteViewsController().getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
				if (delegate == null) {
					delegate = new MenuSuiteDelegate();
					parentViewsController.addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, delegate);
				}
				delegate.getMenuSuiteViewController().setIsFlipPerforming(false);
				delegate.getMenuSuiteViewController().setComesFromNov(true);
				
		    }
		    
		}
	    return super.onKeyDown(keyCode, event);
	}*/
	
	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		super.goBack();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		getParentViewsController().setCurrentActivityApp(this);
	}

}
