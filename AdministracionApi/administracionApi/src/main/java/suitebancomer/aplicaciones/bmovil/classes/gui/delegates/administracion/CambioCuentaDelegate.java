package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.content.res.Resources;
import android.util.Log;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.CambioCuentaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.CambioCuentaResult;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.hideAccountNumber;

public class CambioCuentaDelegate extends DelegateBaseAutenticacion{

	/**
	 * 
	 */
	public static final long CAMBIO_CUENTA_DELEGATE_ID = 6392075930222254965L;
	private CambioCuentaViewController viewController;
	//private String folio;
	
	/*
	 * Componentes de autenticacion
	 */
	
	/**
	 * @return true si se debe mostrar contrasena, false en caso contrario.
	 */
	@Override
	public boolean mostrarContrasenia() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		//boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.cambioCuenta,
		final boolean value = Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.cambioCuenta,
				perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.cambioCuenta, perfil);
		return value;
	}
	
	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.cambioCuenta,
				perfil);
		return value;
	}
	
	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.cambioCuenta,
									perfil);
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}		
		return tipoOTP;
	}

	public CambioCuentaViewController getViewController() {
		return viewController;
	}

	public void setViewController(final CambioCuentaViewController viewController) {
		this.viewController = viewController;
	}
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		final ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		final Resources res = viewController.getResources();
		final Account selectedAccount = viewController.getCambioCuenta().getAccount();
		fila = new ArrayList<String>();
		fila.add(selectedAccount.getNombreTipoCuenta(SuiteAppAdmonApi.appContext.getResources()));
		fila.add(hideAccountNumber(selectedAccount.getNumber()));
		tabla.add(fila);		
		return tabla;
	}
	
	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		final ArrayList<Object> tabla = getDatosTablaConfirmacion(); //new ArrayList<Object>();
//		ArrayList<String> fila;
//		
//		Resources res = viewController.getResources();
//		String cuentaNueva = viewController.getCambioCuenta().getAccount().getPublicName(res, true);	
//		fila = new ArrayList<String>();
//		fila.add(viewController.getString(R.string.accounttype_check));
//		fila.add(cuentaNueva);
//		tabla.add(fila);
//		fila = new ArrayList<String>();
//		fila.add(viewController.getString(R.string.result_page));
//		fila.add(folio);
//		tabla.add(fila);		
//		
		return tabla;
	}

	/**
	 * Invoca la operacion para hacer el cambio de cuenta asociada
	 */
	@Override
	public void realizaOperacion(final ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {

		final int operationId = Server.CAMBIO_CUENTA;//OP Cambio de cuenta
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Autenticacion aut = Autenticacion.getInstance();
		final Perfil perfil = session.getClientProfile();

		final String companiaCelular = session.getCompaniaUsuario();
		final String cuentaAnterior = Tools.obtenerCuentaEje().getFullNumber();
		final String cuentaNueva = viewController.getCambioCuenta().getAccount().getFullNumber();
		final String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.cambioCuenta, perfil);

		final Hashtable<String, String> params = new Hashtable<String, String>();
		
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put("numeroCliente", session.getClientNumber());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		params.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);		
		params.put(ServerConstants.COMPANIA_CELULAR, companiaCelular);
		params.put(Server.J_CUENTA_A, cuentaAnterior);
		params.put(Server.J_CUENTA_N, cuentaNueva);		
		params.put(Server.J_NIP, nip == null ? "":nip);
		params.put(Server.J_CVV2, cvv== null ? "":cvv);
		params.put(ServerConstants.CODIGO_OTP, token == null ? "":token);
		params.put(Server.J_AUT, cadAutenticacion);
		params.put("tarjeta5Dig", campoTarjeta == null ? "" : campoTarjeta);
		
		params.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
		final List<String> listaEncriptar = Arrays.asList(ServerConstants.CVE_ACCESO,
				Server.J_NIP, Server.J_CVV2);
		Encripcion.setContext(SuiteAppAdmonApi.appContext);
		Encripcion.encriptaCadenaAutenticacion(params, listaEncriptar);
		
		//JAIG
		doNetworkOperation(operationId, params,true, new CambioCuentaResult(), confirmacionAutenticacionViewController);
		//		doNetworkOperation(operationId, params, confirmacionAutenticacionViewController);
	}
	
	@Override
	public void doNetworkOperation(final int operationId,	final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler, caller);
	}
	
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
			//cambio de cuenta exitoso
			if(response.getResponse() instanceof CambioCuentaResult){
				final CambioCuentaResult result = (CambioCuentaResult) response.getResponse();
//				folio = result.getFolio();
				actualizarCuentas(result.getAsuntos());
				showResultados();
			}			
		}else if(response.getStatus() == ServerResponse.OPERATION_WARNING){
			//cambio de cuenta no exitoso
			//viewController.showInformationAlert(response.getMessageText());
			final BaseViewControllerCommons current = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp();
			current.showInformationAlert(response.getMessageText());
		}
		
	}
	
	/**
	 * Actualiza las cuentas almacenadas en session 
	 * con las cuentas recibidas al cambiar la cuenta asociada.
	 * 
	 * @param accounts
	 */
	private void actualizarCuentas(final Account[] accounts){
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		session.updateAccounts(accounts);
	}
	
	/**
	 * Muestra la pantalla de resultados.
	 */	
	private void showResultados(){
		SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController()
		.showResultadosViewController(this, -1, -1);
	}
	
	/**
	 * Muestra la pantalla de confirmacion
	 */	
	public void showConfirmacion(){
		final int resSubtitle = R.string.confirmation_subtitulo;
		final int resTitleColor = R.color.primer_azul;
		final int resIcon = R.drawable.bmovil_cambiocuenta_icono;
		final int resTitle = R.string.bmovil_cambio_cuenta_title;
		SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(this, resIcon, resTitle, resSubtitle, resTitleColor);
	}
	
	/**
	 * Sin opciones del menu
	 */
	@Override
	public int getOpcionesMenuResultados() {
		return 0;
	}
	
	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen.
	 * @return ArrayList<Account> con las cuentas del usuario
	 */
	public ArrayList<Object> getCuentasUsuario() {
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Account[] accounts = session.getAccounts();
		final ArrayList<Object> accountsArray = new ArrayList<Object>(accounts.length);
		final Resources res = viewController.getResources();
		ArrayList<Object> item;
		for (final Account acc : accounts) {
			if (!acc.getType().equals(Constants.INVERSION_TYPE)) {
				item = new ArrayList<Object>(2);
				item.add(acc);
//			if (Constants.EXPRESS_TYPE.equals(acc.getType()))
//				item.add("Cuenta express");
//			else
				item.add(acc.getNombreTipoCuenta(SuiteAppAdmonApi.appContext.getResources()));
//			StringBuilder sb = new StringBuilder(acc.getCurrency());
//			sb.append(" ");
//			sb.append(Tools.hideAccountNumber(acc.getNumber()));
//			item.add(sb.toString());
			item.add(Tools.hideAccountNumber(acc.getNumber()));
			if (acc.isVisible()) {
				accountsArray.add(0, item);
			} else {
				accountsArray.add(item);
			}
		}
	}
		return accountsArray;
	}
	
	/**
	 * Handler de lista seleccion para obtener 
	 * la cuenta seleccionada por el usuario
	 */
	 public void performAction(final Object obj) {
	    	if(viewController != null)
	    	viewController.actualizarCuenta(obj);	    	
	 }
	 
	 
	 @Override
	public int getTextoEncabezado() {
		 final int resTitle = R.string.bmovil_cambio_cuenta_title;
		return resTitle;
	}
	 
	 
	 @Override
	public int getNombreImagenEncabezado() {
		 final int resIcon = R.drawable.bmovil_cambiocuenta_icono;
		 return resIcon;
	}
	 
	 @Override
	public String getTextoPantallaResultados() {
		 final String text = viewController.getString(R.string.bmovil_cambio_cuenta_texto_resultados);
		return text;
	}

	@Override
	public String getTextoTituloResultado() {
		return viewController.getString(R.string.transferir_detalle_operacion_exitosaTitle);
	}

	@Override
	public int getColorTituloResultado() {
		return R.color.verde_limon;
	}
	 
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}
}
