package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;
import com.bancomer.mbanking.softtoken.SuiteAppApi;
import com.tutorialbbvasend.clases.TutorialBBVAView;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.timer.TimerController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ContratacionAutenticacionDelegate;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.AutenticacionSTViewController;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.FirmaAutografaCallback;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.delegates.FirmaDelegate;

/**
 * BMovil_v940
 * Created by terry0022 on 15/08/16 - 1:54 PM.
 */
public class FirmaAutografaViewController extends BaseViewController implements View.OnClickListener {

    private TextView aceptar_terminos_label,aceptar_terminos_link,firmar_title;
    private CheckBox acepto_terminos_checkbox;
    private Button continuar;
    //    private ImageView ic_firma;
    public static final Integer SIGNATURE = 147;
    /* componente firma */
    FirmaDelegate firma = new FirmaDelegate(this);

    /* delegate */
    private CambioPerfilDelegate cambioPerfilDelegate;
    public boolean flujoLogin = false;
    private String base64 = "";

    private ImageButton help_firma_autografa;
    private TutorialBBVAView tutorial;
    private RelativeLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_firma_view_controller);

        setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());

        SuiteApp.appContext = this;
        SuiteAppAdmonApi.appContext=this;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            flujoLogin = (Boolean) bundle.get("flujoLogin");
        }

        cambioPerfilDelegate = (CambioPerfilDelegate)getParentViewsController().getBaseDelegateForKey(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
        cambioPerfilDelegate.setFirmaAutografaController(this);

        aceptar_terminos_label = (TextView)findViewById(R.id.aceptar_terminos_label);
        aceptar_terminos_link = (TextView)findViewById(R.id.aceptar_terminos_link);
        firmar_title = (TextView)findViewById(R.id.firmar_title);
        acepto_terminos_checkbox = (CheckBox)findViewById(R.id.acepto_terminos_checkbox);
        continuar = (Button)findViewById(R.id.continuar);
        LinearLayout firmar_layout = (LinearLayout) findViewById(R.id.firmar_layout);

        firma.generateViewSignature(firmar_layout,SIGNATURE);

        scaleToScreenSize();
        firmar_title.setOnClickListener(this);
        aceptar_terminos_link.setOnClickListener(this);
        continuar.setOnClickListener(this);

        CambioPerfilDelegate.cambioPerfilViewController = this;
        cambioPerfilDelegate.setNuevoPerfil(null);

        rootLayout = (RelativeLayout) findViewById(R.id.master_layout);
        help_firma_autografa = (ImageButton) findViewById(R.id.ayuda_clave_activacion);

        tutorial = new TutorialBBVAView(this, rootLayout);


        help_firma_autografa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tutorial.initTutorial(
                        new int[]{R.id.firmar_title, R.id.firmar_layout}, R.id.firmar_layout,
                        "",
                        "Toca dentro del recuadro\npara que puedas ingresar tu firma.",
                        R.id.header_layout, 0,
                        false, 4, true);
            }
        });

    }

    /**
     * Escala el layout al tamaño de la pantalla.
     */
    private void scaleToScreenSize() {
        final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(aceptar_terminos_link, true);
        guiTools.scale(aceptar_terminos_label, true);
        guiTools.scale(firmar_title, true);
        guiTools.scale(continuar, true);
    }

    @Override
    public void onClick(View v) {
        if (v == continuar){
            if (!acepto_terminos_checkbox.isChecked()){
                showMensajeErrorTerminos(getString(R.string.contratacion_autenticacion_aceptar_terminos_y_condiciones_falta));
                return;
            }
            if (base64.equals("")){
                showMensajeErrorTerminos(getString(R.string.contratacion_autenticacion_aceptar_firma_falta));
                return;
            }
            cambioPerfilDelegate.peticionFirma(base64);
        }else if(v == aceptar_terminos_link){
            onVerTerminosLinkClik(v);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            FirmaAutografaCallback.getInstance().setCallBackSession(null);
            FirmaAutografaCallback.getInstance().setCallBackBConnect(null);
            setResult(RESULT_CANCELED, new Intent());
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == SIGNATURE){
                base64 = firma.returnViewSignature(data.getExtras());
                Log.i("TAG", "onActivityResult: " + base64);
            }
        }
    }

    @Override
    public void onUserInteraction() {
        if(ServerCommons.ALLOW_LOG) {
            Log.i(getClass().getSimpleName(), "User Interacted firma Autografa");
        }
        if(FirmaAutografaCallback.getInstance().getCallBackSession() != null) {
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Envia peticion de reinicio a bmovil desde CallBackSession");
            }
            TimerController.getInstance().resetTimer();
        }
        if( FirmaAutografaCallback.getInstance().getCallBackBConnect() != null) {
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Envia peticion de reinicio a bmovil desde CallBackBconnect");
            }
            TimerController.getInstance().resetTimer();
        }
        //super.onUserInteraction();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SuiteApp.appContext = this;
        SuiteAppAdmonApi.appContext = this;
        getParentViewsController().setCurrentActivityApp(this);
        cambioPerfilDelegate.setBaseViewController(this);
        cambioPerfilDelegate.setOwnerController(this);
    }

    /*
	 * (non-Javadoc)
	 *
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#goBack()
	 */
    @Override
    public void goBack() {
        // Solo cuando el perfil del usuario es avanzado se le permite volver a la pantalla anterior
        if (Constants.Perfil.avanzado.equals(Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile())) {
            cambioPerfilDelegate.cambioPerfilCancelado();
        }
        if(!cambioPerfilDelegate.isCambioPerfilLogin()){
            super.goBack();
        }
    }

    /*
	 * (non-Javadoc)
	 *
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#
	 * processNetworkResponse(int,
	 * suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
    @Override
    public void processNetworkResponse(final int operationId, final ServerResponse response) {
        cambioPerfilDelegate.analyzeResponse(operationId, response);
    }

    public void aceptarCambioPerfil() {
        if (acepto_terminos_checkbox.isChecked()) {
            cambioPerfilDelegate.cambioPerfilAceptado();
        } else {
            showMensajeErrorTerminos(getString(R.string.contratacion_autenticacion_aceptar_terminos_y_condiciones_falta));
        }
    }

    public void showMensajeErrorTerminos(String message){
        showInformationAlert(message);
    }

    /**
     * Muestra los terminos y condiciones.
     */
    public void onVerTerminosLinkClik(final View sender) {
        final ContratacionAutenticacionDelegate delegate = new ContratacionAutenticacionDelegate(cambioPerfilDelegate);
        getParentViewsController().addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID,delegate);
        final ContratacionAutenticacionViewController caller = new ContratacionAutenticacionViewController();
        caller.setDelegate(delegate);
        caller.setParentViewsController(((BmovilViewsController) SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController()));
        delegate.setcontratacionAutenticacionViewController(caller);
        delegate.consultarTerminosDeUso();

        if(Server.ALLOW_LOG)
            Log.d(this.getClass().getSimpleName(), "Ver terminos y condiciones.");
    }


}
