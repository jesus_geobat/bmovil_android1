package suitebancomer.aplicaciones.bmovil.classes.model.administracion;


import bancomer.api.common.commons.Constants;

public class Desbloqueo {

	private String contrasenaNueva;
	private Constants.Perfil perfilCliente;
	private String numCelular;
	/**
	 * @return the contrasenaNueva
	 */
	public String getContrasenaNueva() {
		return contrasenaNueva;
	}
	/**
	 * @param contrasenaNueva the contrasenaNueva to set
	 */
	public void setContrasenaNueva(final String contrasenaNueva) {
		this.contrasenaNueva = contrasenaNueva;
	}
	/**
	 * @return the perfilCliente
	 */
	public Constants.Perfil getPerfilCliente() {
		return perfilCliente;
	}
	/**
	 * @param perfilCliente the perfilCliente to set
	 */
	public void setPerfilCliente(final Constants.Perfil perfilCliente) {
		this.perfilCliente = perfilCliente;
	}
	/**
	 * @return the numCelular
	 */
	public String getNumCelular() {
		return numCelular;
	}
	/**
	 * @param numCelular the numCelular to set
	 */
	public void setNumCelular(final String numCelular) {
		this.numCelular = numCelular;
	}
}
