package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class CambiarLimitesResult implements ParsingHandler {
	
	private String folio;

	/**
	 * 
	 * @return Folio de la arquitectura que devuelve la trasacci�n
	 */
	public String getFolio() {
		return folio;
	}

	/*
	 *
	 */
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		throw new UnsupportedOperationException(getClass().getName());
	}

	/**
	 * 
	 */
	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		folio = parser.parseNextValue("folioArq");
	}


}
