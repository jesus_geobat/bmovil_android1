package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ActivacionResult implements ParsingHandler {

	private String fecha;
	private String hora;
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		fecha = parser.parseNextValue("FE");
		hora = parser.parseNextValue("HR");

	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		

	}
	
	public String getFecha() {
		return fecha;
	}
	
	public String getHora() {
		return hora;
	}

}
