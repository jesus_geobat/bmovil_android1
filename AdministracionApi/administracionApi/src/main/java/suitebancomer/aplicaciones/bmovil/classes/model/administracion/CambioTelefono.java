package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

public class CambioTelefono {
	
	private String nuevoTelefono;
	private String nombreCompania;
	
	public String getNuevoTelefono() {
		return nuevoTelefono;
	}
	
	public void setNuevoTelefono(final String nuevoTelefono) {
		this.nuevoTelefono = nuevoTelefono;
	}
	
	public String getNombreCompania() {
		return nombreCompania;
	}
	
	public void setNombreCompania(final String nombreCompania) {
		this.nombreCompania = nombreCompania;
	}

}
