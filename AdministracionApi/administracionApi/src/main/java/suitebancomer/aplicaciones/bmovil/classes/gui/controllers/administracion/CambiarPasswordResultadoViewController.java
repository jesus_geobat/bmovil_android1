package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambiarPasswordDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.ListaDatosViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;


public class CambiarPasswordResultadoViewController extends BaseViewController implements OnClickListener {

	CambiarPasswordDelegate cambiarPasswordDelegate;
	
	
	TextView operacionExitosa;
	TextView msgResultadoLabel;
	LinearLayout layoutFolio;
	ListaDatosViewController folioOperacion;
	ImageButton botonMenu;
	//AMZ
			private BmovilViewsController parentManager;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, getResources().getIdentifier("layout_bmovil_cambiar_password_resultado_admon", "layout", getPackageName()));
		SuiteApp.appContext = this;
		setTitle(R.string.administrar_menu_cambiar_contrasena_titulo, R.drawable.bmovil_cambiar_password_icono);
		//AMZ
		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("resul",parentManager.estados);
		//TrackingHelper.trackState("resul");
		
		
		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		
		setDelegate((CambiarPasswordDelegate)parentViewsController.getBaseDelegateForKey(CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID));
		cambiarPasswordDelegate = (CambiarPasswordDelegate)getDelegate(); 
		cambiarPasswordDelegate.setCambiarPasswordResultadoViewController(this);	
		operacionExitosa = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("cambiar_password_resultado_operacion_exitosa_label", "id"));
		layoutFolio = (LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("cambiar_password_resultado_folio_op_layout", "id"));
		msgResultadoLabel = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("cambiar_password_resultado_mensaje_label", "id"));
		botonMenu = (ImageButton)findViewById(SuiteAppAdmonApi.getResourceId("cambio_contrasena_resultado_boton_menu", "id"));
		botonMenu.setOnClickListener(this);
		configurarPantalla();
		muestraResultado();
	}
	
	private void configurarPantalla(){
		//msgResultadoLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
		//operacionExitosa.setTextSize(TypedValue.COMPLEX_UNIT_PX, 18.0f);
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		//Edittext
		gTools.scale(msgResultadoLabel,true);
		gTools.scale(operacionExitosa,true);
		//Button
		gTools.scale(botonMenu);
	}
	
	/*
	 * Estos de cajon! :|
	 */
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void goBack() {
	//Inhabilitar Back
		if(Server.ALLOW_LOG) Log.d(getLocalClassName(), "goBack disable");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		parentViewsController.removeDelegateFromHashMap(CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID);
		super.onDestroy();
	}
	
	
	@Override
	public void onClick(final View v) {
		// TODO Auto-generated method stub
		if(v.equals(botonMenu)){
			botonMenuClick();
		}
	}
	
	void muestraResultado(){
		operacionExitosa.setText(cambiarPasswordDelegate.getTituloResultado());
		ArrayList<Object> registros;
		final ArrayList<Object> lista = new ArrayList<Object>();

		registros = new ArrayList<Object>();
		registros.add(getString(R.string.administrar_cambiarpassword_folioOperacionLabel));
		registros.add(cambiarPasswordDelegate.getFolioOperacion());
		lista.add(registros);
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		@SuppressWarnings("deprecation")
		final LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		//params.leftMargin 	= GuiTools.getCurrent().getEquivalenceInPixels(getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin));
		//params.rightMargin 	= GuiTools.getCurrent().getEquivalenceInPixels(getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin));
	

		if (folioOperacion == null) {
			folioOperacion = new ListaDatosViewController(this, params, parentViewsController);
			folioOperacion.setNumeroCeldas(2);
			folioOperacion.setLista(lista);
			folioOperacion.setNumeroFilas(lista.size());
			folioOperacion.showLista();
			layoutFolio.addView(folioOperacion);
		}
		msgResultadoLabel.setText(cambiarPasswordDelegate.getMensajeResultado());
		
	}

	void botonMenuClick(){
		//AMZ
				((BmovilViewsController)parentViewsController).touchMenu();
		cambiarPasswordDelegate.showMenu();
	}
}
