package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.SessionStoredListener;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

/**
 * BMovil_v940
 * Created by terry0022 on 13/07/16 - 6:29 PM.
 */
public class ActivationPerfilDelegate extends DelegateBaseAutenticacion implements SessionStoredListener {

    private static final String TAG = "ActivationPerfilDlgt";
    private BaseViewController activacionPerfil;

    public ActivationPerfilDelegate(BaseViewController activacionPerfil){
        this.activacionPerfil = activacionPerfil;
    }

    public void enviarPeticion(String folioSMS){
        Session session=Session.getInstance(SuiteAppAdmonApi.appContext);
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
        paramTable.put(ServerConstants.FOLIO_SMS, folioSMS);
        if(ServerCommons.ARQSERVICE){
            paramTable.put(Constants.IUM_STRING,  session.getIum());
        }
        //JAIG
        SuiteAppAdmonApi.getInstance().getBmovilApplication().invokeNetworkOperation(Server.VALIDA_FOLIO_CAMBIO_PERFIL, paramTable, true, null, activacionPerfil);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if( response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {

            if (operationId == Server.VALIDA_FOLIO_CAMBIO_PERFIL){
                mostrarContratacion();
            }

            if (operationId == Server.ENVIO_SMS_PERFIL){
                if(Server.ALLOW_LOG)
                    Log.i(TAG, "analyzeResponse: ok");
            }

        }
    }

    public void mostrarContratacion() {
        //se limpia preferences despues de activar el cp
        SharedPreferences settings= activacionPerfil.getSharedPreferences(bancomer.api.common.commons.Constants.SHARED_PREFERENCES, activacionPerfil.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();

        CambioPerfilDelegate perfilDelegate = (CambioPerfilDelegate)SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().getBaseDelegateForKey(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
        SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().showContratacionAutenticacion(perfilDelegate);
    }

    @Override
    public void sessionStored() {

    }

    public void confirmar(EditText editText){
        if (validateCampo(editText))
            enviarPeticion(editText.getText().toString());
        else{
            activacionPerfil.showInformationAlert(activacionPerfil.getString(R.string.bmovil_activacion_validacioncve));
        }
    }

    private boolean validateCampo(EditText editText){
        return !TextUtils.isEmpty(editText.getText().toString());
    }

    /**
     * El usuario acepta el cambio de perfil.
     */
    public void cambioPerfilAceptado() {
        final Session session=Session.getInstance(SuiteAppAdmonApi.appContext);
        if (Constants.Perfil.basico.equals(session.getClientProfile())){
            Hashtable<String, String> paramTable = new Hashtable<String, String>();
            paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
            paramTable.put(ServerConstants.OS, ServerConstants.ANDROID);
            if(ServerCommons.ARQSERVICE){
                paramTable.put(Constants.IUM_STRING,  session.getIum());
            }
            SuiteAppAdmonApi.getInstance().getBmovilApplication().invokeNetworkOperation(Server.ENVIO_SMS_PERFIL, paramTable, true, null, activacionPerfil);
        }else{
            mostrarContratacion();
        }
    }

}
