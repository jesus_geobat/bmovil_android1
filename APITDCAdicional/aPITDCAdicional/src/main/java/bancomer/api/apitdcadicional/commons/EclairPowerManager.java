/**
 * 
 */
package bancomer.api.apitdcadicional.commons;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

import bancomer.api.apitdcadicional.implementations.InitTDCAdicional;

/**
 * Implementation of the App power manager for api7 or greater.
 */
public final class EclairPowerManager extends AbstractSuitePowerManager {
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.common.AbstractSuitePowerManager#isScreenOn()
	 */
	@Override
	public boolean isScreenOn() {
		try {
			PowerManager pm = (PowerManager) InitTDCAdicional.getInstance().getBaseViewController().getActivity().getSystemService(InitTDCAdicional.getInstance().getBaseViewController().getActivity().POWER_SERVICE);
			return pm.isScreenOn();
		}catch(Exception ex) {
			Log.wtf("EclairPowerManager", "Error while getting the state of the screen.", ex);
			return false;
		}
	}

}
