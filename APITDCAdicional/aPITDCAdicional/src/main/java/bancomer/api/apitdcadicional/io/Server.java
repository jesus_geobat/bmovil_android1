/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package bancomer.api.apitdcadicional.io;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import bancomer.api.apitdcadicional.commons.ConstantsTDCAdicional;
import bancomer.api.apitdcadicional.models.AceptaTDCAdicional;
import bancomer.api.apitdcadicional.models.OfertaTDCAdicional;
import bancomer.api.common.commons.NameValuePair;
import bancomer.api.common.io.ParsingException;


/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receives the network operation
 * requests and builds a network operation suitable for ClienteHttp class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponseImpl object, so that the application logic can
 * perform high level checks and processes.
 * 
 * @author GoNet.
 */

public class Server {

	/**
	 * Defines if the application show logs
	 */
	public static boolean ALLOW_LOG = true;

	/**
	 * Defines if the application is running on the emulator or in the device.
	 */
	public static boolean EMULATOR = false;

	/**
	 * Indicates simulation usage.
	 */
	public static boolean SIMULATION = false;

	/**
	 * Indicates if the base URL points to development server or production
	 */
	public static boolean DEVELOPMENT = false;
	// public static boolean DEVELOPMENT = true; //TODO remove for TEST

	/**
	 * Indicates simulation usage.
	 */
	public static final long SIMULATION_RESPONSE_TIME = 5;

	/**
	 * Bancomer Infrastructure.
	 */
	public static final int BANCOMER = 0;

	public static int PROVIDER = BANCOMER; // TODO remove for TEST


	// ///////////////////////////////////////////////////////////////////////////
	// Operations identifiers //
	// ///////////////////////////////////////////////////////////////////////////


    /** oneClickTDCAdicional**/

    public static final int CONSULTA_DETALLE_TDC_ADICIONAL = 136;

    public static final int ACEPTACION_DETALLE_TDC_ADICIONAL = 137;

    public static final String JSON_OPERATION_VENTA_TDC = "VTDC001";
	
	/**
	 * Operation codes.
	 */
	public static final String[] OPERATION_CODES = { "00","oneClicTDCAdicional","oneClicTDCAdicional"};


	//one Click
	/**
	 * clase de enum para cambio de url;
	 */
	public enum isJsonValueCode{
		ONECLICK, NONE,CONSUMO,DEPOSITOS,PAPERLESS;
	}

	public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
	//Termina One CLick

	/**
	 * Base URL for providers.
	 */
	public static String[] BASE_URL;

	/**
	 * Path table for provider/operation URLs.
	 */
	public static String[][] OPERATIONS_PATH;

	/**
	 * Operation URLs map.
	 */
	public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

	/**
	 * Operation providers map.
	 */
	public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";


	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";



	public static void setEnviroment() {
		if (DEVELOPMENT) {
			setupDevelopmentServer();
		} else {
			setupProductionServer();
		}

		setupOperations(PROVIDER);
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupProductionServer() {
		BASE_URL = new String[] { "https://www.bancomermovil.com",
				// "https://172.17.100.173",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Produccion"
						: "" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "",
                        "/mbhxp_mx_web/servlet/ServletOperacionWeb",  // Detalle Venta TDC Adicional
                        "/mbhxp_mx_web/servlet/ServletOperacionWeb"  //  Aceptación Venta TDC Adicional
				},
				new String[] { "",
                        "/mbhxp_mx_web/servlet/ServletOperacionWeb",  // Detalle Venta TDC Adicional
                        "/mbhxp_mx_web/servlet/ServletOperacionWeb"  //  Aceptación Venta TDC Adicional

				} };
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupDevelopmentServer() {
		BASE_URL = new String[] {
				"https://www.bancomermovil.net:11443",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Desarrollo"
						: "http://189.254.77.54:8080/servermobile/Servidor/Desarrollo" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "",
                        "/eexd_mx_web/servlet/ServletOperacionWeb", //Detalle Venta TDC Adicional
                        "/eexd_mx_web/servlet/ServletOperacionWeb" //Aceptación Venta TDC Adicional
				},
				new String[] { "",
                        "/eexd_mx_web/servlet/ServletOperacionWeb", //Detalle Venta TDC Adicional
                        "/eexd_mx_web/servlet/ServletOperacionWeb" //Aceptación Venta TDC Adicional

				} };
	}

	/**
	 * Setup operations for a provider.
	 * 
	 * @param provider
	 *            the provider
	 */
	private static void setupOperations(int provider) {

        setupOperation(CONSULTA_DETALLE_TDC_ADICIONAL, provider);
        setupOperation(ACEPTACION_DETALLE_TDC_ADICIONAL, provider);

	}

	/**
	 * Setup operation for a provider.
	 * @param operation the operation
	 * @param provider the provider
	 */
	private static void setupOperation(int operation, int provider) {
		String code = OPERATION_CODES[operation];
		OPERATIONS_MAP.put(
				code,
				new StringBuffer(BASE_URL[provider]).append(
						OPERATIONS_PATH[provider][operation]).toString());
		OPERATIONS_PROVIDER.put(code, new Integer(provider));
	}

	/**
	 * Get the operation URL.
	 * @param operation the operation id
	 * @return the operation URL
	 */
	public static String getOperationUrl(String operation) {
		String url = (String) OPERATIONS_MAP.get(operation);
		return url;
	}

	/**
	 * Referencia a ClienteHttp, en lugar de HttpInvoker.
	 */
	private ClienteHttp clienteHttp = null;

	
	// protected Context mContext;

	public ClienteHttp getClienteHttp() {
		return clienteHttp;
	}

	public void setClienteHttp(ClienteHttp clienteHttp) {
		this.clienteHttp = clienteHttp;
	}


	
	public Server() {
		clienteHttp = new ClienteHttp();
	}

	/**
	 * perform the network operation identifier by operationId with the
	 * parameters passed.
	 * @param operationId the identifier of the operation
	 * @param params Hashtable with the parameters as key-value pairs.
	 * @return the response from the server
	 * @throws IOException exception
	 * @throws ParsingException exception
	 */
	public ServerResponseImpl doNetworkOperation(int operationId, Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = null;
		long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
		Integer provider = (Integer) OPERATIONS_PROVIDER.get(OPERATION_CODES[operationId]);
		if (provider != null) {
			PROVIDER = provider.intValue();
		}

		switch (operationId) {
			case CONSULTA_DETALLE_TDC_ADICIONAL:
				response = consultaDetalleOfertaTDCAdicional(params);
				break;
			case ACEPTACION_DETALLE_TDC_ADICIONAL:
                response= aceptaOfertaTDCAdicional(params);
				break;
		}

		if (sleep > 0) {
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
			}
		}
		return response;
	}



	private ServerResponseImpl consultaDetalleOfertaTDCAdicional(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		OfertaTDCAdicional data=new OfertaTDCAdicional();
		ServerResponseImpl response = new ServerResponseImpl(data);

		NameValuePair[] parameters = new NameValuePair[4];
		parameters[0] = new NameValuePair( ConstantsTDCAdicional.NUMERO_CELULAR_TAG,(String)params.get(ConstantsTDCAdicional.NUMERO_CELULAR_TAG));
		parameters[1] = new NameValuePair( ConstantsTDCAdicional.CVE_CAMP_TAG, (String)params.get(ConstantsTDCAdicional.CVE_CAMP_TAG));
		parameters[2] = new NameValuePair( ConstantsTDCAdicional.IUM_TAG,(String)params.get(ConstantsTDCAdicional.IUM_TAG));
		parameters[3] = new NameValuePair( ConstantsTDCAdicional.OPCION_TAG,(String)params.get(ConstantsTDCAdicional.OPCION_TAG));


		//isjsonvalueCode=isJsonValueCode.VENTATDC;
		if (SIMULATION) {
			//Error Response
			//this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DETALLE_TDC_ADICIONAL],parameters,response,"{\"estado\": \"ERROR\",\"codigoMensaje\": \"Codigo del Mensaje\",\"descripcionMensaje\": \"SERVICIO FUERA DE SERVICIO\"}", true);
			this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DETALLE_TDC_ADICIONAL], parameters, response, "{\"estado\":\"OK\",\"lineaCredito\":\"1000000\",\"producto\":\"AP\",\"desProducto\":\"Platinum\",\"contrato\":\"00743616001234567890\",\"direccionTitular\":\"Av. Universidad No.1220, col Xoco,Coyoacan, Mex. D.F, CP 44444\",\"folio\":\" \",\"fechaOperacion\":\" \"}", true);
		}else{

			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DETALLE_TDC_ADICIONAL], parameters, response, false, true);
		}
		return response;
	}


    private ServerResponseImpl aceptaOfertaTDCAdicional(Hashtable<String, ?> params)
            throws IOException, ParsingException, URISyntaxException {

        AceptaTDCAdicional data=new AceptaTDCAdicional();
        ServerResponseImpl response = new ServerResponseImpl(data);
        //isjsonvalueCode=isJsonValueCode.VENTATDC;

        NameValuePair[] parameters = new NameValuePair[15];
        parameters[0] = new NameValuePair( ConstantsTDCAdicional.NUMERO_CELULAR_TAG,(String)params.get(ConstantsTDCAdicional.NUMERO_CELULAR_TAG));
        parameters[1] = new NameValuePair( ConstantsTDCAdicional.CVE_CAMP_TAG, (String)params.get(ConstantsTDCAdicional.CVE_CAMP_TAG));
        parameters[2] = new NameValuePair( ConstantsTDCAdicional.IUM_TAG,(String)params.get(ConstantsTDCAdicional.IUM_TAG));
        parameters[3] = new NameValuePair( ConstantsTDCAdicional.OPCION_TAG,(String)params.get(ConstantsTDCAdicional.OPCION_TAG));
        parameters[4] = new NameValuePair( ConstantsTDCAdicional.NOMBRE_ADIC_TAG,(String)params.get(ConstantsTDCAdicional.NOMBRE_ADIC_TAG));
        parameters[5] = new NameValuePair( ConstantsTDCAdicional.PRIMER_APELLIDO_ADIC_TAG,(String)params.get(ConstantsTDCAdicional.PRIMER_APELLIDO_ADIC_TAG));
        parameters[6] = new NameValuePair( ConstantsTDCAdicional.SEGUNDO_APELLIDO_ADIC_TAG,(String)params.get(ConstantsTDCAdicional.SEGUNDO_APELLIDO_ADIC_TAG));
        parameters[7] = new NameValuePair( ConstantsTDCAdicional.FECHA_NAC_ADIC_TAG,(String)params.get(ConstantsTDCAdicional.FECHA_NAC_ADIC_TAG));
        parameters[8] = new NameValuePair( ConstantsTDCAdicional.RFC_ADIC_TAG, (String)params.get(ConstantsTDCAdicional.RFC_ADIC_TAG));
        parameters[9] = new NameValuePair( ConstantsTDCAdicional.CREDITO_SOLICITADO_TAG,(String)params.get(ConstantsTDCAdicional.CREDITO_SOLICITADO_TAG));
        parameters[10] = new NameValuePair( ConstantsTDCAdicional.CODIGO_OTP_TAG,(String)params.get(ConstantsTDCAdicional.CODIGO_OTP_TAG));
        parameters[11] = new NameValuePair( ConstantsTDCAdicional.CADENA_AUTENTICACION_TAG,(String)params.get(ConstantsTDCAdicional.CADENA_AUTENTICACION_TAG));
        parameters[12] = new NameValuePair( ConstantsTDCAdicional.CODIGO_NIP_TAG,(String)params.get(ConstantsTDCAdicional.CODIGO_NIP_TAG));
        parameters[13] = new NameValuePair( ConstantsTDCAdicional.CODIGO_CVV2_TAG,(String)params.get(ConstantsTDCAdicional.CODIGO_CVV2_TAG));
        parameters[14] = new NameValuePair( ConstantsTDCAdicional.TARJETA_5IG_TAG,(String)params.get(ConstantsTDCAdicional.TARJETA_5IG_TAG));



        if (SIMULATION) {
            this.clienteHttp.simulateOperation(OPERATION_CODES[ACEPTACION_DETALLE_TDC_ADICIONAL],parameters,response,"{\"estado\":\"OK\",\"lineaCredito\":\"1000000\",\"producto\":\"AP\",\"desProducto\":\"Platinum\",\"contrato\":\"00743616001234567890\"," +
                    "\"direccionTitular\":\"Av. Universidad No.1220, col Xoco,Coyoacan, Mex. D.F, CP 44444\",\"folio\":\"1234567890\",\"fechaOperacion\":\"2014-05-27-11.27.47.659408\",\"envioCorreoElectronico\":\"SI\",\"envioSMS\":\"SI\",\"PM\":\"SI\",\"LP\":{\"campanias\":[{\"cveCamp\":\"0130ABCDEGAS123\",\"desOferta\":\"Incremento de línea de credito\",\"monto\":\"50000\",\"carrusel\":\"SI\"}," +
                    "{\"cveCamp\":\"1234CAMP343435356\",\"desOferta\":\"VentaTDC\",\"monto\":\"30000\",\"carrusel\":\"SI\"},{\"cveCamp\":\"0377POIOIU23434\",\"desOferta\":\"Efectivoinmediato\",\"monto\":\"3000\",\"carrusel\":\"SI\"},{\"cveCamp\":\"0060CAM9123459864\",\"desOferta\":\"PIDE\",\"monto\":\"900000\",\"carrusel\":\"SI\"}]}}", true);


            //Error
          /*  this.clienteHttp.simulateOperation(OPERATION_CODES[ACEPTACION_DETALLE_TDC_ADICIONAL],parameters,response,"{\"estado\":\"ERROR\",\"codigoMensaje\":\"UGE0525\"," +
                    "\"descripcionMensaje\":\"EL CLIENTE NO ES TITULAR DE LA CUENTA.\",\"PM\":\"NO\"," +
                    "\"LP\":{\"campanias\":[{\"cveCamp\":\"0130CAM2202100001\",\"desOferta\":\"INCREMENTO LINEA BANCARIA TDC BANCARIA\"," +
                    "\"monto\":\"3500000\",\"carrusel\":\"SI\"},{\"cveCamp\":\"0060ALT000027CN06\",\"desOferta\":\"PIDE\",\"monto\":\"4150050\",\"carrusel\":\"SI\"}]}}", true);
		  */
        }else{
            clienteHttp.invokeOperation(OPERATION_CODES[ACEPTACION_DETALLE_TDC_ADICIONAL], parameters, response, false, true);
        }
        return response;
    }


	

	/**
	 * Cancel the ongoing network operation.
	 */
	public void cancelNetworkOperations() {
		clienteHttp.abort();
	}



}


/********************************                ********************************/
/******************************** GENERIC THINGS ********************************/
/********************************                ********************************/

			/*

			public ServerResponseImpl doNetworkOperationGeneric(String operationCode, HashMap<String, String> params,  ArrayList<String> urlsProd, ArrayList<String> urlsDev, Boolean isJSON, Object responseModel, String simulatedResponse) throws IOException, ParsingException, URISyntaxException {

				ServerResponseImpl response = null;

				if(!containOperationCode(operationCode)){

					Integer lengthZero = OPERATIONS_PATH[0].length;
					Integer lengthOne = OPERATIONS_PATH[1].length;
					String url0 = urlsProd.get(0);
					String url1 = urlsProd.get(1);
					if (DEVELOPMENT) {
						url0 = urlsDev.get(0);
						url1 = urlsDev.get(1);
					}

					OPERATIONS_PATH[0][lengthZero] = url0;
					OPERATIONS_PATH[1][lengthOne] = url1;

					OPERATION_CODES[OPERATION_CODES.length] = operationCode;

					setupOperation(OPERATION_CODES.length, PROVIDER);

				}

				long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
				Integer provider = (Integer) OPERATIONS_PROVIDER.get(operationCode);
				if (provider != null) {
					PROVIDER = provider.intValue();
				}

				response = genericOperation(params, operationCode, isJSON, responseModel, simulatedResponse);

				if (sleep > 0) {
					try {
						Thread.sleep(sleep);
					} catch (InterruptedException e) {
					}
				}

				return response;
			}

			private Boolean containOperationCode(String operation){
				Boolean ret = false;
				for(String op : OPERATION_CODES){
					if(op.equalsIgnoreCase(operation)) ret = true;
				}

				return ret;
			}

			private ServerResponseImpl genericOperation(HashMap<String, String> params, String operationCode, Boolean isJSON, Object responseModel, String simulatedResponse)
					throws IOException, ParsingException, URISyntaxException {

				//FIXME null ?
				ServerResponseImpl response = new ServerResponseImpl(null);

				if (SIMULATION) {

					this.httpClient.simulateOperation(simulatedResponse,(ParsingHandler) response, true);

				}else{

					Iterator<String> it = params.keySet().iterator();
					NameValuePair[] parameters = new NameValuePair[params.keySet().size()];

					Integer cont = 0;
					while(it.hasNext()){
						String key = it.next();
						parameters[cont] = new NameValuePair(key, params.get(key));

						++cont;
					}

					clienteHttp.invokeOperation(operationCode, parameters, response, false, isJSON);
				}

				return response;
			}


			*/

/********************************                ********************************/
/********************************                ********************************/
/********************************                ********************************/



//one Click
/**
 * clase de enum para cambio de url;
 */
			/*public enum isJsonValueCode{
				ONECLICK, NONE,CONSUMO,DEPOSITOS;
			}

			public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
			//Termina One CLick*/