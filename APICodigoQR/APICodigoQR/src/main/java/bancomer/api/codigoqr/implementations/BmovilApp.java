package bancomer.api.codigoqr.implementations;

import android.annotation.SuppressLint;
import android.util.Log;

import java.util.Hashtable;

import bancomer.api.codigoqr.R;
import bancomer.api.codigoqr.config.SuiteAppCodigoQRApi;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

@SuppressLint("HandlerLeak")
public class BmovilApp extends BaseSubapplication {

    public BmovilApp(BmovilViewsController bmovilViewsController) {
        super();
        viewsController = bmovilViewsController;
    }

    public BmovilViewsController getBmovilViewsController() {
        return (BmovilViewsController) viewsController;
    }

    //////////////////////////////////////////////////////////////////////////////
    //                Network operations										//
    //////////////////////////////////////////////////////////////////////////////

    /**
     * Invoke a network operation. It shows a popup indicating progress with
     * custom texts.
     *
     * @param operationId        network operation identifier. See Server class.
     * @param params             Hashtable with the parameters passed to the Server. See Server
     *                           class for parameter names.
     * @param caller             the BaseScreen instance (that is, the screen), which requests the
     *                           network operation. Must be null if the caller is not a screen.
     * @param progressLabel      text with the title of the progress popup
     * @param progressMessage    text with the content of the progress popup
     * @param callerHandlesError boolean flag indicating wheter the caller is capable
     *                           of handling an error message or the network flux should handle in the default
     *                           implementation
     */
    @Override
    protected void invokeNetworkOperation(int operationId,
                                          final Hashtable<String, ?> params,
                                          final boolean isJson,
                                          final ParsingHandler handler,
                                          final BaseViewControllerCommons caller,
                                          String progressLabel,
                                          String progressMessage,
                                          final boolean callerHandlesError) {

        super.invokeNetworkOperation(operationId, params, isJson, handler, caller, progressLabel, progressMessage, callerHandlesError);
    }

    /**
     * Analyze the network response obtained from the server
     *
     * @param operationId        network identifier. See Server class.
     * @param response           the ServerResponse instance built from the server response. It
     *                           contains the type of result (success, failure), and the real content for
     *                           the application business.
     * @param operationId        network operation identifier. See Server class.
     * @param response           the ServerResponse instance returned from the server.
     * @param throwable          the throwable received.
     * @param callerHandlesError boolean flag indicating wheter the caller is capable
     *                           of handling an error message or the network flux should handle in the default
     *                           implementation
     */
    @Override
    protected void analyzeNetworkResponse(Integer operationId,
                                          ServerResponse response,
                                          Throwable throwable,
                                          final BaseViewControllerCommons caller,
                                          boolean callerHandlesError) {

        super.analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);

        if (SuiteAppCodigoQRApi.getCallBackSession() != null) {
            SuiteAppCodigoQRApi.getCallBackSession().userInteraction();
        }
    }

    public void closeBmovilAppSession(BaseViewControllerCommons caller) {
        applicationLogged = false;
        Session session = Session.getInstance(SuiteAppCodigoQRApi.appContext);
        String username = session.getUsername();
        String ium = session.getIum();
        String client = session.getClientNumber();

        System.gc();
        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put("NT", username);
        params.put(ServerConstants.IUM_ETIQUETA, ium);
        params.put("TE", client);

        if (viewsController != null) {
            caller = viewsController.getCurrentViewController();
        }

        invokeNetworkOperation(Server.CLOSE_SESSION_OPERATION, params, false, null, caller,
                SuiteAppCodigoQRApi.appContext.getString(R.string.alert_closeSession),
                SuiteAppCodigoQRApi.appContext.getString(R.string.alert_thank_you), true);

        if (Server.ALLOW_LOG) Log.e("CerrarSesion", "La sesion fue cerrada satisfactoriamente.");
    }

}
