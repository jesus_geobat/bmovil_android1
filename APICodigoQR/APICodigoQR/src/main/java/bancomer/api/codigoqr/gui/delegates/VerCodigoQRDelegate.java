package bancomer.api.codigoqr.gui.delegates;

import android.graphics.Bitmap;

import com.google.gson.Gson;

import bancomer.api.codigoqr.config.SuiteAppCodigoQRApi;
import bancomer.api.codigoqr.gui.controllers.VerCodigoQRViewController;
import bancomer.api.codigoqr.models.OperacionQR;
import bancomer.api.codigoqr.utils.EncodingUtils;
import bancomer.api.codigoqr.utils.QRGeneratorUtils;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * Created by andres.vicentelinare on 06/10/2016.
 */
public class VerCodigoQRDelegate extends BaseDelegateCommons {

    public final static long VER_CODIGO_QR_DELEGATE_ID = 0x0fdc4cab70b4b3d3L;

    private VerCodigoQRViewController verCodigoQRViewController;
    private OperacionQR operacionQR;

    public VerCodigoQRViewController getVerCodigoQRViewController() {
        return verCodigoQRViewController;
    }

    public void setVerCodigoQRViewController(VerCodigoQRViewController verCodigoQRViewController) {
        this.verCodigoQRViewController = verCodigoQRViewController;
    }

    public OperacionQR getOperacionQR() {
        return operacionQR;
    }

    public void setOperacionQR(OperacionQR operacionQR) {
        this.operacionQR = operacionQR;
    }

    public String getNumeroCuenta() {
        return Tools.hideAccountNumber(operacionQR.getCuentaDestino());
    }

    public String getNombreTitular() {
        return operacionQR.getNombreTitular();
    }

    public boolean isImporteVacio() {
        return Tools.isEmptyOrNull(operacionQR.getImporte());
    }

    public String getImporte() {
        return Tools.formatAmount(operacionQR.getImporte(), false);
    }

    public boolean isMotivoVacio() {
        return Tools.isEmptyOrNull(operacionQR.getMotivoPago());
    }

    public String getMotivo() {
        return operacionQR.getMotivoPago();
    }

    public Bitmap getQRImage(int sideLength) {
        Gson gson = new Gson();
        String json = gson.toJson(operacionQR);
        String base64String = EncodingUtils.encrypt(json.trim());
        return QRGeneratorUtils.encodeAsBitmap(base64String, sideLength);
    }

    public void showPersonalizarQR() {
        operacionQR.setImporte("");
        operacionQR.setMotivoPago("");
        SuiteAppCodigoQRApi.getInstance().getBmovilViewsController().showPersonalizarQR(operacionQR);
    }

}
