package bancomer.api.codigoqr.gui.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bancomer.api.codigoqr.R;
import bancomer.api.codigoqr.config.SuiteAppCodigoQRApi;
import bancomer.api.codigoqr.gui.delegates.PersonalizarQRDelegate;
import bancomer.api.codigoqr.implementations.AmountField;
import bancomer.api.codigoqr.implementations.BaseViewController;

public class PersonalizarQRViewController extends BaseViewController {

    private PersonalizarQRDelegate personalizarQRDelegate;

    private LinearLayout layoutACapturar;
    private TextView labelValorCuenta;
    private TextView labelValorNombre;
    private ImageView imageCodigoQR;
    private TextView labelHintImporte;
    private AmountField txtImporte;
    private TextView labelHintMotivo;
    private EditText txtMotivo;

    private LinearLayout layoutAnimacion;
    private ImageView imagenAnimacion;

    private int[] animaciones;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personalizar_qr);

        setParentViewsController(SuiteAppCodigoQRApi.getInstance().getBmovilViewsController());

        personalizarQRDelegate = (PersonalizarQRDelegate) parentViewsController.getBaseDelegateForKey(PersonalizarQRDelegate.PERSONALIZAR_CODIGO_QR_DELEGATE_ID);
        personalizarQRDelegate.setPersonalizarQRViewController(this);

        findViews();

        loadData();

        InputFilter[] filterImporte = {new InputFilter.LengthFilter(13)};
        txtImporte.setFilters(filterImporte);
        InputFilter[] filterConcepto = {new InputFilter.LengthFilter(20)};
        txtMotivo.setFilters(filterConcepto);

        txtImporte.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (txtImporte.getText().toString().isEmpty()) {
                    labelHintImporte.setVisibility(View.GONE);
                } else {
                    labelHintImporte.setVisibility(View.VISIBLE);
                }
            }
        });

        txtMotivo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (txtMotivo.getText().toString().isEmpty()) {
                    labelHintMotivo.setVisibility(View.GONE);
                } else {
                    labelHintMotivo.setVisibility(View.VISIBLE);
                }
            }
        });

        animaciones = new int[38];
        animaciones[0] = R.drawable.qr1;
        animaciones[1] = R.drawable.qr2;
        animaciones[2] = R.drawable.qr3;
        animaciones[3] = R.drawable.qr4;
        animaciones[4] = R.drawable.qr5;
        animaciones[5] = R.drawable.qr6;
        animaciones[6] = R.drawable.qr7;
        animaciones[7] = R.drawable.qr8;
        animaciones[8] = R.drawable.qr9;
        animaciones[9] = R.drawable.qr10;
        animaciones[10] = R.drawable.qr11;
        animaciones[11] = R.drawable.qr12;
        animaciones[12] = R.drawable.qr13;
        animaciones[13] = R.drawable.qr14;
        animaciones[14] = R.drawable.qr15;
        animaciones[15] = R.drawable.qr16;
        animaciones[16] = R.drawable.qr17;
        animaciones[17] = R.drawable.qr18;
        animaciones[18] = R.drawable.qr19;
        animaciones[19] = R.drawable.qr20;
        animaciones[20] = R.drawable.qr21;
        animaciones[21] = R.drawable.qr22;
        animaciones[22] = R.drawable.qr23;
        animaciones[23] = R.drawable.qr24;
        animaciones[24] = R.drawable.qr25;
        animaciones[25] = R.drawable.qr26;
        animaciones[26] = R.drawable.qr27;
        animaciones[27] = R.drawable.qr28;
        animaciones[28] = R.drawable.qr29;
        animaciones[29] = R.drawable.qr30;
        animaciones[30] = R.drawable.qr31;
        animaciones[31] = R.drawable.qr32;
        animaciones[32] = R.drawable.qr33;
        animaciones[33] = R.drawable.qr34;
        animaciones[34] = R.drawable.qr35;
        animaciones[35] = R.drawable.qr36;
        animaciones[36] = R.drawable.qr37;
        animaciones[37] = R.drawable.qr38;

    }

    private void findViews() {
        layoutACapturar = (LinearLayout) findViewById(R.id.layout_a_capturar);
        labelValorCuenta = (TextView) findViewById(R.id.label_valor_cuenta);
        labelValorNombre = (TextView) findViewById(R.id.label_valor_nombre);
        imageCodigoQR = (ImageView) findViewById(R.id.image_codigo_qr);
        labelHintImporte = (TextView) findViewById(R.id.label_hint_importe);
        txtImporte = (AmountField) findViewById(R.id.txt_importe);
        labelHintMotivo = (TextView) findViewById(R.id.label_hint_motivo);
        txtMotivo = (EditText) findViewById(R.id.txt_motivo);

        layoutAnimacion = (LinearLayout) findViewById(R.id.layout_animacion);
        imagenAnimacion = (ImageView) findViewById(R.id.imagen_animacion);
    }

    private void loadData() {
        labelValorCuenta.setText(personalizarQRDelegate.getNumeroCuenta());
        labelValorNombre.setText(personalizarQRDelegate.getNombreTitular().substring(0,personalizarQRDelegate.getNombreTitular().indexOf(' ')));

        imageCodigoQR.setImageBitmap(personalizarQRDelegate.getQRImage(imageCodigoQR.getLayoutParams().width));
    }

    public void clickAtras(View view) {
        super.goBack();
    }

    public void clickGenerar(View view) {
        layoutAnimacion.setVisibility(View.VISIBLE);
        final Handler handler = new Handler();
        updateFrame(handler, 0);
    }

    private void updateFrame(final Handler handler, final int index) {
        if (index >= animaciones.length) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    personalizarQRDelegate.saveData(txtImporte.getAmount(), txtMotivo.getText().toString());
                    goBack();
                }
            }, 1000);
            return;
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                imagenAnimacion.setImageDrawable(getResources().getDrawable(animaciones[index]));
                imagenAnimacion.postInvalidate();
                updateFrame(handler, index + 1);
            }
        }, 5);
    }

}