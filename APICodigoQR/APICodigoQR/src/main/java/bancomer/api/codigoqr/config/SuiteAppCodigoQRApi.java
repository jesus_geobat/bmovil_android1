package bancomer.api.codigoqr.config;

import android.content.Context;

import com.bancomer.base.callback.CallBackSession;

import bancomer.api.codigoqr.implementations.BmovilApp;
import bancomer.api.codigoqr.implementations.BmovilViewsController;
import suitebancomercoms.classes.common.PropertiesManager;

public class SuiteAppCodigoQRApi extends com.bancomer.base.SuiteApp {

    private static final SuiteAppCodigoQRApi instance = new SuiteAppCodigoQRApi();

    private SuiteAppCodigoQRApi() {
        bmovilViewsController = new BmovilViewsController();
        bmovilApplication = new BmovilApp(bmovilViewsController);
    }

    public static SuiteAppCodigoQRApi getInstance() {
        return instance;
    }

    public static Context appContext;
    private static CallBackSession callBackSession;
    private BmovilApp bmovilApplication;
    private BmovilViewsController bmovilViewsController;

    public static boolean getSofttokenStatus() {
        return PropertiesManager.getCurrent().getSofttokenActivated();
    }

    public static int getResourceId(String nombre, String tipo) {
        return appContext.getResources().getIdentifier(nombre, tipo, appContext.getPackageName());
    }

    public static CallBackSession getCallBackSession() {
        return callBackSession;
    }

    public static void setCallBackSession(CallBackSession callBackSession) {
        SuiteAppCodigoQRApi.callBackSession = callBackSession;
    }

    public BmovilApp getBmovilApplication() {
        return bmovilApplication;
    }

    public BmovilViewsController getBmovilViewsController() {
        return bmovilViewsController;
    }

}
