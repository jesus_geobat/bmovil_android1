package bancomer.api.codigoqr.config;

import android.content.Context;

import com.bancomer.base.callback.CallBackSession;

import bancomer.api.codigoqr.gui.controllers.LeerCodigoQRViewController;
import bancomer.api.codigoqr.gui.controllers.VerCodigoQRViewController;
import bancomer.api.codigoqr.gui.delegates.LeerCodigoQRDelegate;
import bancomer.api.codigoqr.gui.delegates.VerCodigoQRDelegate;
import bancomer.api.codigoqr.implementations.BaseViewsController;
import bancomer.api.codigoqr.models.OperacionQR;

public class CodigoQRFacade {

    private static final CodigoQRFacade instance = new CodigoQRFacade();

    private CodigoQRFacade() {

    }

    public static CodigoQRFacade getInstance() {
        return instance;
    }

    public void configurar(Context context, CallBackSession callBackSession) {
        SuiteAppCodigoQRApi.appContext = context;
        SuiteAppCodigoQRApi.getInstance().getBmovilApplication()
                .setApplicationLogged(true);
        SuiteAppCodigoQRApi.setCallBackSession(callBackSession);
    }

    public void showVerCodigoQR(OperacionQR operacionQR) {
        SuiteAppCodigoQRApi.getInstance().getBmovilViewsController().showVerCodigoQR(operacionQR);
    }

    public void showLeerCodigoQR(LeerCodigoQRResultHandler leerCodigoQRResultHandler) {
        SuiteAppCodigoQRApi.getInstance().getBmovilViewsController().showLeerCodigoQR(leerCodigoQRResultHandler);
    }

}
