package bancomer.api.codigoqr.gui.controllers;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.zxing.Result;

import bancomer.api.codigoqr.R;
import bancomer.api.codigoqr.config.SuiteAppCodigoQRApi;
import bancomer.api.codigoqr.gui.delegates.LeerCodigoQRDelegate;
import bancomer.api.codigoqr.implementations.BaseViewController;
import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class LeerCodigoQRViewController extends BaseViewController implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;

    private LeerCodigoQRDelegate leerCodigoQRDelegate;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_leer_codigo_qr);

        setParentViewsController(SuiteAppCodigoQRApi.getInstance().getBmovilViewsController());

        leerCodigoQRDelegate = (LeerCodigoQRDelegate) parentViewsController.getBaseDelegateForKey(LeerCodigoQRDelegate.LEER_CODIGO_QR_DELEGATE_ID);
        leerCodigoQRDelegate.setLeerCodigoQRViewController(this);

        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(this) {
            @Override
            protected IViewFinder createViewFinderView(Context context) {
                return new CustomViewFinderView(context);
            }
        };
        contentFrame.addView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
//        Toast.makeText(this, "Contents = " + rawResult.getText() +
//                ", Format = " + rawResult.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();

        leerCodigoQRDelegate.decodeQR(rawResult.getText().toString());
        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(LeerCodigoQRViewController.this);
            }
        }, 2000);
    }

    private static class CustomViewFinderView extends ViewFinderView {

        public CustomViewFinderView(Context context) {
            super(context);
            init();
        }

        public CustomViewFinderView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        private void init() {
            setSquareViewFinder(true);
            setBorderStrokeWidth(0);
            setBorderLineLength(0);
        }
    }

    public void clickAtras(View v){
        parentViewsController.removeDelegateFromHashMap(LeerCodigoQRDelegate.LEER_CODIGO_QR_DELEGATE_ID);
        super.goBack();
    }
}
