package bancomer.api.codigoqr.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by evaltierrah on 25/11/16.
 */
public class QRConstants {

    /**
     * Folder to save QR images
     */
    public static final String NAME_IMAGES_QR = "qr.jpg";
    /**
     * Folder to save QR images
     */
    public static final String NAME_PDF_QR = "qr.pdf";
    /**
     * Folder to save QR images
     */
    public static final String FOLDER_IMAGES_QR = "Bancomer";
    /**
     * Path to save QR images
     */
    public static final File PATH_IMAGES_QR = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
    /**
     *
     */
    public static final String TYPE_PDF = "application/pdf";
    /**
     *
     */
    public static final String TYPE_IMAGE_JPEG = "image/jpeg";
    /**
     *
     */
    public static final String IMAGE_ALL = "image/*";
    /**
     * FORMAT DATE
     */
    public static final String FORMAT_DATE = "yyyy-MM-dd_hh:mm:ss";
    /**
     *
     */
    public static final String ANDROID_EMAIL = "android.email";
    /**
     *
     */
    public static final String ANDROID_GM = "image/*";

}
