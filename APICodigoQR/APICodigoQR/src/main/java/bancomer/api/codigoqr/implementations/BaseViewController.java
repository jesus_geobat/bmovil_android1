/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package bancomer.api.codigoqr.implementations;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import java.lang.reflect.Method;

import bancomer.api.codigoqr.R;
import bancomer.api.codigoqr.config.SuiteAppCodigoQRApi;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ControlEventos;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;

/**
 * BaseScreen is the base class for all the screens in the MBanking application
 * It provides basic, common functionality to be included in the business logic
 * so that the application can manage screens in a uniform, consistent way.
 *
 * @author Stefanini IT Solutions
 */
public abstract class BaseViewController extends BaseViewControllerCommons {

    //
    /**
     * Current view edit fields' references. Each view should implement this variable.
     */
    protected static EditText[] sFields;

    /**
     * A progress dialog for long waiting processes.
     */
    protected ProgressDialog mProgressDialog;
    /**
     * Referencias al controlador de vistas padre de esta vista
     */
    protected BaseViewsControllerCommons parentViewsController;
    protected Activity activityContext;

    protected boolean habilitado = true;
    //AMZ
    private BmovilViewsController bm;
    /**
     * The alert error or information dialog.
     */
    private AlertDialog mAlertDialog;

    /**
     * Defines when the last click occurred
     */
    private long touchDownTime;

    /**
     * Called when the activity is first created.
     *
     * @param savedInstanceState state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        bm = SuiteAppCodigoQRApi.getInstance().getBmovilViewsController();
    }

    /**
     * Screen is about to be shown.
     */
    @Override
    protected void onStart() {
        super.onStart();
        sFields = null;
    }

    @Override
    protected void onPause() {
        if (!ControlEventos.isScreenOn(this)) {
            if (ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Se ha bloqueado el telefono");
            }
            finish();
            if (SuiteAppCodigoQRApi.getCallBackSession() != null) {
                if (ServerCommons.ALLOW_LOG) {
                    Log.d(getClass().getSimpleName(), "Se ejecuta cierraSesion desde CallBackSession");
                }
                SuiteAppCodigoQRApi.getCallBackSession().cierraSesion();
            } else {
                if (ServerCommons.ALLOW_LOG) {
                    Log.d(getClass().getSimpleName(), "CallBackSession == null");
                }
            }
        }
        super.onPause();
    }

    /* (non-Javadoc)
         * @see android.app.Activity#onStop()
         */
    @Override
    protected void onStop() {
        if (ServerCommons.ALLOW_LOG) {
            Log.e(getClass().getSimpleName(), "OnStop");
        }
        if (ControlEventos.isAppIsInBackground(this.getBaseContext())) { //Valida el estado de la app si es change Activity no hace nada
            if (ServerCommons.ALLOW_LOG) {
                Log.d(getClass().getSimpleName(), "La app se fue a Background");
            }
            //finish();
            if (SuiteAppCodigoQRApi.getCallBackSession() != null) {
                if (ServerCommons.ALLOW_LOG) {
                    Log.d(getClass().getSimpleName(), "Se ejecuta cierraSesionBackground desde CallBackSession");
                }
                SuiteAppCodigoQRApi.getCallBackSession().cierraSesionBackground(Boolean.FALSE);
            } else {
                if (ServerCommons.ALLOW_LOG) {
                    Log.d(getClass().getSimpleName(), "CallBackSession == null");
                }
            }
        }
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return false;
    }

    /**
     * Overrides and cancels android's back button, if the button is visible then it does the same
     * as clicking on the application's back button.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        onUserInteraction();
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                //AMZ
                if (bm != null) {
                    bm.touchAtras();
                }

                goBack();
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    /**
     * This function has to be overridden by child members, it implements
     * the processing for server response.
     *
     * @param operationId
     * @param response
     */
    public void processNetworkResponse(int operationId, ServerResponse response) {
    }

    /**
     * Determines if the user touched or performed some action in the screen.
     * If he or she did, then reset the logout timer.
     */
    @Override
    public void onUserInteraction() {
        if (Server.ALLOW_LOG) {
            Log.i(getClass().getSimpleName(), "User Interacted Administrar");
        }
        if (SuiteAppCodigoQRApi.getCallBackSession() != null) {
            if (ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Envia peticion de reinicio a bmovil desde CallBackSession");
            }
            SuiteAppCodigoQRApi.getCallBackSession().userInteraction();
        } else {
            if (ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "CallBackSession == null");
            }
        }
        super.onUserInteraction();
    }

    /*
     * Called to process touch screen events.
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchDownTime = SystemClock.elapsedRealtime();
                break;

            case MotionEvent.ACTION_UP:
                //to avoid drag events
                if (SystemClock.elapsedRealtime() - touchDownTime <= 150) {

                    EditText[] textFields = this.getFields();
                    if (textFields != null && textFields.length > 0) {
                        boolean clickIsOutsideEditTexts = true;

                        for (EditText field : textFields) {
                            if (isPointInsideView(ev.getRawX(), ev.getRawY(), field)) {
                                clickIsOutsideEditTexts = false;
                                break;
                            }
                        }

                        if (clickIsOutsideEditTexts) {
                            this.hideSoftKeyboard();
                        }
                    } else {
                        this.hideSoftKeyboard();
                    }
                }
                break;
        }

        return super.dispatchTouchEvent(ev);
    }

    /**
     * Override this from children to return the view's fields.
     *
     * @return the current screen fields.
     */
    protected EditText[] getFields() {
        return sFields;
    }

    /**
     * Gets the trimmed text of a text field content.
     *
     * @param textField the EditText component to retrieve text
     * @return the textField value without trailing spaces
     */
    protected String getText(EditText textField) {
        if (textField != null) {
            return textField.getText().toString().trim();
        } else {
            return "";
        }
    }

    /**
     * Hides the soft keyboard if any editbox called it.
     */
    public void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusedView = getCurrentFocus();
        if (imm != null && focusedView != null) {
            imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }
    }

    /**
     * This is called whenever the back button is pressed.
     * Normally, this function will return to the previous screen.
     */
    public void goBack() {

        this.hideSoftKeyboard();
        parentViewsController.setActivityChanging(true);
        finish();
        parentViewsController.overrideScreenBackTransition();
    }

    /**
     * Shows an error message if the provided resource exists. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message resource to show
     */
    public void showErrorMessage(int errorMessage) {
        if (errorMessage > 0) {
            showErrorMessage(SuiteApp.appContext.getString(errorMessage), null);
        }
    }

    public void showErrorMessage(String errorMessage) {
        showErrorMessage(errorMessage, null);
    }

    /**
     * Places a progress dialog, for long waiting processes.
     *
     * @param strTitle   the dialog title
     * @param strMessage the message to show in the while
     */
    public void muestraIndicadorActividad(String strTitle, String strMessage) {
        if (mProgressDialog != null) {
            ocultaIndicadorActividad();
        }
        mProgressDialog = ProgressDialog.show(SuiteApp.appContext, strTitle,
                strMessage, true);
        // TODO LABM probar
        //mProgressDialog = ProgressDialog.show(SuiteApp.appContext,
        mProgressDialog.setCancelable(false);
    }

    /**
     * Hides the progress dialog.
     */
    public void ocultaIndicadorActividad() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    /**
     * Shows an error message if the message length is greater than 0. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message text to show
     */
    public void showErrorMessage(String errorMessage, OnClickListener listener) {

        if (errorMessage.length() > 0) {
            //AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SuiteAppCodigoQRApi.appContext);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SuiteApp.appContext);
            alertDialogBuilder.setTitle(R.string.label_error);
            alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
            alertDialogBuilder.setMessage(errorMessage);
            alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.show();
        }
    }

    /**
     * Determines if given points are inside view
     *
     * @param x    - x coordinate of point
     * @param y    - y coordinate of point
     * @param view - view object to compare
     * @return true if the points are within view bounds, false otherwise
     */
    private boolean isPointInsideView(float x, float y, View view) {
        int location[] = new int[2];
        view.getLocationOnScreen(location);
        int viewX = location[0];
        int viewY = location[1];

        //point is inside view bounds
        if ((x > viewX && x < (viewX + view.getWidth())) &&
                (y > viewY && y < (viewY + view.getHeight()))) {
            return true;
        } else {
            return false;
        }
    }

    public BaseViewsControllerCommons getParentViewsController() {
        return parentViewsController;
    }

    public void setParentViewsController(BaseViewsControllerCommons parentViewsController) {
        this.parentViewsController = parentViewsController;
    }

    public void hideCurrentDialog() {
        try {
            if (mAlertDialog != null) {
                mAlertDialog.dismiss();
                mAlertDialog = null;
            }
        } catch (Exception ex) {

        }
    }

    public void setCurrentDialog(AlertDialog dialog) {
        this.mAlertDialog = dialog;
    }

    private ScrollView getScrollView() {
        return (ScrollView) findViewById(SuiteApp.getResourceId("body_layout", "id"));
    }

    // #region Information Alert.

    protected void moverScroll() {
        getScrollView().post(new Runnable() {

            @Override
            public void run() {
                getScrollView().fullScroll(ScrollView.FOCUS_UP);
                if (Server.ALLOW_LOG) Log.d(getClass().getName(), "Mover scroll");
            }
        });
    }

    /**
     * Muestra una alerta con el mensaje dado.
     *
     * @param message el mensaje a mostrar
     */
    public void showInformationAlert(int message) {
        if (message > 0) {
            showInformationAlert(SuiteApp.appContext.getString(message));
        }
    }

    /**
     * Shows an information alert with the given message text. No action is performed after this alert is closed.
     *
     * @param message the message text to show
     */
    public void showInformationAlert(String message) {
        if (message.length() > 0) {
            showInformationAlert(message, null);
        }
    }

    /**
     * Shows an information alert with the given message.
     *
     * @param message the message resource to show
     */
    public void showInformationAlert(int message, OnClickListener listener) {
        if (message > 0) {
            showInformationAlert(SuiteApp.appContext.getString(message), listener);
        }
    }

    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     *
     * @param message  the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(String message, OnClickListener listener) {
        showInformationAlert(SuiteApp.appContext.getString(R.string.label_information), message, listener);
    }

    /**
     * Shows an alert dialog.
     *
     * @param title    The alert title resource id.
     * @param message  The alert message resource id.
     * @param listener The listener for the "Ok" button.
     */
    public void showInformationAlert(int title, int message, OnClickListener listener) {
        if (message > 0) {
            showInformationAlert(SuiteApp.appContext.getString(title), SuiteApp.appContext.getString(message), listener);
        }
    }

    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     *
     * @param title    The alert title resource id.
     * @param message  the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(String title, String message, OnClickListener listener) {
        showInformationAlert(title, message, SuiteApp.appContext.getString(R.string.label_ok), listener);
    }

    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     *
     * @param title     The alert title resource id.
     * @param //message the message text to show
     * @param listener  onClickListener to perform action on close
     */
    public void showInformationAlertEspecial(String title, String errorCode, String descripcionError, OnClickListener listener) {
        showInformationAlertEspecial(title, errorCode, descripcionError, SuiteApp.appContext.getString(R.string.label_ok), listener);
    }

    /**
     * Shows an alert dialog.
     *
     * @param title    The alert title resource id.
     * @param message  The alert message resource id.
     * @param okText   the ok text
     * @param listener The listener for the "Ok" button.
     */
    public void showInformationAlert(int title, int message, int okText, OnClickListener listener) {
        if (message > 0) {
            showInformationAlert(SuiteApp.appContext.getString(title), SuiteApp.appContext.getString(message), SuiteApp.appContext.getString(okText), listener);
        }
    }

    /**
     * Shows an alert dialog.
     *
     * @param title    The alert title.
     * @param message  The alert message.
     * @param okText   the ok text
     * @param listener The listener for the "Ok" button.
     */
    public void showInformationAlert(String title, String message, String okText, OnClickListener listener) {
        if (!habilitado)
            return;
        habilitado = false;
        if (message.length() > 0) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
            alertDialogBuilder.setMessage(message);
            if (null == listener) {
                listener = new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
            }

            alertDialogBuilder.setPositiveButton(okText, listener);
            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.create();
            mAlertDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    habilitado = true;
                }
            });
            mAlertDialog.show();
        }
    }

    /**
     * Shows an alert dialog.
     *
     * @param title     The alert title.
     * @param //message The alert message.
     * @param okText    the ok text
     * @param listener  The listener for the "Ok" button.
     */
    public void showInformationAlertEspecial(String title, String errorCode, String descripcionError, String okText, OnClickListener listener) {
        if (!habilitado)
            return;
        habilitado = false;
        if (descripcionError.length() > 0) {

            AlertDialog.Builder builder = new AlertDialog.Builder(SuiteApp.appContext);
            //AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //AlertDialog.Builder builder = new AlertDialog.Builder(SuiteAppCodigoQRApi.appContext);
            builder.setTitle(title);
            builder.setIcon(R.drawable.anicalertaaviso);
            LayoutInflater inflater = ((Activity) SuiteApp.appContext).getLayoutInflater();
            View vista = inflater.inflate(SuiteApp.getResourceId("layout_alert_codigo_error_admon", "layout"), null);
                /*builder.setView(vista)
                   .setPositiveButton(okText, new DialogInterface.OnClickListener() {
	    	           public void onClick(DialogInterface dialog, int id) {
	    	                  dialog.dismiss();
	    	           }
	    	    });*/
            if (null == listener) {
                listener = new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
            }
            builder.setView(vista)
                    .setPositiveButton(okText, listener);

            TextView text1 = (TextView) vista.findViewById(SuiteApp.getResourceId("descripcionError", "id"));
            TextView text2 = (TextView) vista.findViewById(SuiteApp
                    .getResourceId("codigoError", "id"));
            text1.setText(descripcionError);
            text2.setText(errorCode);

            builder.setCancelable(false);

            mAlertDialog = builder.create();

            mAlertDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    habilitado = true;
                }
            });
            mAlertDialog.show();

        }
    }

    public void showYesNoAlert(String message, OnClickListener positiveListener) {
        showYesNoAlert(SuiteApp.appContext.getString(R.string.label_information),
                message,
                SuiteApp.appContext.getString(R.string.common_alert_yesno_positive_button),
                SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button),
                positiveListener,
                null);
    }

    public void showYesNoAlertEspecialTitulo(String errorCode, String descripcionError, OnClickListener positiveListener) {
        showYesNoAlertEspecial(SuiteApp.appContext.getString(R.string.label_information),
                errorCode,
                descripcionError,
                SuiteApp.appContext.getString(R.string.common_alert_yesno_positive_button),
                SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button),
                positiveListener,
                null);
    }

    public void showYesNoAlert(int message, OnClickListener positiveListener) {
        showYesNoAlert(R.string.label_information,
                message,
                R.string.common_alert_yesno_positive_button,
                R.string.common_alert_yesno_negative_button,
                positiveListener,
                null);
    }

    public void showYesNoAlert(int message, OnClickListener positiveListener, OnClickListener negativeListener) {
        showYesNoAlert(R.string.label_information,
                message,
                R.string.common_alert_yesno_positive_button,
                R.string.common_alert_yesno_negative_button,
                positiveListener,
                negativeListener);
    }

    public void showYesNoAlert(String title, String message, OnClickListener positiveListener) {
        showYesNoAlert(title,
                message,
                SuiteApp.appContext.getString(R.string.common_alert_yesno_positive_button),
                SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button),
                positiveListener,
                null);
    }

    public void showYesNoAlert(String message, OnClickListener positiveListener, OnClickListener negativeListener) {
        showYesNoAlert("",
                message,
                SuiteApp.appContext.getString(R.string.common_alert_yesno_positive_button),
                SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button),
                positiveListener,
                negativeListener);
    }

    public void showYesNoAlertEspecialTitulo(String errorCode, String descripcionError, OnClickListener positiveListener, OnClickListener negativeListener) {
        showYesNoAlertEspecial(SuiteApp.appContext.getString(R.string.label_information),
                errorCode,
                descripcionError,
                SuiteApp.appContext.getString(R.string.common_alert_yesno_positive_button),
                SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button),
                positiveListener,
                negativeListener);
    }

    public void showYesNoAlert(int title, int message, OnClickListener positiveListener) {
        showYesNoAlert(title,
                message,
                R.string.common_alert_yesno_positive_button,
                R.string.common_alert_yesno_negative_button,
                positiveListener,
                null);
    }

    public void showYesNoAlert(String title, String message, String okText, OnClickListener positiveListener) {
        showYesNoAlert(title, message, okText, SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button), positiveListener, null);
    }

    public void showYesNoAlert(int title, int message, int okText, OnClickListener positiveListener) {
        showYesNoAlert(title, message, okText, R.string.common_alert_yesno_negative_button, positiveListener, null);
    }

    public void showYesNoAlert(String title, String message, String okText, String calcelText, OnClickListener positiveListener) {
        showYesNoAlert(title, message, okText, calcelText, positiveListener, null);
    }

    public void showYesNoAlert(int title, int message, int okText, int calcelText, OnClickListener positiveListener) {
        showYesNoAlert(title, message, okText, calcelText, positiveListener, null);
    }

    public void showYesNoAlert(int title,
                               int message,
                               int okText,
                               int calcelText,
                               OnClickListener positiveListener,
                               OnClickListener negativeListener) {
        showYesNoAlert(SuiteApp.appContext.getString(title), SuiteApp.appContext.getString(message), SuiteApp.appContext.getString(okText), SuiteApp.appContext.getString(calcelText), positiveListener, negativeListener);
    }

    public void showYesNoAlert(String title,
                               String message,
                               String okText,
                               String calcelText,
                               OnClickListener positiveListener,
                               OnClickListener negativeListener) {
        if (!habilitado)
            return;
        habilitado = false;
        if (message.length() > 0) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setPositiveButton(okText, positiveListener);

            if (null == negativeListener) {
                alertDialogBuilder.setNegativeButton(calcelText, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        habilitado = true;
                        dialog.dismiss();
                    }
                });
            } else {
                alertDialogBuilder.setNegativeButton(calcelText, negativeListener);
            }

            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.create();
            mAlertDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    habilitado = true;
                }
            });
            mAlertDialog.show();

        }
    }

    public void showYesNoAlertEspecial(String title, String errorCode, String descripcionError, String okText,
                                       String calcelText, OnClickListener positiveListener,
                                       OnClickListener negativeListener) {
        if (!habilitado)
            return;
        habilitado = false;
        if (descripcionError.length() > 0) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(title);
            builder.setIcon(R.drawable.anicalertaaviso);
            LayoutInflater inflater = getLayoutInflater();
            View vista = inflater.inflate(R.layout.api_codigo_qr_layout_alert_codigo_error, null);
            builder.setView(vista);
            builder.setPositiveButton(okText, positiveListener);

            if (null == negativeListener) {
                builder.setNegativeButton(calcelText,
                        new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                habilitado = true;
                                dialog.dismiss();
                            }
                        });
            } else {
                builder.setNegativeButton(calcelText,
                        negativeListener);
            }

            TextView text1 = (TextView) vista.findViewById(SuiteApp.getResourceId("descripcionError", "id"));
            TextView text2 = (TextView) vista.findViewById(SuiteApp.getResourceId("codigoError", "id"));
            text1.setText(descripcionError);
            text2.setText(errorCode);

            builder.setCancelable(false);

            mAlertDialog = builder.create();

            mAlertDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    habilitado = true;
                }
            });
            mAlertDialog.show();


        }
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public void showNoYesAlert(int message, OnClickListener positiveListener, OnClickListener negativeListener) {
        showYesNoAlert(R.string.label_information,
                message,
                R.string.common_alert_yesno_negative_button,
                R.string.common_alert_yesno_positive_button,
                negativeListener,
                positiveListener);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }
}