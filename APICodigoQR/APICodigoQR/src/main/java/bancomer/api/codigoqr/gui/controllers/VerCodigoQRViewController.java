package bancomer.api.codigoqr.gui.controllers;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.base.SuiteApp;
import com.bancomer.base.callback.CallBackSession;
import com.tom_roush.pdfbox.pdmodel.PDDocument;
import com.tom_roush.pdfbox.pdmodel.PDPage;
import com.tom_roush.pdfbox.pdmodel.PDPageContentStream;
import com.tom_roush.pdfbox.pdmodel.common.PDRectangle;
import com.tom_roush.pdfbox.pdmodel.graphics.image.JPEGFactory;
import com.tom_roush.pdfbox.pdmodel.graphics.image.PDImageXObject;
import com.tom_roush.pdfbox.util.PDFBoxResourceLoader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bancomer.api.codigoqr.R;
import bancomer.api.codigoqr.config.SuiteAppCodigoQRApi;
import bancomer.api.codigoqr.gui.delegates.VerCodigoQRDelegate;
import bancomer.api.codigoqr.implementations.BaseViewController;
import bancomer.api.codigoqr.implementations.SincronizarSesion;
import bancomer.api.codigoqr.utils.QRConstants;
import bancomer.api.codigoqr.utils.QRGeneratorUtils;
import bancomer.api.common.commons.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class VerCodigoQRViewController extends BaseViewController implements CallBackSession{

    private VerCodigoQRDelegate verCodigoQRDelegate;

    private LinearLayout layoutACapturar;
    private TextView labelValorCuenta;
    private TextView labelValorNombre;
    private LinearLayout layoutContenedorImporte;
    private TextView labelValorImporte;
    private LinearLayout layoutContenedorMotivo;
    private TextView labelValorMotivo;
    private ImageView imageCodigoQR;
    private CallBackSession previousCallBackSession;
    Bitmap capture;
    File imagefile;
    File pdffile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_codigo_qr);
        capture = null;

        setParentViewsController(SuiteAppCodigoQRApi.getInstance().getBmovilApplication().getBmovilViewsController());
        previousCallBackSession = SuiteAppCodigoQRApi.getCallBackSession();
        verCodigoQRDelegate = (VerCodigoQRDelegate) parentViewsController.getBaseDelegateForKey(VerCodigoQRDelegate.VER_CODIGO_QR_DELEGATE_ID);
        verCodigoQRDelegate.setVerCodigoQRViewController(this);

        findViews();
    }

    private void findViews() {
        layoutACapturar = (LinearLayout) findViewById(R.id.layout_a_capturar);
        labelValorCuenta = (TextView) findViewById(R.id.label_valor_cuenta);
        labelValorNombre = (TextView) findViewById(R.id.label_valor_nombre);
        layoutContenedorImporte = (LinearLayout) findViewById(R.id.layout_contenedor_importe);
        labelValorImporte = (TextView) findViewById(R.id.label_valor_importe);
        layoutContenedorMotivo = (LinearLayout) findViewById(R.id.layout_contenedor_motivo);
        labelValorMotivo = (TextView) findViewById(R.id.label_valor_motivo);
        imageCodigoQR = (ImageView) findViewById(R.id.image_codigo_qr);
    }

    @Override
    public void onResume() {
        super.onResume();
        SuiteAppCodigoQRApi.setCallBackSession(previousCallBackSession);
        loadData();
    }

    private void loadData() {
        labelValorCuenta.setText(verCodigoQRDelegate.getNumeroCuenta());
        labelValorNombre.setText(verCodigoQRDelegate.getNombreTitular().substring(0, verCodigoQRDelegate.getNombreTitular().indexOf(' ')));
        if (verCodigoQRDelegate.isImporteVacio()) {
            layoutContenedorImporte.setVisibility(View.GONE);
        } else {
            layoutContenedorImporte.setVisibility(View.VISIBLE);
            labelValorImporte.setText(verCodigoQRDelegate.getImporte());
        }
        if (verCodigoQRDelegate.isMotivoVacio()) {
            layoutContenedorMotivo.setVisibility(View.GONE);
        } else {
            layoutContenedorMotivo.setVisibility(View.VISIBLE);
            labelValorMotivo.setText(verCodigoQRDelegate.getMotivo());
        }

        imageCodigoQR.setImageBitmap(verCodigoQRDelegate.getQRImage(imageCodigoQR.getLayoutParams().width));
    }

    public void clickAtras(View view) {
        parentViewsController.removeDelegateFromHashMap(VerCodigoQRDelegate.VER_CODIGO_QR_DELEGATE_ID);
        super.goBack();
    }

    public void clickCompartir(View view) {
        layoutACapturar.setDrawingCacheEnabled(true);
        layoutACapturar.buildDrawingCache(true);
        this.capture = Bitmap.createBitmap(layoutACapturar.getDrawingCache());
        imagefile = QRGeneratorUtils.saveImageQR(this, this.capture, QRConstants.NAME_IMAGES_QR);
        final Uri imageUri = Uri.fromFile(imagefile);
        final int pageNumber = 1;
        OutputStream os;
        try {
            final File pdfDirPath = new File(QRConstants.PATH_IMAGES_QR, QRConstants.FOLDER_IMAGES_QR);
            pdfDirPath.mkdirs();
            pdffile = new File(pdfDirPath, QRConstants.NAME_PDF_QR);
            pdffile.createNewFile();
            PDFBoxResourceLoader.init(getApplicationContext());
            final PDDocument documentPDF = new PDDocument();
            final PDPage page = new PDPage(new PDRectangle(layoutACapturar.getWidth(),
                    layoutACapturar.getHeight()));
            documentPDF.addPage(page);
            final PDPageContentStream pdPageContentStream = new PDPageContentStream(documentPDF, page);
            final PDImageXObject imageXObject = JPEGFactory.createFromImage(documentPDF, capture);
            pdPageContentStream.drawImage(imageXObject, 0, 0);
            pdPageContentStream.close();
            documentPDF.save(pdffile);
            documentPDF.close();
            final Uri contentUri = Uri.fromFile(pdffile);
            SuiteAppCodigoQRApi.appContext = this;
            SuiteAppCodigoQRApi.setCallBackSession(this);
            SuiteApp.appContext = this;
            getParentViewsController().setActivityChanging(true);
            final Intent emailIntent = new Intent();
            emailIntent.setAction(Intent.ACTION_SEND);
            emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(getString(R.string.share_email)));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_email_subject));
            emailIntent.setType(QRConstants.TYPE_PDF);
            final PackageManager pm = getPackageManager();
            final Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType(QRConstants.IMAGE_ALL);
            final Intent openInChooser = Intent.createChooser(emailIntent, getString(R.string.share_choice_option));
            final List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
            final List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
            for (int i = 0; i < resInfo.size(); i++) {
                // Extract the label, append it, and repackage it in a LabeledIntent
                final ResolveInfo ri = resInfo.get(i);
                final String packageName = ri.activityInfo.packageName;
                if(packageName.contains(QRConstants.ANDROID_EMAIL) || packageName.contains(QRConstants.ANDROID_GM)) {
                    emailIntent.setPackage(packageName);
                    emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    emailIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                } else {
                    final Intent intent = new Intent();
                    intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType(QRConstants.TYPE_IMAGE_JPEG);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Intent.EXTRA_STREAM, imageUri);
                    intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
                }
            }
                // convert intentList to array
            final LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);
            openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
            startActivity(openInChooser);
        } catch (IOException e) {
            throw new RuntimeException(getString(R.string.error_generateqr), e);
        }
        layoutACapturar.setDrawingCacheEnabled(false);
    }

    public void clickDescargar(View view) {
        layoutACapturar.setDrawingCacheEnabled(true);
        layoutACapturar.buildDrawingCache(true);
        this.capture = Bitmap.createBitmap(layoutACapturar.getDrawingCache());
        layoutACapturar.setDrawingCacheEnabled(false);
        final Date now = new Date();
        android.text.format.DateFormat.format(QRConstants.FORMAT_DATE, now);
        QRGeneratorUtils.saveImageQR(this, this.capture, now.getTime() + ".jpg", Boolean.TRUE);
        Toast.makeText(getApplicationContext(), getString(R.string.download_complete), Toast.LENGTH_SHORT).show();

    }

    public void clickPersonalizar(View view) {
        verCodigoQRDelegate.showPersonalizarQR();
    }


    @Override
    public void userInteraction() {
        SuiteAppCodigoQRApi.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();
    }

    @Override
    public void cierraSesionBackground(Boolean isChanging) {
        SincronizarSesion.getInstance().SessionApiToSession(SuiteAppCodigoQRApi.appContext);
        getParentViewsController().setCurrentActivityApp(this);
        getParentViewsController().setActivityChanging(isChanging);
        /*SuiteAppCodigoQRApi.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(isChanging);
        SuiteAppCodigoQRApi.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this);
        SuiteAppCodigoQRApi.getInstance().getBmovilApplication().setApplicationInBackground(false);*/
    }

    @Override
    public void cierraSesion() {
        SincronizarSesion.getInstance().SessionApiToSession(SuiteAppCodigoQRApi.appContext);
        SuiteAppCodigoQRApi.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this);
        SuiteAppCodigoQRApi.getInstance().getBmovilApplication().setApplicationInBackground(false);
    }

    @Override
    public void returnMenuPrincipal() {
        SuiteAppCodigoQRApi.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
        SuiteAppCodigoQRApi.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal();
    }

    @Override
    public void closeSession(String userName, String ium, String client) {

    }

    @Override
    public void setClientProfile(Constants.Perfil perfil) {

    }
}
