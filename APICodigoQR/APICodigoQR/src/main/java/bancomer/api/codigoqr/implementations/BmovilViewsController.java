package bancomer.api.codigoqr.implementations;

import android.util.Log;

import java.util.ArrayList;

import bancomer.api.codigoqr.config.LeerCodigoQRResultHandler;
import bancomer.api.codigoqr.config.SuiteAppCodigoQRApi;
import bancomer.api.codigoqr.gui.controllers.LeerCodigoQRViewController;
import bancomer.api.codigoqr.gui.controllers.PersonalizarQRViewController;
import bancomer.api.codigoqr.gui.controllers.VerCodigoQRViewController;
import bancomer.api.codigoqr.gui.delegates.LeerCodigoQRDelegate;
import bancomer.api.codigoqr.gui.delegates.PersonalizarQRDelegate;
import bancomer.api.codigoqr.gui.delegates.VerCodigoQRDelegate;
import bancomer.api.codigoqr.models.OperacionQR;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class BmovilViewsController extends BaseViewsController {

    //AMZ
    public ArrayList<String> estados = new ArrayList<String>();

    public void cierraViewsController() {
        clearDelegateHashMap();
        super.cierraViewsController();
    }

    public void cerrarSesionBackground() {
        SuiteAppCodigoQRApi.getInstance().getBmovilApplication()
                .setApplicationInBackground(true);
    }

    public void showMenuPrincipal() {
        SuiteAppCodigoQRApi.getInstance().getBmovilApplication()
                .setApplicationLogged(true);
        if (SuiteAppCodigoQRApi.getCallBackSession() != null) {
            if (ServerCommons.ALLOW_LOG) {
                Log.d(getClass().getSimpleName(), "Se ejecuta returnMenuPrincipal desde CallBackSession");
            }
            SuiteAppCodigoQRApi.getCallBackSession().returnMenuPrincipal();
        } else {
            if (ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "CallBackSession == null");
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public boolean consumeAccionesDeReinicio() {
        return false;
    }

    @Override
    public boolean consumeAccionesDePausa() {
        return false;
    }

    @Override
    public boolean consumeAccionesDeAlto() {
        if (!SuiteAppCodigoQRApi.getInstance().getBmovilApplication().isChangingActivity()
                && currentViewControllerApp != null) {
            currentViewControllerApp.hideSoftKeyboard();
        }
        SuiteAppCodigoQRApi.getInstance().getBmovilApplication()
                .setChangingActivity(false);
        return true;
    }

    @Override
    public void removeDelegateFromHashMap(long key) {
        super.removeDelegateFromHashMap(key);
    }

    public void touchAtras() {
        int ultimo = estados.size() - 1;

        if (ultimo >= 0) {
            String ult = estados.get(ultimo);
            if (ult == "reactivacion" || ult == "activacion" || ult == "menu token" || ult == "contratacion datos" || ult == "activacion datos") {
                estados.clear();
            } else {
                estados.remove(ultimo);
            }
        }
    }

    public void showVerCodigoQR(OperacionQR operacionQR) {
        VerCodigoQRDelegate verCodigoQRDelegate = new VerCodigoQRDelegate();
        addDelegateToHashMap(VerCodigoQRDelegate.VER_CODIGO_QR_DELEGATE_ID, verCodigoQRDelegate);

        verCodigoQRDelegate.setOperacionQR(operacionQR);

        showViewController(VerCodigoQRViewController.class, 0);
    }

    public void showPersonalizarQR(OperacionQR operacionQR) {
        PersonalizarQRDelegate personalizarQRDelegate = new PersonalizarQRDelegate();
        addDelegateToHashMap(PersonalizarQRDelegate.PERSONALIZAR_CODIGO_QR_DELEGATE_ID, personalizarQRDelegate);

        personalizarQRDelegate.setOperacionQR(operacionQR);

        showViewController(PersonalizarQRViewController.class, 0);
    }

    public void showLeerCodigoQR(LeerCodigoQRResultHandler leerCodigoQRResultHandler) {
        LeerCodigoQRDelegate leerCodigoQRDelegate = new LeerCodigoQRDelegate();
        addDelegateToHashMap(LeerCodigoQRDelegate.LEER_CODIGO_QR_DELEGATE_ID, leerCodigoQRDelegate);

        leerCodigoQRDelegate.setLeerCodigoQRResultHandler(leerCodigoQRResultHandler);

        showViewController(LeerCodigoQRViewController.class, 0);
    }

}