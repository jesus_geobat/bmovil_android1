package bancomer.api.codigoqr.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;

import bancomer.api.codigoqr.R;
import bancomer.api.codigoqr.config.SuiteAppCodigoQRApi;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by andres.vicentelinare on 07/10/2016.
 */
public class QRGeneratorUtils {

    public static Bitmap encodeAsBitmap(String string, int sideLength) {
        BitMatrix result;
        try {
            Map<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            hints.put(EncodeHintType.MARGIN, 1); /* default = 4 */
            result = new MultiFormatWriter().encode(string,
                    BarcodeFormat.QR_CODE, sideLength, sideLength, hints);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }

        int colorQRImage = SuiteAppCodigoQRApi.appContext.getResources().getColor(R.color.color_QR_image);
        int colorBackground = SuiteAppCodigoQRApi.appContext.getResources().getColor(R.color.color_QR_background);

        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];

        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? colorQRImage : colorBackground;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, sideLength, 0, 0, width, height);
        return bitmap;
    }

    public static File saveImageQR(final Context context, final Bitmap imageQR, final String nombreQR) {
        return saveImageQR(context, imageQR, nombreQR, Boolean.FALSE);
    }

    public static File saveImageQR(final Context context, final Bitmap imageQR, final String nombreQR,
                                   final boolean addGallery) {
        final File jpgDirPath = new File(QRConstants.PATH_IMAGES_QR, QRConstants.FOLDER_IMAGES_QR);
        jpgDirPath.mkdirs();
        final File imageFile = new File(jpgDirPath, nombreQR);
        Uri imageUri = null;
        FileOutputStream imos = null;
        try {
            imageFile.createNewFile();
            imos = new FileOutputStream(imageFile);
            imageQR.compress(Bitmap.CompressFormat.JPEG, 100, imos);
            imos.flush();
            imageUri = Uri.fromFile(imageFile);
            imos.close();
        } catch (IOException e) {
            if (ServerCommons.ALLOW_LOG) {
                Log.e("ExternalStorage", "Error al generar la imagen del QR", e);
            }
        }
        if (addGallery) {
            addImageToGallery(context, imageFile);
        }
        return imageFile;
    }

    /**
     * Agrega la imagen a la galeria
     *
     * @param imageFile Ruta donde esta la imagen
     */
    private static void addImageToGallery(final Context context, final File imageFile) {
        MediaScannerConnection.scanFile(context,
                new String[]{imageFile.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(final String path, final Uri uri) {
                        if (ServerCommons.ALLOW_LOG) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    }
                });
    }

}
