package bancomer.api.codigoqr.config;

import bancomer.api.codigoqr.models.OperacionQR;

/**
 * Created by andres.vicentelinare on 11/10/2016.
 */
public interface LeerCodigoQRResultHandler {

    void gestionaResultadoLecturaQR(OperacionQR operacionQR);

}
