package bancomer.api.codigoqr.gui.delegates;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import bancomer.api.codigoqr.R;
import bancomer.api.codigoqr.config.LeerCodigoQRResultHandler;
import bancomer.api.codigoqr.config.SuiteAppCodigoQRApi;
import bancomer.api.codigoqr.gui.controllers.LeerCodigoQRViewController;
import bancomer.api.codigoqr.models.OperacionQR;
import bancomer.api.codigoqr.utils.EncodingUtils;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * Created by andres.vicentelinare on 06/10/2016.
 */
public class LeerCodigoQRDelegate extends BaseDelegateCommons {

    public final static long LEER_CODIGO_QR_DELEGATE_ID = 0x0b3c85abc5b4e3d0L;

    private LeerCodigoQRViewController leerCodigoQRViewController;

    private LeerCodigoQRResultHandler leerCodigoQRResultHandler;

    public LeerCodigoQRViewController getLeerCodigoQRViewController() {
        return leerCodigoQRViewController;
    }

    public void setLeerCodigoQRViewController(LeerCodigoQRViewController leerCodigoQRViewController) {
        this.leerCodigoQRViewController = leerCodigoQRViewController;
    }

    public LeerCodigoQRResultHandler getLeerCodigoQRResultHandler() {
        return leerCodigoQRResultHandler;
    }

    public void setLeerCodigoQRResultHandler(LeerCodigoQRResultHandler leerCodigoQRResultHandler) {
        this.leerCodigoQRResultHandler = leerCodigoQRResultHandler;
    }

    public void decodeQR(String codigoQR) {
        OperacionQR operacionQR;
        try {
            String json = EncodingUtils.decrypt(codigoQR.trim());
            Gson gson = new Gson();
            operacionQR = gson.fromJson(json, OperacionQR.class);


        } catch (Exception e) {
            Log.e("Excepcion QR", e.getMessage(), e);
            Toast.makeText(leerCodigoQRViewController.getApplicationContext(),
                    leerCodigoQRViewController.getString(R.string.error_invalidqr),
                    Toast.LENGTH_SHORT).show();
            return;
        }

        leerCodigoQRResultHandler.gestionaResultadoLecturaQR(operacionQR);
        leerCodigoQRViewController.goBack();
    }

}
