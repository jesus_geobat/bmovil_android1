package bancomer.api.codigoqr.gui.delegates;

import android.graphics.Bitmap;

import com.google.gson.Gson;

import bancomer.api.codigoqr.gui.controllers.PersonalizarQRViewController;
import bancomer.api.codigoqr.models.OperacionQR;
import bancomer.api.codigoqr.utils.EncodingUtils;
import bancomer.api.codigoqr.utils.QRGeneratorUtils;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * Created by andres.vicentelinare on 21/10/2015.
 */
public class PersonalizarQRDelegate extends BaseDelegateCommons {

    public final static long PERSONALIZAR_CODIGO_QR_DELEGATE_ID = 0x0a3c8cfbc5b0e3d3L;

    private PersonalizarQRViewController personalizarQRViewController;
    private OperacionQR operacionQR;

    public PersonalizarQRViewController getPersonalizarQRViewController() {
        return personalizarQRViewController;
    }

    public void setPersonalizarQRViewController(PersonalizarQRViewController personalizarQRViewController) {
        this.personalizarQRViewController = personalizarQRViewController;
    }

    public OperacionQR getOperacionQR() {
        return operacionQR;
    }

    public void setOperacionQR(OperacionQR operacionQR) {
        this.operacionQR = operacionQR;
    }

    public String getNumeroCuenta() {
        return Tools.hideAccountNumber(operacionQR.getCuentaDestino());
    }

    public String getNombreTitular() {
        return operacionQR.getNombreTitular();
    }

    public Bitmap getQRImage(int sideLength) {
        Gson gson = new Gson();
        String json = gson.toJson(operacionQR);
        String base64String = EncodingUtils.encrypt(json.trim());
        return QRGeneratorUtils.encodeAsBitmap(base64String, sideLength);
    }

    public void saveData(String importe, String motivo) {
        operacionQR.setImporte(importe);
        operacionQR.setMotivoPago(motivo);
    }

}
