package bancomer.api.codigoqr.utils;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

import bancomer.api.codigoqr.config.SuiteAppCodigoQRApi;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

/**
 * Created by andres.vicentelinare on 24/08/2016.
 */
public class ScreenUtils {

    public static boolean isScreenOn() {
        try {
            PowerManager pm = (PowerManager) SuiteAppCodigoQRApi.appContext.getSystemService(Context.POWER_SERVICE);
            return pm.isScreenOn();
        } catch (Exception ex) {
            if (Server.ALLOW_LOG)
                Log.wtf("EclairPowerManager", "Error while getting the state of the screen.", ex);
            return false;
        }
    }

}
