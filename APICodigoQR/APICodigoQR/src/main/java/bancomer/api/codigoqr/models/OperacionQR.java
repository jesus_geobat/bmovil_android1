package bancomer.api.codigoqr.models;

/**
 * Created by andres.vicentelinare on 07/10/2016.
 */
public class OperacionQR {

    private String cuentaDestino;
    private String nombreTitular;
    private String importe;
    private String motivoPago;
    private String tipoCuentaDestino;

    public OperacionQR(String cuentaDestino, String nombreTitular, String tipoCuentaDestino) {
        this.cuentaDestino = cuentaDestino;
        this.nombreTitular = nombreTitular;
        this.importe = "";
        this.motivoPago = "";
        this.tipoCuentaDestino = tipoCuentaDestino;
    }

    public String getTipoCuentaDestino() {
        return tipoCuentaDestino;
    }

    public void setTipoCuentaDestino(String tipoCuentaDestino) {
        this.tipoCuentaDestino = tipoCuentaDestino;
    }

    public void setNombreTitular(String nombreTitular) {
        this.nombreTitular = nombreTitular;
    }

    public String getCuentaDestino() {
        return cuentaDestino;
    }

    public void setCuentaDestino(String cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    public String getNombreTitular() {
        return nombreTitular;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getMotivoPago() {
        return motivoPago;
    }

    public void setMotivoPago(String motivoPago) {
        this.motivoPago = motivoPago;
    }
}
