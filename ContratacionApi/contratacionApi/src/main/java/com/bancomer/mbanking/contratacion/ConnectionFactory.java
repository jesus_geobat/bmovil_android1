package com.bancomer.mbanking.contratacion;

import android.util.Log;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;

import com.bancomer.base.SuiteApp;
import com.google.gson.Gson;

import suitebancomer.aplicaciones.bmovil.classes.model.contratacion.FirmaData;
import suitebancomer.aplicaciones.commservice.commons.BodyType;
import suitebancomer.aplicaciones.commservice.commons.ContentType;
import suitebancomer.aplicaciones.commservice.commons.MethodType;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;

public class ConnectionFactory {

	private int operationId;
	private Hashtable<String, ?> params;
	private Object responseObject;
	private ParametersTO parameters;
	private boolean isJson;

	public ConnectionFactory(final int operationId, final Hashtable<String, ?> params,
							 final boolean isJson, final Object responseObject) {
		super();
		this.operationId = operationId;
		this.params = params;
		this.responseObject = responseObject;
		this.isJson = isJson;
	}

	public IResponseService processConnectionWithParams() throws Exception {

		final Integer opId = Integer.valueOf(operationId);
		parameters = new ParametersTO();
		parameters.setSimulation(Server.SIMULATION);
		if (!Server.SIMULATION) {
			parameters.setProduction(!Server.DEVELOPMENT);
			parameters.setDevelopment(Server.DEVELOPMENT);
		}
		parameters.setOperationId(opId.intValue());
		parameters.setParameters(params.clone());
		parameters.setJson(isJson);
		if(operationId == Constants.ENVIO_FIRMA_CONTRATACION){

			if (suitebancomercoms.aplicaciones.bmovil.classes.io.Server.ALLOW_LOG)
				Log.d("SERVER", "processConnectionWithParams: Envio firma");

			Hashtable<String, String> jsonObject = new Hashtable<String, String>();
			String json = "";

			String telefono = params.get(bancomer.api.common.commons.Constants.NUMERO_CELULAR).toString();
			String base64 = params.get(bancomer.api.common.commons.Constants.FIRMA_CADENA).toString();
			String cardNumber = params.get(bancomer.api.common.commons.Constants.CARD_NUMBER).toString();

			if (suitebancomercoms.aplicaciones.bmovil.classes.io.Server.ALLOW_LOG){
				Log.d("SERVER", "processConnectionWithParams: "+Constants.CARD_NUMBER+" = ["+cardNumber+"]" );
				Log.d("SERVER", "processConnectionWithParams: "+Constants.TELEFONO_PARAM+" = ["+telefono+"]");
				Log.d("SERVER", "processConnectionWithParams: "+Constants.FIRMA_PARAM+" = ["+base64+"]");
			}


			jsonObject.put(Constants.CARD_NUMBER,cardNumber);
			jsonObject.put(Constants.TELEFONO_PARAM, telefono);
			jsonObject.put(Constants.FIRMA_PARAM,base64);

			final Gson gson = new Gson();
			json = gson.toJson(jsonObject);
			parameters.setMethodType(MethodType.POST);
			parameters.setBodyRaw(json);
			parameters.setBodyType(BodyType.RAW);
			parameters.setAddCadena(Boolean.FALSE);
			final Map<String, String> headers = new HashMap<String, String>();
			headers.put(HTTP.CONTENT_TYPE, ContentType.JSON.getContentType());
			parameters.setHeaders(headers);
			parameters.setParameters(null);
			parameters.setParameters(new Hashtable<String,String>());
			parameters.setForceArq(true);
			responseObject = FirmaData.class;

		}
		IResponseService resultado = new ResponseServiceImpl();
		final ICommService serverproxy = new CommServiceProxy(SuiteApp.appContext);
		try {
			resultado = serverproxy.request(parameters,
					responseObject == null ? new Object().getClass()
							: responseObject.getClass());
			// ConsultaEstatusMantenimientoData
			// estatusM=(ConsultaEstatusMantenimientoData)resultado.getObjResponse();
		} catch (UnsupportedEncodingException e) {
			throw new Exception(e);
		} catch (ClientProtocolException e) {
			throw new Exception(e);
		} catch (IllegalStateException e) {
			throw new Exception(e);
		} catch (IOException e) {
			throw new Exception(e);
		} catch (JSONException e) {
			throw new Exception(e);
		}

		return resultado;

	}

	public ServerResponse parserConnection(final IResponseService resultado,
										   final ParsingHandler handler) throws Exception {
		ServerResponse response=null;
		ParsingHandler handlerP=null;
		if (ApiConstants.OPERATION_SUCCESSFUL == resultado.getStatus()) {
			if(resultado.getObjResponse() instanceof ParsingHandler){
				handlerP= (ParsingHandler) resultado.getObjResponse();
			}
			if (parameters.isJson()) {
				if (handler != null) {
					final ParserJSON parser = new ParserJSON(
							resultado.getResponseString());
					try {
						handlerP.process(parser);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw new Exception(e);
					} catch (ParsingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw new Exception(e);
					}
				}
				response = new ServerResponse(resultado.getStatus(),
						resultado.getMessageCode(), resultado.getMessageText(), handlerP);
			} else {
				// Que pasa si no es JSON la respuesta
				Reader reader = null;
				try {
					if (handler != null) {
						reader = new StringReader(resultado.getResponseString());
						final Parser parser = new Parser(reader);
						if(handlerP==null){
							final Class<?> clazz = handler.getClass();
							final Constructor<?> ctor = clazz.getConstructor();
							handlerP = (ParsingHandler) ctor.newInstance();
                		}
						if(operationId == Constants.ENVIO_FIRMA_CONTRATACION){
							FirmaData firmaData = new FirmaData();
							firmaData.setResponse(resultado.getResponseString());
							response = new ServerResponse(firmaData);
						}else{
							response= new ServerResponse(handlerP);
							response.process(parser);
						}
					}else{
						response= new ServerResponse(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
					}
				}catch(Exception e){
					throw new Exception(e);
					
				}finally {
					if (reader != null) {
						try {
							reader.close();
						} catch (Throwable ignored) {
							throw new Exception(ignored);
							
						}
					}
				}
			}

		}else{
			try {
				response= new ServerResponse(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
			} catch (Exception e) {
				// TODO: handle exception
				throw new Exception(e);
			}
			
		}
		
		return response;
	}

}
