package suitebancomer.classes.gui.delegates.contratacion;

import java.util.Hashtable;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomer.classes.gui.controllers.contratacion.BaseViewController;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

public class BaseDelegate extends BaseDelegateCommons {

	
	/**
	 * Invoke a network operation, controlling that it is called after all previous
	 * repaint events have been completed
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 */
	public void doNetworkOperation(final int operationId, final Hashtable<String,?> params,boolean isJson, final ParsingHandler handler, final BaseViewControllerCommons caller) {}
	
	/**
     * 
     * This function has to be overridden by child members, it implements
     * the processing for server response.
     * 
     * @param operationId
     * @param response
     */
    public void analyzeResponse(final int operationId, final ServerResponse response) {}
	
    public void performAction(final Object obj) {}
    
    public long getDelegateIdentifier() { return 0; }
}
