package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.contratacion.R;
import com.bancomer.mbanking.contratacion.SuiteAppContratacion;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionDelegate;
import suitebancomer.classes.gui.controllers.contratacion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.FirmaAutografaCallback;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.delegates.FirmaDelegate;

public class FirmaViewController extends BaseViewController implements View.OnClickListener{

    private TextView aceptar_terminos_label,aceptar_terminos_link,firmar_title;
    private CheckBox acepto_terminos_checkbox;
    private Button continuar;
    //    private ImageView ic_firma;
    public static final Integer SIGNATURE = 147;
    /* componente firma */
    FirmaDelegate firma = new FirmaDelegate(this);
    private String base64 = "";
    private ContratacionDelegate delegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firma_view_controller_contratacion);

        SuiteApp.appContext = this;
        setParentViewsController(SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController());
        setDelegate((ContratacionDelegate) parentViewsController.getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID));

        aceptar_terminos_label = (TextView)findViewById(R.id.aceptar_terminos_label);
        aceptar_terminos_link = (TextView)findViewById(R.id.aceptar_terminos_link);
        firmar_title = (TextView)findViewById(R.id.firmar_title);
        acepto_terminos_checkbox = (CheckBox)findViewById(R.id.acepto_terminos_checkbox);
        continuar = (Button)findViewById(R.id.continuar);
        LinearLayout firmar_layout = (LinearLayout) findViewById(R.id.firmar_layout);

        firma.generateViewSignature(firmar_layout,SIGNATURE);

        scaleToScreenSize();
        firmar_title.setOnClickListener(this);
        continuar.setOnClickListener(this);
        aceptar_terminos_link.setOnClickListener(this);

        delegate = (ContratacionDelegate)getDelegate();
        delegate.setOwnerController(this);

    }

    /**
     * Escala el layout al tamaño de la pantalla.
     */
    private void scaleToScreenSize() {
        final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(aceptar_terminos_link, true);
        guiTools.scale(aceptar_terminos_label, true);
        guiTools.scale(firmar_title, true);
        guiTools.scale(continuar, true);
    }

    @Override
    public void onClick(View v) {
        if (v == aceptar_terminos_link){
            delegate.consultarTerminosDeUso();
        }
        if (v == continuar){
            if (!acepto_terminos_checkbox.isChecked()){
                showMensajeErrorTerminos(getString(R.string.contratacion_autenticacion_aceptar_terminos_y_condiciones_falta));
                return;
            }
            if (base64.equals("")){
                showMensajeErrorTerminos(getString(R.string.contratacion_autenticacion_aceptar_firma_falta));
                return;
            }
            delegate.peticionFirma(base64);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            FirmaAutografaCallback.getInstance().setCallBackSession(null);
            FirmaAutografaCallback.getInstance().setCallBackBConnect(null);
            setResult(RESULT_CANCELED, new Intent());
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == SIGNATURE){
                base64 = firma.returnViewSignature(data.getExtras());
                if (ServerCommons.ALLOW_LOG)
                    Log.i("TAG", "onActivityResult: " + base64);
            }
        }
    }

    /* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
    @Override
    public void processNetworkResponse(final int operationId, final ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

    @Override
    public void onUserInteraction() {
        if(ServerCommons.ALLOW_LOG) {
            Log.i(getClass().getSimpleName(), "User Interacted firma Autografa");
        }
        if(FirmaAutografaCallback.getInstance().getCallBackSession() != null) {
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Envia peticion de reinicio a bmovil desde CallBackSession");
            }
            FirmaAutografaCallback.getInstance().getCallBackSession().userInteraction();
        }
        if( FirmaAutografaCallback.getInstance().getCallBackBConnect() != null) {
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Envia peticion de reinicio a bmovil desde CallBackBconnect");
            }
            FirmaAutografaCallback.getInstance().getCallBackBConnect().userInteraction();
        }
        super.onUserInteraction();
    }

    private void alertDialog(String title, String message, String okText ){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(okText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog mAlertDialog = alertDialogBuilder.create();
        mAlertDialog.show();
    }

    public void showMensajeErrorTerminos(String message){
        showInformationAlert(message);
    }


    @Override
    protected void onResume() {
        super.onResume();
        SuiteApp.appContext = this;
    }
}
