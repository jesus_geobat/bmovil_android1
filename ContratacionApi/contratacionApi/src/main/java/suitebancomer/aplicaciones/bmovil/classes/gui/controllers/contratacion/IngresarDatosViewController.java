package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.contratacion.R;
import com.bancomer.mbanking.contratacion.SuiteAppContratacion;
import com.bbva.apicuentadigital.common.APICuentaDigital;

import java.util.ArrayList;

import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.contratacion.AbstractSuitePowerManager;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import suitebancomer.classes.gui.controllers.contratacion.BaseViewController;
import suitebancomer.classes.gui.views.contratacion.ListaDatosViewController;
import suitebancomer.classes.gui.views.contratacion.SeleccionHorizontalViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.controllers.PopUpAvisoPrivacidadCommon;

//import com.bbva.apicuentadigital.common.APICuentaDigital;


/**
 * Controlador de la pantalla para ingresar datos.
 */
public class IngresarDatosViewController extends BaseViewController {
	/**
	 * Delegado de la pantalla.
	 */
	private ContratacionDelegate delegate;
	
	/**
	 * Componente Lista Datos.
	 */
	private ListaDatosViewController listaDatos;
	
	/**
	 * Componente Seleccion Horizontal para elegir un compa�ia de celular.
	 */
	private SeleccionHorizontalViewController seleccionHorizontal;
	
	/**
	 * Campo de texto para el numero de tarjeta.
	 */
	private EditText tbNumeroTarjeta;
	
	/**
	 * Campo checkbox para selecci�n del tipo de usuario
	 */
	// CGI-Modif Contratacion
//	private CheckBox checkboxToken;
	
	/**
	 * Bandera para indicar si se debe de regresar a la pantalla de inicio en el evento onResume.
	 */
	private boolean goBackToHome;
	
	/**
	 * Bandera para indicar si vino de IngresaDatosST.
	 */
	private boolean cameFromIngresaDatosST = false;


	/**
	 * Textview clickeable que muestra el aviso de privacidad
	 */
	private TextView consultaAvisoPrivacidad;

	//AMZ
	public ArrayList<String> estados = new ArrayList<String>();
	public IngresarDatosViewController(){
		super();
		
	}
	
	public IngresarDatosViewController(final boolean cameFromIngresaDatosST){
		super();
		this.cameFromIngresaDatosST = cameFromIngresaDatosST;
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#onCreate(android.os.Bundle)
	 */
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, 
				getResources().getIdentifier("layout_bmovil_ingresar_datos_api", "layout", getPackageName()));
		setTitle(getResources().getIdentifier("bmovil.contratacion.titulo", "string", getPackageName()),
				getResources().getIdentifier("icono_activacion", "drawable", getPackageName()));
		SuiteApp.appContext = this;
		setParentViewsController(SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((ContratacionDelegate) parentViewsController.getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID));
		delegate = (ContratacionDelegate)getDelegate();
		delegate.setOwnerController(this);
		delegate.setPaso(ContratacionDelegate.AvanceContratacion.IngresarNumTarjeta);
		delegate.setPaso(ContratacionDelegate.AvanceContratacion.TokenUsuario);
		init();

		TrackingHelper.trackState("ingresodatos", estados);
	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		super.onPause();
		goBackToHome = !AbstractSuitePowerManager.getSuitePowerManager().isScreenOn();
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if(goBackToHome) {
			//SuiteAppContratacion.getInstance().getSuiteViewsController().showMenuSuite(true);
			SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(this);
			SuiteAppContratacion.getConsultaEstatusDesactivada().returnToActivity(SuiteAppContratacion.appContext,SuiteAppContratacion.getInstance().getActivityToReturn(), true);
			//SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());
		}
		getParentViewsController().setCurrentActivityApp(this);
		delegate.setOwnerController(this);
	}

	/**
	 * Inicializa los elementos de la pantalla.
	 */
	private void init() {
		findViews();
		cargarComponenteListaDatos();
		cargarComponenteSeleccionHorizontal();
		scaleToCurrentScreen();
		goBackToHome = false;
		// CGI-Modif Contratacion
		
		if(delegate.isDeleteData()) {
			delegate.deleteData();
		}
	}

	/**
	 * Busca todas las referencias necesarias para el control de la pantalla.
	 */
	private void findViews() {
		tbNumeroTarjeta = (EditText)findViewById(SuiteAppContratacion.getResourceId("tbNumeroTarjeta", "id"));
		consultaAvisoPrivacidad = (TextView) findViewById(SuiteAppContratacion.getResourceId("consulta_aviso_privacidad", "id"));
		consultaAvisoPrivacidad.setPaintFlags(consultaAvisoPrivacidad.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		// CGI-Modif Contratacion
//		checkboxToken = (CheckBox)findViewById(R.id.switchConToken);
	}
	
	/**
	 * Escala los elementos de la pantalla para la resoluci�n actual.
	 */
	private void scaleToCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("layoutListaDatos", "id")));
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("lblCompaniaCelular", "id")), true);
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("lblNumeroTarjeta", "id")), true);
		guiTools.scale(tbNumeroTarjeta, true);
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("btnContinuar", "id")), false);
//		guiTools.scale(findViewById(R.id.lblInstruccionesNumeroTarjeta), true);
//		guiTools.scale(findViewById(R.id.LayoutOperarToken));
//		guiTools.scale(findViewById(R.id.lblBancaMovilToken), true);
//		guiTools.scale(findViewById(R.id.informacionBmovil), false);
//		guiTools.scale(findViewById(R.id.switchConToken), false);
		guiTools.scale(consultaAvisoPrivacidad, true);

	}
	
	/**
	 * Carga el componente lista datos con los elementos necesarios. 
	 */
	private void cargarComponenteListaDatos() {
		final ArrayList<Object> datos = delegate.cargarElementosListaDatos();

		final LinearLayout.LayoutParams layoutParams = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		listaDatos = new ListaDatosViewController(SuiteAppContratacion.appContext, layoutParams, getParentViewsController());
		listaDatos.setTitulo(SuiteAppContratacion.getResourceId("bmovil.contratacion.ingresar.datos.ingresar.datos", "string"));
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.showLista();

		final LinearLayout listaDatosLayout = (LinearLayout)findViewById(SuiteAppContratacion.getResourceId("layoutListaDatos", "id"));
		listaDatosLayout.addView(listaDatos);
	}
	
	/**
	 * Carga el componente seleccion horizontal con los elementos necesarios.
	 */
	private void cargarComponenteSeleccionHorizontal() {
		final LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		final ArrayList<Object> companias = delegate.cargarCompaniasSeleccionHorizontal();
		seleccionHorizontal = new SeleccionHorizontalViewController(this, params, companias, getDelegate(), false);

		final LinearLayout seleccionHorizontalLayout = (LinearLayout)findViewById(SuiteAppContratacion.getResourceId("layoutSeleccionHorizontal", "id"));
		seleccionHorizontalLayout.addView(seleccionHorizontal);
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

	/**
	 * Intenta continuar con el flujo del caso de uso.
	 * @param sender La vista que invoca este método.
	 */
	public void botonContinuarClick(final View sender) {
		// CGI-Modif Contratacion
		delegate.validaCampos(seleccionHorizontal.getSelectedItem(), tbNumeroTarjeta.getText().toString(), false);//checkboxToken.isChecked());
	}

	/**
	 * llamar al api cuenta digital
	 * @param sender La vista que invoca este método.*/

	public void botonQuieroClick(final View sender) {
		final Compania compania= (Compania)seleccionHorizontal.getSelectedItem();
		if(compania!=null){
			final String celular= delegate.getConsultaEstatus().getNumCelular();
			final String companiaTel=compania.getNombre();
			final APICuentaDigital getInstance = new APICuentaDigital();

			getInstance.ejecutaAPICuentaDigital(this,celular, companiaTel, Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);

		}else{
			showErrorMessage(R.string.valida_compania_telefonica_contratacion);
		}

	}

	/**
	 * Carga la pantalla de informaci�n sobre tipos de usuario
	 */
    //IDS CAMBIOS EA#14
	//public void botonInformacionClick(final View sender) {
	//	delegate.mostrarInformacion();
	//}
	
    /**
     * Places a progress dialog, for long waiting processes.
     * 
     * @param strTitle the dialog title
     * @param strMessage the message to show in the while
     */
	@Override
    public void muestraIndicadorActividad(final String strTitle, final String strMessage) {
    	if(!cameFromIngresaDatosST){
			if(mProgressDialog != null){
	    		ocultaIndicadorActividad();
	    	}	
			mProgressDialog = ProgressDialog.show(this, strTitle, 
	    			strMessage, true);
			mProgressDialog.setCancelable(false);
    	}else{
    		cameFromIngresaDatosST=false;
    	}
    }

	public ContratacionDelegate getContratacionDelegate() {
		return delegate;
	}

	public void setContratacionDelegate(final ContratacionDelegate delegate) {
		this.delegate = delegate;
	}


	/**
	 * Muestra los terminos y condiciones.
	 */
	public void onConsultaAvisoPrivacidadLinkClik(final View sernder) {
		Intent intent = new Intent();
		intent.setClass(this, PopUpAvisoPrivacidadCommon.class);
		this.startActivityForResult(intent, 1234);

	}

}
