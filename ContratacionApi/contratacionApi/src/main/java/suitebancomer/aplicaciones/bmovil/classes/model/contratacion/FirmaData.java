package suitebancomer.aplicaciones.bmovil.classes.model.contratacion;

import android.util.Log;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * BMovil_v940
 * Created by terry0022 on 07/09/16 - 9:14 PM.
 */
public class FirmaData implements ParsingHandler {
    private static final String TAG = "FirmaData";

    String response;

    public FirmaData() {}

    public void setResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        Log.e(TAG, "process: ");

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        Log.e(TAG, "process: ");
    }

}