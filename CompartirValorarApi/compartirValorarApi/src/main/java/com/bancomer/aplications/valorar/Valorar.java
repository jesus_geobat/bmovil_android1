package com.bancomer.aplications.valorar;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.bancomer.constants.compartirvalorarapi.Constants;
import com.bancomer.mbanking.compartirvalorarapi.R;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

/**
 * Created by alanmichaelgonzalez on 09/11/15.
 */
public class Valorar {

    public ArrayList<String> estados = new ArrayList<String>();

    final Context context;
     String urlMarqket;
     String urlWeb;
     String packageName="";

    /**
     * constructor
     * @param context
     */
    public Valorar(final Context context){
        this.context=context;
    }

    /**
     * metodo que inicia el api de valorar
     * @param aplicacionAValorar
     */
    public void startValorar(final Constants.Aplicaciones aplicacionAValorar){
        getApp(aplicacionAValorar);

      TrackingHelper.trackState("valorar", estados);

        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlMarqket)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlWeb)));
        }
    }

    /**
     * metodo para obtener el nombre del paquete y la url de
     * la aplicaion desde esl archivo string
     * @param aplicacionAValorar
     */
    private void getApp(final Constants.Aplicaciones aplicacionAValorar){
        if(Constants.Aplicaciones.BMOVIL==aplicacionAValorar){
            urlWeb=context.getString(R.string.url_bbva_bmovil);
            packageName=context.getString(R.string.package_bbva_bmovil);
        }else if(Constants.Aplicaciones.LINEABANCOMER==aplicacionAValorar){
            urlWeb=context.getString(R.string.url_linea_Bancomer);
             packageName=context.getString(R.string.package_linea_Bancomer);
        }else if(Constants.Aplicaciones.VIDABANCOMER==aplicacionAValorar){
            urlWeb=context.getString(R.string.url_vida_bancomer);
             packageName=context.getString(R.string.package_vida_bancomer);
        }else if(Constants.Aplicaciones.WALLET==aplicacionAValorar){
            urlWeb=context.getString(R.string.url_bbva_wallet);
            packageName=context.getString(R.string.package_bbva_wallet);
        }
        urlMarqket=context.getString(R.string.uri_prefix_app_market)+packageName;
    }


}
