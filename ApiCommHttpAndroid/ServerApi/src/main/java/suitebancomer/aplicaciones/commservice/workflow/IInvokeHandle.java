package suitebancomer.aplicaciones.commservice.workflow;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.IResponseService;


/**
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public interface IInvokeHandle {

	/**
	 * 
	 * @param request
	 * @throws UnsupportedEncodingException 
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws JSONException 
	 * @throws IllegalStateException 
	 */
	IResponseService handleRequest(final RequestService request, final Class<?> objResponse) 
			throws UnsupportedEncodingException, ClientProtocolException, 
			IOException, IllegalStateException, JSONException;

	IResponseService handleRequestImage(final RequestService request, final Class<?> objResponse)
			throws UnsupportedEncodingException, ClientProtocolException,
			IOException, IllegalStateException, JSONException;

	/**
	 * 
	 * @param successor
	 */
	void setNext(final IInvokeHandle successor);

	IInvokeHandle getNext();
	
	ParametersTO getParametesTO();
	
	void setParametesTO(final ParametersTO parametesTO);

}