package suitebancomer.aplicaciones.commservice.commons;

import java.util.List;
import java.util.Map;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants.isJsonValueCode;

/**
 * Clase que representa un Transfer Object para los parametros.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class ParametersTO {

	//parametros que se envian desde el delegate
	private Object parameters; 
	private int operationId;	//45 - consultaestatusmantenimiento
	private boolean isJson;
	private int provider;
	
	//parametros obtenidos en operacion
	private String operationPath;   ///eexd_mx_web/servlet/ServletOperacionWeb    /bm3wxp_mx_web/servlet/ServletOperacionWeb
	private String operationCode;   //146  //consultaEstatusMantenimiento
	private boolean isSimulation;   // Local
	private boolean isDevelopment;  // Test 
	private boolean isProduction;   // Prod
	private boolean isEmulator;     // emuladores
	private boolean requestCleanCoockies = false; //cleanCockies


	private isJsonValueCode isJsonValue;

    /**
     * Indica el tipo de Body
     *
     * FORM_DATA(0) form-data
     * URL_ENCODED(1) x-www-form-url-encoded
     * RAW(2) raw
     * BINARY(3) binary
     */
    private BodyType bodyType = BodyType.URL_ENCODED;
    /**
     * Indica el tipo de metdo a invocar
     *
     * POST
     * GET
     */
    private MethodType methodType = MethodType.POST;
    /**
     * Mapa para indicar los headers que se agregaran a la peticion
     */
    private Map<String, String> headers;
    /**
     * Guarda el raw que se anexara al body
     */
    private String bodyRaw;
    /**
     * Mapa que contiene los valores que se anexan a la url para los casos de las peticiones RESTFul
     */
    private Map<String, String> paramsUrl;
    /**
     * Indica si el parseo lo hara la clase server o el api
     */
    private boolean parseServer = Boolean.TRUE;
    /**
     * Lista que nos indica que parametros hay que encriptar
     */
    private List<String> listaEncriptar;
    /**
     * Bandera que indica si se agrega o no la cedana a la peticion
     */
    private boolean addCadena = Boolean.TRUE;
    /**
     * Bandera que indica se la peticion se forza para arquitectura
     */
    private boolean forceArq = Boolean.FALSE;
	 
	/**
	 * @return the operationId
	 */
	public final int getOperationId() {
		return operationId;
	}

	/**
	 * @param operationId the operationId to set
	 */
	public final void setOperationId(final int operationId) {
		this.operationId = operationId;
	}

	/**
	 * @return the isProduction
	 */
	public final boolean isProduction() {
		return isProduction;
	}

	/**
	 * @param isProduction the isProduction to set
	 */
	public final void setProduction(final boolean isProduction) {
		this.isProduction = isProduction;
	}
	
	/**
	 * @return the isSimulation
	 */
	public final boolean isSimulation() {
		return isSimulation;
	}

	/**
	 * @param isSimulation the isSimulation to set
	 */
	public final void setSimulation(final boolean isSimulation) {
		this.isSimulation = isSimulation;
	}
	
	/**
	 * @return the isJson
	 */
	public final boolean isJson() {
		return isJson;
	}

	/**
	 * @param isJson the isJson to set
	 */
	public final void setJson(final boolean isJson) {
		this.isJson = isJson;
	}


	/**
	 * @return the isDevelopment
	 */
	public final boolean isDevelopment() {
		return isDevelopment;
	}

	/**
	 * @param isDevelopment the isDevelopment to set
	 */
	public final void setDevelopment(final boolean isDevelopment) {
		this.isDevelopment = isDevelopment;
	}



	/**
	 * @return the isEmulator
	 */
	public final boolean isEmulator() {
		return isEmulator;
	}

	/**
	 * @param isEmulator the isEmulator to set
	 */
	public final void setEmulator(final boolean isEmulator) {
		this.isEmulator = isEmulator;
	}


	/**
	 * @return the isJsonValue
	 */
	public final isJsonValueCode getIsJsonValue() {
		return isJsonValue;
	}

	/**
	 * @param isJsonValue the isJsonValue to set
	 */
	public final void setIsJsonValue(final isJsonValueCode isJsonValue) {
		this.isJsonValue = isJsonValue;
	}
	
	/**
	 * @return the operationPath
	 */
	public final String getOperationPath() {
		return operationPath;
	}

	/**
	 * @param operationPath the operationPath to set
	 */
	public final void setOperationPath(final String operationPath) {
		this.operationPath = operationPath;
	}
	
	public Object getParameters() {
		return parameters;
	}


	public void setParameters(final Object parameters) {
		this.parameters = parameters;
	}

	public String getOperationCode() {
		return operationCode;
	}

	public void setOperationCode(final String operationCode) {
		this.operationCode = operationCode;
	}

	public int getProvider() {
		return provider;
	}

	public void setProvider(final int provider) {
		this.provider = provider;
	}


    /**
     * Gets the value of bodyType and returns bodyType
     *
     * @return Returns value of bodyType
     */
    public BodyType getBodyType() {
        return bodyType;
    }

    /**
     * Sets the bodyType
     * You can use getBodyType() to get the value of bodyType
     */
    public void setBodyType(final BodyType bodyType) {
        this.bodyType = bodyType;
    }

    /**
     * Gets the value of methodType and returns methodType
     *
     * @return Returns value of methodType
     */
    public MethodType getMethodType() {
        return methodType;
    }

    /**
     * Sets the methodType
     * You can use getMethodType() to get the value of methodType
     */
    public void setMethodType(final MethodType methodType) {
        this.methodType = methodType;
    }

    /**
     * Gets the value of headers and returns headers
     *
     * @return Returns value of headers
     */
    public Map<String, String> getHeaders() {
        return headers;
    }

    /**
     * Sets the headers
     * You can use getHeaders() to get the value of headers
     */
    public void setHeaders(final Map<String, String> headers) {
        this.headers = headers;
    }

    /**
     * Gets the value of bodyRaw and returns bodyRaw
     *
     * @return Returns value of bodyRaw
     */
    public String getBodyRaw() {
        return bodyRaw;
    }

    /**
     * Sets the bodyRaw
     * You can use getBodyRaw() to get the value of bodyRaw
     */
    public void setBodyRaw(final String bodyRaw) {
        this.bodyRaw = bodyRaw;
    }

    /**
     * Gets the value of paramsUrl and returns paramsUrl
     *
     * @return Returns value of paramsUrl
     */
    public Map<String, String> getParamsUrl() {
        return paramsUrl;
    }

    /**
     * Sets the paramsUrl
     * You can use getParamsUrl() to get the value of paramsUrl
     */
    public void setParamsUrl(final Map<String, String> paramsUrl) {
        this.paramsUrl = paramsUrl;
    }

    /**
     * Gets the value of parseServer and returns parseServer
     *
     * @return Returns value of parseServer
     */
    public boolean isParseServer() {
        return parseServer;
    }

    /**
     * Sets the parseServer
     * You can use getParseServer() to get the value of parseServer
     */
    public void setParseServer(final boolean parseServer) {
        this.parseServer = parseServer;
    }

    /**
     * Gets the value of listaEncriptar and returns listaEncriptar
     *
     * @return Returns value of listaEncriptar
     */
    public List<String> getListaEncriptar() {
        return listaEncriptar;
    }

    /**
     * Sets the listaEncriptar
     * You can use getListaEncriptar() to get the value of listaEncriptar
     */
    public void setListaEncriptar(final List<String> listaEncriptar) {
        this.listaEncriptar = listaEncriptar;
    }

    /**
     * Gets the value of addCadena and returns addCadena
     *
     * @return Returns value of addCadena
     */
    public boolean isAddCadena() {
        return addCadena;
    }

    /**
     * Sets the addCadena
     * You can use getAddCadena() to get the value of addCadena
     */
    public void setAddCadena(final boolean addCadena) {
        this.addCadena = addCadena;
    }

    /**
     * Gets the value of forceArq and returns forceArq
     *
     * @return Returns value of forceArq
     */
    public boolean isForceArq() {
        return forceArq;
    }

    /**
     * Sets the forceArq
     * You can use getForceArq() to get the value of forceArq
     */
    public void setForceArq(final boolean forceArq) {
        this.forceArq = forceArq;
    }

	/**
	 *
	 * @return Returns value of requestCleanCoockies
	 */
	public boolean isRequestCleanCoockies() {
		return requestCleanCoockies;
	}

	/**
	 * set the requestCleanCoockies
	 * you can use isRequestCleanCoockies() to get the value of requestCleanCoockies
	 * @param requestCleanCoockies boolean true/false
	 */
	public void setRequestCleanCoockies(final boolean requestCleanCoockies) {
		this.requestCleanCoockies = requestCleanCoockies;
	}

}