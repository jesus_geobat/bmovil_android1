package suitebancomer.aplicaciones.commservice.commons;

/**
 * 
 * @author lbermejo
 * Constantes para el API 
 * 
 * IDS Comercial S.A. de C.V
 *
 */
public class ApiConstants {
	// ///////////////////////////////////////////////////////////////////////////
	// Operations identifiers //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * Activation step 1 operation.
	 */
	public static final int ACTIVATION_STEP1_OPERATION = 1;

	/**
	 * Acivation step 2 operation.
	 */
	public static final int ACTIVATION_STEP2_OPERATION = 2;

	/**
	 * Login oepration.
	 */
	public static final int LOGIN_OPERATION = 3;

	/**
	 * Movements operation.
	 */
	public static final int MOVEMENTS_OPERATION = 4;

	/**
	 * Transfer money operation between the user's accounts.
	 */
	public static final int SELF_TRANSFER_OPERATION = 5;

	/**
	 * Transfer money operation to someone else's account in Bancomer.
	 */
	public static final int BANCOMER_TRANSFER_OPERATION = 6;

	/**
	 * External transfer operation.
	 */
	public static final int EXTERNAL_TRANSFER_OPERATION = 7;

	/**
	 * Nipper store purchase operation.
	 */
	public static final int NIPPER_STORE_PURCHASE_OPERATION = 8;

	/**
	 * Nipper airtime purchase operation.
	 */
	public static final int NIPPER_AIRTIME_PURCHASE_OPERATION = 9;

	/**
	 * Change password operation.
	 */
	public static final int CHANGE_PASSWORD_OPERATION = 10;

	/**
	 * Close session operation.
	 */
	public static final int CLOSE_SESSION_OPERATION = 11;

	/**
	 * Get the owner of a credit card.
	 */
	public static final int CARD_OWNER_OPERATION = 12;

	/**
	 * Calculate fee.
	 */
	public static final int CALCULATE_FEE_OPERATION = 13;

	/**
	 * Calculate fee 2.
	 */
	public static final int CALCULATE_FEE_OPERATION2 = 14;

	/**
	 * Service payment.
	 */
	public static final int SERVICE_PAYMENT_OPERATION = 15;

	/**
	 * Help image for service payment.
	 */
	public static final int HELP_IMAGE_OPERATION = 16;

	/**
	 * Request frequent service payment operation list.
	 */
	public static final int FAVORITE_PAYMENT_OPERATION = 18;

	/**
	 * Fast Payment service.
	 */
	public static final int FAST_PAYMENT_OPERATION = 19;

	/**
	 * Service enterprise name operation.
	 */
	public static final int SERVICE_NAME_OPERATION = 20;

	/**
	 * Alta de operaciones sin tarjeta
	 */
	public static final int ALTA_OPSINTARJETA = 23;

	/**
	 * Consulta de operaciones sin tarjeta
	 */
	public static final int CONSULTA_OPSINTARJETA = 24;

	/**
	 * Baja de operaciones sin tarjeta
	 */
	public static final int BAJA_OPSINTARJETA = 25;

	/**
	 * Consulta status del servicio
	 */
	public static final int CONSULTA_ESTATUSSERVICIO = 26;

	/**
	 * Cambia perfil del cliente
	 */
	public static final int CAMBIA_PERFIL = 27;

	/**
	 * Consulta comision para transferir interbancario
	 */
	public static final int CONSULTAR_COMISION_I = 28;

	/**
	 * Consulta comision para transferir interbancario
	 */
	public static final int CONSULTAR_CODIGO_PAGO_SERVICIOS = 29;

	/**
	 * Preregistro de pago de servicios
	 */
	public static final int PREREGISTRAR_PAGO_SERVICIOS = 30;

	/**
	 * Compra de tiempo aire
	 */
	public static final int COMPRA_TIEMPO_AIRE = 31;

	/**
	 * Alta de frecuente
	 */
	public static final int ALTA_FRECUENTE = 32;

	/**
	 * Consulta del beneficiario
	 */
	public static final int CONSULTA_BENEFICIARIO = 33;

	/**
	 * Baja de frecuente
	 */
	public static final int BAJA_FRECUENTE = 34; // OP144

	/**
	 * Baja de frecuente
	 */
	public static final int CONSULTA_CIE = 35;

	/**
		 * 
		 */
	public static final int ACTUALIZAR_FRECUENTE = 36;

	/**
	 * Actualizacion de preregistrado a Frecuente
	 */
	public static final int ACTUALIZAR_PREREGISTRO_FRECUENTE = 37;

	/**
	 * Cambio de telefono asociado
	 * 
	 */
	public static final int CAMBIO_TELEFONO = 38;

	/**
	 * Cambio de cuenta Asociada
	 */
	public static final int CAMBIO_CUENTA = 39;

	/**
	 * Suspencion Temporal
	 */
	public static final int SUSPENDER_CANCELAR = 40;

	/**
	 * Actualizacion de cuentas del usuario
	 */
	public static final int ACTUALIZAR_CUENTAS = 41;

	public static final int CONSULTA_TARJETA_OPERATION = 42;

	/**
	 * Consulta de limites de operacion
	 */
	public static final int CONSULTAR_LIMITES = 43;

	/**
	 * Cambio de limites de operacion
	 */
	public static final int CAMBIAR_LIMITES = 44;

	/**
	 * Consulta de estatus mantenimiento.
	 */
	public static final int CONSULTA_MANTENIMIENTO = 45;

	/**
	 * Desbloqueo de contrasenas
	 */
	public static final int DESBLOQUEO = 46;

	/**
	 * Quitar suspension
	 */
	public static final int QUITAR_SUSPENSION = 47;

	/**
	 * Reactivacion
	 */
	public static final int OP_ACTIVACION = 48;

	/**
	 * Operacion de contratacion final para bmovil.
	 */
	public static final int OP_CONTRATACION_BMOVIL_ALERTAS = 49;

	/**
	 * Operacion de consulta de terminos y condiciones de uso.
	 */
	public static final int OP_CONSULTAR_TERMINOS = 50;

	public static final int OP_ENVIO_CLAVE_ACTIVACION = 51;

	public static final int OP_CONFIGURAR_CORREO = 52;

	public static final int OP_ENVIO_CORREO = 53;

	public static final int OP_VALIDAR_CREDENCIALES = 54;

	public static final int OP_FINALIZAR_CONTRATACION_ALERTAS = 55;

	/** Activacion softtoken - Consulta de tipo de solicitud. */
	public static final int CONSULTA_TARJETA_ST = 56;

	/** Activacion softtoken - Autenticacion */
	public static final int AUTENTICACION_ST = 57;

	/**
	 * Activacion hamburguesa
	 */
	public static final int HAMBURGUESA = 112;
	
	/**
	 * Catalogo de aplicaciones
	 */
	public static final int CATALOGO_APLICACIONES = 113;
	
	/**
	 * Activacion softtoken
	 */
	public static final int EXPORTACION_SOFTTOKEN = 58;
	/**
	 * Activacion softtoken
	 */
	public static final int SINCRONIZACION_SOFTTOKEN = 59;

	/** Activacion softtoken - Contratacion enrolamiento. */
	public static final int CONTRATACION_ENROLAMIENTO_ST = 60;

	/** Activacion softtoken - Finalizar contratacion. */
	public static final int FINALIZAR_CONTRATACION_ST = 61;

	/** Activacion softtoken - Cambio telefono asociado. */
	public static final int CAMBIO_TELEFONO_ASOCIADO_ST = 62;

	/** Activacion softtoken - Solicitud. */
	public static final int SOLICITUD_ST = 63;

	/** Mantenimiento Alertas. */
	public static final int MANTENIMIENTO_ALERTAS = 64;

	/** Consulta de Terminos y Condiciones Sesion */

	public static final int OP_CONSULTAR_TERMINOS_SESION = 65;

	/** Solicitar Alertas */

	public static final int OP_SOLICITAR_ALERTAS = 66;

	// Empieza codigo de SPEI revisar donde se usan las constantes
	/* Identifier for the request spei accounts operation. */
	public static final int SPEI_ACCOUNTS_REQUEST = 67;// 66

	/**
	 * Identifier for the request spei terms and conditions.
	 */
	public static final int SPEI_TERMS_REQUEST = 68; // 67

	/**
	 * The SPEI maintenance operation.
	 */
	public static final int SPEI_MAINTENANCE = 69;// 68

	/**
	 * The request beneficiary account number operation.
	 */
	public static final int CONSULT_BENEFICIARY_ACCOUNT_NUMBER = 70;// 69

	// One CLick
	/**
	 * Operation codes.
	 */
	/** consulta detalle ofertas ilc */
	public static final int CONSULTA_DETALLE_OFERTA = 71;// 67;//66
	public static final int ACEPTACION_OFERTA = 72;// 68;//67
	public static final int EXITO_OFERTA = 73;// 69;//68
	public static final int RECHAZO_OFERTA = 74;// 70;//69
	/** consulta detalle ofertas EFI */
	public static final int CONSULTA_DETALLE_OFERTA_EFI = 75;// 71;//70
	public static final int ACEPTACION_OFERTA_EFI = 76;// 71
	public static final int EXITO_OFERTA_EFI = 77;// 72
	public static final int SIMULADOR_EFI = 78;// 73
	/** consulta detalle consumo */
	public static final int CONSULTA_DETALLE_OFERTA_CONSUMO = 79;
	public static final int POLIZA_OFERTA_CONSUMO = 80;
	public static final int TERMINOS_OFERTA_CONSUMO = 81;
	public static final int EXITO_OFERTA_CONSUMO = 82;
	public static final int DOMICILIACION_OFERTA_CONSUMO = 83;
	public static final int CONTRATO_OFERTA_CONSUMO = 84;
	public static final int RECHAZO_OFERTA_CONSUMO = 85;
	public static final int SMS_OFERTA_CONSUMO = 86;
	public static final int OP_CONSULTA_INTERBANCARIOS = 87;

	/**
	 * Request frequent service payment operation list for BBVA accounts.
	 */
	public static final int FAVORITE_PAYMENT_OPERATION_BBVA = 88;

	// Depositos Movil
	public static final int CONSULTA_DEPOSITOS_CHEQUES = 89;// 87
	public static final int CONSULTA_DEPOSITOS_EFECTIVO = 90; // Ya no se usa 88
	public static final int CONSULTA_PAGO_SERVICIOS = 91;// 89
	public static final int CONSULTA_TRANSFERENCIAS_CUENTA_BBVA = 92;// 90
	public static final int CONSULTA_TRANSFERENCIAS_MIS_CUENTAS = 93;// 91
	public static final int CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS = 94;// 92
	public static final int CONSULTA_TRANSFERENCIAS_CLIENTES_BANCOMER = 95; // Ya no se usa 93
	public static final int CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS = 96;// 94
	public static final int CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO = 97;// 95
	public static final int CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE = 98;// 96
	public static final int CONSULTA_DEPOSITOS_CHEQUES_DETALLE = 99;// 97

	/** consulta importes tarjeta de credito **/
	public static final int OP_CONSULTA_TDC = 100;

	public static final int OP_RETIRO_SIN_TAR = 101;

	public static final int CONSULTA_SIN_TARJETA = 102;

	/** consulta alta de retiro sin tarjeta **/
	public static final int OP_RETIRO_SIN_TAR_12_DIGITOS = 103;
	
	/** sincronizaExportaToken **/
		public static final int OP_SINC_EXP_TOKEN = 104;

		public static final int OP_CONSULTA_OTROS_CREDITOS = 105;
	
	//agregadas paperless
	/** operacion actualizarEstatusEnvioEC*/
	public static final int ACTUALIZAR_ESTATUS_EC=106;
	/** operacion consultarEstadoCuenta*/
	public static final int CONSULTAR_ESTADO_CUENTA=107;
	/** operacion consultarEstatusEnvioEC*/
	public static final int CONSULTAR_ESTATUS_EC=108;
	/** operacion consultaTextoPaperles*/
	public static final int CONSULTAR_TEXTO_PAPERLESS=109;
	/** operacion inhibirEnvioEstadoCuenta*/
	public static final int INHIBIR_ENVIO_EC=110;
	/** operacion obtenerPeriodosEC*/
	public static final int OBTENER_PERIODO_EC=111;
	/** operacion EnviarCorreoEC*/
	public static final int ENVIAR_CORREO_EC=204;

	/* Seccion de alertas digitales*/
	public static final int OP_CONSULTA_CUENTAS_ALERTAS = 114;

	public static final int OP_DETALLE_CUENTA_ALERTAS = 115;

	public static final int OP_ALTA_MOD_SERVICIO_ALERTAS = 116;

	public static final int OP_BAJA_SERVICIO_ALERTAS = 117;

	public static final int OP_MENSAJES_ENVIADOS_ALERTAS = 118;

	public static final int OP_DETALLE_MENSAJE_ENV_ALERTAS = 119;

	public static final int OP_CONSULTA_DETALLE_OTROS_CREDITOS = 120;

	public static final int OP_CONSULTA_SMS_OTROS_CREDITOS = 121;

	public static final int OP_CONSULTA_CORREO_OTROS_CREDITOS = 122;

	public static final int OP_CONSULTA_SPEI = 123;

	public static final int OP_ENVIOCORREO_SPEI = 124;

	public static final int OP_CONSULTA_INVERSIONES_PLAZO = 125;

	public static final int OP_CONSULTA_IMPORTES_PAGO_CREDITOS = 126;

	public static final int OP_PAGO_CREDITO_CH = 127;
//..

	public static final int OP_MIGRA_BASICO = 130;

	public static final int OP_MOVIMIENTOS_TRANSITO = 131;

	public static final int CONSULTA_DETALLE_OFERTA_TDC=154;
	public static final int CONSULTA_TERMINOS_CONDICIONES_TDC=155;
	public static final int ACEPTACION_OFERTA_TDC=133;
	public static final int FORMATOS_FINALES_TDC=134;
	public static final int OFERTA_DOMICILIACION = 135;


	/** FSW - CGI -> Añadido por inclusión de Pago de Créditos Fase 2 */
	public static final int OP_SIMULA_IMPORTES_PAGO_CREDITO = 207;

	public static final int OP_ENVIO_NOTI_PAGO_CREDITO = 208;

	public static final int OP_CONSULTAR_PAGOS_CREDITOS = 138;

	public static final int OP_DETALLE_PAGOS_CREDITOS = 139;

	public static final int OP_AYUDA_PRESTAMO_CONSUMO = 191;

	//Envío de correo
	public static final int ENVIO_CORREO_TDC_ILC_OP = 212;
	public static final int ENVIO_CORREO_HIPO_AUTO_OP = 211;
	public static final int ENVIO_CORREO_CONSUMO_OP = 210;

	/** FIN MERGE PAGO DE CRÉDITOS FASE 2 */

	public static final String JSON_OPERATION_VENTA_TDC = "VTDC001";


	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_VALUE = "BAN2O01";
	public static final String OPERATION_CODE_VALUE_PM = "TKEM001";

	/**
	 * Operation code parameter for RECORTADO.
	 */
	public static final String OPERATION_CODE_VALUE_RECORTADO = "BREC001";

	/**
	 * Operation code parameter for BMOVIL MEJORADO.
	 */
	public static final String OPERATION_CODE_VALUE_BMOVIL_MEJORADO = "MJRS001";

	/**
	 * Operation code parameter for DEPOSITOS
	 */
	public static final String JSON_OPERATION_CODE_VALUE_DEPOSITOS = "O300001";

	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE = "BAN2O05";
	public static final String JSON_OPERATION_CODE_VALUE_PM = "TKEM001";

	/**
	 * Operation code parameter for alertas.
	 */
	public static final String OPERATION_CODE_ALERTAS = "ADGO001";
	
	/**
	 * operacion de hamburguesa  y catalogo de aplicaciones.
	 */
	public static final String CBC_CODE_VALUE = "CBCO001";
	
	public static final String CODE_TEMP="CHIP001";

	// one CLick
	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE_ONECLICK = "ONEC001";

	/**
	* Operation Banquero Gestor Remoto
	*/
	public static final String OPERATION_CODE_VALUE_GESTOR = "BIOM001";

	/**
	 * Operation code parameter for seguros.
	 */
	public static final String OPERATION_CODE_VALUE_SEGUROS="SGRS001";
	/**
				 * 
				 */

	/**
	 * Operation code parameter for new ops.
	 */
	// public static final String JSON_OPERATION_SOLICITAR_ALERTAS_CODE_VALUE =
	// "BREC001";
	public static final String JSON_OPERATION_RECORTADO_CODE_VALUE = "BREC001";

	/**
	 * Operation code for mejoras
	 */
	public static final String JSON_OPERATION_MEJORAS_CODE_VALUE = "MEJO001";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";

	public static final String JSON_OPERATION_CONSULTA_INTERBANCARIOS = "BXCO001";

	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE_CONSUMO = "ONEC002";
	
	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE_PAPERLESS = "PPLS001";

	public static final String CODIGO_ESTATUS_APLICACION = "NE";

	public static final String OPERACIONES_CAT_AU = "{\"perfil\":[{\"recortado\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"oneClickBmovilConsumo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"false\"}}]},{\"basico\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"oneClickBmovilConsumo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"false\"}}]},{\"avanzado\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"oneClickBmovilConsumo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}}]}],\"version\":\"12\"}";

	public static final String LISTA_OPERACIONES = OPERACIONES_CAT_AU;

	public static final String LISTA_TELEFONICAS = "[{\"companiaCelular\":\"TELCEL\",\"nombreImagen\":\"telcel.png\"},{\"companiaCelular\":\"MOVISTAR\",\"nombreImagen\":\"movistar.png\"},{\"companiaCelular\":\"IUSACELL\",\"nombreImagen\":\"iusacell.png\"},{\"companiaCelular\":\"UNEFON\",\"nombreImagen\":\"unefon.png\"}]";

	public static final String CORREO_EJEMPLO = "ejemplo@gonet.us";

	// CODIGO_ESTATUS_APLICACION = "PA";
	public static final String TIPO_INSTRUMENTO = "S1";
	public static final String ESTATUS_INSTRUMENTO = "";
	public static final String PERFIL = "MF02";

	public static final String EMPTY_JSON = "{}";
	
	public static final String OK = "OK";
	public static final int ESTATUS_OK_CODE = 200;
	public static final int ESTATUS_ERROR_ARQ_CODE = 403;
	public static final String EMPTY_LINE = "";

	/**
	 * Response status successful.
	 */
	public static final String STATUS_OK = "OK";

	/**
	 * Response status warning.
	 */
	public static final String STATUS_WARNING = "AVISO";

	/**
	 * Response status error.
	 */
	public static final String STATUS_ERROR = "ERROR";

	/**
	 * Response optional application update.
	 */
	public static final String STATUS_OPTIONAL_UPDATE = "AC";

	/**
	 * Tag for status.
	 */
	public static final String STATUS_TAG = "estado";

	/**
	 * Tag for error code.
	 */
	public static final String CODE_TAG = "codigoMensaje";

	/**
	 * Tag for error message.
	 */
	public static final String MESSAGE_TAG = "descripcionMensaje";

	/**
	 * Alternative Tag for error message.
	 */
	public static final String MESSAGE_INFORMATIVO_TAG = "mensajeInformativo";

	/**
	 * Tag for application mandatory update URL.
	 */
	public static final String URL_TAG = "UR";
	

	/**
     * Operation successful.
     */
    public static final int OPERATION_SUCCESSFUL = 0;

    /**
     * Operation with warning.
     */
    public static final int OPERATION_WARNING = 1;

    /**
     * Operation failed.
     */
    public static final int OPERATION_ERROR = 2;

    /**
     * Operation succesful but asks for optinal updating.
     */
    public static final int OPERATION_OPTIONAL_UPDATE = 3;

    /**
     * Session expired.
     */
    public static final int OPERATION_SESSION_EXPIRED = -100;

    /**
     * Unknown operation result.
     */
    public static final int OPERATION_STATUS_UNKNOWN = -1000;
    
    public static final String CODE_MBANK1111="MBANK1111";
    
    public static final String LAN_STRING="Lan";
    public static final String ANDROID_STRING= "android";
    
    public static final String OPERACION="operacion";

	public static final String OPERACION_COORDENADAS="consultaCoordenada";
	public static final String OPERACION_AUTENTICACIONPM="autenticacionTokenPM";

    public static final String DOBLE_SLASH="\\\\";
    public static final String OPERACION_MAYUS="OPERACION";
    public static final String PARAM_PAR_INICIO= "PAR_INICIO.0";
    public static final String ONCLICK="ONECLICK";
    public static final String CONSUMO="CONSUMO";
    public static final String DEPOSITOS="DEPOSITOS";
	public static final String PAPERLESS="PAPERLESS";
    
    public static final String SLASH="\"";
	public static final String DOBLE_APOSTROPE="\"\"";
	public static final String APOSTROPE="\"";
    
    public static final String TAG = "ApiBancomer";
    
    
	/**
	 * Operation codes.
	 */
	public static final String[] OPERATION_CODES = { "00", // 0
            "01", // 1
            "02", // 2
            "103", // 3
			"104", // 4
            "105", // 5
            "106", // 6
            "107", // 7
            "08", // 8
            "09", // 9
            "110", // 10
            "111", // 11
            "12", // 12
            "13", // 13
			"14", // 14
            "115", // 15
            "16", // 16
            "17", // 17
            "141", // 18
            "19", // 19
            "20", // 20
            "21", // 21
            "22", // 22
            "123", // 23
			"124", // 24
            "125", // 25
            "26", // 26
            "cambioPerfil", // 27
            "113", // 28
            "130", // 29
            "145", // 30
            "109", // 31
			"118", // 32
            "112", // 33
            "144", // 34
            "120", // 35
            "143", // 36
            "146", // 37
            "cambioNumCeluar", // 38
			"cambioCuentaAsociada", // 39
            "suspenderCancelarServicio", // 40
			"actualizarCuentas", // 41
            "consultaTarjetaContratacionE", // 42
			"consultaLimites", // 43
            "configuracionLimites", // 44
			"consultaEstatusMantenimiento", // 45
            "desbloqueoContrasena", // 46
			"quitarSuspension", // 47
            "102", // 48
            "contratacionBMovilAlertas", // 49
			"consultaTerminosCondiciones", // 50
            "envioClaveActivacion", // 51
			"configuracionCorreo", // 52
            "envioCorreo", // 53
            "valCredenciales", // 54
			"finalizarContratacionAlertas", // 55
            "consultaTarjetaST", // 56
			"autenticacionToken", // 57
            "204", // 58
            "203", // 59
            "contratacionE", // 60
			"finalizarContratacionST", // 61
            "cambioTelefonoAsociadoE", // 62
			"solicitudST", // 63
            "mantenimientoAlertas", // 64
			"consultaTerminosCondicionesSesion", // 65
            "solicitarAlertas", // 66/* SPEI */
			"ConsultaCuentasSPEI", // 67
            "consultaTerminosCondicionesSPEI", // 68
			"MantenimientoSpei", // 69
            "ConsultaCuentaTerceros", // 70
			/* OneClick */"cDetalleOfertaBMovil", // 71
            "aceptacionILCBmovil", // 72
			"exitoILC", // 73
            "noAceptacionBMovil", // 74
            "cDetalleOfertaBMovil", // 75
			"aceptaOfertaEFI", // 76
            "exitoEFI", // 77
            "SimulacionEFI", // 78
			"detalleConsumoBMovil", // 79
            "polizaConsumoBMovil", // 80
			"consultaContratoConsumoBMovil", // 81
            "oneClickBmovilConsumo", // 82
			"consultaDomiciliacionBovedaConsumoBMovil", // 83
			"consultaContratoBovedaConsumoBmovil", // 84
            "noAceptacionBMovil", // 85
			"exitoConsumoBMovil", // 86
            "consultaTransferenciaSPEI", // 87
			"consultaFrecuentesTercerosCE", // 88
            "consultarDepositosCheques", // 89
			"consultarDepositosEfectivo", // 90
            "consultarTransfPagoServicios", // 91
			"consultarTransfBancomer", // 92
            "consultarTransfMisCuentas", // 93
			"consultarTransfOtrosBancos", // 94
            "consultarClientesBancomer", // 95
			"consultarOtrosBancos", // 96
            "consultarDepositosEfectivo", // 97
			"detalleDepositosEfectivo", // 98
            "detalleDepositosCheques", // 99
			"importesTDC", // 100
            "retiroSinTarjeta", // 101
            "consultaRetiroSinTarjeta", // 102
			"claveRetiroSinTarjeta", // 103
            "sincronizaExportaToken", // 104
			"consultarCreditos", // 105/*paperles*/
			"actualizarEstatusEnvioEC", // 106
			"obtenerPDFperiodos", // 107
            "consultarEstatusEnvioEC", // 108
			"consultaTextoPaperless", // 109
            "inhibirEnvioEstadoCuenta", // 110
            "obtenerPeriodos", // 111
			"menuHamburguesa", // 112
            "catalogoAplicaciones", // 113
            "consultaCtasAlertas", // 114
            "detalleCuentaAlertas", // 115
            "altaModServicioAlertas", // 116
			"bajaServicioAlertas", // 117
            "detalleMenEnvPorCuenta", // 118
            "detalleMensajeEnv", // 119
            "cDetalleCred", // 120
            "envioSMSConsulta", // 121
            "envioCorreoConsulta", // 122
			"consultaMovInterbancarios", // 123
            "envioCorreoSPEI", // 124
            "consultarInversiones",// 125
            "consultaImpPagoCredito", // 126
            "pagoCreditoCH", // 127
            "consultaPymentServices", // 128
            "AltaPaymentServices", // 129
            "migraBasico", // 130
            "movsTransitoTDC", // 131
            "altaIDSender", //132
            "aceptacionTDCBmovil", //133
            "formatosFinalesOneClickTDC", //134
            "oneClicDomiTDC", //135
            "", //136
            "", //137
            "consultarPagosCreditos", //138
            "detallePagosCreditos", //139
            "posicionPat", //140
            "opGlobal", //141
            "calculoAlternativas", //142
            "consultaCorreo", //143
            "envioCorreo", //144
            "consultaAlternativas", //145
            "TDC", //146
            "detalleAlternativa", //147
            "contrataAlternativaConsumo", //148
            "altaIDSender", //149
            "consultaCoordenada", //150
            "autenticacionTokenPM", //151
            "sincronizaTokenPM", //152
            "exportaTokenPM", //153
			"detalleTDC", //154
            "contratoTDCBMovil", //155
            "", //156
            "detalleAlternativaTDC", //157
            "oneClicSeguros", //158
            "ContratoHTMLSeguros", //159
            "oneClicSeguros", //160
            "getCustomer", //161
            "", //162
			"", //163
            "", //164
            "", //165
            "", //166
            "", //167
            "", //168
            "", //169
            "", //170
            "consultaCURP", //171
            "consultaContratoCD", //172
            "generateOTP", //173
            "validateOTP", //174
            "consultaColonias", //175
            "", //176
            "", //177
            "", //178
            "", //179
            "", //180
            "", //181
            "", //182
            "", //183
            "", //184
            "", //185
            "", //186
            "consultarClabe", // 187
            "", // 188
            "", // 189
            "", // 190
            "ayudaPrestamoConsumo", // 191
			"", // 192
			"", // 193
			"", // 194
			"", // 195
			"", // 196
			"", // 197
			"", // 198
			"", // 199
			"", // 200
			"", // 201
			"", // 202
			"", // 203
			"envioCorreoEC",//204
			"", // 205
			"", // 206
			"simulaImportesPagoCredito", //207
			"envioNotiPagoCred", //208
			"correoGestor", // 209
			"envioCorreoConsumo", // 210
			"envioCorreoHipoAuto", // 211
			"envioCorreoTDCILC" // 212
    };
	
	/**
	 * clase de enum para cambio de url;
	 */
	public enum isJsonValueCode{
		ONECLICK, NONE,CONSUMO,DEPOSITOS,PAPERLESS;
	}
	
	public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
	
	public static String simulationFile="SimulacionAPICom.properties";
	
	public static String confApiCom="ConfAPIComm.properties";

	public static String appCommConfg = "appCommConfg.properties";
	
	public static final String  PREFIX_URL_PRODUCTION="url.production.";
	
	public static final String PREFIX_URL_DEVELOPMENT="url.development.";


	public static final String PREFIX_URL_ARQ_PRODUCTION="url.arq.production.";

	public static final String PREFIX_URL_ARQ_DEVELOPMENT="url.arq.development.";


	public static final String PREFIX_URL_BASE_CUSTOM_DEV="url.base.custom.dev.";
	public static final String PREFIX_URL_BASE_CUSTOM_PROD="url.base.custom.prod.";
	
	public static final String PREFIX_OPERACION_CODE="codeOperation.";
	
	
	public static final String KEY_DEVELOP_URL="developmentURL";
	
	public static final String KEY_DEVELOP_EMULATOR_URL="developmentEmulatorURL";

	public static final String KEY_PRODUCTION_URL="productionURL";
	
	public static final String KEY_PRODUCTION_EMULATOR_URL="productionEmulatorURL";

	public static String allowLogPropertieName = "active.log";

	public static final String PARAMETRO_SEPARADOR = "*";
	
	public static final String UTF_8 = "UTF-8";
	
	public static final String SIGNO_INTERROGACION="?";
	
	public static final String AMP_STRING="&";
	
	public static final String SIGNO_IGUAL="=";

	/**
	 * screen size identifier.
	 */
	public static final String SCREEN_SIZE_PARAM = "screen_size";

	/**
	 * Type of help image identifier.
	 */
	public static final String HELP_IMAGE_TYPE_PARAM = "help_image_type";

	/**
	 * Service provider identifier.
	 */
	public static final String SERVICE_PROVIDER_PARAM = "service_provider";

    public static final String IMAGEN_BMOVIL = "IMAGEN_BMOVIL";

	//PUSH Notifications
	public static final String JSON_OPERATION_ALERTAS_DIGITALES = "ADGO001";
	public static final int ID_SENDER = 132;

	public static  final String ORDER="order";
	/** Consulta Gestor Remoto*/
	public static final int OP_CONSULTA_GESTOR_REMOTO = 196;

	public static final int OP_CONTRATO_GESTOR_REMOTO = 197;

	public static final int OP_CONSULTA_CONTRATO_GESTOR_REMOTO = 198;

	public static final int OP_DETALLE_GESTOR = 199;

	/**
	 * Operation code parameter for patrimonial.
	 */
	public static final String JSON_OPERATION_CODE_PATRIMONIAL = "BANP001";
	public static final int OP_PATRIMONIAL = 140;

	/** opcion de login **/
	public static final int OP_LOGIN_FN = 3;
	public static final int OP_GLOBAL = 141;

	/** opcion de login **/
	public static final String CADENA_STRING_LITERAL = "cadena";

	public static final String USR_STRING="user";
	public static final String PSW_STRING="password";
	public static final String SC_STRING="sc";
	public static final String IUM_STRING="ium";
	//inicia IL
	public static final String OTP_STRING="otp";
	public static final String CVE_CAMP_STRING="cveCamp";

	public static final String ACCOUNT_NUMBER_IL="accountNumber";
	public static final String AMOUNT_IL="amount";
	public static final String RATE_IL="rate";
	public static final String ACCOUNT_OPENING_IL="accountOpening";
	public static final String CELL_PHONE_IL="cellPhoneNumber";
	public static final String CELL_PHONE_NUMBER_IL="cellphoneNumber";
	public static final String CR_IL="cr";
	public static final String MANAGEMENT_UNIT_IL="managementUnit";
	public static final String NOMINAL_GAT_IL="nominalGat";
	public static final String REAL_GAT_IL="realGat";
	public static final String ACCOUNT_SENDER_IL="accountSender";
	public static final String ACCOUNT_RECIEVER_IL="accountReciever";
	public static final String CREATE_CONTRACT_IL="createContract";
	public static final String NUM_CONTRACT_IL="numContract";
	//termina IL

	public static final String JSON_OPERATION_CODE_CREDITOS ="BCRD001";
	public static final int OP_CONSUTA_CORREO = 143;
	public static final int OP_CALCULO_ALTERNATIVA = 142;
	public static final int OP_ENVIO_CORREOS = 144;
	public static final int OP_CONSUTA_ALTERNATIVA = 145;
	//public static final int OP_CONSUTA_TDC = 146;
	public static final int OP_DETALLE_ALTERNATIVA = 147;
	public static final int OP_CONTRATA_ALTERNATIVA_CONSUMO = 148;
	public static final int OP_ID_SENDER = 149;
	public static final int OP_CONSULTA_COORDENADAS = 150;
	public static final int OP_AUTENTICACIONTOKENPM = 151;
	public static final int OP_SINCRONIZACIONPMTOKEN = 152;
	public static final int OP_EXPORTACIONPMTOKEN = 153;
	public static final String OP_ONE_CLIC_TDCADICIONAL = "oneClicTDCAdicional";
	public static final int OP_DETALLEALTERNATIVATDC = 157;
	public static final int CONSULTA_DETALLE_SEGUROS = 158;
	public static final int CONSULTA_TERMINOS_CONDICIONES_SEGUROS = 159;
	public static final int CONSULTA_FORMALIZACION_GF = 160;

	public static final int GET_CUSTOMER = 161;
	public static final int  CAMBIA_NOMINA_CONTRATO=162;
	public static final int REQUEST_PORTABILITY = 163;
	public static final int GET_LIST_PORTABILITY = 164;
	public static final int GET_PORTABILITY = 165;
	public static final int  BANKS_CATALOG=166;

	public static final String OPERACION_CODE_LINK = "LINK001";
	public static final int ENVIO_SMS_PERFIL = 176;
	public static final int VALIDA_FOLIO_PERFIL = 177;

	public static final int CONSULTA_CURP = 171;
	public static final int CONSULTA_CONTRATO = 172;
	public static final int CONSULTA_GENERA_OTP = 173;
	public static final int CONSULTA_VALIDA_OTP = 174;
	public static final int CONSULTA_COLONIAS = 175;

	/**
     * Id de operacion para el D520 - get state
     */
    public static int OP_GET_STATE = 178;

	public static final int ENROLL_STATE = 179;

	public static final int TOKEN_AUTHENTICATION = 180;
    /**
     * Id de operacion para el D520 - finalizaontatacionenroll
     */
    public static int OP_FINALIZA_ENROLL = 181;
	public static final int RESEND_ACTIVATION_KEY = 182;
	public static int OP_SYNC = 183;
    /**
     * Id operacion para D520 catalogs
     */
    public static int OP_CATALOGS = 184;

	public static final int ALLOWED = 186;
	public static final int OP_CONSULTAR_CLABE_OPERACION = 187;
	public static final int OP_SIGNING = 156;
	public static final int OP_ENVIO_CORREO_GESTOR = 209;
	////// Inversión Líquida
	public static final int CONSULTA_DETALLE_IL = 224;
	public static final int CONSULTA_TERMINOS_CONDICIONES_IL = 225;
	public static final int CONSULTA_CONTRATACION_IL = 226;
	public static final int TRANSFERENCIAS_IL = 227;
	public static final int CONSULTA_GATS_IL = 228;
}
