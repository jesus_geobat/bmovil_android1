package suitebancomer.aplicaciones.commservice.request;

import android.text.TextUtils;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants.isJsonValueCode;
import suitebancomer.aplicaciones.commservice.commons.BodyType;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.commservice.commons.ContentType;
import suitebancomer.aplicaciones.commservice.commons.MethodType;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.EncriptarConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * This class implements the Product interface.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class RequestBuilderImpl implements IRequestBuilder {

	public Mapper mapper;

	public RequestBuilderImpl(){
		mapper=new Mapper();
	}

	/**
	 * esta clase se encarga de generar el request.
	 * 
	 * 1.- obtiene el url a la que se hara la peticion llamando la clase Mapper
	 * y en metodo getURLPattern.
	 * 
	 * 2. parsea los parametros que envio el delegate a parametros 
	 * de la peticion post que se hara al server
	 * 
	 * @throws UnsupportedEncodingException 
	 * */
	private int opActual;
	public RequestService create(final RequestService request) throws UnsupportedEncodingException{
		
		final ParametersTO params=request.getParametersTO();
        final Map<String, String> paramsToGet =
                cloneParams((Map<String, String>) request.getParametersTO().getParameters());
		//se agrega el operation code a los parametros del resquest
		request.getParametersTO().setOperationCode(getOperationCode(params.getOperationId()));
		opActual=params.getOperationId();
		//mapper.loadEnvironment(params.isDevelopment(), params.isEmulator(), params.getProvider());
		mapper.loadEnvironment(params.isDevelopment(), params.isEmulator());
		mapper.setupOperation(params.getOperationId(), params.getProvider(), params.isDevelopment(), ServerCommons.ARQSERVICE);
		//String operationCode=mapper.getOperationCode(params.getOperationId());

		//desde el mapper se obtiene la url del servidor ya sea para produccion o para test
        final String urlBeforeRequest = mapper.getURLPattern(params.getOperationId());
        // Se mapean los parametros que van en la url para las peticiones REST
        final String urlPatternRequest = mapper.replaceParamsRest(urlBeforeRequest, params.getParamsUrl());
        if (CommContext.allowLog) {
            Log.d("APIComm:RequestBuilderImpl", urlPatternRequest);
        }
		
		 HttpPost httpPost = new HttpPost(urlPatternRequest);
		 HttpGet httpGet =null;

		//se agregan los parametros al request
		List<NameValuePair> listParams = null;
		try {
			listParams = setParameters(request.getParametersTO());
			if(mapper.isByArqService(params.getOperationId(),
                    params.isForceArq()) || params.getOperationId()==ApiConstants.OP_GLOBAL) {
				if(params.getOperationId()==ApiConstants.CLOSE_SESSION_OPERATION){
					//el logout no usa parametros
					listParams = new ArrayList<NameValuePair>();
				}else if(params.getOperationId()==ApiConstants.OP_LOGIN_FN) {
					httpPost.setHeader(HTTP.CONTENT_TYPE, ContentType.JSON.getContentType());
					JSONObject jo=setParametersLoginArqJson(request.getParametersTO());
					listParams = null;
					httpPost.setEntity(new StringEntity(jo.toString(), "UTF8"));
				}else if(params.getOperationId()==ApiConstants.OP_GLOBAL) {
					httpGet= new HttpGet(urlPatternRequest);
					httpGet.setHeader(HTTP.CONTENT_TYPE, ContentType.JSON.getContentType());
					setParametersGlobalJson(request.getParametersTO(), httpGet);
					listParams = null;
					//httpPost.setEntity(new StringEntity(jo.toString(), "UTF8"));


				}else if(params.getOperationId()==ApiConstants.CONSULTA_DETALLE_IL) {
					String cveCamp = ((Hashtable) request.getParametersTO().getParameters()).get(ApiConstants.CVE_CAMP_STRING).toString().replace("\"","");
					httpGet= new HttpGet(urlPatternRequest+"/"+cveCamp+"?type=IL");
					httpGet.setHeader(HTTP.CONTENT_TYPE, "application/json");
					setParametesDetalleILArqJsonPrueba(request.getParametersTO(), httpGet);
					listParams = null;
				}else if(params.getOperationId()==ApiConstants.CONSULTA_GATS_IL) {
					httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
					JSONObject jo=setParametesGatsILArqJsonPrueba(request.getParametersTO(), httpPost);
					listParams = null;
					httpPost.setEntity(new StringEntity(jo.toString(), "UTF8"));

				}else if(params.getOperationId()==ApiConstants.CONSULTA_TERMINOS_CONDICIONES_IL) {
					httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
					JSONObject jo=setParametesTerminosCondicionesILArqJsonPrueba(request.getParametersTO(), httpPost);
					listParams = null;
					httpPost.setEntity(new StringEntity(jo.toString(), "UTF8"));

				}else if(params.getOperationId()==ApiConstants.CONSULTA_CONTRATACION_IL) {
					String cveCamp = ((Hashtable) request.getParametersTO().getParameters()).get(ApiConstants.CVE_CAMP_STRING).toString().replace("\"","");
					httpPost = new HttpPost(urlPatternRequest+"/"+cveCamp);
					httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
					JSONObject jo=setParametesContratacionILArqJsonPrueba(request.getParametersTO(), httpPost);
					listParams = null;
					httpPost.setEntity(new StringEntity(jo.toString(), "UTF8"));

				}else if(params.getOperationId()==ApiConstants.TRANSFERENCIAS_IL) {
					httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
					JSONObject jo = setParametesTransferenciaILArqJsonPrueba(request.getParametersTO(), httpPost);
					listParams = null;
					httpPost.setEntity(new StringEntity(jo.toString(), "UTF8"));
				}else if(params.getOperationId()==ApiConstants.ALLOWED) {
					String cellphone = ((Hashtable) request.getParametersTO().getParameters()).get("user").toString();
					cellphone = cellphone.replaceAll("\"","");
					httpGet= new HttpGet(urlPatternRequest+"?cellphone="+cellphone);
					listParams = null;
				} else if (params.getMethodType().equals(MethodType.GET)) {
                    final String urlFinalGet = urlPatternRequest + getParamsToGet(paramsToGet);
                    httpGet = new HttpGet(urlFinalGet);
                    if (params.getHeaders() != null) {
                        for (final Map.Entry<String, String> entry : params.getHeaders().entrySet()) {
                            httpGet.setHeader(entry.getKey(), entry.getValue());
                        }
                    }
                    String urlTemp = mapper.getUrlOriginal(params.getOperationId(), params.isDevelopment());
                    urlTemp = urlTemp + getParamsToGetMethod(listParams);
                    listParams = new ArrayList<NameValuePair>();
                    if (params.isAddCadena()) {
                        listParams.add(new BasicNameValuePair(ApiConstants.CADENA_STRING_LITERAL, urlTemp));
                    }
				} else{
					if (params.getHeaders() != null) {
                        for (final Map.Entry<String, String> entry : params.getHeaders().entrySet()) {
							httpPost.setHeader(entry.getKey(), entry.getValue());
							if (CommContext.allowLog)
								Log.d("APIComm: REQUEST", "headers: " + "key = [" + entry.getKey() + "]" + "value = [" + entry.getValue() + "]");
						}
				} else {
                        httpPost.setHeader(HTTP.CONTENT_TYPE, ContentType.FORM_URLENCODED.getContentType());
                    }
                    String urlTemp = mapper.getUrlOriginal(params.getOperationId(), params.isDevelopment());
                    urlTemp = urlTemp + getParamsToGetMethod(listParams);
                    listParams = new ArrayList<NameValuePair>();
                    if (params.isAddCadena()) {
                        listParams.add(new BasicNameValuePair(ApiConstants.CADENA_STRING_LITERAL, urlTemp));
                    }
                    if (!TextUtils.isEmpty(params.getBodyRaw())) {
                        httpPost.setEntity(new StringEntity(params.getBodyRaw(), "UTF8"));
                    }
				}
			}
			if (CommContext.allowLog){
				if(listParams!=null){
				Log.d("APIComm:RequestBuilderImpl", listParams.toString() ); 
				Log.e("APIComm: REQUEST", "url: " + urlPatternRequest+ApiConstants.EMPTY_LINE+getParamsToGetMethod(listParams));
				}else{
					Log.e("APIComm: REQUEST", "url: " + urlPatternRequest+ApiConstants.EMPTY_LINE);
				}
			}
		} catch (JSONException e) {
			if (CommContext.allowLog) {
                Log.v(ApiConstants.TAG, e.getMessage());
            }
        }

        if (params.getBodyType().equals(BodyType.URL_ENCODED)) {
            return build(request, httpPost, listParams, httpGet);
        } else {
            request.setRequestPost(httpPost);
            return request;
        }
	}

	public RequestService createImage(final RequestService request) throws UnsupportedEncodingException{

		final ParametersTO params=request.getParametersTO();
		final Hashtable<String,String> params2= (Hashtable<String, String>) params.getParameters();
		//Esta peticion no trae parametros
		//request.getParametersTO().setOperationCode(getOperationCode(params.getOperationId()));

		//mapper.loadEnvironment(params.isDevelopment(), params.isEmulator(), params.getProvider());
		mapper.loadEnvironment(params.isDevelopment(), params.isEmulator());
		mapper.setupOperation(params.getOperationId(), params.getProvider(), params.isDevelopment(), ServerCommons.ARQSERVICE);
		//String operationCode=mapper.getOperationCode(params.getOperationId());

		final StringBuffer sb = new StringBuffer();
		sb.append((String) params2.get(ApiConstants.SCREEN_SIZE_PARAM)).append("_").append((String) params2.get(ApiConstants.SERVICE_PROVIDER_PARAM)).append("_").append((String) params2.get(ApiConstants.HELP_IMAGE_TYPE_PARAM)).append(".png");
        //Construyendo la peticion
        final StringBuffer url = new StringBuffer();
        url.append(mapper.getURLPattern(params.getOperationId())).append("/").append(sb.toString());

		//desde el mapper se obtiene la url del servidor ya sea para produccion o para test
		final String urlPatternRequest =url.toString();
        if (CommContext.allowLog) {
            Log.d("APIComm:RequestBuilderImpl", urlPatternRequest);
        }

		final HttpPost httpPost = new HttpPost(urlPatternRequest);


		//se crea el resquest
		return build(request,httpPost,null,null);

	}

	public List <NameValuePair> setParametersLoginArq(final ParametersTO parameters) {
		final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) ((Hashtable<String, String> ) parameters.getParameters()).clone();

		convertStringToValues(mapNoJson);
		final List <NameValuePair> params=new ArrayList <NameValuePair>();
		params.add(new BasicNameValuePair(ApiConstants.USR_STRING, mapNoJson.get(ApiConstants.USR_STRING)));
		params.add(new BasicNameValuePair(ApiConstants.PSW_STRING, mapNoJson.get(ApiConstants.PSW_STRING)));
		params.add(new BasicNameValuePair(ApiConstants.SC_STRING, mapNoJson.get(ApiConstants.SC_STRING)));
		params.add(new BasicNameValuePair(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING)));
		return params;
	}

	public JSONObject setParametersLoginArqJson(final ParametersTO parameters) {
		final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) ((Hashtable<String, String> ) parameters.getParameters()).clone();

		convertStringToValues(mapNoJson);
		JSONObject object=null;
		try {
			object=new JSONObject();
			object.put(ApiConstants.USR_STRING, mapNoJson.get(ApiConstants.USR_STRING));
			object.put(ApiConstants.PSW_STRING, mapNoJson.get(ApiConstants.PSW_STRING));
			object.put(ApiConstants.SC_STRING, mapNoJson.get(ApiConstants.SC_STRING));
			object.put(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING));
		} catch (JSONException e) {
			object=new JSONObject();
		}
		return object;
	}
	public void setParametersGlobalJson(final ParametersTO parameters,final HttpGet httpGet) {
		final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) ((Hashtable<String, String> ) parameters.getParameters()).clone();

		convertStringToValues(mapNoJson);
		httpGet.setHeader(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING));

	}

	public void setParametesDetalleILArqJsonPrueba(final ParametersTO parameters, final HttpGet httpGet) {
		final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) ((Hashtable<String, String> ) parameters.getParameters()).clone();

		convertStringToValues(mapNoJson);

		if(CommContext.allowLog) {
			Log.e("HEADER DETALLE", "IUM-"+String.valueOf(mapNoJson.get(ApiConstants.IUM_STRING)));
		}
		httpGet.setHeader(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING));
	}

	public JSONObject setParametesGatsILArqJsonPrueba(final ParametersTO parameters, final HttpPost httpPost) {
		final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) ((Hashtable<String, String> ) parameters.getParameters()).clone();

		convertStringToValues(mapNoJson);

		if(CommContext.allowLog) {
			Log.e("HEADER GATS", "IUM-"+String.valueOf(mapNoJson.get(ApiConstants.IUM_STRING)));
		}
		httpPost.setHeader(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING));

		JSONObject object=null;
		try {
			object=new JSONObject();
			if(CommContext.allowLog){
				Log.e("PARAM GATS", "{"+
						ApiConstants.ACCOUNT_NUMBER_IL+":"+mapNoJson.get(ApiConstants.ACCOUNT_NUMBER_IL)+","+
						ApiConstants.AMOUNT_IL+":"+mapNoJson.get(ApiConstants.AMOUNT_IL)+","+
						ApiConstants.RATE_IL+":"+mapNoJson.get(ApiConstants.RATE_IL)+"}");
			}
			object.put(ApiConstants.ACCOUNT_NUMBER_IL, mapNoJson.get(ApiConstants.ACCOUNT_NUMBER_IL));
			object.put(ApiConstants.AMOUNT_IL, mapNoJson.get(ApiConstants.AMOUNT_IL));
			object.put(ApiConstants.RATE_IL, mapNoJson.get(ApiConstants.RATE_IL));
		} catch (JSONException e) {
			object=new JSONObject();
		}
		return object;
	}

	public JSONObject setParametesContratacionILArqJsonPrueba(final ParametersTO parameters, final HttpPost httpPost) {
		final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) ((Hashtable<String, String> ) parameters.getParameters()).clone();

		convertStringToValues(mapNoJson);

		if(CommContext.allowLog) {
			Log.e("HEADER CONTRATACION", "IUM-"+String.valueOf(mapNoJson.get(ApiConstants.IUM_STRING)));
			Log.e("HEADER CONTRATACION", "OTP-"+String.valueOf(mapNoJson.get(ApiConstants.OTP_STRING)));
		}
		httpPost.setHeader(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING));
		httpPost.setHeader(ApiConstants.OTP_STRING, mapNoJson.get(ApiConstants.OTP_STRING));

		JSONObject object=null;
		try {
			object=new JSONObject();
			if(CommContext.allowLog){
				Log.e("PARAM CONTRATACION", "{"+
						ApiConstants.ACCOUNT_OPENING_IL+":"+mapNoJson.get(ApiConstants.ACCOUNT_OPENING_IL)+","+
						ApiConstants.AMOUNT_IL+":"+mapNoJson.get(ApiConstants.AMOUNT_IL)+","+
						ApiConstants.RATE_IL+":"+mapNoJson.get(ApiConstants.RATE_IL)+","+
						ApiConstants.CELL_PHONE_IL+":"+mapNoJson.get(ApiConstants.CELL_PHONE_IL)+","+
						ApiConstants.CR_IL+":"+mapNoJson.get(ApiConstants.CR_IL)+","+
						ApiConstants.MANAGEMENT_UNIT_IL+":"+mapNoJson.get(ApiConstants.MANAGEMENT_UNIT_IL)+","+
						ApiConstants.NOMINAL_GAT_IL+":"+mapNoJson.get(ApiConstants.NOMINAL_GAT_IL)+","+
						ApiConstants.REAL_GAT_IL+":"+mapNoJson.get(ApiConstants.REAL_GAT_IL)+"}");
			}
			object.put(ApiConstants.ACCOUNT_OPENING_IL, mapNoJson.get(ApiConstants.ACCOUNT_OPENING_IL));
			object.put(ApiConstants.AMOUNT_IL, mapNoJson.get(ApiConstants.AMOUNT_IL));
			object.put(ApiConstants.RATE_IL, mapNoJson.get(ApiConstants.RATE_IL));
			object.put(ApiConstants.CELL_PHONE_IL, mapNoJson.get(ApiConstants.CELL_PHONE_IL));
			object.put(ApiConstants.CR_IL, mapNoJson.get(ApiConstants.CR_IL));
			object.put(ApiConstants.MANAGEMENT_UNIT_IL, mapNoJson.get(ApiConstants.MANAGEMENT_UNIT_IL));
			object.put(ApiConstants.NOMINAL_GAT_IL, mapNoJson.get(ApiConstants.NOMINAL_GAT_IL));
			object.put(ApiConstants.REAL_GAT_IL, mapNoJson.get(ApiConstants.REAL_GAT_IL));

		} catch (JSONException e) {
			object=new JSONObject();
		}
		return object;
	}

	public JSONObject setParametesTransferenciaILArqJsonPrueba(final ParametersTO parameters, final HttpPost httpPost) {
		final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) ((Hashtable<String, String> ) parameters.getParameters()).clone();

		convertStringToValues(mapNoJson);

		if(CommContext.allowLog) {
			Log.e("HEADER TRANSFER", "IUM-"+String.valueOf(mapNoJson.get(ApiConstants.IUM_STRING)));
		}
		httpPost.setHeader(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING));

		JSONObject object=null;
		try {
			object=new JSONObject();
			if(CommContext.allowLog){
				Log.e("PARAM TRANSFER", "{"+
						ApiConstants.ACCOUNT_SENDER_IL+":"+mapNoJson.get(ApiConstants.ACCOUNT_SENDER_IL)+","+
						ApiConstants.ACCOUNT_RECIEVER_IL+":"+mapNoJson.get(ApiConstants.ACCOUNT_RECIEVER_IL)+","+
						ApiConstants.AMOUNT_IL+":"+mapNoJson.get(ApiConstants.AMOUNT_IL)+","+
						ApiConstants.CELL_PHONE_NUMBER_IL+":"+mapNoJson.get(ApiConstants.CELL_PHONE_NUMBER_IL)+"}");
			}
			object.put(ApiConstants.ACCOUNT_SENDER_IL, mapNoJson.get(ApiConstants.ACCOUNT_SENDER_IL));
			object.put(ApiConstants.ACCOUNT_RECIEVER_IL, mapNoJson.get(ApiConstants.ACCOUNT_RECIEVER_IL));
			object.put(ApiConstants.AMOUNT_IL, mapNoJson.get(ApiConstants.AMOUNT_IL));
			object.put(ApiConstants.CELL_PHONE_NUMBER_IL, mapNoJson.get(ApiConstants.CELL_PHONE_NUMBER_IL));
		} catch (JSONException e) {
			object=new JSONObject();
		}
		return object;
	}


	public JSONObject setParametesTerminosCondicionesILArqJsonPrueba(final ParametersTO parameters, final HttpPost httpPost) {
		final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) ((Hashtable<String, String> ) parameters.getParameters()).clone();

		convertStringToValues(mapNoJson);

		if(CommContext.allowLog){
			Log.e("HEADER CONTRATO", "IUM-"+String.valueOf(mapNoJson.get(ApiConstants.IUM_STRING)));
		}
		httpPost.setHeader(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING));

		JSONObject object=null;
		try {
			object=new JSONObject();
			if(CommContext.allowLog){
				Log.e("PARAM CONTRATO", "{"+
						ApiConstants.ACCOUNT_NUMBER_IL+":"+mapNoJson.get(ApiConstants.ACCOUNT_NUMBER_IL)+","+
						ApiConstants.CVE_CAMP_STRING+":"+mapNoJson.get(ApiConstants.CVE_CAMP_STRING)+","+
						ApiConstants.CREATE_CONTRACT_IL+":"+mapNoJson.get(ApiConstants.CREATE_CONTRACT_IL)+","+
						ApiConstants.NUM_CONTRACT_IL+":"+mapNoJson.get(ApiConstants.NUM_CONTRACT_IL)+"}");
			}
			object.put(ApiConstants.ACCOUNT_NUMBER_IL, mapNoJson.get(ApiConstants.ACCOUNT_NUMBER_IL));
			object.put(ApiConstants.CVE_CAMP_STRING, mapNoJson.get(ApiConstants.CVE_CAMP_STRING));
			object.put(ApiConstants.CREATE_CONTRACT_IL, mapNoJson.get(ApiConstants.CREATE_CONTRACT_IL));
			object.put(ApiConstants.NUM_CONTRACT_IL, mapNoJson.get(ApiConstants.NUM_CONTRACT_IL));
		} catch (JSONException e) {
			object=new JSONObject();
		}
		return object;
	}
	/**
	 * que convierte los parametros de la clase ParametersTO
	 * a una lista de NameValuePair
	 * @throws JSONException 
	 * */
	@SuppressWarnings("unchecked")
	public List <NameValuePair> setParameters(final ParametersTO parameters) throws JSONException{
		final Hashtable<String, String> map = (Hashtable<String, String> ) parameters.getParameters();
		final Hashtable<String, String> mapNoJson = (Hashtable<String, String> ) parameters.getParameters();
		convertValuesToString(map);
		String paramsString;

		
		if(parameters.isJson()){
			paramsString=setParametersAux( parameters, map);
		}else{
			paramsString=formaParamsNoJson(parameters.getOperationCode(),mapNoJson);

			if(CommContext.operacionCode != null &&
					CommContext.operacionCode.equals(parameters.getOperationId()) && EncriptarConstants.encriptar) {
				final Boolean exist=mapNoJson.containsKey(ApiConstants.ORDER);
				if(exist){
					mapNoJson.remove(ApiConstants.ORDER);
				}
				mapNoJson.put("OP", parameters.getOperationCode());
				Encripcion.setContext(CommContext.getContext());
				Encripcion.encriptarPeticionString(mapNoJson, CommContext.listaEncriptar, "*", "*", true);
				paramsString=formaParamsNoJson(parameters.getOperationCode(),mapNoJson);
				if(paramsString.startsWith("OP") && mapNoJson.size()==1){
					//se quita el parametro op ya que no debe ir cuando se encripta
					paramsString=paramsString.subSequence(paramsString.indexOf("*EN")+1,paramsString.length()).toString();
				}
				CommContext.operacionCode = null;
				CommContext.listaEncriptar = null;
			}


		}
		String operacion=getOperationCodeV(parameters.getOperationCode(),parameters.getIsJsonValue(),parameters.isJson());

		if(parameters.getOperationId() == ApiConstants.ID_SENDER){
			operacion=ApiConstants.JSON_OPERATION_ALERTAS_DIGITALES;
			//operacion=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
		}
		
		final List <NameValuePair> params=new ArrayList <NameValuePair>();
		params.add(new BasicNameValuePair(ApiConstants.OPERACION_MAYUS, operacion));
		params.add(new BasicNameValuePair(ApiConstants.OPERATION_LOCALE_PARAMETER, ApiConstants.OPERATION_LOCALE_VALUE));
		params.add(new BasicNameValuePair(ApiConstants.PARAM_PAR_INICIO, paramsString));
		
		return params;
	}

	private String setParametersAux(final ParametersTO parameters,final Map<String, String> map) throws JSONException{
		JSONObject json;
		String paramsAux;
		try {
			if(CommContext.operacionCode != null &&
					CommContext.operacionCode.equals(parameters.getOperationId())) {
				map.put(ApiConstants.OPERACION, parameters.getOperationCode());
				Encripcion.setContext(CommContext.getContext());
				Encripcion.encriptaCadenaAutenticacion(map, CommContext.listaEncriptar);
				json = new JSONObject(map.toString());
				CommContext.operacionCode = null;
				CommContext.listaEncriptar = null;
			} else {
				json = new JSONObject(map.toString());
				json.put(ApiConstants.OPERACION, parameters.getOperationCode());
			}
			paramsAux=json.toString();
		}catch (JSONException e){
			json = new JSONObject(map);
			json.put(ApiConstants.OPERACION, parameters.getOperationCode());
			paramsAux=json.toString();
			paramsAux=paramsAux.replaceAll("\\\\\"","\"").replaceAll("\"\"", "\"");
			paramsAux=paramsAux.replaceAll("\"\\{","{").replaceAll("\\}\"","}");

		}
		return paramsAux.replaceAll(ApiConstants.DOBLE_SLASH, ApiConstants.EMPTY_LINE);
	}

	public String formaParamsNoJson(final String operacion,final Map<String, String> parameters){
		final StringBuffer sb = new StringBuffer("OP").append(operacion);
		if (parameters != null) {
			final Boolean exist=parameters.containsKey(ApiConstants.ORDER);
			if(exist){
				String orden = parameters.get(ApiConstants.ORDER);
				orden=orden.replace("\"","");
				
				final StringTokenizer st2 = new StringTokenizer(orden, ApiConstants.PARAMETRO_SEPARADOR);
				while (st2.hasMoreElements()) {
					final String elementName=st2.nextElement().toString();
					sb.append(ApiConstants.PARAMETRO_SEPARADOR);
					sb.append(elementName);
					sb.append(parameters.get(elementName));
				}
			}else{
	        	for (final String key : parameters.keySet()) {
	        		 sb.append(ApiConstants.PARAMETRO_SEPARADOR);
	        		 sb.append(key);
	        		 sb.append(parameters.get(key));
	    		}	
	        }
		}
	    return sb.toString().replace("\"", "");
	}
	
	public void convertValuesToString(final Map<String, String> map) {

		for (final String key : map.keySet()) {
			map.put(key,ApiConstants.SLASH +map.get(key)+ApiConstants.SLASH );
			}
	}

	public void convertStringToValues(final Map<String, String> map) {
		for (final String key : map.keySet()) {
			map.put(key,map.get(key).toString().replace("\"","") );
		}
	}

	/**
	 * metodo para asignar los parametros a las peticiones
	 * post
	 * */
	public RequestService build(final RequestService request, final HttpPost httpPost,
			final List <NameValuePair> listParams ,final HttpGet httpGet) throws UnsupportedEncodingException{
		if(httpGet!=null){
            httpGet.setHeader("Accept-Encoding","UTF-8");
			request.setRequestPost(httpPost);
			request.setRequestGet(httpGet);
		}else{
			request.setRequestGet(null);
            httpPost.setHeader("Accept-Encoding", "UTF-8");
			if(listParams != null){
				httpPost.setEntity(new UrlEncodedFormEntity(listParams));
			}
			request.setRequestPost(httpPost);
		}
		return request;
	}

	/* TODO refactor
	 * 
	 */
	public String getOperationCodeAux(final String operation){
		String operationCodeValueBMovil=ApiConstants.EMPTY_LINE;
		if(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_TDC].equals(operation)){
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
		}else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.FAVORITE_PAYMENT_OPERATION_BBVA])){
			operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CONSULTA_INTERBANCARIOS;
		}else {
			operationCodeValueBMovil = getOperationCodeAux1(operation);

			if (ApiConstants.EMPTY_LINE.equals(operationCodeValueBMovil)) {
				operationCodeValueBMovil = getOperationCodeAux2(operation);

				if (ApiConstants.EMPTY_LINE.equals(operationCodeValueBMovil)) {
					operationCodeValueBMovil = getOperationCodeAux3(operation);

					if (ApiConstants.EMPTY_LINE.equals(operationCodeValueBMovil)) {
						operationCodeValueBMovil = getOperationCodeAux4(operation);

						if (ApiConstants.EMPTY_LINE.equals(operationCodeValueBMovil)) {
							operationCodeValueBMovil = getOperationCodeAux5(operation);
						}
					}
				}
			}
		}

		return operationCodeValueBMovil;
	}

	private String getOperationCodeAux5(final String operation){
		String operationCodeValueBMovil=ApiConstants.EMPTY_LINE;
		if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS])) {
			operationCodeValueBMovil = ApiConstants.CODE_TEMP;
		}else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_PAGO_CREDITO_CH])) {
			operationCodeValueBMovil = ApiConstants.CODE_TEMP;
		} else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_MIGRA_BASICO])) {
			operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_RECORTADO;
		}else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTA_DETALLE_OFERTA_TDC]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTA_TERMINOS_CONDICIONES_TDC]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.ACEPTACION_OFERTA_TDC]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.FORMATOS_FINALES_TDC]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OFERTA_DOMICILIACION])
				) {
			operationCodeValueBMovil = ApiConstants.JSON_OPERATION_VENTA_TDC;
		}else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_PATRIMONIAL])){
			operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CODE_PATRIMONIAL;

		}else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ENVIO_CORREO]) && 53==opActual){
			operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CODE_VALUE;
		}else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSUTA_CORREO])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CALCULO_ALTERNATIVA])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ENVIO_CORREOS])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSUTA_ALTERNATIVA])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_DETALLE_ALTERNATIVA])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONTRATA_ALTERNATIVA_CONSUMO])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_DETALLEALTERNATIVATDC])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.ENVIO_CORREO_HIPO_AUTO_OP])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.ENVIO_CORREO_CONSUMO_OP])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.ENVIO_CORREO_TDC_ILC_OP])
				//|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.EXITO_OFERTA_CONSUMO])
				){
			operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CODE_CREDITOS;
		}
		else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_COORDENADAS]))
		{
			operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CODE_VALUE_PM;
		}
		else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_AUTENTICACIONTOKENPM]))
		{
			operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CODE_VALUE_PM;
		}
		else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_SINCRONIZACIONPMTOKEN]))
		{
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE_PM;
		}
		else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_EXPORTACIONPMTOKEN]))
		{
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE_PM;
		}else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTA_DETALLE_SEGUROS])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTA_TERMINOS_CONDICIONES_SEGUROS])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTA_FORMALIZACION_GF]))
		{
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE_SEGUROS;
		}
		else if(operation.equals(ApiConstants.OP_ONE_CLIC_TDCADICIONAL))
		{
			operationCodeValueBMovil=ApiConstants.JSON_OPERATION_VENTA_TDC;
		}
		else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_MOVIMIENTOS_TRANSITO]))
		{
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
		}
		else if (opActual == ApiConstants.OP_CONSULTA_GESTOR_REMOTO || opActual == ApiConstants.OP_CONSULTA_CONTRATO_GESTOR_REMOTO
				|| opActual == ApiConstants.OP_CONTRATO_GESTOR_REMOTO || opActual == ApiConstants.OP_DETALLE_GESTOR || opActual == ApiConstants.OP_ENVIO_CORREO_GESTOR)
		{
			operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_GESTOR;
		}
		return operationCodeValueBMovil;
	}

	private String getOperationCodeAux4(final String operation){
		String operationCodeValueBMovil=ApiConstants.EMPTY_LINE;
		if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTAR_ESTATUS_EC])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.ACTUALIZAR_ESTATUS_EC]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTAR_TEXTO_PAPERLESS]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.INHIBIR_ENVIO_EC]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OBTENER_PERIODO_EC])||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.ENVIAR_CORREO_EC])||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTAR_ESTADO_CUENTA])

				){
			operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CODE_VALUE_PAPERLESS;

		} else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_OTROS_CREDITOS]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_DETALLE_OTROS_CREDITOS]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_SMS_OTROS_CREDITOS]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_CORREO_OTROS_CREDITOS])
				){
			operationCodeValueBMovil = ApiConstants.CODE_TEMP;
		}else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_INVERSIONES_PLAZO])) {
			operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
		}else if (opActual == ApiConstants.ENVIO_SMS_PERFIL
				|| opActual == ApiConstants.VALIDA_FOLIO_PERFIL){
			operationCodeValueBMovil = ApiConstants.OPERACION_CODE_LINK;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_AYUDA_PRESTAMO_CONSUMO])) {
			operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CODE_VALUE_CONSUMO;
		}else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS])) {
			operationCodeValueBMovil = ApiConstants.CODE_TEMP;
		}else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_PAGO_CREDITO_CH])) {
			operationCodeValueBMovil = ApiConstants.CODE_TEMP;
		} else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_SIMULA_IMPORTES_PAGO_CREDITO])) {
			operationCodeValueBMovil = ApiConstants.CODE_TEMP;
		} else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ENVIO_NOTI_PAGO_CREDITO])) {
			operationCodeValueBMovil = ApiConstants.CODE_TEMP;
		} else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTAR_PAGOS_CREDITOS])) {
			operationCodeValueBMovil = ApiConstants.CODE_TEMP;
		} else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_DETALLE_PAGOS_CREDITOS])) {
			operationCodeValueBMovil = ApiConstants.CODE_TEMP;
		}
		return operationCodeValueBMovil;
	}

	private String getOperationCodeAux3(final String operation){
		String operationCodeValueBMovil=ApiConstants.EMPTY_LINE;

		if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CHANGE_PASSWORD_OPERATION])){
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE;
		}else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.LOGIN_OPERATION])){
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE;
		}else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_CUENTAS_ALERTAS])  ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_DETALLE_CUENTA_ALERTAS]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ALTA_MOD_SERVICIO_ALERTAS]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_BAJA_SERVICIO_ALERTAS]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_MENSAJES_ENVIADOS_ALERTAS]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_DETALLE_MENSAJE_ENV_ALERTAS]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ID_SENDER])
				) {
			operationCodeValueBMovil = ApiConstants.OPERATION_CODE_ALERTAS;

		}
		return operationCodeValueBMovil;
	}

	private String getOperationCodeAux2(final String operation){
		String operationCodeValueBMovil=ApiConstants.EMPTY_LINE;
		if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_TDC])){
			operationCodeValueBMovil=ApiConstants.JSON_OPERATION_MEJORAS_CODE_VALUE;
		} else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_SINC_EXP_TOKEN])){
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
		} else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.SINCRONIZACION_SOFTTOKEN])){
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE;
		}else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CLOSE_SESSION_OPERATION])){
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE;
		}else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ACTIVACION])){
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE;
		}else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.EXPORTACION_SOFTTOKEN])){
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE;
		}else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CATALOGO_APLICACIONES]) ){
			operationCodeValueBMovil=ApiConstants.CBC_CODE_VALUE;
		}else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.HAMBURGUESA]) ){
			operationCodeValueBMovil=ApiConstants.CBC_CODE_VALUE;
		}
		return operationCodeValueBMovil;
	}

	private String getOperationCodeAux1(final String operation){
		String operationCodeValueBMovil=ApiConstants.EMPTY_LINE;
		 if (ApiConstants.OPERATION_CODES[ApiConstants.OP_RETIRO_SIN_TAR].equals(operation)) {
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
		}else if (ApiConstants.OPERATION_CODES[ApiConstants.CONSULTA_SIN_TARJETA].equals(operation)) {
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
		}else if (ApiConstants.OPERATION_CODES[ApiConstants.OP_RETIRO_SIN_TAR_12_DIGITOS].equals(operation)) {
			operationCodeValueBMovil=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
		}else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_SOLICITAR_ALERTAS])
				|| operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CAMBIA_PERFIL])) {
			operationCodeValueBMovil=ApiConstants.JSON_OPERATION_RECORTADO_CODE_VALUE;
		} else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_INTERBANCARIOS]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_SPEI]) ||
				operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ENVIOCORREO_SPEI])){
			operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CONSULTA_INTERBANCARIOS;
		 }else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTAR_CLABE_OPERACION])){
			 operationCodeValueBMovil=ApiConstants.JSON_OPERATION_RECORTADO_CODE_VALUE;
		}

		return operationCodeValueBMovil;
	}


	private String getOperationCodeAuxNotJson(final String operation){
		if(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_TDC].equals(operation)){
        	return ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        	
        }else if(ApiConstants.OPERATION_CODES[ApiConstants.OP_SOLICITAR_ALERTAS].equals(operation)){
        	return ApiConstants.OPERATION_CODE_VALUE_RECORTADO;
        }else{
            return ApiConstants.OPERATION_CODE_VALUE;
        }
	}
	/* TODO refactor
	 * 
	 */
	public String getOperationCodeV(final String operation, final isJsonValueCode isJsonValue,final boolean isJson){
		String operationCodeValueBMovil=ApiConstants.EMPTY_LINE;
		//JAIG SE AGREGA PARA OPERACIONES NO JSON 
		if(isJson){
			operationCodeValueBMovil=getOperationCodeAux(operation);
		}else{
			operationCodeValueBMovil=getOperationCodeAuxNotJson(operation);
		}		
		if(operationCodeValueBMovil.equals(ApiConstants.EMPTY_LINE)) {
        	//One Click
            if(isJsonValue.name()==ApiConstants.ONCLICK ){
            	operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CODE_VALUE_ONECLICK;
            }
            else if(isJsonValue.name()==ApiConstants.CONSUMO ){
            	operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CODE_VALUE_CONSUMO;
            	}
         else if (isJsonValue.name() == ApiConstants.DEPOSITOS ) {
        	 operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CODE_VALUE_DEPOSITOS;
        } else{
        	operationCodeValueBMovil=ApiConstants.JSON_OPERATION_CODE_VALUE;
        	} 
            //Termina One click
        	//sb.append(Server.JSON_OPERATION_CODE_VALUE);
        }
		return operationCodeValueBMovil;
		
	}

	/**
	 * metodo para obtener el nombre de la operacion desde
	 * el properties con el id de operacion que recive el api
	 */
	private String getOperationCode(final int operationId){

		//se forma el id que se buscara el el properties de configuracion para traer el nombbre de la operacion
		final StringBuilder operationName = new StringBuilder(ApiConstants.PREFIX_OPERACION_CODE);
		operationName.append(operationId);
		//se obtiene el operationCode
		return CommContext.getConfigPropertyValue(operationName.toString());
	}
	
	/**
	 * Metodo que pasa un ArrayList<NameValuePair>
	 * a un string de url tipo get
	 * @param parametros
	 * @return String
	 */
	private String getParamsToGetMethod(final List<NameValuePair> parametros){
		final StringBuffer sb = new StringBuffer();
		if(parametros!=null && parametros.size()>0){
			sb.append(ApiConstants.SIGNO_INTERROGACION);
		}
		int index=0;
		for(final NameValuePair nvPair : parametros) {
			sb.append(nvPair.getName());
			sb.append(ApiConstants.SIGNO_IGUAL);
			sb.append(nvPair.getValue());
			index++;
			if(parametros.size()>index){
				sb.append(ApiConstants.AMP_STRING);
			}
			
		}
		    return sb.toString();
	}

    /**
     * Metodo que pasa un ArrayList<NameValuePair>
     * a un string de url tipo get
     *
     * @param parametros Parametros a concanetar
     * @return String Regresa los parametros en formato GET
     */
    private String getParamsToGet(final Map<String, String> parametros) {
        final StringBuffer sb = new StringBuffer();
        final List<NameValuePair> listaParams = new ArrayList<NameValuePair>();
        if (parametros != null && parametros.size() > 0) {
            sb.append(ApiConstants.SIGNO_INTERROGACION);
            for (Map.Entry<String, String> valor : parametros.entrySet()) {
                listaParams.add(new BasicNameValuePair(valor.getKey(), valor.getValue()));
            }
            sb.append(URLEncodedUtils.format(listaParams, "UTF-8"));
        }
        return sb.toString();
    }

    private Map<String, String> cloneParams(Map<String, String> params) {
        Map<String, String> paramsClone = new Hashtable<String, String>();
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                paramsClone.put(entry.getKey(), entry.getValue());
            }
        }
        return paramsClone;
    }



}