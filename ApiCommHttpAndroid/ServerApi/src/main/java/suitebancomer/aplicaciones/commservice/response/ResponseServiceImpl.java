package suitebancomer.aplicaciones.commservice.response;

import org.apache.http.HttpResponse;


/** 
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class ResponseServiceImpl implements IResponseService  {

	private int responde;

	private HttpResponse response;
	
	private Object objResponse;
	
	private String responseString;
	
    /**
     * URL for mandatory updating(the mandatory updating order is received as.
     * error with code MBANK1111)
     */
    private String updateURL = null;
	
	 /**
     * Status of the current response.
     */
    private int status = 0;

    /**
     * Response error code.
     */
    private String messageCode = null;

    /**
     * Error message.
     */
    private String messageText = null;

	
	public HttpResponse getResponse(){
		return response;
	}

	public void setResponse(final HttpResponse response){
		this.response=response;
	}

	public int getResponde() {
		return responde;
	}

	public void setResponde(final int responde) {
		this.responde = responde;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(final int status) {
		this.status = status;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(final String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(final String messageText) {
		this.messageText = messageText;
	}

	public Object getObjResponse() {
		return objResponse;
	}

	public void setObjResponse(final Object objResponse) {
		this.objResponse = objResponse;
	}

	public String getUpdateURL() {
		return updateURL;
	}

	public void setUpdateURL(final String updateURL) {
		this.updateURL = updateURL;
	}
	
	public String getResponseString() {
		return responseString;
	}


	public void setResponseString(final String responseString) {
		this.responseString = responseString;
	}

}