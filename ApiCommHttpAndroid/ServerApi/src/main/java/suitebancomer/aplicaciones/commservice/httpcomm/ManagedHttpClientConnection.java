package suitebancomer.aplicaciones.commservice.httpcomm;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.IResponseService;


/**
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class ManagedHttpClientConnection {

	public HttpInvoker mHttpInvoker;

	/**
	 * manager conection llamara a httpinvoker
	 * enviando el request y reciviendo de regreso un response
	 * 
	 * 
	 * */
	public IResponseService getInstance(final RequestService aRequest) throws ClientProtocolException, IOException{
		mHttpInvoker=new HttpInvoker();
		//se llama el HttpInvoker
		return mHttpInvoker.invoke(aRequest);
	}

}