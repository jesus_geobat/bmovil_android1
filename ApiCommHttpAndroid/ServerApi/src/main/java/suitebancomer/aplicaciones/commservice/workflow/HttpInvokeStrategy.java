package suitebancomer.aplicaciones.commservice.workflow;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import suitebancomer.aplicaciones.commservice.httpcomm.HttpInvoker;
import suitebancomer.aplicaciones.commservice.httpcomm.HttpRequestBase;
import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.IResponseService;


/**
 * This class implements the algorithm using the Strategy interface.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class HttpInvokeStrategy implements IInvokeStrategy {
	
	private final HttpRequestBase httpRequest;
	
	public HttpInvokeStrategy(){
		httpRequest=new HttpInvoker();
	}
	
	public HttpInvokeStrategy(final HttpRequestBase httpRequest){
		this.httpRequest=httpRequest;
	}

	/**
	 * 
	 * ManagedHttpClientConnection bypass
	 * 
	 * @param aHandler
	 * @param aRequest
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	public IResponseService process(final RequestService aRequest) throws ClientProtocolException, IOException{

		return httpRequest.invoke(aRequest);

	}


}