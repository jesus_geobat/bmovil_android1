package suitebancomer.aplicaciones.commservice.workflow;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.IResponseService;


/**
 * This class defines an interface for handling requests and optionally implements
 * the successor link.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public  class InvokeHandler extends AbstractInvokeHandler{

	private IInvokeHandle next;
	
	/**
	 * 
	 * se invoca el metodo handleRequest de la clase CreateRequestBuilderHandler
	 * enviando un request solo con los parametros que se recibieron desde el proxy
	 * y esperando de resultado un responseService
	 * 
	 * @param request
	 * @throws UnsupportedEncodingException 
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws JSONException 
	 * @throws IllegalStateException 
	 */
	public IResponseService handleRequest(final RequestService request, final Class<?> objResponse)
			throws UnsupportedEncodingException, ClientProtocolException, IOException, IllegalStateException, JSONException{
		
		//seteamos los parametros al requesta con el fin de que el CreateRequestBuilderHandler tenga los parametros
		request.setParametersTO(getParametesTO());
		
		final CreateRequestBuilderHandler requestBuilder = new CreateRequestBuilderHandler();
		this.setNext(requestBuilder);
		
		final SendRequestHandler sender = new SendRequestHandler();
		requestBuilder.setNext(sender);
		
		final AdaptResponseHandler responseHandler = new AdaptResponseHandler();
		sender.setNext(responseHandler);
		//se llama la siguente capa que es requestBuilderHandler
		IResponseService resp=null;
		resp = next.handleRequest(request, objResponse);
		return resp;

	}

	public IResponseService handleRequestImage(final RequestService request, final Class<?> objResponse)
			throws UnsupportedEncodingException, ClientProtocolException,
			IOException, IllegalStateException, JSONException{

		//seteamos los parametros al requesta con el fin de que el CreateRequestBuilderHandler tenga los parametros
		request.setParametersTO(getParametesTO());

		final CreateRequestBuilderHandler requestBuilder = new CreateRequestBuilderHandler();
		this.setNext(requestBuilder);

		final SendRequestHandler sender = new SendRequestHandler();
		requestBuilder.setNext(sender);

		final AdaptResponseHandler responseHandler = new AdaptResponseHandler();
		sender.setNext(responseHandler);
		//se llama la siguente capa que es requestBuilderHandler
		IResponseService resp=null;
		resp = next.handleRequestImage(request, objResponse);
		return resp;

	}
	

	/**
	 * @return the next
	 */
	public final IInvokeHandle getNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public final void setNext(final IInvokeHandle next) {
		this.next = next;
	}

}