package suitebancomer.aplicaciones.commservice.commons;

/**
 * Created by evaltierrah on 19/07/16.
 */
public enum ContentType {

    JSON("application/json"),
    TEXT_PLAIN("text/plain"),
    FORM_URLENCODED("application/x-www-form-urlencoded");

    /**
     * Indica el tipo de contenido que se enviara en la url
     */
    String contentType;

    /**
     * Asigna el tipo de contenido
     *
     * @param contentType Tipo de contenido
     */
    ContentType(final String contentType) {
        this.contentType = contentType;
    }

    /**
     * Regresa el tipo de contenido
     *
     * @return Tipo de contenido
     */
    public String getContentType() {
        return contentType;
    }

}
