package suitebancomer.aplicaciones.commservice.response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.commservice.commons.PojoGeneral;
import suitebancomer.aplicaciones.commservice.commons.Utilities;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;


/**
 * This class implements the Product interface.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class JsonAdapter implements IAdapter {

	
	/**
	 * el json adapeter
	 * se encarga de llenar los parametros de response service 
	 * comoo el estatus el codigo del mensaje y el texto
	 * con la informacion del response que se recibio
	 * y el parseo de datos en json.
	 * 
	 * esta respuesta ya formada y se retorna asta el handle que invoco
	 * el api
	 * */
	public void transformResponse( final IResponseService response, final Class<?> objResponse)throws IllegalStateException, IOException, JSONException{
		//convierte la cadena el contenido en una cadena


		if(response.getResponse().getStatusLine().getStatusCode() == ApiConstants.ESTATUS_OK_CODE && response.getResponse().getEntity()!=null){
			/*final BufferedReader rd = new BufferedReader( new InputStreamReader(response.getResponse().getEntity().getContent(), "ISO-8859-1"));
	 
			//convertimos la respuesta a string
			final StringBuffer result = new StringBuffer();
			String line = ApiConstants.EMPTY_LINE;
			
			while (line  != null) {
				result.append(line);
				line = rd.readLine();
			}*/
			final StringBuffer result = readResponse(response.getResponse().getEntity().getContent(),
                    response.getResponse());

            final String resultado = Utilities.replaceSpecialCharacters(result);

            if (CommContext.allowLog){
				Log.e("APIComm:JsonAdapter", resultado );
			}
			JSONObject json;
			try {
				 final Gson gson = new Gson();
				 final Object  m = gson.fromJson(resultado, objResponse);

				response.setObjResponse(m);
				response.setResponseString(resultado);
				json = new JSONObject(resultado);
                android.util.Log.e("resultado server: ", resultado);
		    } catch (JSONException ex) {
		    	json=null;
				response.setResponseString(resultado );
		    }catch (JsonSyntaxException ex){
				json=null;
				response.setResponseString(resultado );
			}

			if (objResponse != PojoGeneral.class){
				setCodesMessageToResponse(json,response);
			}

		}else{
            if(response.getResponse().getEntity()!=null) {
                //convertimos la respuesta a string

                final StringBuffer resulta = readResponse(response.getResponse().getEntity().getContent(),
                        response.getResponse());

                final String resultado = Utilities.replaceSpecialCharacters(resulta);
                android.util.Log.e("resultado server: ", resultado);

				if(response.getResponse().getStatusLine().getStatusCode() == 409 || response.getResponse().getStatusLine().getStatusCode() == 500
						|| response.getResponse().getStatusLine().getStatusCode() == 430)
				response.setResponseString(resultado);
                if (objResponse == PojoGeneral.class){
                    response.setResponseString(resultado);
                }

			}
			try {
				response.setObjResponse(objResponse.newInstance());
				setCodesMessageToResponse(null,response);
			} catch (InstantiationException e) {
				setCodesMessageToResponse(null,response);
			} catch (IllegalAccessException e) {
				setCodesMessageToResponse(null,response);
			}
		}
	}

	private StringBuffer readResponse(final InputStream inputStream, final HttpResponse response) throws IOException {
        final BufferedReader rd;
        if (response.getAllHeaders() != null && response.getAllHeaders().length > 0) {
            if (response.getAllHeaders()[1].toString().equals("content-type: text/html;charset=ISO-8859-1")) {
                rd = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
            } else {
                rd = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            }
        } else {
            rd = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
        }
		//convertimos la respuesta a string
		final StringBuffer result = new StringBuffer();
		String line = ApiConstants.EMPTY_LINE;

		while (line  != null) {
			result.append(line);
			line = rd.readLine();
		}
		return result;
	}

	/**
	 * se llenan los parametro genericos del response 
	 * */
	private void setCodesMessageToResponse(final JSONObject jsonResponse,
											final IResponseService response) 
			throws IOException, JSONException {

        if (jsonResponse == null) {
        	response.setStatus(OPERATION_STATUS_UNKNOWN);
            response.setMessageCode( null);
            response.setMessageText(null);

        } else {

        	response.setStatus(OPERATION_STATUS_UNKNOWN);
            final Result result = parseResult(jsonResponse);
            final String statusText = result.getStatus();
            if (ApiConstants.STATUS_OK.equals(statusText)) {
            	response.setStatus(ApiConstants.OPERATION_SUCCESSFUL);
            } else if (ApiConstants.STATUS_OPTIONAL_UPDATE.equals(statusText)) {
            	response.setStatus(ApiConstants.OPERATION_OPTIONAL_UPDATE);
            } else if (ApiConstants.STATUS_WARNING.equals(statusText)) {
            	response.setStatus(ApiConstants.OPERATION_WARNING);
            } else if (ApiConstants.STATUS_ERROR.equals(statusText)) {
            	response.setStatus(ApiConstants.OPERATION_ERROR);
            	response.setUpdateURL(result.getUpdateURL());
            }

            response.setMessageCode(result.getCode());
            response.setMessageText(result.getMessage());

        }

    }
	
	 /**
     * Parse the operation result from received message.
     * 
     * @return the operation result
     * @throws IOException on communication errors
	 * @throws JSONException 
     * @throws ParsingException on parsing errors
     */
    private Result parseResult(final JSONObject jsonResponse) throws IOException, JSONException {
        Result result = null;

		if(jsonResponse.has(ApiConstants.STATUS_TAG)) {
        final String status = jsonResponse.getString(ApiConstants.STATUS_TAG);
        if (status != null) {
           
            if (ApiConstants.STATUS_OK.equalsIgnoreCase(status) || ApiConstants.STATUS_OPTIONAL_UPDATE.equals(status)) {
                result = new Result(status, null, null);
            } else if (ApiConstants.STATUS_WARNING.equals(status) || ApiConstants.STATUS_ERROR.equals(status)) {
            	result = parseResultExt(jsonResponse, status);
            }
        }

			//Patrimonial Validation
		}else if(jsonResponse.has("status")){
			try{
				String statusST = jsonResponse.getJSONObject("status").toString();
				JSONObject statusJS = new JSONObject(statusST);
				if(statusJS.has("code")){
					String code= statusJS.getString("code");
					if(Integer.parseInt(code)<250){
						result = new Result(ApiConstants.STATUS_OK,null,null);
					}else
						result = parseResultExt(jsonResponse, ApiConstants.STATUS_ERROR);
				}
			}catch (JSONException e){
				Log.d("Erro Parcer Patrimonial",e.getMessage());
				e.printStackTrace();
			}

		}

        return result;
    }
    
    
	/**
	 * @param jsonResponse
	 * @param status
	 * @return
	 * @throws JSONException
	 */
	private Result parseResultExt(final JSONObject jsonResponse,
			final String status) throws JSONException {
		Result result;
		final String code = jsonResponse.getString(ApiConstants.CODE_TAG);
            
		final String messageAux = (code == null) ? null : jsonResponse.optString(ApiConstants.MESSAGE_TAG);
		final String message = (messageAux == null) ? jsonResponse.getString(ApiConstants.MESSAGE_INFORMATIVO_TAG) : messageAux;
         
            
		String updateURL = null;
		if (code != null && code.equals(ApiConstants.CODE_MBANK1111)) {
		    updateURL = jsonResponse.getString(ApiConstants.URL_TAG);
		    result = new Result(status, code, message, updateURL);
		} else {
		    result = new Result(status, code, message);
		}
		return result;
	}

}