package suitebancomer.aplicaciones.commservice.request;



/**
 * This class overrides the factory method to return an instance of a
 * ConcreteProduct.
 * IDS Comercial S.A. de C.V
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class RequestFactory implements IRequestFactory {

	IRequestBuilder requestBuilder;

	@Override
	public IRequestBuilder createRequestBuilder() {
		requestBuilder=new RequestBuilderImpl();
		return requestBuilder;
	}

}