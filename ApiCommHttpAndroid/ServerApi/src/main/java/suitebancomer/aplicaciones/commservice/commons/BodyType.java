package suitebancomer.aplicaciones.commservice.commons;

/**
 * Created by evaltierrah on 16/07/16.
 */
public enum BodyType {

    FORM_DATA(0),
    URL_ENCODED(1),
    RAW(2),
    BINARY(3);

    /**
     * Indica el tipo de body
     */
    int bodyType;

    /**
     * Asigna el tipo de body a enviar
     *
     * @param bodyType Tipo de body
     */
    BodyType(final int bodyType) {
        this.bodyType = bodyType;
    }

    /**
     * Regresa el tipo de body
     *
     * @return Regresa el tipo de body
     */
    public int getBodyType() {
        return bodyType;
    }

}
