package suitebancomer.aplicaciones.commservice.localcomm;

import java.io.UnsupportedEncodingException;


import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpResponse;


import android.util.Log;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;


/**
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 * 
 */
public class LocalResponseBuilder {

	public IResponseService createRequestSimulation(final RequestService aRequest) throws UnsupportedEncodingException {
		
		//se forma el objeto de respuesta
		final IResponseService responseService=new ResponseServiceImpl();
		final HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, ApiConstants.OK);
		StringEntity stringJson;	 
		try{
			
			stringJson=new StringEntity(CommContext.getSimulationPropertyValue(
					String.valueOf(aRequest.getParametersTO().getOperationId()) ), ApiConstants.UTF_8);		

			if (CommContext.allowLog){
				Log.i("APICom:createRequestSimulation URI: ", stringJson.toString() );}
			
		}catch(UnsupportedEncodingException ue){
			stringJson=new StringEntity(ApiConstants.EMPTY_JSON);
		}
		
		response.setEntity(stringJson);
		responseService.setResponse(response);
		return responseService;
		
	}

}