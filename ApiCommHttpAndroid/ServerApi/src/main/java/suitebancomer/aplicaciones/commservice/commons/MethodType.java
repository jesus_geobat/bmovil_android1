package suitebancomer.aplicaciones.commservice.commons;

/**
 * Created by evaltierrah on 16/07/16.
 */
public enum MethodType {

    POST(0),
    GET(1);

    /**
     * Indica el tipo de metodo en la peticion GET o POST
     */
    int methodType;

    /**
     * Asigna el tipo de metodo a invocar
     *
     * @param methodType Tipo de metodo
     */
    MethodType(final int methodType) {
        this.methodType = methodType;
    }

    /**
     * Regresa el tipo de metodo a invocar
     *
     * @return Regresa el tipo de metodo a invocar
     */
    public int getMethodType() {
        return methodType;
    }

}
