package suitebancomer.aplicaciones.commservice.workflow;

import suitebancomer.aplicaciones.commservice.commons.ParametersTO;


/**
 * This class defines an interface for handling requests and optionally implements
 * the successor link.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:21 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public abstract class AbstractInvokeHandler implements IInvokeHandle {

	/* next = (IHandler) NullHandler.getInstance(); */ 
	//private IInvokeHandle next;
	private IInvokeStrategy strategy = null;
	private ParametersTO parameters;
	
	public AbstractInvokeHandler(){
		//contructor
	}
	
	/**
	 * @return the strategy
	 */
	public final IInvokeStrategy getStrategy() {
		return strategy;
	}

	/**
	 * @param strategy the strategy to set
	 */
	public final void setStrategy(final IInvokeStrategy strategy) {
		this.strategy = strategy;
	}
	
	/**
	 * @return 
	 */
	@Override
	public ParametersTO getParametesTO() {
		return parameters;
	}

	/**
	 * @param parameters 
	 */
	@Override
	public void setParametesTO(final ParametersTO parameters) {
		this.parameters = parameters;
	}
	
	
}