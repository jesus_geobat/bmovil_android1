/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomer.aplicaciones.commservice.httpcomm;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.*;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.Map.Entry;

import suitebancomer.aplicaciones.commservice.commons.*;
import suitebancomer.aplicaciones.commservice.request.Mapper;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;


/**
 * TODO: COMENTARIO DE LA CLASE
 *
 * @author BBVA Bancomer, DyD.
 */

public class ClienteHttpInApi {
	
	private static ClienteHttpInApi instance=null;
    /**
     * The field separator in the response.
     */
    private static final String PARAMETER_SEPARATOR = "*";
    
    /**
     * Se utiliza un httpclient de Apache Jakarta
     */
   public DefaultHttpClient client;
    
    //protected Context mContext;
   
   public static ClienteHttpInApi getCurrent() {
	if(instance==null){
		instance=new ClienteHttpInApi();
	}
	return instance;
   }
   
    /**
     * Constructor.
     */
    public ClienteHttpInApi() {
    	createClient();
    }
    
    /**
     * Creates a new instance for the client.
     */
    private void createClient(){
    	if(client==null){
	    	client = new DefaultHttpClient();
	    	final HttpParams params = new BasicHttpParams();
	    	
	    	// Establece el timeout de la conexión y del socket a 30 segundos
	    	HttpConnectionParams.setConnectionTimeout(params, 30 * 1000);
	    	HttpConnectionParams.setSoTimeout(params, 30 * 1000);
	    	//client.setParams(params)
	    	
    	}
    }
    

    
	/**
	 * Sets the activity context for this delegate
	 * @param context the activity using the delegate
	 */
	//public void setContext(Context context){
	//	mContext = context;
	//}
    
    /**
     * Invoke a network operation.
     * @param operation operation code
     * @param parameters parameters
     * @param handler instance capable of parsing and storing the result
     * @throws IOException
     * @throws ParsingException 
     */
    
//*****************************************************************************************
    
    //Los dos métodos de abajo fueron comentados para "homologar" el proceso, ahora sólo entra
    //al tercer invokeOperation que desempeña la misma función que si los tres estuvieran presentes.
    
    
    
    //*****************************************************************************************
    

    
    /*public void invokeOperation(String operation, NameValuePair[] parameters,
    		ParsingHandler handler) throws IOException, ParsingException, URISyntaxException {
        invokeOperation(operation, parameters, handler, false, false);
    }*/
    
    /**
     * Invoke a network operation.
     * @param operation operation code
     * @param parameters parameters
     * @param handler instance capable of parsing the result
     * @param clearCookies true if session cookies must be deleted, false if not
     * @throws IOException
     * @throws ParsingException
     */
   /* public void invokeOperation(String operation, NameValuePair[] parameters, ParsingHandler handler, boolean clearCookies) 
    		throws IOException,	ParsingException, URISyntaxException {
        String url = getUrl(operation);
        String params = getParameterString(operation, parameters);
         Log.e("REQUEST", "params: " + params);
        char separator = ((url.indexOf('?') == -1) ? '?' : '&');
        url = new StringBuffer(url).append(separator).append(params).toString();
         Log.e("REQUEST", "url: " + url);
        retrieve(url, null, null, handler,false);

    }*/
    
    public String invokeOperation(final String url,final ParametersTO parametersTO,final  boolean clearCookies,final boolean isJSON)
    		throws IOException, URISyntaxException {
        //String url = getUrl(parametersTO);
        final NameValuePair[] parameters=convertHashToName((Hashtable<String, ?>) parametersTO.getParameters());
        String params = null;
        // System.out.println("*******Tipo de Operación: "+operation);
    	if (isJSON) {
			params = getParameterStringWithJSON(parametersTO.getOperationCode(), parameters);
			 Log.e("REQUEST", "params: " + params);
		}else {
			params = getParameterString(parametersTO.getOperationCode(), parameters);
			 Log.e("REQUEST", "params: " + params);
		}
    	
        //char separator = ((url.indexOf('?') == -1) ? '?' : '&');
        //url = new StringBuffer(url).append(separator).append(params).toString();
         Log.e("REQUEST", "url: " + url+"?"+params);
     // el método retrive es quien hace el trabajo post, antes se invocaba de la siguiente manera:
        //retrieve(url, null, null, handler,isJSON);
        //y los parametros se concatenaban con la url en las dos lineas comentadas en la linea 164 y 165.
        //retrieve(url, null, null, handler,isJSON);
        return retrieve(url,params);

    }
    
    private NameValuePair[] convertHashToName(final Map<String, ?> params){
    	NameValuePair[] parameters = new NameValuePair[params.size()];
		int i=0;
		for (final Entry<String, ?> entry : params.entrySet()) {
		    final String key = entry.getKey();
		    final Object value = entry.getValue();
			parameters[i] = new NameValuePair(key,(String) (value==null?"":value));
			i++;
		}
		return parameters;
    }
    
    
    /**
     * Retrieve the response from the ApiConstants and store it in the ParsingHandler.
     * @param url the URL to invoke
     * @param method the HTTP method
     * @param type the POST body content type
     * @param handler instance capable of parsing the result
     * @return 
     * @throws IOException
     * @throws ParsingException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private String retrieve( final String url,final String params) throws IOException, URISyntaxException {
    	
    	// Realiza la conexión http y manda la cadena de operación
    	BufferedReader in = null;
    	String result = null;
    	
    	try {
	        
	       // HttpGet request = new HttpGet();
	        final HttpPost request = new HttpPost();

			request.setURI(new URI(url) );
			// Si la siguiente línea de código esté comentada entonces la aplicación se comunica por la vía 1 .173 cuando esté en producci�n, 
			// Si la línea de código no esté comentada entonces se comunica por la vía 2 .174
			request.addHeader("Lan", "android");
			
			//New line
			
			final String data[] = params.split("&");
					
			final String firstData[]= data[0].split("=");
			final String secondData[]= data[1].split("=");
			final String thirdData[]= data[2].split("=");
			
			/*HttpParams postParams = new BasicHttpParams();
			
			postParams.setParameter(firstData[0], firstData[1]);
			postParams.setParameter(secondData[0], secondData[1]);
			postParams.setParameter(thirdData[0], thirdData[1]);
			request.setParams(postParams);
			*/
			//if(ApiConstants.ALLOW_LOG) Log.d("firstKey",firstData[0]);
			//if(ApiConstants.ALLOW_LOG) Log.d("firstValue",firstData[1]);
			
			//if(ApiConstants.ALLOW_LOG) Log.d("secondKey",secondData[0]);
			//if(ApiConstants.ALLOW_LOG) Log.d("secondValue",secondData[1]);
			
			//if(ApiConstants.ALLOW_LOG) Log.d("thirdKey",thirdData[0]);
			//if(ApiConstants.ALLOW_LOG) Log.d("thirdValue",thirdData[1]);
			
			final List postParams = new ArrayList();
			postParams.add(new BasicNameValuePair(firstData[0],firstData[1]));
			postParams.add(new BasicNameValuePair(secondData[0],secondData[1]));
			postParams.add(new BasicNameValuePair(thirdData[0],thirdData[1]));
			request.setEntity(new UrlEncodedFormEntity(postParams));

			
			HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, "OK");
			
			
			client.getConnectionManager().closeExpiredConnections();
			
			
				Utilities.getHeadersAsString(request);
			
			response = client.execute(request);
			
			
	        in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "ISO-8859-1"));
			final StringBuffer sb = new StringBuffer("");
			String line = "";
			final String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line).append(NL);
			}
			
			in.close();
			result = sb.toString(); //respuesta
			

			//TODO niko fix
//			Integer ind = result.indexOf("{");
//			result = result.substring(ind);

			Log.d("debugRespuesta", result);
			//result = result.replaceAll("\\<.*?>", "");
//			String res2 = result.substring(ind);
//			
			Log.d("debugRespuestaParseada", result.replaceAll("\\<.*?>", ""));
    		
		} catch (URISyntaxException e) {
            Log.e(this.getClass().getName(), "Ha ocurrido una excepción en la respuesta.", e);

        }catch (IOException e2){
            Log.e(this.getClass().getName(), "Ha ocurrido una excepción en la respuesta.", e2);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					 e.printStackTrace();
				}
			}
		}
    	return result;
 		
    }

    /**
     * Get the URL to invoke the ApiConstants.
     * @param operation operation code
     * @return a string with the response
     */
    public static String getUrl(final ParametersTO params) {
    	 final Mapper mapper=new Mapper();
    	 mapper.loadEnvironment(params.isDevelopment(), params.isEmulator());
 		mapper.setupOperation(params.getOperationId(), params.getProvider(), params.isDevelopment(), ServerCommons.ARQSERVICE);
 		final String urlPatternRequest = mapper.getURLPattern(params.getOperationId());
 		Log.d("URL: ", urlPatternRequest);
        return urlPatternRequest;
    }

    /**
     * Get the URL to invoke the ApiConstants.
     * @param operation operation code
     * @param parameters parameters to pass to the ApiConstants
     * @return a string with the response
     */
    public static String getParameterString(final String operation, final NameValuePair...  parameters) {

        final StringBuffer sb = new StringBuffer("OP").append(operation);
        if (parameters != null) {
            NameValuePair parameter;
            final int size = parameters.length;
            for (int i = 0; i < size; i++) {
                parameter = parameters[i];
                final String name = parameter.getName();
                final String value = parameter.getValue();
                sb.append(PARAMETER_SEPARATOR);
                // Log.e("Name",name);
                // Log.e("Value",value); 
                sb.append(name);
                sb.append(value);
            }
        }
        
        final String params = sb.toString();
        //params = encode(params);

        sb.setLength(0);

        sb.append(ApiConstants.OPERATION_CODE_PARAMETER).append('=');
        if(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_TDC].equals(operation)){
        	sb.append(ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO);
        	
        }else if(ApiConstants.OPERATION_CODES[ApiConstants.OP_SOLICITAR_ALERTAS].equals(operation)){
        	sb.append(ApiConstants.OPERATION_CODE_VALUE_RECORTADO);
        }else{
            sb.append(ApiConstants.OPERATION_CODE_VALUE);

        }

        sb.append('&').append(ApiConstants.OPERATION_LOCALE_PARAMETER).append('=').append(ApiConstants.OPERATION_LOCALE_VALUE).append('&').append(ApiConstants.PARAM_PAR_INICIO).append('=').append(params);

        return sb.toString();

    }
    
    public static String getParameterStringWithJSON(final String operation, final NameValuePair... parameters) {
    	
    	 Log.e("Entra","aqui con jsOn");
    	
    	//Creamos un objeto de tipo JSON
    	final JSONObject parametros = new JSONObject();

    	try {
			parametros.put("operacion",operation);
		} catch (JSONException e1) {
			 e1.printStackTrace();
		}
        if (parameters != null) {
            NameValuePair parameter;
            final int size = parameters.length;
            for (int i = 0; i < size; i++) {
                parameter = parameters[i];
		        try 
		        {
		        	//Se almacenan los parámetros en el objeto 
		        	parametros.put(parameter.getName(), parameter.getValue());
		        	// Log.e("Name",parameter.getName());
		        	// Log.e("Value",parameter.getValue());
		        }
		        catch (JSONException e) 
		        {
		        	 e.printStackTrace();
		        }
            }
        }
        
        final String params = parametros.toString().replaceAll("\\\\", "");
        //params = encode(params);

        final StringBuffer sb = new StringBuffer();

        sb.append(ApiConstants.OPERATION_CODE_PARAMETER).append('=');
        final String operacionResult=getParameterStringWithJSONAux1(operation);
        //si no encotro la operacion busca en el aux2
        if(ApiConstants.EMPTY_LINE.equals(operacionResult)){
            sb.append(getParameterStringWithJSONAux2(operation));
        }else{
            //si si encontro operacion ya no sigue buscando
            sb.append(operacionResult);
        }

            //Termina One click
        	//sb.append(ApiConstants.JSON_OPERATION_CODE_VALUE);

        sb.append('&').append(ApiConstants.OPERATION_LOCALE_PARAMETER).append('=').append(ApiConstants.OPERATION_LOCALE_VALUE).append('&').append(ApiConstants.PARAM_PAR_INICIO).append('=').append(params);

      //One Click
        //ApiConstants.isjsonvalueCode=isJsonValueCode.NONE;
        return sb.toString();

    }
    private static String getParameterStringWithJSONAux2(final String operation){
        String operacionresult=ApiConstants.EMPTY_LINE;
        if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_INTERBANCARIOS])){
            operacionresult=ApiConstants.JSON_OPERATION_CONSULTA_INTERBANCARIOS;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.FAVORITE_PAYMENT_OPERATION_BBVA])){
            operacionresult=ApiConstants.JSON_OPERATION_CONSULTA_INTERBANCARIOS;
        }else if(operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_TDC])){
            operacionresult=ApiConstants.JSON_OPERATION_MEJORAS_CODE_VALUE;
        } else if("ONECLICK".equals(ApiConstants.isjsonvalueCode.name())) {
            operacionresult=ApiConstants.JSON_OPERATION_CODE_VALUE_ONECLICK;
        } else if("CONSUMO".equals(ApiConstants.isjsonvalueCode.name())) {
            operacionresult=ApiConstants.JSON_OPERATION_CODE_VALUE_CONSUMO;
        } else if ("DEPOSITOS".equals(ApiConstants.isjsonvalueCode.name())  ) {
            operacionresult=ApiConstants.JSON_OPERATION_CODE_VALUE_DEPOSITOS;
        } else if ("PAPERLESS".equals(ApiConstants.isjsonvalueCode.name())  ) {
            operacionresult=ApiConstants.JSON_OPERATION_CODE_VALUE_PAPERLESS;
        } else{
            operacionresult=ApiConstants.JSON_OPERATION_CODE_VALUE;
        }
        return operacionresult;
    }

    private static String getParameterStringWithJSONAux1(final String operation){
        String operacionresult=ApiConstants.EMPTY_LINE;
        if(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_TDC].equals(operation)){
            operacionresult=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        }else if (ApiConstants.OPERATION_CODES[ApiConstants.OP_SINC_EXP_TOKEN].equals(operation)) {
            operacionresult=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        }else if (ApiConstants.OPERATION_CODES[ApiConstants.OP_RETIRO_SIN_TAR].equals(operation)) {
            operacionresult=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        }else if (ApiConstants.OPERATION_CODES[ApiConstants.CONSULTA_SIN_TARJETA].equals(operation)) {
            operacionresult=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        }else if (ApiConstants.OPERATION_CODES[ApiConstants.OP_RETIRO_SIN_TAR_12_DIGITOS].equals(operation)) {
            operacionresult=ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        }else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_SOLICITAR_ALERTAS])
                || operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CAMBIA_PERFIL])) {
            operacionresult=ApiConstants.JSON_OPERATION_RECORTADO_CODE_VALUE;
        }
        return operacionresult;
    }
    
    /**
     * URL encoding.
     * @param s the input string
     * @return the encodec string
     */
    /**
     * URL encoding.
     * @param s the input string
     * @return the encodec string
     */
    public static String encode(final String s) {
        String encoded = s;
        if (s != null) {
                final StringBuffer sb = new StringBuffer(s.length() * 3);
                char c;
                final int size = s.length();
                for (int i = 0; i < size; i++) {
                    c = s.charAt(i);
                    sb.append(auxEncode(c));
                }
                encoded = sb.toString();

        }
        return encoded;
    }

    private static String auxEncode(final char c){
        final StringBuffer result=new StringBuffer();
        if (c == '&') {
            result.append("%26");
        } else if (c == ' ') {
            result.append('+');
        } else if (c == '/'){
            result.append("%2F");
        } else if (auxEncodeDos(c)) {
            result.append(c);
        } else {
            result.append('%');
            if (c > 15) {
                result.append(Integer.toHexString((int)c));
            } else {
                result.append("0").append(Integer.toHexString((int) c));
            }
        }
        return result.toString();
    }

    private static Boolean auxEncodeDos(final char c){
        Boolean result=auxEncodeTres(c);
        if(result){
            return result;
        }else  if (c >= 'A' && c <= 'Z') {
            result=Boolean.TRUE;
        }else if(c >= 'a' && c <= 'z'){
            result=Boolean.TRUE;
        }
        return result;
    }

    private static Boolean auxEncodeTres(final char c){
        Boolean result=Boolean.FALSE;
        if(c >= ',' && c <= ';'){
            result=Boolean.TRUE;
        }else if( c == '_' || c == '?' || c == '*' ){
            result=Boolean.TRUE;
        }
        return result;
    }
    
  
    
    
    /**
     * Gets and creates an image from the ApiConstants, 
     * located in a specific URL
     * 
     * @param url the ApiConstants image
     * @throws IOException
     */
    public Bitmap getImageFromApiConstants(final String url) throws IOException {
    	
    	final HttpPost mHttppost = new HttpPost(url);
    	final HttpResponse mHttpResponse = client.execute(mHttppost);
    	Bitmap image = null;
    	
    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
    	  final HttpEntity entity = mHttpResponse.getEntity();
    	    if ( entity != null) {
    	    	final byte[] buffer = EntityUtils.toByteArray(entity);
    	    	 image = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
    	    }
    	}
    	
        return image;
    }

    /**
     * Closes all current connections
     */
    public  void abort(){
    	synchronized (client) {
    		client.getConnectionManager().shutdown();
        	createClient();
		}
    }
    
  
    
    
  

}
