package com.bancomer.mbanking.softtoken;

import android.content.Context;
import android.util.Log;

import net.otpmt.mtoken.MTokenCore;
import net.otpmt.mtoken.db.BasicFileProvider;
import net.otpmt.mtoken.db.DataProvider;
import net.otpmt.mtoken.db.FileDataProvider;
import net.otpmt.mtoken.db.FileProvider;
import net.otpmt.mtoken.db.ResourceOfflineDataProvider;
import net.otpmt.mtoken.net.SEHTTPConnection;
import net.otpmt.mtoken.util.Util;

import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenSession;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenVinTokenSDKPropertiesFile;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomer.aplicaciones.softtoken.classes.gui.utils.Base64Coder;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

//import com.vintegris.vinaccess.vintoken.sdk.VinTokenSDK;
//import com.vintegris.vinaccess.vintoken.sdk.VinTokenSDKFactory;
//import com.vintegris.vinaccess.vintoken.sdk.exception.InitializationException;
//import com.vintegris.vinaccess.vintoken.sdk.properties.AndroidVTProperties;
//import com.vintegris.vinaccess.vintoken.sdk.result.VTRC;

import com.vintegris.vinaccess.vintoken.sdk.VinTokenSDK;
import com.vintegris.vinaccess.vintoken.sdk.VinTokenSDKFactory;
import com.vintegris.vinaccess.vintoken.sdk.exception.InitializationException;
import com.vintegris.vinaccess.vintoken.sdk.properties.AndroidVTProperties;
import com.vintegris.vinaccess.vintoken.sdk.result.VTRC;

import java.util.Properties;

public class SofttokenApp extends BaseSubapplication {

	private MTokenCore core;

	private final String DATABASE_NAME = "mtokendb2";

	//VintokenSDK
	public AndroidVTProperties androidProperties;
	public Context softtokenContext = SuiteAppApi.appContext;
	//public Context softtokenContext = getSuiteApp();
	public VinTokenSDK vtcore  = null;
	private static long SLEEP_TIME = 2;
	public SofttokenApp(final SuiteAppApi suiteApp) {
		super(suiteApp);
		viewsController = new SofttokenViewsController();
		SofttokenSession.getCurrent().loadSottokenRecordStore();
	}
	
	public SofttokenViewsController getSottokenViewsController() {
		return (SofttokenViewsController)viewsController;
	}

	public synchronized MTokenCore getCore() throws IllegalStateException {

		if (core == null) {
			try {
				DataProvider dataProvider;

				// Sets the database name
				/*
				// This is the SQLite based data provider.
				// This version will not be used because there is a bug in the 
				// SQLite library that may lead to random data loss. This is valid
				// in Android 2.3 and earlier.
				dataProvider = new MTSQLiteDataProvider(
						MBankingApp.MBankingApp, 
						DATABASE_NAME,
						new ResourceOfflineDataProvider(MBankingApp.MBankingApp, R.raw.e));
				 */
				// This is the file based data provider,

				dataProvider = new FileDataProvider(new BasicFileProvider(suiteApp.appContext.getDir(DATABASE_NAME, Context.MODE_PRIVATE)), 
													new ResourceOfflineDataProvider(suiteApp.appContext, R.raw.e));

				/*ACTUALIZACIÓN A API 6.0.8*/
				// UPGRADE to 6.0.7 (6.0.8 edition)
				// Since the original code was:
				//    Options options = new Options();
				//    options.setPDFilter(Options.PDFILTER_WIFI);
				// I must keep those parameters on the upgrade to 6.0.7
				MTokenCore.Options options = new MTokenCore.Options();
				options.setPDFilter(MTokenCore.Options.PDFILTER_WIFI);
				if (MTokenCore.upgradeTo_6_0_7(dataProvider)) {
					if (Server.ALLOW_LOG) Log.i(getClass().getSimpleName(),"Upgraded to 6.0.7");
				}

				// From 6.0.8 onward, a few new filters must be added in order to mitigate problems
				// with PData on newer devices.
				options = new MTokenCore.Options();
				options.setPDFilter(MTokenCore.Options.PDFILTER_RECOMMENDED_201601);


				// Creates the MToken core
				core =  new MTokenCore(SuiteAppApi.appContext,Util.getParticularData(SuiteAppApi.appContext), dataProvider, options);

				if(ServerCommons.DEVELOPMENT && !ServerCommons.SIMULATION) {
					// Add the on-line configuration.
					core.setConnection(new SEHTTPConnection(SuiteAppApi.appContext.getString(R.string.downloadURL_test)));
				}
				else{
					// Add the on-line configuration.
					core.setConnection(new SEHTTPConnection(SuiteAppApi.appContext.getString(R.string.downloadURL_prod)));
				}
				// Add the off-line configuration.
				core.setOfflineConfiguration(SuiteAppApi.appContext.getString(R.string.offlineSeed));
				if(Server.ALLOW_LOG) Log.i(getClass().getSimpleName(), "Core created.");
			} catch (Exception e) {
				if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(), "Unable to initialize the core.", e);
				core = null;
				throw new IllegalStateException("Unable to initialize the core.", e);
			}
		}

		return core;

	}
	
	public void setCore(final MTokenCore core) {
		this.core = core;
	}

	public void resetApplicationData() throws Exception {
		// Dispose the core
		if(core != null) core.dispose();
		core = null;

		// Delete the existing database.
		//MBankingApp.MBankingApp.deleteDatabase(DATABASE_NAME);
		final FileProvider fileProvider = new BasicFileProvider(
				SuiteAppApi.appContext.getDir(DATABASE_NAME, Context.MODE_PRIVATE));
		fileProvider.reset();
	}

	public synchronized boolean isLogged() {
		if (core != null) {
			return core.isLogged();
		} else {
			return false;
		}
	}
	public void inicializaAndroidVTP() {
		try {

			if(ServerCommons.DEVELOPMENT||ServerCommons.SIMULATION) {
				Log.i("stoken","inicializaAndroidVTP:Development");
				androidProperties = new AndroidVTProperties(R.xml.initdev, softtokenContext);
			}
			else if(!ServerCommons.DEVELOPMENT && !ServerCommons.SIMULATION){
				Log.i("stoken","inicializaAndroidVTP:Production");
				androidProperties = new AndroidVTProperties(R.xml.initprod, softtokenContext);
			}

			Log.i("stoken","inicializaVTcore");
			vtcore = VinTokenSDKFactory.getInstance(androidProperties, softtokenContext);

		}
		catch (Exception e) {
			Log.w("stoken", "Ha ocurrido un error en el proceso de lectura del fichero de propiedades"+e.getMessage());
			return;
		}
		new InitSystem().execute(androidProperties, softtokenContext);
	}

	private class InitSystem extends AsyncTask<Object, Void, Boolean> {

		//ProgressDialog pd;
		SharedPreferences pref;
		int rc;

		protected void onPreExecute()
		{
			//pd = ProgressDialog.show(softtokenContext,null,softtokenContext.getString(R.string.menu_comprar_tiempo_aire),false);
			rc = 0;

		}

		@SuppressWarnings("unused")
		@Override
		protected Boolean doInBackground(Object... params) {
			try
			{
				Thread.sleep(SLEEP_TIME * 1000);
				AndroidVTProperties vtp = (AndroidVTProperties) params[0];

				//Se verifica el tiempo de incremento
				pref = softtokenContext.getSharedPreferences(SofttokenConstants.MEMORY_STORE, Context.MODE_PRIVATE);
				SharedPreferences.Editor edit = pref.edit();
				long timeStep;
				Properties properties = androidProperties.toProperties();

				if(properties.containsKey("com.vintegris.vinaccess.vintoken.cfg.oath.totp.TimeIncrement"))
					timeStep = Long.parseLong(properties.getProperty("com.vintegris.vinaccess.vintoken.cfg.oath.totp.TimeIncrement"));
				else
					timeStep = 30L;

				String base64timeStep = Base64Coder.encode(Long.toString(timeStep));
				edit.putString(SofttokenConstants.MEMORY_TOKEN_TOTP_INCREMENT,base64timeStep);

				if(edit.commit())
					Log.i("stoken", "Almacenado el tiempo de incremento TOTP");
				else
					Log.w("stoken", "No se pudo almacenar el tiempo de incremento TOTP");

				Log.d("stoken", "Iniciando VinToken SDK");

				VinTokenSDK vtcore = VinTokenSDKFactory.getInstance(vtp, params[1]);

			}
			catch (Exception e) {
				if(e instanceof InitializationException) {
					InitializationException ie = (InitializationException) e;
					rc = ie.getRc();
					Log.e("stoken rc", String.valueOf(rc));
				}
				Log.e("stoken","No se pudo iniciar el SDK. "+ e.toString());

				return false;
		}

			return true;
		}

		protected void onPostExecute(Boolean succed) {

			// pd.dismiss();
			//Intent i;
			Log.i("stoken","onPostExecute");
			if (!succed) {
				if(rc != VTRC.a)
					Log.e("stoken", "la aplicación no pudo iniciarse");
				// alertaKillApp(getResources().getString(R.string.msg_fallo_sdk));
			}
			else{
				Log.d("stoken", "VinToken SDK iniciado satisfactoriamente");
				//iniciaApp();
			}

		}
	}
}
