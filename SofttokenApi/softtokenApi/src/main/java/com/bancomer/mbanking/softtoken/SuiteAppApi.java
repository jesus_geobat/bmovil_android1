package com.bancomer.mbanking.softtoken;

import android.app.Activity;
import android.content.Context;

import com.bancomer.base.SuiteApp;
import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.base.callback.CallBackSession;

import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.classes.gui.controllers.token.SuiteViewsController;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

public class SuiteAppApi extends SuiteApp  {
	public static boolean isSubAppRunning;
	public static Context appContext;
	private static SuiteAppApi me;
	private SuiteViewsController suiteViewsController;

	private static Activity activityQrToken;
	private static CallBackBConnect intentToReturn;
	private static BaseViewControllerCommons intentToContratacion;

	private static boolean cambioPerfil = false;

	public static boolean isCambioPerfil() {
		return cambioPerfil;
	}

	public static void setCambioPerfil(boolean cambioPerfil) {
		SuiteAppApi.cambioPerfil = cambioPerfil;
	}


	public static Boolean getIsAplicationLogged() {
		return isAplicationLogged;
	}

	public static CallBackSession getCallBackSession() {
		return callBackSession;
	}

	public static void setCallBackSession(final CallBackSession callBackSession) {
		SuiteAppApi.callBackSession = callBackSession;
	}

	private static Boolean isAplicationLogged=false;
	private static CallBackSession callBackSession;

    public static void setIsAplicationLogged(Boolean isAplicationLogged) {
        SuiteAppApi.isAplicationLogged = isAplicationLogged;
    }

    public void initWithSession(final Boolean isAplicationLogged, final CallBackSession callBackSession, final String clientNumberOld, final String userNameOld, final String iumOld){
		this.isAplicationLogged=isAplicationLogged;
		this.callBackSession=callBackSession;
		this.clientNumberOld=clientNumberOld;
		this.userNameOld=userNameOld;
		this.iumOld=iumOld;
	}

	/**
	 * @return the intentToContratacion
	 */
	public static BaseViewControllerCommons getIntentToContratacion() {
		return intentToContratacion;
	}

	/**
	 * @param intentToContratacion the intentToContratacion to set
	 */
	public static void setIntentToContratacion(
			final BaseViewControllerCommons intentToContratacion) {
		SuiteAppApi.intentToContratacion = intentToContratacion;
	}

	public static CallBackBConnect getIntentToReturn() {
		return intentToReturn;
	}

	public static String getUserNameOld() {
		return userNameOld;
	}

	public static String getClientNumberOld() {
		return clientNumberOld;
	}

	public static String getIumOld() {
		return iumOld;
	}

	private  static String userNameOld;
	private static  String clientNumberOld;
	private static String iumOld;

	public static void setIntentToReturn(final CallBackBConnect intentToReturn) {
		SuiteAppApi.intentToReturn = intentToReturn;
	}
	
	public void onCreate(final Context context) {
		super.onCreate(context);
		suiteViewsController = new SuiteViewsController();
		isSubAppRunning = false;
		appContext = context;
		me = this;
		startBmovilAppApi();
		SofttokenConstants.PACKAGE_NAME = getApplicationContext().getPackageName();


	};

	public SuiteViewsController getSuiteViewsControllerApi() {
		return suiteViewsController;
	}

	public static SuiteAppApi getInstanceApi() {
		return me;
	}
	
	// #region BmovilApp
	private BmovilApp bmovilApplication;
	
	public BmovilApp getBmovilApplicationApi() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		return bmovilApplication;
	}
	
	public void startBmovilAppApi() {
		bmovilApplication = new BmovilApp(this);
		isSubAppRunning = true;
	}
	
	public void cierraAplicacionBmovilApi() {
		bmovilApplication.cierraAplicacion();
		bmovilApplication = null;
		isSubAppRunning = false;		
	}
	// #endregion
	
	// #region SofttokenApp
	private SofttokenApp softtokenApp;
	
	public SofttokenApp getSofttokenApplicationApi() {
		if(softtokenApp == null)
			startSofttokenAppApi();
		return softtokenApp;
	}
	
	public static boolean getSofttokenStatusApi() {
		return PropertiesManager.getCurrent().getSofttokenActivated();
	}
	
	public void startSofttokenAppApi() {
		softtokenApp = new SofttokenApp(this);
		isSubAppRunning = true;
	}
	
	public void closeSofttokenAppApi() {
		if(null == softtokenApp)
			return;
		softtokenApp.cierraAplicacion();
		softtokenApp = null;
		isSubAppRunning = false;
	}
	// #endregion
	
	public static int getResourceId(final String nombre, final String tipo){
		return appContext.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}
	
	public Context getApplicationContext(){
		return appContext;
	}

	public void showViewController(final Class<?> viewController, final int flags, final boolean inverted, final String[] extrasKeys, final Object[] extras, final Activity currentActivity) {
		suiteViewsController.setActivityContext(currentActivity);
		suiteViewsController.showViewController(viewController, flags, inverted, extrasKeys, extras);
	}

	public void showViewController(final Class<?> viewController, final int flags, final boolean inverted, final String[] extrasKeys, final Object[] extras) {
		if(SuiteAppApi.getInstanceApi() !=null && SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getViewsController().getCurrentViewControllerApp()!=null){
			suiteViewsController.setCurrentActivityApp(SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getViewsController().getCurrentViewControllerApp());
		}
		suiteViewsController.showViewController(viewController, flags, inverted, extrasKeys, extras);
	}


	public static Activity getActivityQrToken() {
		return activityQrToken;
	}

	public static void setActivityQrToken(final Activity activityQrToken) {
		SuiteAppApi.activityQrToken = activityQrToken;
	}

	}
