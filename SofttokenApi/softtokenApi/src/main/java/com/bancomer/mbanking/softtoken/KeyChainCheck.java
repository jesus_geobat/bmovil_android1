package com.bancomer.mbanking.softtoken;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.regex.Pattern;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenSession;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;

/**
 * Created by izma on 09/11/16.
 */
public class KeyChainCheck {
    private Context context=null;
    KeyStoreWrapper kswrapper;

    public KeyChainCheck(Context c){
        context=c;
    }

    public void verify(){
        suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST validacionEstatusST = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST();
        validacionEstatusST.validaEstatusSofttoken(context);
        kswrapper = KeyStoreWrapper.getInstance(context);
        String paqueteServicio=kswrapper.fetchValueForKey("paqueteServicio");
        String centro=kswrapper.fetchValueForKey(Constants.CENTRO);

        Log.d("KeyChainCheck","user: "+kswrapper.getUserName()
        +"\nseed: "+kswrapper.getSeed()
        +"\ncentro: "+centro
        +"\npaqueteServicio: "+paqueteServicio
        +"\nSTLocal: "+PropertiesManager.getCurrent().getSofttokenActivated()
        +"\nSTForaneo: "+PropertiesManager.getCurrent().getSofttokenService());

        if(!validaTelefono(kswrapper.getUserName())){
            if(!validaScheme(paqueteServicio)) {
                borrar();
            }
            return;
        }
        else{
            if(!validaSeed(kswrapper.getSeed())){
                if(!validaScheme(paqueteServicio)){
                    borrar();
                    return;
                }
            }
            else{
                if((validaPaquete(paqueteServicio) && validaCentro(centro)) ||
                        (!validaPaquete(paqueteServicio) && !validaCentro(centro))){
                    borrar();
                    return;
                }
                else {
                    if (validaCentro(centro)){
                        if (centro.equalsIgnoreCase(suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.BMOVIL)){
                            if(!PropertiesManager.getCurrent().getBmovilActivated()){
                                borrar();
                                return;
                            }
                        }
                    }
                    else{
                        if(paqueteServicio.equalsIgnoreCase(context.getPackageName()) ){
                            if(!PropertiesManager.getCurrent().getBmovilActivated() && !PropertiesManager.getCurrent().getSofttokenActivated()){
                                borrar();
                                borraToken();
                                return;
                            }
                        }
                        else{
                            if(isAppInstalled(paqueteServicio)){
                                if(!PropertiesManager.getCurrent().getSofttokenService()){
                                    borrar();
                                }
                            }
                            else{
                                borrar();
                            }
                        }
                    }
                }
            }
        }

    }

    private boolean validaScheme(String packageName) {
        if (packageName==null || packageName.equalsIgnoreCase("") || packageName.equalsIgnoreCase(" ") || packageName.equalsIgnoreCase("0")) {
            return false;
        }
        boolean installedApp=isAppInstalled(packageName);
        boolean STLocal=PropertiesManager.getCurrent().getSofttokenActivated();
        boolean STForaneo=PropertiesManager.getCurrent().getSofttokenService();
        return (installedApp && (STLocal || STForaneo));
    }

    public void borrar(){
        kswrapper.setUserName(" ");    //setEntry(Constants.USERNAME, " ");
        kswrapper.setSeed(" ");        //setEntry(Constants.SEED, " ");
        kswrapper.storeValueForKey(Constants.CENTRO, " ");  //setEntry(Constants.CENTRO, " ");
        kswrapper.storeValueForKey("paqueteServicio", " ");
        if (ServerCommons.ALLOW_LOG) {
            Log.i(this.getClass().getName(), "Eliminando datos de KeyChain (user & seed)...");
            Log.i("Key-> ", "UserName: " + kswrapper.getUserName());
            Log.i("Key->", "Seed: " + kswrapper.getSeed());
            Log.i("Key->", "Centro: " + kswrapper.fetchValueForKey(Constants.CENTRO));
            Log.i("Key->", "PaqueteServicio: " + kswrapper.fetchValueForKey("paqueteServicio"));
        }
    }





    public boolean borraToken() {
        try {

            final SofttokenSession session = SofttokenSession.getCurrent();
            final Session sessionApp = Session.getInstance(SuiteAppApi.appContext);
            // Intergracion KeyChainAPI
            final KeyStoreWrapper kswrapper = sessionApp.getKeyStoreWrapper();
            kswrapper.storeValueForKey(suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CENTRO, " ");
            //kswrapper.storeValueForKey("paqueteServicio"," ");
            // ---
            session.setSofttokenActivated(false);
            SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().resetApplicationData();
            final SofttokenActivationBackupManager manager = SofttokenActivationBackupManager.getCurrent();
            if(manager.existsABackup())
                manager.deleteBackup();
            return true;
        }
        catch(Exception e) {
            if(Server.ALLOW_LOG)
                Log.e("KeyChainCheck", "Error borraToken: " + e.toString());
            return false;
        }
    }


    private boolean isAppInstalled(String packageName){
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * Metodo para validar telefono
     * @param telefono string
     * @return true si el telefono tiene 10 digitos
     */
    public static boolean validaTelefono(String telefono){
        return ((telefono != null) && (Pattern.matches("[0-9]{10}", telefono)));
    }

    /**
     * Metodo para validar sedd
     * @param seed string
     * @return true si no esta vacio y no es espacio en blanco
     */
    public static boolean validaSeed(String seed){
        return ((seed != null) && (seed.length() != 0) && (!seed.equals(" ")) && (!seed.equals("0")));
    }


    public static boolean validaPaquete(String paquete){
        return ((paquete != null) && (paquete.length() != 0) && (!paquete.equals(" ")) && (!paquete.equals("0")));
    }

    public static boolean validaCentro(String centro){
        return ((centro != null) && (centro.length() != 0) && (!centro.equals(" ")) && (!centro.equals("0")));
    }

}
