/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;
import com.tutorialbbvasend.clases.TutorialBBVAView;
import java.util.ArrayList;
import java.util.Date;

import suitebancomer.aplicaciones.softtoken.classes.common.token.Common;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.UtilsSms;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class ActivacionSTViewController extends SofttokenBaseViewController {

	private final static String TAG = ActivacionSTViewController.class.getSimpleName();
	private String activationCode = "";

	private ContratacionSTDelegate contratacionDelegate;
	private EditText tbClave;
	private Button btnConfirmar;

	private Boolean esSolicitarNuevaClave = false;
	SharedPreferences preferences;

	private ImageButton help_clave_activacion;
	private TutorialBBVAView tutorial;
	private RelativeLayout rootLayout;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppApi.getResourceId("layout_softtoken_clave_activacionapi", "layout"));
		setTitle(R.string.softtoken_activacion_titulo, R.drawable.icono_st_activado);
		SofttokenViewsController parentManager = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		TrackingHelper.trackState("activacion clave", parentManager.estados);
		init();
		help_clave_activacion.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tutorial.initTutorial(
						new int[]{R.id.lblClave, R.id.tbClaveActivacion, R.id.lblInstrucciones}, R.id.tbClaveActivacion,
						"1234567890",
						"",
						R.id.header_layout, 0,
						false, 3, true);
			}
		});
	}

	@Override
	protected void onResume() {
		parentViewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		parentViewsController.setCurrentActivityApp(this);
		setDelegate((ContratacionSTDelegate) parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
		contratacionDelegate = (ContratacionSTDelegate) getDelegate();

		if (contratacionDelegate == null) {
			parentViewsController = SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getViewsController();
			parentViewsController.setCurrentActivityApp(this);
			setDelegate((ContratacionSTDelegate) parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
			contratacionDelegate = (ContratacionSTDelegate) getDelegate();
		}

		contratacionDelegate.setOwnerController(this);

		if (contratacionDelegate.isCargarDatos())
			contratacionDelegate.cargarDatosDelRespaldo();


		if (!contratacionDelegate.isEA12()) {
			muestraAvisoActivacionIvr(this);
			contratacionDelegate.setEA12(Boolean.TRUE);
		}

		this.tbClave.setEnabled(false);

		preferences = getSharedPreferences(bancomer.api.common.commons.Constants.SHARED_PREFERENCES, MODE_PRIVATE);
		String type = preferences.getString(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_TYPE, bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT);
		String activationCode = preferences.getString(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_CODE, bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT);
		String fechaHora = preferences.getString(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_TIME, bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT);
		String codeUsed = preferences.getString(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_CODE_USED, bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT);

		//si el tipo es ac y la clave de activacion existe
		if (Constants.FLAG_ST.equals(type)
				&& !bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT.equals(activationCode)
				&& !fechaHora.equals(Constants.FALSE_STRING)) {
			if (ServerCommons.ALLOW_LOG)
				Log.i(TAG, "onCreate: cp_flag = " + type + " activationCode = " + activationCode);

			//validar con el reutilizable enviando la clave de activiacon
			//ArrayList<String> valueInfoSMS = UtilsSms.fetchInbox(fechaHora, this, activationCode);

//			if (valueInfoSMS != null && valueInfoSMS.size() > 0) {
				if(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT.equals(codeUsed)){
					super.onResume();
					return;
				}
				tbClave.setText(activationCode);

				if (tbClave.getText() != null) {
					if (tbClave.length() >= 10) {
						SharedPreferences settings;
						SharedPreferences.Editor editor;


						settings = this.getSharedPreferences(bancomer.api.common.commons.Constants.SHARED_PREFERENCES, MODE_PRIVATE);
						editor = settings.edit();

						editor.putString(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_CODE_USED,bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT);
						editor.commit();

                        if (Common.getCommon().buscarVariableKeyChain(SofttokenConstants.TIPO_TOKEN).equals(SofttokenConstants.S2)) {
                            tbClave.setText(activationCode);
                        } else {
                            this.activationCode = EncriptaClave.desencripta(activationCode);
                            Log.i("Hassel:CLAVEACTIACION", "" + this.activationCode);
                            tbClave.setText(this.activationCode);
                        }
                    
					}
				}
			/*} else {//si tiene la url pero no es de un sms de bancomer
				showErrorMessage(getString(R.string.clave_no_encontrada), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
					}
				});
			}*/
		}

		super.onResume();
	}

	private void init() {
		findViews();
		scaleToCurrentScreen();

		// Pone el texto subrayado
		final TextView textView = (TextView) findViewById(SuiteAppApi.getResourceId("lblReenviarCodigo", "id"));
		final SpannableString content = new SpannableString(textView.getText());
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		textView.setText(content);
		rootLayout = (RelativeLayout) findViewById(R.id.master_layout);
		help_clave_activacion = (ImageButton) findViewById(R.id.ayuda_clave_activacion);
		tutorial = new TutorialBBVAView(this, rootLayout);
	}

	private void findViews() {
		tbClave = (EditText) findViewById(SuiteAppApi.getResourceId("tbClaveActivacion", "id"));
		btnConfirmar=(Button) findViewById(SuiteAppApi.getResourceId("btnConfirmar", "id"));

		tbClave.addTextChangedListener(
				new TextWatcher() {
					@Override
					public void onTextChanged(final CharSequence cs, final int arg1, final int arg2, final int arg3) {
						// Empty method
					}

					@Override
					public void beforeTextChanged(final CharSequence arg0, final int arg1, final int arg2, final int arg3) {
						// Empty method
					}

					@Override
					public void afterTextChanged(final Editable arg0) {
						btnConfirmar.setEnabled((tbClave.getText().toString().length() == 10));

					}
				}
		);

		btnConfirmar.setEnabled(false);
		if(ServerCommons.SIMULATION) {
			tbClave.setText("1111111111");
			btnConfirmar.setEnabled(true);
		}
	}

	private void scaleToCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scale(findViewById(SuiteAppApi.getResourceId("rootLayout", "id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblClave", "id")), true);
		guiTools.scale(tbClave, true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblInstrucciones", "id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblReenviarCodigo", "id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("btnConfirmar", "id")));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see suitebancomercoms.classes.gui.controllers.BaseViewController#
	 * processNetworkResponse(int,
	 * suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		contratacionDelegate.analyzeResponse(operationId, response);
	}

	/**
	 * Opcion solicitar codigo por llamada
	 *
	 * @param sender la opciOn seleccionada
	 */
	public void onBtnLlamarCodio(final View sender) {
		final Intent sIntent=new Intent(Intent.ACTION_CALL,Uri.parse(String.format("tel:%s", Uri.encode(this.getString(R.string.activacion_numero_solicita_codigo_st)))));
		sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(sIntent);
	}

	/**
	 * Se selecciona la opci�n Continuar.
	 *
	 * @param sender la opci�n seleccionada
	 */
	public void onBtnCorfirmarClick(final View sender) {
		if (activationCode.length() > 10){
			contratacionDelegate.activacionST(activationCode);
		}else{
			contratacionDelegate.activacionST(tbClave.getText().toString());
		}
		try {
			this.finalize();
		} catch (Throwable e) {
			if (ServerCommons.ALLOW_LOG)
				Log.e(TAG, "onBtnCorfirmarClick: "+e.getMessage(), e.getCause());
		}
	}

	public void onBtnCorfirmarClick() {
		contratacionDelegate.activacionST(tbClave.getText().toString());
		try {
			this.finalize();
		} catch (Throwable e) {
			if (ServerCommons.ALLOW_LOG)
				Log.e(TAG, "onBtnCorfirmarClick: "+e.getMessage(), e.getCause());
		}
	}


	public void onReenviarClaveClick(final View view) {
		this.esSolicitarNuevaClave = true;

		this.cleanSharedPreferences();

		contratacionDelegate.mostrarAlertaCambio();
	}


	@Override
	public void onPause() {
		Log.e("ENTRA A ", "ON PAUSE");
		super.onPause();
	}

	@Override
	public void onStop() {
		SuiteAppApi.setCambioPerfil(false);
		if (ServerCommons.ALLOW_LOG)
			Log.d("ENTRA A ","ON STOP");


		super.onStop();
	}

	@Override
	public void onDestroy() {
		if (ServerCommons.ALLOW_LOG)
			Log.d("ENTRA A ", "ON Destroy");
		super.onDestroy();
	}

	public void showCodigoIncorrecto() {
		final Dialog contactDialog = new Dialog(this);
        final LayoutInflater inflater = LayoutInflater.from(this);
        final View layout = inflater.inflate(R.layout.codigo_incorrecto_st,null);
		contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// make dialog itself transparent
		contactDialog.setCancelable(false);
		contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		contactDialog.setContentView(layout);
		final int width = (int)(getResources().getDisplayMetrics().widthPixels * 0.90);
		contactDialog.getWindow().setLayout(width, DrawerLayout.LayoutParams.WRAP_CONTENT);

		final Button intentarNuevamente = (Button) layout.findViewById(R.id.btnintentarnuevamente);
		final Button nuevoCodigo = (Button) layout.findViewById(R.id.btnnuevocodigo);

		intentarNuevamente.setHeight(nuevoCodigo.getHeight());

		intentarNuevamente.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						intentarNuevamente();
						contactDialog.dismiss();
					}
				}
		);
		nuevoCodigo.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						generarNuevoCodigo();
						contactDialog.dismiss();
					}
				}
		);
		contactDialog.show();
	}

	private void generarNuevoCodigo(){
		this.cleanSharedPreferences();
		contratacionDelegate.reenviarClaveActivacion();
	}

	private void intentarNuevamente(){
		tbClave.setText("");
		btnConfirmar.setEnabled(false);
	}

	private void muestraAvisoActivacionIvr(final Context apContex){
		final Dialog contactDialog = new Dialog(apContex);
		final LayoutInflater inflater = (LayoutInflater) apContex.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.aviso_ivr_st, null);
		if(contratacionDelegate.getSoftToken().getNumeroTarjeta().substring(0, 4).equals("0017")) {
			((ImageView) layout.findViewById(R.id.imglinea)).setBackgroundResource(R.drawable.an_im_godinbmovil_pm);
			TextView txtAvisdo=(TextView)layout.findViewById(R.id.avisoivr);
			txtAvisdo.setText("En breve recibirás un mensaje con el código de activación para continuar el registro de tu Token Móvil Empresarial.");

		}

		contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// make dialog itself transparent
		contactDialog.setCancelable(false);
		contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		contactDialog.setContentView(layout);

		final int width = (int)(getResources().getDisplayMetrics().widthPixels * 0.90);
		contactDialog.getWindow().setLayout(width, DrawerLayout.LayoutParams.WRAP_CONTENT);

		final Button aceptarIvr = (Button) layout.findViewById(R.id.btnAceptarivr);
		aceptarIvr.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						contactDialog.dismiss();
					}
				}
		);

		contactDialog.show();
	}

	public String getClaveActivacionFromSMSString(String sms) {
		String claveActivacion;
		try {
			if (!sms.contains("==")) {
				claveActivacion = sms.substring(sms.indexOf(":") + 1, sms.indexOf("."));
				if (claveActivacion.length() >= 10 && !sms.contains("==")) {
					return claveActivacion;
				}
			} else if (sms.contains("==")) {
				claveActivacion = sms.substring(sms.indexOf(":") + 1, sms.indexOf("="));
				if (claveActivacion.length() > 10 && sms.contains("==")) {
					return EncriptaClave.desencripta(claveActivacion + "==");
				}
			}
		}catch (Exception e){
			if (ServerCommons.ALLOW_LOG)
				Log.e(TAG, "getClaveActivacionFromSMSString: "+e.getMessage(), e.getCause());
		}
		return "ERROR";
	}

	private void cleanSharedPreferences() {
		SharedPreferences.Editor editor = preferences.edit();
		editor.clear();
		editor.commit();
	}
}
