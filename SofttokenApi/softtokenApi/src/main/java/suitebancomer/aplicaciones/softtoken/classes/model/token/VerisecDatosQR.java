package suitebancomer.aplicaciones.softtoken.classes.model.token;


import android.util.Log;

import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.softtoken.classes.common.token.Common;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;

/**
 * Created by ehmendezr on 12/28/16.
 */
public class VerisecDatosQR {
    public static String []keyValues = null;

    public static String[] obtenerCampos(byte []bst) {
        String []campos = null;
        byte []cadena;
        try {

            for(int i=0;i<bst.length;i++){
                System.out.println("i:"+i+"|"+"value:"+(int)bst[i]);
            }

            if (bst.length<4)
                return null;
            int length;
            System.out.println("Campos - " + (int)bst[3]);
            campos = new String[(int)bst[3]];
            keyValues = new String[(int)bst[3]];
            int i=3, j=0;
            for (int k=0; k<campos.length; k++) {
                campos[k] = "";
                i+=2;
                length = (int)bst[i];
                System.out.println("length " + length);
                j=0;
                while (j< length) {
                    System.out.println("i = " + i);
                    System.out.println("Length cadena " + (k+1) + " - " + (int)bst[i]);
                    keyValues[k] = Integer.toString((int)bst[i-1]);
                    cadena = new byte[(int)bst[i]];
                    i++;
                    for (; j<cadena.length; j++) {
                        cadena[j] = bst[j+i];
                    }
                    campos[k] += new String(cadena);
                    i+=cadena.length-1;
                    System.out.println("** " + i);
                }
            }
        } catch(Exception e) {
            System.out.println("CADENA INCORRECTA");
            System.out.println(e);
        }
        return campos;
    }

    public static String generarChallage(byte []dst){
        String[] values =obtenerCampos(dst);
        String challenge ="";
        for (int i = 0; i < values.length; i++) {
            challenge = challenge + values[i];
        }
        String newChallenge = challenge.replace(" ","").trim();
        Log.i("stoken", "old challenge: " + challenge);
        Log.i("stoken", "new challenge: " + newChallenge);


        for (int c_lenght = challenge.length(); c_lenght < SofttokenConstants.VERISEK_CHALLENGE_LENGTH; c_lenght++) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(newChallenge);
            stringBuilder.append("0");
            newChallenge = stringBuilder.toString();
        }


        return newChallenge;
    }



}
