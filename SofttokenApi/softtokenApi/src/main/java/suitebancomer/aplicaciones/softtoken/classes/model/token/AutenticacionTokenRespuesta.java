package suitebancomer.aplicaciones.softtoken.classes.model.token;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import bancomer.api.common.commons.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by elunag on 20/07/16.
 */

public class AutenticacionTokenRespuesta implements ParsingHandler {
    private String serialNumbre;
    private String status;
    private String code;
    private String tokenNumber;
    private String description;
    private String errorCode;

    public AutenticacionTokenRespuesta(String serialNumbre, String tokenNumber, String status, String code, String description, String errorCode) {
        this.serialNumbre = serialNumbre;
        this.tokenNumber = tokenNumber;
        this.status = status;
        this.code = code;
        this.description = description;
        this.errorCode = errorCode;
    }

    public AutenticacionTokenRespuesta() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerialNumbre() {
        return serialNumbre;
    }

    public void setSerialNumbre(String serialNumbre) {
        this.serialNumbre = serialNumbre;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTokenNumber() {
        return tokenNumber;
    }

    public void setTokenNumber(String tokenNumber) {
        this.tokenNumber = tokenNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public void process(final Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(final ParserJSON parser) throws IOException, ParsingException {

        try {

            JSONObject jStatus = parser.parserNextObject("status");
            JSONObject jResponse = parser.parserNextObject("response");

            code= parseValidString(jStatus,"code");
            description=parseValidString(jStatus,"description");
            serialNumbre=parseValidString(jResponse,"serialNumber");
            tokenNumber=parseValidString(jResponse,"tokenNumber");
        } catch (ParsingException e) {
            //e.printStackTrace();
            JSONObject eResponse = new JSONObject();


            try {
                eResponse.put("status",parser.parseNextValue("status"));
                eResponse.put("errorCode",parser.parseNextValue("errorCode"));
                eResponse.put("description",parser.parseNextValue("description"));
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            status = parseValidString(eResponse, "status");
            errorCode = parseValidString(eResponse, "errorCode");
            description = parseValidString(eResponse, "description");
        }


    }

    public String parseValidString(JSONObject jObj,String tag){
        return (!jObj.optString(tag).equalsIgnoreCase("") && !jObj.isNull(tag)
                && !jObj.optString(tag).equalsIgnoreCase("NO_VALUE"))
                ? jObj.optString(tag) : Constants.EMPTY_STRING;
    }

}

