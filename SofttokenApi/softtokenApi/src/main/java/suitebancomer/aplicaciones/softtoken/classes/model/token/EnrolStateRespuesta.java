package suitebancomer.aplicaciones.softtoken.classes.model.token;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by elunag on 20/07/16.
 */
public class EnrolStateRespuesta implements Serializable, ParsingHandler
{
    private String status;
    private String code;
    private String description;
    private String cardType;
    private String instrumentType;
    private String instrumentState;
    private String personType;
    private String updateDate;
    private String contractDate;
    private String contractIndicator;
    private String tokenSerial;
    private String enrollSwitch;
    private String useDevise;
    private String deviceStatus;
    private String digitalAccount;

    public EnrolStateRespuesta()
    {
    }

    public EnrolStateRespuesta(String status, String code, String description, String cardType, String instrumentType, String instrumentState, String personType, String updateDate, String contractDate, String contractIndicator, String tokenSerial, String enrollSwitch, String useDevise, String deviceStatus, String digitalAccount) {
        this.status = status;
        this.code = code;
        this.description = description;
        this.cardType = cardType;
        this.instrumentType = instrumentType;
        this.instrumentState = instrumentState;
        this.personType = personType;
        this.updateDate = updateDate;
        this.contractDate = contractDate;
        this.contractIndicator = contractIndicator;
        this.tokenSerial = tokenSerial;
        this.enrollSwitch = enrollSwitch;
        this.useDevise = useDevise;
        this.deviceStatus = deviceStatus;
        this.digitalAccount = digitalAccount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getInstrumentState() {
        return instrumentState;
    }

    public void setInstrumentState(String instrumentState) {
        this.instrumentState = instrumentState;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getContractDate() {
        return contractDate;
    }

    public void setContractDate(String contractDate) {
        this.contractDate = contractDate;
    }

    public String getContractIndicator() {
        return contractIndicator;
    }

    public void setContractIndicator(String contractIndicator) {
        this.contractIndicator = contractIndicator;
    }

    public String getTokenSerial() {
        return tokenSerial;
    }

    public void setTokenSerial(String tokenSerial) {
        this.tokenSerial = tokenSerial;
    }

    public String getEnrollSwitch() {
        return enrollSwitch;
    }

    public void setEnrollSwitch(String enrollSwitch) {
        this.enrollSwitch = enrollSwitch;
    }

    public String getUseDevise() {
        return useDevise;
    }

    public void setUseDevise(String useDevise) {
        this.useDevise = useDevise;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public String getDigitalAccount() {
        return digitalAccount;
    }

    public void setDigitalAccount(String digitalAccount) {
        this.digitalAccount = digitalAccount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException
    {
        try {
            JSONObject status = parser.parserNextObject("status");
            JSONObject response = parser.parserNextObject("response");

            code= status.getString("code");
            description =status.getString("description");


            setCardType(parseValidString(response, "cardType"));
            setInstrumentType(parseValidString(response, "instrumentType"));
            setInstrumentState(parseValidString(response, "instrumentState"));
            setPersonType(parseValidString(response, "personType"));
            setUpdateDate(parseValidString(response, "updateDate"));
            setContractDate(parseValidString(response, "contractDate"));
            setContractIndicator(parseValidString(response, "contractIndicator"));
            setTokenSerial(parseValidString(response, "tokenSerial"));
            setEnrollSwitch(parseValidString(response,"enrollSwitch"));
            setUseDevise(parseValidString(response, "useDevise"));
            setDeviceStatus(parseValidString(response, "deviceStatus"));
            setDigitalAccount(parseValidString(response,"digitalAccount"));

        } catch (JSONException e) {
            status= parser.parseNextValue("status");
            code= parser.parseNextValue("errorCode");
            description= parser.parseNextValue("description");
        }
    }

    public String parseValidString(JSONObject jObj,String tag){
        return (!jObj.optString(tag).equalsIgnoreCase("") && !jObj.isNull(tag)
                && !jObj.optString(tag).equalsIgnoreCase("NO_VALUE"))
                ? jObj.optString(tag) : "";
    }
}
