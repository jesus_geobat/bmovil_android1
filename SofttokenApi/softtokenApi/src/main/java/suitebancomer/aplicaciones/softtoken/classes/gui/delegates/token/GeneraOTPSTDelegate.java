/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.util.Log;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import net.otpmt.mtoken.MToken;
import net.otpmt.mtoken.MTokenCore;
import net.otpmt.mtoken.TransactionSigning;

import br.com.opencs.bincodec.ArrayAlphabet;
import br.com.opencs.bincodec.Base2NCodec;
import br.com.opencs.bincodec.random.RandomAlphabetGenerator;
import orgcoms.bouncycastle.util.encoders.Hex;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.common.token.Common;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenSession;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenVinTokenSDKPropertiesFile;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomer.aplicaciones.softtoken.classes.gui.utils.Base64Coder;
import suitebancomer.aplicaciones.softtoken.classes.model.token.SoftToken;
import suitebancomer.aplicaciones.softtoken.classes.model.token.DatosQROTP;
import suitebancomer.aplicaciones.softtoken.classes.model.token.VerisecDatosQR;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

import com.vintegris.vinaccess.vintoken.sdk.VinTokenSDK;
import com.vintegris.vinaccess.vintoken.sdk.VinTokenSDKFactory;
import com.vintegris.vinaccess.vintoken.sdk.exception.InitializationException;
import com.vintegris.vinaccess.vintoken.sdk.properties.AndroidVTProperties;

import com.vintegris.vinaccess.vintoken.sdk.result.VTOtp;
import com.vintegris.vinaccess.vintoken.sdk.result.VTToken;
import com.vintegris.vinaccess.vintoken.sdk.token.TokenType;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Properties;
public class GeneraOTPSTDelegate extends SofttokenBaseDelegate implements GetOtp{
	public static final long GENERAR_OTP_DELEGATE_ID = -7359658479361200492L;
	private static final String TAG = GeneraOTPSTDelegate.class.getSimpleName();
	private BaseViewControllerCommons ownerController;
	private ContratacionSTDelegate delegateContatacion;
	//VinTokenSDK
	public static AndroidVTProperties androidVTProperties;
	private static VTToken currentToken = null;
	private static VinTokenSDK vtcore = null;
	private static SoftToken softToken;
	private static Properties properties;
	private static String pin;
	private static Long otpTime = null;
	private static SharedPreferences pref;
	private static String codigoOTPService = "";
	Context cnt;

	public BaseViewControllerCommons getOwnerController() {
		return ownerController;
	}

	public void setOwnerController(final BaseViewControllerCommons ownerController) {
		this.ownerController = ownerController;
	}

	public GeneraOTPSTDelegate() {
		delegateContatacion = new ContratacionSTDelegate(this);
	}

	public GeneraOTPSTDelegate(final ContratacionSTDelegate contratacionSTDelegate) {
		delegateContatacion=contratacionSTDelegate;
	}

	public boolean borraToken() {
		try {

			final SofttokenSession session = SofttokenSession.getCurrent();
			final Session sessionApp = Session.getInstance(SuiteAppApi.appContext);
			// Intergracion KeyChainAPI
			final KeyStoreWrapper kswrapper = sessionApp.getKeyStoreWrapper();
			kswrapper.storeValueForKey(Constants.CENTRO, " ");
			// ---
			session.setSofttokenActivated(false);
			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().resetApplicationData();
			final SofttokenActivationBackupManager manager = SofttokenActivationBackupManager.getCurrent();
			if(manager.existsABackup())
				manager.deleteBackup();
			return true;
		}
		catch(Exception e) {
			if(Server.ALLOW_LOG)
				Log.e(TAG, "Error borraToken: " + e.toString());
			return false;
		}
	}

	public void inicializaCore() {
		if(!delegateContatacion.doStartApplication())
			SuiteAppApi.getInstanceApi().closeSofttokenAppApi();
	}

	public String generaOTPTiempo()
	{
		if(Common.getCommon().buscarVariableKeyChain(SofttokenConstants.TIPO_TOKEN).equalsIgnoreCase(SofttokenConstants.S2))
		{
		return generaOTPTIempoVerisek();
		}
		else
	{
		MToken token;
		final MTokenCore core = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getCore();
		boolean logged = core.isLogged();
		if(!logged)
			logged = delegateContatacion.doLogin();

		token = this.getLoadedToken();
		if (token != null) {
			token.calculateNextTimeOTP(true);
			if(Server.ALLOW_LOG) Log.i(TAG, token.getLastOTP());
		} else {
			this.doNoToken();
		}
		return token.getLastOTP();
		}

	}

	public String generaOTPChallenger(final String challenge)
	{String otpChallanger;
		if(Common.getCommon().buscarVariableKeyChain(SofttokenConstants.TIPO_TOKEN).equals(SofttokenConstants.S2)) {
			otpChallanger = generaOTPChanllengeVerisek(challenge);
			if(otpChallanger != null)
			{
				return otpChallanger;
			}else{
				return "";
			}
		}else {
			MToken token;
			token = this.getLoadedToken();
			if (token != null) {
				token.calculateNextChallengeResponseOTP(challenge);
				if (Server.ALLOW_LOG) Log.e(TAG, token.getLastOTP());
			} else {
				this.doNoToken();
			}
			return token.getLastOTP();
		}


	}

	protected MToken getLoadedToken(){
		if (SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().isLogged())
		{
			return SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getCore().getLoadedToken();
		} else {
			return null;
		}
	}

	protected void doNoToken(){
		try {
			ownerController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					ownerController.showInformationAlert(R.string.msg_no_token);
				}
			});
		}catch (NullPointerException e){
			if(Server.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Unable to proceed. Select the token and try again.: " );
		}
	}

	public void validaDatos(String cuenta,final String tipoToken) {
		android.util.Log.i("stoken", "Cuenta challenge: " + cuenta);
		if(cuenta.length() < SofttokenConstants.TOKEN_SEED_REGISTER_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_otp_challenge_error_registro_corto);
		} else if(!PropertiesManager.getCurrent().getSofttokenService()){
			((SofttokenViewsController)ownerController.getParentViewsController()).showPantallaMuestraOTPST(generaOTPChallenger(cuenta), SofttokenConstants.OTP_REGISTRO, "");
		}
		else{
			cnt = SuiteApp.appContext;
			GetOtpBmovil getOtpBmovil = new GetOtpBmovil(GeneraOTPSTDelegate.this, cnt);
			getOtpBmovil.generateOtpRegistro(cuenta);
		}
	}

	public static String generarOtpTiempo() {
		if(Server.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Se genero una otp por tiempo");
		String result = "";
		final ContratacionSTDelegate contDelegate = new ContratacionSTDelegate();
		contDelegate.generaTokendelegate.inicializaCore();
		result = contDelegate.generaTokendelegate.generaOTPTiempo();
		return (Tools.isEmptyOrNull(result)) ? null : result;
	}

	public static String generarOtpChallenge(final String challenge) {
		if(challenge.length() != 5) {
			if(Server.ALLOW_LOG) Log.e("GeneraOTPSTDelegate", "El numero de challenge '" + challenge + "' no tiene exactamente 5 digitos.");
			return null;
		}
		if(Server.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Se genero una otp challenge para: " + challenge);
		String result = "";
		final ContratacionSTDelegate contDelegate = new ContratacionSTDelegate();
		contDelegate.generaTokendelegate.inicializaCore();
		result = contDelegate.generaTokendelegate.generaOTPChallenger(challenge);
		return (Tools.isEmptyOrNull(result)) ? null : result;
	}

	public void borrarDatosConAlerta() {
		ownerController.showYesNoAlert(R.string.label_information,
				R.string.softtoken_menu_principal_aviso_borrar_datos,
				R.string.softtoken_menu_principal_ok_label_borrar_datos_alert,
				new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						if(borraToken())
							SuiteAppApi.getIntentToReturn().returnObjectFromApi(new String("BorraToken"));
						//SuiteAppApi.getInstanceApi().closeSofttokenAppApi();
					}
				});
	}


	public String generarOtpTiempoApi() {
		if(Server.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Se genero una otp por tiempo");
		String result = "";
		final ContratacionSTDelegate contDelegate = new ContratacionSTDelegate();
		contDelegate.generaTokendelegate.inicializaCore();
		result = contDelegate.generaTokendelegate.generaOTPTiempo();
		return (Tools.isEmptyOrNull(result)) ? null : result;
	}

	public String generarOtpChallengeApi(final String challenge) {
		if(challenge.length() != 5) {
			if(Server.ALLOW_LOG) Log.e("GeneraOTPSTDelegate", "El numero de challenge '" + challenge + "' no tiene exactamente 5 digitos.");
			return null;
		}
		if(Server.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Se genero una otp challenge para: " + challenge);
		String result = "";
		final ContratacionSTDelegate contDelegate = new ContratacionSTDelegate();
		contDelegate.generaTokendelegate.inicializaCore();
		result = contDelegate.generaTokendelegate.generaOTPChallenger(challenge);
		return (Tools.isEmptyOrNull(result)) ? null : result;
	}

	public String generaOTPFromQR(final String cadenaQR){
        String valueOtp ="";
        VerisecDatosQR verisecDatosQR =new VerisecDatosQR();
		String challengeVerisec="";

		try{
			byte dst[] = cadenaQR.getBytes();
			dst[3] = (byte) (dst[3] - 2);

            if(Common.getCommon().buscarVariableKeyChain(SofttokenConstants.TIPO_TOKEN).equals(SofttokenConstants.S2)){
				if(Server.SIMULATION == true) {
					valueOtp = "87654321";
				}
				else {
					challengeVerisec = verisecDatosQR.generarChallage(dst);
					Log.i("stokenErick","Generando OTP con challage:"+challengeVerisec);
					Log.i("Serial:",""+ Common.getCommon().buscarVariableKeyChain(SoftToken.TOKEN_SERIAL));
					valueOtp = generaOTPVerisekFromQR(challengeVerisec);
					Log.i("stokenErick","Generado:"+valueOtp);

                }

            }else {
			final TransactionSigning transactionSigning = new TransactionSigning(dst);
			if(Server.ALLOW_LOG){
				for (int i = 0; i < transactionSigning.getParamCount(); i++) {
					Log.i("QRCode", "VALUE " + transactionSigning.getParamType(i) + ": " + transactionSigning.getValue(i));
				}
			}
			final String sTrans = transactionSigning.getChallenge(); //es el challenge, no es la otp final


			if(Server.ALLOW_LOG) Log.w("QRCode","Challenge: "+ sTrans);

			valueOtp = generaOTPChallenger(sTrans);

			if(Server.ALLOW_LOG) Log.w("QRCode","ValueOTP: "+ valueOtp);
            }

		} catch (Exception e) {
			if(Server.ALLOW_LOG){
				e.printStackTrace();
				e.getLocalizedMessage();
				Log.e("token", "Error: "+e);
			}
		}
		return valueOtp;
	}


	public DatosQROTP obtieneDatosFromQR(final String cadenaQR){
		DatosQROTP datosQR = null;
        VerisecDatosQR verisecDatosQR =new VerisecDatosQR();
        try {
			//desofuscar
			if(Server.ALLOW_LOG) Log.w("QRCode", "cadena ofuscada"+ cadenaQR);
			final char decAlphabet[] = RandomAlphabetGenerator.generateRandom(SofttokenConstants.OPTICA_SEED, SofttokenConstants.OPTICA_ROUNDS, RandomAlphabetGenerator.QRCODE_ALPHANUMERIC_NO_SPACE, 32);
			final Base2NCodec decoder = new Base2NCodec(new ArrayAlphabet(decAlphabet));

			final byte dst[] = decoder.decode(cadenaQR);
			if (Common.getCommon().buscarVariableKeyChain(SofttokenConstants.TIPO_TOKEN).equals(SofttokenConstants.S1))
			 {
				Log.i("stoken QRCode", "Usando Gemalto");
				if (Server.ALLOW_LOG) {
				for(int i=0; i < dst.length;i++)
				{
					Log.i("QRCode", "cadena desofuscada bytes hex: "+Integer.toHexString(dst[i]));
				}
			}
			final TransactionSigning transactionSigning = new TransactionSigning(dst);

			datosQR = new DatosQROTP(new String(dst));

			int key;
			String value;
			for (int i = 0; i < transactionSigning.getParamCount(); i++) {
				key = transactionSigning.getParamType(i);
				value = transactionSigning.getValue(i);
				datosQR.put(i, String.valueOf(key), value);
			}

			/*if(Server.ALLOW_LOG){
				Log.i("QRCode", "VALUES: " + datosQR);
			}*/
			if(Server.ALLOW_LOG){
				for (int i = 0; i < transactionSigning.getParamCount(); i++) {
					Log.i("QRCode", "VALUE " + transactionSigning.getParamType(i) + ": " + transactionSigning.getValue(i));
				}
			}


			}
			else {
				Log.i("stoken QRCode","Usando Verisec");
                for(int i=0; i < dst.length;i++)
                {
                    Log.i("HASSEL", ""+(char)Integer.parseInt(String.valueOf(dst[i])));

                }

				String[] values = verisecDatosQR.obtenerCampos(dst);


                datosQR = new DatosQROTP(new String(dst));


                String value;
                for (int i = 0; i < values.length; i++) {
                    value = values[i];
					Log.i("Verisek:PUT:","i:"+i+"|key:"+ String.valueOf(verisecDatosQR.keyValues[i])+"|value:"+value);
					datosQR.put(i, String.valueOf(verisecDatosQR.keyValues[i]), value);
                }

            }
		} 
		catch (Exception e) {
			if(Server.ALLOW_LOG) {
				e.printStackTrace();
				e.getLocalizedMessage();
				Log.e("token", "Error: " + e);
			}
		}
		return datosQR;
	}

	/**
	 * Metodo sobreescrito para generar codigo OTP (Tiempo) mediante un servicio
	 * @param s
	 */
	@Override
	public void setOtpCodigo(String s) {

	}
	/**
	 * Metodo sobreescrito para generar codigo OTP (Registro) mediante un servicio
	 */
	@Override
	public void setOtpRegistro(final String otps) {
		Activity activity = (Activity) cnt;
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				((SofttokenViewsController)ownerController.getParentViewsController()).showPantallaMuestraOTPST(otps, SofttokenConstants.OTP_REGISTRO, "");
			}
		});

	}

	/**
	 * Metodo sobreescrito para generar codigo OTP (codigoQR) mediante un servicio
	 * @param otp
	 */
	@Override
	public void setOtpCodigoQR(String otp) {

	}
	public String[] obtenerCampos(byte []bst) {
		String []campos = null;
		byte []cadena = null;
		try {
			if (bst.length<4)
				return null;
			int length;
			System.out.println("Campos - " + (int)bst[3]);
			campos = new String[(int)bst[3]];
			int i=3, j=0;
			for (int k=0; k<campos.length; k++) {
				campos[k] = "";
				i+=2;
				length = (int)bst[i];
				System.out.println("length " + length);
				j=0;
				while (j< length) {
					System.out.println("i = " + i);
					System.out.println("Length cadena " + (k+1) + " - " + (int)bst[i]);
					cadena = new byte[(int)bst[i]];
					i++;
					for (; j<cadena.length; j++) {
						cadena[j] = bst[j+i];
					}
					campos[k] += new String(cadena);
					i+=cadena.length-1;
					System.out.println("** " + i);
				}
			}
		} catch(Exception e) {
			System.out.println("CADENA INCORRECTA");
			System.out.println(e);
		}
		return campos;
	}

	private static String generarCodigo(String challenge, boolean isQR) {

		VTOtp votp;
		String otp = "";

		char passwd[] = null;
		passwd = generaIUM().toCharArray();
		pin = String.valueOf(passwd);
		Log.w("stoken", "IUM OTP--> " + String.valueOf(pin));

		if (challenge != null) {
			if (isQR) {
				try {
						votp = vtcore.genOtp(currentToken.serial, pin, challenge, TokenType.CHROTP);
						android.util.Log.i("stoken", "Se genera OTP con challenge from QR " + votp);
				}
				catch (Exception e) {
					votp = null;
					android.util.Log.i("stoken", "Error creando challenge from QR");
					android.util.Log.i("stoken", "Exception: " + e.toString());
				}
			}
			else {
				try {
					votp = vtcore.genOtp(currentToken.serial, pin, challenge);
					android.util.Log.i("stoken", "Se genera OTP con challenge");
				}
				catch (Exception e) {
					votp = null;
					android.util.Log.i("stoken", "Error creando challenge");
					android.util.Log.i("stoken", "Exception: " + e.toString());
				}
			}
		}
		else {
			votp = vtcore.genOtp(currentToken.serial, pin, null);
			android.util.Log.i("stoken", "Se genera OTP de tiempo");
		}

		if (Server.SIMULATION) {
			otp = votp.getOtp();
			android.util.Log.i("stoken", "Token 'generado' en simulacion: " + otp);
		}
		else {
			if(votp.isOk()) {
				otp = votp.getOtp();
			}
			else {
				Log.e("stoken", "(generaCodigo) Error al generar el token");
			}
		}
		return otp;
	}

	private static String generaIUM() {
		KeyStoreWrapper kswrapper = KeyStoreWrapper.getInstance(SuiteAppApi.appContext);
        String telefono = kswrapper.getUserName();
		SofttokenSession softTokenSession = SofttokenSession.getCurrent();
		Log.e("stoken", "telefono:" + telefono);
		long seed = softTokenSession.getSeed();
		if (seed == 0) {
			seed = System.currentTimeMillis();
			softTokenSession.setSeed(seed);
		}

        if (softTokenSession.getUsername() != null|| !Constants.EMPTY_STRING.equals(softTokenSession.getUsername())) {
			Log.e("stoken","softTokenSession.getUsername NO es nulo");
			softTokenSession.saveSottokenRecordStore();
			return Tools.buildIUM(softTokenSession.getUsername(), softTokenSession.getSeed(), SuiteAppApi.appContext);
		}else{
            Log.e("stoken","softTokenSession.getUsername es nulo");
            if(softToken.getNumeroTelefono() != null||!Constants.EMPTY_STRING.equals(softToken.getNumeroTelefono())){
				Log.e("stoken", "softToken.getNumeroTelefono NO es nulo");
				softTokenSession.setUsername(softToken.getNumeroTelefono());
			}else {
				Log.e("stoken","softToken.getNumeroTelefono es nulo");
				softTokenSession.setUsername(telefono);
			}
			softTokenSession.saveSottokenRecordStore();
			return Tools.buildIUM(softTokenSession.getUsername(), softTokenSession.getSeed(), SuiteAppApi.appContext);

		}


	}

	public static String generaOTPTIempoVerisek() {
		android.util.Log.i("stoken", "Se va a generar OTP Verisek");
		String otp = "";
		if (inicializarVariablesVinToken()) {
			ArrayList<String> values = SofttokenVinTokenSDKPropertiesFile.readSerialValueFromFile(SuiteAppApi.appContext);
			String tokenSerial = values.get(SofttokenConstants.SERIAL_POSITION);
			pin = values.get(SofttokenConstants.PIN_POSITION);
			currentToken = vtcore.getToken(tokenSerial);

			if (Server.SIMULATION) {
				otp = refreshScreen();
			}
			else {
				if(currentToken.isOk())
					otp = refreshScreen();
				else {
					Log.e("stoken", "Codigo de error: " + String.valueOf(currentToken.getRc()));
					Log.e("stoken", "Descripcion error: " + currentToken.getRcDesc());
				}
			}

		}
		else {
			Log.e("stoken", "Error al inicializar Verisek sdk para generar OTP");
		}
		return otp;
	}

	public static String generaOTPChanllengeVerisek(String challenge) {
		String otpChallenge = "";
		if (inicializarVariablesVinToken()) {
			if (Server.SIMULATION) {
				otpChallenge = "12345678";
			}
			else {
				ArrayList<String> values = SofttokenVinTokenSDKPropertiesFile.readSerialValueFromFile(SuiteAppApi.appContext);
				String tokenSerial = values.get(SofttokenConstants.SERIAL_POSITION);
				pin = values.get(SofttokenConstants.PIN_POSITION);
				currentToken = vtcore.getToken(tokenSerial);
				if (currentToken.isOk()) {
                    challenge = challenge+SofttokenConstants.ZERO_CHALLAGE;
					android.util.Log.i("stoken", "vtcore listo para generar OTP Challenge");
					android.util.Log.i("stoken", "vtcore usara el challage:"+challenge);
					otpChallenge = generarCodigo(challenge, false);
				}
				else {
					android.util.Log.i("stoken", "vtcore aun no está listo para generar Challenge");
				}
			}
		}
		else {
			Log.e("stoken", "Error al inicializar Verisek sdk para generar OTP Challenge");
		}
		return otpChallenge;
	}

	public String generaOTPVerisekFromQR(String challenge) {
		String otpQR = "";
		if (inicializarVariablesVinToken()) {
			if (Server.SIMULATION) {
                Log.i("stoken"," is Simulation");
				otpQR = "12345678";
			}
			else {
				ArrayList<String> values = SofttokenVinTokenSDKPropertiesFile.readSerialValueFromFile(SuiteAppApi.appContext);
				String tokenSerial = values.get(SofttokenConstants.SERIAL_POSITION);
				pin = values.get(SofttokenConstants.PIN_POSITION);
				currentToken = vtcore.getToken(tokenSerial);
				if (currentToken.isOk()) {
					android.util.Log.i("stoken", "vtcore listo para generar OTP QR");
					otpQR = generarCodigo(challenge, true);
				}
				else {
					android.util.Log.i("stoken", "vtcore aun no está listo para generar via QR");
				}
			}
		}
		else {
			Log.e("stoken", "Error al inicializar Verisek sdk para generar OTP para QR");
		}
		return otpQR;
	}

	public static boolean inicializarVariablesVinToken() {
		android.util.Log.i("stoken", "inicializarVariablesVinToken");
		try {
			//androidVTProperties = new AndroidVTProperties(R.xml.init, SuiteAppApi.appContext;

			if(ServerCommons.DEVELOPMENT||ServerCommons.SIMULATION) {
				Log.i("stoken","inicializarVariablesVinToken:Development");
				androidVTProperties = new AndroidVTProperties(R.xml.initdev, SuiteAppApi.appContext);
			}
			else if(!ServerCommons.DEVELOPMENT && !ServerCommons.SIMULATION){
				Log.i("stoken","inicializarVariablesVinToken:Production");
				androidVTProperties = new AndroidVTProperties(R.xml.initprod, SuiteAppApi.appContext);
			}

			vtcore = VinTokenSDKFactory.getInstance(androidVTProperties, SuiteAppApi.appContext);
			pref = SuiteAppApi.appContext.getSharedPreferences(SofttokenConstants.MEMORY_STORE, Context.MODE_PRIVATE);
			properties = androidVTProperties.toProperties();

			String base64OtpTime = pref.getString(SofttokenConstants.MEMORY_TOKEN_TOTP_INCREMENT, Base64Coder.encode(Long.toString(Long.parseLong(properties.getProperty("com.vintegris.vinaccess.vintoken.cfg.oath.totp.TimeIncrement")))));
			String plainStr = Base64Coder.decodeString(base64OtpTime);
			otpTime = Long.valueOf(plainStr) * 1000;
			return true;
		}
		catch (InitializationException e1) {
			android.util.Log.i("stoken", "InitializationException on InicializarVariablesVinToken: " + e1.toString());
			e1.printStackTrace();
			return false;
		}
		catch (UnsupportedEncodingException e) {
			android.util.Log.i("stoken", "UnsupportedEncodingException on InicializarVariablesVinToken: " + e.toString());
			otpTime = Long.parseLong(properties.getProperty("com.vintegris.vinaccess.vintoken.cfg.oath.totp.TimeIncrement")) * 1000;
			Log.w("stoken","Unable to convert the otpIncrement from Base64 to Long value.");
			return false;
		}
		catch (IllegalArgumentException e) {
			android.util.Log.i("stoken", "IllegalArgumentException on InicializarVariablesVinToken: " + e.toString());
			otpTime = Long.parseLong(properties.getProperty("com.vintegris.vinaccess.vintoken.cfg.oath.totp.TimeIncrement")) * 1000;
			Log.w("stoken", "Unable to convert the otpIncrement from Base64 to Long value. Invalid or null Base64 OTP Time Increment.");
			return false;
		}
	}
//
	private static String refreshScreen() {
        String otp = "";
        android.util.Log.i("stoken", "refreshScreen:");
        otp = generarCodigo(null, false);
		return otp;
	}
}
