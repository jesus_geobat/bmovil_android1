/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.softtoken.classes.common.token.Common;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.views.token.ListaSeleccionViewController;
import suitebancomercoms.classes.common.PropertiesManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import net.otpmt.mtoken.TransactionSigning;
import suitebancomer.aplicaciones.softtoken.classes.model.token.DatosQROTP;
public class GeneraOTPSTViewController extends SofttokenBaseViewController {
	private GeneraOTPSTDelegate generaOtpDelegate;
	private ScrollView scrollView;
	private String cadenaQR;
	private ContratacionSTDelegate contratacionSTDelegate;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_softtoken_menu_principal_dos);
		setTitle(R.string.softtoken_titulo, R.drawable.icono_st_activado);			
		init();
	}
	
	@Override
	protected void onResume() {
		generaOtpDelegate.setOwnerController(this);
		((SofttokenViewsController)getParentViewsController()).setCurrentActivityApp(this);
		
		super.onResume();
	}
	
	private void init() {
		parentViewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		parentViewsController.setCurrentActivityApp(this);
		setDelegate((ContratacionSTDelegate) parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
		contratacionSTDelegate = (ContratacionSTDelegate)getDelegate();
		generaOtpDelegate = contratacionSTDelegate.generaTokendelegate;
		contratacionSTDelegate.setOwnerController(this);
		
		findViews();

		ViewGroup parent = (ViewGroup) scrollView.getParent();
		int index = parent.indexOfChild(scrollView);
		parent.removeView(scrollView);
		View view = getLayoutInflater().inflate(R.layout.layout_softtoken_menu_principal_dos, parent, false);
		parent.addView(view, index);

		scaleToCurrentScreen();
		final ImageButton btnRegistrar =(ImageButton)findViewById(R.id.btnRegistrar);
        final ImageButton btnGenerar =(ImageButton)findViewById(R.id.btnGenerar);
        final ImageButton btnreadQR =(ImageButton)findViewById(R.id.btnReadQRCode);
		final ImageView img = (ImageView) findViewById(R.id.imageView);
		int width = (int)(getResources().getDisplayMetrics().widthPixels * .85);
		img.getLayoutParams().width = width;
		if((int)(getResources().getDisplayMetrics().widthPixels) > 540) {
			setMargins(img, btnreadQR.getHeight());
		}else{
			setMargins(img, btnreadQR.getHeight()-20);
		}

		btnGenerar.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
				android.util.Log.i("stoken", "TIPO_TOKEN = " + Common.getCommon().buscarVariableKeyChain(SofttokenConstants.TIPO_TOKEN));
				if(!PropertiesManager.getCurrent().getSofttokenService()){
				final	String tokenGenerado = contratacionSTDelegate.generaTokendelegate.generaOTPTiempo();
					android.util.Log.i("stoken", "generandoToken: " + tokenGenerado);
					if (tokenGenerado != null)
						((SofttokenViewsController) getParentViewsController()).showPantallaMuestraOTPST(tokenGenerado, SofttokenConstants.OTP_TIEMPO,"");
				} else {
					((SofttokenViewsController) getParentViewsController()).showPantallaMuestraOTPST(SofttokenConstants.OTP_SERVICE, SofttokenConstants.OTP_TIEMPO, "");
				}
            }
        });
		btnRegistrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
            	((SofttokenViewsController)getParentViewsController()).showPantallaRegistraCuentaST();
            }
        });

		btnreadQR.setOnClickListener(new View.OnClickListener() {
			public void onClick(final View v) {
				try{
					((SofttokenViewsController)parentViewsController).showLecturaCodigoViewController(123456);

				} catch (NullPointerException e) {
					e.printStackTrace();
					Log.i("Error", "Error: " + e.getLocalizedMessage());
				} catch(RuntimeException e){
					e.printStackTrace();
					Log.i("Error", "Error: "+e.getLocalizedMessage());
				}
			}
		});

        final TextView textView = (TextView)findViewById(R.id.lblBorrarDatos);
        final SpannableString content = new SpannableString(textView.getText());
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		textView.setText(content);

		if (!PropertiesManager.getCurrent().getSofttokenService()&&Common.getCommon().buscarVariableKeyChain(SofttokenConstants.TIPO_TOKEN).equals(SofttokenConstants.S1)) {
			contratacionSTDelegate.generaTokendelegate.inicializaCore();
		}
	}
	
	private void findViews() {
		scrollView = (ScrollView)findViewById(R.id.body_layout);
	}

	private static void setMargins (View v, int altura) {
		if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
			ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
			p.setMargins(0, altura, 0, 0);
			v.requestLayout();
		}
	}
	
	private void scaleToCurrentScreen() {
        final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.rootLayout));
		guiTools.scale(findViewById(R.id.lblBorrarDatos), true);
		guiTools.scale(findViewById(R.id.btnContinuar));
	}

	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		return;
	}
	
	public void onBtnBorrarDatosClick(final View sender) {
		generaOtpDelegate.borrarDatosConAlerta();
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}    

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		if (requestCode == 123456) {
			if (resultCode == RESULT_OK) {
				cadenaQR = data.getExtras().getString("DATA");

                final DatosQROTP datosQR = contratacionSTDelegate.generaTokendelegate.obtieneDatosFromQR(cadenaQR);

				if (datosQR != null) {
					String tipoOperacion = datosQR.getValueWithKey(TransactionSigning.PARAM_REFERENCE);
					if (tipoOperacion != null) {
						((SofttokenViewsController) getParentViewsController()).showInputRegisterView(datosQR);
					} else {
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.softtoken_otp_alta_registro_failure),
								Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.softtoken_otp_alta_registro_failure),
							Toast.LENGTH_LONG).show();
				}

			}
		}
	}

}
