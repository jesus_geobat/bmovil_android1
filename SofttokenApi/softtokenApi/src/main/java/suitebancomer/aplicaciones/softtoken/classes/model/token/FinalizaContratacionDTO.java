package suitebancomer.aplicaciones.softtoken.classes.model.token;

import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import bancomer.api.common.commons.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by smontesdeoca on 22/07/16.
 */
public class FinalizaContratacionDTO implements ParsingHandler {

    /**
     * fecha servidor
     */
    private String date;
    /**
     * fecha servidor
     */
    private String time;
    /**
     * fecha servidor
     */
    private String status;
    /**
     * fecha servidor
     */
    private int code=0;
    /**
     * fecha servidor
     */
    private String description;

    /**
     *código de error
     */
    private String errorCode;


    private String getDataFromJson(final JSONObject jsonObject, final String data,
                                   final String opt) throws JSONException {
        if (!jsonObject.isNull(data)) {
            return jsonObject.getString(data);
        }
        return opt;
    }

    public String parseValidString(JSONObject jObj,String tag){
        return (!jObj.optString(tag).equalsIgnoreCase("") && !jObj.isNull(tag)
                && !jObj.optString(tag).equalsIgnoreCase("NO_VALUE"))
                ? jObj.optString(tag) : Constants.EMPTY_STRING;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser){
        try {
            parseResponse(parser);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParsingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void parseResponse(ParserJSON parser) throws ParsingException, IOException, JSONException {
        final JSONObject jsonObjectResponse = parser.parserNextObject("response");
        final JSONObject jsonObjectStat = parser.parserNextObject("status");


        if(jsonObjectResponse.length() > 0){
            setCode(200);
            setDate(parseValidString(jsonObjectResponse, "date"));
            setTime(parseValidString(jsonObjectResponse, "time"));
        } else {
            parseError(parser);
        }

    }

    public void parseError(ParserJSON parser) throws IOException, ParsingException {
        JSONObject eResponse = new JSONObject();

        try {
            eResponse.put("status",parser.parseNextValue("status"));
            eResponse.put("errorCode",parser.parseNextValue("errorCode"));
            eResponse.put("description",parser.parseNextValue("description"));
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        setCode(Integer.valueOf(parseValidString(eResponse, "status")));
        setErrorCode(parseValidString(eResponse, "errorCode"));
        setDescription(parseValidString(eResponse, "description"));
    }


}

