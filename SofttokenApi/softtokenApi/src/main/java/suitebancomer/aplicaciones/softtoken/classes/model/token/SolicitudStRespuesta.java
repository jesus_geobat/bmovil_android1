package suitebancomer.aplicaciones.softtoken.classes.model.token;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import bancomer.api.common.commons.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by smontesdeoca on 22/07/16.
 */
public class SolicitudStRespuesta implements ParsingHandler {


    private String code;
    private String description;
    private String status;
    private String errorCode;

    public SolicitudStRespuesta() {
    }

    public SolicitudStRespuesta(String code, String description, String status, String errorCode) {
        this.code = code;
        this.description = description;
        this.status = status;
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }
    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        try {

            JSONObject jStat = parser.parserNextObject("status");
            JSONObject jResponse = parser.parserNextObject("response");

            setCode(parseValidString(jStat, "code"));
            setDescription(parseValidString(jStat, "description"));
            setStatus(parseValidString(jResponse, "status"));

        } catch (ParsingException e){
            JSONObject eResponse = new JSONObject();

            try {
                eResponse.put("status",parser.parseNextValue("status"));
                eResponse.put("errorCode",parser.parseNextValue("errorCode"));
                eResponse.put("description",parser.parseNextValue("description"));
            } catch (JSONException e1) {
                e1.printStackTrace();
            }

            setStatus(parseValidString(eResponse,"status"));
            setStatus(parseValidString(eResponse,"errorCode"));
            setStatus(parseValidString(eResponse,"description"));
        }


    }

    public String parseValidString(JSONObject jObj,String tag){
        return (!jObj.optString(tag).equalsIgnoreCase("") && !jObj.isNull(tag)
                && !jObj.optString(tag).equalsIgnoreCase("NO_VALUE"))
                ? jObj.optString(tag) : Constants.EMPTY_STRING;
    }
}
