package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

/**
 * Created by Carlos Hernandez Santes on 8/23/16. IDS
 */

import android.os.Debug;
import android.util.Log;

import org.apache.commons.codec.binary.Base64;

import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

;import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public final class EncriptaClave {

    private static final String SECRET_KEY = "bancomer";
    private static final String ALGORITHM = "DESede";
    private static final String UTF_FORMAT = "utf-8";
    private static final String SECURITY_MD = "MD5";
    private static final String TAG = EncriptaClave.class.getSimpleName();


    public static String desencripta(String textoEncriptado) {
        String base64EncryptedString;
        try {
            byte[] message = Base64.decodeBase64(textoEncriptado.getBytes(UTF_FORMAT));
            MessageDigest md = MessageDigest.getInstance(SECURITY_MD);
            byte[] digestOfPassword = md.digest(SECRET_KEY.getBytes(UTF_FORMAT));
            byte[] keyBytes = new byte[0];
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD) {
                keyBytes = Arrays.copyOf(digestOfPassword, 24);
            } else {
                keyBytes = copyOf(digestOfPassword, 24);
            }
            SecretKey key = new SecretKeySpec(keyBytes, ALGORITHM);
            Cipher decipher = Cipher.getInstance(ALGORITHM);
            decipher.init(Cipher.DECRYPT_MODE, key);
            byte[] plainText = decipher.doFinal(message);
            base64EncryptedString = new String(plainText, UTF_FORMAT);
        } catch (Exception ex) {
            base64EncryptedString = "ERROR";
            if (ServerCommons.ALLOW_LOG)
                Log.e(TAG, "desencripta: "+ex.getMessage(),ex.getCause() );
        }
        return base64EncryptedString;
    }



    private static byte[] copyOf(byte[] from, int length) {
        //copia un arreglo dentro de otro arreglo de un tamaño en especifico, el tamaño en especifico debe ser igual
        //o mayor que el length del arreglo que se desea copiar, se rellenan con ceros los indices sobrantes
        byte[] result = new byte[length];
        for(int i=0 ; i<length; i++) {
            if(from.length > i ){
                result[i] = from[i];
                continue;
            }
            result[i] = 0;
        }
        return result;
    }

}
