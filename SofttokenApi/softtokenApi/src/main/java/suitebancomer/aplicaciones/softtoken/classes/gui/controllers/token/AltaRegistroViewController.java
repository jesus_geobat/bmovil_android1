package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import net.otpmt.mtoken.TransactionSigning;

import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomer.aplicaciones.softtoken.classes.model.token.DatosQROTP;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.common.PropertiesManager;

/**
 * Created by RPEREZ on 03/07/15.
 */
public class AltaRegistroViewController extends SofttokenBaseViewController implements View.OnClickListener {

    public TextView lblresult;
    public TextView lblresult2;
    public TextView lblRegisterUp;
    public TextView lblRegisterCenter;
    public TextView lblRegisterDown;
    private LinearLayout layoutAvisoAltaRegistro;
    public Button btnRegisterConfirm;
    public Button btnRegisterCancel;
    public String codQR="";
    public DatosQROTP datosQR;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_softtoken_alta_registro);
        setTitle(R.string.softtoken_titulo, R.drawable.icono_st_activado);
        relationUI();
        init();

    }

    private void init() {
        parentViewsController =  SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
        parentViewsController.setCurrentActivityApp(this);
        setDelegate(parentViewsController.getBaseDelegateForKey(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID));


        scaleToCurrentScreen();

        datosQR = (DatosQROTP) getIntent().getExtras().getSerializable(SofttokenConstants.QRCODE_DATA_KEY);
        codQR =  datosQR.getCodigoQR();
        String primerDato = "";
        String segundoDato = "";
        String tipoOperacion = "";
        if (datosQR != null) {
            tipoOperacion = datosQR.getValueWithKey(TransactionSigning.PARAM_REFERENCE);
            if (isTipoTransferencia(tipoOperacion)) {
                primerDato = datosQR.getValueWithKey(TransactionSigning.PARAM_ACCOUNT);
                segundoDato = datosQR.getValueWithKey(TransactionSigning.PARAM_TO_ACCOUNT);
            } else {
                primerDato = datosQR.getValueAtPosition(1);
                segundoDato = datosQR.getValueAtPosition(0);
            }
        } else if (Server.ALLOW_LOG){
            Log.i("QRCode", "No se han recibido los datos del QR");
        }

        if (primerDato != null && segundoDato != null && tipoOperacion != null) {
            if (isTipoTransferencia(tipoOperacion)) {
                lblresult.setText(Html.fromHtml("<u>" + primerDato + "</u>"));
                lblresult2.setText(segundoDato);
            }

            if (tipoOperacion.equals("01")) {
                //Terceros Bancomer View
                lblRegisterUp.setText(getResources().getString(R.string.softtoken_otp_alta_registro_cuenta));
                lblRegisterCenter.setText(getResources().getString(R.string.softtoken_otp_alta_registro_nombre));
                lblRegisterDown.setText(getResources().getString(R.string.softtoken_otp_alta_registro_transferencias));
            } else if (tipoOperacion.equals("02")) {
                //Terceros Interbancarios View
                lblRegisterUp.setText(getResources().getString(R.string.softtoken_otp_alta_registro_cuenta));
                lblRegisterCenter.setText(getResources().getString(R.string.softtoken_otp_alta_registro_banco));
                lblRegisterDown.setText(getResources().getString(R.string.softtoken_otp_alta_registro_transferencias));
            } else if (tipoOperacion.equals("03")) {
                //Servicios View
                lblRegisterUp.setText(getResources().getString(R.string.softtoken_otp_alta_registro_convenio));
                lblRegisterCenter.setText(getResources().getString(R.string.softtoken_otp_alta_registro_servicio));
                lblRegisterDown.setText(getResources().getString(R.string.softtoken_otp_alta_registro_pago_servicios));
            } else if (tipoOperacion.equals("04") || tipoOperacion.equals("05")) {
                //Dinero Movil y Tiempo Compra Aire View
                lblRegisterUp.setText(getResources().getString(R.string.softtoken_otp_alta_registro_celular));
                lblRegisterCenter.setText(getResources().getString(R.string.softtoken_otp_alta_registro_compania));
                lblRegisterDown.setText(getResources().getString(R.string.softtoken_otp_alta_registro_envios_comprasTA));
            } else if (tipoOperacion.equals("06")) {
                lblRegisterUp.setVisibility(View.INVISIBLE);
                lblresult.setText(primerDato);
                lblRegisterCenter.setText(getResources().getString(R.string.softtoken_otp_alta_registro_correo1));
                lblresult2.setText(segundoDato + "...");
                lblRegisterDown.setText(getResources().getString(R.string.softtoken_otp_alta_registro_correo2));
            } else {
                lblRegisterUp.setVisibility(View.INVISIBLE);
                lblresult.setText(primerDato);
                lblRegisterCenter.setText(getResources().getString(R.string.softtoken_otp_alta_registro_generico1));
                lblresult2.setText(segundoDato);
                lblRegisterDown.setText(getResources().getString(R.string.softtoken_otp_alta_registro_generico2));
                layoutAvisoAltaRegistro.setVisibility(View.VISIBLE);
            }

        } else {
            SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaGeneraOTPST(true);
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.softtoken_otp_alta_registro_failure),
                    Toast.LENGTH_LONG).show();
        }
    }

    public void relationUI(){
        lblresult = (TextView) findViewById(R.id.resultAltaRegistro1);
        lblresult2 = (TextView) findViewById(R.id.resultAltaRegistro2);
        lblRegisterUp = (TextView) findViewById(R.id.lblAltaRegistroUp);
        lblRegisterCenter = (TextView) findViewById(R.id.lblAltaRegistroCenter);
        lblRegisterDown = (TextView) findViewById(R.id.lblAltaRegistroDown);
        layoutAvisoAltaRegistro = (LinearLayout) findViewById(R.id.layout_aviso_alta_registro);
        btnRegisterConfirm = (Button) findViewById(R.id.btnAltaRegistroConfirmar);
        btnRegisterCancel = (Button) findViewById(R.id.btnAltaRegistroCancelar);

        btnRegisterConfirm.setOnClickListener(this);
        btnRegisterCancel.setOnClickListener(this);
    }

    private void scaleToCurrentScreen() {
        final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.lblAltaRegistro), true);
        guiTools.scale(findViewById(R.id.lblAltaRegistroUp), true);
        guiTools.scale(findViewById(R.id.resultAltaRegistro1), true);
        guiTools.scale(findViewById(R.id.lblAltaRegistroCenter), true);
        guiTools.scale(findViewById(R.id.resultAltaRegistro2), true);
        guiTools.scale(findViewById(R.id.lblAltaRegistroDown), true);
        guiTools.scale(findViewById(R.id.btnAltaRegistroConfirmar));
        guiTools.scale(findViewById(R.id.btnAltaRegistroCancelar));
    }

    private boolean isTipoTransferencia(String tipoOperacion) {
        boolean isTransferencia = false;

        if (tipoOperacion.equals("01")
                || tipoOperacion.equals("02")
                || tipoOperacion.equals("03")
                || tipoOperacion.equals("04")
                || tipoOperacion.equals("05")) {
            isTransferencia = true;
        }

        return isTransferencia;
    }

    @Override
    public void onClick(final View v) {
        if(v.equals(btnRegisterConfirm)) {
            final GeneraOTPSTDelegate otpGen = new GeneraOTPSTDelegate();
            // conflictos ismael
//<<<<<<< HEAD
            String otp = "";
            if (!PropertiesManager.getCurrent().getSofttokenService()){
                otp = otpGen.generaOTPFromQR(codQR);
                ((SofttokenViewsController) getParentViewsController()).showPantallaMuestraOTPST(otp, SofttokenConstants.OTP_TIEMPO, "");
/*=======
            final String otp = otpGen.generaOTPFromQR(datosQR.getCodigoQR());
            ((SofttokenViewsController) getParentViewsController()).showPantallaMuestraOTPST(otp, SofttokenConstants.OTP_TIEMPO);
>>>>>>> origin/BMOVDESAFUSI   */
            }
            else{
                ((SofttokenViewsController) getParentViewsController()).showPantallaMuestraOTPST(otp, SofttokenConstants.OTP_SERVICE_CODIGOQR, codQR);
            }
            //((SofttokenViewsController)getParentViewsController()).showPantallaMuestraOTPST(otp, SofttokenConstants.OTP_TIEMPO);
        }
        if(v.equals(btnRegisterCancel)) {
            SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaGeneraOTPST(true);

        }

    }



}
