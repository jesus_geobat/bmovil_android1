package suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token;

/**
 * Created by IDS_COMERCIAL on 20/07/2016.
 */
public interface GetOtp {
    void setOtpRegistro(String otp);
    void setOtpCodigo(String otp);
    void setOtpCodigoQR(String otp);
}