package suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;

import java.io.File;
import java.lang.reflect.Field;

import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by IDS_COMERCIAL on 20/07/2016.
 */
public class GetOtpBmovil {

    private final String TAG = GetOtpBmovil.class.getSimpleName();

    private static String tipoOtp = "";
    private static String cuentaRegistro = "";
    private static String cadenaQR = "";
    private static GetOtp otpInterfaz;
    private static Context context;
    private static KeyStoreWrapper kswrapper;
    private static String paqueteServicio = "";
    private static int numberTransaction = 0;

    /**
     * constructor
     *
     * @param otp
     * @param context
     */
    public GetOtpBmovil(GetOtp otp, Context context) {
        otpInterfaz = otp;
        this.context = context;
    }

    /**
     * serviceConnection
     */
    public static ServiceConnection sc = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            Parcel data = Parcel.obtain();
            Bundle b = new Bundle();
            b.putString(SofttokenConstants.TIPO_OPTCONSTANT, tipoOtp);
            b.putString(SofttokenConstants.CUENTA_DESTINOCONSTANT, cuentaRegistro);
            b.putString(SofttokenConstants.CADENA_QR, cadenaQR);
            data.writeBundle(b);

            try {
                service.transact(numberTransaction, data, Parcel.obtain(), 0);
            } catch (RemoteException e) {
                if (ServerCommons.ALLOW_LOG) {
                    Log.e("Error en la transaccion", e.getMessage());
                }
            }
            if (ServerCommons.ALLOW_LOG) {
                Log.v("xxx", "[MainActivity]: onServiceConnected()");
            }

            try {
                Field manager = KeyStoreWrapper.class.getDeclaredField(SofttokenConstants.KEY_STORE_MANAGER);
                manager.setAccessible(Boolean.TRUE);
                manager.set(null, null);
            } catch (NoSuchFieldException e) {
                if (ServerCommons.ALLOW_LOG) {
                    Log.e("TAG", "Error ", e);
                }
            } catch (IllegalAccessException e) {
                if (ServerCommons.ALLOW_LOG) {
                    Log.e("TAG", "Error ", e);
                }
            }
            KeyStoreWrapper kswrapper = KeyStoreWrapper.getInstance(context);
            String otp = kswrapper.fetchValueForKey(SofttokenConstants.OTP_CONSTANT);
            if (SofttokenConstants.TIPO_CODIGO.equals(tipoOtp)) {
                otpInterfaz.setOtpCodigo(otp);
            }
            if (SofttokenConstants.TIPO_REGISTRO.equals(tipoOtp)) {
                otpInterfaz.setOtpRegistro(otp);
            }
            if (SofttokenConstants.TIPO_CODIGOQR.equals(tipoOtp)){
                otpInterfaz.setOtpCodigoQR(otp);
            }
            try{
                context.unbindService(this);
            }
            catch (Exception e){
                if(ServerCommons.ALLOW_LOG)Log.d("APP","Servicio no conectado");
            }
        }

        /**
         * Metodo onServiceDisconnected
         * @param arg0 tipo ComponentName
         */
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        }
    };

    /**
     * pide la ejecucion del servicio para generar un otp de codigo
     */
    public void generateOtpCodigo() {
        tipoOtp = SofttokenConstants.TIPO_CODIGO;
        obtenerPaquete();
        if (!paqueteServicio.equals("")) {
            numberTransaction = SofttokenConstants.NUMBER_TRANSACTION;
            Intent i = new Intent();
            i.setComponent(new ComponentName(paqueteServicio, SofttokenConstants.ROOT_SERVICE));
            if ((context.bindService(i, sc, Context.BIND_AUTO_CREATE)) && (!paqueteServicio.equals(SofttokenConstants.PACKAGE_NAME))) {
                logger("Se conecto al servicio");
            } else {
                logger("No se conecto al servicio");
                context.unbindService(sc);
                otpInterfaz.setOtpCodigo(null);
            }
        }
    }

    /**
     * pide la ejecucion del sercicio para que genere una otp de
     * registro
     *
     * @param cuentaRegistro
     */
    public void generateOtpRegistro(String cuentaRegistro) {
        this.cuentaRegistro = cuentaRegistro;
        tipoOtp = SofttokenConstants.TIPO_REGISTRO;
        obtenerPaquete();
        if (!paqueteServicio.equals("")) {
            numberTransaction = SofttokenConstants.NUMBER_TRANSACTION;
            Intent i = new Intent();
            i.setComponent(new ComponentName(paqueteServicio, SofttokenConstants.ROOT_SERVICE));
            if ((context.bindService(i, sc, Context.BIND_AUTO_CREATE)) && (!paqueteServicio.equals(SofttokenConstants.PACKAGE_NAME))) {
                logger("Se conecto al servicio");
            } else {
                logger("No se conecto al servicio");
                context.unbindService(sc);
                otpInterfaz.setOtpRegistro(null);
            }
        }
    }

    /**
     * pide la ejecucion del servicio para que genere una otp con
     * cadenaQR
     *
     * @param cadenaQR
     */
public void generaOtpQR(String cadenaQR){
    this.cadenaQR = cadenaQR;
    tipoOtp = SofttokenConstants.TIPO_CODIGOQR;
    obtenerPaquete();
    if (!paqueteServicio.equals("")) {
        numberTransaction = SofttokenConstants.NUMBER_TRANSACTION;
        Intent i = new Intent();
        i.setComponent(new ComponentName(paqueteServicio, SofttokenConstants.ROOT_SERVICE));
        if ((context.bindService(i, sc, Context.BIND_AUTO_CREATE)) && (!paqueteServicio.equals(SofttokenConstants.PACKAGE_NAME))) {
            logger("Se conecto al servicio");
        } else {
            logger("No se conecto al servicio");
            context.unbindService(sc);
            otpInterfaz.setOtpCodigoQR(null);
        }
    }
}


    /**
     * Metodo que solamente valida si esta activo el servicio
     */
    public boolean validaServicio() {
        boolean activo = false;
        this.obtenerPaquete();
        if (!paqueteServicio.equals("")) {
            Intent i = new Intent();
            i.setComponent(new ComponentName(paqueteServicio, SofttokenConstants.ROOT_SERVICE));
          
		              if ((context.bindService(i, sc, Context.BIND_AUTO_CREATE)) && (!paqueteServicio.equals(SofttokenConstants.PACKAGE_NAME))) {
                logger("Se conecto al servicio");
                activo = true;
            } else {
                logger("No se conecto al servicio");
                context.unbindService(sc);
                activo = false;
            }
        }
        return activo;
    }

    /**
     * Busca la variable paqueteServicio en KeyChain
     *
     * @return String con el valor de la variable
     */
    public void obtenerPaquete() {
        final String pathFileKeyChain = Environment.getExternalStorageDirectory() + SofttokenConstants.ROOT_KEY_CHAIN;
        File fexiste = new File(pathFileKeyChain);
        if (fexiste.exists()) {
            try {
                kswrapper = KeyStoreWrapper.getInstance(context);
                paqueteServicio = kswrapper.fetchValueForKey(SofttokenConstants.PACKAGE_SERVICE);
            } catch (Exception e) {
                logger("Error " + e);
            }
        } else {
            logger("No se econtro el archivo en la ruta:\n " + pathFileKeyChain);
        }
    }

    /**
     * Valida ALLOW_LOG
     *
     * @param msg tipo String
     */
    public void logger(String msg) {
        if (ServerCommons.ALLOW_LOG) {
            Log.w(TAG, msg);
        }
    }
}
