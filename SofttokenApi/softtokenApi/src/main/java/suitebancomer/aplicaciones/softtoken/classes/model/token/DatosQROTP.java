package suitebancomer.aplicaciones.softtoken.classes.model.token;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import suitebancomer.aplicaciones.bmovil.classes.model.NameValuePair;

/**
 * Created by andres.vicentelinare on 02/09/2016.
 */
public class DatosQROTP implements Serializable {

    private String codigoQR;
    private List<NameValuePair> datos;

    public DatosQROTP(String codigoQR) {
        this.codigoQR = codigoQR;
        datos = new ArrayList<NameValuePair>();
    }

    public String getCodigoQR() {
        return codigoQR;
    }

    public void put(int position, String key, String value) {
        datos.add(position, new NameValuePair(key, value));
    }

    public String getValueAtPosition(int position) {
        return datos.get(position).getValue();
    }

    public String getValueWithKey(int key) {
        String value = "";
        for (NameValuePair nameValuePair : datos) {
            if (nameValuePair.getName().equals(String.valueOf(key))) {
                value = nameValuePair.getValue();
                break;
            }
        }
        return value;
    }
}
