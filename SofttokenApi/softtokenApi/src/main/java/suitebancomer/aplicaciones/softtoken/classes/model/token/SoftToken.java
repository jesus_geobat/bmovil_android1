package suitebancomer.aplicaciones.softtoken.classes.model.token;

/**
 * Recoge toda la informacion necesarias para las peticiones de SoftToken.
 *
 * @author CGI
 */
public class SoftToken {
	/**
	 * Se agregan las variables para el Servicio Alllowed del Token Corporativo
	 *
	 * @author ehmendez
	 */
	private String tokenCorporativo;
	private String isLdapUser;
	private String convivencia;
	private String singlesignon;
	private String biometric;
	private String edoCuenta;

	public String getEdoCuenta() {
		return edoCuenta;
	}

	public void setEdoCuenta(String edoCuenta) {
		this.edoCuenta = edoCuenta;
	}

	public String getBiometric() {
		return biometric;
	}

	public void setBiometric(String biometric) {
		this.biometric = biometric;
	}

	public String getSinglesignon() {
		return singlesignon;
	}

	public void setSinglesignon(String singlesignon) {
		this.singlesignon = singlesignon;
	}

	public String getConvivencia() {
		return convivencia;
	}

	public void setConvivencia(String convivencia) {
		this.convivencia = convivencia;
	}

	public String getIsLdapUser() {
		return isLdapUser;
	}

	public void setIsLdapUser(String isLdapUser) {
		this.isLdapUser = isLdapUser;
	}

	public String getTokenCorporativo() {
		return tokenCorporativo;
	}

	public void setTokenCorporativo(String tokenCorporativo) {
		this.tokenCorporativo = tokenCorporativo;
	}




	/** Dispositivo Fisico. */
	private String dispositivoFisico;
	/** EstatusDispositivo. */
	private String estatusDispositivo;

	/** El numero de tarjeta. */
	private String numeroTarjeta;

	/** El numero de telefono. */
	private String numeroTelefono;

	/** El numero de cliente. */
	private String numeroCliente;

	/** El tipo de solicitud. */
	private String tipoSolicitud;

	/** El nombre del token. */
	private String nombreToken;

	/** El numero de serie. */
	private String numeroSerie;

	/** El correo electronico. */
	private String correoElectronico;

	/** La compannia celular. */
	private String companniaCelular;

	/**TipoToken */
	private String tipoToken;
	
	/** La Version App. */
	private String versionApp;

	private String nombreCliente;

	/**  Coordenadas del TAS  */
	private String coordenadas;

	/**  Digito Verificador del TAS  */
	private String digitoVerificador;



	/**
	 * Constructor por defecto.
	 */
	public SoftToken() {
		super();
	}

	/**
	 * Constructor con parametros.
	 *
	 * @param numeroTarjeta
	 *            el numero de tarjeta
	 * @param numeroTelefono
	 *            el numero de telefono
	 * @param numeroCliente
	 *            el numero de cliente
	 * @param tipoSolicitud
	 *            el tipo de solicitud
	 * @param correoElectronico
	 *            el correo electronico
	 * @param companniaCelular
	 *            la compannia celular
	 * @param nombreCliente
	 *            el nombre del cliente
	 */
	public SoftToken(final String numeroTarjeta, final String numeroTelefono,
					 final String numeroCliente, final String tipoSolicitud,
					 final String correoElectronico, final String companniaCelular, final String nombreCliente) {
		super();
		this.numeroTarjeta = numeroTarjeta;
		this.numeroTelefono = numeroTelefono;
		this.numeroCliente = numeroCliente;
		this.tipoSolicitud = tipoSolicitud;
		this.correoElectronico = correoElectronico;
		this.companniaCelular = companniaCelular;
		this.nombreCliente = nombreCliente;
	}

	/**
	 * Constructor con parametros.
	 *
	 * @param numeroTarjeta
	 *            el numero de tarjeta
	 * @param numeroTelefono
	 *            el numero de telefono
	 * @param numeroCliente
	 *            el numero de cliente
	 * @param tipoSolicitud
	 *            el tipo de solicitud
	 * @param nombreToken
	 *            el nombre del token
	 * @param numeroSerie
	 *            el numero de serie
	 * @param correoElectronico
	 *            el correo electronico
	 * @param companniaCelular
	 *            la compannia celular
	 */
	public SoftToken(final String numeroTarjeta, final String numeroTelefono,
					 final String numeroCliente, final String tipoSolicitud, final String nombreToken,
					 final String numeroSerie, final String correoElectronico,
					 final String companniaCelular) {
		super();
		this.numeroTarjeta = numeroTarjeta;
		this.numeroTelefono = numeroTelefono;
		this.numeroCliente = numeroCliente;
		this.tipoSolicitud = tipoSolicitud;
		this.nombreToken = nombreToken;
		this.numeroSerie = numeroSerie;
		this.correoElectronico = correoElectronico;
		this.companniaCelular = companniaCelular;
	}

	/**
	 * Obtiene el numero de tarjeta.
	 *
	 * @return el numero de tarjeta
	 */
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	/**
	 * Establece el numero de tarjeta.
	 *
	 * @param numeroTarjeta
	 *            el numero de tarjeta a establecer
	 */
	public void setNumeroTarjeta(final String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getEstatusDispositivo() {
		return estatusDispositivo;
	}

	public String getDispositivoFisico() {
		return dispositivoFisico;
	}

	public void setDispositivoFisico(final String dispositivoFisico) {
		this.dispositivoFisico = dispositivoFisico;
	}

	public void setEstatusDispositivo(final String estatusDispositivo) {
		this.estatusDispositivo = estatusDispositivo;
	}

	/**
	 * Obtiene el numero de telefono.
	 *
	 * @return el numero de telefono
	 */
	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	/**
	 * Establece el numero de telefono.
	 *
	 * @param numeroTelefono
	 *            el numero de telefono a establecer
	 */
	public void setNumeroTelefono(final String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	/**
	 * Obtiene el numero de cliente.
	 *
	 * @return el numero de cliente
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * Establece el numero de cliente.
	 *
	 * @param numeroCliente
	 *            el numero de cliente a establecer
	 */
	public void setNumeroCliente(final String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	/**
	 * Obtiene el tipo de solictud.
	 *
	 * @return el tipo de solictud
	 */
	public String getTipoSolicitud() {
		return tipoSolicitud;
	}

	/**
	 * Establece el tipo de solictud.
	 *
	 * @param tipoSolicitud
	 *            el tipo de solictud a establecer
	 */
	public void setTipoSolicitud(final String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	/**
	 * Obtiene el nombre del token.
	 *
	 * @return el nombre del token
	 */
	public String getNombreToken() {
		return nombreToken;
	}

	/**
	 * Establece el nombre del token.
	 *
	 * @param nombreToken
	 *            el nombre del token a establecer
	 */
	public void setNombreToken(final String nombreToken) {
		this.nombreToken = nombreToken;
	}

	/**
	 * Obtiene el numero de serie.
	 *
	 * @return el numero de serie
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}

	/**
	 * Establece el numero de serie.
	 *
	 * @param numeroSerie
	 *            el numero de serie a establecer
	 */
	public void setNumeroSerie(final String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	/**
	 * Obtiene el correo electronico.
	 *
	 * @return el correo electronico
	 */
	public String getCorreoElectronico() {
		return correoElectronico;
	}

	/**
	 * Establece el correo electronico.
	 *
	 * @param correoElectronico
	 *            el correo electronico a establecer
	 */
	public void setCorreoElectronico(final String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	/**
	 * Obtiene la compannia celular.
	 *
	 * @return la compannia celular
	 */
	public String getCompanniaCelular() {
		return companniaCelular;
	}

	/**
	 * Establece la compannia celular.
	 *
	 * @param companniaCelular
	 *            la compannia celular a establecer
	 */
	public void setCompanniaCelular(final String companniaCelular) {
		this.companniaCelular = companniaCelular;
	}

	/**
	 * Obtiene el nombre del cliente.
	 *
	 * @return el nombre del cliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}

	/**
	 * Establece el nombre del cliente.
	 *
	 * @param nombreCliente
	 *            el nombre del cliente a establecer
	 */
	public void setNombreCliente(final String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getVersionApp() {
		return versionApp;
	}

	public void setVersionApp(final String versionApp) {
		this.versionApp = versionApp;
	}
	public String getTipoToken() {
		return tipoToken;
	}

	public void setTipoToken(String tipoToken) {
		this.tipoToken = tipoToken;
	}
	
	public String getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(final String coordenadasn) {
		this.coordenadas = coordenadasn;
	}

	public String getDigitoVerificador() {
		return digitoVerificador;
	}

	public void setDigitoVerificador(String digitoVerificador) {
		this.digitoVerificador = digitoVerificador;
	}


	//Inglés




	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	public static final String CELL_PHONE_NUMBER = "cellphoneNumber";

	public static final String CARD_NUMBER = "cardNumber";

	public static final String CARD_TYPE = "cardType";

	public static final String INSTRUMENT_TYPE = "instrumentType";

	public static final String INSTRUMENT_STATE = "instrumentState";

	public static final String PERSON_TYPE = "personType";

	public static final String UPDATE_DATE = "updateDate";

	public static final String CONTRACT_DATE = "contractDate";

	public static final String CONTRACT_INDICATOR = "contractIndicator";

	public static final String TOKEN_SERIAL = "tokenSerial";

	public static final String ENROLL_SWITCH= "enrollSwitch";

	public static final String USE_DEVISE= "useDevise";

	public static final String DEVICE_STATUS= "deviceStatus";

	public static final String DIGITAL_ACCOUNT= "digitalAccount";



	public static final String DESCRIPTION= "description";






	public static final String CODE_NIP = "nip";

	public static final String CODE_CVV2 = "cvv2";

	public static final String CODE_OTP = "otp";

	public static final String OTP_CUENTA_DIGITAL ="digitalAccountOtp";



	//Para server

	public static String TAG_RESPONSE = "response";



	private String cardType;

	public String getcardType() {
		return cardType;
	}

	public void setcardType(String cardType) {
		this.cardType = cardType;
	}



	private String instrumentType;
	public String getinstrumentType() {
		return instrumentType;
	}
	public void setinstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}


	private String instrumentState;
	public String getinstrumentState() {
		return instrumentState;
	}
	public void setinstrumentState(String instrumentState) {
		this.instrumentState = instrumentState;
	}



	private String personType;
	public String getpersonType() {
		return instrumentState;
	}
	public void setpersonType(String personType) {
		this.personType = personType;
	}



	private String updateDate;
	public String getupdateDate() {
		return updateDate;
	}
	public void setupdateDate(String updateDate) {
		this.updateDate = updateDate;
	}



	private String contractDate;
	public String getcontractDate() {
		return contractDate;
	}
	public void setcontractDate(String contractDate) {
		this.contractDate = contractDate;
	}


	private String contractIndicator;
	public String getcontractIndicator() {
		return contractIndicator;
	}
	public void setcontractIndicator(String contractIndicator) {
		this.contractIndicator = contractIndicator;
	}


	private String tokenSerial;
	public String gettokenSerial() {
		return tokenSerial;
	}
	public void settokenSerial(String tokenSerial) {
		this.tokenSerial = tokenSerial;
	}



	private String enrollSwitch;
	public String getenrollSwitch() {
		return enrollSwitch;
	}
	public void setenrollSwitch(String enrollSwitch) {
		this.enrollSwitch = enrollSwitch;
	}


	private String useDevise;
	public String getuseDevise() {
		return useDevise;
	}
	public void setuseDevise(String useDevise) {
		this.useDevise = useDevise;
	}


	private String deviceStatus;
	public String getdeviceStatus() {
		return deviceStatus;
	}
	public void setdeviceStatus(String deviceStatus) {
		this.deviceStatus = deviceStatus;
	}


	private String digitalAccount;
	public String getdigitalAccount() {
		return digitalAccount;
	}
	public void setdigitalAccount(String digitalAccount) {
		this.digitalAccount = digitalAccount;
	}






}