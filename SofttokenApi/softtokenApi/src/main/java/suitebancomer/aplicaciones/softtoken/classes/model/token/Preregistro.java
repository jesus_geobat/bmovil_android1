package suitebancomer.aplicaciones.softtoken.classes.model.token;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;


public class Preregistro implements ParsingHandler {

	private String numero_cliente = null;

	private String numero_celular = null;

	private String instrumento_seguridad = null;

	private String numero_clave_activacion=null;

	private String nip = null;

	private String OTP1 = null;

	private String OTP2 = null;

	private String indicador_solicitud = null;

	private String compania_telefonica = null;

	private String email=null;

	private String numero_tarjeta=null;

	private String numero_serie=null;

	private String nombre_token=null;

	 /**
	  * @return the numero_celular
	  */
	 public String getNumero_celular() {
		 return numero_celular;
	 }

	 /**
	  * @param numero_celular the numero_celular to set
	  */
	 public void setNumero_celular(final String numero_celular) {
		 this.numero_celular = numero_celular;
	 }

	 /**
	  * @return the numero_cliente
	  */
	 public String getNumero_cliente() {
		 return numero_cliente;
	 }

	 /**
	  * @param numero_cliente the numero_cliente to set
	  */
	 public void setNumero_cliente(final String numero_cliente) {
		 this.numero_cliente = numero_cliente;
	 }



	 /**
	  * @return the email
	  */
	 public String getEmail() {
		 return email;
	 }

	 /**
	  * @param email the email to set
	  */
	 public void setEmail(final String email) {
		 this.email = email;
	 }

	 /**
	  * @return the nip
	  */
	 public String getNip() {
		 return nip;
	 }

	 /**
	  * @param nip the nip to set
	  */
	 public void setNip(final String nip) {
		 this.nip = nip;
	 }
	 /**
	  * @return the numero_tarjeta
	  */
	 public String getNumero_tarjeta() {
		 return numero_tarjeta;
	 }

	 /**
	  * @param numero_tarjeta the numero_tarjeta to set
	  */
	 public void setNumero_tarjeta(final String numero_tarjeta) {
		 this.numero_tarjeta = numero_tarjeta;
	 }

	 /**
	  * Process data from parser
	  * @param parser the parser
	  * @throws IOException on communication errors
	  * @throws ParsingException on format errors
	  */
	 public void process(final Parser parser) throws IOException, ParsingException {
		 String entity = parser.parseNextEntity();
		 do {
			 if (entity.startsWith("TE")) {
				 this.numero_cliente = entity.substring(2);
			 } else if (entity.startsWith("IS")) {
				 this.instrumento_seguridad = entity.substring(2);
			 } else if (entity.startsWith("TS")) {
				 this.indicador_solicitud = entity.substring(2);
			 } else if (entity.startsWith("OA")) {
				 this.compania_telefonica = entity.substring(2);
			 } else if (entity.startsWith("EM")) {
				 this.email = entity.substring(2);
			 } else if (entity.startsWith("NA")) {
				 this.nombre_token = entity.substring(2);
			 } else if (entity.startsWith("SE")) {
				 this.numero_serie = entity.substring(2);
			 } else if (entity.startsWith("AC")) {
				 this.numero_clave_activacion = entity.substring(2);
			 }

			 entity = parser.parseNextEntity();
		 } while (entity != null);  
	 }


	 @Override
	 public void process(final ParserJSON parser) throws IOException, ParsingException {
		 // Empty method
	 }
}
