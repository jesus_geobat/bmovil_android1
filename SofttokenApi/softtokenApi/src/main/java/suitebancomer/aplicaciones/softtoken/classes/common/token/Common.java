package suitebancomer.aplicaciones.softtoken.classes.common.token;

import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;

public class Common {
    /**
     * Valida que la variable enviada se encuentre en KeyChain y borrarl
     * @param variable
     */
    public static Common oCommon;

    public static Common getCommon(){
        if (oCommon == null)
            oCommon = new Common();

        return oCommon;
    }

    public void eliminaValorVariable(String variable){
        Session session = Session.getInstance(SuiteAppApi.appContext);

        // Intergracion KeyChainAPI
        // Recoger KeyStoreManager
        KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();
        try{
            kswrapper.storeValueForKey(variable, " ");
            //----
        }catch(Exception e){
            if(Server.ALLOW_LOG) e.printStackTrace();
        }
    }

    public void addVariableKeyChain(String variable, String valor){
        Session session = Session.getInstance(SuiteAppApi.appContext);

        // Intergracion KeyChainAPI
        // Recoger KeyStoreManager
        KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();
        try{
            kswrapper.storeValueForKey(variable, valor);
            //----
        }catch(Exception e){
            if(Server.ALLOW_LOG) e.printStackTrace();
        }
    }

    public String buscarVariableKeyChain(String variable){
        String value = "";
        Session session = Session.getInstance(SuiteAppApi.appContext);
        KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();

        try{
            value = kswrapper.fetchValueForKey(variable);

        }catch (Exception ex){
            if(Server.ALLOW_LOG) ex.printStackTrace();
        }
        return value;
    }
}
