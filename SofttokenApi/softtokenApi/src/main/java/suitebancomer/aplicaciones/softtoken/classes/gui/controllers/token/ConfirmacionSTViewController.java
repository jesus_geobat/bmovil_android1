/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;


import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.token.BmovilViewsController;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class ConfirmacionSTViewController extends SofttokenBaseViewController{

	//AMZ
	private SofttokenViewsController parentManager;
	private BmovilViewsController bm;
	private TextView nip;
	private EditText tbNIP;
	private EditText tbCVV;
	private EditText tbNIPpas;
	private EditText tbCVVpas;
	private EditText tbOTP;
	private EditText digitoValor;
	private TextView coordenadas;
	private ContratacionSTDelegate contratacionDelegate;
	private TextView confirmacionInstruccionesASM;

	private static final String INSTRUMENTO_DP720 = "T3";
	private static final String INSTRUMENTO_OCRA = "T6";

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppApi.getResourceId("layout_softtoken_confirmacionapi", "layout"));
		setTitle(R.string.softtoken_activacion_titulo, R.drawable.icono_st_activado);

		parentViewsController = SuiteAppApi.getInstanceApi()
				.getSofttokenApplicationApi().getSottokenViewsController();
		parentViewsController.setCurrentActivityApp(this);
		contratacionDelegate = (ContratacionSTDelegate)parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == contratacionDelegate) {
			contratacionDelegate = new ContratacionSTDelegate();
		}
		//AMZ
		parentManager = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		bm = SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getBmovilViewsController();
		mostrarCVV();
		
		
		String aux = "";
		final int ultimo = parentManager.estados.size()-1;
		if(ultimo >= 0)
		{
			aux = parentManager.estados.get(ultimo);
		}
		
		if(aux != "activacion datos" )
		{

		}
		TrackingHelper.trackState("activacion nip", parentManager.estados);
		init();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		TrackingHelper.touchAtrasState(3);
	}

	private void mostrarCVV(){
		nip = (TextView) findViewById(SuiteAppApi.getResourceId("lblAdvertenciaNip", "id") );;
		nip.setVisibility(View.GONE);
	}

	@Override
	protected void onResume() {
		this.parentViewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		parentViewsController.setCurrentActivityApp(this);
		setDelegate((ContratacionSTDelegate) parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
		contratacionDelegate = (ContratacionSTDelegate)getDelegate();
		if (null == contratacionDelegate) {
			contratacionDelegate = new ContratacionSTDelegate();
		}
		contratacionDelegate.setOwnerController(this);

		if(contratacionDelegate.getSoftToken().getNumeroTarjeta().substring(0,4).equals("0017"))
		{

			String s= contratacionDelegate.getSoftToken().getCoordenadas();
			String p= contratacionDelegate.getSoftToken().getDigitoVerificador();
			String valorFinal = "Captura el valor de la tripleta que se encuentra en la coordenada "+ "<b>" + s + "</b>"+" y el dígito verificador en la posición "+ "<b>" + p + "</b>";
			coordenadas.setText(Html.fromHtml(valorFinal));
			findViewById(R.id.rootLayoutpas).setVisibility(View.VISIBLE);

		}
		else {
			findViewById(R.id.rootLayout).setVisibility(View.VISIBLE);
		}

		findViewById(R.id.campo_confirmacion_asm_layout).setVisibility(contratacionDelegate.esSustitucionDeToken() ? View.VISIBLE : View.GONE);

		super.onResume();
	}
	
	private void init() {
		findViews();
		// Modificación para incluir ayuda según el tipo de instrumento.
		if (contratacionDelegate.getSoftToken().getinstrumentType() != null) {
			if (contratacionDelegate.getSoftToken().getinstrumentType().equalsIgnoreCase(INSTRUMENTO_DP720)) {
				confirmacionInstruccionesASM.setText(getResources().getString(R.string.confirmation_ayudaCodigoDP270));
			} else if (contratacionDelegate.getSoftToken().getinstrumentType().equalsIgnoreCase(INSTRUMENTO_OCRA)) {
				confirmacionInstruccionesASM.setText(getResources().getString(R.string.confirmation_ayudaCodigoOCRA));
			}
		}

		scaleForCurrentScreen();
	}
	
	private void findViews() {
		tbNIP = (EditText)findViewById(R.id.tbNip);
		tbNIPpas = (EditText)findViewById(R.id.tbNippas);
		tbOTP = (EditText)findViewById(R.id.confirmacion_asm_edittext);
		//softtoken sinc cvv
		tbCVV = (EditText)findViewById(R.id.tbCvv);
		tbCVVpas = (EditText)findViewById(R.id.tbCvvpas);
		coordenadas = (TextView)findViewById(R.id.lblAdvertenciaNippas);
		digitoValor = (EditText)findViewById(R.id.tbDigito);

		// Modificación para incluir ayuda según el tipo de instrumento.
		confirmacionInstruccionesASM = TextView.class.cast(findViewById(R.id.confirmacion_asm_instrucciones_label));
	}
	
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(tbNIP, true);
		guiTools.scale(tbOTP, true);
		
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("rootLayout", "id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblNipTitulo","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblAdvertenciaNip","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("rootSecurityInstrumentsLayout","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("campo_confirmacion_asm_layout","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("confirmacion_asm_inner_layout","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("confirmacion_asm_label","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("confirmacion_asm_instrucciones_label","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("confirmacion_confirmar_button","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblAdvertenciaTiempo","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblAdvertenciaNipCvv","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("tbCvv","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblCvvTitulo","id")),true);

		guiTools.scale(findViewById(SuiteAppApi.getResourceId("rootLayoutpas", "id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblAdvertenciaNippas","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblAdvertenciaNipCvvpas","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("tbCvvpas","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("tbDigito","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("tbNippas","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblNipTitulopas","id")),true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblCvvTitulopas","id")),true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomercoms.classes.gui.controllers.BaseViewController#
	 * processNetworkResponse(int,
	 * suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		contratacionDelegate.analyzeResponse(operationId, response);
	}
	
	/**
	 * Se selecciona la opci�n Continuar.
	 * 
	 * @param sender
	 *            la opci�n seleccionada
	 */
	public void onBtnContinuarClick(final View sender) {

		//softtoken sinc cvv
		if (contratacionDelegate.getSoftToken().getNumeroTarjeta().substring(0, 4).equals("0017")) {

			contratacionDelegate.autenticacionSTPM(tbNIPpas.getText().toString(), tbCVVpas.getText().toString(), tbOTP.getText().toString(), digitoValor.getText().toString(),contratacionDelegate.esSustitucionDeToken());

		} else {
			contratacionDelegate.servicioAllowed(contratacionDelegate.softToken.getNumeroTelefono(), tbNIP.getText().toString(), tbCVV.getText().toString(), tbOTP.getText().toString(), contratacionDelegate.esSustitucionDeToken());
		}
	}
}
