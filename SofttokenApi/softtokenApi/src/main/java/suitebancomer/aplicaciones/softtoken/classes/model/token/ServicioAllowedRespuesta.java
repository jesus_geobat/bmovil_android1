package suitebancomer.aplicaciones.softtoken.classes.model.token;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ServicioAllowedRespuesta implements ParsingHandler {

	private String isLdapUser;
	private String convivencia;
	private String singlesignon;
	private String biometric;
	private String edoCuenta;
	private String tokenCorporativo;

	public String getIsLdapUser() {
		return isLdapUser;
	}

	public void setIsLdapUser(String isLdapUser) {
		this.isLdapUser = isLdapUser;
	}

	public String getConvivencia() {
		return convivencia;
	}

	public void setConvivencia(String convivencia) {
		this.convivencia = convivencia;
	}

	public String getSinglesignon() {
		return singlesignon;
	}

	public void setSinglesignon(String singlesignon) {
		this.singlesignon = singlesignon;
	}

	public String getBiometric() {
		return biometric;
	}

	public void setBiometric(String biometric) {
		this.biometric = biometric;
	}

	public String getEdoCuenta() {
		return edoCuenta;
	}

	public void setEdoCuenta(String edoCuenta) {
		this.edoCuenta = edoCuenta;
	}

	public String getTokenCorporativo() {
		return tokenCorporativo;
	}

	public void setTokenCorporativo(String tokenCorporativo) {
		this.tokenCorporativo = tokenCorporativo;
	}

	public ServicioAllowedRespuesta() {
		 isLdapUser = "";
		convivencia = "";
		singlesignon = "";
	     biometric = "";
		edoCuenta = "";
		tokenCorporativo = "";
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		isLdapUser = parser.parseNextValue("isLdapUser", false);
		convivencia = parser.parseNextValue("convivencia");
		singlesignon= parser.parseNextValue("singlesignon");
		biometric = parser.parseNextValue("biometric");
		edoCuenta = parser.parseNextValue("edoCuenta");
		tokenCorporativo = parser.parseNextValue("tokenCorporativo");


	}

}
