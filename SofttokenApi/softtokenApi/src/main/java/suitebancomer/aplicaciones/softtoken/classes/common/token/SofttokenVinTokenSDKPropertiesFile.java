package suitebancomer.aplicaciones.softtoken.classes.common.token;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class SofttokenVinTokenSDKPropertiesFile {
	
	
	//This method receives just a string which refers to Serial value gotten by enroll process
	
	public static void writeSerialValueIntoFile(String serialValue, String pinValue, Context context)
	{
		
		try {
			
			Properties properties = new Properties();
			properties.setProperty(SofttokenConstants.SERIAL, serialValue);
			properties.setProperty(SofttokenConstants.PIN, pinValue);

			String propertiesPath = context.getFilesDir().getPath().toString() + SofttokenConstants.PROPERTIES_FILE_NAME;

			
			FileOutputStream fileSaver = new FileOutputStream(propertiesPath );
			properties.store(fileSaver, "Serial Value and Pin Stored");
			
			fileSaver.close();
		}
		 catch (IOException e) {
			Log.e("token","Saving File Problem Caused By:"+e.getMessage());
		}
		
	}
	
	
	public static boolean doesFileExist(Context context)
	{
		
		String propertiesPath = context.getFilesDir().getPath().toString() + SofttokenConstants.PROPERTIES_FILE_NAME;
		File file = new File(propertiesPath);
		if(file.exists())
			return true;
		return false;
	}
	
	
	public static ArrayList<String> readSerialValueFromFile(Context context)
	{
		
		
		try {
			String propertiesPath = context.getFilesDir().getPath().toString() + SofttokenConstants.PROPERTIES_FILE_NAME;
			Log.e("ruta",propertiesPath);
			FileInputStream fileReader = new FileInputStream(propertiesPath);
			
			Properties properties = new Properties();			
			properties.load(fileReader);
			fileReader.close();
			
			ArrayList<String> values = new ArrayList<String>();
			values.add(properties.getProperty(SofttokenConstants.SERIAL));
			values.add(properties.getProperty(SofttokenConstants.PIN));
			
			return values;
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return null;
	}

}
