package suitebancomer.aplicaciones.softtoken.classes.model.token;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import bancomer.api.common.commons.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by elunag on 20/07/16.
 */
public class SyncRespuesta implements ParsingHandler
{

    private String status;
    private String code;
    private String description;
    private String errorCode;

    public SyncRespuesta(String status, String code, String description, String errorCode) {
        this.status = status;
        this.code = code;
        this.description = description;
        this.errorCode = errorCode;
    }

    public SyncRespuesta() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser){

        try {
             parseResponse(parser);
        } catch (JSONException e) {
           e.printStackTrace();
        } catch (ParsingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public String parseValidString(JSONObject jObj,String tag){
        return (!jObj.optString(tag).equalsIgnoreCase("") && !jObj.isNull(tag)
                && !jObj.optString(tag).equalsIgnoreCase("NO_VALUE"))
                ? jObj.optString(tag) : Constants.EMPTY_STRING;
    }

    public void parseResponse(ParserJSON parser) throws ParsingException, IOException, JSONException {
        JSONObject jResponse = parser.parserNextObject("response");
        JSONObject jStatus = parser.parserNextObject("status");

        if(jResponse.length() > 0) {
            code = parseValidString(jStatus, "code");
            description = parseValidString(jStatus, "description");
            status = parseValidString(jResponse, "status");
        } else {
            parseError(parser);
        }
    }

    public void parseError(ParserJSON parser) throws ParsingException,IOException{
        JSONObject eResponse = new JSONObject();


        try {
            eResponse.put("status",parser.parseNextValue("status"));
            eResponse.put("errorCode",parser.parseNextValue("errorCode"));
            eResponse.put("description",parser.parseNextValue("description"));
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        status = parseValidString(eResponse,"status");
        code = parseValidString(eResponse,"errorCode");
        description = parseValidString(eResponse,"description");
    }
}
