package suitebancomer.aplicaciones.softtoken.classes.model.token;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by alanmichaelgonzalez on 20/01/16.
 */
public class ConsultaTerminosDeUsoData implements ParsingHandler {

    private String estado;
    private String textoTerminosCondiciones;
    private String codigoMensaje;
    private String descripcionMenjsae;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTextoTerminosCondiciones() {
        return textoTerminosCondiciones;
    }

    public void setTextoTerminosCondiciones(String textoTerminosCondiciones) {
        this.textoTerminosCondiciones = textoTerminosCondiciones;
    }

    public String getCodigoMensaje() {
        return codigoMensaje;
    }

    public void setCodigoMensaje(String codigoMensaje) {
        this.codigoMensaje = codigoMensaje;
    }

    public String getDescripcionMenjsae() {
        return descripcionMenjsae;
    }

    public void setDescripcionMenjsae(String descripcionMenjsae) {
        this.descripcionMenjsae = descripcionMenjsae;
    }


    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

    }
}
