package suitebancomer.aplicaciones.softtoken.classes.common.token;

public class SofttokenConstants {
	// #region DataBase.
	public static final String DB_FILE = "softtoken.db";
	public static final String DB_KEY_ID = "_id";
	public static final String DB_SESSION_TABLE = "sessionTable";
	public static final String DB_SESSION_TABLE_PROPERTY_NAME = "propertyName";
	public static final String DB_SESSION_TABLE_PROPERTY_VALUE = "propertyValue";

	public static final String USERNAME_PROPERTY = "softtokenUsername";
	public static final String IUM_SEED_PROPERTY = "softtokenIum";

	//VinTokenSDK
	public static final String S1 = "S1";
	public static final String S2 = "S2";
	public static final String ZERO_CHALLAGE = "00000000000000000000000000000000000000000000000000000000000";
	public static final String MEMORY_STORE = "USER_PREFERENCES";
	public static final String MEMORY_TOKEN_TOTP_INCREMENT = "TOTP_INCREMENT";
	public static final String SERIAL="serialValue";
	public static final String PIN="pinValue";
	public static final String PROPERTIES_FILE_NAME="propFile";
	public static final int SERIAL_POSITION=0;
	public static final int PIN_POSITION=1;
	public static final String TIPO_TOKEN = "tipoToken";
	public static final String NIP = "NIP";

	//NEW
	public static final int VIN_REG_ERR_CA = 3135;
	public static final int VIN_REG_ERR_FREJA = 3130;
	public static final int VIN_REG_ERR_COMUNICACIONES = 3132;
	public static final int VIN_REG_ERR_COMUNICACIONES_3122 = 3122;


	// #endregion
    /*
    * Ruta del servicio que comparte las OTPs
     */

	public static final String ROOT_SERVICE = "suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ServiceBconnect";
	/*
     * Ruta del servicio que comparte las OTPs
     */
	public static final String ROOT_KEY_CHAIN = "/.bancomer/keyChain.bks";

	/**
	 * Ruta del servicio que comparte las OTPs
	 */
	public static final String PACKAGE_SERVICE = "paqueteServicio";

	/**
	 * Tipo de otp
	 */
	public static final String TIPO_OPTCONSTANT = "tipoOTP";
	/**
	 * Cuenta de destino
	 */
	public static final String CUENTA_DESTINOCONSTANT = "cuentaDestino";
	/**
	 * CadenaQR
	 */
	public static final String CADENA_QR = "cadenaQR";
	/**
	 * Constante de OTP
	 */
	public static final String OTP_CONSTANT = "OTP";
	/**
	 * Constante de codigo
	 */
	public static final String TIPO_CODIGO = "codigo";
	/**
	 * Constante de tipo de regitro
	 */
	public static final String TIPO_REGISTRO = "registro";
	/**
	 * Constante de codigoQR
	 */
	public static final String TIPO_CODIGOQR = "codigoQR";

	/**
	 *
	 */
	public static final String KEY_STORE_MANAGER = "keyStoreManager";
	/**
	 *  Bandera para validar si esta contratado el softtoken en alguna otra api
	 */
	public static String PACKAGE_NAME="";
	/**
	 * Bandera para diferenciar cuando es una OTP por servicio
	 */
	public static String OTP_SERVICE="0000000000";
	/**
	 * Constante para diferenciar cuanto es una OTP por servicio y con codigo QR
	 */
	public static String OTP_SERVICE_CODIGOQR="0000000001";
	/**
	 * The fixed length of a telephone number.
	 */
	public static final int TELEPHONE_NUMBER_LENGTH = 10;

	/**
	 * The fixed length of a card number.
	 */
	public static final int CARD_NUMBER_LENGTH = 16;

	/**
	 * The fixed length of a NIP number.
	 */
	public static final int NIP_LENGTH = 4;

	/**
	 * The fixed length of a CVV number.
	 */
	public static final int CVV_LENGTH = 3;

	/**
	 * The fixed length of a OTP number.
	 */
	public static final int OTP_LENGTH = 8;

	public static final int DV_LENGTH = 1;

	public static final int ACTIVATION_CODE_LENGTH = 10;

	public static final int TOKEN_SEED_REGISTER_LENGTH = 5;

	/** Longitud de Challenge para Verisek */
	public static final int VERISEK_CHALLENGE_LENGTH = 64;

	/** Formato de Challenge para Verisek */
	public static final String VERISEK_CHALLENGE_FORMAT = "000000000000000" +
			"000000000000000" +
			"000000000000000" +
			"00000000000000";

	public static final String VERISEK_CHALLENGE_QR_FORMAT = "000000000000000" +
			"000000000000000" +
			"000000000000000" +
			"00000000000";

	public static final String TOKEN_GENERADO = "token_gene";

	/**
	 * Indicador contratacion - token nuevo.
	 */
	public static final String TOKEN_NUEVO = "N";

	/**
	 * Indicador contratacion - reactivacion.
	 */
	public static final String REACTIVACION = "R";

	/**
	 * Indicador contratacion - sustituacion.
	 */
	public static final String SUSTITUCION = "S";

	/**
	 * Indicador contratacion - no existe solicitud.
	 */
	public static final String NO_EXISTE_SOLICITUD = "X";

	public static final long ESPERA_15_SEG = 15000L;
	public static final long ESPERA_8_SEG = 8000L;
	public static final long ESPERA_7_SEG = 7000L;

	/**
	 * Llave para identificar el parametro que indica el tipo de otp a mostrar
	 * (tiempo/registro).
	 */
	public static final String TIPO_OTP_A_MOSTRAR_PARAM = "tipoOtp";
	/**
	 * Identificador para una otp por tiempo.
	 */
	public static final String OTP_TIEMPO = "tiempo";

	/**
	 * Identificador para una otp por registro.
	 */
	public static final String OTP_REGISTRO = "registro";

	public static final String MENSAJE_ERROR = "mensajeError";

	/**
	 * Constante de validacion de los datos ingresados de la tarjeta del
	 * cliente. El usuario no tiene Bancomer movil contratado.
	 */
	public static final int VALIDACION_TARJETA_00 = 00;

	/**
	 * Constante de validacion de los datos ingresados de la tarjeta del
	 * cliente. El usuario no tiene contratado alertas.
	 */
	public static final int VALIDACION_TARJETA_01 = 01;

	/**
	 * Constante de validacion de los datos ingresados de la tarjeta del
	 * cliente. El telefono ingresado no coincide con el registrado en alertas.
	 */
	public static final int VALIDACION_TARJETA_02 = 02;

	/**
	 * Constante de validacion de los datos ingresados de la tarjeta del
	 * cliente. La compannia ingresada no coincide con la compannia que se tiene
	 * en alertas.
	 */
	public static final int VALIDACION_TARJETA_03 = 03;

	/**
	 * Constante de validacion de los datos ingresados de la tarjeta del
	 * cliente. El telefono asociado al servicio Bmovil no coincide con el
	 * telefono de alertas.
	 */
	public static final int VALIDACION_TARJETA_04 = 04;

	/**
	 * Constante de validacion de los datos ingresados de la tarjeta del
	 * cliente. La compannia asociada al servicio Bmovil no coincide con la
	 * compannia de alertas.
	 */
	public static final int VALIDACION_TARJETA_05 = 05;

	/**
	 * Constante de validacion de los datos ingresados de la tarjeta del
	 * cliente. Estatus correcto para efectuar la solicitud de soft token.
	 */
	public static final int VALIDACION_TARJETA_06 = 06;

	/**
	 * Numero de transaccion
	 */
	public static final int NUMBER_TRANSACTION = 33;

	/**
	 * Constante que indica si se tiene dispositivo f�sico
	 */
	public static final String TIENE_DISPOSITIVO_FISICO = "SI";


	/**
	 * Constante que indica si se tiene el estatus Dispositivo A1
	 */
	public static final String ESTATUS_DISPOSITIVO_A1 = "A1";

	/**
	 * Constante que indica si se tiene switch enrolamiento activado
	 */
	public static final String SWITCH_ENROLAMIENTO_ACTIVADO = "S";

	//Alta Registro: Softtoken ODT2
	public static final String RESULT_REGISTER1 = "result1";
	public static final String RESULT_REGISTER2 = "result2";
	public static final String TYPE_OPERATION = "register_type";
	public static final String CODQR_VALUE = "codqr_value";

	public static final String QRCODE_DATA_KEY = "datosQR";

	public static final int OPTICA_SEED = 96158743;
	public static final int OPTICA_ROUNDS = 100;
}
