package suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token;

/**
 * Created by IDS_COMERCIAL on 20/07/2016.
 */

import android.content.Context;
import android.content.res.Resources;
import android.os.Binder;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Log;

import com.bancomer.mbanking.softtoken.SuiteAppApi;

import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.common.token.Common;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class ServiceBconnectImpl extends Binder {

    private final String TAG = ServiceBconnectImpl.class.getSimpleName();


    Context context;
    ContratacionSTDelegate contDelegate;
    TipoOtpAutenticacion tipoOTP;

    public ServiceBconnectImpl(Resources res, Context contex) {
        this.context = contex;
        final SuiteAppApi softoken = new SuiteAppApi();
        softoken.onCreate(contex);
        if (null == SuiteAppApi.getInstanceApi().getSofttokenApplicationApi())
            SuiteAppApi.getInstanceApi().startSofttokenAppApi();
        contDelegate = new ContratacionSTDelegate();
        if(Common.getCommon().buscarVariableKeyChain(SofttokenConstants.TIPO_TOKEN).equals(SofttokenConstants.S1)){
            contDelegate.generaTokendelegate.inicializaCore();
        }
    }

    /**
     * Valida ALLOW_LOG
     *
     * @param msg tipo String
     */
    public void logger(String msg) {
        if (ServerCommons.ALLOW_LOG) {
            Log.w(TAG, msg);
        }
    }

    /**
     * Metodo
     * @param code  tipo int con el codigo
     * @param data  tipo Parcel
     * @param reply tipo Parcel
     * @param flags tipo int con las banderas
     * @return
     */
    @Override
    protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) {

        logger("entra al servicio");
        if (code == SofttokenConstants.NUMBER_TRANSACTION) {
            Bundle values = data.readBundle();
            String otp = null;
            logger("Servicio ejecutado INTERMEDIO en BConnect");
            // Obtener tipo de otp
            String otpString = (String) values.get("tipoOTP");
            logger("tipo otp solicitada:" + otpString);
            try {
                tipoOTP = TipoOtpAutenticacion.valueOf(otpString);
                logger("valor de otp:" + tipoOTP.toString());
            } catch (Exception e) {
                logger("error al generar otp:" + e.getMessage());
            }
            if (TipoOtpAutenticacion.registro == tipoOTP) {
                logger("solicita otp de registro");
                String cuentaDestino = (String) values.get("cuentaDestino");
                logger("cuenta destino" + cuentaDestino);
                escenarioAlt18(cuentaDestino);
            } else if (TipoOtpAutenticacion.codigo == tipoOTP) {
                logger("solicita otp de codigo");
                escenarioAlt17();
            }
             else {
                logger("solicita otp codigoQR");
                String cadenaQR = (String) values.get("cadenaQR");
                escenarioQR(cadenaQR);
            }
            logger("Saliendo de Servicio");
        }
        return true;
    }


    /**
     * Metodo de EA#17 de B-Connect "Generacion de OTP para softoken"
     */
    public void escenarioAlt17() {
        logger("ea17");
        String otp = null;
        // Generar OTP
        otp = contDelegate.generaTokendelegate.generarOtpTiempo();
        logger("otp generada: " + otp);
        saveOtpInKeyChain(otp);
    }

    /**
     * Metodo de EA#18 de B-Connect "Generacion de OTP de Registro para softoken"
     *
     * @param cuentaDestino
     */
    public void escenarioAlt18(String cuentaDestino) {
        logger("ea18");
        String otp = null;
        // Generar OTP
        otp = (null == cuentaDestino) ? null : contDelegate.generaTokendelegate.generarOtpChallenge(cuentaDestino);
        logger("otp generada: " + otp);
        saveOtpInKeyChain(otp);
    }

    /**
     * Metodo generar OTP "Generacion de OTP con codigo QR"
     *
     * @param cadenaQR
     */
    public void escenarioQR(String cadenaQR){
        logger("eaQR");
        String otp = null;
        otp = (null == cadenaQR) ? null : contDelegate.generaTokendelegate.generaOTPFromQR(cadenaQR);
        logger("otp generda: " + otp);
        saveOtpInKeyChain(otp);
    }

    /**
     * Guardar OTP
     *
     * @param otp tipo String
     */
    public void saveOtpInKeyChain(String otp) {
        KeyStoreWrapper kswrapper = KeyStoreWrapper.getInstance(context);
        logger("Guardando otp en keychain");
        try {
            kswrapper.storeValueForKey("OTP", otp);
        } catch (Exception e) {
            logger("Error saveOtpInKeyChain: " + e.toString());
        }
    }

}
