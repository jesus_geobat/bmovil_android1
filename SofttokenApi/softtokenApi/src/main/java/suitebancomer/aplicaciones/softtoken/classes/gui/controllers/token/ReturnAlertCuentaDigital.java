package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import suitebancomer.aplicaciones.bmovil.classes.model.Account;

/**
 * Created by mcamarillo on 01/09/16.
 */
public interface ReturnAlertCuentaDigital {
    public void returnCuentaDigital(final String alert);

}
