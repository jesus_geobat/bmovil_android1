package suitebancomer.aplicaciones.softtoken.classes.model.token;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import bancomer.api.common.commons.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by mcamarillo on 20/07/16.
 */
public class ResendActivationKey implements ParsingHandler {

    /**
     * fecha servidor
     */
    private String status;
    /**
     * fecha servidor
     */
    private String code;
    /**
     * fecha servidor
     */
    private String description;


    public void process(String parser) throws IOException, ParsingException {


    }

    private String getDataFromJson(final JSONObject jsonObject, final String data,
                                   final String opt) throws JSONException {
        if (!jsonObject.isNull(data)) {
            return jsonObject.getString(data);
        }
        return opt;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        try {
            JSONObject jStatus = parser.parserNextObject("status");
            JSONObject jResponse = parser.parserNextObject("response");

            code = parseValidString(jStatus, "code");
            description = parseValidString(jStatus, "description");
            status = parseValidString(jResponse, "status");

        } catch (ParsingException e) {

            JSONObject eResponse = new JSONObject();


            try {
                eResponse.put("status",parser.parseNextValue("status"));
                eResponse.put("errorCode",parser.parseNextValue("errorCode"));
                eResponse.put("description",parser.parseNextValue("description"));
            } catch (JSONException e1) {
                e1.printStackTrace();
            }

            status = parseValidString(eResponse, "status");
            code = parseValidString(eResponse, "errorCode");
            description = parseValidString(eResponse, "description");
        }
    }

    public String parseValidString(JSONObject jObj,String tag){
        return (!jObj.optString(tag).equalsIgnoreCase("") && !jObj.isNull(tag)
                && !jObj.optString(tag).equalsIgnoreCase("NO_VALUE"))
                ? jObj.optString(tag) : Constants.EMPTY_STRING;
    }
}
