package suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token;

import android.content.Context;

import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomercoms.classes.common.PropertiesManager;

/**
 * Created by davidgarcia on 17/08/16.
 */
public class ValidacionEstatusST implements GetOtp {
    /**
     * Metodo para leer el properties de
     * Realiza la validacion del servicio
     */
    public void validaEstatusSofttoken(Context cnt) {
        final boolean isSofttokenActive = PropertiesManager.getCurrent()
                .getSofttokenActivated();
        String paqueteServicio=KeyStoreWrapper.getInstance(cnt).fetchValueForKey("paqueteServicio");
        if ((!isSofttokenActive) || (!paqueteServicio.equalsIgnoreCase(cnt.getPackageName()))){
            PropertiesManager.getCurrent().setSofttokenActivated(false);
            GetOtpBmovil getOtpBmovil = new GetOtpBmovil(this, cnt);
            if (getOtpBmovil.validaServicio()) {
                PropertiesManager.getCurrent()
                        .setSofttokenService(true);
            }
            else{
                PropertiesManager.getCurrent()
                        .setSofttokenService(false);
            }
        }
    }

    /**
     * Metodo sobreescrito para generar codigo OTP (Tiempo) mediante un servicio
     * @param s
     */
    @Override
    public void setOtpCodigo(String s) {
    }

    /**
     * Metodo sobreescrito para generar codigo OTP (Registro) mediante un servicio
     * @param s
     */
    @Override
    public void setOtpRegistro(String s) {
    }

    /**
     * Metodo sobreescrito para generar codigo OTP (codigoQR) mediante un servicio
     * @param otp
     */
    @Override
    public void setOtpCodigoQR(String otp) {

    }
}
