package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

//import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegateCuentaDigital;

/**
 * Created by mcamarillo on 30/06/16.
 */
public class ActivacionSTCuentaDigitalViewController extends SofttokenBaseViewControllerCD {

    ContratacionSTDelegate contratacionDelegate;

    /**
     * Referencia a la clase misma
     */
    private static ActivacionSTCuentaDigitalViewController me;

    /**
     * Constructor por defecto
     */
    public ActivacionSTCuentaDigitalViewController() {

        me = this;
    }
    /**
     * Metodo que devuelve una instancia de la clase
     * @return una instancia de esta clase
     */
    public static ActivacionSTCuentaDigitalViewController getInstance() {
        return me;
    }

    public ContratacionSTDelegate getContratacionDelegate() {
        return contratacionDelegate;
    }

    public void setContratacionDelegate(ContratacionSTDelegate contratacionDelegate) {
        this.contratacionDelegate = contratacionDelegate;
    }

    @Override
    public void processNetworkResponse(final int operationId, final ServerResponse response) {
        contratacionDelegate.analyzeResponse(operationId, response);
    }





}
