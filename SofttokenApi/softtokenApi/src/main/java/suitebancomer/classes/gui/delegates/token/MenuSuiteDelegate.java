package suitebancomer.classes.gui.delegates.token;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SofttokenApp;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomer.classes.gui.controllers.token.MenuSuiteViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.classes.common.PropertiesManager;
//import com.bancomer.mbanking.softtoken.BmovilApp;
//import suitebancomercoms.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
//import suitebancomercoms.aplicaciones.bmovil.classes.gui.delegates.LoginDelegate;

public class MenuSuiteDelegate extends BaseDelegate {
	
	public final static long MENU_SUITE_DELEGATE_ID = 0x3329474451002cd6L; 
	
	private MenuSuiteViewController menuSuiteViewController;
	
	private boolean isCallActive;
	private boolean bMovilSelected;
	
	public void setbMovilSelected(final boolean bMovilSelected) {
		this.bMovilSelected = bMovilSelected;
	}
	
	public boolean isbMovilSelected() {
		return bMovilSelected;
	}
	
	public MenuSuiteDelegate() {
	}
	
	public boolean isCallActive() {
		return isCallActive;
	}
	
	public void setCallActive(final boolean isCallActive) {
		this.isCallActive = isCallActive;
	}
	
	public boolean isDisconnected(){
		 final SuiteAppApi suiteApp = SuiteAppApi.getInstanceApi();

         final ConnectivityManager connectivity = (ConnectivityManager)
				 suiteApp.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		 
         if (connectivity != null){
             final NetworkInfo[] info = connectivity.getAllNetworkInfo();
             if (info != null){
                 for (int i = 0; i < info.length; i++){
                     if (info[i].getState() == NetworkInfo.State.CONNECTED){
                         return false;
                     }
                 }
             }
         }
         return true;
	}
	


	
	public MenuSuiteViewController getMenuSuiteViewController() {
		return menuSuiteViewController;
	}

	public void setMenuSuiteViewController(final MenuSuiteViewController menuSuiteViewController) {
		this.menuSuiteViewController = menuSuiteViewController;
	}


	/**
	 * Devuelve si existe el archivo PendienteDescarga
	 * @return
	 */
	private boolean existePendienteDescarga(){
		boolean respuesta = false;
        final SofttokenActivationBackupManager manager = SofttokenActivationBackupManager
				.getCurrent();
		if (manager.existsABackup()) {
			respuesta = true;
		}
		return respuesta;
	}
	
	private void showActivacionSTEA12() {
		cambiarDeBMovilTokenMovil().showActivacionSTEA12();
	}

	/**
	 * Redirige al caso de uso ActivacionST escenario principal
	 */
	private void showActivacionSTEP() {
		cambiarDeBMovilTokenMovil().showPantallaIngresoDatos();
	}
	
	/*private void showActivacionST(){
		cambiarDeBMovilTokenMovil().showActivacionSTEP25();
	}*/
	
	private SofttokenViewsController cambiarDeBMovilTokenMovil() {
        final SofttokenViewsController viewsController = SuiteAppApi.getInstanceApi()
				.getSofttokenApplicationApi().getSottokenViewsController();
		menuSuiteViewController.setParentViewsController(viewsController);
		viewsController.setCurrentActivityApp(menuSuiteViewController);

		return viewsController;
	}
	
	private boolean buscarBanderasBmovil(){
        final Session session = Session.getInstance(SuiteAppApi.appContext);
	
		if(session.getUsername()==null || Constants.EMPTY_STRING.equals(session.getUsername())){
			PropertiesManager.getCurrent().setBmovilActivated(false);
		}
		return Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CONTRATAR2x1));
	}
	

	
	public void leerContratacion() {
        final int pendingStatus = Session.getInstance(SuiteAppApi.appContext).getPendingStatus();
		
		if (pendingStatus > 0) {
		} else {
			showLogin();
		}
	}
	
	private void showLogin() {
		/*
		  LoginDelegate loginDelegate = (LoginDelegate) SuiteApp.getInstance()
		 
				.getBmovilApplication().getBmovilViewsController()
				.getBaseDelegateForKey(LoginDelegate.LOGIN_DELEGATE_ID);
		if (loginDelegate == null) {
			loginDelegate = new LoginDelegate();
			SuiteApp.getInstance()
					.getBmovilApplication()
					.getBmovilViewsController()
					.addDelegateToHashMap(LoginDelegate.LOGIN_DELEGATE_ID,
							loginDelegate);
		}

		// LoginDelegate loginDelegate = new LoginDelegate();
		BmovilApp bmovilApp = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bmovilViewsController = bmovilApp
				.getBmovilViewsController();
		bmovilViewsController.setCurrentActivityApp(menuSuiteViewController);
		bmovilViewsController.addDelegateToHashMap(
				LoginDelegate.LOGIN_DELEGATE_ID, loginDelegate);
		bmovilViewsController.showLogin();
	}
	
	public void llamarLineaBancomer(String numeroTel) {
		try {
			isCallActive = true;
	        Intent callIntent = new Intent(Intent.ACTION_CALL);
	        callIntent.setData(Uri.parse(Constants.TEL_URI+numeroTel));
	        menuSuiteViewController.startActivity(callIntent);
	    } catch (ActivityNotFoundException e) {
	    	menuSuiteViewController.showErrorMessage(menuSuiteViewController.getString(R.string.menuSuite_callErrorMessage));
	    }
	    */
	}

	// #region Softtoken.
	public void softtokenSelected() {
		if(null == SuiteAppApi.getInstanceApi().getSofttokenApplicationApi())
			SuiteAppApi.getInstanceApi().startSofttokenAppApi();

        final SofttokenViewsController viewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		if(SuiteAppApi.getSofttokenStatusApi()) {
			//menuSuiteViewController.restableceMenu();
			viewsController.setCurrentActivityApp(menuSuiteViewController);
			viewsController.showPantallaGeneraOTPST(false);
		} else {
			viewsController.showContratacionSotfttoken(menuSuiteViewController);
		}
	}
	
	public void leerContratacionST() {
			
	}
	// #endregion
	
	public void onBackPressed() {	
		/*
		if(menuSuiteViewController.getLoginViewController() != null && 
		   menuSuiteViewController.getLoginViewController().getVisibility() == View.VISIBLE) {
			menuSuiteViewController.plegarOpcion();
		} else if(menuSuiteViewController.getAplicacionDesactivadaViewController() != null && 
				  menuSuiteViewController.getAplicacionDesactivadaViewController().getVisibility() == View.VISIBLE) {
			menuSuiteViewController.plegarOpcionAplicacionDesactivada();
		} else if(menuSuiteViewController.getContratacionSTViewController() != null && 
				  menuSuiteViewController.getContratacionSTViewController().getVisibility() == View.VISIBLE) {
			menuSuiteViewController.plegarOpcionST();
		} else {
			SuiteApp.getInstance().cierraAplicacionSuite();
		}
		*/
	}
	
	
	/**
	 * Se selecciona la opci�n de men� Token M�vil.
	 * @param sender la opci�n seleccionada
	 */
	public void onBtnContinuarclick(final View sender) {
        final SofttokenApp softtokenApp = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi();

        final SofttokenViewsController viewsController = softtokenApp.getSottokenViewsController();
		if (SuiteAppApi.getSofttokenStatusApi()) {
			//menuSuiteViewController.restableceMenu();
			viewsController.setCurrentActivityApp(menuSuiteViewController);
			viewsController.showPantallaGeneraOTPST(false);
		} else {
//			viewsController.setCurrentActivityApp(menuSuiteViewController);
//			
//			SofttokenActivationBackupManager manager = SofttokenActivationBackupManager
//					.getCurrent();
//
//			if (manager.existsABackup()) {
//				softtokenApp.getSottokenViewsController()
//						.showPantallaCActivacionST(true);
//			} else {
//				softtokenApp.getSottokenViewsController()
//						.showPantallaIngresoDatos(menuSuiteViewController);
//			}
			
			//EA#10, RN8 y RN9: comprobar la conectividad a internet antes de acceder a la aplicación
			if (this.isDisconnected()) {
				//menuSuiteViewController.setButtonsDisabled(false);
				menuSuiteViewController.showInformationAlert(R.string.menuSuite_alert_disconnected);
				
			} else {
				// Contin�a el flujo normal del EA#2: paso 6
				if(existePendienteDescarga()) {
					showActivacionSTEA12();
				} else {
					showActivacionSTEP();
				}
			}
		}
	}
	
	
	
}
