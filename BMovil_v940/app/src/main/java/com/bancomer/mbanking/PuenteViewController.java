package com.bancomer.mbanking;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.bancomer.mbanking.softtoken.SuiteAppApi;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.classes.gui.controllers.MenuSuiteViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class PuenteViewController extends Activity {

    private static final String TAG = "PuenteViewController";
    // private String activationCode;
    private Uri data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puente_view_controller);
        data = getIntent().getData();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        data = intent.getData();
    }

    @Override
    protected void onResume() {
        String type = "";
        String activationCode = "";
        SharedPreferences preferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
        type=preferences.getString(Constants.SHARED_PREFERENCES_FLAG_TYPE, Constants.EMPTY_STRING);
        if(Constants.EMPTY_STRING.equals(type)){
            if (ServerCommons.ALLOW_LOG)
                Log.e(TAG, "onResume: Se abre sms con url no esperada" );
        }else {

            if (data != null) {
                try {
                    getIntent().setData(null);
                    getIntent().removeExtra("key");
                    String[] text;
                    if (data.toString().contains(".html")) {
                        text = data.toString().split(".html.?");
                    }else{
                        text = data.toString().split("com.bancomer/");
                    }
                    activationCode = text[text.length - 1];

                    if (!activationCode.endsWith("==") && ( !Session.getInstance(SuiteAppApi.appContext).getKeyStoreWrapper().fetchValueForKey("tipoToken").equals("S2")))
                        activationCode += "==";

                    String[] activations = activationCode.split("=");
                    if (activations.length > 1) {
                        type = activations[0];
                        activationCode = activationCode.replace(type + "=", Constants.EMPTY_STRING);
                    }
                } catch (Exception e) {
                    if (ServerCommons.ALLOW_LOG)
                        Log.e(TAG, "onResume: " + e.getMessage(), e.getCause());
                }
            }

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(Constants.SHARED_PREFERENCES_FLAG_CODE, activationCode);
            editor.putString(Constants.SHARED_PREFERENCES_FLAG_TYPE, type);
            editor.putString(Constants.SHARED_PREFERENCES_CODE_USED, type);
            editor.apply();
        }

        startActivity(new Intent(this, MenuSuiteViewController.class));
        finish();

        super.onResume();
    }

}
