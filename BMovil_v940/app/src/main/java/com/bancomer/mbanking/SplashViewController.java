package com.bancomer.mbanking;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.ImageView;

import com.adobe.mobile.Config;
import com.bancomer.mbanking.softtoken.KeyChainCheck;

import java.util.Timer;
import java.util.TimerTask;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;

/**
 * Activity that presents the splash screen. This is the main launcher for all the application.
 * @author Michael Andrade
 *
 */

public class SplashViewController extends BaseViewController {
	
	/**
	 * The timer for presenting the splash
	 */
	private Timer splashTimer;
	
	/**
	 * The action performed when the timer expires
	 */
	private TimerTask splashTimerTask;
	
	private SuiteApp suiteApp;
	
	private boolean isAppCanceled;
	
	/**
	 * Default constructor of this activity
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {


		ServerCommons.loadEnviromentConfig(this);
		if(!Server.ALLOW_LOG){
			ApiConstants.allowLogPropertieName = getLocalClassName();
		}
		super.onCreate(savedInstanceState, 0, R.layout.layout_splash_activity);
		
		suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getSuiteViewsController());
		try {
			String v=getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
			v=v.replace(".","");
			BmovilConstants.APPLICATION_VERSION = v;
			suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.APPLICATION_VERSION=v;
			Constants.APPLICATION_VERSION=v;
		}
		catch (PackageManager.NameNotFoundException e){
			if(ServerCommons.ALLOW_LOG){
				Log.d("APP", "No se encontro la version de la app");
			}
		}
		boolean stActive= PropertiesManager.getCurrent().getSofttokenActivated();
		if (stActive){
			KeyStoreWrapper.getInstance(this).storeValueForKey("paqueteServicio", this.getPackageName());
			KeyStoreWrapper.getInstance(this).storeValueForKey(Constants.CENTRO, " ");
		}
		suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST validacionEstatusST = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST();
		validacionEstatusST.validaEstatusSofttoken(this);
		KeyChainCheck kcc=new KeyChainCheck(this);
		kcc.verify();

		ImageView img = (ImageView)findViewById(R.id.splashImage);
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		img.getLayoutParams().height = metrics.heightPixels;
		//AMZ
		Config.setContext(this.getApplicationContext());
		//AMZFIN
	}
	
	/**
	 * Method used when this activity awakes from pause state
	 */
	@Override
	protected void onResume() {
		super.onResume();
		//AMZ
		Config.collectLifecycleData();
		//AMZFIN
		getParentViewsController().setCurrentActivityApp(this);
		
		splashTimer = new Timer();
		splashTimerTask = new SplashTask();
		
		splashTimer.schedule(splashTimerTask, Constants.SPLASH_VIEW_CONTROLLER_DURATION);
	}
	
	@Override
	protected void onPause() {
		//goBack();
		super.onPause();
		//AMZ
		Config.pauseCollectingLifecycleData();
		//AMZFIN
		splashTimer.cancel();
	}

	/**
     * Overrides and cancels android's back button, if the button is visible then it does the same
     * as clicking on the application's back button.
     */
    @Override    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			splashTimer.cancel();
    			splashTimerTask.cancel();
    			isAppCanceled = true;
    			android.os.Process.killProcess(android.os.Process.myPid());
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}	
    	
    }
	
	/**
	 * Use handler to respond to splash timeout.
	 */
	private Handler mApplicationHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (!isAppCanceled) {
				((SuiteApp)getApplication()).getSuiteViewsController().showMenuSuite(false);
			}
		}
	};
	
	/**
	 * Class that indicates when to start the application
	 */
	private class SplashTask extends TimerTask {

		/**
		 * The actions to be performed
		 */
		@Override
		public void run() {
			Message msg = new Message();
			mApplicationHandler.sendMessage(msg);
		}
		
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		return true;
	}
}
