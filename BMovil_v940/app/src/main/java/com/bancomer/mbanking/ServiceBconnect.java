package com.bancomer.mbanking;

// Copyright Infinite Bandwidth ltd (c) 2010. All rights reserved.
// Created 23 Oct 2010, by M. Massenzio (marco@alertavert.com)

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;


public class ServiceBconnect extends Service {


    @Override
    public IBinder onBind(Intent intent) {
        return new ServiceBconnectImpl(getResources(),this);
    }
}