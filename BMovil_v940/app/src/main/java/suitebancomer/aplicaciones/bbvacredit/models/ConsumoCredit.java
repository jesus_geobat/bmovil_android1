package suitebancomer.aplicaciones.bbvacredit.models;
/** Created: April 22th, 2015. Author:OOS.*/

public class ConsumoCredit {
	
	private String producto;
	private String cveSubP;
	private String pagMilS;
	
	
	
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getCveSubP() {
		return cveSubP;
	}
	public void setCveSubP(String cveSubP) {
		this.cveSubP = cveSubP;
	}
	public String getPagMilS() {
		return pagMilS;
	}
	public void setPagMilS(String pagMilS) {
		this.pagMilS = pagMilS;
	}
	
}
