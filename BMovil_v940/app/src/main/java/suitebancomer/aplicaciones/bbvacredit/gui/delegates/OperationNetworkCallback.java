package suitebancomer.aplicaciones.bbvacredit.gui.delegates;

import suitebancomer.aplicaciones.bbvacredit.io.ServerResponseCredit;

public interface OperationNetworkCallback {
	/**
     * Se procesa la respuesta obtenida del servidor.
     * @param operationId el identificador de la operaci�n
     * @param response la respuesta del servidor
     */
    public void analyzeResponse(String operationId, ServerResponseCredit response);
}
