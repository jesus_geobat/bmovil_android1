package suitebancomer.aplicaciones.bbvacredit.io;

import android.util.Log;

import java.util.ResourceBundle;

public class CreditPropertyList {
	public String read(String propertyToRecover)
    {
		ResourceBundle myResources = ResourceBundle.getBundle("suitebancomer.aplicaciones.bbvacredit.io.creditPropertiesFile");
		String prop = myResources.getString(propertyToRecover);
		Log.w("DATOS PROPERTY", prop);
		return prop;
    }
}