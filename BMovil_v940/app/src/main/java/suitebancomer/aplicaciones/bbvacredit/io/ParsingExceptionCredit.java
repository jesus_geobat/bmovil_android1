package suitebancomer.aplicaciones.bbvacredit.io;

public class ParsingExceptionCredit extends Exception {
	
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	
	/**
     * Default constructor.
     * @param message the message
     */
    public ParsingExceptionCredit(String message) {
        super(message);
    }
}
