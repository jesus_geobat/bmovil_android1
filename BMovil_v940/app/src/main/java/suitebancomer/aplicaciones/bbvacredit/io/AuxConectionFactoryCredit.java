package suitebancomer.aplicaciones.bbvacredit.io;
import com.bancomer.mbanking.SuiteApp;
import org.json.JSONException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;


/**
 * Created by alanmichaelgonzalez on 27/04/16.
 */
public class AuxConectionFactoryCredit {


    /**
     * convierte los parametros de server a los parametro como lo ocupara server api
     * @param params
     * @return
     * @throws IOException
     * @throws ParsingExceptionCredit
     * @throws JSONException
     * @throws ParsingException
     * @throws URISyntaxException
     */
    public static Hashtable<String, String> calculo(Hashtable<String, ?> params) {
        //CalculoData data = new CalculoData();
            Hashtable<String, String> json = new Hashtable<String, String>();
            // Se distingue entre los par�metros que vayamos a meter ? para diferenciar las peticiones de liquidaci�n y contrataci�n; por tanto no meter par�metros vacios
            if (params.containsKey(ServerConstantsCredit.CVESUBP_PARAM) || params.containsKey(ServerConstantsCredit.CVEPROD_PARAM) || params.containsKey(ServerConstantsCredit.CVEPLAZO_PARAM)) {
                // Contrataci�n
                //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION));
                json.put(ServerConstantsCredit.CLIENTE_PARAM, String.valueOf(params.get(ServerConstantsCredit.CLIENTE_PARAM)));
                json.put(ServerConstantsCredit.IUM_PARAM,  String.valueOf(params.get(ServerConstantsCredit.IUM_PARAM)));
                json.put(ServerConstantsCredit.NUMERO_CEL_PARAM,  String.valueOf(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));
                json.put(ServerConstantsCredit.CVEPROD_PARAM,  String.valueOf(params.get(ServerConstantsCredit.CVEPROD_PARAM)));
                json.put(ServerConstantsCredit.CVESUBP_PARAM,  String.valueOf(params.get(ServerConstantsCredit.CVESUBP_PARAM)));
                json.put(ServerConstantsCredit.CVEPLAZO_PARAM,  String.valueOf(params.get(ServerConstantsCredit.CVEPLAZO_PARAM)));
                json.put(ServerConstantsCredit.MON_DESE_PARAM,  String.valueOf(params.get(ServerConstantsCredit.MON_DESE_PARAM)));
                json.put(ServerConstantsCredit.TIPO_OP_PARAM,  String.valueOf(params.get(ServerConstantsCredit.TIPO_OP_PARAM)));

            } else {
                // Liquidacion
                //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION));
                json.put(ServerConstantsCredit.CLIENTE_PARAM, String.valueOf(params.get(ServerConstantsCredit.CLIENTE_PARAM)));
                json.put(ServerConstantsCredit.IUM_PARAM, String.valueOf(params.get(ServerConstantsCredit.IUM_PARAM)));
                json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, String.valueOf(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));
                json.put(ServerConstantsCredit.CONTRATO_PARAM, String.valueOf(params.get(ServerConstantsCredit.CONTRATO_PARAM)));
                json.put(ServerConstantsCredit.PAGO_MENSUAL_PARAM, String.valueOf(params.get(ServerConstantsCredit.PAGO_MENSUAL_PARAM)));
                json.put(ServerConstantsCredit.TIPO_OP_PARAM, String.valueOf(params.get(ServerConstantsCredit.TIPO_OP_PARAM)));

            }



        return json;
    }

    /**
     * convierte los parametros de server a los parametro como lo ocupara server api
     * @param params
     * @return
     */
    public static   Hashtable<String, String> consultaCorreo(Hashtable<String, ?> params)  {
            // ConsultaCorreoData data = new ConsultaCorreoData();

            Hashtable<String, String> json = new Hashtable<String, String>();
            //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CONSULTA_CORREO_OPERACION));
            json.put(ServerConstantsCredit.CLIENTE_PARAM, String.valueOf(params.get(ServerConstantsCredit.CLIENTE_PARAM)));
            json.put(ServerConstantsCredit.IUM_PARAM, String.valueOf(params.get(ServerConstantsCredit.IUM_PARAM)));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, String.valueOf(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));
        return json;
    }

    /**
     * convierte los parametros de server a los parametro como lo ocupara server api
     * @param params
     * @return
     */
    public static  Hashtable<String, String> envioCorreo(Hashtable<String, ?> params) {
        //EnvioCorreoData data = new EnvioCorreoData();


            Hashtable<String, String> json = new Hashtable<String, String>();

            //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.ENVIO_CORREO_OPERACION));
            json.put(ServerConstantsCredit.CLIENTE_PARAM,String.valueOf(params.get(ServerConstantsCredit.CLIENTE_PARAM)));
            json.put(ServerConstantsCredit.IUM_PARAM, String.valueOf(params.get(ServerConstantsCredit.IUM_PARAM)));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, String.valueOf(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));

            String indicador = (String) params.get(ServerConstantsCredit.IND_CORREO);

            if (indicador.equals("C")) {
                json.put(ServerConstantsCredit.SCN,String.valueOf(params.get(ServerConstantsCredit.SCN)));
                json.put(ServerConstantsCredit.CN_IMPORTE, String.valueOf(params.get(ServerConstantsCredit.CN_IMPORTE)));
                json.put(ServerConstantsCredit.CN_TASA_ANUAL, String.valueOf(params.get(ServerConstantsCredit.CN_TASA_ANUAL)));
                json.put(ServerConstantsCredit.CN_PLAZO,String.valueOf(params.get(ServerConstantsCredit.CN_PLAZO)));
                json.put(ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL,String.valueOf(params.get(ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL)));

                json.put(ServerConstantsCredit.SPPI,String.valueOf(params.get(ServerConstantsCredit.SPPI)));
                json.put(ServerConstantsCredit.PP_IMPORTE,String.valueOf(params.get(ServerConstantsCredit.PP_IMPORTE)));
                json.put(ServerConstantsCredit.PP_TASA_ANUAL,String.valueOf(params.get(ServerConstantsCredit.PP_TASA_ANUAL)));
                json.put(ServerConstantsCredit.PP_PLAZO,String.valueOf(params.get(ServerConstantsCredit.PP_PLAZO)));
                json.put(ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL,String.valueOf(params.get(ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL)));

                json.put(ServerConstantsCredit.STDC,String.valueOf(params.get(ServerConstantsCredit.STDC)));
                json.put(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO,String.valueOf(params.get(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO)));
                json.put(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO, String.valueOf(params.get(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO)));

                json.put(ServerConstantsCredit.SILDC, String.valueOf(params.get(ServerConstantsCredit.SILDC)));
                json.put(ServerConstantsCredit.IL_TARJETA_DE_CREDITO, String.valueOf(params.get(ServerConstantsCredit.IL_TARJETA_DE_CREDITO)));
                json.put(ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA,String.valueOf(params.get(ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA)));
                json.put(ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA,String.valueOf(params.get(ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA)));

                json.put(ServerConstantsCredit.SCA, String.valueOf(params.get(ServerConstantsCredit.SCA)));
                json.put(ServerConstantsCredit.CA_IMPORTE,String.valueOf(params.get(ServerConstantsCredit.CA_IMPORTE)));
                json.put(ServerConstantsCredit.CA_TASA_ANUAL,String.valueOf(params.get(ServerConstantsCredit.CA_TASA_ANUAL)));
                json.put(ServerConstantsCredit.CA_PLAZO,String.valueOf(params.get(ServerConstantsCredit.CA_PLAZO)));
                json.put(ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL,String.valueOf(params.get(ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL)));

                json.put(ServerConstantsCredit.SCH, String.valueOf(params.get(ServerConstantsCredit.SCH)));
                json.put(ServerConstantsCredit.CH_IMPORTE, String.valueOf(params.get(ServerConstantsCredit.CH_IMPORTE)));
                json.put(ServerConstantsCredit.CH_TASA_ANUAL, String.valueOf(params.get(ServerConstantsCredit.CH_TASA_ANUAL)));
                json.put(ServerConstantsCredit.CH_PLAZO, String.valueOf(params.get(ServerConstantsCredit.CH_PLAZO)));
                json.put(ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL,String.valueOf(params.get(ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL)));

                json.put(ServerConstantsCredit.CON_CAT, String.valueOf(params.get(ServerConstantsCredit.CON_CAT)));
                json.put(ServerConstantsCredit.CON_FECHA_CALCULO,String.valueOf(params.get(ServerConstantsCredit.CON_FECHA_CALCULO)));

            } else if (indicador.equals("L")) {

                //liquidacion
                json.put(ServerConstantsCredit.SCNL, String.valueOf(params.get(ServerConstantsCredit.SCNL)));
                json.put(ServerConstantsCredit.CN_DESC, String.valueOf(params.get(ServerConstantsCredit.CN_DESC)));
                json.put(ServerConstantsCredit.CN_CANT, String.valueOf(params.get(ServerConstantsCredit.CN_CANT)));

                json.put(ServerConstantsCredit.SPPIL, String.valueOf(params.get(ServerConstantsCredit.SPPIL)));
                json.put(ServerConstantsCredit.PP_DESC, String.valueOf(params.get(ServerConstantsCredit.PP_DESC)));
                json.put(ServerConstantsCredit.PP_CANT, String.valueOf(params.get(ServerConstantsCredit.PP_CANT)));

                json.put(ServerConstantsCredit.SCHL, String.valueOf(params.get(ServerConstantsCredit.SCHL)));
                json.put(ServerConstantsCredit.CH_DESC,String.valueOf(params.get(ServerConstantsCredit.CH_DESC)));
                json.put(ServerConstantsCredit.CH_CANT, String.valueOf(params.get(ServerConstantsCredit.CH_CANT)));

                json.put(ServerConstantsCredit.SCAL, String.valueOf(params.get(ServerConstantsCredit.SCAL)));
                json.put(ServerConstantsCredit.CA_DESC, String.valueOf(params.get(ServerConstantsCredit.CA_DESC)));
                json.put(ServerConstantsCredit.CA_CANT, String.valueOf(params.get(ServerConstantsCredit.CA_CANT)));

                json.put(ServerConstantsCredit.SP5, String.valueOf(params.get(ServerConstantsCredit.SP5)));
                json.put(ServerConstantsCredit.P5_DESC, String.valueOf(params.get(ServerConstantsCredit.P5_DESC)));
                json.put(ServerConstantsCredit.P5_CANT, String.valueOf(params.get(ServerConstantsCredit.P5_CANT)));

                json.put(ServerConstantsCredit.SP6, String.valueOf(params.get(ServerConstantsCredit.SP6)));
                json.put(ServerConstantsCredit.P6_DESC, String.valueOf(params.get(ServerConstantsCredit.P6_DESC)));
                json.put(ServerConstantsCredit.P6_CANT, String.valueOf(params.get(ServerConstantsCredit.P6_CANT)));


                //alternativas
                json.put(ServerConstantsCredit.SCR, String.valueOf(params.get(ServerConstantsCredit.SCR)));

                json.put(ServerConstantsCredit.SAN, String.valueOf(params.get(ServerConstantsCredit.SAN)));
                json.put(ServerConstantsCredit.CN, String.valueOf(params.get(ServerConstantsCredit.CN)));

                json.put(ServerConstantsCredit.SAP, String.valueOf(params.get(ServerConstantsCredit.SAP)));
                json.put(ServerConstantsCredit.PP, String.valueOf(params.get(ServerConstantsCredit.PP)));

                json.put(ServerConstantsCredit.SAI, String.valueOf(params.get(ServerConstantsCredit.SAI)));
                json.put(ServerConstantsCredit.IL, String.valueOf(params.get(ServerConstantsCredit.IL)));

                json.put(ServerConstantsCredit.SAH, String.valueOf(params.get(ServerConstantsCredit.SAH)));
                json.put(ServerConstantsCredit.CH, String.valueOf(params.get(ServerConstantsCredit.CH)));

                json.put(ServerConstantsCredit.SAA, String.valueOf(params.get(ServerConstantsCredit.SAA)));
                json.put(ServerConstantsCredit.CA, String.valueOf(params.get(ServerConstantsCredit.CA)));

                json.put(ServerConstantsCredit.SAT, String.valueOf(params.get(ServerConstantsCredit.SAT)));
                json.put(ServerConstantsCredit.TC, String.valueOf(params.get(ServerConstantsCredit.TC)));


            }

            json.put(ServerConstantsCredit.IND_CORREO, indicador);
            json.put(ServerConstantsCredit.email, String.valueOf(params.get(ServerConstantsCredit.email)));



        return json;
    }

    /**
     * convierte los parametros de server a los parametro como lo ocupara server api
     * @param params
     * @return
     */
    public static  Hashtable<String, String>  consultaAlternativas(Hashtable<String, ?> params) {
       // ConsultaAlternativasData data = new ConsultaAlternativasData();
        Hashtable<String, String> json = new Hashtable<String, String>();

            //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CONSULTA_ALTERNATIVAS));
            json.put(ServerConstantsCredit.CLIENTE_PARAM, String.valueOf(params.get(ServerConstantsCredit.CLIENTE_PARAM)));
            json.put(ServerConstantsCredit.IUM_PARAM, String.valueOf(params.get(ServerConstantsCredit.IUM_PARAM)));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, String.valueOf(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));


        return json;
    }

    /**
     *
     * @param params
     * @return
     */
    public static Hashtable<String, String>   consultaDetalleAlternativa(Hashtable<String, ?> params)  {
       // DetalleAlternativa data = new DetalleAlternativa();
        Hashtable<String, String> json = new Hashtable<String, String>();

           // json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.DETALLE_ALTERNATIVA));
            json.put(ServerConstantsCredit.CLIENTE_PARAM, String.valueOf(params.get(ServerConstantsCredit.CLIENTE_PARAM)));
            json.put(ServerConstantsCredit.IUM_PARAM, String.valueOf(params.get(ServerConstantsCredit.IUM_PARAM)));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, String.valueOf(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));
            json.put(ServerConstantsCredit.PRODUCTO_PARAM, String.valueOf(params.get(ServerConstantsCredit.PRODUCTO_PARAM)));

        return json;
    }

    /**
     * convierte los parametros de server a los parametro como lo ocupara server api
     * @param params
     * @return
     */
    public static Hashtable<String, String> contrataAlternativaConsumo(Hashtable<String, ?> params) {
        //AceptaOfertaConsumo aceptaofertaConsumo=new AceptaOfertaConsumo();
        Hashtable<String, String> json = new Hashtable<String, String>();
            //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CONTRATA_ALTERNATIVA_CONSUMO));
            json.put("numeroCelular", String.valueOf(params.get("numeroCelular")));
            json.put("claveCamp", String.valueOf(params.get("claveCamp")));
            json.put("estatus", String.valueOf(params.get("estatus")));
            json.put("codigoOTP", String.valueOf(params.get("codigoOTP")));
            json.put("seguroObli", String.valueOf(params.get("seguroObli")));
            json.put("IUM", String.valueOf(params.get("IUM")));
            json.put("producto", Session.getInstance(SuiteApp.appContext).getCveCamp());


        return json;
    }

    /**
     * convierte los parametros de server a los parametro como lo ocupara server api
     * @param params
     * @return
     */
    public static Hashtable<String, String> guardarEliminarSimulacion(Hashtable<String, ?> params) {
        //CalculoData data = new CalculoData();

            Hashtable<String, String> json = new Hashtable<String, String>();
            //json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION));
            json.put(ServerConstantsCredit.CLIENTE_PARAM, String.valueOf(params.get(ServerConstantsCredit.CLIENTE_PARAM)));
            json.put(ServerConstantsCredit.IUM_PARAM, String.valueOf(params.get(ServerConstantsCredit.IUM_PARAM)));
            json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, String.valueOf(params.get(ServerConstantsCredit.NUMERO_CEL_PARAM)));
            json.put(ServerConstantsCredit.TIPO_OP_PARAM, String.valueOf(params.get(ServerConstantsCredit.TIPO_OP_PARAM)));

        return json;
    }
}
