package suitebancomer.aplicaciones.bbvacredit.gui.delegates;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.common.Session.State;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.MenuPrincipalActivity;
import suitebancomer.aplicaciones.bbvacredit.io.AuxConectionFactoryCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerResponseCredit;
import suitebancomer.aplicaciones.bbvacredit.models.CalculoData;
import suitebancomer.aplicaciones.bbvacredit.models.ConsultaAlternativasData;
import suitebancomer.aplicaciones.bbvacredit.models.CreditoContratado;
import suitebancomer.aplicaciones.bbvacredit.models.ObjetoCreditos;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class MenuPrincipalCreditDelegate  extends BaseDelegateOperacion {

	private Session session;

	private ConsultaAlternativasData data;

	private Boolean showCreditosOfertados = true;

	private Boolean showCreditosContratados = true;

	private Integer tipoOperacion;

	private MenuPrincipalActivity act;


	public Boolean getRegresar(){
		if(!getCreditsSim().isEmpty() || !getProductsSim().isEmpty()){
			session.setRegresar(true);
		}else{
			session.setRegresar(false);
		}
		return session.getRegresar();
	}

	public ConsultaAlternativasData getData() {
		return data;
	}

	public void setData(ConsultaAlternativasData data) {
		this.data = data;
	}

	public Boolean getShowCreditosContratados() {
		return showCreditosContratados;
	}

	public void setShowCreditosContratados(Boolean showCreditosContratados) {
		this.showCreditosContratados = showCreditosContratados;
	}

	public Boolean getShowCreditosOfertados() {
		return showCreditosOfertados;
	}

	public void setShowCreditosOfertados(Boolean showCreditosOfertados) {
		this.showCreditosOfertados = showCreditosOfertados;
	}

	/**
	 * Default constructor
	 */
	public MenuPrincipalCreditDelegate() {
		session = Session.getInstance(MainController.getInstance().getContext());
	}

	public void setDataFromCredit(ObjetoCreditos creditos) {
		data = new ConsultaAlternativasData(creditos.getEstado(), creditos.getMontotSol(), creditos.getPorcTotal(), creditos.getPagMTot(), null, creditos.getProductos(), creditos.getCreditos(), creditos.getCreditosContratados(), null, null);
	}

	public <T> void redirectToView(Class<T> c){
		MainController.getInstance().showScreen(c);
	}

	@Override
	protected String getCodigoOperacion() {
		// TODO Auto-generated method stub
		return Server.CONSULTA_ALTERNATIVAS;
	}

	public ArrayList<Producto> getCreditosOfertados(){
		return data!=null?data.getProductos():null;
	}

	public ArrayList<CreditoContratado> getCreditosContratados(){
		return data!=null?data.getCreditosContratados():null;
	}

	public void resetStates(){
		Iterator<Producto> it = session.getCreditos().getProductos().iterator();

		while(it.hasNext()){
			it.next().setIndSimBoolean(false);
		}

		Iterator<CreditoContratado> itC = session.getCreditos().getCreditosContratados().iterator();

		while(itC.hasNext()){
			itC.next().setIndicadorSim(false);
		}

		data.setProductos(session.getCreditos().getProductos());
		data.setCreditosContratados(session.getCreditos().getCreditosContratados());
	}

	public void saveStates(HashMap<Integer,State> states){
		session.setStateList(states);
	}

	public void resetContStates(){

		Iterator<CreditoContratado> itC = session.getCreditos().getCreditosContratados().iterator();

		while(itC.hasNext()){
			itC.next().setIndicadorSim(false);
		}

		data.setCreditosContratados(session.getCreditos().getCreditosContratados());
	}

	public HashMap<Integer,State> getStates(){
		return session.getStateList();
	}

	private Integer getNumeroOperacion(String op){
		Integer ret = 0;
		if(op.equals(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS_STR)){
			ret = ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS;
		}else if(op.equals(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS_STR)){
			ret = ConstantsCredit.OP_GUARDAR_ALTERNATIVAS;
		}else if(op.equals(ConstantsCredit.OP_ELIMINAR_STR)){
			ret = ConstantsCredit.OP_ELIMINAR;
		}

		return ret;
	}

	/**
	 * Peticiones
	 */

	private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}else{
			paramTable.put(cnt, "");
			Log.d(param.getClass().getName(), param.toString()+" empty or null");
		}
	}

	private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}
	}

	public void addProductId(Integer index, Boolean isLiquidacion){
		SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
		Editor editor = sp.edit();
		if(isLiquidacion){
			editor.putInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX_LIQ, index);

		}else{
			editor.putInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, index);

		}
		editor.commit();
	}

	public ArrayList<Producto> getProductsSim(){
		ArrayList<Producto> lista = new ArrayList<Producto>();
		if(data.getProductos() != null){
			Iterator<Producto> it = data.getProductos().iterator();

			while(it.hasNext()){
				Producto p = it.next();
				if(p.getIndSimBoolean()){
					lista.add(p);
				}
			}

		}
		return lista;
	}

	public ArrayList<CreditoContratado> getCreditsSim(){

		ArrayList<CreditoContratado> lista = new ArrayList<CreditoContratado>();
		if(data.getProductos() != null){
			Iterator<CreditoContratado> it = data.getCreditosContratados().iterator();

			while(it.hasNext()){
				CreditoContratado cr = it.next();
				if(cr.getIndicadorSim()){
					lista.add(cr);
				}
			}
		}
		return lista;
	}

	public void doEliminar(MenuPrincipalActivity act){
		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();

		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);

		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);

		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);

		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

		// Mapeamos el tipo de operacion -> 4
		addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);
		tipoOperacion = ConstantsCredit.OP_ELIMINAR;
		this.act = act;

		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
		this.doNetworkOperation(Server.CALCULO_OPERACION, paramTable2,true,new CalculoData(),MainController.getInstance().getContext());
	}

	public void doGuardar(MenuPrincipalActivity act){
		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();

		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);

		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);

		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);

		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

		// Mapeamos el tipo de operacion -> 3
		addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);
		tipoOperacion = ConstantsCredit.OP_GUARDAR_ALTERNATIVAS;
		this.act = act;
		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.calculo(paramTable);
		this.doNetworkOperation(Server.CALCULO_OPERACION, paramTable2,true,new CalculoData(),MainController.getInstance().getContext());
	}

	public void doCalculo(){

		Hashtable<String, String> paramTable = new Hashtable<String, String>();

		// Mapeamos el codigo de operacion
		addParametro(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);

		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);

		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);

		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

		//This line is used to call the new Activity.		

		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.consultaAlternativas(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2,true,new ConsultaAlternativasData(), MainController.getInstance().getContext());
	}

	public void changeContSimulated(Integer index,MenuPrincipalActivity act){
		if(session.getCreditos().getCreditosContratados() != null){
			if(session.getCreditos().getCreditosContratados().size() > index){
				session.getCreditos().getCreditosContratados().get(index).setIndicadorSim(false);
				data.getCreditosContratados().get(index).setIndicadorSim(false);
				act.checkStates();
				session.setVisibilityButton(act, R.id.pGlobalBottomMenuRC);
				session.setVisibilityButton(act, R.id.pGlobalBottomMenuGuardar);
			}
		}
	}

	public void changeOfertSimulated(Integer index,MenuPrincipalActivity act){
		if(session.getCreditos().getProductos() != null){
			if(session.getCreditos().getProductos().size() > index){
				session.getCreditos().getProductos().get(index).setIndSimBoolean(false);
				data.getProductos().get(index).setIndSimBoolean(false);
				act.checkStates();
				session.setVisibilityButton(act, R.id.pGlobalBottomMenuRC);
				session.setVisibilityButton(act, R.id.pGlobalBottomMenuGuardar);
			}
		}
	}

	private void setSim2False(){
		for (int i = 0; i < data.getProductos().size(); i++) {
			data.getProductos().get(i).setIndSimBoolean(Boolean.FALSE);
		}
	}

	private void setSim2FalseCContratados(){

		/**
		 * The line 330 was causing an "array out of bounds" problem, so it was
		 * changed by the line 331.
		 * Aril 8th, 2015.
		 * Modified by Omar Orozco Silverio. 
		 */
		//for (int i = 0; i < data.getProductos().size(); i++) {
		for (int i = 0; i < data.getCreditosContratados().size(); i++) {	
			data.getCreditosContratados().get(i).setIndicadorSim(Boolean.FALSE);
		}
	}


	public void analyzeResponse(String operationId, ServerResponseCredit response) {

		if(getCodigoOperacion().equals(operationId)){
			if((response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL)||(response.getStatus() == ServerResponseCredit.OPERATION_OPTIONAL_UPDATE)){
				data = (ConsultaAlternativasData) response.getResponse();

				if(data == null)
					Log.e("Error","No existe información");

				// Comprobamos que la lista de productos no sea vac�a
				if((data.getProductos() != null)&&(!data.getProductos().isEmpty())){
					// seteamos el indicadorSim a false de todo el objeto
					setSim2False();
				}else{
					// No se muestran
					showCreditosOfertados = false;
				}

				// Comprobamos que la lista de CredContratados no sea vac�a
				if((data.getCreditosContratados() != null)&&(!data.getCreditosContratados().isEmpty())){
					// seteamos el indicadorSim a false de todo el objeto
					setSim2FalseCContratados();
				}else{
					// No se muestran
					showCreditosContratados = false;
				}

				// Guradar en sesion parametros para mostrar creditos
				session.setShowCreditosContratados(showCreditosContratados);
				session.setShowCreditosOfertados(showCreditosOfertados);


				Log.e("estado",data.getEstado());
				if(data.getProductos()==null)
					Log.e("Productos","nulos");
				// Guardamos el objeto Creditos
				session.setCreditos(new ObjetoCreditos(data.getEstado(), data.getMontotSol(), data.getPorcTotal(), data.getPagMTot(), data.getProductos(), data.getCreditos(), data.getCreditosContratados()));

				// Desbloqueamos la pantalla
				//MainController.getInstance().ocultaIndicadorActividad();

				//if(showCreditosContratados && showCreditosOfertados){

				// This is Inception
				//MainController.getInstance().showScreen(MenuPrincipalActivity.class);
				if(!showCreditosOfertados && !showCreditosContratados)
				{
					//act.showInformationAlert("Alert");

					MainController.getInstance().getActivityController().getCurrentActivity().showInformationAlertPL(SuiteApp.appContext.getString(R.string.label_alert_information_noCredits));
					//MainController.getInstance().getActivityController().getCurrentActivity().showInformationAlert("Alert BAseViewCOntroller maincontroller");
                   //MainController.getInstance().getMenuSuiteController().parentManager.showMenuPrincipal();
           			
				}    


				
				else{
					if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session
							MainController.getInstance().getStartBmovilInBack().showMenuCredit();
					}else{
						MainController.getInstance().getMenuSuiteController().showMenuCredit();
					}
				}
				/*}else{
    				session.getActivityLogin().showInformationAlert(session.getActivityLogin().getString(R.string.label_alert_information_noCredits));
    				session.getActivityLogin().clearPass();
    			}*/

			}
			else if(response.getStatus() == ServerResponseCredit.OPERATION_ERROR){
				if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session
					MainController.getInstance().getStartBmovilInBack().habilitarVista();
				}else{
					MainController.getInstance().getMenuSuiteController().habilitarVista();
				}

			}
		}else if(Server.CALCULO_OPERACION.equals(operationId)){
			if((response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL)||(response.getStatus() == ServerResponseCredit.OPERATION_OPTIONAL_UPDATE)){
				CalculoData respuesta = (CalculoData) response.getResponse();


				// Informamos el obj creditos con la informaci�n necesaria
				session.getCreditos().setCreditos(respuesta.getCreditos());
				session.getCreditos().setEstado(respuesta.getEstado());
				session.getCreditos().setMontotSol(respuesta.getMontotSol());
				session.getCreditos().setPagMTot(respuesta.getPagMTot());
				session.getCreditos().setPorcTotal(respuesta.getPorcTotal());
				session.getCreditos().setProductos(respuesta.getProductos());


				data.setCreditos(respuesta.getCreditos());
				data.setEstado(respuesta.getEstado());
				data.setMontotSol(respuesta.getMontotSol());
				data.setPagMTot(respuesta.getPagMTot());
				data.setPorcTotal(respuesta.getPorcTotal());
				data.setProductos(respuesta.getProductos());

				// Si es eliminar reseteamos los estados de los contratados, los ofertados deben llegar del server bien
				if(tipoOperacion.equals(ConstantsCredit.OP_ELIMINAR)) resetStates(); //resetContStates();

				if(tipoOperacion.equals(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS)) act.showInformationAlert("Su simulaci�n se guard� con �xito");

				act.checkStates();
				session.setVisibilityButton(act, R.id.pGlobalBottomMenuRC);
				session.setVisibilityButton(act, R.id.pGlobalBottomMenuGuardar);
				// Desbloqueamos la pantalla
				MainController.getInstance().ocultaIndicadorActividad();
			}

		}
	}

	public void setSessionByBmovil(String userNumber,String password,String ium, String user,String mail)
	{
		session.setIdUsuario(userNumber);
		session.setPassword(password);
		session.setIum(ium);
		Log.e("Número Celular",user);
		session.setNumCelular(user);
		session.setEmail(mail);
	}


}
