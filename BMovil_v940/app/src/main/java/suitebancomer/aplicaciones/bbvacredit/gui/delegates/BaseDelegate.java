package suitebancomer.aplicaciones.bbvacredit.gui.delegates;

import android.content.Context;

import java.util.Hashtable;

import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.io.ServerConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerResponseCredit;

public class BaseDelegate implements OperationNetworkCallback {

	/**
	 * Invoke a network operation, controlling that it is called after all previous
	 * repaint events have been completed
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 */
	protected void doNetworkOperation(final String operationId, final Hashtable<String,?> params) {
		MainController.getInstance().invokeNetworkOperation(operationId, params, this, "operacion", "conectando");
	}

	/**
	 * Metodo que llamara el mainController (metodo que llamara el server api)
	 * @param operationId
	 * @param params
	 * @param isJson
	 * @param hadler
	 * @param contex
	 */
	protected void doNetworkOperation(final String operationId, final Hashtable<String,?> params,final Boolean isJson,final Object hadler,final Context contex) {
		MainController.getInstance().invokeNetworkOperation(operationId, params, this, "operacion", "conectando", isJson, hadler, contex);
	}
	
	@Override
	public void analyzeResponse(String operationId, ServerResponseCredit response) {
		// TODO Auto-generated method stub	
	}
    
    /**
     * Calcula los parametros comunes de envio al servidor para todas las
     * operaciones.
     *
     * @param idOperation el identificador de la operacion
     * @return un mapa con los parametros definidos
     */
    @SuppressWarnings("rawtypes")
	protected Hashtable getOperationContext(String idOperation) {
        // Se definen los par�metros a enviar al servidor
        Hashtable paramTable = new Hashtable();

        return paramTable;
    }
    
    /**
     * Calcula los parametros comunes de envio al servidor para todas las
     * operaciones.
     *
     * @param idOperation el identificador de la operacion
     * @return un mapa con los parametros definidos
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	protected Hashtable getJSONOperationContext(String idOperation) {
    	Hashtable paramTable = new Hashtable();
        
    	paramTable.put(ServerConstantsCredit.JSON_ID_OPERACION_ETIQUETA, idOperation);
        paramTable.put(ServerConstantsCredit.JSON_ID_USUARIO_ETIQUETA, Session.getInstance().getIdUsuario());

        return paramTable;
    }
}
