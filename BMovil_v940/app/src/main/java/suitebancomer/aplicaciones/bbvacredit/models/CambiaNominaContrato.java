package suitebancomer.aplicaciones.bbvacredit.models;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONException;

import java.io.IOException;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.model.ExternalBank;

/**
 * Created by eluna on 28/06/2016.
 */
public class CambiaNominaContrato implements ParsingHandler
{

    private String txtHTML1;
    public String code;
    public String description;
    public String status;
    public String errorCode;

    private static CambiaNominaContrato ourInstance = new CambiaNominaContrato();

    public static CambiaNominaContrato getInstance() {
        return ourInstance;
    }

    public CambiaNominaContrato() {
    }

    public String getTxtHTML1() {
        return txtHTML1;
    }

    public void setTxtHTML1(String txtHTML1) {
        this.txtHTML1 = txtHTML1;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException
    {

        try {
            txtHTML1= parser.parserNextObject("response").getString("txtHTML1");


            //errorCode = parser.parseNextValue("errorCode");
        } catch (JSONException e) {
            // e.printStackTrace();
        }finally {
            description =parser.parseNextValue("description");
            errorCode = parser.parseNextValue("errorCode");
            status = parser.parseNextValue("status");
        }
    }
}
