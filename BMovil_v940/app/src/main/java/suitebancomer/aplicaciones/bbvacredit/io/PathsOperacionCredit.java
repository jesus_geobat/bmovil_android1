package suitebancomer.aplicaciones.bbvacredit.io;

public class PathsOperacionCredit {
private String[] paths = new String[ServerConstantsCredit.NUM_PROVEEDORES];
    
    public PathsOperacionCredit(){
    }
    
    public PathsOperacionCredit(String[] paths){
        this.paths = paths;
    }
    
    public PathsOperacionCredit(String pathProveedor1, String pathProveedor2, String pathProveedor3){
        this.paths[0] = pathProveedor1;
        this.paths[1] = pathProveedor2;
        this.paths[2] = pathProveedor3;
    }
    
    public String getPathProveedor(int provider){
        return this.paths[provider];
    }
    
    public void setPathProveedor(int provider, String path){
        this.paths[provider] = path;
    }

    public String[] getPaths() {
        return paths;
    }

    public void setPaths(String[] paths) {
        this.paths = paths;
    }
}
