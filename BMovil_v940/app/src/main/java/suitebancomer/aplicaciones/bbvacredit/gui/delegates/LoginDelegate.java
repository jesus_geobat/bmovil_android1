package suitebancomer.aplicaciones.bbvacredit.gui.delegates;

import java.util.HashMap;
import java.util.Hashtable;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.LoginActivity;
import suitebancomer.aplicaciones.bbvacredit.io.ServerConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerResponseCredit;
import suitebancomer.aplicaciones.bbvacredit.models.LoginData;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.os.Build;
import android.widget.EditText;

public class LoginDelegate extends BaseDelegateOperacion {
	
	private String user;
	private String codedPass;
	private String ium;
	private Session sesion;
	
	private LoginActivity act;
	
	/**
	 * Default constructor
	 */
	public LoginDelegate() {
	}
	
	@Override
	protected String getCodigoOperacion() {
		return Server.LOGIN_OPERACION;
	}
	
	public void realizarLogin(String user, String pass, LoginActivity act) {

		this.act = act;
		// Seteamos las credenciales en la misma clase
		//setUser(user);
		setCodedPass(pass);
		
		doLogin();
    }
	
	private void doLogin(){

		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		// NT
		paramTable.put(ServerConstantsCredit.USERNAME_PARAM, this.getUser());
		// NP
		paramTable.put(ServerConstantsCredit.PASSWORD_PARAM, this.getCodedPass());
		
		// IU - Generamos el IUM
		//this.setIum(Tools.buildIUM(this.getUser(), System.currentTimeMillis(), MainController.getInstance().getContext()));
		//paramTable.put(ServerConstants.IUM_ETIQUETA, ium);
		paramTable.put(ServerConstantsCredit.IUM_ETIQUETA, ConstantsCredit.IUM_ESTATICO);
		
		// MM - Generacion del modelo
		String marcaModelo = Build.BRAND + Build.MODEL;
		paramTable.put(ServerConstantsCredit.MARCA_MODELO, marcaModelo);
		
		// Get valores cat�logos de sesi�n, sino inicializa a cero
		sesion = Session.getInstance(MainController.getInstance().getContext());
		HashMap<String,String> catalogVersions = sesion.getCatalogVersions();

		if (catalogVersions != null) {
		
			paramTable.put(ServerConstantsCredit.VERSION_C1, catalogVersions.get(ServerConstantsCredit.VERSION_C1));
			paramTable.put(ServerConstantsCredit.VERSION_C4, catalogVersions.get(ServerConstantsCredit.VERSION_C4));
			paramTable.put(ServerConstantsCredit.VERSION_C5, catalogVersions.get(ServerConstantsCredit.VERSION_C5));
			paramTable.put(ServerConstantsCredit.VERSION_C8, catalogVersions.get(ServerConstantsCredit.VERSION_C8));
			paramTable.put(ServerConstantsCredit.VERSION_TA, catalogVersions.get(ServerConstantsCredit.VERSION_TA));
			paramTable.put(ServerConstantsCredit.VERSION_DM, catalogVersions.get(ServerConstantsCredit.VERSION_DM));
			paramTable.put(ServerConstantsCredit.VERSION_SV, catalogVersions.get(ServerConstantsCredit.VERSION_SV));
			paramTable.put(ServerConstantsCredit.VERSION_MS, catalogVersions.get(ServerConstantsCredit.VERSION_MS));
		}		
		
		//TODO: delete, solo para test
		String changeMe = "910";
		paramTable.put(ServerConstantsCredit.VERSION_VM, changeMe);
		
    	this.doNetworkOperation(getCodigoOperacion(), paramTable);
	}
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getCodedPass() {
		return codedPass;
	}

	public void setCodedPass(String pass) {
		this.codedPass = pass;//Tools.buildMD5Pass(getUser(), pass);
	}

	public String getIum() {
		return ium;
	}

	public void setIum(String ium) {
		this.ium = ium;
	}

	/**
     * Calcula los parametros propios de envio al servidor para esta operacion
     * en concreto.
     *
     * @param idOperation el identificador de la operacion
     * @return un mapa con los parametros definidos
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Hashtable<String, ?> getJSONOperationContext(String idOperation) {
        Hashtable paramTable = super.getJSONOperationContext(idOperation);
        paramTable.put(ServerConstantsCredit.JSON_CONTRASENA_ETIQUETA, getCodedPass());
        
        return paramTable;
    }
    
    public void analyzeResponse(String operationId, ServerResponseCredit response) {
    	
    	// TODO: mapear correctamente
    	if(Server.LOGIN_OPERACION.equals(operationId)){
    		if((response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL)||(response.getStatus() == ServerResponseCredit.OPERATION_OPTIONAL_UPDATE)){
    			LoginData data = (LoginData) response.getResponse();
    			
    			// Mapeamos Sesion
    			sesion.setIdUsuario(data.getClientNumber());
    			sesion.setPassword(codedPass);
    			sesion.setIum(getIum());
    			sesion.setNumCelular(getUser());
    			sesion.setEmail(data.getEmailCliente());
    			
    			
    			//sesion.setActivityLogin(act);
    			
    			// Llamamos al delegate de Menu principal
    			// Este delegate carga en session un obj creditos
    			//MenuPrincipalCreditDelegate delegateMP = new MenuPrincipalCreditDelegate();
    			//delegateMP.doCalculo();
    		}else{
        		MainController.getInstance().ocultaIndicadorActividad();
        	}
    	}
    }
    
    /**
	 * Establece el usuario y desactiva el campo si ya existe en sesi�n, si no
	 * lo activa
	 * 
	 * @param mUserEdit
	 *            el campo de texto usuario
	 */
	public void establecerUsuario(EditText mUserEdit) {
		// Fix para maqueta
		mUserEdit.setText("");
		mUserEdit.setEnabled(false);
		mUserEdit.setFocusable(false);
		mUserEdit.setFocusableInTouchMode(false);
		mUserEdit.setClickable(false);
		String user = ConstantsCredit.CELULAR;
		mUserEdit.setText(Tools.hideUsername(user));
		this.setUser(user);
	}
}
