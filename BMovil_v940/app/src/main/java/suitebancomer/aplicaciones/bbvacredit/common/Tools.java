package suitebancomer.aplicaciones.bbvacredit.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.util.encoders.Hex;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bancomer.mbanking.R;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.models.ConsumoCredit;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.LinearLayout;


public class Tools {

	public final static int TAM_FRAGMENTO_TEXTO = 1000;

	/**
	 * Builds the MD5 code for the user.
	 * @param username the username
	 * @param pass the user password
	 * @return the MD5 code for the user
	 */
	public static String buildMD5Pass(String username, String pass) {

		StringBuffer sb;

		sb = new StringBuffer(username);
		sb.append(pass);

		String input = sb.toString();

		Digest  digest = new MD5Digest();
		byte[]  resBuf = new byte[digest.getDigestSize()];
		byte[]  bytes = input.getBytes();
		digest.update(bytes, 0, bytes.length);
		digest.doFinal(resBuf, 0);

		String output = new String(Hex.encode(resBuf)).toUpperCase();

		return output;

	}

	//BBVA CREDIT INTEGRATION

	/** Modificacion 50986
	 * Los siguientes metodos ya no son necesarios ya que en la respuesta de DetalleAlternativa
	 * ya vienen estipulados los datos que se obtenian con estos metodos.
	 */

    /*
	public static String formatterForBmovil(String importe)
	{
		StringBuffer buffer = new StringBuffer();
		boolean flag=false;
		int counter = -1;
		for(int i=0;i<importe.length();i++)
		{
			if(importe.charAt(i)!='.')	
				buffer.append(importe.charAt(i));
			else
				flag=true;
			if(flag)
				counter++;
		}
		switch(counter)
		{
		case -1: buffer.append("00");
		break;
		case 1:	buffer.append("0");
		break;
		}
		return buffer.toString();
	}
    */


	//public static ArrayList<ConsumoCredit> getJsonCreditParsed()
	//{

		/**
		 * Created: April 22th, 2015.
		 * Author: Omar Orozco Silverio.
		 * 
		 * This was the original Json for Consumo Task but it has no "CveSubp" value which matches 
		 * with CveSubP value from DetalleArlternativa Response.

    	  final String JsonConsumo = "{\"Consumo\":["
    			+ "{\"producto\":\"PBCCMNOM02\",\"CveSubp\":\"CN61\",\"pagoMilS\":\"11.88\"},"
    			+ "{\"producto\":\"PBCCMNOM01\",\"CveSubp\":\"CU87\",\"pagoMilS\":\"16.24\"},"
    			+ "{\"producto\":\"PBCCMPPI02\",\"CveSubp\":\"CP95\",\"pagoMilS\":\"11.68\"},"
    			+ "{\"producto\":\"PBCCMPPI02\",\"CveSubp\":\"CP45\",\"pagoMilS\":\"16.24\"}"
    			+ "]}";

		 */

        /*
		final String JsonConsumo = "{\"Consumo\":["
				+ "{\"producto\":\"PBCCMNOM02\",\"CveSubp\":\"CN61\",\"pagoMilS\":\"11.88\"},"
				+ "{\"producto\":\"PBCCMNOM01\",\"CveSubp\":\"CU86\",\"pagoMilS\":\"16.24\"},"
				+ "{\"producto\":\"PBCCMPPI02\",\"CveSubp\":\"CP95\",\"pagoMilS\":\"11.68\"},"
				+ "{\"producto\":\"PBCCMPPI02\",\"CveSubp\":\"CP45\",\"pagoMilS\":\"16.24\"}"
				+ "]}";

		try {
			JSONObject mainJson = new JSONObject(JsonConsumo);
			JSONArray productos = mainJson.getJSONArray(ConstantsCredit.JSON_CONSUMO_TAG);
			ArrayList<ConsumoCredit> productsList = new ArrayList<ConsumoCredit>();


			ConsumoCredit auxInstance;
			JSONObject auxJson;


			for(int i=0;i<productos.length();i++)
			{
				auxInstance = new ConsumoCredit();
				auxJson = productos.getJSONObject(i);

				auxInstance.setCveSubP(auxJson.getString(ConstantsCredit.CVESUBP));
				auxInstance.setPagMilS(auxJson.getString(ConstantsCredit.PAGOMILS));
				auxInstance.setProducto(auxJson.getString(ConstantsCredit.PRODUCTO));

				productsList.add(auxInstance);
			}

			return productsList;

		} catch (JSONException e) {			
			Log.e("Problem caused when Creating Json, reason:\n",e.getLocalizedMessage());
		}


		return null;
	}
    */
	/*
	public static String getImporteDelSeguro(String pagoMilS, String MonDeseS)
	{
		// Created: April 22th, 2015. Author:OOS.
		return String.valueOf(Float.valueOf(pagoMilS)*Float.valueOf(MonDeseS)/1000);
	}
	*/


    /*
	public static String getTotalPagos(String desPlazoE)
	{
		// Created: April 22th, 2015. Author:OOS.
		StringBuffer buffer = new StringBuffer();

		loop : for(int i=0;i<desPlazoE.length();i++)
		{
			switch(desPlazoE.charAt(i))
			{
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				buffer.append(desPlazoE.charAt(i));
				break;
			default :
				break loop;
			}
		}
		return buffer.toString();
	}
	*/
	//END REGION


	/**
	 * Mask the username (the telephone number), with a mask character and only.
	 * letting visible a certain number of digits
	 * @param username the telephone number
	 * @return a text containing the fist digits of the number masked with a
	 * character, and the last digits in clear
	 */
	public static String hideUsername(String username) {
		String result = "";
		if ((username != null) && (username.length() > ConstantsCredit.VISIBLE_NUMBER_CHARCOUNT)) {
			StringBuffer sb = new StringBuffer();
			// mask the first characters of the username with "*" and leave visible the
			// VISIBLE_USERNAME_CHARCOUNT last characters
			for (int i = 0; i < username.length() - ConstantsCredit.VISIBLE_NUMBER_CHARCOUNT; i++) {
				sb.append("*");
			}
			sb.append(username.substring(username.length() - ConstantsCredit.VISIBLE_NUMBER_CHARCOUNT));
			result = sb.toString();
		}
		return result;
	}

	/**
	 * Hide part of the account number, showing the first digits of the account as
	 * an asterisk (*), and only letting visible a certain number of digits. Example:
	 * Account 12345678901234567890 would turn into *7890
	 * @param account the account number
	 * @return a string with the masked account number
	 */
	public static String hideAccountNumber(String account) {
		String result = "";
		if ((account != null) && (account.length() > ConstantsCredit.VISIBLE_NUMBER_ACCOUNT)) {
			StringBuffer sb = new StringBuffer("*");
			sb.append(account.substring(account.length() - ConstantsCredit.VISIBLE_NUMBER_ACCOUNT));
			result = sb.toString();
		} else {
			if (account.length() == ConstantsCredit.VISIBLE_NUMBER_ACCOUNT) {
				result = account;
			}
		}

		return result;
	}

	/**
	 * Determines if a string is empty or null
	 * @param text the string object to evaluate
	 * @return true if the string object has no real value
	 */
	public static boolean isEmptyOrNull(String text){
		return text==null || text.trim().length() == 0;
	}

	/**
	 * 
	 * Parsea una fecha a Date dado un formato
	 * 
	 * @param inDate
	 * @param format
	 * @return
	 * @throws ParseException
	 */

	public static Date getDate(String inDate, String format) throws ParseException {
		SimpleDateFormat s = new SimpleDateFormat(format);
		return s.parse(inDate);
	}

	public static String getFormatedDate(Date inDate, String format) throws ParseException {
		SimpleDateFormat s = new SimpleDateFormat(format);
		return s.format(inDate);
	}

	public static void trazaTexto(String titulo, String mensaje) {
		Log.d(titulo,"");
		for (int i = 0; i < (mensaje.length() / TAM_FRAGMENTO_TEXTO) + 1; i++) {
			Log.d("", mensaje.substring(i*TAM_FRAGMENTO_TEXTO, Math.min((i+1)*TAM_FRAGMENTO_TEXTO, mensaje.length())));
		}
	}

	/**
	 * Build the IUM (unique identifier of the installation).
	 * @param username the username
	 * @param seed the random seed
	 * @param applicationContext application context to retrieve phone data
	 * @return the IUM as a hexadecimal string
	 */
	public static String buildIUM(String username, long seed, Context applicationContext) {

		StringBuffer sb;

		if(username != null)
			sb = new StringBuffer(username);
		else
			sb = new StringBuffer();

		sb.append(seed);
		String imei = getImei(applicationContext);
		if (imei != null) {
			sb.append(imei);
		}
		String imsi = getImsi(applicationContext);
		if (imsi != null) {
			sb.append(imsi);
		}

		String props = getSystemProperties();
		if (props != null) {
			sb.append(props);
		}

		String input = sb.toString();

		Digest  digest = new MD5Digest();
		byte[]  resBuf = new byte[digest.getDigestSize()];
		byte[]  bytes = input.getBytes();
		digest.update(bytes, 0, bytes.length);
		digest.doFinal(resBuf, 0);

		String output = new String(Hex.encode(resBuf)).toUpperCase();
		//String output ="221F9C291CBA91A48BFECAD4BBDB9534";


		return output;

	}

	/**
	 * Try to obtain the IMEI from the telephone.
	 * @param applicationContext application context to retrieve phone data
	 * @return the IMEI from the telephone, or null if it cannot be obtained
	 */
	private static String getImei(Context applicationContext) {
		TelephonyManager telephonyManager =
				(TelephonyManager) applicationContext.getSystemService(Context.TELEPHONY_SERVICE);
		String imei = telephonyManager.getDeviceId();
		if (TextUtils.isEmpty(imei)) {
			imei = "";
		}
		return imei;
	}

	// TODO: PENDING, Must check in real device -- folvera
	/**
	 * Try to obtain the IMSI from the telephone SIM card.
	 * @param applicationContext application context to retrieve phone data
	 * @return the IMSI from the telephone SIM card, or null if it cannot be obtained
	 */
	private static String getImsi(Context applicationContext)  {
		TelephonyManager telephonyManager =
				(TelephonyManager) applicationContext.getSystemService(Context.TELEPHONY_SERVICE);
		String imsi = telephonyManager.getSubscriberId();
		if (imsi != null) {
			return "misdn:" + imsi;
		} else {
			return null;
		}
	}

	/**
	 * .
	 * @return SOMETHING.
	 */
	private static String getSystemProperties() {
		StringBuffer result = new StringBuffer();
		try {
			result.append(System.getProperty("java.runtime.name"));
		} catch (Throwable th) { };
		try {
			result.append(System.getProperty("os.version"));
		} catch (Throwable th) { };
		try {
			result.append(System.getProperty("java.vm.name"));
		} catch (Throwable th) { };
		try {
			result.append(System.getProperty("java.runtime.version"));
		} catch (Throwable th) { };
		try {
			result.append(System.getProperty("java.vm.version"));
		} catch (Throwable th) { };


		System.out.println("System Properties = " + result.toString());

		return result.toString();
	}

	/**
	 * Si es un campo del json, devuelve el parseo a la clase, sino se devuelve null
	 * 
	 * @param json
	 * @param campo
	 * @param clase
	 * @return
	 * @throws JSONException
	 */
	public static <T> T getJSONChecked(JSONObject json, String campo, Class<T> clase) throws JSONException{
		if(json.has(campo)){
			T ret = null;
			if(clase.equals(String.class)){
				// String
				ret = (T) json.getString(campo);
			}else if(clase.equals(Double.class)){
				// Double
				ret = (T) Double.valueOf(json.getDouble(campo));
			}else if(clase.equals(Integer.class)){
				// Integer
				ret = (T) Integer.valueOf(json.getInt(campo));
			}else if(clase.equals(JSONArray.class)){
				// JSONArray
				ret = (T) json.getJSONArray(campo);
			}else if(clase.equals(Boolean.class)){
				// Boolean
				ret = (T) Boolean.valueOf(json.getBoolean(campo));
			}

			return ret;
		}else{
			if(clase.equals(Boolean.class)){
				// Boolean
				return (T) Boolean.valueOf(false);
			}else{
				return null;
			}
		}
	}

	/**
	 * Return the current session
	 * @return
	 */
	public static Session getCurrentSession(){
		return Session.getInstance(MainController.getInstance().getContext());
	}

	public static void setIsContratacionPreference(Boolean isContratacion){
		SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
		Editor editor = sp.edit();
		editor.putBoolean(ConstantsCredit.SHARED_IS_CONTRATACION, isContratacion);
		editor.commit();
	}

	public static Boolean getIsContratacionPreference(){
		SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
		return sp.getBoolean(ConstantsCredit.SHARED_IS_CONTRATACION, false);
	}


	//agm BBVA Credit
	public static float getPosicionBarraInferiorS(ImageButton button)
	{


		float h=button.getHeight();
		float y=button.getY();
		float s=(float) ((h*1.5)+y);

		Log.d("posicicion"," entero  "+s);
		return s;
	

	}
	
	
	public static float getPosicionBarraInferiorB(LinearLayout button)
	{
		float h=button.getHeight();
		float y=button.getY();
		float b=y+h;
		return b;
	}
}
