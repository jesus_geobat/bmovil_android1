package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ContactoPatrimonialViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.DetallePosicionPatrimonial;
import suitebancomer.classes.gui.delegates.BaseDelegate;

/**
 * Created by OOROZCO on 4/15/16.
 */
public class ContactoPatrimonialDelegate extends BaseDelegate {

    public static final long CONTACTO_PATRIMONIAL_DELEGATE = 631453334566764321L;

    private DetallePosicionPatrimonial detalle;

    public void setDetalle(DetallePosicionPatrimonial detalle) {
        this.detalle = detalle;
    }

    private ContactoPatrimonialViewController controller;


    public void setController(ContactoPatrimonialViewController controller) {
        this.controller = controller;
    }

    public ContactoPatrimonialDelegate(DetallePosicionPatrimonial detalle)
    {
        this.detalle = detalle;
    }

    public DetallePosicionPatrimonial getDetalle() {
        return detalle;
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)
        {
            if(controller.getDialogContratos().isShowing())
                controller.getDialogContratos().dismiss();

            this.detalle = (DetallePosicionPatrimonial) response.getResponse();
            controller.setDataToView();
        }
    }
}
