package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MovimientosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.MovimientoTransito;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import tracking.TrackingHelper;

/**
 * Created by ana_mezquitillo on 18/02/16.
 */
public class DetalleMovTransitoViewController  extends BaseViewController implements View.OnClickListener {

    ImageButton btnMenu;
    //AMZ
    public BmovilViewsController parentManager;
    //AMZ

    TextView fecha;
    TextView nomComercio;
    TextView descripcion;
    TextView importe;
    TextView hora;
    TextView numAutorizacion;
    TextView situacion;

    MovimientoTransito movimientoSeleccionado;;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_detalle_mov_transito);
        setTitle(R.string.consultar_movimientos_title, R.drawable.bmovil_consultar_icono);
        //AMZ
        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

        TrackingHelper.trackState("detalle mov", parentManager.estados);


        SuiteApp suiteApp = (SuiteApp)getApplication();
        setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());

        fecha = (TextView) findViewById(R.id.detalle_transito_fecha);
        descripcion = (TextView) findViewById(R.id.detalle_transito_descripcion);
        importe = (TextView) findViewById(R.id.detalle_transito_importe);
        nomComercio = (TextView) findViewById(R.id.detalle_transito_nombre_comercio);
        hora = (TextView) findViewById(R.id.detalle_transito_hora);
        numAutorizacion = (TextView) findViewById(R.id.detalle_transito_numAutorizacion);
        situacion = (TextView) findViewById(R.id.detalle_transito_situacion);

        btnMenu = (ImageButton) findViewById(R.id.detalle_transito_boton_menu);
        btnMenu.setOnClickListener(this);

        movimientoSeleccionado = ((MovimientosDelegate)parentViewsController.getBaseDelegateForKey(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID)).getMovTransito();
        scaleForCurrentScreen();
        llenaDetalleDescripcion();
    }

    public void llenaDetalleDescripcion(){
        fecha.setText(Tools.formatDate(movimientoSeleccionado.getFecha()));
        descripcion.setText(movimientoSeleccionado.getDescripcion());
        importe.setText(movimientoSeleccionado.getImporte());
        nomComercio.setText(movimientoSeleccionado.getNombreComercio());
        hora.setText(movimientoSeleccionado.getHora());
        numAutorizacion.setText(movimientoSeleccionado.getNumeroAutorizacion());
        situacion.setText(movimientoSeleccionado.getSituacion());

    }

    @Override
    public void onClick(View v) {
        if (v == btnMenu) {
            ((BmovilViewsController)parentViewsController).touchMenu();

            parentViewsController.removeDelegateFromHashMap(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID);
            ((BmovilViewsController)parentViewsController).showMenuPrincipal(true);
            parentViewsController.removeDelegateFromHashMap(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    private void scaleForCurrentScreen() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.layoutRoot));
        guiTools.scale(findViewById(R.id.detalle_movimientos_transito_title), true);
        guiTools.scale(findViewById(R.id.detalle_transito_nombre_comercio), true);
        guiTools.scale(findViewById(R.id.detalle_transito_descripcion), true);
        guiTools.scale(findViewById(R.id.detalle_transito_fecha), true);
        guiTools.scale(findViewById(R.id.detalle_transito_hora), true);
        guiTools.scale(findViewById(R.id.detalle_transito_importe), true);
        guiTools.scale(findViewById(R.id.detalle_transito_numAutorizacion), true);
        guiTools.scale(findViewById(R.id.detalle_transito_situacion), true);
        guiTools.scale(btnMenu);

    }

}
