package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Services payment destination custom delegate for confirmation and result views
 */
public class PagoServicio extends FrecuenteMulticanalData implements ParsingHandler {

	private Account cuentaOrigen;
	private String convenio;
	private String servicio;
	private String referencia;
	private String concepto;
	private String importe;
	//private String aliasFrecuente;
	//frecuente correo electronico
	private String aliasFrecuente= "";
	private String correoFrecuente = "";
	private String tipoOperacion;
	private boolean isPreregistered;
	
	
	public PagoServicio() {
		reset();
	}
	
	public PagoServicio(Account cuentaOrigen, String convenio, String servicio, String referencia, String concepto,
						String importe, String aliasFrecuente, String tipoOperacion, String usuario) {
		this.cuentaOrigen = cuentaOrigen;
		this.convenio = convenio;
		this.servicio = servicio;
		this.referencia = referencia;
		this.concepto = concepto;
		this.importe = importe;
		this.aliasFrecuente = aliasFrecuente;
		this.tipoOperacion = tipoOperacion;
		
		this.setUsuario(usuario);
	}
	
	public Account getCuentaOrigen() {
		return cuentaOrigen;
	}
	public void setCuentaOrigen(Account cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}
	public String getConvenioServicio() {
		return convenio;
	}
	public void setConvenioServicio(String convenioServicio) {
		this.convenio = convenioServicio;
	}
	public String getReferenciaServicio() {
		return referencia;
	}
	public void setReferenciaServicio(String referenciaServicio) {
		this.referencia = referenciaServicio;
	}
	public String getConceptoServicio() {
		return concepto;
	}
	public void setConceptoServicio(String conceptoServicio) {
		this.concepto = conceptoServicio;
	}
	public String getImporte() {
		return importe;
	}
	public boolean isPreregistered() {
		return isPreregistered;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public void setPreregistered(boolean preregistered) {
		this.isPreregistered = preregistered;
	}
	
	public String getAliasFrecuente() {
		return aliasFrecuente;
	}

	//frecuente correo electronico
	@Override
	public String getCorreoFrecuente() {
		return correoFrecuente;
	}
	@Override
	public void setCorreoFrecuente(String correoFrecuente) {
		this.correoFrecuente = correoFrecuente;
	}

	public void reset() {
		convenio = "";
		servicio = "";
		referencia = "";
		importe = "";
		aliasFrecuente = "";
		tipoOperacion = "";
		convenio = "";
	}
	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		referencia = parser.parseNextValue("RF");
    	concepto = parser.parseNextValue("CP", false);
    	importe = parser.parseNextValue("IM", false);
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}
}
