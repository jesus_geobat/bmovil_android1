package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.AcercaDeViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuAdministrarViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ActualizarCuentasResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTerminosDeUsoData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultarLimitesResult;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class MenuAdministrarDelegate extends DelegateBaseAutenticacion {
	public final static long MENU_ADMINISTRAR_DELEGATE_ID = 0x4ed434c61ca109f1L;

	MenuAdministrarViewController menuAdministrarViewController;

	public MenuAdministrarViewController getMenuAdministrarViewController() {
		return menuAdministrarViewController;
	}

	public void setMenuAdministrarViewController(
			MenuAdministrarViewController menuAdministrarViewController) {
		this.menuAdministrarViewController = menuAdministrarViewController;
	}

	/**
	 * AcercaDe...
	 * 
	 * */
	AcercaDeViewController acercaDeViewController;

	public AcercaDeViewController getAcercaDeViewController() {
		return acercaDeViewController;
	}

	public void setAcercaDeViewController(
			AcercaDeViewController acercaDeViewController) {
		this.acercaDeViewController = acercaDeViewController;
	}

	/**
	 * Se encarga de inicializar las etiquetas con la info conrrespondiente
	 */
	public void inicializaAcercaDe() {
		String version = SuiteApp.appContext
				.getString(R.string.administrar_acercade_version)
				+ " "
				+ Tools.getDoubleAmountFromServerString(BmovilConstants.APPLICATION_VERSION);
		String identificador = SuiteApp.appContext
				.getString(R.string.administrar_acercade_identificador)
				+ " "
				+ SuiteApp.appContext.getString(R.string.app_identifier);
		String terminal = SuiteApp.appContext
				.getString(R.string.administrar_acercade_terminal)
				+ " "
				+ Tools.getDeviceForAcercaDe();
		acercaDeViewController.getLblVersion().setText(version);
		acercaDeViewController.getLblIdentificador().setText(identificador);
		acercaDeViewController.getLblTerminal().setText(terminal);
	}

	/**
	 * MenuAdministrar
	 */

	@Override
	public void performAction(Object obj) {

		if (obj instanceof String) {
			String opcionSeleccionada = (String) obj;
			menuAdministrarViewController
					.opcionSeleccionada(opcionSeleccionada);
		}
	}

	/**
	 * 
	 * @return lista de opciones del menu de administrar
	 */
	public ArrayList<Object> getDatosMenu() {
		ArrayList<Object> lista = new ArrayList<Object>();
		ArrayList<Object> registros;
		registros = new ArrayList<Object>(2);
		registros.add(BmovilConstants.MADMINISTRAR_CAMBIAR_CONTRASENA);
		registros
				.add(SuiteApp.appContext
						.getString(R.string.administrar_menu_cambiar_contrasena_titulo));
		lista.add(registros);
		Session session = Session.getInstance(SuiteApp.getInstance());
		if (configurarMontos()) {
			if (Autenticacion.getInstance().mostrarOperacion(
					Constants.Operacion.cambioLimites,
					session.getClientProfile())) {
				registros = new ArrayList<Object>(2);
				registros.add(BmovilConstants.MADMINISTRAR_CONFIGURAR_MONTOS);
				registros
						.add(SuiteApp.appContext
								.getString(R.string.administrar_menu_configurarmontos_titulo));
				lista.add(registros);
			}
		}else{
			if (Autenticacion.getInstance().mostrarOperacion(
					Constants.Operacion.cambioLimites,
					session.getClientProfile())) {
				registros = new ArrayList<Object>(2);
				registros.add(BmovilConstants.MADMINISTRAR_CONFIGURAR_MONTOS);
				registros
						.add(SuiteApp.appContext
								.getString(R.string.administrar_menu_configurarmontos_bas_rec_titulo));
				lista.add(registros);
			}
		}

		// if (configurarAlertas()) {
		// registros = new ArrayList<Object>(2);
		// registros.add(Constants.MADMINISTRAR_CONFIGURAR_ALERTAS);
		// registros.add(SuiteApp.appContext.getString(R.string.administrar_menu_configuraralertas_titulo));
		// lista.add(registros);
		// }
		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.configurarCorreo,
				session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(BmovilConstants.MADMINISTRAR_CONFIGURAR_CORREO);
			registros
					.add(SuiteApp.appContext
							.getString(R.string.administrar_menu_configurarcorreo_titulo));
			lista.add(registros);
		}

		registros = new ArrayList<Object>(2);
		registros.add(BmovilConstants.MADMINISTRAR_ACTUALIZAR_CUENTAS);
		registros.add(SuiteApp.appContext
				.getString(R.string.administrar_menu_actualizarcuentas_titulo));
		lista.add(registros);
		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.cambioTelefono, session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(BmovilConstants.MADMINISTRAR_CAMBIO_TELEFONO);
			registros
					.add(SuiteApp.appContext
							.getString(R.string.administrar_menu_cambiotelefono_titulo));
			lista.add(registros);

		}

		// Incidencia #21907
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext)
				.getClientProfile();
		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.cambioPerfil, session.getClientProfile())) {
			if (profile == Constants.Perfil.avanzado) {
				registros = new ArrayList<Object>(2);
				registros.add(BmovilConstants.MADMINISTRAR_OPERAR_SIN_TOKEN);
				registros
						.add(SuiteApp.appContext
								.getString(R.string.administrar_menu_operar_sin_token_titulo));
				lista.add(registros);

			} else if (profile == Constants.Perfil.basico) {
				if (esVisible() && tipoValido()) {
					registros = new ArrayList<Object>(2);
					registros.add(BmovilConstants.MADMINISTRAR_OPERAR_CON_TOKEN);
					registros
							.add(SuiteApp.appContext
									.getString(R.string.administrar_menu_operar_con_token_titulo));
					lista.add(registros);
				}
			}else if (profile == Constants.Perfil.recortado) {
					registros = new ArrayList<Object>(2);
					registros.add(BmovilConstants.MADMINISTRAR_OPERAR_RECORTADO);
					registros
							.add(SuiteApp.appContext
									.getString(R.string.administrar_menu_recortado));
					lista.add(registros);
				}

		}
		registros = new ArrayList<Object>(2);
		registros.add(BmovilConstants.MADMINISTRAR_CONSULTAR_CONTRATO);
		registros
				.add(SuiteApp.appContext
						.getString(R.string.administrar_menu_consultar_contrato_titulo));
		lista.add(registros);

		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.cambioCuenta, session.getClientProfile())) {
			if (mostrarCambioCuenta()) {
				registros = new ArrayList<Object>(2);
				registros.add(BmovilConstants.MADMINISTRAR_CAMBIO_CUENTA);
				registros
						.add(SuiteApp.appContext
								.getString(R.string.administrar_menu_cambiocuenta_titulo));
				lista.add(registros);
			}
		}

		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.suspenderCancelar,
				session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(BmovilConstants.MADMINISTRAR_SUSPENDER_CANCELAR);
			registros.add(SuiteApp.appContext
					.getString(R.string.administrar_menu_supender_titulo));
			lista.add(registros);
		}
		//SPEI
		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.MantenimientoSpeimovil,
				session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(BmovilConstants.MADMINISTRAR_ASOCIAR_CELULAR);
			registros.add(SuiteApp.appContext.getString(R.string.bmovil_asociar_cuenta_telefono_menu_consultar));		
			lista.add(registros);
		}
		//Termina SPEI

		//Envio de estado de cuenta
		if(ConsultarEstatusEnvioEstadodeCuenta()){

			registros = new ArrayList<Object>(2);
			registros.add(BmovilConstants.MADMINISTRAR_CONSULTA_ESTATUS_ENVIO_ESTADO_DE_CUENTA);
			registros.add(SuiteApp.appContext.getString(R.string.administrar_menu_envioestadocuenta_titulo));
			lista.add(registros);
		}
		
		//ModificaciÛn "Menu Administrar"
		registros = new ArrayList<Object>(2);
		registros.add(BmovilConstants.MADMINISTRAR_NOVEDADES);
		registros.add(SuiteApp.appContext
				.getString(R.string.administrar_novedades));
		lista.add(registros);
		
		registros = new ArrayList<Object>(2);
		registros.add(BmovilConstants.MADMINISTRAR_ACERCADE);
		registros.add(SuiteApp.appContext
				.getString(R.string.administrar_acercade_titulo));
		lista.add(registros);
		return lista;

	}

	private boolean tipoValido() {
		String type = Tools.obtenerCuentaEje().getType();
		return !Constants.CREDIT_TYPE.equals(type)
				&& !Constants.EXPRESS_TYPE.equals(type);
	}

	private boolean esVisible() {
		Account account = Tools.obtenerCuentaEje();
		boolean response = false;
		if (account != null) {
			response = true;
		}
		return response;
	}

	/**
	 * La opcion de configurar montos es visible en el menu administrar
	 * 
	 * @return true si el usuario cuenta con un perfil avanzado
	 */
	boolean configurarMontos() {
		boolean visibleConfigMontos = false;
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext)
				.getClientProfile();
		visibleConfigMontos = (profile == Constants.Perfil.avanzado);
		return visibleConfigMontos;
	}

	/**
	 * La opcion de enviar estado de cuenta es visible en el menu administrar
	 *
	 * @return false si el usuario cuenta con un perfil recortado
	 */
	boolean ConsultarEstatusEnvioEstadodeCuenta() {
		boolean visibleEstadodeCuenta = true;
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext)
				.getClientProfile();
		visibleEstadodeCuenta = (profile != Constants.Perfil.recortado);
		return visibleEstadodeCuenta;
	}

	/**
	 * La opcion de configurar Alertas es visible en el menu administrar
	 * 
	 * @return true si el usuario cuenta con un perfil avanzado
	 */
	boolean configurarAlertas() {
		boolean visibleConfigAlertas = false;
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext)
				.getClientProfile();
		visibleConfigAlertas = (profile == Constants.Perfil.avanzado);
		return visibleConfigAlertas;
	}

	/**
	 * Actualiza las cuentas del usuario
	 */
	public void mostrarAlertaActualizarCuentas() {
		menuAdministrarViewController.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				menuAdministrarViewController.showInformationAlert(
						R.string.borrarDatos_dialogSucceed,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								actualizarCuentas();
							}
						});
			}
		});

	}

	/**
	 * Invoca la operacion al server para actualizar las cuentas desde el menu
	 * de administrar
	 * 
	 */
	public void actualizarCuentas() {
		int operationId = Server.ACTUALIZAR_CUENTAS;
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put("numeroTelefono", session.getUsername());
		params.put("numeroCliente", session.getClientNumber());
		params.put("IUM", session.getIum());
		//JAIG
		doNetworkOperation(operationId, params,true,new ActualizarCuentasResult(),isJsonValueCode.NONE, menuAdministrarViewController);
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {

		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			if (response.getResponse() instanceof ActualizarCuentasResult) {
				ActualizarCuentasResult result = (ActualizarCuentasResult) response
						.getResponse();
				actualizacionDeCuentasExitosa(result.getAsuntos());
			} else if (response.getResponse() instanceof ConsultarLimitesResult) {
				ConsultarLimitesResult result = (ConsultarLimitesResult) response
						.getResponse();
				//showConfigurarMontos(result);
			} else if (response.getResponse() instanceof ConsultaTerminosDeUsoData) {
				ConsultaTerminosDeUsoData terminosResponse = (ConsultaTerminosDeUsoData) response
						.getResponse();
				showTerminosDeUso(terminosResponse.getTerminosHtml());
			}
		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			menuAdministrarViewController.showInformationAlert(response
					.getMessageText());
		}
		menuAdministrarViewController.setRunningTask(false);
	}

	@Override
	public void doNetworkOperation(int operationId,
			Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {

		BmovilViewsController parent = null;

		if (menuAdministrarViewController.getParentViewsController() instanceof BmovilViewsController) {

			parent = ((BmovilViewsController) menuAdministrarViewController
					.getParentViewsController());
		} else {

			parent = SuiteApp.getInstance().getBmovilApplication()
					.getBmovilViewsController();
		}

		parent.getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode,
				caller);
	}

	/**
	 * Realiza la actualizacion de cuentas del usuario cuando la operacion fue
	 * exitosa.
	 * 
	 * @param accounts
	 */
	private void actualizacionDeCuentasExitosa(Account[] accounts) {
		Session session = Session.getInstance(SuiteApp.appContext);
		session.updateAccounts(accounts);
		menuAdministrarViewController.muestraCuentasActualizadas();
	}

	public void consultarLimites() {

		int operationId = Server.CONSULTAR_LIMITES;
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		doNetworkOperation(operationId, params,true, new ConsultarLimitesResult(),isJsonValueCode.NONE,menuAdministrarViewController);
	}

	/**
	 * Muestra la pantalla de configurar montos, con los montos indicados en el
	 * objeto ConfigurarMontos
	 * 
	 * @param result
	 */
	/*private void showConfigurarMontos(ConsultarLimitesResult result) {

		ConfigurarMontos cm = new ConfigurarMontos(result.getLimusuOp(),
				result.getLimusuDiario(), result.getLimusuMensual(),
				result.getLimmaxOp(), result.getLimmaxDiario(),
				result.getLimmaxMensual());
		menuAdministrarViewController.showConfigurarMontos(cm);
	}*/

	/**
	 * Muestra los terminos de uso contratados
	 * 
	 * @param result
	 */
	private void showTerminosDeUso(String msg) {
		menuAdministrarViewController.showTerminosDeUso(msg);
	}

	public void showTerminosDeUso() {
		int operationId = Server.OP_CONSULTAR_TERMINOS_SESION;
		Session session = Session.getInstance(SuiteApp.appContext);

		Constants.Perfil perfil = session.getClientProfile();

		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(
				ServerConstants.PERFIL_CLIENTE,
				perfil.equals(Constants.Perfil.avanzado) ? Constants.PROFILE_ADVANCED_03
						: Constants.PROFILE_BASIC_01);

		doNetworkOperation(operationId, params,true,new ConsultaTerminosDeUsoData(),isJsonValueCode.NONE, menuAdministrarViewController);
	}

	/**
	 * Retorna true si el usuario tiene mas de una cuenta, false si la opcion de
	 * cambio de cuenta no debe mostrarse.
	 */
	private boolean mostrarCambioCuenta() {
		Session session = Session.getInstance(SuiteApp.appContext);
		return (session.getAccounts().length > 1);
	}
	public void showConfirmacion(){
		int resSubtitle = 0;
		int resTitleColor = 0;
		int resIcon = R.drawable.bmovil_configurarmontos_icono;
		int resTitle = R.string.bmovil_configurar_montos_title;
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(this, resIcon, resTitle, resSubtitle, resTitleColor);
	}
	
	
	
	@Override
	public boolean mostrarContrasenia() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile(); 
		boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.consultaLimites, 
				perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile(); 
		boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.consultaLimites, perfil);
		return value;
	}
	
	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.consultaLimites, 
				perfil);
		return value;
	}
	
	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.consultaLimites,
									perfil);
		} catch (Exception ex) {
			if (Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}		
		return tipoOTP;
	}
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
	
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add("Operación");
		fila.add("Consultar Límites");
		tabla.add(fila);
		fila = new ArrayList<String>();
		fila.add("Perfil");
		Session sesion= Session.getInstance(menuAdministrarViewController);
		if(Constants.Perfil.avanzado.equals(sesion.getClientProfile())){
		
			fila.add("Avanzado");
		
		}else if(Constants.Perfil.basico.equals(sesion.getClientProfile())){
			fila.add("Básico");
			
		}
		else{
			fila.add("MicroPago");
		}
		tabla.add(fila);
		
		return tabla;
	}
	

	/**
	 * Invoca la operacion para hacer el cambio de cuenta asociada
	 */
	@Override
	public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
	
		
		int operationId = Server.CONSULTAR_LIMITES;
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		params.put(ServerConstants.CVE_ACCESO, contrasenia==null?"":contrasenia);
		params.put(ServerConstants.TARJETA_5DIG, campoTarjeta==null?"":campoTarjeta);
		params.put(ServerConstants.CODIGO_NIP, nip==null?"":nip);
		params.put(ServerConstants.CODIGO_CVV2, cvv==null?"":cvv);
		params.put(ServerConstants.CODIGO_OTP, token==null?"":token);
		

		if(Server.ALLOW_LOG) Log.d(">> CGI", "Perfil peticion >> "+session.getClientProfile().name());
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultaLimites, session.getClientProfile());

		
		
		params.put(ServerConstants.CADENA_AUTENTICACION,cadenaAutenticacion);
		params.put(ServerConstants.VERSION_FLUJO, Constants.APPLICATION_VERSION);
		//JAIG
		doNetworkOperation(operationId, params,true,new ConsultarLimitesResult(),isJsonValueCode.NONE, confirmacionAutenticacionViewController);
		
	}
	@Override
	public int getTextoEncabezado() {
		int resTitle;
			resTitle = R.string.administrar_menu_configurarmontos_bas_rec_titulo;	
		return resTitle;
	}

	@Override
	public int getNombreImagenEncabezado() {
		int resIcon = R.drawable.bmovil_configurarmontos_icono;
		return resIcon;
	}

	
	@Override
	public String getTextoTituloResultado() {
		int txtTitulo = R.string.transferir_detalle_operacion_exitosaTitle;
		return menuAdministrarViewController.getString(txtTitulo);
	}
	
	public void conusltarLimitesJumpOp(MenuAdministrarViewController administrarViewController, String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
	
		int operationId = Server.CONSULTAR_LIMITES;
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		params.put(ServerConstants.CVE_ACCESO, contrasenia==null?"":contrasenia);
		params.put(ServerConstants.TARJETA_5DIG, campoTarjeta==null?"":campoTarjeta);
		params.put(ServerConstants.CODIGO_NIP, nip==null?"":nip);
		params.put(ServerConstants.CODIGO_CVV2, cvv==null?"":cvv);
		params.put(ServerConstants.CODIGO_OTP, token==null?"":token);
		
		if(Server.ALLOW_LOG) Log.d(">> CGI", "Perfil peticion jump >> "+session.getClientProfile().name());
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultaLimites, session.getClientProfile());

		
		
		params.put(ServerConstants.CADENA_AUTENTICACION,cadenaAutenticacion);
		params.put(ServerConstants.VERSION_FLUJO, BmovilConstants.APPLICATION_VERSION);
		//JAIG
		doNetworkOperation(operationId, params,true,new ConsultarLimitesResult(),isJsonValueCode.NONE, administrarViewController);
		
	}


}
