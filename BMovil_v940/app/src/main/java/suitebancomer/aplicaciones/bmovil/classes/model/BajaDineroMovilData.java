package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class BajaDineroMovilData implements ParsingHandler {
	private String fechaServidor;
	private String horaServidor;
	
	/**
	 * @return the fechaServidor
	 */
	public String getFechaServidor() {
		return fechaServidor;
	}

	/**
	 * @param fechaServidor the fechaServidor to set
	 */
	public void setFechaServidor(String fechaServidor) {
		this.fechaServidor = fechaServidor;
	}

	/**
	 * @return the horaServidor
	 */
	public String getHoraServidor() {
		return horaServidor;
	}

	/**
	 * @param horaServidor the horaServidor to set
	 */
	public void setHoraServidor(String horaServidor) {
		this.horaServidor = horaServidor;
	}

	public BajaDineroMovilData() {
		fechaServidor = "";
		horaServidor = "";
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		fechaServidor = parser.parseNextValue("FE");
		horaServidor = parser.parseNextValue("HR");
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
