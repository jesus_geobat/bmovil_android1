package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfigurarAlertasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class ConfigurarAlertasViewController extends BaseViewController {
	private ConfigurarAlertasDelegate configurarAlertasDelegate;
	
	private LinearLayout cuentaOrigenLayout;
	private EditText tbMontoRetiros;
	private EditText tbMontoDepositos;
	private CuentaOrigenViewController ctaOrigen;
	
	
	public ConfigurarAlertasViewController() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_configurar_alertas);
		
		setTitle(R.string.bmovil_configurar_alertas_titulo, R.drawable.bmovil_alta_frecuente_icono);

		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(getParentViewsController().getBaseDelegateForKey(ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID));
		configurarAlertasDelegate = (ConfigurarAlertasDelegate)getDelegate();
		configurarAlertasDelegate.setOwnerController(this);
		init();
	}
	
	private void init() {
		findViews();
		scaleToCurrentScreen();
		cargarCuentaOrigen();
		tbMontoDepositos.addTextChangedListener(new BmovilTextWatcher(this));
		tbMontoRetiros.addTextChangedListener(new BmovilTextWatcher(this));
	}
	
	private void findViews() {
		cuentaOrigenLayout = (LinearLayout)findViewById(R.id.layoutCuentaOrigen);
		tbMontoRetiros = (EditText)findViewById(R.id.tbMontoRetiros);
		tbMontoDepositos = (EditText)findViewById(R.id.tbMontoDepositos);
	}
	
	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.lblRecibirAlertas), true);
		guiTools.scale(findViewById(R.id.lblRetiros), true);
		guiTools.scale(findViewById(R.id.toggleRetiros), false);
		guiTools.scale(findViewById(R.id.lblMontoRetiros), true);
		guiTools.scale(findViewById(R.id.tbMontoRetiros), true);
		guiTools.scale(findViewById(R.id.lblDepositos), true);
		guiTools.scale(findViewById(R.id.toggleDepositos), true);
		guiTools.scale(findViewById(R.id.lblMontoDepositos), true);
		guiTools.scale(findViewById(R.id.tbMontoDepositos), true);
		guiTools.scale(findViewById(R.id.lblMostarSaldo), true);
		guiTools.scale(findViewById(R.id.toggleMostrarSaldo), true);
		guiTools.scale(findViewById(R.id.lblConsultarTarifas), true);
		
	}
	
	private void cargarCuentaOrigen() {
		cuentaOrigenLayout.addView(ctaOrigen);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		super.processNetworkResponse(operationId, response);
	}
	
	
}
