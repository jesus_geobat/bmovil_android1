package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ResultadosRetiroSinTarjetaViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjetaViewDto;

public class ResultadosRetiroSinTarjetaDelegate extends ResultadosDelegate {

    public final static long RESULTADOS_RETIRO_SIN_TARJETA_DELEGATE_ID = 0x1ef4e7c61ca112bfL;

    public AltaRetiroSinTarjetaDelegate altaRetiroSinTarjetaDelegate;

    private ResultadosRetiroSinTarjetaViewController resultadosRetiroSinTarjetaViewController;

    public ResultadosRetiroSinTarjetaDelegate(AltaRetiroSinTarjetaDelegate operationDelegate) {
        super(operationDelegate);
        this.altaRetiroSinTarjetaDelegate = operationDelegate;
    }

    public void setResultadosRetiroSinTarjetaViewController(ResultadosRetiroSinTarjetaViewController viewController) {
        this.resultadosRetiroSinTarjetaViewController = viewController;
        setResultadosViewController(viewController);
    }

    public void consultaDatos() {
        RetiroSinTarjetaViewDto retiroSinTarjetaViewDto = altaRetiroSinTarjetaDelegate.getDatosResultados();
        resultadosRetiroSinTarjetaViewController.setImporte(retiroSinTarjetaViewDto.getImporte());
        if (getTipoRetiro().equals(AltaRetiroSinTarjetaDelegate.TipoRetiro.RETIRO_SIN_TARJETA)) {
            resultadosRetiroSinTarjetaViewController.setClaveRetiro(retiroSinTarjetaViewDto.getClaveRetiro());
        }
        resultadosRetiroSinTarjetaViewController.setCodigoSeguridad(retiroSinTarjetaViewDto.getCodigoSeguridad());
        resultadosRetiroSinTarjetaViewController.setCuentaRetiro(retiroSinTarjetaViewDto.getCuentaRetiro());
        resultadosRetiroSinTarjetaViewController.setNumeroCelular(retiroSinTarjetaViewDto.getNumeroCelular());
        resultadosRetiroSinTarjetaViewController.setConcepto(retiroSinTarjetaViewDto.getConcepto());
        resultadosRetiroSinTarjetaViewController.setVencimiento(retiroSinTarjetaViewDto.getVigencia());
        resultadosRetiroSinTarjetaViewController.setFolio(retiroSinTarjetaViewDto.getFolio());
        resultadosRetiroSinTarjetaViewController.setNombre(retiroSinTarjetaViewDto.getNombre());
        resultadosRetiroSinTarjetaViewController.setCompania(retiroSinTarjetaViewDto.getCompania());
    }

    public AltaRetiroSinTarjetaDelegate.TipoRetiro getTipoRetiro() {
        return altaRetiroSinTarjetaDelegate.getTipoRetiro();
    }

}
