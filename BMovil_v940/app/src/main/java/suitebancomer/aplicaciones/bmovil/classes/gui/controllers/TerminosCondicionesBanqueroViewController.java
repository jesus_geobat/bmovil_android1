package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

        import android.os.Bundle;
        import android.view.View;
        import android.webkit.WebView;
        import android.widget.ImageButton;
        import android.widget.TextView;
        import com.bancomer.mbanking.R;
        import com.bancomer.mbanking.SuiteApp;
        import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MenuPrincipalDelegate;
        import suitebancomer.aplicaciones.bmovil.classes.model.TerminosCondicionesBanquero;
        import suitebancomer.classes.gui.controllers.BaseViewController;
/**
 * Created by eluna on 21/06/2016.
 */
public class TerminosCondicionesBanqueroViewController extends BaseViewController implements View.OnClickListener
{
    private BmovilViewsController parentManager;
    private WebView textoTerminosCondiciones;
    private ImageButton btnAtrasLogicoTerminos;
    String strTerminos;
    String contrato;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState, 0, R.layout.layout_bmovil_terminos_condiciones_banquero);
        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        SuiteApp suiteApp = (SuiteApp) getApplication();
        setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
        setDelegate(parentViewsController.getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID));
        strTerminos = TerminosCondicionesBanquero.getInstance().getTerminosHtml().replace("//", "");
        final String[] leyendas ;
        leyendas = strTerminos.split("html>");
        contrato = "<html>" + leyendas[1] + "html>";
        initValues();
    }

    private void initValues()
    {
        textoTerminosCondiciones = (WebView) findViewById(R.id.webViewTerminosContrato);
        textoTerminosCondiciones.getSettings().setJavaScriptEnabled(true);
        textoTerminosCondiciones.getSettings().setDefaultTextEncodingName("utf-8");
        textoTerminosCondiciones.loadData(contrato, "text/html; charset=utf-8", "utf-8");
        textoTerminosCondiciones.getSettings().setBuiltInZoomControls(true);
        textoTerminosCondiciones.getSettings().setSupportZoom(true);
        textoTerminosCondiciones.getSettings().setLoadWithOverviewMode(true);
        textoTerminosCondiciones.getSettings().setUseWideViewPort(true);
        textoTerminosCondiciones.setInitialScale(15);

        btnAtrasLogicoTerminos = (ImageButton) findViewById(R.id.btnAtrasLogicoTerminos);
        btnAtrasLogicoTerminos.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v)
    {
        if (v.equals(btnAtrasLogicoTerminos)){
            goBack();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void goBack() {
        parentViewsController.removeDelegateFromHashMap(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID);
        super.goBack();
    }
}
