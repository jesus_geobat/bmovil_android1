package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;




import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MantenimientoSpeiMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
//import tracking.TrackingHelper;

public class ConsultaCuentaSpeiMovilViewController extends BaseViewController {
	//#region Class fields.
	/**
	 * The delegate for this screen.
	 */
	private MantenimientoSpeiMovilDelegate ownDelegate; 
	
	/**
	 * The linear layout for hold the accounts list.
	 */
	private LinearLayout accountsListLayout;
	
	/**
	 * The ListaSeleccion component to show the accounts list.
	 */
	private ListaSeleccionViewController accountsList;
	
	/**
	 * The edit button.
	 */
	private Button editButton;
	
	/**
	 * The delete button.
	 */
	private Button deleteButton;
	
	
	//Etiquetado Ale
	//AMZ Ale
	private BmovilViewsController parentManager;
	//#endregion
	
	//#region Life-cycle management.
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_asociacion_cuenta_telefono);
		setTitle(R.string.bmovil_asociar_cuenta_telefono_alternative_title, R.drawable.icono_spei);
		
		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(MantenimientoSpeiMovilDelegate.DELEGATE_ID));
		ownDelegate = (MantenimientoSpeiMovilDelegate) getDelegate();
		ownDelegate.setOwnerController(this);
		
		findViews();
		scaleForCurrentScreen();
		
//		accountsList = ownDelegate.loadAccountsList();
//		if(null != accountsList)
//			accountsListLayout.addView(accountsList);
//		else
//			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while loading the accounts list.");
	}

	/**
	 * Search the required views for the operation of this screen.
	 */
	private void findViews() {
		accountsListLayout = (LinearLayout)findViewById(R.id.account_list_layout);
		editButton = (Button)findViewById(R.id.editButton);
		deleteButton = (Button)findViewById(R.id.deleteButton);
	}
	
	/**
	 * Scale the shown views to fit the current screen resolution.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.mainLayout));
		guiTools.scale(accountsListLayout);
		guiTools.scale(findViewById(R.id.helpImage));
		guiTools.scale(findViewById(R.id.buttonsLayout));
		guiTools.scale(editButton);
		guiTools.scale(deleteButton);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		ownDelegate.setOwnerController(this);
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		
		ownDelegate.requestSpeiAccounts();
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(MantenimientoSpeiMovilDelegate.DELEGATE_ID);
		super.goBack();
	}
	//#endregion
	
	//#region Network.
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		ownDelegate.analyzeResponse(operationId, response);
	}
	//#endregion
	
	//#region OnClick Listeners
	/**
	 * Behavior when the edit button is clicked.
	 * @param sender The clicked view.
	 */
	public void onEditButtonClick(View sender) {
		Object[] selectedData = getSelectedDataFromListaSeleccion();
		ownDelegate.elementSelected(selectedData, true);
	}
	
	/**
	 * Behavior when the delete button is clicked.
	 * @param sender The clicked view.
	 */
	public void onDeleteButtonClick(View sender) {
		Object[] selectedData = getSelectedDataFromListaSeleccion();
		ownDelegate.elementSelected(selectedData, false);
	}
	//#endregion
	
	/**
	 * Sets the accounts list element and shows it on the screen.
	 * @param accountsList The ListaSeleccion element with the data loaded.
	 */
	public void setAccountsList(ListaSeleccionViewController accountsList) {
		this.accountsList = accountsList;
		
		if(null != this.accountsList) {
			accountsListLayout.removeAllViews();
			accountsListLayout.addView(this.accountsList);
			this.accountsList.cargarTabla();
			moverScroll();
		} else {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while loading the accounts list.");
		}
	}
	
	/**
	 * Activa o desactiva el boton de asociar.
	 * @param enabled True para activar el botón, false para desactivarlo.
	 */
	public void setEditButtonEnabled(boolean enabled) {
		editButton.setEnabled(enabled);
		editButton.setBackgroundResource(enabled ? R.drawable.btn_asociar_activado : R.drawable.btn_asociar_desactivado);
		//Etiquetado Ale
		if(enabled == true ){
		//AMZ Ale
		Map<String,Object> Paso1OperacionMap = new HashMap<String, Object>();
		//AMZ Ale
		Paso1OperacionMap.put("evento_paso1","event46");
		Paso1OperacionMap.put("&&products","operaciones;admin+Asociar numero");
		Paso1OperacionMap.put("eVar12","paso1:Asociar");
	//	TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
		}
			
	}
	
	/**
	 * Activa o desactiva el boton de desacosiar.
	 * @param enabled True para activar el botón, false para desactivarlo.
	 */
	public void setDeleteButtonEnabled(boolean enabled) {
		deleteButton.setEnabled(enabled);
		deleteButton.setBackgroundResource(enabled ? R.drawable.btn_desasociar_activado : R.drawable.btn_desasociar_desactivado);
		//Etiquetado Ale
				if(enabled == true ){
				//AMZ Ale
				Map<String,Object> Paso1OperacionMap = new HashMap<String, Object>();
				//AMZ Ale
				Paso1OperacionMap.put("evento_paso1","event46");
				Paso1OperacionMap.put("&&products","operaciones;admin+ Desasociar numero");
				Paso1OperacionMap.put("eVar12","paso1:Desasociar");
		//		TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
				}
				
				
		
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(MotionEvent.ACTION_DOWN == ev.getAction())
			accountsList.setMarqueeEnabled(false);
		else if(MotionEvent.ACTION_UP == ev.getAction())
			accountsList.setMarqueeEnabled(true);
		
		return super.dispatchTouchEvent(ev);
	}
	
	/**
	 * Obtains the data from the currently selected element of the ListaSeleccion element.
	 * @return The element data.
	 */
	@SuppressWarnings("unchecked")
	private Object[] getSelectedDataFromListaSeleccion() {
		int selectedIndex = accountsList.getOpcionSeleccionada();
		if(selectedIndex < 0)
			return null;
		
		Object[] selectedData = null;
		
		try {
			selectedData = (Object[])((ArrayList<Object>)accountsList.getLista().get(selectedIndex)).get(0);
		} catch(Exception ex) {
			selectedData = null;
			if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Error al obtener los datos del elemento seleccionado", ex);
		}
		
		return selectedData;
	}

	/**
	 * Clears the accounts list selection.
	 */
	public void clearAccountsListSelection() {
		if(null == accountsList)
			return;
		
		accountsList.setOpcionSeleccionada(-1);
		accountsList.cargarTabla();
	}
}
