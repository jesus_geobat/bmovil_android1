package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.common.ToolsCommons;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ResultadosRetiroSinTarjetaDelegate;
import tracking.TrackingHelper;

import static android.provider.MediaStore.*;


public class ResultadosRetiroSinTarjetaViewController extends ResultadosViewController {

    private ResultadosRetiroSinTarjetaDelegate resultadosRetiroSinTarjetaDelegate;

    private DrawerLayout drawerLayout;

    private ImageView imageTitulo;
    private TextView lblTitulo;

    private TextView lblDescripcionClaveRetiro;
    private LinearLayout layoutContenedorClaveRetiro;
    private RelativeLayout layoutContenedorDatosCelular;
    private ImageView imagenCompania;
    private TextView lblNombre;
    private TextView lblNumeroCelular2;
    private TextView lblCompania;
    private LinearLayout layoutContenedorVencimiento;
    private TextView lblVencimiento2;
    private LinearLayout layoutContenedorVencimientoDetalle;
    private LinearLayout layoutContenedorCelularDetalle;

    private TextView lblImporte;
    private TextView lblClaveRetiro1;
    private TextView lblClaveRetiro2;
    private TextView lblClaveRetiro3;
    private TextView lblCodigoSeguridad;
    private TextView lblCuentaRetiro;
    private TextView lblNumeroCelular;
    private TextView lblConcepto;
    private TextView lblVencimiento;
    private TextView lblFolio;

    private Button btnDetalle;
    private LinearLayout layoutDetalle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER, R.layout.layout_bmovil_resultados_retiro_sin_tarjeta);
        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        setDelegate(getParentViewsController().getBaseDelegateForKey(ResultadosRetiroSinTarjetaDelegate.RESULTADOS_RETIRO_SIN_TARJETA_DELEGATE_ID));

        resultadosRetiroSinTarjetaDelegate = (ResultadosRetiroSinTarjetaDelegate) getDelegate();
        resultadosRetiroSinTarjetaDelegate.setResultadosRetiroSinTarjetaViewController(this);

        resultadosDelegate = resultadosRetiroSinTarjetaDelegate;

        findViews();

        configuraPantalla();

        resultadosRetiroSinTarjetaDelegate.consultaDatos();
        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("resul", parentManager.estados);
        moverScroll();

        final View view = getLayoutInflater().inflate(R.layout.layout_resultado_retiro_felicidades, drawerLayout, false);
        drawerLayout.addView(view, drawerLayout.getChildCount());

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                    view.animate()
                            .alpha(0.0f)
                            .setDuration(500)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    drawerLayout.removeViewAt(drawerLayout.getChildCount() - 1);
                                }
                            });
                } else {
                    drawerLayout.removeViewAt(drawerLayout.getChildCount() - 1);
                }
            }
        }, 2000);
    }

    private void findViews() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        lblImporte = (TextView) findViewById(R.id.valor_importe);
        lblClaveRetiro1 = (TextView) findViewById(R.id.valor_clave_retiro1);
        lblClaveRetiro2 = (TextView) findViewById(R.id.valor_clave_retiro2);
        lblClaveRetiro3 = (TextView) findViewById(R.id.valor_clave_retiro3);
        lblCodigoSeguridad = (TextView) findViewById(R.id.valor_codigo_seguridad);
        lblCuentaRetiro = (TextView) findViewById(R.id.valor_cuenta_retiro);
        lblNumeroCelular = (TextView) findViewById(R.id.valor_numero_celular);
        lblConcepto = (TextView) findViewById(R.id.valor_concepto);
        lblVencimiento = (TextView) findViewById(R.id.valor_vencimiento);
        lblFolio = (TextView) findViewById(R.id.valor_folio);

        btnDetalle = (Button) findViewById(R.id.boton_detalle);
        layoutDetalle = (LinearLayout) findViewById(R.id.layout_detalle);

        imageTitulo = (ImageView) findViewById(R.id.imagen_titulo);
        lblTitulo = (TextView) findViewById(R.id.texto_titulo);

        lblDescripcionClaveRetiro = (TextView) findViewById(R.id.label_descripcion_clave_retiro);
        layoutContenedorClaveRetiro = (LinearLayout) findViewById(R.id.layout_container_clave_retiro);
        layoutContenedorDatosCelular = (RelativeLayout) findViewById(R.id.layout_container_datos_celular);
        imagenCompania = (ImageView) findViewById(R.id.imagen_compania);
        lblNombre = (TextView) findViewById(R.id.valor_nombre);
        lblNumeroCelular2 = (TextView) findViewById(R.id.valor_numero_celular2);
        lblCompania = (TextView) findViewById(R.id.valor_compania);
        layoutContenedorVencimiento = (LinearLayout) findViewById(R.id.layout_contenedor_vencimiento);
        lblVencimiento2 = (TextView) findViewById(R.id.valor_vencimiento2);
        layoutContenedorVencimientoDetalle = (LinearLayout) findViewById(R.id.layout_contenedor_vencimiento_detalle);
        layoutContenedorCelularDetalle = (LinearLayout) findViewById(R.id.layout_contenedor_celular_detalle);
    }

    @Override
    public void configuraPantalla() {
        if (resultadosRetiroSinTarjetaDelegate.getTipoRetiro() == AltaRetiroSinTarjetaDelegate.TipoRetiro.RETIRO_SIN_TARJETA) {
            imageTitulo.setImageDrawable(getResources().getDrawable(R.drawable.ic_parami));
            lblTitulo.setText(R.string.bmovil_unificacion_label_parami);
            lblDescripcionClaveRetiro.setText(R.string.bmovil_unificacion_label_clave12digitos);
            layoutContenedorClaveRetiro.setVisibility(View.VISIBLE);
            layoutContenedorDatosCelular.setVisibility(View.GONE);
            layoutContenedorVencimiento.setVisibility(View.GONE);
            layoutContenedorVencimientoDetalle.setVisibility(View.VISIBLE);
            layoutContenedorCelularDetalle.setVisibility(View.VISIBLE);
        } else {
            imageTitulo.setImageDrawable(getResources().getDrawable(R.drawable.ic_paraalguienmas));
            lblTitulo.setText(R.string.bmovil_unificacion_label_paraalguienmas);
            lblTitulo.setTextColor(Color.parseColor("#82c645"));
            lblDescripcionClaveRetiro.setText(R.string.bmovil_unificacion_label_claveviaSMS);
            layoutContenedorClaveRetiro.setVisibility(View.GONE);
            layoutContenedorDatosCelular.setVisibility(View.VISIBLE);
            layoutContenedorVencimiento.setVisibility(View.VISIBLE);
            layoutContenedorVencimientoDetalle.setVisibility(View.GONE);
            layoutContenedorCelularDetalle.setVisibility(View.GONE);
        }
    }

    public void setImporte(String importe) {
        lblImporte.setText(importe);
    }

    public void setClaveRetiro(String claveRetiro) {
        String[] clave = claveRetiro.split("-");
        lblClaveRetiro1.setText(clave[0]);
        lblClaveRetiro2.setText(clave[1]);
        lblClaveRetiro3.setText(clave[2]);
    }

    public void setCodigoSeguridad(String codigoSeguridad) {
        lblCodigoSeguridad.setText(codigoSeguridad);
    }

    public void setCuentaRetiro(String cuentaRetiro) {
        lblCuentaRetiro.setText(cuentaRetiro);
    }

    public void setNumeroCelular(String numeroCelular) {
        lblNumeroCelular.setText(numeroCelular);
        lblNumeroCelular2.setText(ToolsCommons.formatPhoneNumber(numeroCelular));
    }

    public void setConcepto(String concepto) {
        lblConcepto.setText(concepto);
    }

    public void setVencimiento(String vencimiento) {
        lblVencimiento.setText(vencimiento);
        lblVencimiento2.setText(ToolsCommons.formatDateDineroMovil(vencimiento));
    }

    public void setFolio(String folio) {
        lblFolio.setText(folio);
    }

    public void setNombre(String nombre) {
        lblNombre.setText(Tools.formatStringMaxLengthWithEllipsis(nombre,6));
    }

    public void setCompania(String compania) {
        lblCompania.setText(compania);
        if (compania.equals("TELCEL")) {
            imagenCompania.setImageDrawable(getResources().getDrawable(R.drawable.txt_telcel));
        } else if (compania.equals("MOVISTAR")) {
            imagenCompania.setImageDrawable(getResources().getDrawable(R.drawable.txt_movistar));
        } else if (compania.equals("IUSACELL")) {
            imagenCompania.setImageDrawable(getResources().getDrawable(R.drawable.txt_iusacell));
        } else if (compania.equals("UNEFON")) {
            imagenCompania.setImageDrawable(getResources().getDrawable(R.drawable.txt_unfeon));
        } else if (compania.equals("NEXTEL")) {
            imagenCompania.setImageDrawable(getResources().getDrawable(R.drawable.txt_nextel));
        }
    }

    public void clickDetalle(View view) {
        if (layoutDetalle.getVisibility() == View.GONE) {
            btnDetalle.setText("Menos Detalle   -");
            layoutDetalle.setVisibility(View.VISIBLE);
        } else {
            btnDetalle.setText("Más Detalle   +");
            layoutDetalle.setVisibility(View.GONE);
        }
    }

    public void clickOpciones(View view) {
        openOptionsMenu();
    }

    public void clickCaptura(View view){
        try {
            moverScroll();
        }catch (Exception e) {
            Log.d("Capturando:", "Error en mover Scroll");
        }
        //Se espera a que termine el Scroll para hacer la captura
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getWindow().getDecorView().setDrawingCacheEnabled(true);
                getWindow().getDecorView().buildDrawingCache(true);
                Bitmap capture = Bitmap.createBitmap(getWindow().getDecorView().getDrawingCache());
                getWindow().getDecorView().setDrawingCacheEnabled(false);
                Date now = new Date();
                android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
                Uri result = null;
                try{
                    String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
                    File myDir = new File(root + "/Captura de Pantallas");
                    myDir.mkdirs();
                    File imageFile = new File(myDir,now.toString().trim()+".png");
                    FileOutputStream fos = new FileOutputStream(imageFile);
                    int quality = 100;
                    capture.compress(Bitmap.CompressFormat.PNG,quality,fos);
                    fos.flush();
                    fos.close();
                    ContentValues image = new ContentValues();
                    image.put(Images.Media.TITLE, imageFile.getName());
                    image.put(Images.Media.DISPLAY_NAME, imageFile.getName());
                    image.put(Images.Media.DESCRIPTION, "BMovil Captures");
                    image.put(Images.Media.DATE_ADDED, now.toString());
                    image.put(Images.Media.DATE_TAKEN, now.toString());
                    image.put(Images.Media.DATE_MODIFIED, now.toString());
                    image.put(Images.Media.MIME_TYPE, "image/png");
                    image.put(Images.Media.ORIENTATION, 0);

                    File parent = imageFile.getParentFile();
                    String path = parent.toString().toLowerCase();
                    String name = parent.getName().toLowerCase();
                    image.put(Images.ImageColumns.BUCKET_ID, path.hashCode());
                    image.put(Images.ImageColumns.BUCKET_DISPLAY_NAME, name);
                    image.put(Images.Media.SIZE, imageFile.length());
                    image.put(Images.Media.DATA, imageFile.getAbsolutePath());
                    result = getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, image);
                } catch (Exception e) {
                    Log.d("Exception",e.toString());
                }finally {
                    if(result != null){
                        Toast.makeText(getInstance().getApplicationContext(),"Guardado correcto",2).show();
                    }
                }
            }
        }, 1500);
    }

    public void clickMenu(View view) {
//        opinatorHandler.removeCallbacks(opinatorRunner);
        resultadosRetiroSinTarjetaDelegate.altaRetiroSinTarjetaDelegate.getTransferencia().setImporte(null);
        resultadosRetiroSinTarjetaDelegate.altaRetiroSinTarjetaDelegate.getTransferencia().setConcepto(null);
        botonMenuClick();
    }
}
