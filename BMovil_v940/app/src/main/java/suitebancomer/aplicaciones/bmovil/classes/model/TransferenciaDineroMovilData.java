package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class TransferenciaDineroMovilData implements ParsingHandler {
	/**
	 * Clave de OTP num�rico.
	 */
	private String claveOTP;
	
	/**
	 * Importe.
	 */
	private String importe;
	
	/**
	 * Folio de operación.
	 */
	private String folio;
	
	/**
	 * Fecha de operación.
	 */
	private String fechaOperacion;
	
	/**
	 * Fecha de expiraci�n.
	 */
	private String fechaExpiracion;
	
	/**
	 * Hora de operación.
	 */
	private String horaOperacion;
	
	/**
	 * @return La clave de OTP.
	 */
	public String getClaveOTP() {
		return claveOTP;
	}

	/**
	 * @param claveOTP La clave de OTP a establecer.
	 */
	public void setClaveOTP(String claveOTP) {
		this.claveOTP = claveOTP;
	}

	/**
	 * @return El importe.
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * @param importe El importe a establecer.
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}

	/**
	 * @return El folio de operación.
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * @param folio El folio de operación a establecer.
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * @return La fecha de operación.
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	/**
	 * @param fechaOperacion La fecha de operación a establecer.
	 */
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	/**
	 * @return La fecha de expiraci�n.
	 */
	public String getFechaExpiracion() {
		return fechaExpiracion;
	}

	/**
	 * @param fechaExpiracion La fecha de expiraci�n a establecer.
	 */
	public void setFechaExpiracion(String fechaExpiracion) {
		this.fechaExpiracion = fechaExpiracion;
	}

	/**
	 * @return La hora de operación.
	 */
	public String getHoraOperacion() {
		return horaOperacion;
	}

	/**
	 * @param horaOperacion La hora de operación establecer.
	 */
	public void setHoraOperacion(String horaOperacion) {
		this.horaOperacion = horaOperacion;
	}

	/**
	 * Constructor por defecto.
	 */
	public TransferenciaDineroMovilData()  {
		claveOTP 		= "";
		importe 		= "";
		folio 			= "";
		fechaOperacion 	= "";
		fechaExpiracion = "";
		horaOperacion 	= "";
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		claveOTP 		= parser.parseNextValue("OT");
		importe 		= parser.parseNextValue("IM");
		folio 			= parser.parseNextValue("FO");
		fechaOperacion 	= parser.parseNextValue("FE");
		fechaExpiracion	= parser.parseNextValue("FX");
		horaOperacion 	= parser.parseNextValue("HO");	
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}
}
