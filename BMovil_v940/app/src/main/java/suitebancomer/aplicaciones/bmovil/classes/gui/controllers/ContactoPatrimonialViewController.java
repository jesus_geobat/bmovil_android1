package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import suitebancomer.aplicaciones.bmovil.classes.common.ContactoPatrimonialLinearLayout;
import suitebancomer.aplicaciones.bmovil.classes.common.ContratoPatrimonialAdapter;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ContactoPatrimonialDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.DetallePromotorPatrimonial;
import suitebancomer.aplicaciones.bmovil.classes.model.DetallePromotorpatrimonial2;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by OOROZCO on 3/14/16.
 */
public class  ContactoPatrimonialViewController extends BaseViewController {

    private ImageView divider;
    private LinearLayout linear_table_header;
    private Dialog dialogContratos;
    private DisplayMetrics displayMetrics;
    private LinearLayout header;
    private LinearLayout linear_second_header;
    private ArrayList <View> objectsToBeMeasured;

    private ContactoPatrimonialDelegate delegate;

    private TextView text_view_banker_number;
    private TextView text_view_banker_name;
    private TextView text_view_office_address;
    private TextView text_view_office_name;
    private TextView text_view_contract_number;
    private TextView text_view_banker_name2;
    private TextView text_view_banker_number2;



    public Dialog getDialogContratos() {
        return dialogContratos;
    }


    public ContactoPatrimonialDelegate getDelegate (){
        return delegate;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_contacto_patrimonial);

        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        delegate = (ContactoPatrimonialDelegate) parentViewsController.getBaseDelegateForKey(ContactoPatrimonialDelegate.CONTACTO_PATRIMONIAL_DELEGATE);
        delegate.setController(this);

        linkUIElements();
        setListeners();
        customizeView();

        objectsToBeMeasured = new ArrayList<View>();
        objectsToBeMeasured.add(header);
        objectsToBeMeasured.add(linear_second_header);

        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        setDataToView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio())
            return;
        getParentViewsController().setCurrentActivityApp(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }


    private void linkUIElements()
    {
        divider = (ImageView) findViewById(R.id.title_divider);
        linear_table_header = (LinearLayout) findViewById(R.id.linear_table_header);
        header = (LinearLayout) findViewById(R.id.header_layout);
        linear_second_header = (LinearLayout) findViewById(R.id.linear_second_header);
        text_view_banker_number = (TextView) findViewById(R.id.text_view_banker_number);
        text_view_banker_name = (TextView) findViewById(R.id.text_view_banker_name);
        text_view_office_address = (TextView) findViewById(R.id.text_view_office_address);
        text_view_office_name = (TextView) findViewById(R.id.text_view_office_name);
        text_view_contract_number = (TextView) findViewById(R.id.text_view_contract_number);
        text_view_banker_name2 = (TextView)findViewById(R.id.text_view_banker_name2);
        text_view_banker_number2 = (TextView)findViewById(R.id.text_view_banker_number2);


    }

    private void customizeView()
    {

        setTitle(R.string.bmovil_contactos_cabecera, R.drawable.bmovil_mis_cuentas_icono);

        LinearLayout.LayoutParams lp =  (LinearLayout.LayoutParams)divider.getLayoutParams();
        lp.topMargin=5;
        lp.bottomMargin=0;
        divider.setLayoutParams(lp);
        divider.requestLayout();

    }

    private void setListeners()
    {
        linear_table_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAction();
            }
        });

        findViewById(R.id.image_view_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = delegate.getDetalle().getPromotor().getTelefonoPromotor() + PhoneNumberUtils.PAUSE +  PhoneNumberUtils.PAUSE +  PhoneNumberUtils.PAUSE + delegate.getDetalle().getPromotor().getExtensionPromotor();
                String encodedNumber = "";
                try {
                    encodedNumber = URLEncoder.encode(number, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                Date date = Session.getInstance(getApplicationContext()).getServerDate();

          //     if (serverValidation(date)) {
                    System.out.println("Enconder: " + encodedNumber);
                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + encodedNumber)));
              /*  } else

                    ((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showAvisoPatrimonial();
*/

            }
        });
        findViewById(R.id.image_view_call2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String number = delegate.getDetalle().getPromotor2().getTelefonoPromotor() + PhoneNumberUtils.PAUSE +  PhoneNumberUtils.PAUSE +  PhoneNumberUtils.PAUSE +  delegate.getDetalle().getPromotor2().getExtensionPromotor();
                String encodedNumber = "";
                try {
                    encodedNumber = URLEncoder.encode(number, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            /*   Date date = Session.getInstance(getApplicationContext()).getServerDate();

                if (serverValidation(date))
                */
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + encodedNumber)));
              /*  else

                    ((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showAvisoPatrimonial();
*/
            }
        });

        //Android Emial Data Test--

    /*    findViewById(R.id.image_view_mail1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SendEmail =  new Intent(Intent.ACTION_SENDTO);
                SendEmail.setData(Uri.parse("mailto: "+delegate.getDetalle().getPromotor().getEmailPromotor()));
                startActivity(Intent.createChooser(SendEmail, "Email"));

            }


        });



        findViewById(R.id.image_view_mail2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SendEmail =  new Intent(Intent.ACTION_SENDTO);
                SendEmail.setData(Uri.parse("mailto: "+delegate.getDetalle().getPromotor2().getEmailPromotor()));
                startActivity(Intent.createChooser(SendEmail, "Email"));

            }


        });

        */





    }

    private void dialogAction()
    {
        dialogContratos = new Dialog(this);
        dialogContratos.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogContratos.getWindow().setBackgroundDrawable(new ColorDrawable(00000000));
        dialogContratos.setContentView(R.layout.dialog_bmovil_contratos_contacto_patrimonial);

        ((TextView)dialogContratos.findViewById(R.id.text_num_contrato)).setText("Contrato MX *"+delegate.getDetalle().getContratoBpigo().substring(delegate.getDetalle().getContratoBpigo().length()-5));
        ((TextView)dialogContratos.findViewById(R.id.text_total_cartera)).setText(Tools.formatPatrimonialAmount(delegate.getDetalle().getTotalCartera(), true));

        WindowManager.LayoutParams dialogParams = dialogContratos.getWindow().getAttributes();

        dialogParams.gravity = Gravity.TOP;
        dialogParams.y = suitebancomer.aplicaciones.bmovil.classes.common.Tools.getUsedSpaceInPX(objectsToBeMeasured,this,14);

        dialogContratos.show();

        LinearLayout linear_hide_dialog = (LinearLayout) dialogContratos.findViewById(R.id.linear_hide_dialog);
        ListView list_otros_contratos = (ListView)  dialogContratos.findViewById(R.id.list_otros_contratos);


        linear_hide_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogContratos.dismiss();
            }
        });


        list_otros_contratos.setAdapter(new ContratoPatrimonialAdapter(Session.getInstance(this).getListaContratos(),this,true));
        addCustomFooterToListView(list_otros_contratos);

    }

    private void addCustomFooterToListView(ListView list)
    {
        View footer = new View(this);
        footer.setLayoutParams(new AbsListView.LayoutParams(
                AbsListView.LayoutParams.MATCH_PARENT,
                Math.round( 10 * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))));
        footer.setBackgroundColor(Color.parseColor("#00000000"));
        list.addFooterView(footer);
    }



    public void setDataToView()
    {
        DetallePromotorPatrimonial promotor = delegate.getDetalle().getPromotor();
        DetallePromotorpatrimonial2 promotor2 = delegate.getDetalle().getPromotor2();
        String bankerNumber = promotor.getTelefonoPromotor() + ", ext " + promotor.getExtensionPromotor();
        text_view_banker_number.setText(bankerNumber);
        text_view_banker_name.setText(promotor.getNombrePromotor());
        text_view_office_address.setText(promotor.getDomicilioSucursal() + ","+ " " + promotor.getPoblacionSucursal() + ","+" C.P. " + promotor.getCodigoPostalPromotor() + "," + " " + promotor.getEstadoPromotor()+".");
        text_view_office_name.setText(promotor.getNombreSucursal());
        String contractNumber = delegate.getDetalle().getContratoBpigo();
        int length = contractNumber.length();
        text_view_contract_number.setText("Contrato MX *"+contractNumber.substring(length-5,length));

        /**
         * banquero numero 2
         */
        String bankerNumer2 = promotor2.getTelefonoPromotor() + ", ext "  + promotor2.getExtensionPromotor();
        text_view_banker_number2.setText(bankerNumer2);
        text_view_banker_name2.setText(promotor2.getNombrePromotor());


    }


    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId,response);
    }

    public boolean serverValidation(Date CurrentTime ){

    try {
    SimpleDateFormat df = new SimpleDateFormat("kk:mm");
    Date d1 = df.parse("18:00");
    Date d2 = Session.getInstance(getApplicationContext()).getServerTimePatrimonial();

    Calendar c1 = Calendar.getInstance();
    c1.setTime(d2);
    c1.set(Calendar.HOUR_OF_DAY, d1.getHours());
    c1.set(Calendar.MINUTE, d1.getMinutes());

    if(c1.getTime().equals(CurrentTime) || c1.getTime().after(CurrentTime) )
        return false;
    else
        return true;

    }catch (ParseException e){
    System.out.println("Parce : " + e.getMessage() );
    e.printStackTrace();
        }
        return false;
        }

}
