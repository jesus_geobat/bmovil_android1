package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRegistroRetiroSinTarjetaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRegistroViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjetaViewDto;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtp;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtpBmovil;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;

public class ConfirmacionRegistroRetiroSinTarjetaDelegate extends DelegateBaseAutenticacion {
	private ConfirmacionRegistroRetiroSinTarjetaViewController viewController;
	public final static long CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID = 0x0f34f4c614a018afL;
	private AltaRetiroSinTarjetaDelegate operationDelegate;
	private boolean debePedirContrasena;
	private boolean debePedirNip;
	private TipoOtpAutenticacion tokenAMostrar;
	private boolean debePedirCVV;

	private TipoInstrumento tipoInstrumentoSeguridad;

	public void setViewController(ConfirmacionRegistroRetiroSinTarjetaViewController viewController) {
		this.viewController = viewController;
	}
    //ehmendezr validacion S2
	public ConfirmacionRegistroRetiroSinTarjetaDelegate(AltaRetiroSinTarjetaDelegate delegateOp) {
		this.operationDelegate = delegateOp;
		debePedirContrasena = operationDelegate.mostrarContrasenia();
		debePedirNip = operationDelegate.mostrarNIP();
		debePedirCVV = operationDelegate.mostrarCVV();
		tokenAMostrar = TipoOtpAutenticacion.registro;
		String instrumento = Session.getInstance(SuiteApp.appContext).getSecurityInstrument();
		if (instrumento.equals(Constants.IS_TYPE_DP270)) {
			tipoInstrumentoSeguridad = TipoInstrumento.DP270;
		} else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
			tipoInstrumentoSeguridad = TipoInstrumento.OCRA;
		} else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)||instrumento.equals(Constants.TYPE_SOFTOKEN.S2.value)) {
			tipoInstrumentoSeguridad = TipoInstrumento.SoftToken;
		} else {
			tipoInstrumentoSeguridad = TipoInstrumento.sinInstrumento;
		}
	}

	public void consultaDatos() {
		RetiroSinTarjetaViewDto retiroSinTarjetaViewDto = operationDelegate.getDatosConfirmacionRegistro();
		viewController.setNumeroCelular(retiroSinTarjetaViewDto.getNumeroCelular());
		viewController.setNombre(retiroSinTarjetaViewDto.getNombre());
		viewController.setCompania(retiroSinTarjetaViewDto.getCompania());
	}

	public AltaRetiroSinTarjetaDelegate consultaOperationsDelegate() {
		return operationDelegate;
	}

	public boolean consultaDebePedirContrasena() {
		return debePedirContrasena;
	}

	public boolean consultaDebePedirNIP() {
		return debePedirNip;
	}

	public boolean consultaDebePedirCVV() {
		return debePedirCVV;
	}

	public TipoInstrumento consultaTipoInstrumentoSeguridad() {
		return tipoInstrumentoSeguridad;
	}

	public TipoOtpAutenticacion consultaInstrumentoSeguridad() {
		return tokenAMostrar;
	}

	public boolean validaDatos(){
		boolean esOk = true;
		StringBuilder msg = new StringBuilder("");
		if(debePedirContrasena){
			String contrasena = viewController.pideContrasena();
			if(contrasena.length() == 0){
				esOk = false;
				msg.append(viewController.getString(R.string.confirmation_valorVacio));
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_componenteContrasena));
				msg.append(".");
			}else if(contrasena.length() != Constants.PASSWORD_LENGTH){
				esOk = false;
				msg.append(viewController.getString(R.string.confirmation_valorIncompleto1));
				msg.append(" ");
				msg.append(Constants.PASSWORD_LENGTH);
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_valorIncompleto2));
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_componenteContrasena));
				msg.append(".");
			}
		}else if(debePedirNip){
			String nip = viewController.pideNIP();
			if(nip.length() == 0){
				esOk = false;
				msg.append(viewController.getString(R.string.confirmation_valorVacio));
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_componenteNip));
				msg.append(".");
			}else if(nip.length() != Constants.NIP_LENGTH){
				esOk = false;
				msg.append(viewController.getString(R.string.confirmation_valorIncompleto1));
				msg.append(" ");
				msg.append(Constants.NIP_LENGTH);
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_valorIncompleto2));
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_componenteNip));
				msg.append(".");
			}

		}else if(tokenAMostrar != TipoOtpAutenticacion.ninguno){
			String asm = viewController.pideASM();
			if (asm.equals("")) {
				esOk = false;
				String mensaje = viewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				mensaje += ".";
				msg = new StringBuilder(mensaje);

			} else if (asm.length() != Constants.ASM_LENGTH) {
				esOk = false;
				String mensaje = viewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.ASM_LENGTH;
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}	
				mensaje += ".";
				msg = new StringBuilder(mensaje);
			}
			
		}else if(debePedirCVV){
			String cvv = viewController.pideCVV();
			if (cvv.equals("")) {
				esOk = false;
				String mensaje = viewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				msg = new StringBuilder(mensaje);
			} else if (cvv.length() != Constants.CVV_LENGTH) {
				esOk = false;
				String mensaje = viewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.CVV_LENGTH;
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				msg = new StringBuilder(mensaje);
			}			
		}		
		
		if(!esOk)
			viewController.showInformationAlert(msg.toString());
			
		return esOk;
	}
	
	/**
	 * notifica al delegate de la op actual la confirmacion para realizar la operacion.
	 * 
	 * @param contrasena
	 * @param nip
	 * @param asm
	 * @param cvv
	 */
	public void enviaPeticionOperacion(final String contrasena,final String nip, String asm,final String cvv) {
		if (tokenAMostrar != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken
				&& (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService())) {
			if (SuiteAppCRApi.getSofttokenStatus()) {
				if (ServerCommons.ALLOW_LOG) {
					Log.d("APP", "softToken local");

				}
				asm = loadOtpFromSofttoken(tokenAMostrar);

				operationDelegate.realizaOperacion(viewController, contrasena, nip, asm, cvv);
			} else if (!SuiteAppCRApi.getSofttokenStatus() && PropertiesManager.getCurrent().getSofttokenService()) {
				if (ServerCommons.ALLOW_LOG) {
					Log.d("APP", "softoken compartido");
				}
				GetOtpBmovil otp = new GetOtpBmovil(new GetOtp() {
					/**
					 * @param otp
					 */
					@Override
					public void setOtpRegistro(String otp) {
						if (ServerCommons.ALLOW_LOG) {
							Log.d("APP", "la otp en callback: " + otp);
						}
						operationDelegate.realizaOperacion(viewController, contrasena, nip, otp, cvv);
					}

					/**
					 * @param otp
					 */
					@Override
					public void setOtpCodigo(String otp) {

					}

					/**
					 * @param otp
					 */
					@Override
					public void setOtpCodigoQR(String otp) {

					}
				}, com.bancomer.base.SuiteApp.appContext);

				otp.generateOtpRegistro(operationDelegate.getNumeroCuentaParaRegistroOperacion());
			}
		} else if (tokenAMostrar != TipoOtpAutenticacion.ninguno && (tipoInstrumentoSeguridad == TipoInstrumento.DP270 || tipoInstrumentoSeguridad == TipoInstrumento.OCRA)) {
			operationDelegate.realizaOperacion(viewController, contrasena, nip, asm, cvv);
		} else if (tokenAMostrar == TipoOtpAutenticacion.ninguno) {
			operationDelegate.realizaOperacion(viewController, contrasena, nip, asm, cvv);

		}
	}

	@Override
	public String getEtiquetaCampoNip() {
		return viewController.getString(R.string.confirmation_nip);
	}
	
	@Override
	public String getTextoAyudaNIP() {
		return viewController.getString(R.string.confirmation_ayudaNip);
	}
	
	@Override
	public String getEtiquetaCampoContrasenia() {
		return viewController.getString(R.string.confirmation_contrasena);
	}
	
	@Override
	public String getEtiquetaCampoOCRA() {
		return viewController.getString(R.string.confirmation_ocra);
	}
	
	@Override
	public String getEtiquetaCampoDP270() {
		return viewController.getString(R.string.confirmation_dp270);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenActivado() {
		return viewController.getString(R.string.confirmation_softtokenActivado);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenDesactivado() {
		return viewController.getString(R.string.confirmation_softtokenDesactivado);
	}
	
	@Override
	public String getEtiquetaCampoCVV() {
		return viewController.getString(R.string.confirmation_CVV);
	}
	
	@Override
	public int getTextoEncabezado() {
		return operationDelegate.getTextoEncabezado();
	}
	
	@Override
	public int getNombreImagenEncabezado() {
		return operationDelegate.getNombreImagenEncabezado();
	}
	
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if(response.getStatus() == ServerResponse.OPERATION_ERROR){
			viewController.limpiarCampos();
			BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
			BmovilViewsController bmvc = bmApp.getBmovilViewsController(); 
			BaseViewController current = bmvc.getCurrentViewControllerApp();
			current.showInformationAlert(response.getMessageText());
		}
		operationDelegate.analyzeResponse(operationId, response);
	}
	
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		return tokenAMostrar;
	}

	@Override
	public String loadOtpFromSofttoken(TipoOtpAutenticacion tipoOTP) {
		return loadOtpFromSofttoken(tipoOTP, operationDelegate);
	}
}
