package suitebancomer.aplicaciones.bmovil.classes.common;

public enum TipoInstrumento {
	OCRA("OCRA"),
	DP720("DP720");
	
	public String value;
	
	private TipoInstrumento(String value) {
		this.value = value;
	}
}
