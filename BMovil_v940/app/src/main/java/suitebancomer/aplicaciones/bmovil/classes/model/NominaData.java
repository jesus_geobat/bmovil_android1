package suitebancomer.aplicaciones.bmovil.classes.model;


import org.json.JSONException;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class NominaData implements ParsingHandler {
    public String code;
    public String description;
    public String status;
    public String errorCode;
    public String birthDate;

    public NominaData() {
    }

    public NominaData(String code, String description, String status, String errorCode) {
        this.code = code;
        this.description = description;
        this.status = status;
        this.errorCode = errorCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
    //parsiar respuesta que llego del server
        try {
            birthDate= parser.parserNextObject("response").getJSONObject("person").getString("birthDate");
            code=parser.parserNextObject("status").getString("code");
            description =parser.parserNextObject("status").getString("description");

        } catch (JSONException e) {
            status= parser.parseNextValue("status");
            errorCode= parser.parseNextValue("errorCode");
            description= parser.parseNextValue("description");
        }finally {

        }
    }
}
