package suitebancomer.aplicaciones.bmovil.classes.model;

public class CompraTiempoAire extends FrecuenteMulticanalData{
	
	private Account cuentaOrigen;
	private String celularDestino;
	private String claveCompania;
	private String nombreCompania;
	private String importe;
	private String aliasFrecuente;
	//frecuente correo electronico
	private String correoFrecuente;
	private String tipoOperacion;
	private String frecuenteMulticanal;
	
	public CompraTiempoAire() {
		
	}

	public CompraTiempoAire(Account cuentaOrigen, String celularDestino,
			String claveCompania, String nombreCompania, String importe,
//			String aliasFrecuente, String tipoOperacion, **Frecuente correo electronico
			String aliasFrecuente,String correoFrecuente, String tipoOperacion,
			String frecuenteMulticanal) {
		super();
		this.cuentaOrigen = cuentaOrigen;
		this.celularDestino = celularDestino;
		this.claveCompania = claveCompania;
		this.nombreCompania = nombreCompania;
		this.importe = importe;
		this.aliasFrecuente = aliasFrecuente;
		//Frecuente correo electronico
		this.correoFrecuente = correoFrecuente;
		this.tipoOperacion = tipoOperacion;
		this.frecuenteMulticanal = frecuenteMulticanal;
	}

	public Account getCuentaOrigen() {
		return cuentaOrigen;
	}

	public void setCuentaOrigen(Account cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	public String getCelularDestino() {
		return celularDestino;
	}

	public void setCelularDestino(String celularDestino) {
		this.celularDestino = celularDestino;
	}

	public String getClaveCompania() {
		return claveCompania;
	}

	public void setClaveCompania(String claveCompania) {
		this.claveCompania = claveCompania;
	}

	public String getNombreCompania() {
		return nombreCompania;
	}

	public void setNombreCompania(String nombreCompania) {
		this.nombreCompania = nombreCompania;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getAliasFrecuente() {
		return aliasFrecuente;
	}

	public void setAliasFrecuente(String aliasFrecuente) {
		this.aliasFrecuente = aliasFrecuente;
	}
	//frecuente correo electronico
	public String getCorreoFrecuente() {
		return correoFrecuente;
	}

	public void setCorreoFrecuente(String correoFrecuente) {
		this.correoFrecuente = correoFrecuente;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getFrecuenteMulticanal() {
		return frecuenteMulticanal;
	}

	public void setFrecuenteMulticanal(String frecuenteMulticanal) {
		this.frecuenteMulticanal = frecuenteMulticanal;
	}
	
}
