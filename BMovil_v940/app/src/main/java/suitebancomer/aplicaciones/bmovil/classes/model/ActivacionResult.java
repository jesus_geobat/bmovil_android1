package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ActivacionResult implements ParsingHandler {

	private String fecha;
	private String hora;
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		fecha = parser.parseNextValue("FE");
		hora = parser.parseNextValue("HR");

	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		

	}
	
	public String getFecha() {
		return fecha;
	}
	
	public String getHora() {
		return hora;
	}

}
