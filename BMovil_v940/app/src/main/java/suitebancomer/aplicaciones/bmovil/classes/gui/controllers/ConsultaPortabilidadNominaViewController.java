package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import java.util.ArrayList;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaPortabilidadNominaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TiempoAireDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.GetListCuenta;
import suitebancomer.aplicaciones.bmovil.classes.model.ListPortability;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;

/**
 * Created by elunag on 06/07/16.
 */
public class ConsultaPortabilidadNominaViewController  extends BaseViewController implements View.OnClickListener
{
    LinearLayout vista;
    public static CuentaOrigenViewController componenteCtaOrigen;
    public ListaSeleccionViewController listaSeleccion;
    ConsultaPortabilidadNominaDelegate delegate;
    public BmovilViewsController parentManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.bmovil_consulta_portabilidad_nomina);
        setTitle(R.string.consulta_nomina_title, R.drawable.bmovil_consultar_icono);
        SuiteApp suiteApp = (SuiteApp)getApplication();
        setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
        setDelegate(parentViewsController.getBaseDelegateForKey(ConsultaPortabilidadNominaDelegate.CONSULTA_PORTABILIDAD_NOMINA_DELEGATE_ID));
        delegate = (ConsultaPortabilidadNominaDelegate)getDelegate();
        if (delegate == null) {
            delegate = new ConsultaPortabilidadNominaDelegate();
            parentViewsController.addDelegateToHashMap(ConsultaPortabilidadNominaDelegate.CONSULTA_PORTABILIDAD_NOMINA_DELEGATE_ID, delegate);
            setDelegate(delegate);
        }
        delegate.setConsultaPortabilidadNominaViewController(this);
        vista = (LinearLayout)findViewById(R.id.cambia_nomina_seleccion_view_controller_layout);
        cargaCuentaOrigenComponent();
        componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.transferirInterbancariaF);
        delegate.realizaOperacionGetList();
        init();
        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

    }

    public void init() {
        scaleForCurrentScreen();
    }

    @Override
    public void onClick(final View view) {

    }

    private void scaleForCurrentScreen() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(vista);
    }

    @SuppressWarnings("deprecation")
    public void cargaCuentaOrigenComponent(){
        ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
        componenteCtaOrigen.setDelegate(delegate);
        componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
        componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.cambio_nomina_detalle_cuenta_origen));
        componenteCtaOrigen.init();
        vista.addView(componenteCtaOrigen);

    }

    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

    @SuppressWarnings("deprecation")
    public void cargaListaSeleccionComponent(){
        ArrayList<Object> listaCuetasAMostrar = delegate.getDatosTablaPortability();

        ArrayList<Object> lista = new ArrayList<Object>();
        ArrayList<Object> encabezado = new ArrayList<Object>();
        encabezado.add(null);
        encabezado.add(getString(R.string.consulta_nomina_table_fecha));
        encabezado.add(getString(R.string.consulta_nomina_table_banco_origen));
        encabezado.add(getString(R.string.consulta_nomina_table_estatus));

        lista = listaCuetasAMostrar;

        LinearLayout.LayoutParams params;
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        if (listaSeleccion == null) {
            listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
            listaSeleccion.setDelegate(delegate);
            listaSeleccion.setEncabezado(encabezado);
            listaSeleccion.setLista(lista);
            listaSeleccion.setNumeroColumnas(3) ;
            if (lista.size() == 0) {
                listaSeleccion.setTextoAMostrar("Sin movimientos");
            }else {
                listaSeleccion.setTextoAMostrar(null);
                listaSeleccion.setNumeroFilas(lista.size());
            }
            listaSeleccion.setOpcionSeleccionada(-1);
            listaSeleccion.setTitle("Últimos movimientos");
            listaSeleccion.setSeleccionable(false);
            listaSeleccion.setAlturaFija(true);
            listaSeleccion.setExisteFiltro(false);
            listaSeleccion.setSingleLine(true);
            listaSeleccion.setFijarLista(true);
            listaSeleccion.cargarTabla();
            vista.addView(listaSeleccion);
        }
        else {
            listaSeleccion.setLista(lista);
            listaSeleccion.setAlturaFija(true);
            listaSeleccion.setOpcionSeleccionada(-1);
            listaSeleccion.cargarTabla();
        }
        componenteCtaOrigen.getImgDerecha().setEnabled(true);
        componenteCtaOrigen.getImgIzquierda().setEnabled(true);
        componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev)
    {
        parentViewsController.setActivityChanging(Boolean.FALSE);
        if(MotionEvent.ACTION_DOWN == ev.getAction()) {
            listaSeleccion.setMarqueeEnabled(false);
        }else if(MotionEvent.ACTION_UP == ev.getAction()) {
            listaSeleccion.setMarqueeEnabled(true);
        }
        listaSeleccion.getParent().requestDisallowInterceptTouchEvent(false);
        return super.dispatchTouchEvent(ev);
    }

    public void opcionSeleccionada(int opcionSeleccionada)
    {
        final String bankNumber = GetListCuenta.getInstance().getPortabilities().get(opcionSeleccionada).getExternalBank().getId();
        final String createdReferenceNumber = GetListCuenta.getInstance().getPortabilities().get(opcionSeleccionada).getCreatedReferenceNumber();
        final String companyNumber = GetListCuenta.getInstance().getPortabilities().get(opcionSeleccionada).getCompany().getId();
        final String  referenceNumber = componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getNumber();

        delegate.realizaOperacionGetPortability(referenceNumber,bankNumber, createdReferenceNumber, companyNumber);
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void goBack() {
        parentViewsController.removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
        super.goBack();
    }

    @Override
    public void onUserInteraction() {
        //super.onUserInteraction();
        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();

    }
}
