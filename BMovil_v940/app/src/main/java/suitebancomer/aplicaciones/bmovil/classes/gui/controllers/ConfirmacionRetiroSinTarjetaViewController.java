package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.common.ToolsCommons;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomercoms.classes.common.PropertiesManager;
import tracking.TrackingHelper;

public class ConfirmacionRetiroSinTarjetaViewController extends BaseViewController implements OnClickListener {

    private ImageView imageTitulo;
    private TextView lblTitulo;

    private LinearLayout layoutContenedorDatosRetiro;
    private LinearLayout layoutContenedorDatosDineroMovil;

    private TextView lblImporte;
    private TextView lblCuentaRetiro;
    private TextView lblNumeroCelular;
    private TextView lblConcepto;

    private TextView lblCuentaRetiro2;
    private TextView lblNumeroCelular2;
    private TextView lblConcepto2;
    private ImageView imageCompania;
    private TextView lblNombre;

    private LinearLayout contenedorContrasena;
    private LinearLayout contenedorCampoTarjeta;
    private LinearLayout contenedorNIP;
    private LinearLayout contenedorASM;
    private LinearLayout contenedorCVV;

    private EditText contrasena;
    private EditText tarjeta;
    private EditText nip;
    private EditText asm;
    private EditText cvv;

    private TextView instruccionesTarjeta;
    private TextView instruccionesNIP;
    private TextView instruccionesASM;
    private TextView instruccionesCVV;

    private Button confirmarButton;

    private ConfirmacionRetiroSinTarjetaDelegate confirmacionRetiroSinTarjetaDelegate;

    public BmovilViewsController parentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER, R.layout.layout_bmovil_confirmacion_retiro_sin_tarjeta);

        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

        TrackingHelper.trackState("confirma", parentManager.estados);

        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        setDelegate(getParentViewsController().getBaseDelegateForKey(ConfirmacionRetiroSinTarjetaDelegate.CONFIRMACION_RETIRO_SIN_TARJETA_DELEGATE_DELEGATE_ID));

        confirmacionRetiroSinTarjetaDelegate = (ConfirmacionRetiroSinTarjetaDelegate) getDelegate();
        confirmacionRetiroSinTarjetaDelegate.setConfirmacionRetiroSinTarjetaViewController(this);

        findViews();

        confirmacionRetiroSinTarjetaDelegate.consultaDatos();

        configuraPantalla();
        moverScroll();

        contrasena.addTextChangedListener(new BmovilTextWatcher(this));
        nip.addTextChangedListener(new BmovilTextWatcher(this));
        asm.addTextChangedListener(new BmovilTextWatcher(this));
        cvv.addTextChangedListener(new BmovilTextWatcher(this));
        tarjeta.addTextChangedListener(new BmovilTextWatcher(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    private void configuraPantalla() {
        if (confirmacionRetiroSinTarjetaDelegate.getTipoRetiro() == AltaRetiroSinTarjetaDelegate.TipoRetiro.RETIRO_SIN_TARJETA) {
            imageTitulo.setImageDrawable(getResources().getDrawable(R.drawable.ic_parami));
            lblTitulo.setText(R.string.bmovil_unificacion_label_parami);
            layoutContenedorDatosRetiro.setVisibility(View.VISIBLE);
            layoutContenedorDatosDineroMovil.setVisibility(View.GONE);
        } else {
            imageTitulo.setImageDrawable(getResources().getDrawable(R.drawable.ic_paraalguienmas));
            lblTitulo.setText(R.string.bmovil_unificacion_label_paraalguienmas);
            lblTitulo.setTextColor(Color.parseColor("#82c645"));
            layoutContenedorDatosRetiro.setVisibility(View.GONE);
            layoutContenedorDatosDineroMovil.setVisibility(View.VISIBLE);
        }

        mostrarContrasena(confirmacionRetiroSinTarjetaDelegate.consultaDebePedirContrasena());
        mostrarNIP(confirmacionRetiroSinTarjetaDelegate.consultaDebePedirNIP());
        mostrarASM(confirmacionRetiroSinTarjetaDelegate.consultaInstrumentoSeguridad());
        mostrarCVV(confirmacionRetiroSinTarjetaDelegate.consultaDebePedirCVV());
        mostrarCampoTarjeta(confirmacionRetiroSinTarjetaDelegate.mostrarCampoTarjeta());

        confirmarButton.setOnClickListener(this);
    }

    public void mostrarContrasena(boolean visibility) {
        contenedorContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
//            campoContrasena.setText(confirmacionRetiroSinTarjetaDelegate.getEtiquetaCampoContrasenia());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
            contrasena.setFilters(userFilterArray);
            contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    public void mostrarNIP(boolean visibility) {
        contenedorNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
//            campoNIP.setText(confirmacionRetiroSinTarjetaDelegate.getEtiquetaCampoNip());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
            nip.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(tarjeta);
            nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = confirmacionRetiroSinTarjetaDelegate.getTextoAyudaNIP();
            if (instrucciones.equals("")) {
                instruccionesNIP.setVisibility(View.GONE);
            } else {
                instruccionesNIP.setVisibility(View.VISIBLE);
                instruccionesNIP.setText(instrucciones);
            }
        } else {
            nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP) {
        switch (tipoOTP) {
            case ninguno:
                contenedorASM.setVisibility(View.GONE);
                asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
                break;
            case codigo:
            case registro:
                contenedorASM.setVisibility(View.VISIBLE);
                InputFilter[] userFilterArray = new InputFilter[1];
                userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
                asm.setFilters(userFilterArray);
                cambiarAccionTexto(contrasena);
                cambiarAccionTexto(tarjeta);
                cambiarAccionTexto(nip);
                asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
                break;
        }

        Constants.TipoInstrumento tipoInstrumento = confirmacionRetiroSinTarjetaDelegate.consultaTipoInstrumentoSeguridad();

        switch (tipoInstrumento) {
            case OCRA:
//                campoASM.setText(confirmacionRetiroSinTarjetaDelegate.getEtiquetaCampoOCRA());
                break;
            case DP270:
//                campoASM.setText(confirmacionRetiroSinTarjetaDelegate.getEtiquetaCampoDP270());
                break;
            case SoftToken:
                if (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
                    asm.setText(Constants.DUMMY_OTP);
                    asm.setEnabled(false);
//                    campoASM.setText(confirmacionRetiroSinTarjetaDelegate.getEtiquetaCampoSoftokenActivado());
                } else {
                    asm.setText("");
                    asm.setEnabled(true);
//                    campoASM.setText(confirmacionRetiroSinTarjetaDelegate.getEtiquetaCampoSoftokenDesactivado());
                }
                break;
            default:
                break;
        }
        String instrucciones = confirmacionRetiroSinTarjetaDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
        if (instrucciones.equals("")) {
            instruccionesASM.setVisibility(View.GONE);
        } else {
            instruccionesASM.setVisibility(View.VISIBLE);
            instruccionesASM.setText(instrucciones);
        }
    }

    private void mostrarCampoTarjeta(boolean visibility) {
        contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
//            campoTarjeta.setText(confirmacionRetiroSinTarjetaDelegate.getEtiquetaCampoTarjeta());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(5);
            tarjeta.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = confirmacionRetiroSinTarjetaDelegate.getTextoAyudaTarjeta();
            if (instrucciones.equals("")) {
                instruccionesTarjeta.setVisibility(View.GONE);
            } else {
                instruccionesTarjeta.setVisibility(View.VISIBLE);
                instruccionesTarjeta.setText(instrucciones);
            }
        } else {
            tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    private void cambiarAccionTexto(EditText campo) {
        if (campo.getVisibility() == View.VISIBLE) {
            campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        }
    }

    public String pideContrasena() {
        if (contrasena.getVisibility() == View.GONE) {
            return "";
        } else {
            return contrasena.getText().toString();
        }
    }

    public String pideNIP() {
        if (nip.getVisibility() == View.GONE) {
            return "";
        } else {
            return nip.getText().toString();
        }
    }

    public String pideASM() {
        if (asm.getVisibility() == View.GONE) {
            return "";
        } else {
            return asm.getText().toString();
        }
    }

    public String pideCVV() {
        if (cvv.getVisibility() == View.GONE) {
            return "";
        } else {
            return cvv.getText().toString();
        }
    }

    public void mostrarCVV(boolean visibility) {
        contenedorCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);

        if (visibility) {
//            campoCVV.setText(confirmacionRetiroSinTarjetaDelegate.getEtiquetaCampoCVV());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
            cvv.setFilters(userFilterArray);
            String instrucciones = confirmacionRetiroSinTarjetaDelegate.getTextoAyudaCVV();
            instruccionesCVV.setText(instrucciones);
            instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(nip);
            cambiarAccionTexto(asm);
            cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == confirmarButton && !parentViewsController.isActivityChanging()) {
            botonConfirmarClick();
        }
    }

    public void botonConfirmarClick() {
        confirmacionRetiroSinTarjetaDelegate.enviaPeticionOperacion();
        if (confirmacionRetiroSinTarjetaDelegate.res) {
            if (Server.ALLOW_LOG)
                Log.d("titulo pantalla", confirmacionRetiroSinTarjetaDelegate.getTextoTituloResultado());
        }
    }

    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        confirmacionRetiroSinTarjetaDelegate.analyzeResponse(operationId, response);
    }

    private void findViews() {
        imageTitulo = (ImageView) findViewById(R.id.imagen_titulo);
        lblTitulo = (TextView) findViewById(R.id.texto_titulo);

        layoutContenedorDatosRetiro = (LinearLayout) findViewById(R.id.layout_contenedor_datos_retiro);
        layoutContenedorDatosDineroMovil = (LinearLayout) findViewById(R.id.layout_contenedor_datos_dinero_movil);

        lblImporte = (TextView) findViewById(R.id.valor_importe);
        lblCuentaRetiro = (TextView) findViewById(R.id.valor_cuenta_retiro);
        lblNumeroCelular = (TextView) findViewById(R.id.valor_numero_celular);
        lblConcepto = (TextView) findViewById(R.id.valor_concepto);

        lblCuentaRetiro2 = (TextView) findViewById(R.id.valor_cuenta_retiro2);
        lblNumeroCelular2 = (TextView) findViewById(R.id.valor_numero_celular2);
        lblConcepto2 = (TextView) findViewById(R.id.valor_concepto2);
        imageCompania = (ImageView) findViewById(R.id.imagen_compania);
        lblNombre = (TextView) findViewById(R.id.valor_nombre);

        contenedorContrasena = (LinearLayout) findViewById(R.id.campo_confirmacion_contrasena_layout);
        contenedorCampoTarjeta = (LinearLayout) findViewById(R.id.campo_confirmacion_campotarjeta_layout);
        contenedorNIP = (LinearLayout) findViewById(R.id.campo_confirmacion_nip_layout);
        contenedorASM = (LinearLayout) findViewById(R.id.campo_confirmacion_asm_layout);
        contenedorCVV = (LinearLayout) findViewById(R.id.campo_confirmacion_cvv_layout);

        contrasena = (EditText) contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
        tarjeta = (EditText) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
        nip = (EditText) contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
        asm = (EditText) contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
        cvv = (EditText) contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);

        instruccionesTarjeta = (TextView) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);
        instruccionesNIP = (TextView) contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
        instruccionesASM = (TextView) contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
        instruccionesCVV = (TextView) contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);

        confirmarButton = (Button) findViewById(R.id.confirmacion_confirmar_button);
    }

    public void limpiarCampos() {
        contrasena.setText("");
        nip.setText("");
        asm.setText("");
        cvv.setText("");
        tarjeta.setText("");
    }


    public String pideTarjeta() {
        if (tarjeta.getVisibility() == View.GONE) {
            return "";
        } else
            return tarjeta.getText().toString();
    }

    public void setImporte(String importe) {
        lblImporte.setText(importe);
    }

    public void setCuentaRetiro(String cuentaRetiro) {
        lblCuentaRetiro.setText(cuentaRetiro);
        lblCuentaRetiro2.setText(cuentaRetiro);
    }

    public void setNumeroCelular(String numeroCelular) {
        lblNumeroCelular.setText(numeroCelular);
        lblNumeroCelular2.setText(ToolsCommons.formatPhoneNumber(numeroCelular));
    }

    public void setConcepto(String concepto) {
        lblConcepto.setText(concepto);
        lblConcepto2.setText(concepto);
    }

    public void setCompania(String compania) {
        if (compania.equals("TELCEL")) {
            imageCompania.setImageDrawable(getResources().getDrawable(R.drawable.im_telcel));
        } else if (compania.equals("MOVISTAR")) {
            imageCompania.setImageDrawable(getResources().getDrawable(R.drawable.im_movistar));
        } else if (compania.equals("IUSACELL")) {
            imageCompania.setImageDrawable(getResources().getDrawable(R.drawable.im_iusacell));
        } else if (compania.equals("UNEFON")) {
            imageCompania.setImageDrawable(getResources().getDrawable(R.drawable.im_unefon));
        } else if (compania.equals("NEXTEL")) {
            imageCompania.setImageDrawable(getResources().getDrawable(R.drawable.im_nextel));
        }
    }

    public void setNombre(String nombre) {
            lblNombre.setText(nombre);
    }
}
