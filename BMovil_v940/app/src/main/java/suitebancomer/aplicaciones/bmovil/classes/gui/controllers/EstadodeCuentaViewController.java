package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import tracking.TrackingHelper;

import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.EstadodeCuentaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Periodo;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import tracking.TrackingHelper;

/**
 * @author Carlos Santoyo
 */

public class EstadodeCuentaViewController extends BaseViewController implements OnClickListener {

	private LinearLayout vista;
	private LinearLayout contenedorPrincipal;
	private TextView txtSelecciona;
	private TextView txtFormato;
	public TextView txtUltimos24;
	private TextView txtHistorico;
	private TextView txtCorte1;
	private TextView comboboxFormato;
	public TextView comboboxUltimos24;
	private TextView comboboxHistorico;
	private TextView comboboxCorte;
	private String TipoFormato;
	private String operacion = "Consulta estado financiero";
	private ArrayList <Periodo> periodos;
	private String Periodo;
	private String Corte;
	private TextView txtContraseña;
	private TextView txtConfirmaC;
	private EditText edTxtContraseña;
	private EditText edTxtConfirmaC;
	private String contraseña;
	private String confirmacion;
	private Button continuar;
	private String[] arreglo;
	String[] arreglocorte;
	private int numeroCorte;
	private String referencia;
	private Account account;
	AlertDialog.Builder builder1;
	AlertDialog.Builder builder2;
	AlertDialog dialog;
	AlertDialog dialog1;

	private EstadodeCuentaDelegate estadodeCuentaDelegate;
	public CuentaOrigenViewController componenteCtaOrigen;
	public BmovilViewsController parentManager;

	public String getOperacion(){
		return operacion;
	}

	public void setOperacion(String op){
		operacion = op;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public String getConfirmacion() {
		return confirmacion;
	}

	public void setConfirmacion(String confirmacion) {
		this.confirmacion = confirmacion;
	}

	public String getPeriodo() {
		return Periodo;
	}

	public void setPeriodo(String periodo) {
		Periodo = periodo;
	}

	public String getCorte() {
		return Corte;
	}

	public void setCorte(String corte) {
		Corte = corte;
	}

	public String getTipoFormato() {
		return TipoFormato;
	}

	public void setTipoFormato(String tipoFormato) {
		TipoFormato = tipoFormato;
	}

	public String getReferencia(){return referencia;}

	public void setReferencia(String ref){ referencia=ref; }



	/**
	 * Default constructor for this activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_consulta_estado_de_cuenta_nuevo);
		setTitle(R.string.consultar_estados_de_cuenta_title, R.drawable.bmovil_consultar_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

		TrackingHelper.trackState("movimientos", parentManager.estados);

		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(EstadodeCuentaDelegate.ESTADODECUENTA_DELEGATE_ID));
		estadodeCuentaDelegate = (EstadodeCuentaDelegate)getDelegate();
		estadodeCuentaDelegate.setEstadodeCuentaViewController(EstadodeCuentaViewController.this);
		init();
		account = (Account) getIntent().getSerializableExtra("bundle");
		showCuentaOrigen();
		estadodeCuentaDelegate.cargarPeriodos();
		comboboxFormato.setText("PDF");
		setTipoFormato("PDF");

	}


	/**
	 * Busca las vistas usadas y guarda sus referencias.
	 */
	private void findViews(){

		//txtSelecciona = (TextView) findViewById(R.id.estado_de_cuenta_nuevo_txtselecciona);
		txtFormato= (TextView) findViewById(R.id.txtFormato);
		txtUltimos24 = (TextView) findViewById(R.id.txtUltimos24);
		txtHistorico = (TextView) findViewById(R.id.txtHistorico);
		txtCorte1 = (TextView) findViewById(R.id.txtCorte);
		txtContraseña = (TextView) findViewById(R.id.txtContraseña);
		txtConfirmaC = (TextView) findViewById(R.id.txtConfirmaC);
		comboboxFormato = (TextView) findViewById(R.id.comboboxFormato);
		comboboxUltimos24 = (TextView) findViewById(R.id.comboboxUltimos24);
		comboboxUltimos24.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onPeriodosClick(v);
			}
		});
		comboboxHistorico = (TextView) findViewById(R.id.comboboxHistorico);
		comboboxCorte = (TextView) findViewById(R.id.comboboxCorte);
		edTxtContraseña = (EditText) findViewById(R.id.edtxtContraseña);
		edTxtConfirmaC = (EditText) findViewById(R.id.edtxtConfirmaC);
		continuar = (Button) findViewById(R.id.estado_de_cuenta_boton_continuar);
		vista = (LinearLayout)findViewById(R.id.estado_de_cuenta_layout);
		contenedorPrincipal = (LinearLayout)findViewById(R.id.contenedorPrincipal);
		TextView labelaviso = (TextView) findViewById(R.id.lblAviso1);
		Session session = Session.getInstance(SuiteApp.appContext);
		labelaviso.setText("Te enviaremos el estado de cuenta al correo "+ session.getEmail());
	}

	/**
	 * Redimensiona las vistas para que mantegan la relacion de aspecto en cualquier pantalla.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scalePaddings(contenedorPrincipal);
		//guiTools.scale(txtSelecciona, true);
		guiTools.scale(txtFormato, true);
		guiTools.scale(txtUltimos24, true);
		guiTools.scale(txtHistorico, true);
		guiTools.scale(txtCorte1, true);
		guiTools.scale(txtContraseña, true);
		guiTools.scale(txtConfirmaC, true);
		guiTools.scale(comboboxFormato, true);
		guiTools.scale(comboboxUltimos24, true);
		guiTools.scale(comboboxHistorico, true);
		guiTools.scale(comboboxCorte, true);
		guiTools.scale(edTxtContraseña, true);
		guiTools.scale(edTxtConfirmaC, true);
		guiTools.scale(vista);
	}

	private void init() {
		findViews();
		scaleForCurrentScreen();

	}

	@SuppressWarnings("deprecation")
	public void showCuentaOrigen(){
		ArrayList<Account> listaCuetasAMostrar = estadodeCuentaDelegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		if(estadodeCuentaDelegate.getCuentaOrigen()== null){
			componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
			componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
			componenteCtaOrigen.setDelegate(estadodeCuentaDelegate);
			componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
			componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
			componenteCtaOrigen.init();
			vista.addView(componenteCtaOrigen);
		}else {

			componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
			componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
			componenteCtaOrigen.setDelegate(estadodeCuentaDelegate);
			componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
			componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(estadodeCuentaDelegate.getCuentaOrigen()));
			componenteCtaOrigen.init();
			vista.addView(componenteCtaOrigen);
		}

	}

	/*public void onFormatoSeleccionado(String tipo) {
		comboboxFormato.setText(tipo);
		setTipoFormato(tipo);
		txtUltimos24.setVisibility(View.VISIBLE);
		comboboxUltimos24.setVisibility(View.VISIBLE);
		comboboxUltimos24.setText("Selecciona");
		estadodeCuentaDelegate.cargarPeriodos();

	}


	public void onFormatoClick(View view) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		final String[] arreglopdf = new String[] {
				"PDF"
		};

		builder.setTitle(
				R.string.bmovil_consultar_dineromovil_movimientos_tipo_titulo)
				.setItems(arreglopdf, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						onFormatoSeleccionado(arreglopdf[which]);

					}
				}).show();
	}*/

	public void onPeriodoSeleccionado(String periodo) {
		arreglocorte = null;

		String conformato = periodo;
		String[] separado = conformato.split(" ");
		String mesletra = separado[0];
		String mesNumero = " ";
		String año = separado[1];

		if (mesletra.equals("Enero")) {
			mesNumero = "01";

		} else if (mesletra.equals("Febrero")) {
			mesNumero = "02";

		} else if (mesletra.equals("Marzo")) {
			mesNumero = "03";

		} else if (mesletra.equals("Abril")) {
			mesNumero = "04";

		} else if (mesletra.equals("Mayo")) {
			mesNumero = "05";

		} else if (mesletra.equals("Junio")) {
			mesNumero = "06";

		} else if (mesletra.equals("Julio")) {
			mesNumero = "07";

		} else if (mesletra.equals("Agosto")) {
			mesNumero = "08";

		} else if (mesletra.equals("Septiembre")) {
			mesNumero = "09";

		} else if (mesletra.equals("Octubre")) {
			mesNumero = "10";

		} else if (mesletra.equals("Noviembre")) {
			mesNumero = "11";

		} else if (mesletra.equals("Diciembre")) {
			mesNumero = "12";

		}
		setPeriodo(año+mesNumero);
		comboboxUltimos24.setText(periodo);
		txtCorte1.setVisibility(View.VISIBLE);
		comboboxCorte.setVisibility(View.VISIBLE);
		edTxtConfirmaC.setVisibility(View.VISIBLE);
		txtConfirmaC.setVisibility(View.VISIBLE);
		txtContraseña.setVisibility(View.VISIBLE);
		edTxtContraseña.setVisibility(View.VISIBLE);


		int tamaño = 0;
		for(int n=0; n< periodos.size(); n++){
			String periodoigual = periodos.get(n).getPeriodo();
			if(periodoigual.equals(Periodo)){
				tamaño= tamaño +1;
			}
		}

		Log.d("tamaño", String.valueOf(tamaño));

		arreglocorte = new String[tamaño];
		int i=0;

		for(int n=0; n< periodos.size(); n++){
			String periodoigual = periodos.get(n).getPeriodo();
			Log.d("fecha de corte",periodos.get(n).getFechaCorte());
			if(periodoigual.equals(Periodo)){
				arreglocorte[i] = periodos.get(n).getFechaCorte();
				i++;
			}
		}

		if(tamaño == 1){
			comboboxCorte.setText(arreglocorte[0]);
			setCorte(arreglocorte[0]);
			for(int n=0; n< periodos.size(); n++){
				String periodoigual = periodos.get(n).getPeriodo();
				String corteigual = periodos.get(n).getFechaCorte();
				if(periodoigual.equals(Periodo)&& corteigual.equals(Corte)){
					setReferencia(periodos.get(n).getReferencia());
				}
			}


		}else if(tamaño>1) {
			comboboxCorte.setClickable(true);
			comboboxCorte.setText("Selecciona");
			comboboxCorte.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onCorteClick(v);
				}
			});
		}
	}


	public void onPeriodosClick(View view) {

		 builder1 = new AlertDialog.Builder(this);


		periodos = estadodeCuentaDelegate.getTotalPeriodos().getPeriodos();


		arreglo = new String[periodos.size()];
		int i=0;

		for(int n=0; n< periodos.size(); n++){
			if(n==0){
				String sinformato = periodos.get(n).getPeriodo();
				String año = sinformato.substring(0,4);
				String mes = sinformato.substring(4,6);
				String mesletra = " ";
				if (mes.equals("01")) {
					mesletra = "Enero";

				} else if (mes.equals("02")) {
					mesletra = "Febrero";

				} else if (mes.equals("03")) {
					mesletra = "Marzo";

				} else if (mes.equals("04")) {
					mesletra = "Abril";

				} else if (mes.equals("05")) {
					mesletra = "Mayo";

				} else if (mes.equals("06")) {
					mesletra = "Junio";

				} else if (mes.equals("07")) {
					mesletra = "Julio";

				} else if (mes.equals("08")) {
					mesletra = "Agosto";

				} else if (mes.equals("09")) {
					mesletra = "Septiembre";

				} else if (mes.equals("10")) {
					mesletra = "Octubre";

				} else if (mes.equals("11")) {
					mesletra = "Noviembre";

				} else if (mes.equals("12")) {
					mesletra = "Diciembre";

				}
				arreglo[i] = mesletra+ " "+año;
			}else if(periodos.get(n).getPeriodo() != periodos.get(n-1).getPeriodo()){
				String sinformato = periodos.get(n).getPeriodo();
				String año = sinformato.substring(0,4);
				String mes = sinformato.substring(4,6);
				String mesletra = " ";
				if (mes.equals("01")) {
					mesletra = "Enero";

				} else if (mes.equals("02")) {
					mesletra = "Febrero";

				} else if (mes.equals("03")) {
					mesletra = "Marzo";

				} else if (mes.equals("04")) {
					mesletra = "Abril";

				} else if (mes.equals("05")) {
					mesletra = "Mayo";

				} else if (mes.equals("06")) {
					mesletra = "Junio";

				} else if (mes.equals("07")) {
					mesletra = "Julio";

				} else if (mes.equals("08")) {
					mesletra = "Agosto";

				} else if (mes.equals("09")) {
					mesletra = "Septiembre";

				} else if (mes.equals("10")) {
					mesletra = "Octubre";

				} else if (mes.equals("11")) {
					mesletra = "Noviembre";

				} else if (mes.equals("12")) {
					mesletra = "Diciembre";

				}

				arreglo[i] = mesletra+ " "+año;
			}
			i++;
		}


		builder1.setTitle(
				R.string.bmovil_consultar_dineromovil_movimientos_tipo_titulo)
				.setItems(arreglo, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						onPeriodoSeleccionado(arreglo[which]);
					}
				});
		dialog = builder1.create();
		dialog.show();
	}

	public void onCorteSeleccionado(String corte) {
		comboboxCorte.setText(corte);
		setCorte(corte);
		edTxtConfirmaC.setVisibility(View.VISIBLE);
		txtConfirmaC.setVisibility(View.VISIBLE);
		txtContraseña.setVisibility(View.VISIBLE);
		edTxtContraseña.setVisibility(View.VISIBLE);



		for(int n=0; n< periodos.size(); n++){
			String periodoigual = periodos.get(n).getPeriodo();
			String corteigual = periodos.get(n).getFechaCorte();
			if(periodoigual.equals(Periodo)&& corteigual.equals(Corte)){
				setReferencia(periodos.get(n).getReferencia());
			}
		}

	}


	public void onCorteClick(View view) {

		builder2 = new AlertDialog.Builder(this);
		builder2.setTitle(
				R.string.bmovil_consultar_dineromovil_movimientos_tipo_titulo)
				.setItems(arreglocorte, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						numeroCorte = which;
						onCorteSeleccionado(arreglocorte[which]);
					}
				});
		dialog1 = builder2.create();
		dialog1.show();
	}


	public void onVizualizarClick(View view) {
			setContraseña(edTxtContraseña.getText().toString());
			setConfirmacion( edTxtConfirmaC.getText().toString());

		Log.w("Corte nuemro", String.valueOf(numeroCorte));

		estadodeCuentaDelegate.validarDatos(getTipoFormato(), getPeriodo(), getCorte(), getContraseña(), getConfirmacion(), referencia, numeroCorte);

	}


	@SuppressWarnings("deprecation")
	public void llenaListaDatos(){
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
	}

	public CuentaOrigenViewController getComponenteCtaOrigen() {
		return componenteCtaOrigen;
	}

	public void resetViews (){
		arreglocorte=null;
		arreglo=null;
		comboboxFormato.setText("PDF");
		comboboxUltimos24.setText("Selecciona");
		txtCorte1.setVisibility(View.GONE);
		comboboxCorte.setVisibility(View.GONE);
		edTxtContraseña.setText("");
		edTxtConfirmaC.setText("");

	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		estadodeCuentaDelegate.analyzeResponse(operationId, response);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		setHabilitado(true);
		getParentViewsController().setCurrentActivityApp(this);
		if (estadodeCuentaDelegate != null) {
			estadodeCuentaDelegate.setCallerController(this);
		}
		Periodo = null;
		Corte=null;
		contraseña=null;
		confirmacion=null;

	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
		resetViews();
		Log.e("Dialog", String.valueOf(dialog));
		if(dialog != null){
			dialog.dismiss();
		}
		if(dialog1 != null){
			dialog1.dismiss();
		}
	}

	@Override
	public void goBack() {
		super.goBack();
		((BmovilViewsController)parentViewsController).touchMenu();
		((BmovilViewsController)parentViewsController).showMenuPrincipal(true);
	}



	@Override
	public void onClick(View v) {


		if(estadodeCuentaDelegate.res) {
			//ARR
			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();

			//ARR
			paso2OperacionMap.put("evento_paso2", "event47");
			paso2OperacionMap.put("&&products", "operaciones;consultar estado financiero");
			paso2OperacionMap.put("eVar12", "paso2:revisa y autoriza");

			TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
		}
	}

}

