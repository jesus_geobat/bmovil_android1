package suitebancomer.aplicaciones.bmovil.classes.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;


/**
 * Created by OOROZCO on 3/11/16.
 */
public class ContratosPatrimonial implements ParsingHandler {


    private ArrayList<ContratoPatrimonial> listaContratos = new ArrayList<ContratoPatrimonial>();


    public ArrayList<ContratoPatrimonial> getListaContratos() {
        return listaContratos;
    }

    public void process(Parser parser) throws IOException, ParsingException {

    }


    public void process(ParserJSON parser) throws IOException, ParsingException {

        try{

       JSONObject responceJSON = parser.parserNextObject("response");


            JSONArray array = responceJSON.getJSONArray("cuentasPat");

     // JSONArray array = parser.parseNextValueWithArray("cuentasPat");

        for(int index = 0 ; index < array.length() ; index ++)
        {
            try {
                JSONObject contract = array.getJSONObject(index);
                listaContratos.add(new ContratoPatrimonial(contract.getString("numCuenta"),contract.getString("totalCartera"),contract));
            } catch (JSONException e) {
                Log.e("Mario","JSonError" + e.getMessage());
                e.printStackTrace();
            }
        }
        }
        catch (JSONException e ){

            JSONArray array = parser.parseNextValueWithArray("cuentasPat");

            for(int index = 0 ; index < array.length() ; index ++)
            {
                try {
                    JSONObject contract = array.getJSONObject(index);
                    listaContratos.add(new ContratoPatrimonial(contract.getString("numCuenta"),contract.getString("totalCartera"),contract));
                } catch (JSONException r) {
                    Log.e("Mario","JSonError" + r.getMessage());
                    r.printStackTrace();
                }
            }

            Log.e("ExcepcionJSONPat",e.getMessage());
            e.printStackTrace();
        }

    }
}
