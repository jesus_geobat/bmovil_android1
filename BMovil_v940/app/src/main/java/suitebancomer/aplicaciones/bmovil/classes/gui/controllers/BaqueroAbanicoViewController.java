package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuPrincipalDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.BanqueroPersonal;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by Alberto on 24/11/16.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class BaqueroAbanicoViewController extends BaseViewController implements View.OnClickListener{

    public static boolean pass = false;
    private BmovilViewsController parentManager;
    private ImageView btntoggle_1;
    private ImageView icocompartir;
    private ImageView icoagendarllamada;
    private ImageView icollamada;
    private ImageView icomail;
    private ImageView icochat;

    private TextView txtNombreGestor;
    private TextView txtCorreoGestor;
    final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 0, R.layout.layout_bmovil_banquero_abanico);
        overridePendingTransition(R.anim.alpha, R.anim.alpha_1);
        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        SuiteApp suiteApp = (SuiteApp)getApplication();
        setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
        setDelegate(parentViewsController.getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID));
        if(!pass){
            ImageView animationTarget = (ImageView) this.findViewById(R.id.btntoggle_1);
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_90);
            animationTarget.startAnimation(animation);
            try{ Thread.sleep(150); }catch(InterruptedException e){ }
        }
        initValues();
        configurarPantalla();
        txtNombreGestor.setText(BanqueroPersonal.getInstance().getNombreGestor());
        txtCorreoGestor.setText(BanqueroPersonal.getInstance().getEmail().toLowerCase());
        startAnimation(icocompartir);
        startAnimation_AgendarLlamada(icoagendarllamada);
        startAnimation_llamada(icollamada);
        startAnimation_correo(icomail);
        startAnimation_chat(icochat);
    }

    private void initValues()
    {
        txtNombreGestor = (TextView) findViewById(R.id.txtNombreGestor);
        txtCorreoGestor = (TextView) findViewById(R.id.txtCorreoGestor);

        btntoggle_1 = (ImageView) findViewById(R.id.btntoggle_1);
        btntoggle_1.setOnClickListener(this);

        icocompartir = (ImageView) findViewById(R.id.icocompartir);
        icocompartir.setOnClickListener(this);

        icoagendarllamada = (ImageView) findViewById(R.id.icoagendarllamada);
        icoagendarllamada.setOnClickListener(this);

        icollamada = (ImageView) findViewById(R.id.icollamada);
        icollamada.setOnClickListener(this);

        icomail = (ImageView) findViewById(R.id.icomail);
        icomail.setOnClickListener(this);

        icochat = (ImageView) findViewById(R.id.icochat);
        icochat.setOnClickListener(this);

        pass = false;
    }
    private void configurarPantalla()
    {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());

        gTools.scale(findViewById(R.id.layout_base_gestor_remoto), false);
        gTools.scale(findViewById(R.id.layout_title_1), false);
        gTools.scale(findViewById(R.id.layout_title), false);
        gTools.scale(findViewById(R.id.layoutScroll), false);
        gTools.scale(findViewById(R.id.layout_title_gestor_remoto), false);
        gTools.scale(findViewById(R.id.layout_title_2), false);
        gTools.scale(findViewById(R.id.layout_title_3), false);

        gTools.scale(findViewById(R.id.layout_title_gestor_remoto_1), false);
        gTools.scale(findViewById(R.id.layout_title_21), false);
        gTools.scale(findViewById(R.id.layout_title_32), false);
        gTools.scale(findViewById(R.id.layout_title_gestor_remoto_3), false);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
    public void llamarBanqueroPersonal(final String telefonoGestor) {
        try {
            final Intent sIntent = new Intent(Intent.ACTION_CALL,Uri.parse(String.format("tel:%s",Uri.encode(telefonoGestor))));
            sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(sIntent);
        } catch (ActivityNotFoundException e) {
            return;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btntoggle_1)){
            pass=true;
            ImageView animationTarget = (ImageView) this.findViewById(R.id.btntoggle_1);
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_0);
            animationTarget.startAnimation(animation);
            startAnimationR(icocompartir);
            startAnimation_AgendarLlamadaR(icoagendarllamada);
            startAnimation_llamadaR(icollamada);
            startAnimation_correoR(icomail);
            startAnimation_chatR(icochat);

            parentManager.showBanqueroPersonal();
        }

        if (v.equals(icollamada)){
            //5517976509,#1313
            final String telefonoGestor = "+5255" + BanqueroPersonal.getInstance().getTelefono1() + ",#44";//+ BanqueroPersonal.getInstance().getExtension1();
            llamarBanqueroPersonal(telefonoGestor);
            finish();
            SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBmovilApp().cerrarSesion();
        }
        /*if (v.equals(icomail)){
            parentManager.showEnviarCorreo();
        }*/
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
        parentViewsController.consumeAccionesDePausa();
    }
    @Override
    public void goBack() {
        //parentViewsController.removeDelegateFromHashMap(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
        //super.goBack();
        parentManager.showBanqueroPersonal();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void startAnimation(View view) {
        float scale = getResources().getDisplayMetrics().density;
        ImageView someImage = (ImageView) findViewById(R.id.icocompartir);
        TextView someText =  (TextView) findViewById(R.id.textLabel0);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(someImage,
                "x", 285.0f * scale, 260.0f * scale);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(someImage,
                "y", 150.0f * scale, 25.0f * scale);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(someText,
                "x", 285.0f * scale, 255.0f * scale);
        ObjectAnimator anim4 = ObjectAnimator.ofFloat(someText,
                "y", 150.0f * scale, 75.0f * scale);
    AnimatorSet animSet = new AnimatorSet();
    animSet.play(anim1).with(anim2);
        animSet.play(anim3).with(anim4);
    animSet.setDuration(150);
    animSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.start();
    }

    public void startAnimation_AgendarLlamada(View view) {
        float scale = getResources().getDisplayMetrics().density;
        ImageView someImage = (ImageView) findViewById(R.id.icoagendarllamada);
        TextView someText =  (TextView) findViewById(R.id.textLabel1);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(someImage,
                "x", 285.0f * scale, 190.0f * scale);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(someImage,
                "y", 150.0f * scale, 65.0f * scale);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(someText,
                "x", 285.0f * scale, 155.0f * scale);
        ObjectAnimator anim4 = ObjectAnimator.ofFloat(someText,
                "y", 150.0f * scale, 115.0f * scale);
        AnimatorSet animSet = new AnimatorSet();
        animSet.play(anim1).with(anim2);
        animSet.play(anim3).with(anim4);
        animSet.setDuration(150);
        animSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.start();
    }

    public void startAnimation_llamada(View view) {
        float scale = getResources().getDisplayMetrics().density;
        ImageView someImage = (ImageView) findViewById(R.id.icollamada);
        TextView someText =  (TextView) findViewById(R.id.textLabel2);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(someImage,
                "x", 285.0f * scale, 155.0f * scale);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(someText,
                "x", 285.0f * scale, 160.0f * scale);
        AnimatorSet animSet = new AnimatorSet();
        animSet.play(anim1);
        animSet.play(anim3);
        animSet.setDuration(150);
        animSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.start();
    }

    public void startAnimation_correo(View view) {
        float scale = getResources().getDisplayMetrics().density;
        ImageView someImage = (ImageView) findViewById(R.id.icomail);
        TextView someText =  (TextView) findViewById(R.id.textLabel3);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(someImage,
                "x", 285.0f * scale, 185.0f * scale);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(someImage,
                "y", 150.0f * scale, 230.0f * scale);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(someText,
                "x", 285.0f * scale, 192.0f * scale);
        ObjectAnimator anim4 = ObjectAnimator.ofFloat(someText,
                "y", 150.0f * scale, 280.0f * scale);
        AnimatorSet animSet = new AnimatorSet();
        animSet.play(anim1).with(anim2);
        animSet.play(anim3).with(anim4);
        animSet.setDuration(150);
        animSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.start();
    }

    public void startAnimation_chat(View view) {
        float scale = getResources().getDisplayMetrics().density;
        ImageView someImage = (ImageView) findViewById(R.id.icochat);
        TextView someText =  (TextView) findViewById(R.id.textLabel4);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(someImage,
                "x", 285.0f * scale, 255.0f * scale);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(someImage,
                "y", 150.0f * scale, 275.0f * scale);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(someText,
                "x", 285.0f * scale, 265.0f * scale);
        ObjectAnimator anim4 = ObjectAnimator.ofFloat(someText,
                "y", 150.0f * scale, 325.0f * scale);
        AnimatorSet animSet = new AnimatorSet();
        animSet.play(anim1).with(anim2);
        animSet.play(anim3).with(anim4);
        animSet.setDuration(150);
        animSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.start();
    }

    ////////


    public void startAnimationR(View view) {
        float scale = getResources().getDisplayMetrics().density;
        ImageView someImage = (ImageView) findViewById(R.id.icocompartir);
        TextView someText =  (TextView) findViewById(R.id.textLabel0);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(someImage,
                "x", 260.0f * scale, 285.0f * scale);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(someImage,
                "y", 25.0f * scale, 150.0f * scale);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(someText,
                "x", 255.0f * scale, 285.0f * scale);
        ObjectAnimator anim4 = ObjectAnimator.ofFloat(someText,
                "y", 75.0f * scale, 150.0f * scale);
        AnimatorSet animSet = new AnimatorSet();
        animSet.play(anim1).with(anim2);
        animSet.play(anim3).with(anim4);
        animSet.setDuration(200);
        animSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.start();
    }

    public void startAnimation_AgendarLlamadaR(View view) {
        float scale = getResources().getDisplayMetrics().density;
        ImageView someImage = (ImageView) findViewById(R.id.icoagendarllamada);
        TextView someText =  (TextView) findViewById(R.id.textLabel1);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(someImage,
                "x", 190.0f * scale, 285.0f * scale);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(someImage,
                "y", 65.0f * scale, 150.0f * scale);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(someText,
                "x", 155.0f * scale, 285.0f * scale);
        ObjectAnimator anim4 = ObjectAnimator.ofFloat(someText,
                "y", 115.0f * scale, 150.0f * scale);
        AnimatorSet animSet = new AnimatorSet();
        animSet.play(anim1).with(anim2);
        animSet.play(anim3).with(anim4);
        animSet.setDuration(200);
        animSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.start();
    }

    public void startAnimation_llamadaR(View view) {
        float scale = getResources().getDisplayMetrics().density;
        ImageView someImage = (ImageView) findViewById(R.id.icollamada);
        TextView someText =  (TextView) findViewById(R.id.textLabel2);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(someImage,
                "x", 155.0f * scale, 285.0f * scale);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(someText,
                "x", 160.0f * scale, 285.0f * scale);
        AnimatorSet animSet = new AnimatorSet();
        animSet.play(anim1);
        animSet.play(anim3);
        animSet.setDuration(200);
        animSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.start();
    }

    public void startAnimation_correoR(View view) {
        float scale = getResources().getDisplayMetrics().density;
        ImageView someImage = (ImageView) findViewById(R.id.icomail);
        TextView someText =  (TextView) findViewById(R.id.textLabel3);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(someImage,
                "x", 185.0f * scale, 285.0f * scale);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(someImage,
                "y", 230.0f * scale, 150.0f * scale);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(someText,
                "x", 192.0f * scale, 285.0f * scale);
        ObjectAnimator anim4 = ObjectAnimator.ofFloat(someText,
                "y", 280.0f * scale, 150.0f * scale);
        AnimatorSet animSet = new AnimatorSet();
        animSet.play(anim1).with(anim2);
        animSet.play(anim3).with(anim4);
        animSet.setDuration(200);
        animSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.start();
    }

    public void startAnimation_chatR(View view) {
        float scale = getResources().getDisplayMetrics().density;
        ImageView someImage = (ImageView) findViewById(R.id.icochat);
        TextView someText =  (TextView) findViewById(R.id.textLabel4);
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(someImage,
                "x", 255.0f * scale, 285.0f * scale);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(someImage,
                "y", 275.0f * scale, 150.0f * scale);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(someText,
                "x", 265.0f * scale, 285.0f * scale);
        ObjectAnimator anim4 = ObjectAnimator.ofFloat(someText,
                "y", 325.0f * scale, 150.0f * scale);
        AnimatorSet animSet = new AnimatorSet();
        animSet.play(anim1).with(anim2);
        animSet.play(anim3).with(anim4);
        animSet.setDuration(200);
        animSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.start();
    }

}

