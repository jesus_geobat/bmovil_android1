package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.TipoRetiroSinTarjetaViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by andres.vicentelinare on 23/09/2016.
 */
public class TipoRetiroSinTarjetaDelegate extends DelegateBaseOperacion {

    public static final long TIPO_RETIRO_SIN_TARJETA_DELEGATE_ID = 7509910320323232323L;

    public enum AccionTipoRetiro {
        RETIRO,
        CONSULTA
    }

    private AccionTipoRetiro accionTipoRetiro;

    private boolean solicitarAlertarRetiroSinTarjeta;

    private TipoRetiroSinTarjetaViewController tipoRetiroSinTarjetaViewController;

    public TipoRetiroSinTarjetaDelegate() {
        accionTipoRetiro = AccionTipoRetiro.RETIRO;
    }

    public void comprobarRecortado() {
        solicitarAlertas(tipoRetiroSinTarjetaViewController);
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler handler, Server.isJsonValueCode isJsonValueCode, BaseViewController caller) {
        ((BmovilViewsController) tipoRetiroSinTarjetaViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode, caller, true);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
            if (operationId == Server.OP_SOLICITAR_ALERTAS) {
                SolicitarAlertasData validacionalertas = (SolicitarAlertasData) response.getResponse();
                if (Constants.ALERT02.equals(validacionalertas.getValidacionAlertas())) {
                    // 02 no tiene alertas
                    SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
                    if (solicitarAlertarRetiroSinTarjeta) {
                        SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_retiroSinTarjeta_recortado_sin_alertas));
                    } else {
                        SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));
                    }
                } else {
                    setSa(validacionalertas);
                    analyzeAlertasRecortadoSinError();
                }
            }
        }
    }

    public void setTipoRetiroSinTarjetaViewController(TipoRetiroSinTarjetaViewController tipoRetiroSinTarjetaViewController) {
        this.tipoRetiroSinTarjetaViewController = tipoRetiroSinTarjetaViewController;
    }

    public AccionTipoRetiro getAccionTipoRetiro() {
        return accionTipoRetiro;
    }

    public void setAccionTipoRetiro(AccionTipoRetiro accionTipoRetiro) {
        this.accionTipoRetiro = accionTipoRetiro;
    }

    public void setSolicitarAlertarRetiroSinTarjeta(boolean solicitarAlertarRetiroSinTarjeta) {
        this.solicitarAlertarRetiroSinTarjeta = solicitarAlertarRetiroSinTarjeta;
    }
}
