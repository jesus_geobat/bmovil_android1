package suitebancomer.aplicaciones.bmovil.classes.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;


/**
 * Created by ana_mezquitillo on 18/02/16.
 */
public class MovimientoTransitoExtract implements ParsingHandler {

    /**
     * Mensaje informativo recibido
     */
    private String mensajeInformativo;
    /**
     * Fecha
     */
    private String fecha = null;

    /**
     * Descripcion
     */
    private String descripcion = null;

    /**
     * Hora
     */
    private String hora = null;

    /**
     * Importe
     */
    private String importe = null;

    /**
     * Autorizacion
     */
    private String numeroAutorizacion = null;

    /**
     * Situacion
     */
    private String situacion = null;

    /**
     * Nombre de Comercio
     */
    private String nombreComercio = null;
    /**
     * Array de Movimeintos en transito
     */
    private ArrayList<MovimientoTransito> movimientoTransitos = null;

    /**
     * Default Constructor
     */
    public MovimientoTransitoExtract(){
        movimientoTransitos = new ArrayList<MovimientoTransito>();
    }

    public MovimientoTransitoExtract(String mensajeInformativo, String descripcion, String fecha, String hora,
                                     String importe, String numeroAutorizacion, String situacion, String nombreComercio){

        this.descripcion = descripcion;
        this.fecha = fecha;
        this.hora = hora;
        this.importe = importe;
        this.numeroAutorizacion = numeroAutorizacion;
        this.situacion = situacion;
        this.nombreComercio = nombreComercio;
    }


    public String getMensajeInformativo() {
        return mensajeInformativo;
    }

    public void setMensajeInformativo(String mensajeInformativo) {
        this.mensajeInformativo = mensajeInformativo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public String getNombreComercio() {
        return nombreComercio;
    }

    public void setNombreComercio(String nombreComercio) {
        this.nombreComercio = nombreComercio;
    }

    public ArrayList<MovimientoTransito> getMovimientoTransitos() {
        return movimientoTransitos;
    }

    public void setMovimientoTransitos(ArrayList<MovimientoTransito> movimientoTransitos) {
        this.movimientoTransitos = movimientoTransitos;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        //this.movimientoTransitos = parseMovementsTransito(parser);
    }

    /**
     * Parse movements from parser data
     * @param parser the parser
     * @return the movements
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     * @throws NumberFormatException on format errors
     */
    private ArrayList<MovimientoTransito> parseMovementsTransito(ParserJSON parser) throws IOException, NumberFormatException, ParsingException {

        ArrayList<MovimientoTransito> result = null;
        JSONArray elements=null;
        JSONObject actual=null;
        try{
           result = new ArrayList<MovimientoTransito>();
            this.mensajeInformativo = parser.parseNextValue("mensajeInformativo");
            elements = parser.parseNextValueWithArray("movsTransito");

            MovimientoTransito movementTran;
            for(int i = 0; i < elements.length(); i++) {
                actual = elements.getJSONObject(i);
                String fecha = Tools.formatShortDate(actual.getString("fecha")); //parser.parseNextValue("fecha"));
                boolean negativeSign = false;
                String importe = Tools.formatAmount(actual.getString("importe"),negativeSign);//parser.parseNextValue("importe"), negativeSign);
                String descripcion = actual.getString("descripcion");
                String hora = actual.getString("hora");
                String numeroAutorizacion  = actual.getString("numeroAutorizacion");
                String situacion = actual.getString("situacion");
                String nombreComercio = actual.getString("nombreComercio");
                movementTran = new MovimientoTransito(descripcion,fecha,hora,importe,numeroAutorizacion,situacion,nombreComercio);
                result.add(movementTran);
            }
        }catch (JSONException e){
            Log.e(this.getClass().getSimpleName(), "No se pudo interpretar el json.", e);
            return null;
        }
        return  result;

    }
    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub
        this.movimientoTransitos = parseMovementsTransito(parser);

    }
}
