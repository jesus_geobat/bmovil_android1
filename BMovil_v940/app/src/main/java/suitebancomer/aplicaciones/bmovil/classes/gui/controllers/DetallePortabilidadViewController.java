package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaPortabilidadNominaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetallePortabilidadDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TiempoAireDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.DetallePortability;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;

/**
 * Created by elunag on 08/07/16.
 */
public class DetallePortabilidadViewController extends BaseViewController implements View.OnClickListener
{
    TextView cuenta,folio,banco,status,aplicacion,anexoContrato, motivo;
    ImageButton btnMenu;
    DetallePortabilidadDelegate delegate;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.bmovil_detalle_portabilidad);
        setTitle(R.string.consulta_nomina_title, R.drawable.bmovil_consultar_icono);
        SuiteApp suiteApp = (SuiteApp) getApplication();
        setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
        setDelegate(parentViewsController.getBaseDelegateForKey(DetallePortabilidadDelegate.DETALLE_PORTABILIDAD_DELEGATE_ID));
        delegate = (DetallePortabilidadDelegate)getDelegate();
        if (delegate == null) {
            delegate = new DetallePortabilidadDelegate();
            parentViewsController.addDelegateToHashMap(DetallePortabilidadDelegate.DETALLE_PORTABILIDAD_DELEGATE_ID, delegate);
            setDelegate(delegate);
        }
        delegate.setDetallePortabilidadViewController(this);
        init();

    }

    public void init()
    {
        cuenta = (TextView)findViewById(R.id.cuenta);
        folio = (TextView)findViewById(R.id.folio);
        banco = (TextView)findViewById(R.id.bancoOrigen);
        status = (TextView)findViewById(R.id.estatus);
        aplicacion = (TextView) findViewById(R.id.aplicacion);
        motivo = (TextView) findViewById(R.id.motivo);
        btnMenu = (ImageButton) findViewById(R.id.detalle_portabilidad_boton_menu);
        btnMenu.setOnClickListener(this);
        anexoContrato = (TextView) findViewById(R.id.anexoContratoDetalle);
        anexoContrato.setOnClickListener(this);

        cuenta.setText("Cuenta: " + String.valueOf(DetallePortability.getInstance().getReferenceNumber()));
        folio.setText("Folio: " + String.valueOf(DetallePortability.getInstance().getOperationReferenceNumber()));
        banco.setText("Banco origen: " + String.valueOf(DetallePortability.getInstance().getExternalBank().getName()));
        if (DetallePortability.getInstance().getStatus().equals("P")) {
            status.setText("Estatus: Pendiente");
        }
        else if (DetallePortability.getInstance().getStatus().equals("A")) {
            status.setText("Estatus: Alta");
        }
        else if (DetallePortability.getInstance().getStatus().equals("E")) {
            status.setText("Estatus: Enviada");
        }
        else if (DetallePortability.getInstance().getStatus().equals("B")) {
            status.setText("Estatus: Baja");
        }
        else if (DetallePortability.getInstance().getStatus().equals("R")) {
            status.setText("Estatus: Rechazada");
        }
        else if (DetallePortability.getInstance().getStatus().equals("C")) {
            status.setText("Estatus: Cancelada");
        }
        aplicacion.setText("Aplicación: " + String.valueOf(DetallePortability.getInstance().getCreatedDate()));
        if (DetallePortability.getInstance().getStatus().equals("R")) {
            motivo.setVisibility(View.VISIBLE);
            if(DetallePortability.getInstance().getMotivoRechazo()==null){
                motivo.setText("Motivo de rechazo: " + "No disponible");
            } else if(DetallePortability.getInstance().getMotivoRechazo().equals("")||DetallePortability.getInstance().getMotivoRechazo().equals("null")){
                motivo.setText("Motivo de rechazo: " + "No disponible");
            } else{
                motivo.setText("Motivo de rechazo: " + DetallePortability.getInstance().getMotivoRechazo());
            }
        } else {
            motivo.setVisibility(View.INVISIBLE);
        }
        String solicitud = String.valueOf(DetallePortability.getInstance().getOperationReferenceNumber()).substring(20,21);
        if(solicitud.equalsIgnoreCase(Constants.solicitud_Sucursal)){
            anexoContrato.setVisibility(View.INVISIBLE);
        }


    }

    @Override
    public void onClick(final View view)
    {
        if (view == btnMenu){
            parentViewsController.removeDelegateFromHashMap(ConsultaPortabilidadNominaDelegate.CONSULTA_PORTABILIDAD_NOMINA_DELEGATE_ID);
            ((BmovilViewsController)parentViewsController).showMenuPrincipal(true);
        }
        else if(view == anexoContrato){
            delegate.contratoCambiaNomina();
        }
    }

    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void goBack() {
        parentViewsController.removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
        super.goBack();
    }

    @Override
    public void onUserInteraction() {
        //super.onUserInteraction();
        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();

    }
}
