package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import android.util.Log;

public class ConsultaRetiroSinTarjetaData implements ParsingHandler {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mensajeInformativo;
	
	private List<ConsultaRetiroSinTarjeta> retiros;

	public List<ConsultaRetiroSinTarjeta> getRetiros() {
		return retiros;
	}

	public void setRetiros(List<ConsultaRetiroSinTarjeta> retiros) {
		this.retiros = retiros;
	}

	public ConsultaRetiroSinTarjetaData(String mensajeInformativo) {
		super();
		this.mensajeInformativo = mensajeInformativo;
		
	}

	public ConsultaRetiroSinTarjetaData() {
		// TODO Auto-generated constructor stubthis.mensajeInformativo = mensajeInformativo;
		this.mensajeInformativo = "";
		
	}

	

	public String getMensajeInformativo() {
		return mensajeInformativo;
	}

	public void setMensajeInformativo(String mensajeInformativo) {
		this.mensajeInformativo = mensajeInformativo;
	}
	
	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		try{
			this.mensajeInformativo = parser.parseNextValue("mensajeInformativo");
			if (parser.parseNextValue("estado").equalsIgnoreCase("OK")){
			JSONArray datos = parser.parseNextValueWithArray("retiro");
			retiros = new ArrayList<ConsultaRetiroSinTarjeta>();
			for (int i = 0; i < datos.length(); i++) {
				JSONObject jsonDatos = datos.getJSONObject(i);
				ConsultaRetiroSinTarjeta retiroData = new ConsultaRetiroSinTarjeta(
						jsonDatos.getString("folioOperacion"),
						jsonDatos.getString("importe"),
						jsonDatos.getString("cuentaCargo"),
						jsonDatos.getString("estatusOperacion"),
						jsonDatos.getString("numeroCelular"),
						jsonDatos.getString("companiaCelular"),
						jsonDatos.getString("beneficiario"),
						jsonDatos.getString("concepto"),
						jsonDatos.getString("fechaOperacion"),
						jsonDatos.getString("fechaExpiracion"),
						jsonDatos.getString("horaOperacionExp"),
						jsonDatos.getString("codigoSeguridad"),
						jsonDatos.getString("indicador"));
				
				retiros.add(retiroData);
				
			}
			}else{
				retiros = new ArrayList<ConsultaRetiroSinTarjeta>();
				
			}
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
		}
		
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}


}
