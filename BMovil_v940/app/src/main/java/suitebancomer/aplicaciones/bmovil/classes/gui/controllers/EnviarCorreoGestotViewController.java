package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.EnviarCorreoDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.BanqueroPersonal;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import tracking.TrackingHelper;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;



/**
 * Created by Alberto on 11/11/16.
 */
public class EnviarCorreoGestotViewController  extends BaseViewController {
    //AMZ
    private BmovilViewsController parentManager;

    private BaseViewController viewController;
    /**
     * Delegado asociado con el controlador.
     */
    private EnviarCorreoDelegate ecDelegate;


    /**
     * CheckBox para seleccionar si se copia al usuario en el correo.
     */
    private CheckBox cbConCopia;

    /**
     * Campo de texto para indicar el correo del destinatario.
     */
    private EditText tbCorreoBeneficiario;

    /**
     * Campo de texto para indicar el mensaje del correo.
     */
    private EditText tbMensaje;


    private TextView correoDelBaquero;


    // #region Ciclo de vida.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_gestor_remoto_enviarcorreo);
        setTitle(R.string.bmovil_enviar_correo_titulo, R.drawable.icono_enviar_correo);

        //AMZ
        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("correo", parentManager.estados);
        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        setDelegate(parentViewsController.getBaseDelegateForKey(EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID));
        //ecDelegate = (EnviarCorreoDelegate)getDelegate();
        if(ecDelegate==null){
            ecDelegate= new EnviarCorreoDelegate();
        }
        ecDelegate.setOwnerController(this);
        init();
        correoDelBaquero.setText(BanqueroPersonal.getInstance().getEmail().toLowerCase());
        tbCorreoBeneficiario.setText(Session.getInstance().getEmail());
    }

    @Override
    protected void onResume() {
        super.onResume();
        getParentViewsController().setCurrentActivityApp(this);
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void goBack() {
        parentViewsController.removeDelegateFromHashMap(EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID);
        super.goBack();
    }

    private void init() {
        findViews();
        scaleForCurrentScreen();
        tbMensaje = (EditText)findViewById(R.id.tbMensaje);
        correoDelBaquero = (TextView) findViewById(R.id.correoDelBaquero);
        tbCorreoBeneficiario.addTextChangedListener(new BmovilTextWatcher(this));
        tbMensaje.addTextChangedListener(new BmovilTextWatcher(this));
    }

    /**
     * Busca la referencia a todas las subvistas necesarias.
     */
    private void findViews() {
        cbConCopia = (CheckBox)findViewById(R.id.cbCopia);
        tbCorreoBeneficiario = (EditText)findViewById(R.id.tbCorreoBeneficiario);
        tbCorreoBeneficiario.setEnabled(false);
        tbMensaje = (EditText)findViewById(R.id.tbMensaje);
    }

    /**
     * Escala las subvistas para la pantalla actual.
     */
    private void scaleForCurrentScreen() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(findViewById(R.id.rootLayout));
        guiTools.scale(findViewById(R.id.lblBanqueroPersonal), false);
        guiTools.scale(findViewById(R.id.lblCorreoBeneficiario), false);
        guiTools.scale(tbCorreoBeneficiario, false);
        guiTools.scale(findViewById(R.id.lblConCopia), false);
        guiTools.scale(cbConCopia);
        guiTools.scale(findViewById(R.id.lblMensaje), false);
        guiTools.scale(tbMensaje, false);
        guiTools.scale(findViewById(R.id.layoutSend));
        guiTools.scale(findViewById(R.id.btnEnviar));
    }

    /**
     * Evento al presionar el boton Enviar.
     * @param sender Vista que invoca el método.
     */
    public void onBotonEnviarClick(View sender) {

       ecDelegate.validaDatosGestor(true, cbConCopia.isChecked(), tbCorreoBeneficiario.getText().toString(), tbMensaje.getText().toString());


    }


    /**
     * Habilita o deshabilta el campo de correo del beneficiario segun el estado del CheckBox del beneficiario.
     * @param sender La vista que invoca al método.
     */
    public void onCheckboxBeneficiarioClick(View sender) {
        if (sender != cbConCopia)
            return;
        tbCorreoBeneficiario.setEnabled(cbConCopia.isChecked());
    }

    /* (non-Javadoc)
     * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
     */
    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        ecDelegate.analyzeResponse(operationId, response);
    }
}
