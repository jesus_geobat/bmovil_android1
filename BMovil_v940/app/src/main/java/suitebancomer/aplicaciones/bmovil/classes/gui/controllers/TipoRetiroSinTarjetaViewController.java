package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.gallery.Image;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TipoRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.gui.controllers.BaseViewController;
import tracking.TrackingHelper;

public class TipoRetiroSinTarjetaViewController extends BaseViewController {

    private TipoRetiroSinTarjetaDelegate tipoRetiroSinTarjetaDelegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER, R.layout.activity_tipo_retiro_sin_tarjeta_view_controller);

        BmovilViewsController parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("tipo retiro sin tarjeta", parentManager.estados);

        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        getParentViewsController().setCurrentActivityApp(this);

        tipoRetiroSinTarjetaDelegate = (TipoRetiroSinTarjetaDelegate) parentManager.getBaseDelegateForKey(TipoRetiroSinTarjetaDelegate.TIPO_RETIRO_SIN_TARJETA_DELEGATE_ID);
        if (tipoRetiroSinTarjetaDelegate == null) {
            tipoRetiroSinTarjetaDelegate = new TipoRetiroSinTarjetaDelegate();
        }
        tipoRetiroSinTarjetaDelegate.setTipoRetiroSinTarjetaViewController(this);



    }

    public void clickParaMi(View view) {
        if (tipoRetiroSinTarjetaDelegate.getAccionTipoRetiro().equals(TipoRetiroSinTarjetaDelegate.AccionTipoRetiro.RETIRO)) {
            mostrarRetiroParaMi();
        } else {
            mostrarConsultaParaMi();
        }
    }

    public void clickParaAlguienMas(View view) {
        if (tipoRetiroSinTarjetaDelegate.getAccionTipoRetiro().equals(TipoRetiroSinTarjetaDelegate.AccionTipoRetiro.RETIRO)) {
            mostrarRetiroParaAlguienMas();
        } else {
            mostrarConsultaParaAlguienMas();
        }
    }

    private void mostrarRetiroParaMi() {
        if(Autenticacion.getInstance().isOperable(Constants.Operacion.retiroSinTarjeta, Session.getInstance(SuiteApp.appContext).getClientProfile())){
            ((BmovilViewsController) parentViewsController).showRetiroSinTarjetaViewController(null, AltaRetiroSinTarjetaDelegate.TipoRetiro.RETIRO_SIN_TARJETA);
        }else{
            tipoRetiroSinTarjetaDelegate.setSolicitarAlertarRetiroSinTarjeta(true);
            tipoRetiroSinTarjetaDelegate.comprobarRecortado();
            //TODO undefined in P026 Mis cuentas Quiero - RN16
        }
    }

    private void mostrarRetiroParaAlguienMas() {
        Map<String, Object> inicioOperacionMap = new HashMap<String, Object>();
        if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.dineroMovil, Session.getInstance(SuiteApp.appContext).getClientProfile())) {
            if (Server.ALLOW_LOG)
                Log.d("TipoRetiroSinTarjetaViewController", "Entraste a dinero móvil");
            //ARR
            inicioOperacionMap.put("evento_inicio", "event45");
            inicioOperacionMap.put("&&products", "operaciones;transferencias+dinero movil");
            inicioOperacionMap.put("eVar12", "inicio");

            TrackingHelper.trackInicioOperacion(inicioOperacionMap);
            if (Session.getInstance(SuiteApp.appContext).puedeOperarConTDCEje()) {
                ((BmovilViewsController) parentViewsController).showFrecuentesDineroMovilViewController();
            } else {
                showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
            }
        } else {
            tipoRetiroSinTarjetaDelegate.setSolicitarAlertarRetiroSinTarjeta(false);
            tipoRetiroSinTarjetaDelegate.comprobarRecortado();
        }
    }

    private void mostrarConsultaParaMi() {
        if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.retiroSinTarjeta, Session.getInstance(SuiteApp.appContext).getClientProfile())) {
            if (Server.ALLOW_LOG)
                Log.d(getLocalClassName().trim(), "Entraste a consultar retiros sin tarjeta");
            ((BmovilViewsController) parentViewsController).showConsultarRetiroSinTarjeta();
        } else {
            tipoRetiroSinTarjetaDelegate.setSolicitarAlertarRetiroSinTarjeta(true);
            tipoRetiroSinTarjetaDelegate.comprobarRecortado();
        }
    }

    private void mostrarConsultaParaAlguienMas() {
        Map<String, Object> inicioOperacionMap = new HashMap<String, Object>();
        inicioOperacionMap.put("evento_inicio", "event45");
        inicioOperacionMap.put("&&products", "consulta;dinero movil");
        inicioOperacionMap.put("eVar12", "inicio");
        TrackingHelper.trackInicioConsulta(inicioOperacionMap);
        if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.dineroMovil, Session.getInstance(SuiteApp.appContext).getClientProfile())) {
            ((BmovilViewsController) parentViewsController).showConsultarDineroMovil();
        } else {
            tipoRetiroSinTarjetaDelegate.setSolicitarAlertarRetiroSinTarjeta(false);
            tipoRetiroSinTarjetaDelegate.comprobarRecortado();
        }
    }

    public void processNetworkResponse(int operationId, ServerResponse response) {
        tipoRetiroSinTarjetaDelegate.analyzeResponse(operationId, response);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

}
