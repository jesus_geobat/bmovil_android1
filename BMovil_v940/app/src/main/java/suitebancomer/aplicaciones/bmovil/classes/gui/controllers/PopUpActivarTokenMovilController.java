package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bancomer.mbanking.R;

import suitebancomer.aplicaciones.bmovil.classes.common.TokenPopUpAdapter;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PopUpActivarTokenMovilDelegate;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by CGI on 15/11/2016.
 */
public class PopUpActivarTokenMovilController extends DialogFragment{

    private TextView tTitulo;
    private TextView tSubtitulo;
    private TextView tActivar;

    private ImageView imagen;

    private Button btnCerrar;
    private Button btnAceptar;
    private ViewPager viewPager;
    private BaseViewController caller;

    public BaseViewController getCaller() {
        return caller;
    }

    public void setCaller(BaseViewController caller) {
        this.caller = caller;
    }

    private PopUpActivarTokenMovilDelegate delegate;
    private View v;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        v = inflater.inflate(R.layout.layout_popup_activar_token_movil, null);
        builder.setView(v);
        delegate = new PopUpActivarTokenMovilDelegate();

        findViews(v);

        btnCerrar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();

                    }
                }
        );

        btnAceptar.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        delegate.showActivacionToken();
                    }
                }
        );

        viewPager.setAdapter(new TokenPopUpAdapter(getContext()));

        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                caller.onUserInteraction();
                viewPager.getRootView().onTouchEvent(event);
                return false;
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                changePageMarker(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        AlertDialog result = builder.create();
        result.setView(v, 0, 0, 0, 0);
        return result;
    }


    @Override
    public void onResume(){
        super.onResume();
        Window window = getDialog().getWindow();
        int width= getResources().getDimensionPixelSize(R.dimen.popup_token_width);
        int height = getResources().getDimensionPixelSize(R.dimen.popup_token_height);
        window.setLayout(width, height);
        window.setGravity(Gravity.CENTER);
    }



    private void findViews(View v) {
//        tTitulo = (TextView) v.findViewById(R.id.lblAvisoPopToken);
//        tSubtitulo = (TextView) v.findViewById(R.id.lblTercerosPopToken);
//        tActivar = (TextView) v.findViewById(R.id.lblActivar1PopToken);
//
//        imagen = (ImageView) v.findViewById(R.id.Imag_TFisico);

        btnCerrar = (Button) v.findViewById(R.id.popTokenBtnCerrar);
        btnAceptar = (Button) v.findViewById(R.id.btnActivarToken);
        viewPager = (ViewPager)v.findViewById(R.id.viewpager);


    }

    private void changePageMarker(int position) {
        ImageView pag1 = (ImageView) v.findViewById(R.id.pag1);
        ImageView pag2 = (ImageView) v.findViewById(R.id.pag2);
        ImageView pag3 = (ImageView) v.findViewById(R.id.pag3);
        ImageView pag4 = (ImageView) v.findViewById(R.id.pag4);
        ImageView pag5 = (ImageView) v.findViewById(R.id.pag5);
        ImageView pag6 = (ImageView) v.findViewById(R.id.pag6);
        switch (position){
            case 0:
                pag1.setBackgroundResource(R.drawable.btn_blue);
                pag2.setBackgroundResource(R.drawable.btn_gray);
                pag3.setBackgroundResource(R.drawable.btn_gray);
                pag4.setBackgroundResource(R.drawable.btn_gray);
                pag5.setBackgroundResource(R.drawable.btn_gray);
                pag6.setBackgroundResource(R.drawable.btn_gray);
                break;
            case 1:
                pag1.setBackgroundResource(R.drawable.btn_gray);
                pag2.setBackgroundResource(R.drawable.btn_blue);
                pag3.setBackgroundResource(R.drawable.btn_gray);
                pag4.setBackgroundResource(R.drawable.btn_gray);
                pag5.setBackgroundResource(R.drawable.btn_gray);
                pag6.setBackgroundResource(R.drawable.btn_gray);
                break;
            case 2:
                pag1.setBackgroundResource(R.drawable.btn_gray);
                pag2.setBackgroundResource(R.drawable.btn_gray);
                pag3.setBackgroundResource(R.drawable.btn_blue);
                pag4.setBackgroundResource(R.drawable.btn_gray);
                pag5.setBackgroundResource(R.drawable.btn_gray);
                pag6.setBackgroundResource(R.drawable.btn_gray);
                break;
            case 3:
                pag1.setBackgroundResource(R.drawable.btn_gray);
                pag2.setBackgroundResource(R.drawable.btn_gray);
                pag3.setBackgroundResource(R.drawable.btn_gray);
                pag4.setBackgroundResource(R.drawable.btn_blue);
                pag5.setBackgroundResource(R.drawable.btn_gray);
                pag6.setBackgroundResource(R.drawable.btn_gray);
                break;
            case 4:
                pag1.setBackgroundResource(R.drawable.btn_gray);
                pag2.setBackgroundResource(R.drawable.btn_gray);
                pag3.setBackgroundResource(R.drawable.btn_gray);
                pag4.setBackgroundResource(R.drawable.btn_gray);
                pag5.setBackgroundResource(R.drawable.btn_blue);
                pag6.setBackgroundResource(R.drawable.btn_gray);
                break;
            case 5:
                pag1.setBackgroundResource(R.drawable.btn_gray);
                pag2.setBackgroundResource(R.drawable.btn_gray);
                pag3.setBackgroundResource(R.drawable.btn_gray);
                pag4.setBackgroundResource(R.drawable.btn_gray);
                pag5.setBackgroundResource(R.drawable.btn_gray);
                pag6.setBackgroundResource(R.drawable.btn_blue);
                break;
            default:
                break;
        }
    }

}
