package suitebancomer.aplicaciones.bmovil.classes.model;

public class CambioCuenta {

	private Account account;
	
	public Account getAccount() {
		return account;
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}
}
