package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.CampaniaPaperlessDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import tracking.TrackingHelper;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.R;

public class CampaniaPaperlessViewController extends BaseViewController {
	
	LinearLayout contenedorPrincipal, aviso;
	CampaniaPaperlessDelegate delegate; 
	//AMZ
	private BmovilViewsController parentManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,
				R.layout.layout_bmovil_confirmacion);
		setTitle(R.string.confirmacion_paperless_title, R.drawable.paperless_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();	

		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(CampaniaPaperlessDelegate.CAMPANIA_PAPERLESS_DELEGATE_ID));
		delegate = (CampaniaPaperlessDelegate) getDelegate();
		delegate.setViewController(this);
		findViews();
		scaleForCurrentScreen();
		confirmarcampania();
	}
	
	/**
	 * 
	 */
	private void findViews() {
		contenedorPrincipal = (LinearLayout) findViewById(R.id.confirmacion_parent_parent_view);
		//aviso = (LinearLayout) findViewById(R.id.seeAviso);
		//aviso.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Escala la vistas en base a la pantalla donde se muestra
	 */
	private void scaleForCurrentScreen(){
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		//gTools.scale(contenedorPrincipal);
		//gTools.scale(aviso);
	}
	
	
	@Override
	protected void onPause() {
		parentViewsController.consumeAccionesDePausa();
		super.onPause();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		//getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	public void onBackPressed() {		
	}
	
	/**
	 * Muestra la pantalla de confirmacion 
	 */
	private void confirmarcampania(){
		//AMZ
		//TrackingHelper.trackState("suspender", parentManager.estados);	
		delegate.realizaConfirmar();
	}
	
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}
	
}

