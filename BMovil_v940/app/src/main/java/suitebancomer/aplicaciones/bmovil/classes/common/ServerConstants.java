package suitebancomer.aplicaciones.bmovil.classes.common;

/**
 * Constantes para la parte servidora de la aplicacion.
 * 
 * @author CGI
 */
public class ServerConstants {

    public static final String ID_OPERACION = "idOperacion";
    public static final String CELULAR_BENEFICIARIO = "celularBeneficiario";
    public static final String COMPANIA_BENEFICIARIO = "companiaBeneficiario";
    public static final String BANCO_BENEFICIARIO = "bancoBeneficiario";
    public static final String CUENTA_OTRO_BANCO = "cuentaOtroBanco";
    public static final String CUENTA_TERCERO = "cuentaTercero";
    public static final String TITULAR_CUENTA_TERCERO = "titularCuentaTercero";
    public static final String SERVICIO = "servicio";
    public static final String CONVENIO = "convenio";
    public static final String TRANSFERIR_BANCOMER = "106";
    public static final String PAGO_SERVICIOS = "115";
    public static final String DINERO_MOVIL = "123";
    public static final String COMPRA_TIEMPO_AIRE = "109";
    public static final String TRANSFERENCIA_INTERBANCARIA = "107";
	public static final String NUMERO_TELEFONO_ETIQUETA = "NT";
	public static final String CONTRASENA_ETIQUETA = "NP";
	public static final String IUM_ETIQUETA = "IU";
	public static final String JSON_IUM_ETIQUETA = "IUM";
	public static final String NUMERO_CLIENTE_ETIQUETA = "TE";
	public static final String CUENTA_CARGO_ETIQUETA = "CC";
	public static final String IMPORTE_ETIQUETA = "IM";
	public static final String NOMBRE_BENEFICIARIO_ETIQUETA = "BF";
	public static final String CONCEPTO_ETIQUETA = "CP";
	public static final String NUMERO_CELULAR_TERCERO_ETIQUETA = "NU";
	public static final String OPERADORA_TELEFONIA_CELULAR_ETIQUETA = "OA";
	public static final String NIP_ETIQUETA = "NI";
	public static final String CVV_ETIQUETA = "CV";
	public static final String OTP_TOKEN_ETIQUETA = "OT";
	public static final String INSTRUCCION_VALIDACION_ETIQUETA = "VA";
	public static final String PERIODO_ETIQUETA= "PE";

    /**
     * Banco destino
     */
    public static final String BANCO_DESTINO = "bancoDestino";

    //PAPERLESS
    /** Constante del arreglo de cuentas. */
    public static final String ARRCUENTAS = "arrCuentas";

	/** Constante del numero de telefono. */
	public static final String NUMERO_TELEFONO = "numeroTelefono";
	
	/** Constante del numero de tarjeta. */
    public static final String NUMERO_TARJETA = "numeroTarjeta";
    
    /** Constante de la compania celular. */
    public static final String COMPANIA_CELULAR = "companiaCelular";
    
    /** Constante del numero celular compania nuevo. */
    public static final String COMPANIA_CELULAR_NUEVO = "companiaCelularNueva";
    
    /** Constante numero celular nuevo.*/
    public static final String NUMERO_CELULAR_NUEVO = "numeroCelNuevo";
    
    /** Constante del estatus de Bmovil. */
    public static final String ESTATUS_BMOVIL = "estatusBmovil";

    /** Constante de la validacion del numero de telefono y compannia, de alertas y de Bmovil. */
    public static final String VALIDACION_ALERTAS = "validacionAlertas";

    /** Constante del numero de telufono asociado a Alertas. */
    public static final String NUMERO_ALERTAS = "numeroAlertas";

    /** Constante de la compannia telefonica asociada a Alertas. */
    public static final String COMPANNIA_ALERTAS = "companiaAlertas";

    /** Constante del numero del cliente. */
    public static final String NUMERO_CLIENTE = "numeroCliente";

    /** Constante del indicador de Softtoken. */
    public static final String INDICADOR_CONTRATACION = "indicadorContratacion";

    /** Constante del numero de serie de Softtoken. */
    public static final String NUM_SERIE_TOKEN = "numSerieToken";

    /** Constante del tipo de instrumento. */
    public static final String TIPO_INSTRUMENTO = "tipoInstrumento";

    /** Constante del correo electronico del cliente. */
    public static final String CORREO_ELECTRONICO = "correoElectronico";

    /** Constante del nombre del cliente. */
    public static final String NOMBRE_CLIENTE = "nombreCliente";
    
    /** Constante del cve de acceso. */
    public static final String CVE_ACCESO = "cveAcceso";
    
    /** Constante del codigo nip. */
    public static final String CODIGO_NIP = "codigoNIP";
    
    /** Constante del codigo otp. */
    public static final String CODIGO_OTP = "codigoOTP";
    
    /** Constante de la Version APP */
    public static final String VERSION_APP = "versionApp";
    
    /** Constante de la Version */
    public static final String VERSION = "version";
    
    /** Constante del codigo cvv2. */
    public static final String CODIGO_CVV2 = "codigoCVV2";
    
    /** Constante de la cadena de autenticacion. */
    public static final String CADENA_AUTENTICACION = "cadenaAutenticacion";
			
    /** Constante del perfil del cliente. */
    public static final String PERFIL_CLIENTE = "perfilCliente";
    
    /** Constante del perfil del cliente. */
    public static final String EMAIL_CLIENTE = "emailCliente";
    
    /** Constante del perfil*/
    public static final String EMAIL = "email";
    
    /** Constante de acepto terminos y condiciones. */
    public static final String ACEPTO_TERMINOS_CONDICIONES = "aceptoTerminosCondiciones";
    
    /** Constante del tipo de mantenimiento de alerta. */
    public static final String INDICADOR = "indicador";
    
    /** Constante de los cinco digitos de la tarjeta . */
    public static final String TARJETA_5DIG = "tarjeta5Dig";
    	
	/** Constante del folio. */
	public static final String FOLIO = "folio";
	
	/** Constante de la fecha. */
	public static final String FECHA = "fecha";
	
	/** Constante de la hora. */
	public static final String HORA = "hora";
	
	/** Constante del switchEnrolamiento. */
	public static final String SWITCH_ENROLAMIENTO = "switchEnrolamiento";
	
	/** Constante del dispositivoFisico. */
	public static final String DISPOSITIVO_FISICO = "dispositivoFisico";

	/** Constante del estatusDispositivo. */
	public static final String ESTATUS_DISPOSITIVO = "estatusDispositivo";
	
	/** Constante de nuevo perfil */
	public static final String PERFIL_NUEVO_ETIQUETA = "perfilNuevo";
	

	/** Constante del estatusDispositivo. */
	public static final String VERSION_MIDLET = "VM";
	
	public static final String APP_VERSION_CONSULTA = "100";
	
	public static final String VERSION_FLUJO = "versionFlujo";
	
	public static final String CODIGO_TRANSACCION= "codTrans";
	
	//SPEI
	//CÛdigo Alex
	public static final String CELLPHONENUMBER_PARAM = "TelefonoDeposito";
	
	public static final String PERIODO = "periodo";
	
	//O3
	public static final String IUM="IUM";
	public static final String CUENTA="numeroCuenta";
	public static final String TIPO_CUENTA="tipoCuenta";
	public static final String TIPO_OPERACION="tipoOperacion";
	//public static final String PERIODO="periodo";
	public static final String NUMERO_CELULAR="numeroCelular";
	public static final String NUM_MOVTO = "numMovto";
	public static final String REFERENCIA_INTERNA = "referenciaInterna";
	public static final String REFERENCIA_AMPLIADA = "referenciaAmpliada";

	// Importes TDC
	//public static final String NUMERO_CELULAR = "numeroCelular";
	//public static final String TIPO_CUENTA = "tipoCuenta";
	public static final String OPERACION = "operacion";

    // Sinc Exp ST
    public static final String TIPO_SOLICITUD = "tipoSolicitud";
    public static final String OTP1 = "OTP1";
    public static final String OTP2 = "OTP2";
    public static final String NOMBRE_TOKEN = "nombreToken";

    //PAPERLESS VARIABLES CONFIRMACION
    public static final String ID_PRODUCTO ="idProducto";

    public static final String CLAVE_CONTRATACION ="claveContratacion";

    public static final String CLAVE_RESPUESTA = "claveRespuesta";

    public static final String ID_CAMPAÑA = "idCampana"; /***/

    public static final String CAUSA_INHIBICION = "causaInhibicion";

    //public static final String IUM = "IUM";

    //PAPERLESS VARIABLES TEXTO
    //public static final String OPERACION ="operacion";

    public static final String ID_PRODUCTO_TP ="idProducto";

    public static final String INDICADOR_PAGINA ="indicadorPagina";

    public static final String INDICADOR_SUBSECCION ="indicadorSubseccion";

    public static final String VERSION_PAPERLESS ="version";

    public static final String ULTIMA_SECCION_CONSULTADA ="ultimaSeccionConsultada";

    public static final String NUMERO_CELULAR_TP = "numeroCelular";

    public static final String IUM_TP = "IUM";

    public static final String FECHA_CORTE = "fechaCorte";

    public static final String REFERENCIA = "referencia";


    //PARAMETROS
    public static final String PARAMS_TEXTO_NT= "NT";
    public static final String PARAMS_TEXTO_NP= "NP";
    public static final String PARAMS_TEXTO_IU= "IU";
    public static final String PARAMS_TEXTO_TE= "TE";
    public static final String PARAMS_TEXTO_FO= "FO";
    public static final String PARAMS_TEXTO_CG= "CG";
    public static final String PARAMS_TEXTO_FE= "FE";
    public static final String PARAMS_TEXTO_NI= "NI";
    public static final String PARAMS_TEXTO_CV= "CV";
    public static final String PARAMS_TEXTO_OT= "OT";
    public static final String PARAMS_TEXTO_VA= "VA";
    public static final String PARAMS_TEXTO_DE= "DE";
    public static final String PARAMS_TEXTO_CA= "CA";
    public static final String PARAMS_TEXTO_RF= "RF";
    public static final String PARAMS_TEXTO_TP= "TP";
    public static final String PARAMS_TEXTO_CP= "CP";
    public static final String PARAMS_TEXTO_OA= "OA";
    public static final String PARAMS_TEXTO_IT= "IT";

    // SPEI
    public static final String TIPO_COMPROBANTE = "tipoComprobante";
    public static final String NUMERO_DE_REFERENCIA = "numeroDeReferencia";
    public static final String CORREO_DESTINO = "correoDestino";
    public static final String MENSAJE_A = "mensajeA";


    public static final String CONTRATO_CAMBIO_NOMINA_RESPUESTA =" <html>  <head>  <title>  Formato Alta Portabilidad BCOM  </title>  </head>  <body>  <div style=\"height:25px; clear:both;\"></div>  <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#01569A &#33;important; text-align:left; font-family: Arial Narrow, Arial, Helvetica, sans-serif; font-size:14pt; width:800px &#33;important; \">  <tr>  <td width=\"100%\" rowspan=\"9\" valign=\"top\">  <div>                                                                                                         <span style=\"color:#BFE7FB; font-size:28pt;\">Formato para solicitar a trav&eacute;s del servicio de banca electr&oacute;nica por internet las transferencias de los recursos correspondientes a prestaciones laborales </div> \n<br></br>\n   <div style=\\\"text-align:justify; \\\">  <b>CUENTA ORDENANTE:<\\\\/b>   <\\\\/div>                  <div style=\\\"height:10px; clear:both;\\\"><\\\\/div> <div style=\\\"text-align:justify; \\\">  <b>INSTITUCI&Oacute;N ORDENANTE:<\\\\/b>   <\\\\/div>  <div style=\\\"height:10px; clear:both;\\\"><\\\\/div>  <div style=\\\"text-align:justify; \\\">  <b>CUENTA RECEPTORA:<\\\\/b>   <\\\\/div>  <div style=\\\"height:10px; clear:both;\\\"><\\\\/div>  <div style=\\\"text-align:justify; \\\">  <b>INSTITUCI&Oacute;N RECEPTORA:<\\\\/b>   <\\\\/div> \n" +
            "  <div style=\"text-align:justify; \"> <br></br> Por este acto solicito que los recursos correspondientes a mis salario o, seg&uacute;n sea el caso, pensiones, as&iacute; como las dem&aacute;s prestaciones laborales que se depositen en la cuenta que me lleva la Instituci&oacute;n Ordenante se transfieran, sin costo a mi cargo, a la cuenta que me lleva la Instituci&oacute;n Receptora.  </div>                              <div style=\"height:20px; clear:both;\"></div> <div style=\"height:20px; clear:both;\"></div>  <div style=\"text-align:justify; \">  Las transferencias que por este medio solicito deber&aacute;n realizarse en las siguientes fechas, seg&uacute;n sea el caso:  </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"width:4%; float:left; text-align:left; display:inline-block; clear:none;\">  1.  </div>  <div style=\"width:96%; text-align:justify; float:left; display:inline-block; clear:none;\">   En el supuesto que los recursos que solicito transferir se abonen a mi cuenta de dep&oacute;sito que lleva esa instituci&oacute;n <b>a m&aacute;s tardar a las 15:00:00 horas</b> de un D&iacute;a H&aacute;bil Bancario, dicha instituci&oacute;n deber&aacute; transferir dichos recursos ese mismo D&iacute;a H&aacute;bil Bancario, o </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"width:4%; float:left; text-align:left; display:inline-block; clear:none;\">  2.  </div>                                                                                                  <div style=\"width:96%; text-align:justify; float:left; display:inline-block; clear:none;\"> Por otra parte, en el supuesto que los recursos que solicito transferir se abonen a mi cuenta de dep&oacute;sito que lleva esa instituci&oacute;n <b>despu&eacute;s de las 15:00:00 horas</b> de un D&iacute;a H&aacute;bil Bancario, dichos recursos deber&aacute;n ser transferidos a m&aacute;s tardar a la apertura del D&iacute;a H&aacute;bil Bancario inmediato siguiente.  </div>                                <div style=\"height:20px; clear:both;\"></div> <div style=\"text-align:justify; \">                                                                                                                                                                                                                                                                                                                                                                                                                                     Reconozco que las transferencias que, por medio de la presente, solicito hacer de los recursos correspondientes a mis salarios o, seg&uacute;n sea el caso, pensiones, as&iacute; como las dem&aacute;s prestaciones laborales comenzar&aacute;n a efectuarse (i) a partir del sexto D&iacute;a H&aacute;bil Bancario siguiente a la fecha en que la instituci&oacute;n que deba hacer dichas transferencias reciba esta solicitud, en caso que yo haya presentado esta solicitud directamente ante esa instituci&oacute;n, o bien, (ii) a partir del onceavo D&iacute;a H&aacute;bil Bancario siguiente al d&iacute;a en que la instituci&oacute;n que deba hacer dichas transferencias reciba esta solicitud por parte de la instituci&oacute;n que vaya a recibir dichos recursos, en caso que yo haya presentado esta solicitud ante esa &uacute;ltima instituci&oacute;n.                                                                                                                                                           </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"text-align:justify; \">  En todo caso, me reservo el derecho de cancelar la presente solicitud en cualquier momento, sujeto a la orden de cancelaci&oacute;n que presente en t&eacute;rminos del formato que me debe proporcionar la instituci&oacute;n que deba realizar la transferencia solicitada por medio de la presente.  </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"text-align:justify; \">                         Las transferencias que solicito por este conducto consisten en aquellas que las instituciones de cr&eacute;dito est&aacute;n obligadas a realizar de conformidad con lo dispuesto por el art&iacute;culo 18 de la Ley para la Transparencia y Ordenamiento de los Servicios Financieros y est&aacute;n reguladas por la Circular 3/2012, emitida por el Banco de M&eacute;xico, disponible en el portal en internet <b><u>www.banxico.org.mx</u></b> </div>  <div style=\"height:20px; clear:both;\"></div>                                                                                <div style=\"text-align:justify; \"> La presente solicitud ha sido recibida  por BBVA Bancomer el <u>__</u> de <u>____________</u> de <u>____</u> y, para dar seguimiento a su tramitaci&oacute;n, se le ha asignado el folio: <u>_________________________<u>  </div>  </td>  </tr>  </table>  <div style=\"height:20px; clear:both;\"></div>                                                                                                                         <div style=\"width:1000px; font-family: Arial Narrow, Arial, Helvetica, sans-serif; font-size:12pt; font-weight:bold; text-align:right;\"> </div>  </body>  </html>";


    //VISTA PATRIMONIAL

    public static String TELEFONO_TAG="telefono";
    public static String IUM_TAG="ium";
    public static String NUMCUENTA_TAG="numCuenta";
    public static String TIPOCONSULTA_TAG="tipoConsulta";
    public static String TOTALCARTERA_TAG="totalCartera";

}