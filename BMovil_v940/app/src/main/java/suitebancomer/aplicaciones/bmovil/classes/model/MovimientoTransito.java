package suitebancomer.aplicaciones.bmovil.classes.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by ana_mezquitillo on 17/02/16.
 */
public class MovimientoTransito implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * Mensaje informativo recibido
     */
    private String mensajeInformativo;
    /**
     * Fecha
     */
    private String fecha = null;

    /**
     * Descripcion
     */
    private String descripcion = null;

    /**
     * Hora
     */
    private String hora = null;

    /**
     * Importe
     */
    private String importe = null;

    /**
     * Autorizacion
     */
    private String numeroAutorizacion = null;

    /**
     * Situacion
     */
    private String situacion = null;

    /**
     * Nombre de Comercio
     */
    private String nombreComercio = null;

    /**
     * Default Constructor
     */
    public MovimientoTransito(){
        // TODO Auto-generated method stub
    }

    public MovimientoTransito(String descripcion, String fecha, String hora, String importe, String numeroAutorizacion, String situacion, String nombreComercio){
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.hora = hora;
        this.importe = importe;
        this.numeroAutorizacion = numeroAutorizacion;
        this.situacion = situacion;
        this.nombreComercio = nombreComercio;
    }

    public String getMensajeInformativo() {
        return mensajeInformativo;
    }

    public void setMensajeInformativo(String mensajeInformativo) {
        this.mensajeInformativo = mensajeInformativo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public String getNombreComercio() {
        return nombreComercio;
    }

    public void setNombreComercio(String nombreComercio) {
        this.nombreComercio = nombreComercio;
    }

/*
    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        // TODO Auto-generated method stub
        try{
            this.mensajeInformativo = parser.parseNextValue("mensajeInformativo");
            JSONArray datos = parser.parseNextValueWithArray("movsTransito");

            for (int i = 0; i < datos.length(); i++) {
                JSONObject jsonDatos= datos.getJSONObject(i);
                this.descripcion = jsonDatos.getString("descripcion");
                this.fecha = jsonDatos.getString("fecha");
                this.hora = jsonDatos.getString("hora");
                this.importe = jsonDatos.getString("importe");
                this.numeroAutorizacion = jsonDatos.getString("numeroAutorizacion");
                this.situacion = jsonDatos.getString("situacion");
                this.nombreComercio = jsonDatos.getString("nombreComercio");
            }
        }catch(Exception e){
            Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
        }

    }*/
}
