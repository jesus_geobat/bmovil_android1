package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.common.ToolsCommons;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionRegistroRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomercoms.classes.common.PropertiesManager;
import tracking.TrackingHelper;

public class ConfirmacionRegistroRetiroSinTarjetaViewController extends BaseViewController implements OnClickListener {

    private ImageView imageCompania;
    private TextView lblNombre;
    private TextView lblNumeroCelular;

    private LinearLayout contenedorContrasena;
    private LinearLayout contenedorNIP;
    private LinearLayout contenedorASM;
    private LinearLayout contenedorCVV;

    private EditText contrasena;
    private EditText nip;
    private EditText asm;
    private EditText cvv;

    private TextView instruccionesNIP;
    private TextView instruccionesASM;
    private TextView instruccionesCVV;

    private Button confirmarButton;

    private ConfirmacionRegistroRetiroSinTarjetaDelegate delegate;

    public BmovilViewsController parentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER, R.layout.layout_bmovil_confirmacion_registro_retiro_sin_tarjeta);
        //AMZ
        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("confreg", parentManager.estados);

        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        setDelegate(getParentViewsController().getBaseDelegateForKey(ConfirmacionRegistroRetiroSinTarjetaDelegate.CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID));

        delegate = (ConfirmacionRegistroRetiroSinTarjetaDelegate) getDelegate();
        delegate.setViewController(this);

        findViews();

        delegate.consultaDatos();

        configuraPantalla();
        moverScroll();

        contrasena.addTextChangedListener(new BmovilTextWatcher(this));
        nip.addTextChangedListener(new BmovilTextWatcher(this));
        asm.addTextChangedListener(new BmovilTextWatcher(this));
        cvv.addTextChangedListener(new BmovilTextWatcher(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    private void configuraPantalla() {
        mostrarContrasena(delegate.consultaDebePedirContrasena());
        mostrarNIP(delegate.consultaDebePedirNIP());
        mostrarASM(delegate.consultaInstrumentoSeguridad());
        mostrarCVV(delegate.consultaDebePedirCVV());

        confirmarButton.setOnClickListener(this);
    }

    /**
     * Se encarga de hacer visible el campo de contrasena y configurarlo con el
     * mensaje y limite de caracteres
     *
     * @param visibility
     */
    public void mostrarContrasena(boolean visibility) {
        contenedorContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
//            campoContrasena.setText(delegate.getEtiquetaCampoContrasenia());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
            contrasena.setFilters(userFilterArray);
            contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    /**
     * Se encarga de hacer visible el campo de nip y configurarlo con el
     * mensaje y limite de caracteres
     */
    public void mostrarNIP(boolean visibility) {
        contenedorNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
        nip.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
//            campoNIP.setText(delegate.getEtiquetaCampoNip());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
            nip.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = delegate.getTextoAyudaNIP();
            if (instrucciones.equals("")) {
                instruccionesNIP.setVisibility(View.GONE);
            } else {
                instruccionesNIP.setVisibility(View.VISIBLE);
                instruccionesNIP.setText(instrucciones);
            }
        } else {
            nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    /**
     * Se encarga de hacer visible el campo de asm y configurarlo con el
     * mensaje y limite de caracteres
     */
    public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP) {
        switch (tipoOTP) {
            case ninguno:
                contenedorASM.setVisibility(View.GONE);
                asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
                break;
            case codigo:
            case registro:
                contenedorASM.setVisibility(View.VISIBLE);
                InputFilter[] userFilterArray = new InputFilter[1];
                userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
                asm.setFilters(userFilterArray);
                cambiarAccionTexto(contrasena);
                cambiarAccionTexto(nip);
                asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
                break;
        }

        Constants.TipoInstrumento tipoInstrumento = delegate.consultaTipoInstrumentoSeguridad();

        switch (tipoInstrumento) {
            case OCRA:
//                campoASM.setText(delegate.getEtiquetaCampoOCRA());
                break;
            case DP270:
//                campoASM.setText(delegate.getEtiquetaCampoDP270());
                break;
            case SoftToken:
                if (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
                    asm.setText(Constants.DUMMY_OTP);
                    asm.setEnabled(false);
//                    campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
                } else {
                    asm.setText("");
                    asm.setEnabled(true);
//                    campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
                }
                break;
            default:
                break;
        }
        String instrucciones = delegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
        if (instrucciones.equals("")) {
            instruccionesASM.setVisibility(View.GONE);
        } else {
            instruccionesASM.setVisibility(View.VISIBLE);
            instruccionesASM.setText(instrucciones);
        }
    }

    private void cambiarAccionTexto(EditText campo) {
        if (campo.getVisibility() == View.VISIBLE) {
            campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        }
    }

    /**
     * Si el campo es visible se obtiene el valor ingresado, de lo contrario retorna empty
     *
     * @return valor de la contrasena.
     */
    public String pideContrasena() {
        if (contrasena.getVisibility() == View.GONE) {
            return "";
        } else {
            return contrasena.getText().toString();
        }
    }

    /**
     * Si el campo es visible se obtiene el valor ingresado, de lo contrario retorna empty
     *
     * @return valor del nip
     */
    public String pideNIP() {
        if (nip.getVisibility() == View.GONE) {
            return "";
        } else {
            return nip.getText().toString();
        }
    }

    /**
     * @return el valor de asm si es visible o empty
     */
    public String pideASM() {
        if (asm.getVisibility() == View.GONE) {
            return "";
        } else {
            return asm.getText().toString();
        }
    }

    /**
     * @return el valor de cvv si es visible o empty
     */
    public String pideCVV() {
        if (cvv.getVisibility() == View.GONE) {
            return "";
        } else {
            return cvv.getText().toString();
        }
    }

    /**
     * @param visibility
     */
    public void mostrarCVV(boolean visibility) {
        contenedorCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);

        if (visibility) {
//            campoCVV.setText(delegate.getEtiquetaCampoCVV());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
            cvv.setFilters(userFilterArray);
            String instrucciones = delegate.getTextoAyudaCVV();
            instruccionesCVV.setText(instrucciones);
            instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(nip);
            cambiarAccionTexto(asm);
            cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == confirmarButton && !parentViewsController.isActivityChanging()) {
            botonConfirmarClick();
        }
    }

    /**
     * Se confirma la operacion y se hace la peticion
     */
    public void botonConfirmarClick() {
        if (delegate.validaDatos()) {
            String contrasena = pideContrasena();
            String nip = pideNIP();
            String asm = pideASM();
            String cvv = pideCVV();
            delegate.enviaPeticionOperacion(contrasena, nip, asm, cvv);
            //ARR

            Map<String, Object> operacionRealizadaMap = new HashMap<String, Object>();

            //Comprobacion de titulos

            if (getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_miscuentas)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;transferencias+mis cuentas");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;transferencias+otra cuenta bbva bancomer");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;transferencias+cuenta express");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;transferencias+otros bancos");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;transferencias+dinero movil");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.transferir_otrosBBVA_TDC_title)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;pagar+tarjeta credito");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.tiempo_aire_title)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;comprar+tiempo aire");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            }
        }
    }

    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

    private void findViews() {
        imageCompania = (ImageView) findViewById(R.id.imagen_compania);
        lblNombre = (TextView) findViewById(R.id.valor_nombre);
        lblNumeroCelular = (TextView) findViewById(R.id.valor_numero_celular);

        contenedorContrasena = (LinearLayout) findViewById(R.id.campo_confirmacion_contrasena_layout);
        contenedorNIP = (LinearLayout) findViewById(R.id.campo_confirmacion_nip_layout);
        contenedorASM = (LinearLayout) findViewById(R.id.campo_confirmacion_asm_layout);
        contenedorCVV = (LinearLayout) findViewById(R.id.campo_confirmacion_cvv_layout);

        contrasena = (EditText) contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
        nip = (EditText) contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
        asm = (EditText) contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
        cvv = (EditText) contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);

        instruccionesNIP = (TextView) contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
        instruccionesASM = (TextView) contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
        instruccionesCVV = (TextView) contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);

        confirmarButton = (Button) findViewById(R.id.confirmacion_confirmar_button);
    }

    public void limpiarCampos() {
        contrasena.setText("");
        nip.setText("");
        asm.setText("");
        cvv.setText("");
    }

    public void setCompania(String compania) {
        if (compania.equals("TELCEL")) {
            imageCompania.setImageDrawable(getResources().getDrawable(R.drawable.im_telcel));
        } else if (compania.equals("MOVISTAR")) {
            imageCompania.setImageDrawable(getResources().getDrawable(R.drawable.im_movistar));
        } else if (compania.equals("IUSACELL")) {
            imageCompania.setImageDrawable(getResources().getDrawable(R.drawable.im_iusacell));
        } else if (compania.equals("UNEFON")) {
            imageCompania.setImageDrawable(getResources().getDrawable(R.drawable.im_unefon));
        } else if (compania.equals("NEXTEL")) {
            imageCompania.setImageDrawable(getResources().getDrawable(R.drawable.im_nextel));
        }
    }

    public void setNombre(String nombre) {
        lblNombre.setText(nombre);
    }

    public void setNumeroCelular(String numeroCelular) {
        lblNumeroCelular.setText(ToolsCommons.formatPhoneNumber(numeroCelular));
    }

}