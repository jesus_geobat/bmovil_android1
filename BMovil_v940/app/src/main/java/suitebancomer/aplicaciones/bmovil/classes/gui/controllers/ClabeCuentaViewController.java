package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.base.callback.CallBackAPI;
import com.bancomer.base.callback.CallBackSession;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.AbstractContactMannager;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.SincronizarSesion;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.gallery.Image;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ClabeCuentaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DineroMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.views.GifView;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountField;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.SeleccionHorizontalViewController;
import tracking.TrackingHelper;

//import com.bancomer.base.callback.CallBackAPI;


public class ClabeCuentaViewController extends BaseViewController implements CallBackModule, CallBackSession, CallBackAPI {
	/**
	 * Codigo de la petici�n de un contacto desde la agenda.
	 */
	public static final int ContactsRequestCode = 1;

	/**
	 * Contenedor para el componente CuentaOrigen.
	 */
	private LinearLayout ctaOrigenLayout;

	/**
	 * Contenedor para el componente SeleccionHorizontal.
	 */
	private LinearLayout seleccionHorizontalLayout;

	/**
	 * El componente CuentaOrigen
	 */
	private CuentaOrigenViewController ctaOrigen;

	/**
	 * El componente SeleccionHorizontal.
	 */

	/**
	 * El delegado para este controlador.
	 */
	private ClabeCuentaDelegate delegate;

	/**
	 * Campo de clabe
	 */

	private TextView clabe;
	private TextView numCuenta;
	private TextView numTarjeta;
	private TextView numcliente;
	private TextView lblclabe;
	private TextView lblnumCuenta;
	private TextView lblnumTarjeta;
	private TextView lblNumCliente;

	private ImageButton btnClabe;
	private ImageButton btnTarjeta;
	private ImageButton btnCuenta;
	private ImageButton btnCliente;
	private GifView gv0;
	private GifView gv;
	private GifView gv1;
	private GifView gv2;
	LinearLayout datos;

	private View lineCero;
	private View lineOne;
	private View lineTwo;

	private TextView copy;
	//AMZ
	public BmovilViewsController parentManager;
			//AMZ

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_clabe_cuenta);
		setTitle(R.string.clabe_cuenta_titulo, R.drawable.bmovil_consultar_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("clabe cuenta", parentManager.estados);


		//TrackingHelper.trackState("nuevo", parentManager.estados);

		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(ClabeCuentaDelegate.CLABE_CUENTA_DELEGATE_ID));
		delegate = (ClabeCuentaDelegate)getDelegate();
		delegate.setViewController(this);

		init();
	}

	/**
	 * Inicializa la vista.
	 */
	private void init() {
		findViews();
		//scaleForCurrentScreen();
		cargarComponenteCuentaOrigen();
	}

	/**
	 * Busca las vistas en los layouts.
	 */
	private void findViews() {
		ctaOrigenLayout = (LinearLayout)findViewById(R.id.ctaOrigenLayout);
		//
		clabe=(TextView)findViewById(R.id.lblCClabe);
		numCuenta=(TextView)findViewById(R.id.lblCuenta);
		numTarjeta=(TextView)findViewById(R.id.lblTarjeta);
		numcliente=(TextView)findViewById(R.id.lblCliente);
		lblclabe=(TextView)findViewById(R.id.lblClabe);
		lblnumCuenta=(TextView)findViewById(R.id.lblNumCuenta);
		lblnumTarjeta=(TextView)findViewById(R.id.lblNumTarjeta);
		lblNumCliente=(TextView)findViewById(R.id.lblNumCliente);
		btnCliente=(ImageButton)findViewById(R.id.ic_copy0);
		btnClabe=(ImageButton)findViewById(R.id.ic_copy);
		btnCuenta=(ImageButton)findViewById(R.id.ic_copy2);
		btnTarjeta=(ImageButton)findViewById(R.id.ic_copy3);
		lineCero=(View)findViewById(R.id.lineCero);
		lineOne=(View)findViewById(R.id.lineOne);
		lineTwo=(View)findViewById(R.id.lineTwo);
		copy=(TextView)findViewById(R.id.copy);
		gv0= (GifView)findViewById(R.id.ic_correcto0);
		gv = (GifView)findViewById(R.id.ic_correcto);
		gv1 = (GifView)findViewById(R.id.ic_correcto2);
		gv2 = (GifView)findViewById(R.id.ic_correcto3);
		datos = (LinearLayout)findViewById(R.id.datos);

	}


	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scale(findViewById(R.id.rootLayout));

		guiTools.scale(findViewById(R.id.lblNumCliente), true);
		guiTools.scale(numcliente, true);

		guiTools.scale(findViewById(R.id.lblClabe));
		guiTools.scale(clabe, true);

		guiTools.scale(findViewById(R.id.lblNumTarjeta), true);
		guiTools.scale(numTarjeta, true);

		guiTools.scale(findViewById(R.id.lblNumCuenta), true);
		guiTools.scale(numCuenta, true);

		guiTools.scale(findViewById(R.id.btnContinuar));
	}

	/**
	 * Carga el componente CuentaOrigen.
	 */
	private void cargarComponenteCuentaOrigen() {
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		@SuppressWarnings("deprecation")
		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		ctaOrigen = new CuentaOrigenViewController(this, params, parentViewsController, this);
		ctaOrigen.setDelegate(delegate);

		Iterator<Account> iterator = listaCuetasAMostrar.iterator();
		while (iterator.hasNext()) {
			Account account = iterator.next();
			if (account.getType().equals("CE") || (account.getType().equals("TP"))) {
				iterator.remove();
			}
		}

		ctaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		ctaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.clabe_cuenta_titulo_cuentaconsultar));
		ctaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getCuentaOrigen()));
		ctaOrigen.init();

		ctaOrigenLayout.addView(ctaOrigen);
		delegate.setCuentaOrigen(ctaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());

		delegate.consultarClabe();
	}

	public void reactivarCtaOrigen(Account cuenta){
			delegate.setCuentaOrigen(cuenta);
			ctaOrigen.getImgDerecha().setEnabled(true);
			ctaOrigen.getImgIzquierda().setEnabled(true);
			ctaOrigen.getVistaCtaOrigen().setEnabled(ctaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().size() > 1);
			delegate.consultarClabe();
	}
	public void mostrarDatos(String clabe,String cuenta,String tarjeta,String type){
		copy.setText(getString(R.string.clabe_cuenta_ayudaCopiar));
		this.clabe.setVisibility(View.VISIBLE);
		this.lblclabe.setVisibility(View.VISIBLE);
		datos.setVisibility(View.VISIBLE);
		if (type.equals("CE")) {
			copy.setText("");
			datos.setVisibility(View.INVISIBLE);
			//this.ctaOrigen.setVisibility(View.INVISIBLE);
			this.clabe.setVisibility(View.INVISIBLE);
			this.lblclabe.setVisibility(View.INVISIBLE);
			this.numCuenta.setVisibility(View.INVISIBLE);
			this.lblnumCuenta.setVisibility(View.INVISIBLE);
			this.numTarjeta.setVisibility(View.INVISIBLE);
			this.lblnumTarjeta.setVisibility(View.INVISIBLE);
			this.lineCero.setVisibility(View.INVISIBLE);
			this.lineTwo.setVisibility(View.INVISIBLE);
			this.lineOne.setVisibility(View.INVISIBLE);
			this.btnClabe.setVisibility(View.INVISIBLE);
			this.btnCuenta.setVisibility(View.INVISIBLE);
			this.btnTarjeta.setVisibility(View.INVISIBLE);
			this.numcliente.setVisibility(View.INVISIBLE);
			this.lblNumCliente.setVisibility(View.INVISIBLE);
			this.btnCliente.setVisibility(View.INVISIBLE);
			this.showInformationAlert("Operación no permitida para Cuentas Express");
		}else if(type.equals("TC")){
			this.numcliente.setText(tarjeta);
			this.lblNumCliente.setText("Número de tarjeta");
			this.btnCliente.setVisibility(View.VISIBLE);
			this.numCuenta.setVisibility(View.INVISIBLE);
			this.lblnumCuenta.setVisibility(View.INVISIBLE);
			this.numTarjeta.setVisibility(View.INVISIBLE);
			this.lblnumTarjeta.setVisibility(View.INVISIBLE);
			this.btnCuenta.setVisibility(View.INVISIBLE);
			this.btnTarjeta.setVisibility(View.INVISIBLE);
			this.lineTwo.setVisibility(View.INVISIBLE);
			this.lineOne.setVisibility(View.INVISIBLE);
			this.btnCliente.setVisibility(View.VISIBLE);
			this.lineCero.setVisibility(View.VISIBLE);
			this.numcliente.setVisibility(View.VISIBLE);
			this.lblNumCliente.setVisibility(View.VISIBLE);
			this.clabe.setVisibility(View.INVISIBLE);
			this.lblclabe.setVisibility(View.INVISIBLE);
			this.btnClabe.setVisibility(View.INVISIBLE);


		}else if(type.equals("CH")||type.equals("AH")||type.equals("LI")){
			this.clabe.setText(clabe);
			//Se pasa cuenta a 10 posiciones
			String newCuenta = cuenta.substring(10,20);

			this.numCuenta.setText(newCuenta);
			this.numTarjeta.setText(tarjeta);
			this.numcliente.setText(Session.getInstance(this).getClientNumber());
			this.lblNumCliente.setText("Número de cliente");

			this.numcliente.setVisibility(View.VISIBLE);
			this.lblNumCliente.setVisibility(View.VISIBLE);
			this.numCuenta.setVisibility(View.VISIBLE);
			this.lblnumCuenta.setVisibility(View.VISIBLE);
			this.numTarjeta.setVisibility(View.INVISIBLE);
			this.lblnumTarjeta.setVisibility(View.INVISIBLE);
			this.lineCero.setVisibility(View.VISIBLE);
			this.lineTwo.setVisibility(View.VISIBLE);
			this.lineOne.setVisibility(View.VISIBLE);
			this.btnCliente.setVisibility(View.VISIBLE);
			this.btnClabe.setVisibility(View.VISIBLE);
			this.btnCuenta.setVisibility(View.VISIBLE);
			this.btnTarjeta.setVisibility(View.INVISIBLE);
		}
	}
	public void ocultarDatos(){
		copy.setText("");
		datos.setVisibility(View.VISIBLE);
		//this.ctaOrigen.setVisibility(View.INVISIBLE);
		this.numcliente.setVisibility(View.INVISIBLE);
		this.lblNumCliente.setVisibility(View.INVISIBLE);
		this.clabe.setVisibility(View.INVISIBLE);
		this.lblclabe.setVisibility(View.INVISIBLE);
		this.numCuenta.setVisibility(View.INVISIBLE);
		this.lblnumCuenta.setVisibility(View.INVISIBLE);
		this.numTarjeta.setVisibility(View.INVISIBLE);
		this.lblnumTarjeta.setVisibility(View.INVISIBLE);
		this.lineTwo.setVisibility(View.INVISIBLE);
		this.lineOne.setVisibility(View.INVISIBLE);
		this.lineCero.setVisibility(View.INVISIBLE);
		this.btnCliente.setVisibility(View.INVISIBLE);
		this.btnClabe.setVisibility(View.INVISIBLE);
		this.btnCuenta.setVisibility(View.INVISIBLE);
		this.btnTarjeta.setVisibility(View.INVISIBLE);
	}
	


	public void botonContinuarClick(View view) {

	}


	public void copycliente(View view){
		try {
			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
				android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				clipboard.setText(numcliente.getText());
			} else {
				android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", numcliente.getText());
				clipboard.setPrimaryClip(clip);
			}
			btnCliente.setVisibility(View.INVISIBLE);
			gv0.setVisibility(View.VISIBLE);
			gv0.setGIFResource(R.drawable.an_ic_correcto);
			Toast.makeText(ClabeCuentaViewController.this, "Copiado al portapapeles",
					Toast.LENGTH_SHORT).show();
			new CountDownTimer(800, 800) {

				public void onTick(long millisUntilFinished) {
				}
				public void onFinish() {
					btnCliente.setVisibility(View.VISIBLE);
					gv0.setVisibility(View.INVISIBLE);
				}
			}.start();
		}catch (Exception e){
			Toast.makeText(ClabeCuentaViewController.this, "Error: "+e+"",
					Toast.LENGTH_SHORT).show();
		}

	}

	public void copytarjeta(View view){
		try {
			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
				android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				clipboard.setText(numTarjeta.getText());
			} else {
				android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", numTarjeta.getText());
				clipboard.setPrimaryClip(clip);
			}
			//ImageButton btnPress = (ImageButton) findViewById (R.id.ic_copy3);
			btnTarjeta.setVisibility(View.INVISIBLE);
			gv2.setVisibility(View.VISIBLE);
			gv2.setGIFResource(R.drawable.an_ic_correcto);
			Toast.makeText(ClabeCuentaViewController.this, "Copiado al portapapeles",
					Toast.LENGTH_SHORT).show();
			new CountDownTimer(800, 800) {

				public void onTick(long millisUntilFinished) {
				}
				public void onFinish() {
					btnTarjeta.setVisibility(View.VISIBLE);
					gv2.setVisibility(View.INVISIBLE);
				}
			}.start();
		}catch (Exception e){
			Toast.makeText(ClabeCuentaViewController.this, "Error: "+e+"",
					Toast.LENGTH_SHORT).show();
		}

	}
	public void copycuenta(View view){
		try {
			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
				android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				clipboard.setText(numCuenta.getText());
			} else {
				android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", numCuenta.getText());
				clipboard.setPrimaryClip(clip);
			}

			btnCuenta.setVisibility(View.INVISIBLE);
			gv1.setVisibility(View.VISIBLE);
			gv1.setGIFResource(R.drawable.an_ic_correcto);
			//AnimationDrawable correct = (AnimationDrawable) btnCuenta.getBackground();
			//correct.start();

			Toast.makeText(ClabeCuentaViewController.this, "Copiado al portapapeles", Toast.LENGTH_SHORT).show();
			new CountDownTimer(800, 800) {
				public void onTick(long millisUntilFinished) {
				}
				public void onFinish() {
					btnCuenta.setVisibility(View.VISIBLE);
					gv1.setVisibility(View.INVISIBLE);
				}
			}.start();

		}catch (Exception e){
		Toast.makeText(ClabeCuentaViewController.this, "Error: "+e+"",
				Toast.LENGTH_SHORT).show();
	}

}
	public void copyclabe(View view){
		try {
			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
				android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				clipboard.setText(clabe.getText());
			} else {
				android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", clabe.getText());
				clipboard.setPrimaryClip(clip);
			}
			//btnClabe.setBackgroundResource(R.drawable.an_ic_correcto);

			btnClabe.setVisibility(View.INVISIBLE);
			gv.setVisibility(View.VISIBLE);
			gv.setGIFResource(R.drawable.an_ic_correcto);
			Toast.makeText(ClabeCuentaViewController.this, "Copiado al portapapeles",
					Toast.LENGTH_SHORT).show();

			new CountDownTimer(800, 800) {

				public void onTick(long millisUntilFinished) {

				}

				public void onFinish() {

					btnClabe.setVisibility(View.VISIBLE);
					gv.setVisibility(View.INVISIBLE);
					//btnClabe.setBackgroundResource(R.drawable.an_ic_clabe);
				}
			}.start();
		}catch (Exception e){
			Toast.makeText(ClabeCuentaViewController.this, "Error: "+e+"",
					Toast.LENGTH_SHORT).show();
		}
	}

	public void irmenu(View view){
		delegate.showMenu();
	}


	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

	@Override
	protected void onResume() {	
		super.onResume();
		setHabilitado(true);
		if(delegate != null)
			delegate.setViewController(this);
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (parentViewsController.consumeAccionesDePausa()) {
			return;
		}
	}

	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID);
		super.goBack();

		/*

		InitOpinator init = InitOpinator.getInstance();
		// Establecer Contexto en el que se debe mostrar Opinator
		init.setContext(this);
		// Establecer callback en API
		init.setCallBackAPI(this);
		// Establecer designator para punto de reentrada tras funcion API
		init.setDesignator(bancomer.api.opinator.commons.Constants.OpiAtrasDinMovilAbandono);
		// Establecer que la aplicacion cambia de activity
		getParentViewsController().setActivityChanging(true);
		if(delegate.esFrecuente())
		{
			init.esFrecuente(true);
		}
		// Mostrar opinator
		init.showOpinatorViewController();
		*/
	}

	@Override
	public void userInteraction() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();
	}

	@Override
	public void cierraSesionBackground(Boolean isChanging) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		parentViewsController.setActivityChanging(isChanging);
		if (parentManager.consumeAccionesDePausa()) {
			finish();
			if (MenuHamburguesaViewsControllers.getIsOterApp()) {
				MenuHamburguesaViewsControllers.setIsOterApp(Boolean.FALSE);
			}
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	public void returnToPrincipal() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
	}

	@Override
	public void returnDataFromModule(String operation, bancomer.api.common.io.ServerResponse response) {

	}

	@Override
	public void returnDataFromModule(String operation, suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse response) {

	}

	@Override
	public void cierraSesion() {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication()
				.setApplicationInBackground(Boolean.FALSE);
		SuiteApp.getInstance().getBmovilApplication().logoutApp(Boolean.FALSE);
	}

	@Override
	public void returnMenuPrincipal() {
		Session.getInstance(SuiteApp.appContext).reloadFiles();
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
	}

	@Override
	public void closeSession(String userName, String ium, String client) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBmovilApp().closeSession(userName, ium, client);
	}

	@Override
	public void setClientProfile(Constants.Perfil perfil) {

	}
}
