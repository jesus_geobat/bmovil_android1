package suitebancomer.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bancomer.mbanking.R;

/**
 * Created by alejandro.ruizgarcia on 16/11/2016.
 */
public class TokenPopUpAdapter extends PagerAdapter {

    public enum ModelObject {

        AVISO(1, R.layout.popup_token_aviso),
        BMOVIL(2, R.layout.popup_token_enbmovil),
        SOLICITADOS(3, R.layout.popup_token_datos_solicitados),
        CONFIDENCIALES(4, R.layout.popup_token_datos_confidenciales),
        SMS(5,R.layout.popup_token_ingresa_codigo),
        FELICIDADES(6,R.layout.popup_token_felicidades);



        private int mTitleResId;
        private int mLayoutResId;

        ModelObject(int titleResId, int layoutResId) {
            mTitleResId = titleResId;
            mLayoutResId = layoutResId;
        }

        public int getTitleResId() {
            return mTitleResId;
        }

        public int getLayoutResId() {
            return mLayoutResId;
        }
    }
    @Override
    public int getCount() {
        return ModelObject.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }



    private Context mContext;

    public TokenPopUpAdapter(Context context){
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position){
        ModelObject modelObject = ModelObject.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(modelObject.getLayoutResId(), collection, false);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        ModelObject customPagerEnum = ModelObject.values()[position];
        return mContext.getString(customPagerEnum.getTitleResId());
    }

}
