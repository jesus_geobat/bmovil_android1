package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

        import android.content.Intent;
        import android.os.Bundle;
        import android.text.Html;
        import android.view.KeyEvent;
        import android.view.View;
        import android.widget.ImageButton;
        import android.widget.TextView;

        import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ContratacionBanqueroPersonalDelegate;
        import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MenuPrincipalDelegate;
        import suitebancomer.aplicaciones.bmovil.classes.model.ContratoGestor;
        import suitebancomer.classes.gui.controllers.BaseViewController;
        import com.bancomer.mbanking.R;
        import com.bancomer.mbanking.SuiteApp;
/**
 * Created by eluna on 20/06/2016.
 */
public class ActivacionBanqueroPersonalViewController extends BaseViewController implements View.OnClickListener
{
    private BmovilViewsController parentManager;
    private TextView txttitleActivacionBanquero;
    private TextView nombreBanquero, telefono, horario, fecha, hora;
    private ImageButton btnMenu;
    private TextView linkConsultarContrato;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState,0, R.layout.layout_bmovil_activacion_gestor_remoto);
        final Intent intent = new Intent(this,SplashBanqueroViewController.class);
        intent.putExtra("tipoRespuesta",R.drawable.splash_op_exit);
        startActivity(intent);
        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        SuiteApp suiteApp = (SuiteApp) getApplication();
        setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
        setDelegate(parentViewsController.getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID));
        initValues();
    }

    public void initValues()
    {
        txttitleActivacionBanquero = (TextView) findViewById(R.id.titleActivacionBanquero);
        txttitleActivacionBanquero.setText(Html.fromHtml("<font>Ahora cuentas con un <br/></font><font color='#009ee5'>Banquero Personal Contigo, <br/></font><font>contáctalo cuando quieras.</font>"));
        nombreBanquero = (TextView) findViewById(R.id.txtnombreBanquero);
        telefono = (TextView) findViewById(R.id.txtTelefono);
        fecha = (TextView) findViewById(R.id.txtFecha);
        hora = (TextView) findViewById(R.id.txtHora);
        nombreBanquero.setText(ContratoGestor.getInstance().getNombreGestor());
        telefono.setText(ContratoGestor.getInstance().getTelefono());
        fecha.setText(ContratoGestor.getInstance().getFechaOperacion());
        hora.setText(ContratoGestor.getInstance().getHoraOperacion());
        btnMenu = (ImageButton) findViewById(R.id.btnMenuBanquero);
        btnMenu.setOnClickListener(this);
        linkConsultarContrato = (TextView) findViewById(R.id.linkConsultarContrato);
        linkConsultarContrato.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public boolean onKeyDown(final int keyCode,final KeyEvent event) {

        switch (keyCode){
            case KeyEvent.KEYCODE_BACK:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onClick(final View v)
    {
        if (v.equals(btnMenu)){
            parentManager.showMenuPrincipal();
        }
        if (v.equals(linkConsultarContrato)){
            final ContratacionBanqueroPersonalDelegate delegate = new ContratacionBanqueroPersonalDelegate();
            delegate.peticionConsultaContratoGestor(this);
        }
    }
}
