package suitebancomer.aplicaciones.bmovil.classes.model;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
/**
 * Created by elunag on 06/07/16.
 */
public class GetListCuenta implements ParsingHandler
{
    public String code;
    public String description;
    public String status;
    public String errorCode;
    private ArrayList<ListPortability> portabilities;

    private static GetListCuenta ourInstance = new GetListCuenta();

    public static GetListCuenta getInstance() {
        return ourInstance;
    }

    public GetListCuenta() {
    }

    public GetListCuenta(final String idBanck,final String createdReferenceNumber,final String companyNumber,final String referenceNumber){


    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public ArrayList<ListPortability> getPortabilities() {
        return portabilities;
    }

    public void setPortabilities(ArrayList<ListPortability> portabilities) {
        this.portabilities = portabilities;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        try {
            String portability = parser.parseNextValue("response");
            code=parser.parserNextObject("status").getString("code");
            description =parser.parserNextObject("status").getString("description");

            final JsonObject json = (JsonObject) new JsonParser().parse(portability).getAsJsonObject();
            final JsonArray array = (JsonArray) json.get("portability");
            final Gson gson = new Gson();

            final ArrayList<ListPortability> listaPortability = new ArrayList<ListPortability>();
            ListPortability getListPortability;
            final Iterator<JsonElement> iterator = array.iterator();

            while (iterator.hasNext()) {
                getListPortability = gson.fromJson((JsonObject) iterator.next(), ListPortability.class);
                listaPortability.add(getListPortability);
            }
            portabilities = listaPortability;
        } catch (JSONException e) {
            //e.printStackTrace();
        }finally {
            status= parser.parseNextValue("status");
            errorCode= parser.parseNextValue("errorCode");
            description= parser.parseNextValue("description");
        }
    }
}
