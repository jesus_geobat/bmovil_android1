package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bbvacredit.models.CambiaNominaContrato;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ResultadosViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.GetListBanks;
import suitebancomer.aplicaciones.bmovil.classes.model.PortabilidadData;
import suitebancomer.aplicaciones.commservice.commons.BodyType;
import suitebancomer.aplicaciones.commservice.commons.MethodType;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.commons.PojoGeneral;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class ResultadosDelegate extends DelegateBaseOperacion {
	
	public final static long RESULTADOS_DELEGATE_ID = 0x1ef4f4c61ca112bfL;
	
	private ArrayList<Object> datosLista;
	private DelegateBaseOperacion operationDelegate;
	private int listaOpcionesMenu;
	//One Click
	private boolean isSMS=false;
	private boolean isEmail=false;
	//Termina One Click
	private Boolean frecOpOK = false;
	private String tipoOperacion;


	/**
	 * PendingIntent to tell the SMS app to notify us.
	 */
	private PendingIntent mSentPendingIntent;
	
	/** 
	 * The BroadcastReceiver that we use to listen for the notification back.
	 */
	private BroadcastReceiver mBroadcastReceiver;
	
	private ResultadosViewController resultadosViewController;
	
	public Boolean getFrecOpOK() {
		return frecOpOK;
	}

	public void setFrecOpOK(Boolean frecOpOK) {
		this.frecOpOK = frecOpOK;
	}
	
	public ResultadosDelegate(DelegateBaseOperacion operationDelegate) {
		this.operationDelegate = operationDelegate;
		listaOpcionesMenu = operationDelegate.getOpcionesMenuResultados();
	}
	
	public DelegateBaseOperacion getOperationDelegate() {
		return operationDelegate;
	}
	
	public void setResultadosViewController(ResultadosViewController viewController) {
		this.resultadosViewController = viewController;
	}

	//freceunte correo electronico
	public ResultadosViewController getResultadosViewController() {
		return resultadosViewController;
	}
	public BroadcastReceiver getmBroadcastReceiver() {
		return mBroadcastReceiver;
	}

	public void setmBroadcastReceiver(BroadcastReceiver mBroadcastReceiver) {
		this.mBroadcastReceiver = mBroadcastReceiver;
	}

	public void consultaDatosLista() {
		resultadosViewController.setListaDatos(operationDelegate.getDatosTablaResultados());
		if(operationDelegate instanceof InterbancariosDelegate){
			//resultadosViewController.setListaClave(((InterbancariosDelegate)operationDelegate).getDatosTablaClave());
			if(!((InterbancariosDelegate)operationDelegate).isBajaFrecuente() && ((InterbancariosDelegate)operationDelegate).validaTC())
				resultadosViewController.setListaClave(((InterbancariosDelegate)operationDelegate).getDatosTablaClave());

		}
	}

	public DelegateBaseOperacion consultaOperationDelegate() {
		return operationDelegate;
	}
	
	public void enviaPeticionOperacion() {
		
	}
	
	public void consultaOpcionesMenu() {
		
	}
	
	public void consultaTextoSMS() {
		
	}

	public void enviaSMS() {
		//One click
				if(operationDelegate instanceof ExitoILCDelegate){
					isSMS=true;
					isEmail=false;
					((ExitoILCDelegate) operationDelegate).setcontroladorExitoILCView(resultadosViewController);
					((ExitoILCDelegate) operationDelegate).realizaOperacion(Server.EXITO_OFERTA, resultadosViewController, false, true);
					
				}else if(operationDelegate instanceof ExitoEFIDelegate){
					isSMS=true;
					isEmail=false;
					((ExitoEFIDelegate) operationDelegate).setcontroladorExitoILCView(resultadosViewController);
					((ExitoEFIDelegate) operationDelegate).realizaOperacion(Server.EXITO_OFERTA_EFI, resultadosViewController, false, true);
				}else{//termina One click
    	String smsText = Tools.removeSpecialCharacters(operationDelegate.getTextoSMS());

		mSentPendingIntent = PendingIntent.getBroadcast(resultadosViewController, 0,  new Intent(Constants.SENT), 0);
        SmsManager smsMgr = SmsManager.getDefault();
        resultadosViewController.muestraIndicadorActividad(resultadosViewController.getString(R.string.label_information),
				   resultadosViewController.getString(R.string.sms_sending));
        String mPhone=Session.getInstance(SuiteApp.appContext).getUsername();
        
        if(mBroadcastReceiver == null){
    		mBroadcastReceiver = resultadosViewController.createBroadcastReceiver();
        }
        
        ArrayList<String> messages = smsMgr.divideMessage(smsText);
        for (int i = 0; i < messages.size(); i++) {
		     String text = messages.get(i).trim();
		     if(text.length()>0) {
		    	 if(Server.ALLOW_LOG) Log.d("sms mensaje", text);
	  		     // send the message, passing in the pending intent, sentPI
		    	 smsMgr.sendTextMessage(mPhone, null, text, mSentPendingIntent, null);
		    	 resultadosViewController.registerReceiver(mBroadcastReceiver, new IntentFilter(Constants.SENT));
		     }
	     }
				}
    }

	public void guardaPDF() {
		
	}
	
	public void enviaEmail() {
		//One Click
				if(operationDelegate instanceof ExitoILCDelegate){
					isEmail=true;
					isSMS=false;
					((ExitoILCDelegate) operationDelegate).setcontroladorExitoILCView(resultadosViewController);
					((ExitoILCDelegate) operationDelegate).realizaOperacion(Server.EXITO_OFERTA, resultadosViewController, true, false);
					
				}else if(operationDelegate instanceof ExitoEFIDelegate){
					isEmail=true;
					isSMS=false;
					((ExitoEFIDelegate) operationDelegate).setcontroladorExitoILCView(resultadosViewController);
					((ExitoEFIDelegate) operationDelegate).realizaOperacion(Server.EXITO_OFERTA_EFI, resultadosViewController, true, false);
				}else{// Termina One click
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showEnviarCorreo(this);
	}
	}
	public void guardaFrecuente() {
		((BmovilViewsController)resultadosViewController.getParentViewsController()).showAltaFrecuente(operationDelegate);
	}
	
	public void guardaRapido() {
		
	}
	
	public void borraOperacion() {
		
	}
	
	@Override
	public int getNombreImagenEncabezado() {
		return operationDelegate.getNombreImagenEncabezado();
	}
	
	@Override
	public int getTextoEncabezado() {
		return operationDelegate.getTextoEncabezado();
	}
	
	@Override
	public String getTextoTituloResultado() {
		return operationDelegate.getTextoTituloResultado();
	}
	
	@Override
	public int getColorTituloResultado() {
		return operationDelegate.getColorTituloResultado();
	}
	
	@Override
	public String getTextoPantallaResultados() {
		return operationDelegate.getTextoPantallaResultados();
	}
	
	@Override
	public String getTituloTextoEspecialResultados() {
		return operationDelegate.getTituloTextoEspecialResultados();
	}
	
	@Override
	public String getTextoEspecialResultados() {
		return operationDelegate.getTextoEspecialResultados();
	}
	
	@Override
	public int getOpcionesMenuResultados() {
		return listaOpcionesMenu;
	}
	
	@Override
	public String getTextoAyudaResultados() {
		return operationDelegate.getTextoAyudaResultados();
	}
	
	//One CLick
		public void analyzeResponse(int operationId, ServerResponse response) {
			if(operationId==Server.EXITO_OFERTA){
				if(isSMS){
				resultadosViewController.showInformationAlert(R.string.bmovil_alert_exitoefi_sms);
				}else if(isEmail){
				resultadosViewController.showInformationAlert(R.string.bmovil_alert_exitoefi_email);	
				}
			}else if(operationId==Server.EXITO_OFERTA_EFI){
				if(isSMS){
					resultadosViewController.showInformationAlert(R.string.bmovil_alert_exitoefi_sms);
					}else if(isEmail){
					resultadosViewController.showInformationAlert(R.string.bmovil_alert_exitoefi_email);	
				}
			}else if (operationId == Server.CAMBIA_NOMINA_CONTRATO){
				CambiaNominaContrato cont = new CambiaNominaContrato();
				try {
					cont.process(new ParserJSON(response.getResponsePlain()));
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParsingException e) {
					e.printStackTrace();
				}
				if(cont.getErrorCode() == null ) {
					//here

					CambiaNominaContrato contrato = CambiaNominaContrato.getInstance();
					String jsonCaracteresEspeciales = response.getResponsePlain();
					jsonCaracteresEspeciales = jsonCaracteresEspeciales.replace("\\r", "");
					jsonCaracteresEspeciales = jsonCaracteresEspeciales.replace("\\n", "");
					jsonCaracteresEspeciales = jsonCaracteresEspeciales.replace("\\", "");
					jsonCaracteresEspeciales = jsonCaracteresEspeciales.replace("\"{", "{");
					jsonCaracteresEspeciales = jsonCaracteresEspeciales.replace("}\"", "}");
					contrato.setTxtHTML1(jsonCaracteresEspeciales);
					if (contrato.getTxtHTML1() != null) {
						((BmovilViewsController) resultadosViewController.getParentViewsController()).showContratoCambiaNomina();
					} else {
						resultadosViewController.showInformationAlert("Servicio no disponible", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								dialogInterface.dismiss();
							}
						});
					}
				}else{
					resultadosViewController.showInformationAlert(cont.getDescription(), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
						}
					});
				}
			}
		}
			//Termina One CLick

	public void contratoCambiaNomina()
	{
		ParametersTO parametersTO = new ParametersTO();
		final Map<String, String> headers = new HashMap<String, String>();
		headers.put(HTTP.CONTENT_TYPE, "application/json");
		parametersTO.setHeaders(headers);
		parametersTO.setParameters(new Hashtable<String, String>());
		parametersTO.setMethodType(MethodType.POST);
        final JsonObject paramTable = new JsonObject();
		int operacion = Server.CAMBIA_NOMINA_CONTRATO;
        final JsonObject portabilityRequest = new JsonObject();
        portabilityRequest.addProperty("referenceNumber", PortabilidadData.getInstance().getReferenceNumber());
		String codigoBanco="";
		for(int i=0;i<GetListBanks.getInstance().getListBanksCatalogs().size();i++){
			if(String.valueOf(PortabilidadData.getInstance().getPositionBank()).equals(GetListBanks.getInstance().getListBanksCatalogs().get(i).getIdBanco())){
				codigoBanco=GetListBanks.getInstance().getListBanksCatalogs().get(i).getClaveSPEI();
			}
		}
        portabilityRequest.addProperty("bankNumber", codigoBanco);
        portabilityRequest.addProperty("createdReferenceNumber", PortabilidadData.getInstance().getCreatedReferenceNumber());
		/*paramTable.put("companyNumber",PortabilidadData.getInstance().getCompanyNumber());*/
        portabilityRequest.addProperty("companyNumber","4203710262");
        paramTable.add("getPortabilityRequest", portabilityRequest);
		paramTable.addProperty("ium", Session.getInstance(resultadosViewController).getIum());
        paramTable.addProperty("type", "A");
		String json = "";
		final Gson gson = new Gson();
		json = gson.toJson(paramTable);
		parametersTO.setBodyRaw(json);
		parametersTO.setBodyType(BodyType.RAW);
		parametersTO.setForceArq(true);
		parametersTO.setAddCadena(false);
		doNetworkOperation(operacion, parametersTO, Boolean.TRUE, new PojoGeneral(), Server.isJsonValueCode.NONE, resultadosViewController);
	}

	@Override
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler hadler, Server.isJsonValueCode isJsonValue, BaseViewController caller) {
		((BmovilViewsController) caller
				.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, hadler, isJsonValue, caller);
	}

	public void doNetworkOperation(final int operationId, final ParametersTO params, final boolean isJson,
                                   final Object handler, final Server.isJsonValueCode isJsonValueCode,
                                   final BaseViewController caller) {
		((BmovilViewsController)resultadosViewController.getParentViewsController()).getBmovilApp()
				.invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode,
						caller, true);
	}
}
