package suitebancomer.aplicaciones.bmovil.classes.model;


import java.io.Serializable;

/**
 * Created by root on 07/07/16.
 */
public class ListBanksCatalog implements Serializable {
    private String idBanco;
    private String claveSPEI;
    private String tipo;
    private String nombreBanco;
    private int status;

    private  static ListBanksCatalog ourInstance= new ListBanksCatalog();

    public static ListBanksCatalog getlistBanksCatalog(){
        return ourInstance;
    }

    public ListBanksCatalog(){
    }

    public ListBanksCatalog(String idBanco, String claveSPEI, String tipo, String nombreBanco, int status) {
        this.idBanco = idBanco;
        this.claveSPEI = claveSPEI;
        this.tipo = tipo;
        this.nombreBanco = nombreBanco;
        this.status = status;
    }

    public String getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(String idBanco) {
        this.idBanco = idBanco;
    }

    public String getClaveSPEI() {
        return claveSPEI;
    }

    public void setClaveSPEI(String claveSPEI) {
        this.claveSPEI = claveSPEI;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
