package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.common.ToolsCommons;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjeta;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountPlaceholderText;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import tracking.TrackingHelper;



public class AltaRetiroSinTarjetaViewController extends BaseViewController{

	private AltaRetiroSinTarjetaDelegate delegate;
	public BmovilViewsController parentManager;
	private RetiroSinTarjeta datosBeneficiario;
	private CuentaOrigenViewController componenteCtaOrigen;
	private HashMap<String, Compania> listaCompanias;
	private Session session;

	private ImageView imageTitulo;
	private TextView lblTitulo;

	private LinearLayout vistaCompCuentaRetiro;
	private AmountPlaceholderText campoOtroImporte;
	private Button btnAgregarConcepto;
	public EditText textConcepto;
	private Button btnContinuar;

	private LinearLayout layoutContenedorDatosDineroMovil;
	private ImageView imagenCompania;
	private TextView valorNombre;
	private TextView valorNumeroCelular;
	private ImageView logoCompania;

	private String importe;

	private final String EMPTY = "";

	private ListaDatosViewController listaDatos;

	private LinearLayout datosBeneficiarioLayout;

	public RetiroSinTarjeta getDatosBeneficiario() {
		return datosBeneficiario;
	}

	public CuentaOrigenViewController getComponenteCtaOrigen() {
		return componenteCtaOrigen;
	}

	public void setComponenteCtaOrigen(
			CuentaOrigenViewController componenteCtaOrigen) {
		this.componenteCtaOrigen = componenteCtaOrigen;
	}

	/**
	 * Default constructor for this activity
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER, R.layout.activity_retiro_sin_tarjeta);
		//marqueeEnabled = true;
		//setTitle(R.string.transferir_alta_retiro_sintar_title, R.drawable.bmovil_dinero_movil_icono);
		//AMZ


		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("retiro sin tarjeta", parentManager.estados);

		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		delegate = (AltaRetiroSinTarjetaDelegate) parentViewsController.getBaseDelegateForKey(AltaRetiroSinTarjetaDelegate.ALTA_RETIRO_SINTAR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new AltaRetiroSinTarjetaDelegate();
			parentViewsController.addDelegateToHashMap(AltaRetiroSinTarjetaDelegate.ALTA_RETIRO_SINTAR_DELEGATE_ID, delegate);
		}
		delegate.setAltaRetiroSinTarjetaViewController(this);

		datosBeneficiario = new RetiroSinTarjeta();


		delegate.setViewController(this);
		init();
	}

	public void init() {
		session = Session.getInstance(SuiteApp.appContext);

		findViews();

		if (delegate.getTipoRetiro().equals(AltaRetiroSinTarjetaDelegate.TipoRetiro.RETIRO_SIN_TARJETA)) {
			imageTitulo.setImageDrawable(getResources().getDrawable(R.drawable.ic_parami));
			lblTitulo.setText(R.string.bmovil_unificacion_label_parami);
			layoutContenedorDatosDineroMovil.setVisibility(View.GONE);
		} else {
			imageTitulo.setImageDrawable(getResources().getDrawable(R.drawable.ic_paraalguienmas));
			lblTitulo.setText(R.string.bmovil_unificacion_label_paraalguienmas);
			lblTitulo.setTextColor(Color.parseColor("#82c645"));
			cargarLayoutContenedorDatosDineroMovil();
			if((delegate.getTransferencia().getImporte()== null || delegate.getTransferencia().getImporte().equalsIgnoreCase("000") || delegate.getTransferencia().getImporte().equalsIgnoreCase(""))){
				campoOtroImporte.setText("");
			}else{
				campoOtroImporte.setAmount(delegate.getTransferencia().getImporte());
			}
			textConcepto.setText(delegate.getTransferencia().getConcepto());
		}
		btnContinuar.setVisibility(textConcepto.getVisibility() == View.VISIBLE || campoOtroImporte.getText().length() > 0 ? View.VISIBLE : View.GONE);
		textConcepto.setVisibility((null == delegate.getTransferencia().getConcepto() || delegate.getTransferencia().getConcepto().equalsIgnoreCase(""))  ? View.GONE : View.VISIBLE);
		btnAgregarConcepto.setVisibility((null == delegate.getTransferencia().getConcepto() || delegate.getTransferencia().getConcepto().equalsIgnoreCase("")) ? View.VISIBLE : View.GONE);
		scaleForCurrentScreen();
		muestraComponenteCuentaOrigen();

		setDataBeneficiario();

		campoOtroImporte.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				onUserInteraction();
				importe = campoOtroImporte.getAmount();
				btnContinuar.setVisibility(textConcepto.getVisibility() == View.VISIBLE || campoOtroImporte.getText().length() > 0 ? View.VISIBLE : View.GONE);
			}
		});
	}

	private void cargarLayoutContenedorDatosDineroMovil() {
		layoutContenedorDatosDineroMovil.setVisibility(View.VISIBLE);
		valorNombre.setText(delegate.getTransferencia().getBeneficiario());
		valorNumeroCelular.setText(ToolsCommons.formatPhoneNumber(delegate.getTransferencia().getCelularbeneficiario()));
		if (delegate.getTransferencia().getNombreCompania().equals("TELCEL")) {
			imagenCompania.setImageDrawable(getResources().getDrawable(R.drawable.ic_telcel));
			logoCompania.setImageDrawable(getResources().getDrawable(R.drawable.logo_telcel));
		} else if (delegate.getTransferencia().getNombreCompania().equals("MOVISTAR")) {
			imagenCompania.setImageDrawable(getResources().getDrawable(R.drawable.ic_movistar));
			logoCompania.setImageDrawable(getResources().getDrawable(R.drawable.logo_movistar));
		} else if (delegate.getTransferencia().getNombreCompania().equals("IUSACELL")) {
			imagenCompania.setImageDrawable(getResources().getDrawable(R.drawable.ic_iusacell));
			logoCompania.setImageDrawable(getResources().getDrawable(R.drawable.logo_iusacell));
		} else if (delegate.getTransferencia().getNombreCompania().equals("UNEFON")) {
			imagenCompania.setImageDrawable(getResources().getDrawable(R.drawable.ic_unefon));
			logoCompania.setImageDrawable(getResources().getDrawable(R.drawable.logo_unefon));
		} else if (delegate.getTransferencia().getNombreCompania().equals("NEXTEL")) {
			imagenCompania.setImageDrawable(getResources().getDrawable(R.drawable.ic_nextel));
			logoCompania.setImageDrawable(getResources().getDrawable(R.drawable.logo_nextel));
		}
	}

	public void muestraComponenteCuentaOrigen() {
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		componenteCtaOrigen = new CuentaOrigenViewController(this, params, parentViewsController, this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setVisibility(View.GONE);
		componenteCtaOrigen.setDelegate(delegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		if (delegate.getCuentaOrigenSeleccionada() != null) {
			componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getCuentaOrigenSeleccionada()));
		} else {
			componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
		}
		componenteCtaOrigen.init();
		componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.transferir);
		vistaCompCuentaRetiro.addView(componenteCtaOrigen);
		cargaCuentaSeleccionada();
	}

	public void cargaCuentaSeleccionada(){
//		textCuentaDestino.setText(Tools.enmascaraCuentaDestino(delegate.getInnerTransaction().getCuentaDestino().getNumber()));
	}

	public void reactivarCtaOrigen(Account cuenta){
		delegate.getTransferencia().setCuentaOrigen(cuenta);
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled(componenteCtaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().size() > 1);
	}

	public void actualizaCuentaOrigen(Account cuenta){

		delegate.setCuentaOrigenSeleccionada(cuenta);
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
		datosBeneficiario.setCuentaOrigen(delegate.getCuentaOrigenSeleccionada());
	}

	private void findViews() {
		vistaCompCuentaRetiro = (LinearLayout) findViewById(R.id.transfer_mis_cuentas_layout);
		campoOtroImporte = (AmountPlaceholderText) findViewById(R.id.textOtroImporte_retirosintar);
		btnAgregarConcepto = (Button) findViewById(R.id.btn_agregar_concepto);
		textConcepto = (EditText) findViewById(R.id.textConcepto_retirosintar);
		btnContinuar = (Button) findViewById(R.id.btnContinuar_retirosintar);

		layoutContenedorDatosDineroMovil = (LinearLayout) findViewById(R.id.layout_contenedor_datos_dinero_movil);
		imagenCompania = (ImageView) findViewById(R.id.imagen_compania);
		valorNombre = (TextView) findViewById(R.id.valor_nombre);
		valorNumeroCelular = (TextView) findViewById(R.id.valor_numero_celular);
		logoCompania = (ImageView) findViewById(R.id.logo_compania);

		imageTitulo = (ImageView) findViewById(R.id.imagen_titulo);
		lblTitulo = (TextView) findViewById(R.id.texto_titulo);

		datosBeneficiarioLayout = (LinearLayout)findViewById(R.id.transfer_mis_cuentas_layout);
	}

	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scale(findViewById(R.id.rootLayout));
		guiTools.scale(findViewById(R.id.transfer_mis_cuentas_layout));
	}

	public void setDataBeneficiario() {
		session = Session.getInstance(SuiteApp.appContext);

		datosBeneficiario.setBeneficiario(session.getNombreCliente());
		datosBeneficiario.setCelularBeneficiario(session.getUsername());
		session.getCatalogoDineroMovil();
		datosBeneficiario.setCuentaOrigen(delegate.getCuentaOrigenSeleccionada());
		listaCompanias = delegate.cargarListaCompanias();
		datosBeneficiario.setCompania(listaCompanias.get(session.getCompaniaUsuario()));

		//muestraTDCData(getListaBeneficiario());
	}

	public void muestraListaCuentas() {
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	public void boton100(View view) {
		view.requestFocus();
		realizarOperacion("100");
	}

	public void boton200(View view) {
		realizarOperacion("200");
	}

	public void boton300(View view) {
		realizarOperacion("300");
	}

	public void boton500(View view) {
		realizarOperacion("500");
	}

	public void boton1000(View view) {
		realizarOperacion("1000");
	}

	public void boton1500(View view) {
		realizarOperacion("1500");
	}

	public void buttonAgregarConcepto(View view) {
		btnAgregarConcepto.setVisibility(View.GONE);
		textConcepto.setVisibility(View.VISIBLE);
		btnContinuar.setVisibility(View.VISIBLE);
	}

	public void botonContinuarRetiroClick(View view) {
		importe = campoOtroImporte.getAmount();
		if (delegate.getTipoRetiro().equals(AltaRetiroSinTarjetaDelegate.TipoRetiro.RETIRO_SIN_TARJETA)) {
			realizarOperacionRetiroSinTarjeta(importe);
		} else {
			realizarOperacionDineroMovil(importe);
		}

//        realizarOperacion(campoOtroImporte.getAmount());
	}

	private void realizarOperacion(String imp) {

		campoOtroImporte.setAmount(imp+"00");
		importe = imp;
		btnContinuar.setVisibility(View.VISIBLE);

	}

	private void realizarOperacionRetiroSinTarjeta(String imp) {
		if (!isHabilitado())
			return;
		setHabilitado(false);

		datosBeneficiario.setCuentaOrigen(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		datosBeneficiario.setConcepto(Tools.removeSpecialCharacters(textConcepto.getText().toString()));
		delegate.getTransferencia().setConcepto(textConcepto.getText().toString());
		Compania compania = listaCompanias.get(session.getCompaniaUsuario());
		datosBeneficiario.setCompania(compania);

		delegate.setModeloRetiroSinTarjeta(datosBeneficiario);

		delegate.validaDatos(datosBeneficiario.getCelularBeneficiario(), datosBeneficiario.getCelularBeneficiario(), importe,
				Tools.removeSpecialCharacters(datosBeneficiario.getBeneficiario()));
	}

	private void realizarOperacionDineroMovil(String imp) {
		if(!isHabilitado())
			return;
		setHabilitado(false);
		if(parentViewsController.isActivityChanging())
			return;
		delegate.getTransferencia().setConcepto(textConcepto.getText().toString());
		TransferenciaDineroMovil model = delegate.getTransferencia();

		model.setCuentaOrigen(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		model.setConcepto(Tools.removeSpecialCharacters(textConcepto.getText().toString()));

		HashMap<String, String> mapa = new HashMap<String, String>();
//        Compania compania = (Compania) seleccionHorizontal.getSelectedItem();
		//mapa.put(delegate.getTransferencia().getNombreCompania(), delegate.getTransferencia().getNombreCompania());// TODO Quitar hardcodes
		model.setCompania(delegate.getTransferencia().getCompania());
		model.setNombreCompania(delegate.getTransferencia().getNombreCompania());

		if(!delegate.getTipoRetiro().equals(AltaRetiroSinTarjetaDelegate.TipoRetiro.DINERO_MOVIL_FRECUENTE)){
			model.setTipoOperacion("TP09");
			model.setFrecuenteMulticanal("");
			model.setAliasFrecuente("");
		}

//            delegate.validaDatos(campoNumeroTelefono.getText().toString(),
//                    campoConfirmarNumero.getText().toString(),
//                    (View.VISIBLE == campoOtroImporte.getVisibility()) ? campoOtroImporte.getAmount() : null,
//                    Tools.removeSpecialCharacters(campoBeneficiario.getText().toString()));
		delegate.validaDatos(model.getCelularbeneficiario(),
				model.getCelularbeneficiario(),
				importe,
				Tools.removeSpecialCharacters(model.getBeneficiario()));
	}




	@SuppressLint("InflateParams")
	public void muestraTDCData(ArrayList<Object> tabla){

		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(datosBeneficiarioLayout);

		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		listaDatos = new ListaDatosViewController(this, params, parentViewsController);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(tabla);
		listaDatos.setNumeroFilas(tabla.size());
		listaDatos.showLista();
		datosBeneficiarioLayout.addView(listaDatos);
	}

	public ArrayList<Object> getListaBeneficiario(){
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_retiro_sintarjeta_numcelular));
		fila.add(datosBeneficiario.getCelularBeneficiario());
		tabla.add(fila);
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_retiro_sintarjeta_compcelular));
		fila.add(datosBeneficiario.getCompania().getNombre());
		tabla.add(fila);
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_retiro_sintarjeta_beneficiario));
		fila.add(datosBeneficiario.getBeneficiario());
		tabla.add(fila);
		return tabla;
	}

}