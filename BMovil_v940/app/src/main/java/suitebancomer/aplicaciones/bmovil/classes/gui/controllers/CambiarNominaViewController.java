package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import java.util.ArrayList;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.CambiarNominaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TiempoAireDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.CambiarNominaData;
import suitebancomer.aplicaciones.bmovil.classes.model.GetListBanks;
import suitebancomer.aplicaciones.bmovil.classes.model.NominaData;
import suitebancomer.aplicaciones.bmovil.classes.model.PortabilidadData;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;

public class CambiarNominaViewController extends BaseViewController implements OnClickListener, DialogInterface.OnDismissListener {


	public Button txtTipo;
	public Button txtBanco;
	public EditText txtCuentaOrigen;
	public Spinner spiCuentaDestino;
	public TextView txtTuFechaNacimiento;
	public ImageButton checkbox;
	private boolean fechaAceptado=false;
	private String fechaNacimiento="";
	private boolean skipParentValidation;
	LinearLayout vista;
	public CuentaOrigenViewController componenteCtaOrigen;
	ImageButton btnContinuar;
	private CambiarNominaDelegate delegate;
	ListaSeleccionViewController listaSeleccion;
	Dialog combo;
	boolean selectedTipoCuenta;
	boolean selectedBanco = false;
	protected static final int SHOW_LISTA_BANCOS = 159;
	public BmovilViewsController parentManager;
	static ArrayList<Account> listaCuetasMostrar;

	private static int positionCuenta = 0;

	//Variable control  lista Catalogo Bancos
	public int posionBancos=0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_cambia_nomina);
		setTitle(R.string.menu_comprar_contratar_title, R.drawable.bmovil_comprar_icono);

		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(CambiarNominaDelegate.CAMBIARNOMINA_DELEGATE_ID));
		delegate = (CambiarNominaDelegate) getDelegate();
		if (delegate == null) {
			delegate = new CambiarNominaDelegate();
			parentViewsController.addDelegateToHashMap(CambiarNominaDelegate.CAMBIARNOMINA_DELEGATE_ID, delegate);
			setDelegate(delegate);
		}
		delegate.setCambiarNominaViewController(this);
		//delegate.consultarFechaNacimiento();
		init();
		configurarPantalla();
		//muestraIndicadorActividad("Cargando fecha","espere");
		delegate.getCustomer();
	}

	private void configurarPantalla(){
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(findViewById(R.id.LabelTipo), true);
		gTools.scale(findViewById(R.id.txtTipo));
		gTools.scale(findViewById(R.id.lblBanco), true);
		gTools.scale(findViewById(R.id.txtBanco));
		gTools.scale(findViewById(R.id.lblCuentaOrigen), true);
		gTools.scale(findViewById(R.id.txtCuentaOrigen));
		gTools.scale(findViewById(R.id.lblCuentaDestino), true);
		gTools.scale(findViewById(R.id.spiCuentaDestino));
		gTools.scale(findViewById(R.id.txtAyuda));
		gTools.scale(findViewById(R.id.fechaNacimiento), true);
		gTools.scale(findViewById(R.id.txtTuFechaNacimiento), true);
		gTools.scale(findViewById(R.id.checkbox));
		gTools.scale(findViewById(R.id.cambia_nomina_contenedor_principal));
		//gTools.scale(findViewById(R.id.cambia_nomina_view_controller_layout));
		gTools.scale(findViewById(R.id.transfer_interbancario_numeroTarjeta));
		gTools.scale(findViewById(R.id.fechaL));
		gTools.scale(findViewById(R.id.fechaN));
	}



	public void init(){
		//frecuntes correo electronico
		CambiarNominaData cambiarNominaData=new CambiarNominaData();
		delegate.setCambiarNomina(cambiarNominaData);
		vista = (LinearLayout) findViewById(R.id.transfer_interbancario_view_controller_layout);
		//vistaCtaOrigen = (LinearLayout)findViewById(R.id.transfer_interbancario_cuenta_origen_view);
		btnContinuar = (ImageButton) findViewById(R.id.transfer_interbancario_boton_continuar);
		btnContinuar.setOnClickListener(this);
		txtTipo = (Button) findViewById(R.id.txtTipo);
		txtBanco = (Button) findViewById(R.id.txtBanco);
		//txtBanco.setEnabled(false);
		txtCuentaOrigen = (EditText) findViewById(R.id.txtCuentaOrigen);
		spiCuentaDestino = (Spinner) findViewById(R.id.spiCuentaDestino);
		txtTuFechaNacimiento = (TextView) findViewById(R.id.txtTuFechaNacimiento);
		checkbox = (ImageButton) findViewById(R.id.checkbox);

		txtTipo.setOnClickListener(this);
		txtBanco.setOnClickListener(this);
		checkbox.setOnClickListener(this);

		cargarCuentas();

		Session session = Session.getInstance(SuiteApp.appContext);
		String fechaActual = Tools.dateForReference(session.getServerDate());

		spiCuentaDestino.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
				positionCuenta = position;
			}

			@Override
			public void onNothingSelected(final AdapterView<?> parent) {/*Empty method*/ }
		});

		txtCuentaOrigen.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(final View v, final int keyCode, final KeyEvent event) {
				if (event.getAction() == event.ACTION_DOWN && keyCode == event.KEYCODE_ENTER) {
					final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
					inputMethodManager.hideSoftInputFromWindow(txtCuentaOrigen.getWindowToken(), 0);
					inputMethodManager.hideSoftInputFromWindow(spiCuentaDestino.getWindowToken(), 0);
				}
				return false;
			}
		});

    }

	@SuppressWarnings("deprecation")
	public void cargarCuentas(){
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		listaCuetasMostrar = delegate.cargaCuentasOrigen();
		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = 8;
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
//		params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
		ArrayList<String> cuentas = new ArrayList<String>();
		for(Account acc : listaCuetasAMostrar) {
			cuentas.add("*"+acc.getNumber().substring(acc.getNumber().length() - 5, acc.getNumber().length()));
		}
		int posSpinner = 0;
		for (int i=0; i<cuentas.size(); i++)
		{
			if (listaCuetasAMostrar.get(i).equals(delegate.getCuentaDestino())){
				posSpinner = i;
				break;
			}
		}
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item,cuentas);
		spiCuentaDestino.setAdapter(adapter);
		spiCuentaDestino.setSelection(posSpinner);
	}

	@SuppressWarnings("deprecation")
	public void cargarComboTipoCuentas(){
		ArrayList<Object> listaTiposCuenta = delegate.getListaTipoCuentaDestino();
		combo = new Dialog(this){
			@Override
			public boolean onTouchEvent(MotionEvent event) {
				//Log.d("Dialog", "onTouchEvent");
				return super.onTouchEvent(event);
			}
		};
		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.rightMargin =  getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);

		listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
		listaSeleccion.setDelegate(delegate);
		listaSeleccion.setNumeroColumnas(1);
		listaSeleccion.setLista(listaTiposCuenta);
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setSeleccionable(false);
		listaSeleccion.setAlturaFija(false);
		listaSeleccion.setNumeroFilas(7);
		listaSeleccion.setExisteFiltro(false);
		listaSeleccion.setSingleLine(true);
		listaSeleccion.cargarTabla();
		combo.setTitle(getString(R.string.combo_selectAccountType));
		combo.addContentView(listaSeleccion, params);
		combo.show();
	}

	@SuppressWarnings("deprecation")
	public void cargarComboBancos(){
		ArrayList<Object> listaTiposCuenta = delegate.getListadoBancos();
		combo = new Dialog(this);
		combo.setOnDismissListener(this);

		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.rightMargin =  getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
	
		listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
		listaSeleccion.setDelegate(delegate);
		listaSeleccion.setNumeroColumnas(1);
		listaSeleccion.setLista(listaTiposCuenta);
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setSeleccionable(false);
		listaSeleccion.setAlturaFija(false);
		listaSeleccion.setNumeroFilas(7);
		listaSeleccion.setExisteFiltro(false);
		listaSeleccion.setSingleLine(true);
		listaSeleccion.cargarTabla();
		combo.setTitle(getString(R.string.combo_selectBank));
		combo.addContentView(listaSeleccion, params);
		combo.show();
	}
	
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

	public String getFechaNacimeinto() {
		return fechaNacimiento;
	}
	 
	
	@Override
	public void onClick(View v) {
		if (v == btnContinuar && !parentViewsController.isActivityChanging()) {

			delegate.validaDatos();

		}else if (v == txtTipo) {
			//Log.d("Interbancariosviewcontroller", "Presionaste tipo de cuenta");
			
				selectedTipoCuenta = true;
			    selectedBanco=false;
				cargarComboTipoCuentas();
		
			}else if (v == txtBanco) {
			//Log.d("Interbancariosviewcontroller", "Presionaste banco destino");
			
				selectedTipoCuenta = false;
				selectedBanco=true;

				//txtBanco.setEnabled(false);
			    //cargarComboBancos();
			
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showListaBancos(SHOW_LISTA_BANCOS);



			  skipParentValidation = true;
		}else if(v == checkbox){
			if(fechaAceptado){
				fechaAceptado=false;
				checkbox.setImageResource(R.drawable.a_checkapagado);
			}else{
				fechaAceptado=true;
				checkbox.setImageResource(R.drawable.a_checkencendido);
			}

		}
    }


	public boolean isFechaAceptado() {
		return fechaAceptado;
	}

	public void setFechaAceptado(boolean fechaAceptado) {
		this.fechaAceptado = fechaAceptado;
	}

	public String getTipoCuenta(Resources res, String type)
	{
		String tipoCuenta = "";
		if (Constants.CREDIT_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.cardType_credit);
        } else if (Constants.CLABE_TYPE_ACCOUNT.equals(type)) {
        	tipoCuenta = res.getString(R.string.cardType_clabe);
        } else if (Constants.DEBIT_TYPE.equals(type)) {
			tipoCuenta = res.getString(R.string.cardType_debit);
		}//SPEI
		//Termina SPEI

		String tipo = type;

		if (tipo.equals(Constants.DEBIT_TYPE)) {
			InputFilter[] filter = {new InputFilter.LengthFilter(Constants.CARD_NUMBER_LENGTH)};
			txtCuentaOrigen.setFilters(filter);
			txtCuentaOrigen.setHint("16 dígitos");
		} else if (tipo.equals(Constants.CREDIT_TYPE)) {
			InputFilter[] filter = {new InputFilter.LengthFilter(Constants.CARD_NUMBER_LENGTH)};
			txtCuentaOrigen.setFilters(filter);
		}else if(tipo.equals(Constants.CLABE_TYPE_ACCOUNT)){
			InputFilter[] filter = {new InputFilter.LengthFilter(Constants.CUENTA_CLABE_LENGTH)};
			txtCuentaOrigen.setFilters(filter);
			txtCuentaOrigen.setHint("18 dígitos");
		}

        return tipoCuenta;
	}

	@Override
	protected void onResume() {
		super.onResume();
		getParentViewsController().setCurrentActivityApp(this);

	}


	@Override
	public void onDismiss(DialogInterface dialog) {
		//Log.d("Interbancarios", "entro al dismiss del dialog");

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {	
		//pidiendoContactos = false;
		
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == SHOW_LISTA_BANCOS  && resultCode == RESULT_OK){
			Bundle extras = data.getExtras();
			String institucionBancaria = extras.getString("institucionBancaria");
			String nombreBanco = extras.getString("nombreBanco");
			delegate.getCambiarNomina().setInstitucionBancaria(institucionBancaria);
			delegate.getCambiarNomina().setBancoActual(nombreBanco);
			txtBanco.setText(nombreBanco);
			//Log.d(getLocalClassName(), "Banco seleccionado: "+nombreBanco);
	    }
		//bancoDestino.setEnabled(true);
		//delegate.numeroTarjeta();
	}

	public void actualizarSeleccion(Object select){
		if (select instanceof Account) {
			delegate.getCambiarNomina().setCuentaDestino((Account) select);
		}else {
			if (selectedTipoCuenta) {
					delegate.getCambiarNomina().setTipoCuentaOrigen((String)select);
				 	txtTipo.setText(getTipoCuenta(getResources(), (String) select));
					//txtBanco.setEnabled(true);
					//txtBanco.setText("");
					txtCuentaOrigen.setText("");

				delegate.numeroTarjeta();
				combo.dismiss();
				}
			else if(selectedBanco){
				String banconombre="";

					//posionBancos=Integer.parseInt((String)select)-2;
					//PortabilidadData.getInstance().setPositionBank(posionBancos);
					for(int i=0;i<GetListBanks.getInstance().getListBanksCatalogs().size();i++){
						if(GetListBanks.getInstance().getListBanksCatalogs().get(i).getIdBanco().toString().equals(select.toString())){
							banconombre=GetListBanks.getInstance().getListBanksCatalogs().get(i).getNombreBanco();
							posionBancos = Integer.parseInt(GetListBanks.getInstance().getListBanksCatalogs().get(i).getIdBanco());
						}
					}
					/**GetListBanks.getInstance().getListBanksCatalogs().get()
					banconombre=GetListBanks.getInstance().getListBanksCatalogs().get(posionBancos).getNombreBanco();
					txtBanco.setText(banconombre);**/
					txtBanco.setText(banconombre);

			}

			}

	}

	public void cargaFecha(NominaData nd){
		String fecha = nd.birthDate;

		String year = fecha.substring(0,4);
		String mouth = fecha.substring(5,7);
		String day = fecha.substring(8, 10);
		fechaNacimiento=day+"/"+mouth+"/"+year;
		String valorfinal = "es: "+"<font color=#006ec1>"+day+"/"+mouth+"/"+year+"</font>";
		txtTuFechaNacimiento.setText(Html.fromHtml(valorfinal));
		delegate.banksCatalog();
		//ocultaIndicadorActividad();
	}
	
	public static int getPositionCuenta() {
		return positionCuenta;
	}

	public static ArrayList<Account> getListaCuetasMostrar() {
		return listaCuetasMostrar;
	}

	@Override
	protected void onPause() {
		super.onPause();
		//parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(CambiarNominaDelegate.CAMBIARNOMINA_DELEGATE_ID);
		super.goBack();
	}

	@Override
	public void onUserInteraction() {
		//super.onUserInteraction();
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();

	}
}
