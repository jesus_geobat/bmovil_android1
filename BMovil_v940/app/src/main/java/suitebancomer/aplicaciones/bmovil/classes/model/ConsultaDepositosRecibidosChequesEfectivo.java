package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import org.json.JSONException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class ConsultaDepositosRecibidosChequesEfectivo implements ParsingHandler {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String fecha;
	private String hora;
	private String importe;
	private String numeroSucursal;
	private String nombreSucursal;
	private String estadoSucursal;
	private String poblacionSucursal;
	private String nombreOrdenante;
	
	private String tipoOperacion;
	private String banco;
	
	private String numMovto;
	private String referenciaInterna;
	private String referenciaAmpliada;
	
	public ConsultaDepositosRecibidosChequesEfectivo(){
		fecha="";
		hora="";
		importe="";
		numeroSucursal="";
		nombreSucursal="";
		estadoSucursal="";
		poblacionSucursal="";
		nombreOrdenante="";
		tipoOperacion="";
		banco="";
		numMovto="";
		referenciaInterna="";
		referenciaAmpliada="";
	}

	public String getNombreOrdenante() {
		return nombreOrdenante;
	}

	public void setNombreOrdenante(String nombreOrdenante) {
		this.nombreOrdenante = nombreOrdenante;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getNumeroSucursal() {
		return numeroSucursal;
	}

	public void setNumeroSucursal(String numeroSucursal) {
		this.numeroSucursal = numeroSucursal;
	}

	public String getNombreSucursal() {
		return nombreSucursal;
	}

	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}

	public String getEstadoSucursal() {
		return estadoSucursal;
	}

	public void setEstadoSucursal(String estadoSucursal) {
		this.estadoSucursal = estadoSucursal;
	}

	public String getPoblacionSucursal() {
		return poblacionSucursal;
	}

	public void setPoblacionSucursal(String poblacionSucursal) {
		this.poblacionSucursal = poblacionSucursal;
	}
	
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getNumMovto() {
		return numMovto;
	}

	public void setNumMovto(String numMovto) {
		this.numMovto = numMovto;
	}

	public String getReferenciaInterna() {
		return referenciaInterna;
	}

	public void setReferenciaInterna(String referenciaInterna) {
		this.referenciaInterna = referenciaInterna;
	}

	public String getReferenciaAmpliada() {
		return referenciaAmpliada;
	}

	public void setReferenciaAmpliada(String referenciaAmpliada) {
		this.referenciaAmpliada = referenciaAmpliada;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		numeroSucursal = parser.parseNextValue("numeroSucursal");
		nombreSucursal = parser.parseNextValue("nombreSucursal");
		estadoSucursal = parser.parseNextValue("estadoSucursal");
		poblacionSucursal = parser.parseNextValue("poblacionSucursal");
		try{
			nombreOrdenante = parser.parseNextValue("nombreOrdenante");
		}catch(Exception jsone){
			if(Server.ALLOW_LOG) jsone.printStackTrace();
		}
	}
}
