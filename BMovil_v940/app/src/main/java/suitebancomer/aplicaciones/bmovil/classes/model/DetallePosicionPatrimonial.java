package suitebancomer.aplicaciones.bmovil.classes.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by OOROZCO on 3/23/16.
 */
public class DetallePosicionPatrimonial  {

    private String contratoBpigo = null;
    private String inversionesBancarias = null;
    private String fondosDeuda = null;
    private String fondosRentaVariable = null;
    private String mdoDineroReportos = null;
    private String mercadoCapitales = null;
    private String obligaciones = null;
    private String estructurados = null;
    private String otros = null;
    private String efectivoDisponible = null;
    private String totalCartera = null;
    private String claveGrupo = null;
    private String instrumento = null;
    private String emisora = null;
    private String serieEmisora = null;
    private String descripcion = null;
    private String titulos = null;
    private String precio = null;
    private String tasaBruta = null;
    private String diasDevengados = null;
    private String interesPremio = null;
    private String fechaLiquidacion = null;
    private String valuacion = null;
    private String costoPromedio = null;

    private DetallePromotorPatrimonial promotor = new DetallePromotorPatrimonial();
    private DetallePromotorpatrimonial2 promotor2 =  new DetallePromotorpatrimonial2();


    public String getContratoBpigo() {
        return contratoBpigo;
    }

    public String getInversionesBancarias() {
        return inversionesBancarias;
    }

    public String getFondosDeuda() {
        return fondosDeuda;
    }

    public String getFondosRentaVariable() {
        return fondosRentaVariable;
    }

    public String getMdoDineroReportos() {
        return mdoDineroReportos;
    }

    public String getMercadoCapitales() {
        return mercadoCapitales;
    }

    public String getObligaciones() {
        return obligaciones;
    }

    public String getEstructurados() {
        return estructurados;
    }

    public String getOtros() {
        return otros;
    }

    public String getEfectivoDisponible() {
        return efectivoDisponible;
    }

    public String getTotalCartera() {
        return totalCartera;
    }

    public String getClaveGrupo() {
        return claveGrupo;
    }

    public String getInstrumento() {
        return instrumento;
    }

    public String getEmisora() {
        return emisora;
    }

    public String getSerieEmisora() {
        return serieEmisora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getTitulos() {
        return titulos;
    }

    public String getPrecio() {
        return precio;
    }

    public String getTasaBruta() {
        return tasaBruta;
    }

    public String getDiasDevengados() {
        return diasDevengados;
    }

    public String getInteresPremio() {
        return interesPremio;
    }

    public String getFechaLiquidacion() {
        return fechaLiquidacion;
    }

    public String getValuacion() {
        return valuacion;
    }

    public String getCostoPromedio() {
        return costoPromedio;
    }

    public DetallePromotorPatrimonial getPromotor() {
        return promotor;
    }

    public DetallePromotorpatrimonial2 getPromotor2(){ return promotor2;}

    public  DetallePosicionPatrimonial (JSONObject Detalle){

        process(Detalle);
    }


    public void process(JSONObject parser) {

        try {


            JSONObject detalleContrato = parser.getJSONObject("detalleposicion");

            inversionesBancarias = detalleContrato.getString("inversionesBancarias").trim();
            fondosDeuda = detalleContrato.getString("fondosDeuda").trim();
            fondosRentaVariable = detalleContrato.getString("fondosRentaVariable").trim();
            mdoDineroReportos = detalleContrato.getString("mdoDineroReportos").trim();
            mercadoCapitales = detalleContrato.getString("mercadoCapitales").trim();
            obligaciones = detalleContrato.getString("obligaciones").trim();
            estructurados = detalleContrato.getString("estructurados").trim();
            otros = detalleContrato.getString("otros").trim();//Comentar diferencia con D520.
            efectivoDisponible = detalleContrato.getString("efectivoDisponible").trim();
            totalCartera = detalleContrato.getString("totalCartera").trim();
            contratoBpigo = detalleContrato.getString("contratoBpigo").trim();

            JSONObject detallePromotor = parser.getJSONArray("promotorAsignado").getJSONObject(0);


            promotor.setNumeroPromotor(detallePromotor.getString("numeroPromotor").trim());
            promotor.setNombrePromotor(detallePromotor.getString("nombrePromotor").trim());
            promotor.setCentrocosto(detallePromotor.getString("centroCosto").trim());
            promotor.setTelefonoPromotor(detallePromotor.getString("telefonoPromotor").trim());
            promotor.setExtensionPromotor(detallePromotor.getString("extensionPromotor").trim());
            promotor.setCodigoPostalPromotor(detallePromotor.getString("codigoPostalPromotor").trim());
            promotor.setEstadoPromotor(detallePromotor.getString("estadoPromotor").trim());
            promotor.setClaveSucursal(detallePromotor.getString("claveSucursal").trim());
            promotor.setNombreSucursal(detallePromotor.getString("nombreSucursal").trim());
            promotor.setDomicilioSucursal(detallePromotor.getString("domicilioSucursal").trim());
            promotor.setPoblacionSucursal(detallePromotor.getString("poblacionSucursal").trim());
            promotor.setCuentaEje(detallePromotor.getString("cuentaEje").trim());
            promotor.setSegmentoCliente(detallePromotor.getString("segmentoCliente").trim());
           // promotor.setEmailPromotor(detallePromotor.getString("emailPromotor").trim());

            JSONObject detallePromotor2 = parser.getJSONArray("promotorAsignado").getJSONObject(1);

            promotor2.setNumeroPromotor(detallePromotor2.getString("numeroPromotor").trim());
            promotor2.setNombrePromotor(detallePromotor2.getString("nombrePromotor").trim());
            promotor2.setCentrocosto(detallePromotor2.getString("centroCosto").trim());
            promotor2.setTelefonoPromotor(detallePromotor2.getString("telefonoPromotor").trim());
            promotor2.setExtensionPromotor(detallePromotor2.getString("extensionPromotor").trim());
            promotor2.setCodigoPostalPromotor(detallePromotor2.getString("codigoPostalPromotor").trim());
            promotor2.setEstadoPromotor(detallePromotor2.getString("estadoPromotor").trim());
            promotor2.setClaveSucursal(detallePromotor2.getString("claveSucursal").trim());
            promotor2.setNombreSucursal(detallePromotor2.getString("nombreSucursal").trim());
            promotor2.setDomicilioSucursal(detallePromotor2.getString("domicilioSucursal").trim());
            promotor2.setPoblacionSucursal(detallePromotor2.getString("poblacionSucursal").trim());
            promotor2.setCuentaEje(detallePromotor2.getString("cuentaEje").trim());
            promotor2.setSegmentoCliente(detallePromotor2.getString("segmentoCliente").trim());
           // promotor2.setEmailPromotor(detallePromotor2.getString("emailPromotor").trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
