package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.view.View;
import android.widget.TextView;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultarDepositosRecibidosViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDepositosBBVABancomerYEfectivoData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDepositosChequesData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDepositosRecbidosTransDeOtrosBancos;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDepositosRecibidosChequesEfectivo;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasDeOtrosBancosData;
import suitebancomer.classes.gui.controllers.BaseViewController;
import tracking.TrackingHelper;

public class DepositosRecibidosDelegate extends DelegateBaseAutenticacion {
	/**
	 * Identificador del delegado.
	 */
	public static final long DEPOSITOS_RECIBIDOS_DELEGATE_ID = -2L;

	/**
	 * El controlador actual de la pantalla.
	 */
	private BaseViewController viewController;

	/**
	 * @return El controlador de la pantalla.
	 */
	public BaseViewController getViewController() {
		return viewController;
	}

	/**
	 * @param viewController
	 *            El controlador de la pantalla a establecer.
	 */
	public void setViewController(BaseViewController viewController) {
		this.viewController = viewController;
	}

	private Account cuentaActual = null;
	
	private ArrayList<Object> movimientosDR;

	private int tipoOperacionActual = -1;

	private int periodoActual = 0;
	
	private DelegateBaseOperacion operationDelegate;
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #
	 * realizaOperacion(suitebancomer.aplicaciones.bmovil.classes.gui.controllers
	 * .ConfirmacionViewController, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */

	@Override
	public void doNetworkOperation(int operationId,
			Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if (viewController != null)
			((BmovilViewsController) viewController.getParentViewsController())
					.getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode,
							caller, true);
	}

	public ArrayList<?> getMovimientosDR() {
		return movimientosDR;
	}

	public void setMovimientosDR(ArrayList<Object> movimientosDR) {
		this.movimientosDR = movimientosDR;
	}

	public int getTipoOperacionActual() {
		return tipoOperacionActual;
	}

	public void setTipoOperacionActual(int tipoOperacionActual) {
		this.tipoOperacionActual = tipoOperacionActual;
	}

	public int getMovimientoSeleccionadoDR() {
		return movimientoSeleccionadoDR;
	}

	public void setMovimientoSeleccionadoDR(int movimientoSeleccionadoDR) {
		this.movimientoSeleccionadoDR = movimientoSeleccionadoDR;
	}

	public int getTipoOperacion() {
		return tipoOperacionActual;
	}

	public DelegateBaseOperacion getOperationDelegate() {
		return operationDelegate;
	}

	public void setOperationDelegate(DelegateBaseOperacion operationDelegate) {
		this.operationDelegate = operationDelegate;
	}

	public void setCuentaActual(Account cuentaActual) {
		this.cuentaActual = cuentaActual;
	}

	public Account getCuentaActual() {
		return cuentaActual;
	}

	public int getPeriodoActual() {
		return periodoActual;
	}

	public void setPeriodoActual(int periodoActual) {
		this.periodoActual = periodoActual;
	}

	public void setTipoOperacion(int tipoOperacion) {
		this.tipoOperacionActual = tipoOperacion;
	}

	public void verMasMovimientos() {

		periodoActual++;
		consultarMovimientosDR();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int,
	 * suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			viewController.ocultaIndicadorActividad();
			
			ArrayList movimientosRecibidos = null;
			
			if (operationId == Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO) {
				movimientosRecibidos = ((ConsultaDepositosBBVABancomerYEfectivoData) response.getResponse()).getMovimientos();
				periodoActual = Integer.parseInt(((ConsultaDepositosBBVABancomerYEfectivoData) response.getResponse()).getPeriodo());
				
				if ((this.movimientosDR == null) || (this.movimientosDR.size() == 0)) {
					this.movimientosDR = movimientosRecibidos;
				} else {
					this.movimientosDR.addAll(movimientosRecibidos);
				}
				actualizarMovimientosDR();
				
			} else if (operationId == Server.CONSULTA_DEPOSITOS_CHEQUES) {
				movimientosRecibidos = ((ConsultaDepositosChequesData) response.getResponse()).getMovimientos();
				periodoActual = Integer.parseInt(((ConsultaDepositosChequesData) response.getResponse()).getPeriodo());
				
				if ((this.movimientosDR == null) || (this.movimientosDR.size() == 0)) {
					this.movimientosDR = movimientosRecibidos;
				} else {
					this.movimientosDR.addAll(movimientosRecibidos);
				}
				actualizarMovimientosDR();
				
			} else if (operationId == Server.CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS) {
				movimientosRecibidos = ((ConsultaTransferenciasDeOtrosBancosData) response.getResponse()).getMovimientos();
				periodoActual = Integer.parseInt(((ConsultaTransferenciasDeOtrosBancosData) response.getResponse()).getPeriodo());
				
				if ((this.movimientosDR == null) || (this.movimientosDR.size() == 0)) {
					this.movimientosDR = movimientosRecibidos;
				} else {
					this.movimientosDR.addAll(movimientosRecibidos);
				}
				actualizarMovimientosDR();
				
			} else if ((operationId == Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE)
					|| (operationId == Server.CONSULTA_DEPOSITOS_CHEQUES_DETALLE)) {
				ConsultaDepositosRecibidosChequesEfectivo detalleMovimiento = (ConsultaDepositosRecibidosChequesEfectivo) response.getResponse();
				
				// Añadir al movimiento seleccionado el detalle del movimiento
				ConsultaDepositosRecibidosChequesEfectivo movimiento = (ConsultaDepositosRecibidosChequesEfectivo) movimientosDR.get(movimientoSeleccionadoDR);
				movimiento.setNumeroSucursal(detalleMovimiento.getNumeroSucursal());
				movimiento.setNombreSucursal(detalleMovimiento.getNombreSucursal());
				movimiento.setEstadoSucursal(detalleMovimiento.getEstadoSucursal());
				movimiento.setPoblacionSucursal(detalleMovimiento.getPoblacionSucursal());
				movimiento.setNombreOrdenante(detalleMovimiento.getNombreOrdenante());
				
				showDetallesMovimientoDR();
			}
			
		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			
			if (response.getMessageCode().equals(Constants.CODIGO_AVISO_ACA022)||
					response.getMessageCode().equals(Constants.CODIGO_AVISO_ACA0022)) {
				//if (periodoActual < 2) {
					// Realizar petición del siguiente período
					//verMasMovimientos();
				//} else {
					viewController.ocultaIndicadorActividad();
					
					if ((this.movimientosDR == null) || (this.movimientosDR.isEmpty())) {
						// Muestro el alert si no había ningún movimiento listado*/
						mostrarMensajeWarning(response);
						viewController.ocultaIndicadorActividad();
					}

					actualizarMovimientosDR();
				//}
				
			} else {
				// No era un error de movimiento: resto el período que sumé al consultar movimientos
				viewController.ocultaIndicadorActividad();
				if (periodoActual > 0) {
					periodoActual--;
				}
				mostrarMensajeWarning(response);				
			}
			
		} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
			
			if ((response.getMessageCode().equals(Constants.CODIGO_ERROR_MBANK0003)) || (response.getMessageCode().equals(Constants.CODIGO_ERROR_MBANK0007))) {
				//if (periodoActual < 2) {
					// Realizar petición del siguiente período
					//verMasMovimientos();
				//} else {
					viewController.ocultaIndicadorActividad();
					
					if ((this.movimientosDR == null) || (this.movimientosDR.isEmpty())) {
						// Muestro el alert si no había ningún movimiento listado*/
						mostrarMensajeError(response);
						viewController.ocultaIndicadorActividad();
					}
					actualizarMovimientosDR();
				//}
				
			} else {
				// No era un error de movimiento: resto el período que sumé al consultar movimientos
				viewController.ocultaIndicadorActividad();
				if (periodoActual > 0) {
					periodoActual--;
				}
				mostrarMensajeError(response);
				//movimientosDR = null;				
			}
			//((ConsultarDepositosRecibidosViewController) viewController).muestraVerMasMovimientos(false);
		}
		
		if (periodoActual >= 2 || response.getStatus() == ServerResponse.OPERATION_ERROR ||
				(response.getStatus() == ServerResponse.OPERATION_WARNING && 
				(response.getMessageCode().equals(Constants.CODIGO_AVISO_ACA022) || 
				response.getMessageCode().equals(Constants.CODIGO_AVISO_ACA0022) ||
				response.getMessageCode().equals(Constants.CODIGO_AVISO_FEA0023)))) {
			((ConsultarDepositosRecibidosViewController) viewController).muestraVerMasMovimientos(false);
		} else {
			((ConsultarDepositosRecibidosViewController) viewController).muestraVerMasMovimientos(true);
		}
		
		((ConsultarDepositosRecibidosViewController)viewController).getComponenteCtaOrigen().getImgDerecha().setEnabled(true);
		((ConsultarDepositosRecibidosViewController)viewController).getComponenteCtaOrigen().getImgIzquierda().setEnabled(true);
		((ConsultarDepositosRecibidosViewController)viewController).getComponenteCtaOrigen().getVistaCtaOrigen().setEnabled((((ConsultarDepositosRecibidosViewController)viewController).getComponenteCtaOrigen().getListaCuetasAMostrar().size() > 1));
	}
	
	private void mostrarMensajeWarning(ServerResponse response) {
		if (viewController != null) {
			((BmovilViewsController) viewController
					.getParentViewsController())
					.getCurrentViewControllerApp()
					.showInformationAlertEspecial(
							viewController.getString(R.string.label_error),
							response.getMessageCode(),
							response.getMessageText(), null);
//			viewController.getString(R.string.bmovil_menu_consultar_alert_texto_sin_movimientos_periodo), null);
			
		} else {
			BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
			BmovilViewsController bmvc = bmApp.getBmovilViewsController();
			BaseViewController current = bmvc.getCurrentViewControllerApp();

			current.showInformationAlertEspecial(
					viewController.getString(R.string.label_error),
					response.getMessageCode(), response.getMessageText(),
//					response.getMessageCode(), viewController.getString(R.string.bmovil_menu_consultar_alert_texto_sin_movimientos_periodo), null);
					null);
		}
	}
	
	private void mostrarMensajeError(ServerResponse response) {
		BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bmvc = bmApp.getBmovilViewsController();
		BaseViewController current = bmvc.getCurrentViewControllerApp();
		
		current.showInformationAlertEspecial(
				viewController.getString(R.string.label_error),
				response.getMessageCode(), response.getMessageText(), null);
//		response.getMessageCode(), viewController.getString(R.string.bmovil_menu_consultar_alert_texto_sin_movimientos_periodo), null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.classes.gui.delegates.BaseDelegate#getDelegateIdentifier()
	 */
	@SuppressWarnings("static-access")
	@Override
	public long getDelegateIdentifier() {
		return this.DEPOSITOS_RECIBIDOS_DELEGATE_ID;
	}

	/*
	 * ##########################################################################
	 * #############################################
	 */

	public ArrayList<Object> getHeaderListaMovimientos() {
		ArrayList<Object> result = new ArrayList<Object>();

		result.add(null);
		result.add(SuiteApp.appContext
				.getString(R.string.bmovil_consultar_depositosrecibidos_movimientos_lista_fecha));
		result.add(SuiteApp.appContext
				.getString(R.string.bmovil_consultar_depositosrecibidos_movimientos_lista_importe));

		return result;
	}

	public ArrayList<Object> getDatosTablaMovimientosDR() {
		ArrayList<Object> datos = new ArrayList<Object>();
		ArrayList<String> registro;

		if (null == movimientosDR || movimientosDR.isEmpty()) {
			registro = new ArrayList<String>();
			registro.add(null);
			registro.add("");
			registro.add("");
			registro.add("");
			registro.add("");
		} else {
			if (tipoOperacionActual == Server.CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS) {
				for (Object consulta : movimientosDR) {
					registro = new ArrayList<String>();
					registro.add(null);
					registro.add(Tools
							.formatShortDate(((ConsultaDepositosRecbidosTransDeOtrosBancos) consulta)
									.getFecha()));
					registro.add(Tools
							.formatAmount(
									((ConsultaDepositosRecbidosTransDeOtrosBancos) consulta)
											.getImporte(), false));
					datos.add(registro);
				}
			} else if (tipoOperacionActual == Server.CONSULTA_DEPOSITOS_CHEQUES
					|| tipoOperacionActual == Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO) {
				for (Object consulta : movimientosDR) {
					registro = new ArrayList<String>();
					registro.add(null);
					registro.add(Tools
							.formatShortDate(((ConsultaDepositosRecibidosChequesEfectivo) consulta)
									.getFecha()));
					registro.add(Tools
							.formatAmount(
									((ConsultaDepositosRecibidosChequesEfectivo) consulta)
											.getImporte(), false));
					datos.add(registro);
				}
			}
		}

		return datos;
	}

	public void consultarMovimientosDR() {

		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);
		
		paramTable.put(ServerConstants.CUENTA, cuentaActual.getNumber());
		paramTable.put(ServerConstants.TIPO_CUENTA, cuentaActual.getType());
		paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
		paramTable.put(ServerConstants.IUM, session.getIum());
		paramTable.put(ServerConstants.PERIODO, String.valueOf(periodoActual));

		switch (tipoOperacionActual) {

		case Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO:
			paramTable.put(ServerConstants.TIPO_OPERACION, "depositoEfectivo"); //RN6
			//JAIG
			((BmovilViewsController) viewController.getParentViewsController())
			.getBmovilApp().invokeNetworkOperation(Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO,
					paramTable,true,new ConsultaDepositosBBVABancomerYEfectivoData(),isJsonValueCode.DEPOSITOS, viewController, true);
		break;
		case Server.CONSULTA_DEPOSITOS_CHEQUES:
			paramTable.put(ServerConstants.TIPO_OPERACION, "depositoCheques"); //RN6
			//JAIG
			((BmovilViewsController) viewController.getParentViewsController())
			.getBmovilApp().invokeNetworkOperation(Server.CONSULTA_DEPOSITOS_CHEQUES,
					paramTable,true,new ConsultaDepositosChequesData(),isJsonValueCode.DEPOSITOS, viewController, true);
		break;

		case Server.CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS:
			paramTable.put(ServerConstants.TIPO_OPERACION, "otrosBancos"); //RN6
			//JAIG
			((BmovilViewsController) viewController.getParentViewsController())
			.getBmovilApp().invokeNetworkOperation(Server.CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS,
					paramTable,true,new ConsultaTransferenciasDeOtrosBancosData(),isJsonValueCode.DEPOSITOS, viewController, true);
			break;

		default:

			break;
		}
		//JAIG RESUELTO Y PROBADO
		/*((BmovilViewsController) viewController.getParentViewsController())
				.getBmovilApp().invokeNetworkOperation(tipoOperacionActual,
						paramTable, viewController, true);
						*/
	}
	
	private void consultarDetalleMovimientoDR(int tipoOperacion) {
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);
		ConsultaDepositosRecibidosChequesEfectivo movimiento = (ConsultaDepositosRecibidosChequesEfectivo) movimientosDR.get(movimientoSeleccionadoDR);
		
		paramTable.put(ServerConstants.CUENTA, cuentaActual.getNumber());
		paramTable.put(ServerConstants.NUM_MOVTO, movimiento.getNumMovto());
		/***RHO***/
		String referenciaInterna = "", referenciaAmpliada = "";
		if(movimiento.getTipoOperacion().equals("Transferencias")){
			referenciaInterna = movimiento.getReferenciaInterna();
			referenciaAmpliada = movimiento.getReferenciaAmpliada();
		}
		paramTable.put(ServerConstants.REFERENCIA_INTERNA, referenciaInterna);
		paramTable.put(ServerConstants.REFERENCIA_AMPLIADA, referenciaAmpliada);
		paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
		paramTable.put(ServerConstants.IUM, session.getIum());
		//JAIG
		((BmovilViewsController) viewController.getParentViewsController())
		.getBmovilApp().invokeNetworkOperation(tipoOperacion,
				paramTable,true, new ConsultaDepositosRecibidosChequesEfectivo(),isJsonValueCode.DEPOSITOS, viewController, true);
	}

	public void actualizarMovimientosDR() {

		ConsultarDepositosRecibidosViewController controller = (ConsultarDepositosRecibidosViewController) viewController;

		ArrayList<Object> movimientos = getDatosTablaMovimientosDR();

		controller.getListaEnvios().setVisibility(View.VISIBLE);
		controller.getListaEnvios().setNumeroFilas(movimientos.size());
		controller.getListaEnvios().setLista(movimientos);
		/******/
		controller.getListaEnvios().setOpcionSeleccionada(-1); /***RHO***/

		if (movimientos.isEmpty())
			controller.getListaEnvios().setTextoAMostrar("");
		else {
			controller.getListaEnvios().setTextoAMostrar(null);
			controller.getListaEnvios().setNumeroColumnas(2);
		}

		controller.getListaEnvios().cargarTabla();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.classes.gui.delegates.BaseDelegate#performAction(java.lang
	 * .Object)
	 */
	@Override
	public void performAction(Object obj) {
		if (obj instanceof Account) {
			Account nuevaCuenta = (Account) obj;
			
			if (!cuentaActual.equals(nuevaCuenta)) {
				// Debo borrar los movimientos que ya hubiera de la cuenta anterior
				movimientosDR = null;
				actualizarMovimientosDR();
			}
			
			cuentaActual = nuevaCuenta;
			periodoActual = 0;
			/*if(tipoOperacionActual != -1)
				consultarMovimientosDR();
			else{*/
			/***/
			((ConsultarDepositosRecibidosViewController) viewController).muestraVerMasMovimientos(false);
			TextView comboTipoOperacion = ((ConsultarDepositosRecibidosViewController) viewController).getComboFiltro();
			comboTipoOperacion.setText(viewController.getString(R.string.bmovil_consultar_depositosrecibidos_movimientos_tipooperacion_seleccion));
			/***/
				((ConsultarDepositosRecibidosViewController)viewController).getComponenteCtaOrigen().getImgDerecha().setEnabled(true);
				((ConsultarDepositosRecibidosViewController)viewController).getComponenteCtaOrigen().getImgIzquierda().setEnabled(true);
				((ConsultarDepositosRecibidosViewController)viewController).getComponenteCtaOrigen().getVistaCtaOrigen().setEnabled((((ConsultarDepositosRecibidosViewController)viewController).getComponenteCtaOrigen().getListaCuetasAMostrar().size() > 1));
			//}
		} else {

			ConsultarDepositosRecibidosViewController controller = (ConsultarDepositosRecibidosViewController) viewController;
			movimientoSeleccionadoDR = controller.getListaEnvios().getOpcionSeleccionada();

			// AMZ inicio
			Map<String, Object> ConsultaRealizadaMap = new HashMap<String, Object>();

			ConsultaRealizadaMap.put("evento_realizada", "event52");
			ConsultaRealizadaMap.put("&&products",
					"consulta;depositosrecibidos");
			ConsultaRealizadaMap.put("eVar12", "operacion realizada");
			TrackingHelper.trackConsultaRealizada(ConsultaRealizadaMap);
			// AMZ fin
			
			if (tipoOperacionActual == Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO) {
				consultarDetalleMovimientoDR(Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE);
			} else if (tipoOperacionActual == Server.CONSULTA_DEPOSITOS_CHEQUES) {
				consultarDetalleMovimientoDR(Server.CONSULTA_DEPOSITOS_CHEQUES_DETALLE);
			} else {
				showDetallesMovimientoDR();
			}
		}
	}

	private int movimientoSeleccionadoDR;

	private void showDetallesMovimientoDR() {
		SuiteApp.getInstance().getBmovilApplication()
				.getBmovilViewsController().showDetalleDepositosRecibidos();
	}

	public ArrayList<Object> getDatosTablaDetalleMovimientoDR() {
		ArrayList<Object> datos = new ArrayList<Object>();

		if (tipoOperacionActual == Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO) {
			ConsultaDepositosRecibidosChequesEfectivo 
			movimiento = (ConsultaDepositosRecibidosChequesEfectivo) movimientosDR.get(movimientoSeleccionadoDR);
			if(movimiento.getTipoOperacion().equals("Efectivo")) //efectivo
				datos = obtenerTablaListaDepositosEfectivo();
			else if(movimiento.getTipoOperacion().equals("Transferencias")) //traspaso de tercero
				datos = obtenerTablaListaDepositosTerceros();
		} else if (tipoOperacionActual == Server.CONSULTA_DEPOSITOS_CHEQUES) {
			datos = obtenerTablaListaDepositosConCheques();
		} else if (tipoOperacionActual == Server.CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS) {
			datos = obtenerTablaListaTransferenciasOtrosBancos();
		}

		return datos;
	}
	
	private ArrayList<Object> obtenerTablaListaDepositosEfectivo() {
		ArrayList<Object> datos = new ArrayList<Object>();
		Object movimiento = movimientosDR.get(movimientoSeleccionadoDR);
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_deporecibido));
		/***RHO***/
		//fila.add(cuentaActual.getNumber());
		fila.add(Tools.enmascaraCuentaDestino(cuentaActual.getNumber()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_importe));
		fila.add(Tools.formatAmount(
				((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
						.getImporte(), false));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_tipoop));
		fila.add("Efectivo");
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_titulo_datosuc));
		fila.add(" ");
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_numero));
		fila.add(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
				.getNumeroSucursal());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_nombre));
		fila.add(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
				.getNombreSucursal());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_estado));
		fila.add(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
				.getEstadoSucursal());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_poblacion));
		fila.add(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
				.getPoblacionSucursal());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_fecha));
		fila.add(Tools
				.formatDate(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
						.getFecha()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_hora));
		fila.add(Tools
				.formatTime(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
						.getHora()));
		datos.add(fila);
		
		return datos;
	}
	
	private ArrayList<Object> obtenerTablaListaDepositosTerceros() {
		ArrayList<Object> datos = new ArrayList<Object>();
		Object movimiento = movimientosDR.get(movimientoSeleccionadoDR);
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_deporecibido));
		/***RHO***/
		//fila.add(cuentaActual.getNumber());
		fila.add(Tools.enmascaraCuentaDestino(cuentaActual.getNumber()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_importe));
		fila.add(Tools.formatAmount(
				((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
						.getImporte(), false));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_tipoop));
		fila.add("Transferencia");
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_titulo_datoenvio));
		fila.add(" ");
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_nombre));
		fila.add(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
				.getNombreOrdenante());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_fecha));
		fila.add(Tools
				.formatDate(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
						.getFecha()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_hora));
		fila.add(Tools
				.formatTime(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
						.getHora()));
		datos.add(fila);
		
		return datos;
	}
	
	private ArrayList<Object> obtenerTablaListaDepositosConCheques() {
		ArrayList<Object> datos = new ArrayList<Object>();
		Object movimiento = movimientosDR.get(movimientoSeleccionadoDR);
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_deporecibido));
		/***RHO***/
		//fila.add(cuentaActual.getNumber());
		fila.add(Tools.enmascaraCuentaDestino(cuentaActual.getNumber()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_importe));
		fila.add(Tools.formatAmount(
				((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
						.getImporte(), false));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_tipoop));
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_movimientos_txttipooperacion_cheques));
		datos.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_banco));
		fila.add(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
				.getBanco());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_titulo_datosuc));
		fila.add(" ");
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_numero));
		fila.add(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
				.getNumeroSucursal());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_nombre));
		fila.add(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
				.getNombreSucursal());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_estado));
		fila.add(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
				.getEstadoSucursal());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_poblacion));
		fila.add(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
				.getPoblacionSucursal());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_fecha));
		fila.add(Tools
				.formatDate(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
						.getFecha()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_hora));
		fila.add(Tools
				.formatTime(((ConsultaDepositosRecibidosChequesEfectivo) movimiento)
						.getHora()));
		datos.add(fila);
		
		return datos;
	}
	
	private ArrayList<Object> obtenerTablaListaTransferenciasOtrosBancos() {
		ArrayList<Object> datos = new ArrayList<Object>();
		Object movimiento = movimientosDR.get(movimientoSeleccionadoDR);
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_deporecibido));
		/***RHO***/
		//fila.add(cuentaActual.getNumber());
		//fila.add(cuentaActual.getNumber());
		fila.add(Tools.enmascaraCuentaDestino(cuentaActual.getNumber()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_importe));
		fila.add(Tools.formatAmount(
				((ConsultaDepositosRecbidosTransDeOtrosBancos) movimiento)
						.getImporte(), false));
		datos.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_tipoop));
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_movimientos_txttipooperacion_transferencias));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_titulo_datoenvio));
		fila.add(" ");
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_motivopago));
		fila.add(((ConsultaDepositosRecbidosTransDeOtrosBancos) movimiento)
				.getMotivoPago());		
		datos.add(fila);
		
		/***CLAVE RASTREO***/
		String clave = ((ConsultaDepositosRecbidosTransDeOtrosBancos) movimiento)
				.getClaveRastreo();
		if (! Tools.isEmptyOrNull(clave)){
			fila = new ArrayList<String>();
			fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_claverastreo));
			fila.add(clave);
			datos.add(fila);
		}
		
		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_referencianum));
		fila.add(((ConsultaDepositosRecbidosTransDeOtrosBancos) movimiento)
				.getReferenciaNumerica());		
		datos.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_cuenta));
		fila.add(Tools.hideAccountNumber(((ConsultaDepositosRecbidosTransDeOtrosBancos) movimiento)
				.getCuentaOrdenante()));		
		datos.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_banco));
		fila.add(((ConsultaDepositosRecbidosTransDeOtrosBancos) movimiento)
				.getBancoOrdenante());		
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_fecha));
		fila.add(Tools
				.formatDate(((ConsultaDepositosRecbidosTransDeOtrosBancos) movimiento)
						.getFecha()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController
				.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_hora));
		fila.add(Tools
				.formatTime(((ConsultaDepositosRecbidosTransDeOtrosBancos) movimiento)
						.getHora()));
		datos.add(fila);
		
		return datos;
	}

	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la
	 * lista es ordenada seg�n sea requerido.
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		
		return obtenerTodasMenosTDC();
	}
	
	private ArrayList<Account> obtenerTodasMenosTDC() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		
		if (profile == Constants.Perfil.avanzado) {
			Account[] accounts = Session.getInstance(SuiteApp.appContext)
					.getAccounts();

			/*
			for (Account acc : accounts) {
				if (acc.isVisible()
						&& !acc.getType().equals(Constants.CREDIT_TYPE)) {
					accountsArray.add(acc);
					break;
				}
			}*/
			

			for (Account acc : accounts) {
				if (/*!acc.isVisible()
						&& */!acc.getType().equals(Constants.CREDIT_TYPE))
					accountsArray.add(acc);
			}
		} else {
			accountsArray.add(Tools.obtenerCuentaEje());
		}
		
		return accountsArray;
	}
	
	public void enviaEmail() {
		
						SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showEnviarCorreo(this);
	
}
}