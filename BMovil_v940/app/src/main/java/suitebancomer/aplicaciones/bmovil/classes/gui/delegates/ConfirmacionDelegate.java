package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtp;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtpBmovil;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class ConfirmacionDelegate extends DelegateBaseOperacion
{
	public final static long CONFIRMACION_DELEGATE_DELEGATE_ID = 0x1ef4f4c61ca109bfL;
	
	private ArrayList<String> datosLista;
	private DelegateBaseOperacion operationDelegate;
	private boolean debePedirContrasena;
	private boolean debePedirNip;
	private Constants.TipoOtpAutenticacion tokenAMostrar;
	private boolean debePedirCVV;
	
	
	private Constants.TipoInstrumento tipoInstrumentoSeguridad;
	//private String textoInstrumentoSeguridad;
	private ConfirmacionViewController confirmacionViewController;
	
	private boolean debePedirTarjeta;
	//AMZ
		public boolean res = false;
	//ehmendezr validacion S2
	public ConfirmacionDelegate(DelegateBaseOperacion delegateBaseOperacion) {
		this.operationDelegate = delegateBaseOperacion;
		debePedirContrasena = operationDelegate.mostrarContrasenia();
		debePedirNip = operationDelegate.mostrarNIP();
		debePedirCVV = operationDelegate.mostrarCVV();
		tokenAMostrar = operationDelegate.tokenAMostrar();
		debePedirTarjeta = mostrarCampoTarjeta();
		String instrumento = Session.getInstance(SuiteApp.appContext).getSecurityInstrument();
		if (instrumento.equals(Constants.IS_TYPE_DP270)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
		} else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
		} else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value) || instrumento.equals(Constants.TYPE_SOFTOKEN.S2.value)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
		} else {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
		}
	}

	public void setConfirmacionViewController(ConfirmacionViewController confirmacionViewController) {
		this.confirmacionViewController = confirmacionViewController;
	}

	public void consultaDatosLista() {
		confirmacionViewController.setListaDatos(operationDelegate.getDatosTablaConfirmacion());
	}
	
	public DelegateBaseOperacion consultaOperationsDelegate() {
		return operationDelegate;
	}
	
	public boolean consultaDebePedirContrasena() {
		return debePedirContrasena;
	}
	
	public boolean consultaDebePedirNIP() {
		return debePedirNip;
	}
	
	public boolean consultaDebePedirCVV() {
		return debePedirCVV;
	}
	
	public Constants.TipoInstrumento consultaTipoInstrumentoSeguridad() {
		return tipoInstrumentoSeguridad;
	}
	
	public Constants.TipoOtpAutenticacion consultaInstrumentoSeguridad() {
		return tokenAMostrar;
	}

	static String contrasena = null;
	static String nip = null;
	static String asm = null;
	static String cvv = null;
	static String tarjeta = null;
	public void enviaPeticionOperacion() {
		contrasena = null;
		nip = null;
		asm = null;
		cvv = null;
		res = false;
		if (debePedirContrasena) {
			contrasena = confirmacionViewController.pideContrasena();
			if (contrasena.equals("")) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			} else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.PASSWORD_LENGTH;
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			}
		}

		tarjeta = null;
		if(debePedirTarjeta){
			tarjeta = confirmacionViewController.pideTarjeta();
			String mensaje = "";
			if(tarjeta.equals("")){
				mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			}else if(tarjeta.length() != 5){
				mensaje =  "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			}
		}

		if (debePedirNip) {
			nip = confirmacionViewController.pideNIP();
			if (nip.equals("")) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			} else if (nip.length() != Constants.NIP_LENGTH) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.NIP_LENGTH;
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			}
		}
		if (tokenAMostrar != Constants.TipoOtpAutenticacion.ninguno) {
			asm = confirmacionViewController.pideASM();
			if (asm.equals("")) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				mensaje += ".";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			} else if (asm.length() != Constants.ASM_LENGTH) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.ASM_LENGTH;
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				mensaje += ".";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			}
		}
		if (debePedirCVV) {
			cvv = confirmacionViewController.pideCVV();
			if (cvv.equals("")) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			} else if (cvv.length() != Constants.CVV_LENGTH) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.CVV_LENGTH;
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			}
		}

		String newToken = null;
		if(tokenAMostrar != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken
				&& (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService())){

			if (SuiteAppCRApi.getSofttokenStatus()) {
				if (ServerCommons.ALLOW_LOG) {
					Log.d("APP", "softToken local");
				}
				newToken = loadOtpFromSofttoken(tokenAMostrar);

				this.finaliceOP(newToken, contrasena, nip, tarjeta, cvv);
			} else if (!SuiteAppCRApi.getSofttokenStatus() && PropertiesManager.getCurrent().getSofttokenService()) {
				if (ServerCommons.ALLOW_LOG) {
					Log.d("APP", "softoken compartido");
				}
				//suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST validacionEstatusST = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST();
				//validacionEstatusST.validaEstatusSofttoken(SuiteApp.appContext);
				GetOtpBmovil otpG = new GetOtpBmovil(new GetOtp() {
					/**
					 *
					 * @param otp
					 */
					@Override
					public void setOtpRegistro(String otp) {

					}

					/**
					 *
					 * @param otp
					 */
					@Override
					public void setOtpCodigo(String otp) {
						if (ServerCommons.ALLOW_LOG) {
							Log.d("APP", "la otp en callback: " + otp);
						}
						finaliceOP(otp, contrasena, nip, tarjeta, cvv);
					}

					/**
					 *
					 * @param otp
					 */
					@Override
					public void setOtpCodigoQR(String otp) {

					}
				}, com.bancomer.base.SuiteApp.appContext);

				otpG.generateOtpCodigo();
			}

		}
		else if(tokenAMostrar != TipoOtpAutenticacion.ninguno ) {
			finaliceOP(asm, contrasena, nip, tarjeta, cvv);
		}
		else if(tokenAMostrar == TipoOtpAutenticacion.ninguno){
			finaliceOP(null, contrasena, nip, tarjeta, cvv);
		}
		res = true;

	}

	private void finaliceOP(String newToken, String contrasena, String nip, String tarjeta, String cvv){
		String asm="";
		if(null != newToken)
			asm = newToken;

		if ((operationDelegate instanceof AltaRetiroSinTarjetaDelegate) || (operationDelegate instanceof ConsultaRetiroSinTarjetaDelegate)) {
			operationDelegate.realizaOperacion(confirmacionViewController, contrasena, nip, asm, tarjeta, cvv);

		}else if(operationDelegate instanceof CambiarNominaDelegate){
			if(confirmacionViewController.getPortaibilidad()){
				//operationDelegate.realizaOperacion();
				operationDelegate.realizaOperacion(confirmacionViewController, contrasena, nip, asm, tarjeta, cvv);
			}else{
				confirmacionViewController.showInformationAlert("Porfavor Confirmar la solicitud de transferencia.");
			}
		}
		else {
			operationDelegate.realizaOperacion(confirmacionViewController, contrasena, nip, asm, tarjeta);
		}

	}
	

	@Override
	public String getEtiquetaCampoNip() {
		return confirmacionViewController.getString(R.string.confirmation_nip);
	}
	
	@Override
	public String getTextoAyudaNIP() {
		return confirmacionViewController.getString(R.string.confirmation_ayudaNip);
	}
	
	@Override
	public String getEtiquetaCampoContrasenia() {
		return confirmacionViewController.getString(R.string.confirmation_contrasena);
	}
	
	@Override
	public String getEtiquetaCampoOCRA() {
		return confirmacionViewController.getString(R.string.confirmation_ocra);
	}
	
	@Override
	public String getEtiquetaCampoDP270() {
		return confirmacionViewController.getString(R.string.confirmation_dp270);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenActivado() {
		return confirmacionViewController.getString(R.string.confirmation_softtokenActivado);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenDesactivado() {
		return confirmacionViewController.getString(R.string.confirmation_softtokenDesactivado);
	}
	
	@Override
	public String getEtiquetaCampoCVV() {
		return confirmacionViewController.getString(R.string.confirmation_CVV);
	}

	public String getTextoAyudaInstrumentoSeguridad(Constants.TipoInstrumento tipoInstrumento) {
		Constants.TipoOtpAutenticacion tokenAMostrar = tokenAMostrar();
		//se resuelve el contexto nulo
		if(getContext()==null && confirmacionViewController!=null){
			setContext(confirmacionViewController);
		}
		return getTextoAyudaInstrumentoSeguridad(tipoInstrumento, (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService()), tokenAMostrar);
	}

	/*
	@Override
	public String getTextoAyudaInstrumentoSeguridad(Constants.TipoInstrumento tipoInstrumento) {
		if (tokenAMostrar == Constants.TipoOtpAutenticacion.ninguno) {
			return "";
		} else if (tokenAMostrar == Constants.TipoOtpAutenticacion.registro) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteApp.getSofttokenStatus()) {
						return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
					} else {
						return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroSofttokenDesactivado);
					}
				case OCRA:
					return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroOCRA);
				case DP270:
					return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroDP270);
				case sinInstrumento:
				default:
					return "";
			}
		} else if (tokenAMostrar == Constants.TipoOtpAutenticacion.codigo) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteApp.getSofttokenStatus()) {
						return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoSofttokenActivado);
					} else {
						return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoSofttokenDesactivado);
					}					
				case OCRA:
					return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoOCRA);
				case DP270:
					return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoDP270);
				case sinInstrumento:
				default:
					return "";
			}
		}
		return "";
	}
	*/

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		Session session = Session.getInstance(SuiteApp.appContext);
		Constants.Perfil perfil = session.getClientProfile();
	
		if(response.getStatus() == ServerResponse.OPERATION_ERROR && !Constants.Perfil.recortado.equals(perfil)){
			confirmacionViewController.limpiarCampos();
			((BmovilViewsController)confirmacionViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
		}
		operationDelegate.analyzeResponse(operationId, response);
	}
	
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		return tokenAMostrar;
	}
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return operationDelegate.mostrarCampoTarjeta();
	}

	@Override
	public String loadOtpFromSofttoken(TipoOtpAutenticacion tipoOTP) {
		return loadOtpFromSofttoken(tipoOTP, operationDelegate);
	}

	@Override
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler hadler, Server.isJsonValueCode isJsonValue, BaseViewController caller) {
		((BmovilViewsController) caller
				.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, hadler, isJsonValue, caller);
	}

    public void doNetworkOperation(final int operationId, final ParametersTO params, final boolean isJson,
                                   final Object handler, final Server.isJsonValueCode isJsonValueCode,
                                   final BaseViewController caller) {
        ((BmovilViewsController)confirmacionViewController.getParentViewsController()).getBmovilApp()
                .invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode,
                        caller, true);
    }
}
