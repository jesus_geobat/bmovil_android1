package suitebancomer.aplicaciones.bmovil.classes.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class ConsultaPagoCreditosData implements ParsingHandler {

    private static final long serialVersionUID = 1L;
    private ArrayList<ConsultaObtenerComprobante> movimientos;
    private String estado;
    private String codigoMensaje;
    private String descripcionMensaje;
    private String periodo;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoMensaje() {
        return codigoMensaje;
    }

    public void setCodigoMensaje(String codigoMensaje) {
        this.codigoMensaje = codigoMensaje;
    }

    public String getDescripcionMensaje() {
        return descripcionMensaje;
    }

    public void setDescripcionMensaje(String descripcionMensaje) {
        this.descripcionMensaje = descripcionMensaje;
    }

    public String getPeriodo() {
        return periodo;
    }

    public ArrayList<ConsultaObtenerComprobante> getMovimientos() {
        return movimientos;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public ConsultaPagoCreditosData() {
        estado = null;
        codigoMensaje = null;
        descripcionMensaje = null;
        periodo = null;
        movimientos = new ArrayList<ConsultaObtenerComprobante>();
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        estado = parser.parseNextValue("estado");
        //codigoMensaje = parser.parseNextValue("codigoMensaje");
        //descripcionMensaje = parser.parseNextValue("descripcionMensaje");
        if (estado.equals("OK")) {
            periodo = parser.parseNextValue("periodo");
            JSONObject aux = parser.parserNextObject("arrMovtos");
            JSONArray aux1;
            try {
                aux1 = aux.getJSONArray("ocMovtos");
                for (int i = 0; i < aux1.length(); i++) {
                    ConsultaObtenerComprobante cob = new ConsultaObtenerComprobante();
                    cob.setCuentaAbonoOGuia(aux1.getJSONObject(i).getString(
                            "numCredito"));
                    cob.setFecha(aux1.getJSONObject(i).getString("fecha"));
                    cob.setFolio(aux1.getJSONObject(i).getString("folio"));
                    cob.setHora(aux1.getJSONObject(i).getString("hora"));
                    cob.setImporte(aux1.getJSONObject(i).getString("importe"));
                    movimientos.add(cob);
                }
            } catch (JSONException e) {
                if (Server.ALLOW_LOG) e.printStackTrace();
            }
        } else if (estado.equals("ERROR")) {
            codigoMensaje = parser.parseNextValue("codigoMensaje");
            descripcionMensaje = parser.parseNextValue("descripcionMensaje");
        }
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }
}
