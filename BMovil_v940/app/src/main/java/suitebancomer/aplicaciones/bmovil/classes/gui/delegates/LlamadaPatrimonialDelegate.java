package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.app.Dialog;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.AvisoHorarioPatrimoniaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ContactoPatrimonialViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;

/**
 * Created by marioaguilar on 19/09/16.
 */
public class LlamadaPatrimonialDelegate extends BaseDelegate {


    public static final long LLAMADA_PATRIMONIAL_DELEGATE = 631453334566764322L;

    private AvisoHorarioPatrimoniaViewController controller;

    private Dialog currentDialog;

    public Dialog getCurrentDialog() {
        return currentDialog;
    }

    public void setCurrentDialog(Dialog currentDialog) {
        this.currentDialog = currentDialog;
    }

    public void setController(AvisoHorarioPatrimoniaViewController controller) {
        this.controller = controller;
    }



}
