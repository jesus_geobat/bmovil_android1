package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.BorrarDatosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.Catalog;
import suitebancomer.aplicaciones.bmovil.classes.model.NameValuePair;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class BorrarDatosViewController extends BaseViewController implements View.OnClickListener {
	
	private BorrarDatosDelegate borrarDatosDelegate;
	
	private TextView mTextView1;
	private TextView mTextView2;
	private Button mButton;
	
	/**
	 * Default constructor for this activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_borrar_datos);
		setTitle(R.string.borrarDatos_title, R.drawable.icono_borrar_datos, R.color.magenta);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate((BorrarDatosDelegate)parentViewsController.getBaseDelegateForKey(BorrarDatosDelegate.BORRAR_DATOS_DELEGATE_ID));
		borrarDatosDelegate = (BorrarDatosDelegate)getDelegate();
		borrarDatosDelegate.setBorrarDatosViewController(this);
		
		findViews();
		scaleToScreenSize();
		
		mButton.setOnClickListener(this);
		
		Catalog catalog = Session.getInstance(SuiteApp.appContext).getCatalog(Session.AYUDAS_CATALOG);
		NameValuePair texto1 = catalog.getNameValuePair("ALACT");
		NameValuePair texto2 = catalog.getNameValuePair("ALACT2");
		
		mTextView1.setText(texto1.getValue());
		mTextView2.setText(texto2.getValue());
	}

	@Override
	protected void onResume() {
		super.onResume();
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	public void onClick(View v) {
		if (v == mButton) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
	    	alertDialog.setTitle(R.string.common_confirmation);
	    	alertDialog.setMessage(R.string.borrarDatos_dialogConfirmation);
	    	
	    	//botón Cancelar
	    	alertDialog.setPositiveButton(R.string.common_cancel, null);
	    	
	    	//Continue button
	   		alertDialog.setNegativeButton(R.string.borrarDatos_buttonText, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						borrarDatosDelegate.desactivarAplicacion();
					}
			});
	    	alertDialog.show();
		}
	}

	@Override
	public void goBack() {
		getParentViewsController().removeDelegateFromHashMap(BorrarDatosDelegate.BORRAR_DATOS_DELEGATE_ID);
		super.goBack();
	}
	
	private void findViews() {
		mTextView1 = (TextView)findViewById(R.id.borrarDatos_texto1);
		mTextView2 = (TextView)findViewById(R.id.borrarDatos_texto2);
		mButton = (Button)findViewById(R.id.borrarDatos_boton);
	}

	private void scaleToScreenSize() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(mTextView1, true);
		guiTools.scale(mTextView2, true);
		
		guiTools.scale(mButton);
	}
}
