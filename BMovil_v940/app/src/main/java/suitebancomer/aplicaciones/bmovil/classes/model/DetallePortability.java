package suitebancomer.aplicaciones.bmovil.classes.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.json.JSONException;
import java.io.IOException;
import java.io.Serializable;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by elunag on 08/07/16.
 */
public class DetallePortability implements Serializable, ParsingHandler
{
    public String code;
    public String description;
    public String errorCode;
    private String referenceNumber;
    private String motivoRechazo;
    public String operationReferenceNumber;
    private ExternalBank externalBank;
    private String status;
    private String createdDate;

    private static DetallePortability ourInstance = new DetallePortability();

    public static DetallePortability getInstance() {
        return ourInstance;
    }

    public DetallePortability() {
    }

    public DetallePortability(final String code,final String description,final String errorCode,final String referenceNumber, final String operationReferenceNumber,final ExternalBank externalBank,final String status,final String createdDate)
    {
        this.code = code;
        this.description = description;
        this.errorCode = errorCode;
        this.referenceNumber = referenceNumber;
        this.operationReferenceNumber = operationReferenceNumber;
        this.externalBank = externalBank;
        this.status = status;
        this.createdDate = createdDate;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getOperationReferenceNumber() {
        return operationReferenceNumber;
    }

    public void setOperationReferenceNumber(String operationReferenceNumber) {
        this.operationReferenceNumber = operationReferenceNumber;
    }

    public ExternalBank getExternalBank() {
        return externalBank;
    }

    public void setExternalBank(ExternalBank externalBank) {
        this.externalBank = externalBank;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static DetallePortability getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(DetallePortability ourInstance) {
        DetallePortability.ourInstance = ourInstance;
    }

    public String getCode() {
        return code;
    }

    public String getMotivoRechazo() {
        return motivoRechazo;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        try {
            //birthDate= parser.parserNextObject("response").getJSONObject("person").getString("birthDate");
            code=parser.parserNextObject("status").getString("code");
            description =parser.parserNextObject("status").getString("description");
            referenceNumber = parser.parserNextObject("response").getString("referenceNumber");
            operationReferenceNumber = parser.parserNextObject("response").getString("createdReferenceNumber");
            referenceNumber = parser.parserNextObject("response").getJSONObject("externalAccount").getString("referenceNumber");
            final String banco = parser.parserNextObject("response").getString("externalBank");
            final Gson gson = new Gson();
            final JsonElement mJson = new JsonParser().parse(banco);
            externalBank = gson.fromJson(mJson, ExternalBank.class);
            status = parser.parserNextObject("response").getString("status");
            motivoRechazo = parser.parserNextObject("response").getString("rejectionDescription");
            createdDate = parser.parserNextObject("response").getString("createdDate");


            //errorCode = parser.parseNextValue("errorCode");
        } catch (JSONException e) {
            // e.printStackTrace();
        }finally {
            description =parser.parseNextValue("description");
            errorCode = parser.parseNextValue("errorCode");
        }

    }
}
