package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bancomer.mbanking.R;

import suitebancomer.aplicaciones.bmovil.classes.common.ToolsCommons;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;

/**
 * Created by andres.vicentelinare on 12/10/2016.
 */
public class FrecuentesDineroMovilAdapter extends ArrayAdapter<Payment> {

    Context context;
    int layoutResourceId;
    Payment data[] = null;

    public FrecuentesDineroMovilAdapter(Context context, int layoutResourceId, Payment[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        FrecuentesDineroMovilHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new FrecuentesDineroMovilHolder();
            holder.txtNombre = (TextView) row.findViewById(R.id.valor_nombre);
            holder.txtNombre.setSelected(true);
            holder.txtCelular = (TextView) row.findViewById(R.id.valor_celular);

            row.setTag(holder);
        } else {
            holder = (FrecuentesDineroMovilHolder) row.getTag();
        }

        Payment payment = data[position];
        holder.txtNombre.setText(payment.getNickname());
        holder.txtCelular.setText(ToolsCommons.formatPhoneNumber(payment.getBeneficiaryAccount()));

        return row;
    }

    static class FrecuentesDineroMovilHolder {
        TextView txtNombre;
        TextView txtCelular;
    }
}
