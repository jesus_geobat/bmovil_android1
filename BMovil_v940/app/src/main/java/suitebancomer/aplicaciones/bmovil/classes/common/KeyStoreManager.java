package suitebancomer.aplicaciones.bmovil.classes.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;

import javax.crypto.spec.SecretKeySpec;

import suitebancomer.aplicaciones.bmovil.classes.io.Server;

import com.bancomer.keygenerator.ToolsKey;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

@SuppressLint("NewApi")
public class KeyStoreManager {
	static {
	     Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);
	}
	 
	/**
	 * Password por defecto del keystore.
	 */
	private final String DEFAULT_KEY_STORE_PASSWORD = "passssword";
	
	private String mKeyStorePassword = DEFAULT_KEY_STORE_PASSWORD;
	private String mKeyStoreName;
	private KeyStore mKeystore = null;
	private File keyStoreFile;
	private Context context;
	
	/**
	 * 
	 * @param keyStoreFilePath
	 * @param keyStoreFileName
	 * @throws KeyStoreException
	 * @throws IOException 
	 */
	//public KeyStoreManager(File keyStoreFilePath, String keyStoreFileName, Context context) throws KeyStoreException, IOException{
	//mKeyStoreName = keyStoreFileName;
	public KeyStoreManager(File keyStoreFilePath, String keyStoreFileName, Context context) throws KeyStoreException{
		try{
			mKeyStoreName = keyStoreFileName;
			// Creamos el fichero
			keyStoreFile = new File(keyStoreFilePath,mKeyStoreName);

			setContext(context);
			mKeyStorePassword = ToolsKey.generateKey(context);
			if(Server.ALLOW_LOG) Log.i("BConnectKeyStoreManager","Clave para keystore " + mKeyStorePassword);
			//System.out.println("Iniciando keystore manager con fichero: " + keyStoreFile.getAbsolutePath());

			openOrCreate();

		} catch (Exception e){
			throw new KeyStoreException(e);
		}
	}
	
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	/**
	 * Crea el keystore
	 * @throws KeyStoreException
	 */
	private void openOrCreate() throws KeyStoreException {
		try {
			// Creamos la instancia del keystore
			mKeystore = KeyStore.getInstance(KeyStore.getDefaultType());

			// Comprobamos si existe el fichero.
			if (!keyStoreFile.exists() || (0 == keyStoreFile.length())) {
				// No existe, iniciamos el keystore
				mKeystore.load(null, null);

				//System.out.println("Keystore creado OK");
			} else {
				// Existe, cargamos el keystore con el contenido del fichero.
				if (Server.ALLOW_LOG)
					Log.i("BConnectKeyStoreManager", "Accediendo a keystore " + mKeyStorePassword);
				mKeystore.load(new FileInputStream(keyStoreFile), mKeyStorePassword.toCharArray());
				//System.out.println("Keystore inicializado OK");
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			throw new KeyStoreException(e);
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			throw new KeyStoreException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new KeyStoreException(e);
		} catch (KeyStoreException e) {
			throw new KeyStoreException(e);
		}
	}
	
	/**
	 * Almacena en el keystore una nueva entrada, informando si se ha relizado correctamente o no la escritura. Si se produce
	 * un error lanzaría la exception KeyManagerStoreException
	 * @param alias clave
	 * @param secretKey valor
	 * @return true si ha escrito correctamente, false en caso contrario
	 */
	public boolean setEntry(String alias, String secretKey) throws KeyManagerStoreException{

	    boolean keyStoreEntryWritten = false;

	    if (mKeystore != null && secretKey != null) {
	        // store something in the key store
	        SecretKeySpec sks = new SecretKeySpec(secretKey.getBytes(), "MD5");
	        KeyStore.SecretKeyEntry ske = new KeyStore.SecretKeyEntry(sks);
	        KeyStore.ProtectionParameter pp = new KeyStore.PasswordProtection(null);

	        try {
	        	//if(Server.ALLOW_LOG) Log.i("Introducir dato en keystore",alias + " " + secretKey);
	            mKeystore.setEntry(alias, ske, pp);

	            // Salvamos el keystore con la nueva clave
	            boolean success = save();

	            if (success) {
	                keyStoreEntryWritten = true;
	            }
	        } catch (KeyStoreException ex) {
	            //System.out.println("Failed to read keystore" + mKeyStoreName);
	            throw new KeyManagerStoreException(ex);
	        }
	    }
	    return keyStoreEntryWritten;
	}

	/**
	 * Retorna el valor de la clave almacenada en el keystore.
	 * @param alias
	 * @return
	 */
	public String getEntry(String alias) throws KeyManagerStoreException {

	    String secretStr = null;
	    byte[] secret = null;

	    if (mKeystore != null) {
	        try {
	            if (!mKeystore.containsAlias(alias)) {
	            	//System.out.println(new StringBuilder().append("Keystore ").append(mKeyStoreName)
	                   //     .append(" no contiene clave ").append(alias).toString());
	                return null;
	            }
		        // get my entry from the key store
		        KeyStore.ProtectionParameter pp = new KeyStore.PasswordProtection(null);
		        KeyStore.SecretKeyEntry ske = null;
		        ske = (KeyStore.SecretKeyEntry) mKeystore.getEntry(alias, pp);
		        if (ske != null) {
		            SecretKeySpec sks = (SecretKeySpec) ske.getSecretKey();
		            secret = sks.getEncoded();

		            if (secret != null) {
		                secretStr = new String(secret);
		            } else {
		            	//System.out.println(new StringBuilder().append("La lectura ha sido vacía").append(alias).toString());
		            }
		        } else {
		        	//System.out.println("Fallo al leer del keystore la clave " + alias);
		        }
	        } catch (Exception ex) {
	        	//System.out.println("Fallo al leer del keystore la clave " + alias);
	        	throw new KeyManagerStoreException(ex);
	        }

	       
	    } 
	    
	    return secretStr;
	}
	
	/**
	 * Nos permitirá almacenar el keystore en un fichero.
	 * @return
	 */
	private boolean save() {

	    FileOutputStream fos = null;
	    boolean keyStoreSaved = true;

	    try {
	        fos = new FileOutputStream(keyStoreFile);
	        mKeystore.store(fos, mKeyStorePassword.toCharArray());
	    } catch (Exception ex) {
	        keyStoreSaved = false;
	       // System.out.println("Ha fallado al salvar el keystore " + mKeyStoreName);
	    } finally {
	        if (fos != null) {
	            try {
	                fos.close();
	            } catch (IOException ex) {
	                keyStoreSaved = false;
	                //System.out.println("Fallo al cerrar FileOutputStream");
	            }
	        }
	    }
	    return keyStoreSaved;
	}
	/**
	 * Cambia la contraseña de cifrado para el keystore
	 * @param mKeyStorePassword
	 */
	public void setKeyStorePassword(String mKeyStorePassword) {
		this.mKeyStorePassword = mKeyStorePassword;
	}
	
	
}
