package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.common.ToolsCommons;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.FrecuentesDineroMovilViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;
import suitebancomer.aplicaciones.bmovil.classes.model.PaymentExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;

/**
 * Created by andres.vicentelinare on 12/10/2016.
 */
public class FrecuentesDineroMovilDelegate extends DelegateBaseOperacion {

    public final static long FRECUENTES_DINERO_MOVIL_DELEGATE_ID = -6907597496349345428L;

    private FrecuentesDineroMovilViewController frecuentesDineroMovilViewController;
    private Payment[] frecuentes;

    public FrecuentesDineroMovilViewController getFrecuentesDineroMovilViewController() {
        return frecuentesDineroMovilViewController;
    }

    public void setFrecuentesDineroMovilViewController(FrecuentesDineroMovilViewController frecuentesDineroMovilViewController) {
        this.frecuentesDineroMovilViewController = frecuentesDineroMovilViewController;
    }

    public Payment[] getFrecuentes() {
        return frecuentes;
    }

    public void setFrecuentes(Payment[] frecuentes) {
        this.frecuentes = frecuentes;
    }

    public void consultarFrecuentes() {
        frecuentes = null;

        Session session = Session.getInstance(SuiteApp.appContext);
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());//NT
        paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
        paramTable.put("TE", session.getClientNumber());//TE
        paramTable.put("TP", Constants.tipoCFDineroMovil);//TP
        paramTable.put("AP", "");
        paramTable.put("VM", Constants.APPLICATION_VERSION);

        ((BmovilViewsController) frecuentesDineroMovilViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable, false, new PaymentExtract(), Server.isJsonValueCode.NONE, frecuentesDineroMovilViewController, true);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {

            if (operationId == Server.OP_SOLICITAR_ALERTAS) {
                SolicitarAlertasData validacionalertas = (SolicitarAlertasData) response.getResponse();
                if (Constants.ALERT02.equals(validacionalertas.getValidacionAlertas())) {
                    // 02 no tiene alertas
                    SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
                    SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));
                } else {
                    setSa(validacionalertas);
                    analyzeAlertasRecortadoSinError();
                }
                return;
            }

            if (response.getResponse() instanceof PaymentExtract) {
                PaymentExtract extract = (PaymentExtract) response.getResponse();
                setFrecuentes(extract.getPayments());
                frecuentesDineroMovilViewController.muestraFrecuentes("");
            }

        } else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
            if (operationId != Server.FAVORITE_PAYMENT_OPERATION) {
                frecuentesDineroMovilViewController.showErrorMessage(response.getMessageText());
            }

        } else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
            if (operationId == Server.FAVORITE_PAYMENT_OPERATION) {
                frecuentes = null;
                frecuentesDineroMovilViewController.muestraFrecuentes("");
            }
        }
    }

    public Payment[] frecuentesFiltrados(String filtro) {
        List<Payment> payments = new ArrayList<Payment>();

        if (frecuentes != null) {
            for (Payment payment : frecuentes) {
                if (payment.getNickname().toLowerCase().contains(filtro.toLowerCase())) {
                    payments.add(payment);
                }
            }
        }

        return payments.toArray(new Payment[payments.size()]);
    }


    public ArrayList<Object> getDatosTablaFrecuentes(String filtro) {
        ArrayList<Object> listaDatos = new ArrayList<Object>();
        ArrayList<Object> registro;
        if (frecuentes != null) {
            for (Payment payment : frecuentes) {
                if(payment.getNickname().toLowerCase().contains(filtro.toLowerCase())) {
                    registro = new ArrayList<Object>();
                    registro.add(payment);
                    registro.add(payment.getNickname());
                    registro.add(ToolsCommons.formatPhoneNumber(payment.getBeneficiaryAccount()));
                    listaDatos.add(registro);
                }
            }
        }
        return listaDatos;
    }

    @Override
    public void performAction(Object obj) {
        TransferenciaDineroMovil transferenciaDineroMovil = new TransferenciaDineroMovil();
        Payment selectedPayment = (Payment)obj;

        transferenciaDineroMovil.setCelularbeneficiario(selectedPayment.getBeneficiaryAccount());
        transferenciaDineroMovil.setNombreCompania(selectedPayment.getOperadora());
        transferenciaDineroMovil.setBeneficiario(selectedPayment.getBeneficiary());
        transferenciaDineroMovil.setConcepto(selectedPayment.getConcept());
        //multicanal
        transferenciaDineroMovil.setFrecuenteMulticanal(selectedPayment.getIdToken());
        transferenciaDineroMovil.setIdCanal(selectedPayment.getIdCanal());
        transferenciaDineroMovil.setUsuario(selectedPayment.getUsuario());
        transferenciaDineroMovil.setAliasFrecuente(selectedPayment.getNickname());
        transferenciaDineroMovil.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());
        ((BmovilViewsController) frecuentesDineroMovilViewController.getParentViewsController()).showRetiroSinTarjetaViewController(transferenciaDineroMovil, AltaRetiroSinTarjetaDelegate.TipoRetiro.DINERO_MOVIL_FRECUENTE);
    }

    public ArrayList<Object> getDatosHeaderTablaFrecuentes(){
        ArrayList<Object> header = new ArrayList<Object>(0);
        header.add(" ");
        header.add("Nombre corto");
        header.add("Teléfono");

        return header;
    }
}
