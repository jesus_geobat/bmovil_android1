package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;

import java.util.Hashtable;

        import bancomer.api.common.commons.Constants;
        import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
        import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
        import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
        import suitebancomer.aplicaciones.bmovil.classes.io.Server;
        import suitebancomer.aplicaciones.bmovil.classes.model.ContratoGestor;
        import suitebancomer.aplicaciones.bmovil.classes.model.TerminosCondicionesBanquero;
        import suitebancomer.classes.gui.controllers.BaseViewController;
        import suitebancomer.classes.gui.delegates.BaseDelegate;
        import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
        import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
/**
 * Created by eluna on 21/06/2016.
 */
public class ContratacionBanqueroPersonalDelegate extends BaseDelegate
{
    BaseViewController viewController;

    public void peticionConsultaContratoGestor(BaseViewController viewController)
    {
        this.viewController = viewController;
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        int operacion = Server.OP_CONSULTA_CONTRATO_GESTOR_REMOTO;
        paramTable.put(ServerConstants.OPERACION,"consultaContratoGestor");
        doNetworkOperation(operacion, paramTable, Boolean.TRUE, TerminosCondicionesBanquero.getInstance(), Server.isJsonValueCode.NONE, viewController);

    }

    public void peticionConsultaDetalleContratoGestor(BaseViewController viewController)
    {
        this.viewController = viewController;
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        int operacion = Server.OP_DETALLE_GESTOR;
        paramTable.put(ServerConstants.OPERACION,"detalleGestor");
        Session session = Session.getInstance(viewController);
        paramTable.put(ServerConstants.NUMERO_TELEFONO,session.getUsername());
        paramTable.put(ServerConstants.IUM, session.getIum());
        //paramTable.put("claveCampaña","0558CAM0095550001");
        paramTable.put("claveCampana",MenuPrincipalDelegate.getStoreClavComp(viewController));//pruebas clavCamp
        paramTable.put("aceptaCampaña","0002");
        Log.d("ContrataConBanqueroPersonalDElegate","Clave Campaña: "+MenuPrincipalDelegate.getStoreClavComp(viewController));
        doNetworkOperation(operacion, paramTable, Boolean.TRUE, null, Server.isJsonValueCode.NONE, viewController);
    }

    public void peticionContratacionBanqueroPersonal(final BaseViewController viewController,
                                                     final  String contrasena, final String nip,
                                                     final String cvv, final String token)
    {
        this.viewController = viewController;
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        int operacion = Server.OP_CONTRATO_GESTOR_REMOTO;
        paramTable.put(ServerConstants.OPERACION,"contratoGestor");
        Session session = Session.getInstance(viewController);
        paramTable.put(ServerConstants.NUMERO_TELEFONO,session.getUsername());
        paramTable.put(ServerConstants.IUM, session.getIum());
        //paramTable.put("claveCampaña","0558CAM0095550001");
        paramTable.put("claveCampaña",MenuPrincipalDelegate.getStoreClavComp(viewController));//pruebas clavCamp
        Log.d("ContrataConBanqueroPersonalDElegate", "Clave Campaña: " + MenuPrincipalDelegate.getStoreClavComp(viewController));
        paramTable.put("aceptaCampaña","0001");
        paramTable.put("cveAcceso",contrasena);
        paramTable.put("codigoCVV2",cvv);
        paramTable.put("codigoNIP",nip);

        paramTable.put("codigoOTP",token);
        String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.gestorRemoto,Session.getInstance(viewController).getClientProfile());
        paramTable.put("cadenaAutenticacion",cadenaAutenticacion);
        doNetworkOperation(operacion, paramTable, Boolean.TRUE, ContratoGestor.getInstance(), Server.isJsonValueCode.NONE, viewController);
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler hadler, Server.isJsonValueCode isJsonValue, BaseViewController caller) {
        ((BmovilViewsController) caller
                .getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, hadler, isJsonValue, caller);
    }
}
