package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class SincroExportSTData implements ParsingHandler {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String estado;
	private String indReac2x1;
	
	public SincroExportSTData() {
		super();
	}
	public SincroExportSTData(String estado, String indReac2x1) {
		super();
		this.estado = estado;
		this.indReac2x1 = indReac2x1;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getIndReac2x1() {
		return indReac2x1;
	}
	public void setIndReac2x1(String indReac2x1) {
		this.indReac2x1 = indReac2x1;
	}
	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		
		this.estado= parser.parseNextValue("estado");
		this.indReac2x1 = parser.parseNextValue("indReac2x1");
		
	}
}
