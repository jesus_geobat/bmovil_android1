package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;
import java.util.Hashtable;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ExitoOfertaConsumoViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuPrincipalViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.ActualizarCuentasResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaPoliza;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;

public class ExitoOfertaConsumoDelegate extends BaseDelegate{
	OfertaConsumo ofertaConsumo;
	Promociones promocion;
	AceptaOfertaConsumo aceptaOfertaConsumo;
	ExitoOfertaConsumoViewController exitoOfertaConsumoViewController;
	
	public static final long EXITO_OFERTA_CONSUMO_DELEGATE = 631811334266765318L;
	private BaseViewController controladorDetalleILCView;


	public ExitoOfertaConsumoDelegate(AceptaOfertaConsumo aceptaOfertaConsumo, OfertaConsumo ofertaConsumo){
		this.ofertaConsumo= ofertaConsumo;
		this.aceptaOfertaConsumo= aceptaOfertaConsumo;


	}	

	public AceptaOfertaConsumo getAceptaOfertaConsumo() {
		return aceptaOfertaConsumo;
	}


	public void setAceptaOfertaConsumo(AceptaOfertaConsumo aceptaOfertaConsumo) {
		this.aceptaOfertaConsumo = aceptaOfertaConsumo;
	}


	public OfertaConsumo getOfertaConsumo() {
		return ofertaConsumo;
	}



	public void setOfertaConsumo(OfertaConsumo ofertaConsumo) {
		this.ofertaConsumo = ofertaConsumo;
	}


	public void realizaOperacion(int idOperacion,
			BaseViewController baseViewController, boolean isSMSEMail) {
		Session session = Session.getInstance(SuiteApp.appContext);
		String ium= session.getIum();
		String numCel= session.getUsername();
		if(idOperacion == Server.DOMICILIACION_OFERTA_CONSUMO){
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "consultaDomiciliacionBovedaConsumoBMovil");
			params.put("IdProducto", "PBCCMDOM01");
			params.put("numeroCelular", numCel);
			params.put("bscPagar", ofertaConsumo.getbscPagar());
			params.put("numCredito",aceptaOfertaConsumo.getNumeroCredito());// de donde se obtiene
			params.put("indicad", "D");
			params.put("IUM", ium);
			//JAIG
			doNetworkOperation(Server.DOMICILIACION_OFERTA_CONSUMO, params,true,new ConsultaPoliza(),isJsonValueCode.CONSUMO,
					baseViewController);
		
		
		}else if (idOperacion == Server.ACTUALIZAR_CUENTAS){//codigo actualizacion de cuentas
			
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("numeroTelefono", session.getUsername());
			params.put("numeroCliente", session.getClientNumber());
			params.put("IUM", session.getIum());
			//JAIG
			doNetworkOperation(Server.ACTUALIZAR_CUENTAS, params,true, new ActualizarCuentasResult(),isJsonValueCode.NONE, baseViewController);
			
		}
		
		
		else if(idOperacion == Server.CONTRATO_OFERTA_CONSUMO){
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "consultaContratoBovedaConsumoBmovil");
			params.put("IdProducto", ofertaConsumo.getProducto());
			params.put("numeroCelular", numCel);
			params.put("numCredito",aceptaOfertaConsumo.getNumeroCredito());// de donde se obtiene
			params.put("indicad", "C");
			params.put("plazoSal", aceptaOfertaConsumo.getPlazoSal());
			params.put("IUM", ium);
			//JAIG
			doNetworkOperation(Server.CONTRATO_OFERTA_CONSUMO, params,true,new ConsultaPoliza(),isJsonValueCode.CONSUMO,
					baseViewController);
		}else if(idOperacion==Server.SMS_OFERTA_CONSUMO){
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "exitoConsumoBMovil");
			params.put("numeroCelular",numCel);
			params.put("numCredito",aceptaOfertaConsumo.getNumeroCredito());
			params.put("idProducto",ofertaConsumo.getProducto());
			String estatus="";
			if(aceptaOfertaConsumo.getEstatusSal().equals("4")&& ofertaConsumo.getEstatusOferta().equals(Constants.PREABPROBADO)){
				estatus="E";
			}else if(!aceptaOfertaConsumo.getEstatusSal().equals("4")&& ofertaConsumo.getEstatusOferta().equals(Constants.PREABPROBADO)){
				estatus="T";
			}else if(ofertaConsumo.getEstatusOferta().equals(Constants.PREFORMALIZADO)){
				estatus="E";
			}
			params.put("estatusContratacion",estatus); //definir de donde se obtendra
			params.put("cuentaVinc",ofertaConsumo.getCuentaVinc());
			params.put("fechaCat",ofertaConsumo.getFechaCat());
			String importe=Tools.formatAmount(ofertaConsumo.getImporte(),false);
			importe=importe.replace("$", "");
			params.put("Importe",importe);
			params.put("plazoDes",aceptaOfertaConsumo.getPlazoSal());
			System.out.println("Valor plazoDes: "+ aceptaOfertaConsumo.getPlazoSal());
			String pagoMensualFijo = Tools.formatAmount(ofertaConsumo.getPagoMenFijo(),false);
			params.put("pagoMensualFijo",pagoMensualFijo);
			String email= session.getEmail();
			params.put("email",email);
			params.put("Cat",ofertaConsumo.getCat());
			if(isSMSEMail)
				params.put("mensajeA","011");
			else if(email.equals(""))
				params.put("mensajeA","001");
			else
				params.put("mensajeA","001");		
			params.put("IUM",ium);
			//JAIG
			doNetworkOperation(Server.SMS_OFERTA_CONSUMO, params,true,null,isJsonValueCode.CONSUMO,
					baseViewController);
		}

	}
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if( controladorDetalleILCView != null)
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,false);
	}
	
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (operationId == Server.DOMICILIACION_OFERTA_CONSUMO){
			ConsultaPoliza polizaResponse = (ConsultaPoliza)response.getResponse();
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showformatodomiciliacion(polizaResponse.getTerminosHtml());
		}else if(operationId == Server.CONTRATO_OFERTA_CONSUMO){
			ConsultaPoliza polizaResponse = (ConsultaPoliza)response.getResponse();
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showContrato(polizaResponse.getTerminosHtml());
		}else if(operationId == Server.SMS_OFERTA_CONSUMO){
			
			this.realizaOperacion(Server.ACTUALIZAR_CUENTAS, controladorDetalleILCView, false);//operacion de actualizacion de cuentas
			
			
		
		}else if(operationId==Server.ACTUALIZAR_CUENTAS){  //Actualizacion de cuentas
					
					if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
							if(response.getResponse() instanceof ActualizarCuentasResult){
				
								ActualizarCuentasResult result = (ActualizarCuentasResult) response
										.getResponse();
								actualizacionDeCuentasExitosa(result.getAsuntos());
					
				
			}
					}	//fin successfull
					else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
						exitoOfertaConsumoViewController.showInformationAlert(response
								.getMessageText());
				
					}
			
			
		}//Fin de validacion de actualizacion de cuentas
	}

	public BaseViewController getControladorDetalleILCView() {
		return controladorDetalleILCView;
	}
	public void setControladorDetalleILCView(
			BaseViewController controladorDetalleILCView) {
		this.controladorDetalleILCView = controladorDetalleILCView;
	}
	
	//Metodo que actualiza las cuentas
	private void actualizacionDeCuentasExitosa(Account[] accounts) {
		Session session = Session.getInstance(SuiteApp.appContext);
		session.updateAccounts(accounts);
		
    }
	
	
	public void showMenu() {
		// TODO Auto-generated method stub
		((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showMenuPrincipal();
		
	}
	
	
	
}
