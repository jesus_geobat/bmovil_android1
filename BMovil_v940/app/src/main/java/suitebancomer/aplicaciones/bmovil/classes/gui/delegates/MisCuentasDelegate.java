package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import bancomer.api.codigoqr.config.CodigoQRFacade;
import bancomer.api.codigoqr.models.OperacionQR;
import bancomer.api.consultainversiones.gui.controllers.DetalleInversionViewController;
import bancomer.api.consultainversiones.gui.controllers.DetalleMenuInversionesViewController;
import bancomer.api.consultainversiones.implementations.InitConsultaInversiones;
import bancomer.api.consultainversiones.implementations.InitMenuInversiones;
import bancomer.api.consultainversiones.models.ConsultaInversionesPlazoData;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import bancomer.api.consultaotroscreditos.models.ConsultaOtrosCreditosData;
import bancomer.api.consultaotroscreditos.models.Creditos;
import bancomer.api.pagarcreditos.gui.controllers.PagarCreditoViewController;
import bancomer.api.pagarcreditos.implementations.InitPagoCredito;
import bancomer.api.pagarcreditos.io.BanderasServer;
import bancomer.api.pagarcreditos.models.Credito;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuPrincipalViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MisCuentasViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ContratoPatrimonial;
import suitebancomer.aplicaciones.bmovil.classes.model.ContratosPatrimonial;
import suitebancomer.aplicaciones.bmovil.classes.model.DetallePosicionPatrimonial;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.commservice.commons.MethodType;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import bancomer.api.pagarcreditos.models.ImportesPagoCreditoData;

public class MisCuentasDelegate extends DelegateBaseAutenticacion {

	public final static long MIS_CUENTAS_DELEGATE_ID = 0xfa68bdd87706a316L;
	
	private MisCuentasViewController misCuentasViewController;
	
	private Account cuentaSeleccionada;

	private Creditos creditoSeleccionado;

	private Class<?> selectedClass;
	
	private Boolean opcionRetiroSinTarjeta= false;

	private ConsultaOtrosCreditosData consultaOtrosCreditosData;

	private ConsultaInversionesPlazoData consultaInversionesPlazoData;

	//Patrimonial
	private String patrimonialOption;


	public Boolean getOpcionRetiroSinTarjeta() {
		return opcionRetiroSinTarjeta;
	}

	public void setOpcionRetiroSinTarjeta(Boolean opcionRetiroSinTarjeta) {
		this.opcionRetiroSinTarjeta = opcionRetiroSinTarjeta;
	}

	public ConsultaOtrosCreditosData getConsultaOtrosCreditosData() {
		return consultaOtrosCreditosData;
	}

	public void setConsultaOtrosCreditosData(
			ConsultaOtrosCreditosData consultaOtrosCreditosData) {
		this.consultaOtrosCreditosData = consultaOtrosCreditosData;
	}

	public Creditos getCreditoSeleccionado() {
		return creditoSeleccionado;
	}

	public void setCreditoSeleccionado(Creditos creditoSeleccionado) {
		this.creditoSeleccionado = creditoSeleccionado;
	}

	public ConsultaInversionesPlazoData getConsultaInversionesPlazoData() {
		return consultaInversionesPlazoData;
	}

	public void setConsultaInversionesPlazoData(ConsultaInversionesPlazoData consultaInversionesPlazoData) {
		this.consultaInversionesPlazoData = consultaInversionesPlazoData;
	}


	public void setCuentaSeleccionada(Account cuentaSeleccionada) {
		this.cuentaSeleccionada = cuentaSeleccionada;
	}



	public void inicializar()
	{

	}
	
	public MisCuentasViewController getMisCuentasViewController() {
		return misCuentasViewController;
	}

	public void setMisCuentasViewController(MisCuentasViewController misCuentasViewController) {
		this.misCuentasViewController = misCuentasViewController;
	}
	
	public ArrayList<Object> getOpcionesVisibles() {
		ArrayList<Object> registros;
		ArrayList<Object>  lista = new ArrayList<Object>();
		
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();

		if(!selectedClass.equals(Creditos.class)) {

			boolean validateFullMenu = (profile == Constants.Perfil.avanzado) || cuentaSeleccionada.isVisible();

			registros = new ArrayList<Object>(2);
			registros.add(Constants.OPERACION_VER_MOVIMIENTOS_TYPE);
			registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_movimientos));
			lista.add(registros);

			if (validateFullMenu) {
				//if(Autenticacion.getInstance().isVisible(Constants.Operacion.dineroMovil, profile)){
				registros = new ArrayList<Object>(2);
				//registros.add(Constants.OPERACION_EFECTIVO_MOVIL_TYPE);
				//registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_dineroMovil));
				registros.add(Constants.OPERACION_TRASPASO_MIS_CUENTAS);
				registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_traspasoMisCuentas));
				lista.add(registros);
				//}

				/** UNIFICACIÓN DINERO MÓVIL + RETIRO SIN TARJETA */
//				if (Autenticacion.getInstance().isVisible(Constants.Operacion.dineroMovil, profile) && !cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
//					registros = new ArrayList<Object>(2);
//					registros.add(Constants.OPERACION_EFECTIVO_MOVIL_TYPE);
//					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_dineroMovil));
//					lista.add(registros);
//				}

				if (Autenticacion.getInstance().isVisible(Constants.Operacion.transferirBancomer, profile) && !cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.INTERNAL_TRANSFER_DEBIT_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_transferenciaBancomer));
					lista.add(registros);
				}

				/*if(Autenticacion.getInstance().isVisible(Constants.Operacion.transferirBancomer, profile)){
					registros = new ArrayList<Object>(2);
					registros.add(Constants.INTERNAL_TRANSFER_EXPRESS_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_transferenciaExpress));
					lista.add(registros);
				}
				*/
				if (Autenticacion.getInstance().isVisible(Constants.Operacion.transferirInterbancaria, profile)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.EXTERNAL_TRANSFER_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_transferenciaOtros));
					lista.add(registros);
				}

				//			if(Autenticacion.getInstance().isVisible(Constants.Operacion.importesTDC, profile)){
//				if (cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.OPERACION_TDC);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_pagarTDC));
					lista.add(registros);
//				}

				if (Autenticacion.getInstance().isVisible(Constants.Operacion.pagoServicios, profile)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.SERVICE_PAYMENT_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_pagarServicio));
					lista.add(registros);
				}

				if (Autenticacion.getInstance().isVisible(Constants.Operacion.compraTiempoAire, profile) && !cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.AIRTIME_PURCHASE_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_compraTiempoAire));
					lista.add(registros);
				}

				/*if(Autenticacion.getInstance().isVisible(Constants.Operacion.compraTiempoAire, profile)){
					registros = new ArrayList<Object>(2);
					registros.add(Constants.AIRTIME_PURCHASE_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_compraTiempoAire));
					lista.add(registros);
				}*/

				//O3
				if (!cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.OPERACION_DEPOSITOS_RECIBIDOS);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_depositosrecibidos));
					lista.add(registros);
				}

				//Retiro sin tarjeta
				if (Autenticacion.getInstance().isVisible(Constants.Operacion.retiroSinTarjeta, profile) && !cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
					//&& (!cuentaSeleccionada.getType().equalsIgnoreCase("TC"))){
					registros = new ArrayList<Object>(2);
					registros.add(Constants.OPERACION_RETIRO_SINTARJETA);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_retiroSinTarjeta));
					lista.add(registros);
				}
				if(ServerCommons.EDOCUENTA) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.CONSULTAR_ESTADOS_DE_CUENTA);
					registros.add("Estados Financieros");
					lista.add(registros);
				}

				if (!cuentaSeleccionada.getType().equals(Constants.EXPRESS_TYPE) && !cuentaSeleccionada.getType().equals(Constants.PREPAID_TYPE)) {
				registros = new ArrayList<Object>(2);
				registros.add(Constants.OPERACION_CLABE_CUENTA);
					if (cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)){
						registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_ConsultarNumTarjeta));
					}else {
						registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_ConsultarClabe));
					}
						lista.add(registros);
					}

				registros = new ArrayList<Object>(2);
				registros.add(Constants.OPERACION_VER_CODIGO_QR);
				registros.add("Ver código QR");
				lista.add(registros);

				//cambiar tu nomina
				if (Autenticacion.getInstance().isVisible(Constants.Operacion.transferirInterbancariaF, profile)){
				 if ((cuentaSeleccionada.getType().equals(Constants.CHECK_TYPE) || cuentaSeleccionada.getType().equals(Constants.LIBRETON_TYPE) || cuentaSeleccionada.getType().equals(Constants.SAVINGS_TYPE)) && (Session.getInstance(SuiteApp.appContext).getClientProfile().equals(Constants.Perfil.avanzado))  && ServerCommons.ARQSERVICE) {
					//&& (!cuentaSeleccionada.getType().equalsIgnoreCase("TC"))){
					registros = new ArrayList<Object>(2);
					registros.add(Constants.OPERACION_CAMBIAR_NOMINA);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_cambiaNomina));
					lista.add(registros);
				}
				}
				}


		}else{
			// Se habrá seleccionado un credito
			registros = new ArrayList<Object>(2);
			registros.add(Constants.OPERACION_OTROS_CREDITOS);
			registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_informacionGeneral));
			lista.add(registros);

			registros = new ArrayList<Object>(2);
			registros.add(Constants.OPERACION_PAGAR_OTROS_CREDITOS);
			registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_pagarCredito));
			lista.add(registros);
		}
		
		return lista;
	}
	
	public ArrayList<Object> agruparCuentas(){
		ArrayList<Object> arregloCuentasTarjetas = new ArrayList<Object>();
		
        ArrayList<Account> cuentas = new ArrayList<Account>(5);
        ArrayList<Account> tarjetas = new ArrayList<Account>(5);
        
        for(Account a : Session.getInstance(SuiteApp.appContext).getAccounts()){
        //for(Account a : Session.getAccountsMC()){
      	  if (!a.getType().equals(Constants.CREDIT_TYPE)&& !a.getType().equals(Constants.INVERSION_TYPE) ){
           	  cuentas.add(a);
      	  } else if (a.getType().equals(Constants.CREDIT_TYPE)) {
      		  tarjetas.add(a);
      	  }
        }
        
        if (cuentas.size() > 0) {
			arregloCuentasTarjetas.add(cuentas);
		}
        if (tarjetas.size() > 0) {
			arregloCuentasTarjetas.add(tarjetas);
		}
        
		return arregloCuentasTarjetas;
	}
	
	public ArrayList<Account> getInversiones() {
		ArrayList<Account> inversiones = new ArrayList<Account>();

		for(Account a : Session.getInstance(SuiteApp.appContext).getAccounts()){
			if (a.getType().equals(Constants.INVERSION_TYPE)) {
				inversiones.add(a);
			}
		}

		return inversiones;
	}

	@Override
	public void performAction(Object obj) {
		if (misCuentasViewController.getMisCuentasOpciones() != null && misCuentasViewController.getMisCuentasOpciones().isShowing()) {
			if(selectedClass.equals(Creditos.class)){
				if (bancomer.api.common.commons.Tools.isNetworkConnected(misCuentasViewController)) {
					misCuentasViewController.getMisCuentasOpciones().opcionSeleccionada(creditoSeleccionado, (String)obj);
				} else {
					misCuentasViewController.getMisCuentasOpciones().dismiss();
					misCuentasViewController.showErrorMessage(misCuentasViewController.getString(R.string.error_communications));
				}
			}else{
				misCuentasViewController.getMisCuentasOpciones().opcionSeleccionada(cuentaSeleccionada, (String)obj);
			}
		} else if(obj instanceof Creditos){
			creditoSeleccionado = (Creditos)obj;
			selectedClass = Creditos.class;

			misCuentasViewController.seleccionaCredito(creditoSeleccionado);
		} else if(obj instanceof Account && ((Account) obj).getType().equals("IN")) {
			cuentaSeleccionada = (Account)obj;
			consultarInversion(((Account) obj).getFullNumber());
		} else {
			cuentaSeleccionada = (Account)obj;
			selectedClass = Account.class;
			misCuentasViewController.seleccionaCuenta(cuentaSeleccionada);
		}
	}

	public void consultarImportesPagoCredito(String numeroCredito) {
		//misCuentasViewController.muestraIndicadorActividad(misCuentasViewController.getString(R.string.alert_operation), misCuentasViewController.getString(R.string.alert_connecting));
		//misCuentasViewController.getParentViewsController().setActivityChanging(true);

		InitPagoCredito init = InitPagoCredito.getInstance(misCuentasViewController, new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		init.setCallBackSession(misCuentasViewController);
		init.setCallBackAPI(misCuentasViewController);
		init.configurar();
		//init.setPatrimonial(Session.getInstance(SuiteApp.appContext).isPatrimonial());
		init.consultarImportes(numeroCredito);
	}

	public void mostrarPagoCredito(ImportesPagoCreditoData importesPagoCreditoData) {
		InitPagoCredito init = InitPagoCredito.getInstance(misCuentasViewController, new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		misCuentasViewController.getParentViewsController().setActivityChanging(true);
		init.setCallBackSession(misCuentasViewController);
		init.setCallBackAPI(misCuentasViewController);

		Credito creditos = new Credito();
		creditos.setAdeudo(creditoSeleccionado.getAdeudo());
		creditos.setEstatusCredito(creditoSeleccionado.getEstatusCredito());
		creditos.setNumeroCredito(creditoSeleccionado.getNumeroCredito());
		creditos.setTipoCredito(creditoSeleccionado.getTipoCredito());

		init.configurar();
		init.setImportesAndCredito(importesPagoCreditoData, creditos);

		final Intent intent = new Intent(misCuentasViewController,
				PagarCreditoViewController.class);
		intent.putExtra("patrimonial",init.isPatrimonial());
		misCuentasViewController.startActivity(intent);
	}

	public void procesarClickCreditos(Creditos credito, String opcionSeleccionada){
		/**
		 * Inicializamos los estados posibles para los escenarios
		 */
		String statusVigente = "0";
		ArrayList<String> listaEstados = new ArrayList<String>();
		listaEstados.add("2");
		listaEstados.add("3");
		listaEstados.add("C");
		listaEstados.add("V");
		/**
		 *
		 */


		if(credito.getEstatusCredito().equalsIgnoreCase(statusVigente)){
			//EA#4
			// Llamada al modulo de Consulta Creditos
			Session session = Session.getInstance(SuiteApp.getInstance());
			// Inicializamos la clase de configuracion

			if (opcionSeleccionada.equals(Constants.OPERACION_OTROS_CREDITOS)) {

				misCuentasViewController.muestraIndicadorActividad(misCuentasViewController.getString(R.string.alert_operation), misCuentasViewController.getString(R.string.alert_connecting));
				((BmovilViewsController)misCuentasViewController.getParentViewsController()).setActivityChanging(true);

				InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(misCuentasViewController);//(misCuentasViewController,((BmovilViewsController)misCuentasViewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
				// Establecemos los parametros necesarios
				init.getConsultaSend().setIUM(session.getIum());
				init.getConsultaSend().setUsername(session.getUsername());
				init.getConsultaSend().setCallBackModule(misCuentasViewController);
				init.getConsultaSend().setCallBackSession(misCuentasViewController);
				init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);

				// Realizamos la llamada a consulta otros creditos
				//init.setPatrimonail(Session.getInstance(SuiteApp.appContext).isPatrimonial());
				init.preShowDetalleConsultarOtrosCreditos(credito);
			} else if (opcionSeleccionada.equals(Constants.OPERACION_PAGAR_OTROS_CREDITOS)) {

				consultarImportesPagoCredito(credito.getNumeroCredito());
			}
		}
		else if(listaEstados.contains(credito.getEstatusCredito())){
			//EA#5
			DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					misCuentasViewController.getMisCuentasOpciones().dismiss();
				}
			};

			if(credito.getEstatusCredito().equalsIgnoreCase("V")){
				if(credito.getTipoCredito().equalsIgnoreCase("C") || credito.getTipoCredito().equalsIgnoreCase("P") || credito.getTipoCredito().equalsIgnoreCase("N")){
					misCuentasViewController.showInformationAlert(misCuentasViewController.getString(R.string.label_error), misCuentasViewController.getString(R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusVC), onClickListener);
				}else if (credito.getTipoCredito().equalsIgnoreCase("H")){
					misCuentasViewController.showInformationAlert(misCuentasViewController.getString(R.string.label_error), misCuentasViewController.getString(R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusVH), onClickListener);
				}

			}else{
				if(credito.getTipoCredito().equalsIgnoreCase("H")){
					misCuentasViewController.showInformationAlert(misCuentasViewController.getString(R.string.label_error), misCuentasViewController.getString(R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusBH), onClickListener);
				}else if(credito.getTipoCredito().equalsIgnoreCase("C") || credito.getTipoCredito().equalsIgnoreCase("P") || credito.getTipoCredito().equalsIgnoreCase("N")){
					misCuentasViewController.showInformationAlert(misCuentasViewController.getString(R.string.label_error), misCuentasViewController.getString(R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusBC), onClickListener);
				}

			}
		}
	}
	
	public void comprobarRecortado(){
	//	Session session = Session.getInstance(SuiteApp.appContext);
	//	Constants.Perfil perfil = session.getClientProfile();
		
	//	if(Constants.Perfil.recortado.equals(perfil)){
				
				//	if(Constants.siEstatusAlertas.equals(session.getEstatusAlertas())){
						// tiene ALERTAS
						
						solicitarAlertas(misCuentasViewController);
			
//					} else {
//						// NO tiene ALERTAS
//						
//						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
//						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));
//						
//					}
			//	}
		
	}


	
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {

		if (misCuentasViewController instanceof MisCuentasViewController) {
			 ((BmovilViewsController)misCuentasViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller, true);
		}
		
	}
	
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			//* Show List Patrimonial
			if (operationId == Server.OP_PATRIMONIAL)
			{
				if(patrimonialOption.equals(misCuentasViewController.getString(R.string.bmovil_opcion_contratos)))
				{
					ContratosPatrimonial listaContratos = (ContratosPatrimonial) response.getResponse();
					Session.getInstance(misCuentasViewController).setListaContratos(listaContratos.getListaContratos());
					misCuentasViewController.dropList();
					System.out.println("Patrimonial Success");

				} /*else if (patrimonialOption.equals(misCuentasViewController.getString(R.string.bmovil_opcion_detalle_posicion)))
				{
					((BmovilViewsController)misCuentasViewController.getParentViewsController()).showDetallePosicionPatrimonial((DetallePosicionPatrimonial) response.getResponse());
				}
				*/

			}

			else if (operationId == Server.OP_SOLICITAR_ALERTAS) {


				SolicitarAlertasData validacionalertas = (SolicitarAlertasData) response.getResponse();
				if (Constants.ALERT02.equals(validacionalertas.getValidacionAlertas())){
					// 02 no tiene alertas
					SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
					if (opcionRetiroSinTarjeta){
						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_retiroSinTarjeta_recortado_sin_alertas));
						opcionRetiroSinTarjeta=false;
					}else {
						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));
					}
					
				} else{
				
					setSa(validacionalertas);
					analyzeAlertasRecortadoSinError();
				}
				return;
				
			}  else if(operationId == Server.OP_CONSULTA_OTROS_CREDITOS){

				ConsultaOtrosCreditosData consultaOtrosCreditosData = (ConsultaOtrosCreditosData) response.getResponse();

				setConsultaOtrosCreditosData(consultaOtrosCreditosData);

				if (! (SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController() instanceof MisCuentasViewController) ){
					((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showMisCuentasViewController();
				}else{
					procesaOtrosCreditos();
				}
			}
			
		}else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {} 
		
	else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
			misCuentasViewController.showInformationAlertEspecial(misCuentasViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
		}
		}
/*=======
	else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {}
		misCuentasViewController.showInformationAlertEspecial(misCuentasViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
	}
>>>>>>> MisCuentasDelegate.java

*/



	public void procesaOtrosCreditos(){
		ConsultaOtrosCreditosData ccData = getConsultaOtrosCreditosData();

		if((ccData != null) && (ccData.getListaCreditos() != null) && (!ccData.getListaCreditos().isEmpty())){
			ArrayList<Object> encabezado = new ArrayList<Object>();
			encabezado.add(null);
			encabezado.add(misCuentasViewController.getString(R.string.bmovil_menu_miscuentas_otroscreditos_tabla_titulo));

			List<Creditos> listaCreditos = ccData.getListaCreditos();
			ArrayList<Object> listaTmp = new ArrayList<Object>();

			Creditos credito = null;
			ArrayList<Object> registros;
			Double saldoActual;
			Double saldoTotal = 0.0;
			for (int i = 0; i < listaCreditos.size(); i++)
			{
				registros = new ArrayList<Object>();

				credito = listaCreditos.get(i);
				registros.add(credito);

				String creditName = credito.getNumeroCredito();
				if(credito.getTipoCredito().equalsIgnoreCase("H")){
					creditName = "Hipotecario";
				}else if(credito.getTipoCredito().equalsIgnoreCase("C")){
					creditName = "Consumo";
				}else if(credito.getTipoCredito().equalsIgnoreCase("P")){
					creditName = "Préstamo Personal";
				}else if(credito.getTipoCredito().equalsIgnoreCase("N")){
					creditName = "Nómina";
				}
				registros.add(creditName+"\n"+ Tools.hideAccountNumber(credito.getNumeroCredito()));

				saldoActual = Tools.getDoubleAmountFromServerString(credito.getAdeudo());
				Boolean neg = false;
				if(saldoActual < 0) neg = true;
				registros.add(Tools.formatAmount(credito.getAdeudo(), neg));

				listaTmp.add(registros);

				saldoTotal = saldoTotal + saldoActual;
			}
			String saldo = Tools.convertDoubleToBigDecimalAndReturnString(saldoTotal);
			encabezado.add(Tools.formatAmount(saldo,saldoTotal<0));

			misCuentasViewController.llenarEncabezados(encabezado, 2, listaTmp);
		}else{
			misCuentasViewController.ocultaTablaOtrosCreditos();
		}
	}


	public void mostrarInversion(ConsultaInversionesPlazoData inversionesPlazo) {
		InitConsultaInversiones init = InitConsultaInversiones.getInstance(misCuentasViewController, new bancomer.api.consultainversiones.io.BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		misCuentasViewController.getParentViewsController().setActivityChanging(true);
		init.setCallBackSession(misCuentasViewController);
		init.setCallBackAPI(misCuentasViewController);

		init.configurar(inversionesPlazo.getListaInversiones().get(0));
		init.setInversionesData(inversionesPlazo);

		final Intent intent = new Intent(misCuentasViewController,
				DetalleInversionViewController.class);
		misCuentasViewController.startActivity(intent);
	}

	public void mostrarMenuInversiones(ConsultaInversionesPlazoData inversionesPlazo){

		InitMenuInversiones init = InitMenuInversiones.getInstance(misCuentasViewController, new bancomer.api.consultainversiones.io.BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		misCuentasViewController.getParentViewsController().setActivityChanging(true);
		init.setCallBackSession(misCuentasViewController);
		init.setCallBackAPI(misCuentasViewController);
		init.setNumeroCuenta(inversionesPlazo.getNumeroCuenta());

		init.configurar(inversionesPlazo.getListaInversiones());

		final Intent intent = new Intent(misCuentasViewController,
				DetalleMenuInversionesViewController.class);
		misCuentasViewController.startActivity(intent);
	}


	public void consultarInversion(String fullNumber) {
		misCuentasViewController.muestraIndicadorActividad(misCuentasViewController.getString(R.string.alert_operation), misCuentasViewController.getString(R.string.alert_connecting));
		misCuentasViewController.getParentViewsController().setActivityChanging(true);

		InitConsultaInversiones init = InitConsultaInversiones.getInstance(misCuentasViewController, new bancomer.api.consultainversiones.io.BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		init.setCallBackSession(misCuentasViewController);
		init.setCallBackAPI(misCuentasViewController);
		init.configurar(null);

		init.consultarInversiones(fullNumber);
	}

	public Account getCuentaSeleccionada() {
		return cuentaSeleccionada;
	}

	@Override
	public boolean mostrarContrasenia() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.consultarCreditos,
				perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.consultarCreditos, perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.consultarCreditos,
				perfil);
		return value;
	}

	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public Constants.TipoOtpAutenticacion tokenAMostrar() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.consultarCreditos,
					perfil);
		} catch (Exception ex) {
			Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}
		return tipoOTP;
	}

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;

		fila = new ArrayList<String>();
		fila.add("Consulta");
		fila.add("Creditos Otorgados");
		tabla.add(fila);
		fila = new ArrayList<String>();
		fila.add("Fecha de operación");

		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		fila.add(formatter.format(date));

		tabla.add(fila);

		return tabla;
	}

	@Override
	public int getTextoEncabezado() {
		int resTitle;
		resTitle = R.string.mis_cuentas_title;
		return resTitle;
	}

	@Override
	public int getNombreImagenEncabezado() {
		int resIcon = R.drawable.bmovil_mis_cuentas_icono;
		return resIcon;
	}


	@Override
	public String getTextoTituloResultado() {
		int txtTitulo = R.string.transferir_detalle_operacion_exitosaTitle;
		return misCuentasViewController.getString(txtTitulo);
	}


	/**
	 * Invoca la operacion para hacer el cambio de cuenta asociada
	 */
	@Override
	public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
		confirmacionAutenticacionViewController.muestraIndicadorActividad(confirmacionAutenticacionViewController.getString(R.string.alert_operation), confirmacionAutenticacionViewController.getString(R.string.alert_connecting));

		// Llamada al modulo de Consulta Creditos
		Session session = Session.getInstance(SuiteApp.getInstance());
		// Inicializamos la clase de configuracion
		InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(confirmacionAutenticacionViewController);//(confirmacionAutenticacionViewController,((BmovilViewsController)confirmacionAutenticacionViewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
		// Establecemos los parametros necesarios
		init.getConsultaSend().setIUM(session.getIum());
		init.getConsultaSend().setUsername(session.getUsername());
		init.getConsultaSend().setCveAcceso(contrasenia==null?"":contrasenia);
		init.getConsultaSend().setTarjeta5Dig(campoTarjeta==null?"":campoTarjeta);
		init.getConsultaSend().setCodigoNip(nip==null?"":nip);
		init.getConsultaSend().setCodigoCvv2(cvv==null?"":cvv);
		init.getConsultaSend().setCodigoOtp(token==null?"":token);
		init.getConsultaSend().setCallBackModule(confirmacionAutenticacionViewController);
		init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultarCreditos, session.getClientProfile());
		init.getConsultaSend().setCadenaAutenticacion(cadenaAutenticacion);
		// Realizamos la peticion
		init.lookForCreditosData(confirmacionAutenticacionViewController);


	}


	public void consultaOtrosCreditos(MenuPrincipalViewController viewController){
		viewController.muestraIndicadorActividad(viewController.getString(R.string.alert_operation), viewController.getString(R.string.alert_connecting));

		// Llamada al modulo de Consulta Creditos
		Session session = Session.getInstance(SuiteApp.getInstance());
		// Inicializamos la clase de configuracion
		if (Server.ALLOW_LOG) Log.d("[CGI-Configuracion-Obligatorio] >> PRE -", ((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient().toString());
		InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(viewController);//(viewController, ((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
		// Establecemos los parametros necesarios
		init.getConsultaSend().setIUM(session.getIum());
		init.getConsultaSend().setUsername(session.getUsername());
		init.getConsultaSend().setCallBackModule(misCuentasViewController);
		init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultarCreditos, session.getClientProfile());
		init.getConsultaSend().setCadenaAutenticacion(cadenaAutenticacion);
		// Realizamos la peticion
		init.lookForCreditosData(viewController);
	}


	public void patrimonialServerRequest(String optionRequest, String numCuenta, String optionFlow)
	{
		patrimonialOption = optionFlow;
		/*Hashtable<String, String> params = new Hashtable<String, String>();
>>>>>>> BMovil_Desarrollo_Patrimmonial
		Session session = Session.getInstance(misCuentasViewController);
		params.put(ServerConstants.OPERACION, misCuentasViewController.getString(R.string.bmovil_patrimonial_operacion));
		params.put(ServerConstants.TELEFONO_TAG, session.getUsername());
		params.put(ServerConstants.IUM_TAG, session.getIum());
		params.put(ServerConstants.NUMCUENTA_TAG, numCuenta);
		params.put(ServerConstants.TIPOCONSULTA_TAG, optionRequest);
<<<<<<< HEAD

		if(patrimonialOption.equals(misCuentasViewController.getString(R.string.bmovil_opcion_contratos)))
			((BmovilViewsController)misCuentasViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.OP_PATRIMONIAL, params,true, new ContratosPatrimonial(), Server.isJsonValueCode.NONE, misCuentasViewController,false);
=======
		*/

		ParametersTO parametersTO = new ParametersTO();
		final Map<String, String> headers = new HashMap<String, String>();
		//headers.put(HTTP.CONTENT_TYPE, "application/json");
		headers.put("ium", Session.getInstance(getContext()).getIum().replace('"', ' ').trim());
		parametersTO.setHeaders(headers);
		parametersTO.setParameters(new Hashtable<String, String>());
		parametersTO.setMethodType(MethodType.GET);

		if(patrimonialOption.equals(misCuentasViewController.getString(R.string.bmovil_opcion_contratos)))
			((BmovilViewsController)misCuentasViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.OP_PATRIMONIAL, parametersTO,true, new ContratosPatrimonial(), Server.isJsonValueCode.NONE, misCuentasViewController,false);

		/*else if (patrimonialOption.equals(misCuentasViewController.getString(R.string.bmovil_opcion_detalle_posicion)))
			((BmovilViewsController)misCuentasViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.OP_PATRIMONIAL, params,true, new DetallePosicionPatrimonial(), Server.isJsonValueCode.NONE, misCuentasViewController,false);
			*/
	}


	public void DetallePatrimonial (String NumCuenta){
	ArrayList<ContratoPatrimonial>	Contratos = Session.getInstance(this.getContext()).getListaContratos();
		Iterator<ContratoPatrimonial> iterator = Contratos.iterator();
		while (iterator.hasNext()){
		ContratoPatrimonial contrato = iterator.next();
		if(contrato.getContratoB().equals(NumCuenta)){
			((BmovilViewsController) misCuentasViewController.getParentViewsController()).showDetallePosicionPatrimonial(contrato.getContrato());
			break;
		}


		}

	}

	public void mostrarVerCodigoQR(Account cuenta) {
		CodigoQRFacade.getInstance().configurar(misCuentasViewController, misCuentasViewController);
		misCuentasViewController.getParentViewsController().setActivityChanging(true);

		OperacionQR operacionQR = new OperacionQR(cuenta.getNumber(), Session.getInstance(SuiteApp.appContext).getNombreCliente(), cuenta.getType());

		CodigoQRFacade.getInstance().showVerCodigoQR(operacionQR);
	}

}
