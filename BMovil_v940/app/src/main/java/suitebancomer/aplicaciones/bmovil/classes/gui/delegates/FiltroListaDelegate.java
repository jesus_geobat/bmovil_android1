package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;
import java.util.Iterator;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.FiltroListaViewController;

public class FiltroListaDelegate
{
	
	ArrayList<Object> listaOriginal;
	FiltroListaViewController filtroLlistaviewcontroller;
	ArrayList<Object> nuevaLista;
	
	
	public FiltroListaViewController getFiltroListaviewcontroller() {
		return filtroLlistaviewcontroller;
	}

	public void setFiltroListaviewcontroller(FiltroListaViewController listaviewcontroller) {
		this.filtroLlistaviewcontroller = listaviewcontroller;
	}

	public ArrayList<Object> getListaOriginal() {
		return listaOriginal;
	}

	public void guardaCopiaOriginal(ArrayList<Object> listaOriginal)
	{
		this.listaOriginal = listaOriginal;
	}
	
	@SuppressWarnings("unchecked")
	public void realizaBusqueda(String str)
	{
		if (!str.equals("")) {
			str = str.toLowerCase();
			nuevaLista = new ArrayList<Object>();
			for (int i = 0; i < listaOriginal.size(); i++) {
				ArrayList<Object> registro = (ArrayList<Object>) listaOriginal.get(i);
				for (int j = 1; j < registro.size(); j++) {
//					String reg = ((String)registro.get(j)).toLowerCase();
					String reg = checkFilterTypes(registro.get(j));
					reg = reg.replace(",", "");
					if (reg.contains(str)) {
						if (!nuevaLista.contains(registro)) {
							nuevaLista.add(registro);
						}
					}
				}
			}
		}
		else {
			nuevaLista = listaOriginal;
		}
	}
	
	private String checkFilterTypes(Object registro){
		String ret = "";

		if(registro instanceof String){
			ret = ((String)registro).toLowerCase();
		}else if(registro instanceof ArrayList){
			ArrayList<Object> array = (ArrayList)registro;
			Iterator it = array.iterator();
			Object temporalItem = "";
			while(it.hasNext()){
				temporalItem = it.next();
				if(temporalItem instanceof String){
					ret = ret + ((String)temporalItem).toLowerCase();
				}
			}
		}

		return ret;
	}

	public void actualizaCampos()
	{
		filtroLlistaviewcontroller.getListaSeleccion().setLista(nuevaLista);
		filtroLlistaviewcontroller.getListaSeleccion().cargarTabla();		
	}

}
