package suitebancomer.aplicaciones.bmovil.classes.model;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by root on 07/07/16.
 */
public class GetListBanks implements  ParsingHandler {


    public String status;
    public String code;
    public String description;
    public String errorCode;
    private ArrayList<ListBanksCatalog> catalogos;

    private static GetListBanks ourInstance= new GetListBanks();

    public  static GetListBanks getInstance(){
        return  ourInstance;
    }

    public GetListBanks(){
    }

    public GetListBanks(final String status, final String code, final  String description){

    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public ArrayList<ListBanksCatalog> getListBanksCatalogs() {
        return catalogos;
    }

    public void setListBanksCatalogs(ArrayList<ListBanksCatalog> catalogs) {
        this.catalogos = catalogs;
    }

    @Override
    public void process(Parser parser) throws  IOException, ParsingException{

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException{
        try {
            String portability = parser.parseNextValue("response");
            code=parser.parserNextObject("status").getString("code");
            description =parser.parserNextObject("status").getString("description");

            final JsonObject json = (JsonObject) new JsonParser().parse(portability).getAsJsonObject();
            final JsonArray array = (JsonArray) json.get("catalogos");
            final Gson gson = new Gson();

            final ArrayList<ListBanksCatalog> listBanksCatalogs = new ArrayList<ListBanksCatalog>();
            final ArrayList<ListBanksCatalog> listBanksCatalogs2 = new ArrayList<ListBanksCatalog>();
            ListBanksCatalog getListBanksCatalog;
            final Iterator<JsonElement> iterator = array.iterator();

            while (iterator.hasNext()) {
                getListBanksCatalog = gson.fromJson((JsonObject) iterator.next(), ListBanksCatalog.class);
                listBanksCatalogs.add(getListBanksCatalog);
                listBanksCatalogs2.add(getListBanksCatalog);
            }

            Collections.sort(listBanksCatalogs2, new Comparator<ListBanksCatalog>() {
                @Override
                public int compare(ListBanksCatalog lhs, ListBanksCatalog rhs) {
                    return lhs.getNombreBanco().compareToIgnoreCase(rhs.getNombreBanco());

                }
            });

            for (int i = 0;i<listBanksCatalogs2.size();i++){
                for(int j =0;j<listBanksCatalogs.size();j++){
                    if(listBanksCatalogs2.get(i).getNombreBanco().equals(listBanksCatalogs.get(j).getNombreBanco())){
                        //ListBanksCatalog temp = listBanksCatalogs.get(i);
                        //listBanksCatalogs2.add(i,listBanksCatalogs.get(j));
                        listBanksCatalogs2.set(i,listBanksCatalogs.get(j));
                        //listBanksCatalogs.add(j,temp);
                    }
                }
            }
            catalogos = listBanksCatalogs2;
        } catch (JSONException e) {
            //e.printStackTrace();
        }finally {
            status= parser.parseNextValue("status");
            errorCode= parser.parseNextValue("errorCode");
            description= parser.parseNextValue("description");
        }
    }
}

