package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.bbvacredit.gui.delegates.BaseDelegate;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.classes.gui.controllers.MenuSuiteViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by alejandro.ruizgarcia on 16/11/2016.
 */
public class PopUpActivarTokenMovilDelegate extends BaseDelegate {

    public void showActivacionToken(){
        SuiteAppApi sftoken=new SuiteAppApi();

        ServerCommons.ALLOW_LOG= Server.ALLOW_LOG;
        ServerCommons.DEVELOPMENT=Server.DEVELOPMENT;
        ServerCommons.SIMULATION=Server.SIMULATION;
        sftoken.onCreate(SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());
        sftoken.setIntentToReturn(new MenuSuiteViewController());

        sftoken.initWithSession(SuiteApp.getInstance().getBmovilApplication().isApplicationLogged(), SuiteApp.getInstance().getMenuPrincipalViewController(), Session.getInstance(SuiteApp.appContext).getClientNumber(),Session.getInstance(SuiteApp.appContext).getUsername(),Session.getInstance(SuiteApp.appContext).getIum());
        sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());

        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
        sftoken.showViewController(suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.IngresoDatosSTViewController.class, 0, false, null, null, SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());

    }
}
