package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.os.Looper;
import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import org.apache.http.impl.client.DefaultHttpClient;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import bancomer.api.apitdcadicional.implementations.InitTDCAdicional;
import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.gui.delegates.MainViewHostDelegate;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.domiciliaciontdc.implementations.InitDomiciliacionTDC;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuPrincipalViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.BanqueroPersonal;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.aplicaciones.bmovil.classes.model.TextoPaperless;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;
import suitebancomercoms.classes.common.PropertiesManager;
import com.bancomer.mbanking.apipush.MainActivity;
import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;
import com.bancomer.oneclickseguros.implementations.InitOneClickSeguros;

//Analytics

public class MenuPrincipalDelegate extends BaseDelegate implements MainViewHostDelegate {
    private static final String CLAVE_COMP = "DFRTODSCamp";

    Promociones promocion;
    /**
     * El controlador actual de la pantalla.
     */
    private BaseViewController viewController;
    private String claveCampanaPaperless;

	/**
	 * Identificador del delegado.
	 */
	public static final long MENU_PRINCIPAL_DELEGATE_ID = 0xb35981680eb5fe19L;
	
	public MenuPrincipalDelegate(Promociones promocion){
		this.promocion= promocion;
	}
	
	/**
	 * @return El controlador de la pantalla.
	 */
	public BaseViewController getViewController() {
		return viewController;
	}

	/**
	 * @param viewController El controlador de la pantalla a establecer.
	 */
	public void setViewController(BaseViewController viewController) {
		this.viewController = viewController;
	}
	
	
	public void realizaOperacionILC(int idOperacion,BaseViewController baseViewController) {
		Session session = Session.getInstance(SuiteApp.appContext);
		String user = session.getUsername();
		String ium= session.getIum();		
		Hashtable<String, String> params = new Hashtable<String, String>();
      //  params.put("operacion", "cDetalleOfertaBMovil");
		params.put("numeroCelular", user);
		params.put("cveCamp", promocion.getCveCamp());
		params.put("IUM", ium);
		
        doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA, params,true,new OfertaILC(), Server.isJsonValueCode.ONECLICK,
                baseViewController);
		
		
	}
    //JACT
    public void initVentaTDC(Promociones promocion)
    {
        DelegateBaseOperacion delegate = new DelegateBaseOperacion();
        Promociones data = new Promociones();
        data.setCveCamp(promocion.getCveCamp());
        data.setDesOferta(promocion.getCveCamp());
        data.setMonto(promocion.getMonto());
        data.setCarrusel("");
        DefaultHttpClient client = SuiteApp.getInstance().getBmovilApplication().getServer().getClienteHttp().getClient();
        client.setCookieStore(suitebancomer.aplicaciones.commservice.httpcomm.HttpInvoker.getCookieStore());
		InitVentaTDC init = InitVentaTDC.getInstance(viewController, data, delegate, Session.getInstance(viewController),Autenticacion.getInstance(), SuiteApp.getSofttokenStatus(), Tools.cuentaOneClick().getNumber(),client,this,Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
        viewController.getParentViewsController().setActivityChanging(true);
        init.startVentaTDC();
        
        
    }
    
    
    public void initTDCAdicional(Promociones promocion)
    {
        DelegateBaseOperacion delegate = new DelegateBaseOperacion();
        Promociones data = new Promociones();
        data.setCveCamp(promocion.getCveCamp());
        data.setDesOferta(promocion.getCveCamp());
        data.setMonto(promocion.getMonto());
        data.setCarrusel("");
        DefaultHttpClient client = SuiteApp.getInstance().getBmovilApplication().getServer().getClienteHttp().getClient();
        client.setCookieStore(suitebancomer.aplicaciones.commservice.httpcomm.HttpInvoker.getCookieStore());
		InitTDCAdicional init = InitTDCAdicional.getInstance(viewController, data, delegate, Session.getInstance(viewController), Autenticacion.getInstance(), SuiteApp.getSofttokenStatus(),client,this,Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
        viewController.getParentViewsController().setActivityChanging(true);
        init.startAdicional();
        
        
    }

	public void initOneClickSeguros(Promociones promocion)
	{
		DelegateBaseOperacion delegate = new DelegateBaseOperacion();
		Promociones data = new Promociones();
		data.setCveCamp(promocion.getCveCamp());
		data.setDesOferta(promocion.getCveCamp());
		data.setMonto(promocion.getMonto());
		data.setCarrusel("");
		DefaultHttpClient client = SuiteApp.getInstance().getBmovilApplication().getServer().getClienteHttp().getClient();
		client.setCookieStore(suitebancomer.aplicaciones.commservice.httpcomm.HttpInvoker.getCookieStore());
		InitOneClickSeguros init = InitOneClickSeguros.getInstance(viewController, data, delegate, Session.getInstance(viewController), Autenticacion.getInstance(), SuiteApp.getSofttokenStatus(),client,this, Session.getInstance(viewController).getAccountsOneClick(), Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG, SuiteApp.appContext);
		viewController.getParentViewsController().setActivityChanging(true);
		init.startSeguros();
	}

	public void initOneClickILC(Promociones promocion){
		DelegateBaseOperacion delegate = new DelegateBaseOperacion();
		Promociones data = new Promociones();
		data.setCveCamp(promocion.getCveCamp());
		data.setDesOferta(promocion.getDesOferta());
		data.setMonto(promocion.getMonto());
		data.setCarrusel(promocion.getCarrusel());
		DefaultHttpClient client = SuiteApp.getInstance().getBmovilApplication().getServer().getClienteHttp().getClient();
		client.setCookieStore(suitebancomer.aplicaciones.commservice.httpcomm.HttpInvoker.getCookieStore());
		InitILC init = InitILC.getInstance(viewController,data,delegate,Session.getInstance(viewController), Autenticacion.getInstance(), SuiteApp.getSofttokenStatus(),client,this, Session.getInstance(viewController).getAccounts(), Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
		viewController.getParentViewsController().setActivityChanging(true);
		init.ejecutaAPIILC();
	}

	public void initOneClickConsumo(Promociones promocion){
			DelegateBaseOperacion delegate = new DelegateBaseOperacion();
		Promociones data = new Promociones();
		data.setCveCamp(promocion.getCveCamp());
		data.setDesOferta(promocion.getDesOferta());
		data.setMonto(promocion.getMonto());
		data.setCarrusel(promocion.getCarrusel());
		DefaultHttpClient client = SuiteApp.getInstance().getBmovilApplication().getServer().getClienteHttp().getClient();
		client.setCookieStore(suitebancomer.aplicaciones.commservice.httpcomm.HttpInvoker.getCookieStore());
		InitConsumo init = InitConsumo.getInstance(viewController,data,delegate,Session.getInstance(viewController), Autenticacion.getInstance(), SuiteApp.getSofttokenStatus(),client,this, false, Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
		viewController.getParentViewsController().setActivityChanging(true);
		init.ejecutaAPIConsumo();
	}


	public void initDomiciliacionTDC(Promociones promocion)
    {
        //Add lines for start DomiciliacionTDC Flow.
        
        DelegateBaseOperacion delegate = new DelegateBaseOperacion();
        Promociones data = new Promociones();
        data.setCveCamp(promocion.getCveCamp());
        data.setDesOferta(promocion.getCveCamp());
        data.setMonto(""); //This campaign has not amount
        data.setCarrusel(""); //Carrousel is not necessary for this campaign
        DefaultHttpClient client = SuiteApp.getInstance().getBmovilApplication().getServer().getClienteHttp().getClient();
        client.setCookieStore(suitebancomer.aplicaciones.commservice.httpcomm.HttpInvoker.getCookieStore());
		InitDomiciliacionTDC init = InitDomiciliacionTDC.getInstance(viewController, data, delegate, Session.getInstance(viewController), Autenticacion.getInstance(), SuiteApp.getSofttokenStatus(), client, this, Session.getInstance(viewController).getAccountsOneClick(), Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);
		viewController.getParentViewsController().setActivityChanging(true);
        init.ejecutaAPIDomiciliacion();
        
    }

	private void initOneClickInversionLiquida(Promociones promocion)
	{
		DelegateBaseOperacion delegate = new DelegateBaseOperacion();
		Promociones data = new Promociones();
		data.setCveCamp(promocion.getCveCamp());
		data.setDesOferta(promocion.getCveCamp());
		data.setMonto(promocion.getMonto());
		data.setCarrusel("");
		DefaultHttpClient client = SuiteApp.getInstance().getBmovilApplication().getServer().getClienteHttp().getClient();
		client.setCookieStore(suitebancomer.aplicaciones.commservice.httpcomm.HttpInvoker.getCookieStore());
		InitOneClickIL init = InitOneClickIL.getInstance(viewController, data, delegate, Session.getInstance(viewController), Autenticacion.getInstance(), SuiteApp.getSofttokenStatus(), client, this, Session.getInstance(viewController).getAccountsOneClick(), Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG, SuiteApp.appContext);
		viewController.getParentViewsController().setActivityChanging(true);
		init.startInvLiq();
	}
	
	public void realizaOperacionEFI(int idOperacion,BaseViewController baseViewController) {
		Session session = Session.getInstance(SuiteApp.appContext);
		String user = session.getUsername();
		String ium= session.getIum();		
		Hashtable<String, String> params = new Hashtable<String, String>();
       // params.put("operacion", "cDetalleOfertaBMovil");
		params.put("numeroCelular",user );
		params.put("cveCamp",promocion.getCveCamp());
		params.put("IUM", ium);
		
        doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA_EFI, params,true,new OfertaEFI(), Server.isJsonValueCode.ONECLICK,
				baseViewController);
		
		
	}
	
	public void realizaOperacionCONSUMO(int idOperacion,BaseViewController baseViewController) {
		Session session = Session.getInstance(SuiteApp.appContext);
		String user = session.getUsername();
		String ium= session.getIum();		
		Hashtable<String, String> params = new Hashtable<String, String>();
       // params.put("operacion", "detalleConsumoBMovil");
		params.put("numeroCelular",user );
		params.put("claveCamp",promocion.getCveCamp());
		params.put("IUM", ium);
		params.put("importePar","000");
		/*if(!session.getImporteParcial().equals("")){
			params.put("importePar",session.getImporteParcial());
			Log.w("montos","importePar: "+session.getImporteParcial());

		} else {
			params.put("importePar","000");
		}*/

        //JAIG
        doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA_CONSUMO, params,true, new OfertaConsumo(), Server.isJsonValueCode.CONSUMO,
                baseViewController);
	}
	//Paperless

	public void realizaOperacionconsultaTextoPaperless(int idOperacion,BaseViewController baseViewController) {
		Session session = Session.getInstance(SuiteApp.appContext);

		String user = session.getUsername();
		String ium= session.getIum();

		Hashtable<String, String> params = new Hashtable<String, String>();
        //params.put(ServerConstants.OPERACION, "consultaTextoPaperless" );
		params.put(ServerConstants.NUMERO_CELULAR_TP, user);
		params.put(ServerConstants.IUM_TP, ium);

        doNetworkOperation(Server.CONSULTAR_TEXTO_PAPERLESS, params,true, new TextoPaperless(), Server.isJsonValueCode.PAPERLESS,
				baseViewController);

		baseViewController.muestraIndicadorActividad("",
				baseViewController.getString(R.string.alert_operation));

	}


	public void prepararTextoPaperless(String claveCampana){
		claveCampanaPaperless = claveCampana;
	    realizaOperacionconsultaTextoPaperless(Server.CONSULTAR_TEXTO_PAPERLESS, viewController);
	}
	//Termina Paperless

	@Override
    public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler, Server.isJsonValueCode isJsonValueCode, BaseViewController caller) {
		String camp = MainActivity.getIdCampania();
		if( viewController != null)
			if (camp!=null) {
				MainActivity.setIdCampania(null);
				Looper.prepare();
				TimerController timerContr = TimerController.getInstance();
				timerContr.setSessionCloser(SuiteApp.getInstance().getBmovilApplication());
				timerContr.initTimer(SuiteApp.appContext, suitebancomer.classes.gui.controllers.MenuSuiteViewController.class);
				timerContr.resetTimer();
				((BmovilViewsController) viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode, caller, false);
				Looper.loop();
			} else {
				((BmovilViewsController) viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode, caller, false);
			}
	}
	
	public void analyzeResponse(int operationId, ServerResponse response) {
		if(operationId==Server.CONSULTA_DETALLE_OFERTA){
		OfertaILC ofertaILC= (OfertaILC)response.getResponse();
		((BmovilViewsController)viewController.getParentViewsController()).showDetalleILC(ofertaILC, promocion);
		}else if(operationId==Server.CONSULTA_DETALLE_OFERTA_EFI){
			OfertaEFI ofertaEFI= (OfertaEFI)response.getResponse();
			((BmovilViewsController)viewController.getParentViewsController()).showDetalleEFI(ofertaEFI, promocion);
			
		}else if(operationId==Server.CONSULTA_DETALLE_OFERTA_CONSUMO){
			OfertaConsumo ofertaConsumo= (OfertaConsumo)response.getResponse();
			((BmovilViewsController)viewController.getParentViewsController()).showDetalleConsumo(ofertaConsumo, promocion);

		}else if(operationId==Server.CONSULTAR_TEXTO_PAPERLESS){
			TextoPaperless textoPaperless = (TextoPaperless)response.getResponse();
			viewController.ocultaIndicadorActividad();
			((BmovilViewsController)viewController.getParentViewsController()).showTextoPaperless(textoPaperless,claveCampanaPaperless);
		}else if (operationId==Server.OP_CONSULTA_GESTOR_REMOTO){
			BanqueroPersonal banqueroPersonal = (BanqueroPersonal)response.getResponse();
			BanqueroPersonal.getInstance().setNombreGestor(banqueroPersonal.getNombreGestor());
			BanqueroPersonal.getInstance().setTelefono1(banqueroPersonal.getTelefono1());
			BanqueroPersonal.getInstance().setExtension1(banqueroPersonal.getExtension1());
			BanqueroPersonal.getInstance().setExtension2(banqueroPersonal.getExtension2());
			BanqueroPersonal.getInstance().setEmail(banqueroPersonal.getEmail());
		}
		
	}
	
	public void promocionesselected(View v){
		//ARR
				Map<String, Object> eventoMenu = new HashMap<String, Object>();
		int a=v.getId();
		Session session = Session.getInstance(SuiteApp.appContext);
		Promociones[] promociones=session.getPromociones();
		for(int x=0;x< promociones.length;x ++){
			if(a==x){
				promocion= new Promociones();
				promocion= promociones[x];
				break;
			}
		}
		String cveCamp= promocion.getCveCamp().substring(0,4);
		if(cveCamp.equals("0130")){
			initOneClickILC(promocion);
		}else if(cveCamp.equals("0377")){
			//ARR
			eventoMenu.put("evento_menu", "event27");
			eventoMenu.put("eVar27", "detalleEfi");
			realizaOperacionEFI(Server.CONSULTA_DETALLE_OFERTA_EFI, viewController);
		}else if(cveCamp.equals("0060")){
//			realizaOperacionCONSUMO(Server.CONSULTA_DETALLE_OFERTA_CONSUMO, viewController);
			initOneClickConsumo(promocion);
		}else if(cveCamp.equals("0296")){
			initVentaTDC(promocion);
		}else if(cveCamp.equals("0485")){
			initTDCAdicional(promocion);
		}else if(cveCamp.equals("0489")){
			initDomiciliacionTDC(promocion);
		}else if(cveCamp.equals("0133")){
			initOneClickSeguros(promocion);
		}else if(cveCamp.equals("0558")){
			((MenuPrincipalViewController)viewController).contratacionBanqueroPersonal();
            saveClavComp(getViewController(),promocion.getCveCamp());
		}else if(cveCamp.equals("0572")){
			initOneClickInversionLiquida(promocion);
		}

	}

    //para pruebas dinamicas de clave comp ****
    public static String getStoreClavComp(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(CLAVE_COMP, "0558CAM0095550001");
    }

    public static void saveClavComp(Context context, String numCam) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(CLAVE_COMP, numCam)
                .apply();

    }
    ///*****

    public void restoreMenuPrincipal() {
        ((MenuPrincipalViewController) viewController).restoreMenu();

	}


	/**
	 * Funcion para evaluar si debe mostrar la opcion alertas por el perfil
	 * @return true si el perfil es avanzado, falso EOC
	 */
	public boolean muestraOpcionAlertas(){
		// Inicializacion de variable
		boolean muestra = false;
		// Obtener perfil de cliente de la sesion
		Session sesion = Session.getInstance(SuiteApp.appContext);
		Constants.Perfil perfil = sesion.getClientProfile();
		// Si el perfil de cliente es avanzado se debe mostrar el boton
		if(perfil.equals(Constants.Perfil.avanzado)){
			muestra = true;
		}
		if(Server.ALLOW_LOG) Log.d("[CGI-Opcion-Alertas]>> ", "[MenuPrincipaldelgate] Muestras opcion alertas ->  Muestra alertas " + muestra + " perfil " + perfil);
		return muestra;
	}
	public void consultaOtrosCreditos(){
		viewController.muestraIndicadorActividad(viewController.getString(R.string.alert_operation), viewController.getString(R.string.alert_connecting));

		// Llamada al modulo de Consulta Creditos
		Session session = Session.getInstance(SuiteApp.getInstance());
		// Inicializamos la clase de configuracion
		//Log.d("[CGI-Configuracion-Obligatorio] >> PRE -", ((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient().toString());
		//InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(viewController, ((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
		// Establecemos los parametros necesarios
		InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(viewController);
		init.getConsultaSend().setIUM(session.getIum());
		init.getConsultaSend().setUsername(session.getUsername());
		init.getConsultaSend().setCallBackModule((MenuPrincipalViewController)viewController);
		init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultarCreditos, session.getClientProfile());
		init.getConsultaSend().setCadenaAutenticacion(cadenaAutenticacion);
		// Realizamos la peticion
		init.lookForCreditosData(viewController);


	}

	@Override
	public void updateActivityChangingState() {
		viewController.getParentViewsController().setActivityChanging(false);
	}
}
