package suitebancomer.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bancomer.mbanking.R;

import java.util.ArrayList;

/**
 * Created by OOROZCO on 3/23/16.
 */
public class DetalleDePosicionPatrimonialAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> tags;
    private ArrayList<String> cantidades;


    public DetalleDePosicionPatrimonialAdapter(Context context, ArrayList<String> tags, ArrayList<String> cantidades)
    {
        this.context = context;
        this.tags = tags;
        this.cantidades = cantidades;

    }


    @Override
    public int getCount() {
        return tags.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        if(convertView == null)
            convertView = inflater.inflate(R.layout.list_item_detalle_posicion_patrimonial, null);

        TextView celda1 = (TextView) convertView.findViewById(R.id.txtTag);
        TextView celda2 = (TextView) convertView.findViewById(R.id.txtCantidad);

        Typeface light = Typeface.createFromAsset(context.getAssets(), "fonts/stag_sans_light.otf");
        celda1.setText(tags.get(position));
        celda1.setTypeface(light);
        celda2.setText(cantidades.get(position));
        celda2.setTypeface(light);
        ((TextView)convertView.findViewById(R.id.text_view_symbol)).setTypeface(light);

        return convertView;
    }
}
