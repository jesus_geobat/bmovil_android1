package suitebancomer.aplicaciones.bmovil.classes.common;

/**
 * Created by miguel.vicentelinare on 23/09/2015.
 */
public class ErrorData {

    private String errorTitle;
    private String errorCode;
    private String errorDescription;

    public ErrorData(String errorTitle, String errorCode, String errorDescription) {
        this.errorTitle = errorTitle;
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    public String getErrorTitle() {
        return errorTitle;
    }

    public void setErrorTitle(String errorTitle) {
        this.errorTitle = errorTitle;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
