package suitebancomer.aplicaciones.bmovil.classes.model;

public class ConfigurarCorreo {
	/**
	 * Nuevo correo del cliente.
	 */
	private String nuevoCorreo;
	
	/**
	 * @return Nuevo correo del cliente.
	 */
	public String getNuevoCorreo() {
		return nuevoCorreo;
	}

	/**
	 * @param nuevoCorreo Nuevo correo del cliente.
	 */
	public void setNuevoCorreo(String nuevoCorreo) {
		this.nuevoCorreo = nuevoCorreo;
	}
	
	public ConfigurarCorreo() {
		nuevoCorreo = null;
	}
	
	public ConfigurarCorreo(String nuevoCorreo) {
		this.nuevoCorreo = nuevoCorreo;
	}
}
