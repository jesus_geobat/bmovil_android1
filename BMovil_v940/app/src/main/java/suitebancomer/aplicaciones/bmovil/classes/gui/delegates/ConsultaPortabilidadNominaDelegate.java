package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;
import android.content.DialogInterface;

import com.bancomer.mbanking.SuiteApp;
import com.google.gson.Gson;

import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultaPortabilidadNominaViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.DetallePortability;
import suitebancomer.aplicaciones.bmovil.classes.model.GetListCuenta;
import suitebancomer.aplicaciones.bmovil.classes.model.ListPortability;
import suitebancomer.aplicaciones.bmovil.classes.model.ListaConsultaPortabilidad;
import suitebancomer.aplicaciones.commservice.commons.BodyType;
import suitebancomer.aplicaciones.commservice.commons.MethodType;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.commons.PojoGeneral;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by elunag on 06/07/16.
 */
public class ConsultaPortabilidadNominaDelegate extends DelegateBaseAutenticacion
{
    public final static long CONSULTA_PORTABILIDAD_NOMINA_DELEGATE_ID = 0xf6e588275b9a27b9L;
    public boolean detallePortabilidad = Boolean.FALSE;
    private ConsultaPortabilidadNominaViewController consultaPortabilidadNominaViewController;

    /**
     * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la
     * lista es ordenada seg�n sea reguerido.
     *
     * @return
     */
    public ArrayList<Account> cargaCuentasOrigen()
    {
        Constants.Perfil profile = Session.getInstance(SuiteApp.appContext)
                .getClientProfile();
        ArrayList<Account> accountsArray = new ArrayList<Account>();
        Account[] accounts = Session.getInstance(SuiteApp.appContext)
                .getAccounts();

        if (profile == Constants.Perfil.avanzado)
        {
            for(Account acc : accounts) {
                if (acc.getType().equals(Constants.LIBRETON_TYPE) || acc.getType().equals(Constants.SAVINGS_TYPE) || acc.getType().equals(Constants.CHECK_TYPE)) {
                    accountsArray.add(acc);
                }
            }
        }
        return accountsArray;
    }

    public void realizaOperacionGetList()
    {
        ParametersTO parametersTO = new ParametersTO();
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put(HTTP.CONTENT_TYPE, "application/json");
        headers.put("ium",Session.getInstance(consultaPortabilidadNominaViewController).getIum());
        parametersTO.setHeaders(headers);

        parametersTO.setParameters(new Hashtable<String, String>());
        parametersTO.setMethodType(MethodType.POST);
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        int operacion = 0;
        operacion = Server.CONSULTA_GET_LIST_PORTABILIDAD;
        paramTable.put("referenceNumber", ConsultaPortabilidadNominaViewController.componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getNumber());
        paramTable.put("type", "R");
        paramTable.put("nextPaginationKey", "1");
        //paramTable.put("ium", Session.getInstance(consultaPortabilidadNominaViewController).getIum());
        String json = "";
        final Gson gson = new Gson();
        json = gson.toJson(paramTable);
        parametersTO.setBodyRaw(json);
        parametersTO.setBodyType(BodyType.RAW);
        parametersTO.setForceArq(true);
        parametersTO.setAddCadena(false);
        doNetworkOperation(operacion, parametersTO, true, new PojoGeneral(), Server.isJsonValueCode.NONE, consultaPortabilidadNominaViewController);

    }

    public void realizaOperacionGetPortability(final String referenceNumber,String bankNumber,final String createdReferenceNumber,final String companyNumber)
    {
        ParametersTO parametersTO = new ParametersTO();
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put(HTTP.CONTENT_TYPE, "application/json");
        headers.put("ium",Session.getInstance(consultaPortabilidadNominaViewController).getIum());
        parametersTO.setHeaders(headers);
        parametersTO.setParameters(new Hashtable<String, String>());
        parametersTO.setMethodType(MethodType.POST);
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        int operacion = 0;
        operacion = Server.GET_PORTABILIDAD;
        paramTable.put("bankNumber",bankNumber);
        paramTable.put("createdReferenceNumber",createdReferenceNumber);
        paramTable.put("companyNumber", companyNumber);
        paramTable.put("referenceNumber", referenceNumber);
        //paramTable.put("ium", Session.getInstance(consultaPortabilidadNominaViewController).getIum());
        String json = "";
        final Gson gson = new Gson();
        json = gson.toJson(paramTable);
        parametersTO.setBodyRaw(json);
        parametersTO.setBodyType(BodyType.RAW);
        parametersTO.setForceArq(true);
        parametersTO.setAddCadena(false);
        doNetworkOperation(operacion, parametersTO, true, new PojoGeneral(), Server.isJsonValueCode.NONE, consultaPortabilidadNominaViewController);

    }

    public void doNetworkOperation(final int operationId, final ParametersTO params, final boolean isJson,
                                   final Object handler, final Server.isJsonValueCode isJsonValueCode,
                                   final BaseViewController caller) {
        ((BmovilViewsController)caller.getParentViewsController()).getBmovilApp()
                .invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode,
                        caller, true);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if(operationId == Server.CONSULTA_GET_LIST_PORTABILIDAD)
        {
            GetListCuenta getListCuenta = GetListCuenta.getInstance();
            try {
                getListCuenta.process(new ParserJSON(response.getResponsePlain()));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParsingException e) {
                e.printStackTrace();
            }
            if(getListCuenta.getPortabilities()!= null && getListCuenta.getCode().equalsIgnoreCase(response.CODE_OK))
            {
                //consultaPortabilidadNominaViewController.showInformationAlert("Muestra Lista de portabilities");
                consultaPortabilidadNominaViewController.cargaListaSeleccionComponent();
            }
            else if ((getListCuenta.getCode() == null || getListCuenta.getCode() != null) && getListCuenta.getDescription() != null){
                consultaPortabilidadNominaViewController.showInformationAlert(getListCuenta.getDescription(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                dialog.dismiss();
                                consultaPortabilidadNominaViewController.finish();
                            }


                        }
                );
            }
            else if (null == getListCuenta.getPortabilities() && getListCuenta.getCode() != null){
                consultaPortabilidadNominaViewController.showInformationAlert("No tienes ninguna Portabilidad", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                dialog.dismiss();
                                consultaPortabilidadNominaViewController.finish();
                            }


                        }
                );
            }
            else {
                consultaPortabilidadNominaViewController.finish();
                ((BmovilViewsController)consultaPortabilidadNominaViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert("Servicio no disponible", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                dialog.dismiss();
                            }
                        }
                );
            }

        }
        if (operationId == Server.GET_PORTABILIDAD)
        {
            DetallePortability detallePortability = DetallePortability.getInstance();
            try {
                detallePortability.process(new ParserJSON(response.getResponsePlain()));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParsingException e) {
                e.printStackTrace();
            }
            if(detallePortability.getCode()!= null && detallePortability.getCode().equalsIgnoreCase(response.CODE_OK))
            {
                ((BmovilViewsController)consultaPortabilidadNominaViewController.getParentViewsController()).showDetallePortability();
            }else if (detallePortability.getDescription() != null){
                consultaPortabilidadNominaViewController.showInformationAlert(detallePortability.getDescription(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                dialog.dismiss();
                                consultaPortabilidadNominaViewController.finish();
                            }


                        }
                );
            }
            else {
                consultaPortabilidadNominaViewController.showInformationAlert("Servicio no disponible", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                dialog.dismiss();
                            }
                        }
                );
            }
        }
    }

    public void setConsultaPortabilidadNominaViewController(ConsultaPortabilidadNominaViewController consultaPortabilidadNominaViewController) {
        this.consultaPortabilidadNominaViewController = consultaPortabilidadNominaViewController;
    }

    public ArrayList<Object> getDatosTablaPortability() {

        ArrayList<Object> listaDatos = new ArrayList<Object>();
        ArrayList<ListPortability> listPortabilities = new ArrayList<ListPortability>();
        listPortabilities = GetListCuenta.getInstance().getPortabilities();
        ListaConsultaPortabilidad lp;
        ArrayList<Object> registro;

        for (int i = 0; i < listPortabilities.size(); i++)
        {
            lp = new ListaConsultaPortabilidad();
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
            Date fecha = null;
            try {
                fecha = formatoDelTexto.parse(listPortabilities.get(i).getCreatedDate().toString());
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            lp.setFecha((String) android.text.format.DateFormat.format("dd/MM", fecha));
            lp.setNombreBanco(listPortabilities.get(i).getExternalBank().getName());
             if (listPortabilities.get(i).getStatus().equals("R")){
            lp.setStatus("Rechazada");
            }
             else if (listPortabilities.get(i).getStatus().equals("P")) {
                lp.setStatus("Pendiente");
            }
            else if (listPortabilities.get(i).getStatus().equals("A")){
                lp.setStatus("Alta");
            }
            else if (listPortabilities.get(i).getStatus().equals("E")){
                lp.setStatus("Enviada");
            }
            else if (listPortabilities.get(i).getStatus().equals("B")){
                lp.setStatus("Baja");
            }
            else if (listPortabilities.get(i).getStatus().equals("C")){
                lp.setStatus("Cancelada");
            }
            registro = new ArrayList<Object>();
            registro.add("");
            registro.add(lp.getFecha());
            registro.add(lp.getNombreBanco());
            registro.add(lp.getStatus());
            listaDatos.add(registro);
        }

        return listaDatos;
    }

    @Override
    public void performAction(Object obj) {
       if (obj instanceof Account)
       {
           realizaOperacionGetList();
       }
       else {
           consultaPortabilidadNominaViewController.opcionSeleccionada(consultaPortabilidadNominaViewController.listaSeleccion.getOpcionSeleccionada());
       }

    }
}
