package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ClabeCuentaViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultarClabeData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovilData;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class ClabeCuentaDelegate extends DelegateBaseAutenticacion {
	/**
	 * Identificador del delegado.
	 */
	public static final long CLABE_CUENTA_DELEGATE_ID = 0x0abc8cabc5b0b3d4L;//4

	private Account cuentaOrigen;
	ClabeCuentaViewController clabeCuentaViewController;

	/**
	 * El controlador actual de la pantalla.
	 */
	private BaseViewController viewController;


	private Integer currentOperation;

	/**
	 * @return El controlador de la pantalla.
	 */
	public BaseViewController getViewController() {
		return viewController;
	}

	public void setCuentaOrigen(Account cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}
	public Account getCuentaOrigen() {
		return cuentaOrigen;
	}
	/**
	 * @param viewController El controlador de la pantalla a establecer.
	 */
	public void setViewController(BaseViewController viewController) {
		this.viewController = viewController;
	}


	/**
	 * Constructor por defecto
	 */
	public ClabeCuentaDelegate() {

		//transferencia = new TransferenciaDineroMovil();
	}

	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada seg�n sea reguerido.
	 * @return Lista de cuentas para el componente CuentaOrigen.
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();

		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();

		if(profile == Perfil.avanzado) {
			for(Account acc : accounts) {
				if(acc.isVisible()) {
					accountsArray.add(acc);
					break;
				}
			}
			for(Account acc : accounts) {
				if(!acc.isVisible())
					accountsArray.add(acc);
			}
		} else {
			for(Account acc : accounts) {
				if(acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE))
					accountsArray.add(acc);
			}
		}

		return accountsArray;
	}


	public void consultarClabe() {

			if (!getCuentaOrigen().getType().equals("TC")) {
				Hashtable<String, String> paramTable = new Hashtable<String, String>();
				Session session = Session.getInstance(viewController);
				paramTable.put("numeroTelefono", session.getUsername());
				paramTable.put("IUM", session.getIum());
				paramTable.put("numeroTarjeta", "");
				paramTable.put("numeroCuenta", getCuentaOrigen().getType() + getCuentaOrigen().getNumber());
				viewController.muestraIndicadorActividad("Operación en curso", "Contectando con servidor remoto");
				this.doNetworkOperation(Server.OP_CONSULTAR_CLABE_OPERACION, paramTable, true, new ConsultarClabeData(), Server.isjsonvalueCode.NONE, getViewController());
			} else {
				ClabeCuentaViewController claview = (ClabeCuentaViewController) viewController;
				claview.mostrarDatos("", "", getCuentaOrigen().getNumber(), getCuentaOrigen().getType());
			}
		}



	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if( viewController != null)
			setCurrentOperation(operationId);
			((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
	}

	public void setCurrentOperation(Integer currentOperation) {
		this.currentOperation = currentOperation;
	}

	/**
	 * La respuesta del servidor.
	 */
	private TransferenciaDineroMovilData serverResponse;

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
			ClabeCuentaViewController claview = (ClabeCuentaViewController) viewController;
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				viewController.ocultaIndicadorActividad();
				ConsultarClabeData ccd = (ConsultarClabeData) response.getResponse();
				claview.mostrarDatos(ccd.getCuentaClabe(), getCuentaOrigen().getNumber(), ccd.getTarjeta(), getCuentaOrigen().getType());
			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				viewController.ocultaIndicadorActividad();
				claview.ocultarDatos();
				BaseViewController current = ((BmovilViewsController) viewController.getParentViewsController()).getCurrentViewControllerApp();
				viewController.showInformationAlert(response.getMessageText(), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						((BmovilViewsController) viewController.getParentViewsController()).showMenuPrincipal();
					}
				});
			}
		}

	public void showMenu(){
		((BmovilViewsController)viewController.getParentViewsController()).showMenuPrincipal();
	}

	@Override
	public void performAction(Object obj) {
		if (viewController instanceof ClabeCuentaViewController) {
			((ClabeCuentaViewController)viewController).reactivarCtaOrigen((Account) obj);
		}
	}
}



