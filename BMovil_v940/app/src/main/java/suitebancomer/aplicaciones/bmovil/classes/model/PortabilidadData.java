package suitebancomer.aplicaciones.bmovil.classes.model;

import org.json.JSONException;

import java.io.IOException;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by aogarciar on 28/06/2016.
 */
public class PortabilidadData implements ParsingHandler {

    public String code;
    public String description;
    public String errorCode;
    public String operationReferenceNumber;
    public String createdDate;
    public String operationTime;
    public String referenceNumber;
    public String createdReferenceNumber;
    public String companyNumber;
    public int positionBank;

    private static PortabilidadData ourInstance = new PortabilidadData();

    public static void renewInstance() {
        ourInstance = new PortabilidadData();
    }

    public static PortabilidadData getInstance() {
        return ourInstance;
    }

    public PortabilidadData() {
    }

    public String getOperationReferenceNumber() {
        return operationReferenceNumber;
    }

    public void setOperationReferenceNumber(final String operationReferenceNumber) {
        this.operationReferenceNumber = operationReferenceNumber;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(String operationTime) {
        this.operationTime = operationTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getCreatedReferenceNumber() {
        return createdReferenceNumber;
    }

    public void setCreatedReferenceNumber(String createdReferenceNumber) {
        this.createdReferenceNumber = createdReferenceNumber;
    }

    public String getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(String companyNumber) {
        this.companyNumber = companyNumber;
    }

    public int getPositionBank() {
        return positionBank;
    }

    public void setPositionBank(int positionBank) {
        this.positionBank = positionBank;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        try {

            code=parser.parserNextObject("status").getString("code");
            description =parser.parserNextObject("status").getString("description");
            operationReferenceNumber = parser.parserNextObject("response").getString("operationReferenceNumber");
            createdDate = parser.parserNextObject("response").getString("createdDate");
            operationTime = parser.parserNextObject("response").getString("operationTime");
            createdReferenceNumber = parser.parserNextObject("response").getString("createdReferenceNumber");
            companyNumber = parser.parserNextObject("response").getJSONObject("company").getString("id");

        } catch (JSONException e) {
           // e.printStackTrace();
        }finally {
            description =parser.parseNextValue("description");
            errorCode = parser.parseNextValue("errorCode");
        }

    }
}
