package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import suitebancomer.aplicaciones.bbvacredit.models.CambiaNominaContrato;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TiempoAireDelegate;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
/**
 * Created by eluna on 28/06/2016.
 */
public class ContratoCambiaNominaViewController  extends BaseViewController {
    private BmovilViewsController parentManager;
    private String textoTerminos;
    private WebView textoTerminosCondiciones;
    private LinearLayout layoutWeb;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_contrato_cambia_nomina);
        setTitle(R.string.menu_comprar_contratar_title, R.drawable.bmovil_comprar_icono);
        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        SuiteApp suiteApp = (SuiteApp) getApplication();
        setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
        setDelegate(parentViewsController.getBaseDelegateForKey(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID));
        final String[] leyendas ;
        leyendas = CambiaNominaContrato.getInstance().getTxtHTML1().split("html>");
        textoTerminos = "<html>" + leyendas[1] + "html>";


        initValues();
    }

    private void initValues() {
        textoTerminosCondiciones = (WebView) findViewById(R.id.webViewTerminosContrato);
        textoTerminosCondiciones.getSettings().setJavaScriptEnabled(true);
        textoTerminosCondiciones.getSettings().setDefaultTextEncodingName("utf-8");
        textoTerminosCondiciones.loadData(textoTerminos, "text/html; charset=utf-8", "utf-8");
        textoTerminosCondiciones.getSettings().setBuiltInZoomControls(true);
        textoTerminosCondiciones.getSettings().setSupportZoom(true);
        textoTerminosCondiciones.getSettings().setLoadWithOverviewMode(true);
        textoTerminosCondiciones.getSettings().setUseWideViewPort(true);
        textoTerminosCondiciones.setInitialScale(15);
    }


    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void goBack() {
        parentViewsController.removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
        super.goBack();
    }

    @Override
    public void onUserInteraction() {
        //super.onUserInteraction();
        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();

    }
}
