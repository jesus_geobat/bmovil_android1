package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimeZone;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.EstadodeCuentaViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaEC;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaECExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaECResult;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class EstadodeCuentaDelegate extends DelegateBaseAutenticacion{

	public final static long ESTADODECUENTA_DELEGATE_ID = 9022323031661701077L;
	protected SuiteApp suiteApp = null;

	private Account cuentaActual = null;
	private EstadodeCuentaViewController estadodecuentaViewController;
	private ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController;
	private ConsultaECExtract totalPeriodos;
	private ConsultaEC consultaEC;
	private String Tipo;
	private String Periodo;
	private String Corte;
	private String CorteCodigo;
	private String Cuenta;
	private String Contraseña;
	private String Referencia;
	public boolean res = false;
	private String Token;

	private Account cuentaOrigen;
	public void setCuentaOrigen(Account cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}
	public Account getCuentaOrigen() {
		return cuentaOrigen;
	}

	//NEW CODE
	public void setConfirmacionAutenticacionViewController(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController)
	{
		this.confirmacionAutenticacionViewController=confirmacionAutenticacionViewController;
	}

	public ConsultaECExtract getTotalPeriodos() {
		return totalPeriodos;
	}

	public EstadodeCuentaViewController getEstadodeCuentaViewController() {
		return estadodecuentaViewController;
	}

	public void setEstadodeCuentaViewController(EstadodeCuentaViewController estadodecuentaViewController) {
		this.estadodecuentaViewController = estadodecuentaViewController;
	}

	public Account getCuentaSeleccionada(){
		return estadodecuentaViewController.getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada();
	}

	/**
	 * The owner view controller.
	 */
	private BaseViewController ownerController;

	/**
	 * @return The view controller associated to this delegate.
	 */
	public BaseViewController getCallerController() {
		return ownerController;
	}

	/**
	 * @param callerController The view controller to associate to this delegate.
	 */
	public void setCallerController(BaseViewController callerController) {
		this.ownerController = callerController;
	}

	public ConsultaEC getconsultaEC() {
		return consultaEC;
	}

	public void setConsultaEC(ConsultaEC consultaEC) {
		this.consultaEC = consultaEC;
	}

	@Override
	public void performAction(Object obj) {
		estadodecuentaViewController.resetViews();
		estadodecuentaViewController.llenaListaDatos();
		if (obj instanceof Account) {
			cuentaActual = getCuentaSeleccionada();
			if(cuentaActual.getType().equals("CM") || cuentaActual.getType().equals("CE")){
				estadodecuentaViewController.comboboxUltimos24.setEnabled(false);
				estadodecuentaViewController.resetViews();
				totalPeriodos = null;
				showalerta();
			}else{
				estadodecuentaViewController.comboboxUltimos24.setEnabled(true);
				cargarPeriodos();
			}
		}else{
			estadodecuentaViewController.comboboxUltimos24.setEnabled(true);
			cargarPeriodos();
		}


	}

	public void showalerta(){
		new AlertDialog.Builder(estadodecuentaViewController).setMessage("Por el momento no es posible consultar estados financieros para este tipo de cuenta.")
				.setTitle(R.string.label_error)
				.setIcon(R.drawable.anicalertaaviso)
				.setNeutralButton(R.string.common_alert_yesno_positive_button,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton){}
						}).show();
	}


	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada según sea requerido.
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();

		if(profile ==  Constants.Perfil.avanzado ){

			for(Account acc : accounts) {
				if(acc.isVisible()) {
					accountsArray.add(acc);
					break;
				}
			}

			for(Account acc : accounts) {
				if(!acc.isVisible())
					accountsArray.add(acc);
			}

		}else{
			accountsArray.add(Tools.obtenerCuentaEje());
		}
		return accountsArray;
	}

	/**
	 * Carga los periodos
	 *
	 */
	public void cargarPeriodos() {



		int operationId = Server.OBTENER_PERIODO_EC;
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put("numeroCelular", session.getUsername());
		String cuentaR = getCuentaSeleccionada().getNumber();
		if (getCuentaSeleccionada().getType().equals("TC")) {
			params.put("numeroCuenta", cuentaR.substring(cuentaR.length() - 16));
		} else {
			params.put("numeroCuenta", cuentaR.substring(cuentaR.length() - 10));
		}
		params.put("tipoCuenta", getCuentaSeleccionada().getType());
		params.put("IUM", session.getIum());

		//JAIG
		doNetworkOperation(operationId, params, true, new ConsultaECExtract(), isJsonValueCode.PAPERLESS, estadodecuentaViewController);

	}


	public void doNetworkOperation(int operationId, Hashtable<String, ?> params,boolean isJson, ParsingHandler handler, Server.isJsonValueCode isJsonValueCode, BaseViewController caller) {
		((BmovilViewsController)estadodecuentaViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode, caller);
	}


	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus()  == ServerResponse.OPERATION_SUCCESSFUL) {
			if(response.getResponse() instanceof ConsultaECExtract){
				totalPeriodos = (ConsultaECExtract) response.getResponse();
				Log.e("tamano", ""+totalPeriodos.getPeriodos().size());
				estadodecuentaViewController.txtUltimos24.setVisibility(View.VISIBLE);
				estadodecuentaViewController.comboboxUltimos24.setVisibility(View.VISIBLE);
			}else if (response.getResponse() instanceof ConsultaECResult) {
				ConsultaECResult data = (ConsultaECResult) response.getResponse();
				showPdf(data.getPdf());
			}else{
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showResultadosViewController(this, getNombreImagenEncabezado(), getTextoEncabezado());

			}

		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING ||
				response.getStatus() == ServerResponse.OPERATION_ERROR) {
			totalPeriodos = null;
			estadodecuentaViewController.ocultaIndicadorActividad();
			estadodecuentaViewController.showInformationAlertEspecial(estadodecuentaViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
		}
	}
	public void validarDatos(String tipo, String periodo, String corte, String contraseña, String confirmacion, String referencia, int numeroCorte) {


		Tipo = tipo;
		Periodo = periodo;
		Corte = corte;
		Contraseña = contraseña;
		if(numeroCorte == 0){
			CorteCodigo = "C1";
		}else{
			CorteCodigo = "C0";
		}

		Referencia = referencia;
		Cuenta= getCuentaSeleccionada().getNumber();

		if(tipo == null){
			new AlertDialog.Builder(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp()).setMessage("Selecciona un Tipo")
					.setTitle(R.string.label_error)
					.setIcon(R.drawable.anicalertaaviso)
					.setNeutralButton(R.string.common_alert_yesno_positive_button,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton){}
							}).show();

		}else if(periodo == null){
			new AlertDialog.Builder(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp()).setMessage("Es necesario seleccionar el periodo a consultar")
					.setTitle(R.string.label_error)
					.setIcon(R.drawable.anicalertaaviso)
					.setNeutralButton(R.string.common_alert_yesno_positive_button,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton){}
							}).show();

		}else if (corte == null){
			new AlertDialog.Builder(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp()).setMessage("Selecciona un Corte")
					.setTitle(R.string.label_error)
					.setIcon(R.drawable.anicalertaaviso)
					.setNeutralButton(R.string.common_alert_yesno_positive_button,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton){}
							}).show();
		} else if(contraseña.length() >= 6 && confirmacion.length() >= 6 && contraseña.length() <= 8 && confirmacion.length() <= 8){
				if(contraseña.equals(confirmacion)){
					showConfirmacion();
				} else{
					new AlertDialog.Builder(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp()).setMessage(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_valida_textoUno))
							.setTitle(R.string.label_error)
							.setIcon(R.drawable.anicalertaaviso)
							.setNeutralButton(R.string.common_alert_yesno_positive_button,
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int whichButton){}
									}).show();
				}
		}else if(contraseña.length() < 1){
			new AlertDialog.Builder(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp()).setMessage(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_valida_textodos))
					.setTitle(R.string.label_error)
					.setIcon(R.drawable.anicalertaaviso)
					.setNeutralButton(R.string.common_alert_yesno_positive_button,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton){}
							}).show();

		} else if (confirmacion.length() < 1){new AlertDialog.Builder(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp()).setMessage("Tú confirmación de contraseña no coincide")
				.setTitle(R.string.label_error)
				.setIcon(R.drawable.anicalertaaviso)
				.setNeutralButton(R.string.common_alert_yesno_positive_button,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
							}
						}).show();
		}

	}

	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;

		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_autoriza_operacion));
		fila.add("Consulta estado financiero");
		tabla.add(fila);

		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_autoriza_cuenta));
		String numero = estadodecuentaViewController.getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada().getNumber();
		String last = "*" + numero.substring(numero.length() -5);
		Log.d("cuenta", numero);
		Log.d("last", last);
		fila.add(last);
		tabla.add(fila);

		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_autoriza_formato));
		fila.add(Tipo);
		tabla.add(fila);

		String año = Periodo.substring(0,4);
		String mes = Periodo.substring(4,6);
		String mesletra = " ";
		if (mes.equals("01")) {
			mesletra = "Enero";

		} else if (mes.equals("02")) {
			mesletra = "Febrero";

		} else if (mes.equals("03")) {
			mesletra = "Marzo";

		} else if (mes.equals("04")) {
			mesletra = "Abril";

		} else if (mes.equals("05")) {
			mesletra = "Mayo";

		} else if (mes.equals("06")) {
			mesletra = "Junio";

		} else if (mes.equals("07")) {
			mesletra = "Julio";

		} else if (mes.equals("08")) {
			mesletra = "Agosto";

		} else if (mes.equals("09")) {
			mesletra = "Septiembre";

		} else if (mes.equals("10")) {
			mesletra = "Octubre";

		} else if (mes.equals("11")) {
			mesletra = "Noviembre";

		} else if (mes.equals("12")) {
			mesletra = "Diciembre";

		}

		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_autoriza_periodo));
		fila.add(mesletra+" "+año);
		tabla.add(fila);

		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_autoriza_corte));
		fila.add(Corte);
		tabla.add(fila);

		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_exitosa_fecha));
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = df.format(c.getTime());
		fila.add(formattedDate);
		tabla.add(fila);


		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_exitosa_hora));
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
		Date currentLocalTime = cal.getTime();
		DateFormat date = new SimpleDateFormat("HH:mm a");
		String localTime = date.format(currentLocalTime);
		fila.add(localTime);
		tabla.add(fila);



		return tabla;
	}

	public ArrayList<Object> getDatosTablaConfirmacion() {


		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;

		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_autoriza_operacion));
		fila.add(estadodecuentaViewController.getOperacion());
		tabla.add(fila);


		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_autoriza_cuenta));
		String numero = estadodecuentaViewController.getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada().getNumber();
		String last = "*" + numero.substring(numero.length() -5);
		Log.d("cuenta", numero);
		Log.d("last", last);
		fila.add(last);
		tabla.add(fila);


		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_autoriza_formato));
		fila.add(estadodecuentaViewController.getTipoFormato());
		tabla.add(fila);

		String año = estadodecuentaViewController.getPeriodo().substring(0,4);
		String mes = estadodecuentaViewController.getPeriodo().substring(4,6);
		String mesletra = " ";
		if (mes.equals("01")) {
			mesletra = "Enero";

		} else if (mes.equals("02")) {
			mesletra = "Febrero";

		} else if (mes.equals("03")) {
			mesletra = "Marzo";

		} else if (mes.equals("04")) {
			mesletra = "Abril";

		} else if (mes.equals("05")) {
			mesletra = "Mayo";

		} else if (mes.equals("06")) {
			mesletra = "Junio";

		} else if (mes.equals("07")) {
			mesletra = "Julio";

		} else if (mes.equals("08")) {
			mesletra = "Agosto";

		} else if (mes.equals("09")) {
			mesletra = "Septiembre";

		} else if (mes.equals("10")) {
			mesletra = "Octubre";

		} else if (mes.equals("11")) {
			mesletra = "Noviembre";

		} else if (mes.equals("12")) {
			mesletra = "Diciembre";

		}
		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_autoriza_periodo));
		fila.add(mesletra+ " "+año);
		tabla.add(fila);

		fila = new ArrayList<String>();
		fila.add(estadodecuentaViewController.getResources().getString(R.string.bmovil_Estado_de_cuenta_autoriza_corte));
		fila.add(estadodecuentaViewController.getCorte());
		tabla.add(fila);


		return tabla;
	}


	@SuppressLint("NewApi")
	public void showPdf(String pdfData){

		if(pdfData.length() > 1){
			byte[] data = Base64.decode(pdfData, Base64.DEFAULT);

			if (new File(estadodecuentaViewController.getFilesDir(), "/edocta.pdf").delete()) {
				File edoctaFile = new File(estadodecuentaViewController.getFilesDir() + "/edocta.pdf");
				OutputStream out = null;
				try {
					Log.e("Nuevo","Archivo");
					out = new BufferedOutputStream(new FileOutputStream(edoctaFile));
					out.write(data);
					out.close();
					SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConsultarEstadosDeCuentaPDF();
				} catch (Exception e) {
					if(Server.ALLOW_LOG) Log.d("FILE", Log.getStackTraceString(e));
				}
			} else {
				File edoctaFile = new File(estadodecuentaViewController.getFilesDir() + "/edocta.pdf");
				OutputStream out = null;
				try {
					Log.e("Nuevo","Archivo2");
					out = new BufferedOutputStream(new FileOutputStream(edoctaFile));
					out.write(data);
					out.close();
					SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConsultarEstadosDeCuentaPDF();
				} catch (Exception e) {
					if(Server.ALLOW_LOG) Log.d("FILE", Log.getStackTraceString(e));
				}
			}
		}else{
			new AlertDialog.Builder(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp()).setMessage("No se encontro pdf para el periodo seleccionado")
					.setTitle(R.string.label_error)
					.setIcon(R.drawable.anicalertaaviso)
					.setNeutralButton(R.string.common_alert_yesno_positive_button,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton) {
									SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().touchMenu();
									SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
								}
							}).show();


		}

	}



	public int getColorTituloResultado() {
		int colorRecurso = 0;

		colorRecurso = R.color.verde_bmovil_detalle;

		return colorRecurso;
	}

	public void showConfirmacion() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		bMovilVC.showConfirmacionAutenticacionViewController(this,
				getNombreImagenEncabezado(),
				getTextoEncabezado(),
				R.string.confirmation_subtitulo, getColorTituloResultado());
	}


	/**
	 * @return true si se debe mostrar contrasena, false en caso contrario.
	 */
	@Override

	public boolean mostrarContrasenia() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarContrasena(Operacion.consultarEstadoCuenta,
				perfil);

		Log.e("mostrarContrasenia()",String.valueOf(value));
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarCVV(Operacion.consultarEstadoCuenta, perfil);
		Log.e("mostrarCVV()",String.valueOf(value));
		return value;
	}

	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarNIP(Operacion.consultarEstadoCuenta,
				perfil);
		Log.e("mostrarNIP()", String.valueOf(value));
		return value;
	}

	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}

	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext)
				.getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Operacion.consultarEstadoCuenta,perfil);

		} catch (Exception ex) {
			Log.e(this.getClass().getName(),
					"Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}

		return tipoOTP;
	}

	/**
	 * Invoka la conexion al server para consultar el estado financiero
	 *
	 */
	public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAut,String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
		Token= token;
			int operationId = Server.ENVIAR_CORREO_EC;
			Session session = Session.getInstance(SuiteApp.appContext);
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("numeroCelular", session.getUsername());
			params.put("IUM", session.getIum());
			String last = getCuentaSeleccionada().getNumber().substring(getCuentaSeleccionada().getNumber().length() -5);
			params.put("nombreArchivo", "EC"+last);
			params.put("contrasenaEC", Contraseña);
			params.put("periodo",Periodo);
			params.put("corte",CorteCodigo);
			if(getCuentaSeleccionada().getType().equals("TC")){
				params.put("numeroCuenta",getCuentaSeleccionada().getNumber().substring(getCuentaSeleccionada().getNumber().length()-16));
			}else{
				params.put("numeroCuenta",getCuentaSeleccionada().getNumber().substring(getCuentaSeleccionada().getNumber().length()-10));
			}
			if(Tipo.equals("PDF")){
				params.put("tipoArchivo", "1");
			}
			else if(Tipo.equals("XML")){
				params.put("tipoArchivo", "2");
			}
			params.put(ServerConstants.CVE_ACCESO, "" );
			params.put(Server.J_NIP, "");
			params.put(Server.J_CVV2, "");
			params.put("codigoOTP", Token);
			params.put(Server.J_AUT, "00100");
			params.put("tarjeta5Dig", "");
			params.put("email", session.getEmail());
			params.put("indsft","N");
			params.put("indsus","S");
			params.put("cliente","");
			params.put("tipoCuenta", getCuentaSeleccionada().getType());

			//JAIG
			doNetworkOperation(operationId, params, true, null, Server.isJsonValueCode.PAPERLESS, confirmacionAut);
			confirmacionAut.muestraIndicadorActividad(confirmacionAut.getString(R.string.alert_operation),
					confirmacionAut.getString(R.string.alert_operation));
	}

	@Override
	public int getTextoEncabezado() {
		return (R.string.consultar_estados_de_cuenta_title);

	}

	public String getTextoTituloResultado() {

		String textoTituloResultado = SuiteApp.appContext.getString(R.string.bmovil_common_resultado_operacion_exitosa);
		return textoTituloResultado;
	}

	public String getTextoEspecialResultados() {

		String textoEspecialResultados = SuiteApp.appContext.getString(R.string.bmovil_Estado_de_cuenta_lbltextoEspecial);
		return textoEspecialResultados;
	}


	@Override
	public int getOpcionesMenuResultados() {
		// TODO Auto-generated method stub
		return 0;
	}


}
