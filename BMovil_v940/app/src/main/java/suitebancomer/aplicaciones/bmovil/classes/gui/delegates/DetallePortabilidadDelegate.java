package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;


import android.content.DialogInterface;

import com.google.gson.Gson;

import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import suitebancomer.aplicaciones.bbvacredit.models.CambiaNominaContrato;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultaPortabilidadNominaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.DetallePortabilidadViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.DetallePortability;
import suitebancomer.aplicaciones.commservice.commons.BodyType;
import suitebancomer.aplicaciones.commservice.commons.MethodType;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.commons.PojoGeneral;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by elunag on 11/07/16.
 */
public class DetallePortabilidadDelegate extends DelegateBaseOperacion {

    public final static long DETALLE_PORTABILIDAD_DELEGATE_ID = 0x1ef4f4c61ca122bfL;

    DetallePortabilidadViewController detallePortabilidadViewController;



    public void setDetallePortabilidadViewController(DetallePortabilidadViewController viewController) {
        this.detallePortabilidadViewController = viewController;
    }


    //One CLick
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (operationId == Server.CAMBIA_NOMINA_CONTRATO){
            CambiaNominaContrato contrato = CambiaNominaContrato.getInstance();
                String jsonCaracteresEspeciales = response.getResponsePlain();
                jsonCaracteresEspeciales = jsonCaracteresEspeciales.replace("\\r","");
                jsonCaracteresEspeciales = jsonCaracteresEspeciales.replace("\\n","");
                jsonCaracteresEspeciales = jsonCaracteresEspeciales.replace("\\","");
                jsonCaracteresEspeciales = jsonCaracteresEspeciales.replace("\"{","{");
                jsonCaracteresEspeciales = jsonCaracteresEspeciales.replace("}\"","}");
                contrato.setTxtHTML1(jsonCaracteresEspeciales);
                if (contrato.getTxtHTML1() != null)
                {
                    ((BmovilViewsController)detallePortabilidadViewController.getParentViewsController()).showContratoCambiaNomina();
                }
                else {
                    detallePortabilidadViewController.showInformationAlert("Servicio no disponible", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                }

        }
    }
    //Termina One CLick

    public void contratoCambiaNomina()
    {
        ParametersTO parametersTO = new ParametersTO();
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put(HTTP.CONTENT_TYPE, "application/json");
        parametersTO.setHeaders(headers);
        parametersTO.setParameters(new Hashtable<String, String>());
        parametersTO.setMethodType(MethodType.POST);
        JSONObject object=null;
        JSONObject objGetPortabilityRequest = null;
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        int operacion = Server.CAMBIA_NOMINA_CONTRATO;
        paramTable.put("referenceNumber", ConsultaPortabilidadNominaViewController.componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getNumber());
        paramTable.put("bankNumber",DetallePortability.getInstance().getExternalBank().getId());
        paramTable.put("createdReferenceNumber", DetallePortability.getInstance().getOperationReferenceNumber());
        paramTable.put("companyNumber", "4203710262");
        paramTable.put("ium", Session.getInstance(detallePortabilidadViewController).getIum());

        objGetPortabilityRequest = new JSONObject();
        try {
            objGetPortabilityRequest.put("referenceNumber",paramTable.get("referenceNumber"));

        objGetPortabilityRequest.put("bankNumber",paramTable.get("bankNumber"));
        objGetPortabilityRequest.put("createdReferenceNumber",paramTable.get("createdReferenceNumber"));
        objGetPortabilityRequest.put("companyNumber",paramTable.get("companyNumber"));

        object=new JSONObject();
        object.put("ium",paramTable.get("ium"));
        object.put("getPortabilityRequest",objGetPortabilityRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String json = "";
        final Gson gson = new Gson();
        json = gson.toJson(object);
        parametersTO.setBodyRaw(object.toString());
        parametersTO.setBodyType(BodyType.RAW);
        parametersTO.setForceArq(true);
        parametersTO.setAddCadena(false);
        doNetworkOperation(operacion, parametersTO, Boolean.TRUE, new PojoGeneral(), Server.isJsonValueCode.NONE, detallePortabilidadViewController);
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler hadler, Server.isJsonValueCode isJsonValue, BaseViewController caller) {
        ((BmovilViewsController) caller
                .getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, hadler, isJsonValue, caller);
    }

    public void doNetworkOperation(final int operationId, final ParametersTO params, final boolean isJson,
                                   final Object handler, final Server.isJsonValueCode isJsonValueCode,
                                   final BaseViewController caller) {
        ((BmovilViewsController)detallePortabilidadViewController.getParentViewsController()).getBmovilApp()
                .invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode,
                        caller, true);
    }
}
