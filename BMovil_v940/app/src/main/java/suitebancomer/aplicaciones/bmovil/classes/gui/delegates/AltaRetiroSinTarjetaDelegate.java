package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.StartBmovilInBack;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.AltaRetiroSinTarjetaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRegistroRetiroSinTarjetaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRegistroViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRetiroSinTarjetaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.PagoTdcViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.RegistrarOperacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultarLimitesResult;
import suitebancomer.aplicaciones.bmovil.classes.model.FrecuenteMulticanalResult;
import suitebancomer.aplicaciones.bmovil.classes.model.RegistrarOperacionResult;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjeta;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjetaResult;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjetaViewDto;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovilData;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class AltaRetiroSinTarjetaDelegate extends DelegateBaseAutenticacion {

    public enum TipoRetiro {
        RETIRO_SIN_TARJETA,
        DINERO_MOVIL_FRECUENTE,
        DINERO_MOVIL_NUEVO
    }

	private Session session;

	private BaseViewController viewController;

    public final static long ALTA_RETIRO_SINTAR_DELEGATE_ID = 5L;
    private AltaRetiroSinTarjetaViewController altaRetiroSinTarjetaViewController;
    private RetiroSinTarjetaResult retiroSinTarjetaResult;

    private RetiroSinTarjeta modeloRetiroSinTarjeta;
	private Account cuentaOrigenSeleccionada = null;
	public Account getCuentaOrigenSeleccionada() {
		return cuentaOrigenSeleccionada;
	}
	public void setCuentaOrigenSeleccionada(Account cuentaOrigenSeleccionada) {
		this.cuentaOrigenSeleccionada = cuentaOrigenSeleccionada;
	}

    /**
     * Modelo de transferencia de dinero m�vil.
     */
    private TransferenciaDineroMovil transferencia;

    private TipoRetiro tipoRetiro;
    private Constants.Operacion tipoOperacion;

	public ConsultarLimitesResult getLimitesMontos() {
		return limitesMontos;
	}

    private ConsultarLimitesResult limitesMontos;

    private Integer currentOperation;

    public void setCurrentOperation(Integer currentOperation) {
        this.currentOperation = currentOperation;
    }

    public RetiroSinTarjeta getModeloRetiroSinTarjeta() {
        return modeloRetiroSinTarjeta;
    }

    public void setModeloRetiroSinTarjeta(RetiroSinTarjeta modeloRetiroSinTarjeta) {
        this.modeloRetiroSinTarjeta = modeloRetiroSinTarjeta;
    }

    public void setAltaRetiroSinTarjetaViewController(
            AltaRetiroSinTarjetaViewController altaRetiroSinTarjetaViewController) {
        this.altaRetiroSinTarjetaViewController = altaRetiroSinTarjetaViewController;
    }

    public RetiroSinTarjetaResult getRetiroSinTarjetaResult() {
        return retiroSinTarjetaResult;
    }

	public void setRetiroSinTarjetaResult(RetiroSinTarjetaResult retiroSinTarjetaResult) {
		this.retiroSinTarjetaResult = retiroSinTarjetaResult;
	}

	public TipoRetiro getTipoRetiro() {
		return tipoRetiro;
	}

	public void setTipoRetiro(TipoRetiro tipoRetiro) {
		this.tipoRetiro = tipoRetiro;
	}

	/**
	 * @return El tipo de operación.
	 */
	public Constants.Operacion getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * @param tipoOperacion El tipo de operación a estblecer.
	 */
	public void setTipoOperacion(Constants.Operacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}



	/**
	 * Constructor por defecto
	 */
	public AltaRetiroSinTarjetaDelegate() {
		this.tipoOperacion = Constants.Operacion.retiroSinTarjeta;
        transferencia = new TransferenciaDineroMovil();
        modeloRetiroSinTarjeta = new RetiroSinTarjeta();
        tipoRetiro = TipoRetiro.RETIRO_SIN_TARJETA;

	}


	/* peticion al servidor de alta de retiro sin tarjeta desde confirmacion*/
	public void operacionAltaRetiroSinTarjeta(ConfirmacionRetiroSinTarjetaViewController confirmacionRetiroSinTarjetaViewController,String contrasenia, String nip, String token, String campoTarjeta, String cvv) {

		session = Session.getInstance(SuiteApp.appContext);

		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion, session.getClientProfile(), Tools.getDoubleAmountFromServerString(modeloRetiroSinTarjeta.getImporte()));
		String tipoCuentaSeleccionada= modeloRetiroSinTarjeta.getCuentaOrigen().getType();
		String numeroCuentaSeleccionada= modeloRetiroSinTarjeta.getCuentaOrigen().getNumber();

		String importeRetiro= modeloRetiroSinTarjeta.getImporte();
		String conceptoRetiro= modeloRetiroSinTarjeta.getConcepto();

		//prepare data
		Hashtable<String,String> paramTable = new Hashtable<String,String>();

		//paramTable.put(ServerConstants.OPERACION, Server.OPERATION_CODES[Server.OP_RETIRO_SIN_TAR]);
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		paramTable.put("numeroCelular", session.getUsername());
		paramTable.put(ServerConstants.CVE_ACCESO, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);
		paramTable.put(ServerConstants.CODIGO_NIP, Tools.isEmptyOrNull(nip) ? "" : nip);
		paramTable.put(ServerConstants.CODIGO_CVV2, Tools.isEmptyOrNull(cvv) ? "" : cvv);
		paramTable.put(ServerConstants.CADENA_AUTENTICACION, cadenaAutenticacion);
		paramTable.put(ServerConstants.CODIGO_OTP,  Tools.isEmptyOrNull(token) ? "" : token);
		paramTable.put(ServerConstants.TIPO_CUENTA,tipoCuentaSeleccionada);
		paramTable.put("numeroCuenta", numeroCuentaSeleccionada);
		paramTable.put("cuentaDestino", "");



		paramTable.put(ServerConstants.PERFIL_CLIENTE,session.getClientProfile().name());
		paramTable.put(ServerConstants.TARJETA_5DIG,Tools.isEmptyOrNull(campoTarjeta) ? "" : campoTarjeta);


		paramTable.put("importe",importeRetiro);
		paramTable.put("concepto", conceptoRetiro);

		//JAIG NO
        doNetworkOperation(Server.OP_RETIRO_SIN_TAR, paramTable, true, new RetiroSinTarjetaResult(), isJsonValueCode.NONE, confirmacionRetiroSinTarjetaViewController);
    }

    /**
     * regresa a el activity StartBmovilInBack indicando que ocurrio un error
     */
    private void returnToStartBmovilInBack() {
        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
        ActivityCompat.finishAffinity(altaRetiroSinTarjetaViewController);
        altaRetiroSinTarjetaViewController.finish();
        Intent i = new Intent(SuiteApp.getInstance(), StartBmovilInBack.class);
        i.putExtra(Constants.RESULT_STRING, Constants.FALSE_STRING);
        i.putExtra(Constants.TIMEOUT_STRING, Constants.FALSE_STRING);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        altaRetiroSinTarjetaViewController.startActivity(i);
    }

	/**
	 * @category RegistrarOperacion
	 * Muestra la pantalla de confirmacion registro
	 */
	public void showConfirmacionRegistro() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		bMovilVC.showConfirmacionRetiroSinTarjeta(this);
	}
    /**
     * La respuesta del servidor.
     */
    private TransferenciaDineroMovilData serverResponse;

	public TransferenciaDineroMovilData getServerResponse() {
		return serverResponse;
	}

	public void setServerResponse(TransferenciaDineroMovilData serverResponse) {
		this.serverResponse = serverResponse;
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {

		if (operationId == Server.OP_RETIRO_SIN_TAR) {
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				retiroSinTarjetaResult = (RetiroSinTarjetaResult) response.getResponse();
				operacionExistosa();
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				if (bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//flujo bmovil
					altaRetiroSinTarjetaViewController.showInformationAlert(response.getMessageText());
				} else {
					altaRetiroSinTarjetaViewController.showInformationAlert(response.getMessageText(), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
							returnToStartBmovilInBack();
						}
					});
				}

			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {

				altaRetiroSinTarjetaViewController.ocultaIndicadorActividad();
				if (bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//flujo bmovil
					altaRetiroSinTarjetaViewController.showInformationAlert(response.getMessageText());
				} else {
					altaRetiroSinTarjetaViewController.showInformationAlert(response.getMessageText(), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
							returnToStartBmovilInBack();
						}
					});
				}
				//se trata en el BaseSubapplication.java
				//si entra por error pero no trae codigo ni mensaje, se debe tratar?
			}

			altaRetiroSinTarjetaViewController.ocultaIndicadorActividad();
		} else if (operationId == Server.OP_SOLICITAR_ALERTAS) {
			analyzeAlertasRecortadoSinError();
			return;
		} else if (operationId == Server.OP_VALIDAR_CREDENCIALES) {
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				showConfirmacionRegistro();
				return;
			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
				BmovilViewsController bmvc = bmApp.getBmovilViewsController();
				BaseViewController current = bmvc.getCurrentViewControllerApp();
				current.showInformationAlert(response.getMessageText());
				return;
			}
		} else if(response.getResponse() instanceof TransferenciaDineroMovilData) {
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				serverResponse = (TransferenciaDineroMovilData) response.getResponse();

				if (tipoRetiro.equals(TipoRetiro.DINERO_MOVIL_FRECUENTE) && (tokenAMostrar() == Constants.TipoOtpAutenticacion.codigo
						|| tokenAMostrar() == Constants.TipoOtpAutenticacion.registro)
						&& Tools.isEmptyOrNull(transferencia.getFrecuenteMulticanal())) {
					actualizaFrecuenteAMulticanal();
				} else {
					operacionExistosa();
				}
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
				BmovilViewsController bmvc = bmApp.getBmovilViewsController();
				BaseViewController current = bmvc.getCurrentViewControllerApp();
				current.showInformationAlert(response.getMessageText());
			}
		} else if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			operacionExistosa();
		}

	}

	private void operacionExistosa() {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "operacionExistosa");
		showResultados();
	}

	/**
	 * Muestra la pantalla de resultados.
	 */
	public void showResultados() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showResultadosRetiroSinTarjetaViewController(this);
	}

	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
		setCurrentOperation(operationId);
		((BmovilViewsController)caller.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode, caller, true);
	}




	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada segun sea requerido.
	 *
	 * @return
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();

		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();

		if (profile == Constants.Perfil.basico) {
			for (Account acc : accounts) {
				if (acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE))) {
					accountsArray.add(acc);
				}
			}
		} else {
			for (Account acc : accounts) {
				if (acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE))) {
					accountsArray.add(acc);
					break;
				}
			}

			for (Account acc : accounts) {
				if (!acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE)))
					accountsArray.add(acc);
			}
		}

		if (((profile == Constants.Perfil.basico) || (accounts.length == 1)) && (Tools.obtenerCuentaEje().getType().equals(Constants.CREDIT_TYPE))) { //cuentaEjeTDC) ){
			altaRetiroSinTarjetaViewController.showInformationAlert(R.string.error_cuenta_eje_credito);
		}

		return accountsArray;
	}


	private Account getCuentaOrigen(){
		return ((PagoTdcViewController)viewController).getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada();
	}

	public BaseViewController getViewController() {
		return viewController;
	}

	public void setViewController(BaseViewController viewController) {
		this.viewController = viewController;
	}

	/**
	 * Carga la lista de companias para las transferencias de dinero movil.
	 * @return La lista de Companias en una mapa con clave 'nombre' y valor la propia compania.
	 */
	public HashMap<String,Compania> cargarListaCompanias() {
		HashMap<String,Compania> listaCompanias = new HashMap<String,Compania>();
		CatalogoVersionado catalogoDineroMovil = Session.getInstance(SuiteApp.appContext).getCatalogoDineroMovil();

		Compania compIterada;

		for (Object comp : catalogoDineroMovil.getObjetos()) {
			compIterada=(Compania)comp;
			listaCompanias.put(compIterada.getNombre(),compIterada);
		}
		return listaCompanias;
	}



	/**
	 * Obtiene el catálogo de importes para la compania especificada.
	 * @param identificadorCompania Identificador de la compania a buscar.
	 * @return La lista de importes.
	 */
	public String[] obtenerCatalogoDeImportes(String identificadorCompania) {
		ArrayList<String> importes = new ArrayList<String>();

		CatalogoVersionado catalogoDineroMovil = Session.getInstance(SuiteApp.appContext).getCatalogoDineroMovil();
		Vector<Object> companias = catalogoDineroMovil.getObjetos();
		for (Object comp : companias) {
			Compania compania = (Compania)comp;
			if(identificadorCompania == compania.getNombre()) {
				for (int monto : compania.getMontosValidos()) {
					importes.add(String.valueOf(monto));
				}
				break;
			}
		}
		importes.add(SuiteApp.appContext.getResources().getString(R.string.transferir_dineromovil_datos_importe_otro));

		return importes.toArray(new String[importes.size()]);
	}



	/****/

	@Override
	public void performAction(Object obj) {
		if (tipoRetiro.equals(TipoRetiro.RETIRO_SIN_TARJETA)) {
			if (Server.ALLOW_LOG) Log.d("altaRSTDelegate", "Regrese de lista Seleccion");

			altaRetiroSinTarjetaViewController.getComponenteCtaOrigen().isSeleccionado();
			altaRetiroSinTarjetaViewController.muestraListaCuentas();
			altaRetiroSinTarjetaViewController.getComponenteCtaOrigen().setSeleccionado(false);
		} else {
			altaRetiroSinTarjetaViewController.reactivarCtaOrigen((Account) obj);
		}
	}





	public void validaDatos(String telefono, String confirmacion, String otroImporte, String beneficiario) {
		double otroMonto;
		AltaRetiroSinTarjetaViewController controller;

		try {
			controller = (AltaRetiroSinTarjetaViewController) altaRetiroSinTarjetaViewController;
		} catch (ClassCastException ex) {
			if (Server.ALLOW_LOG)
				Log.e("AltaRSDelegate", "Error al convertir el view controller a un AltaRetiroSinTarjetaViewController", ex);
			return;
		}

		try {
			otroMonto = Double.parseDouble(otroImporte);
		} catch (Exception ex) {
			otroMonto = 0.0;
		}

		altaRetiroSinTarjetaViewController.setHabilitado(true);
		if(Constants.TELEPHONE_NUMBER_LENGTH != telefono.length()) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_telefono_corto);
		} else if (Constants.TELEPHONE_NUMBER_LENGTH != confirmacion.length()) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_confirmar_cotro);
		} else if (!telefono.equalsIgnoreCase(confirmacion)) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_confirmar_mal);
		} else if (0 == beneficiario.length()) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_beneficiario_nulo);
		} else if (Constants.BALANCE_EN_CEROS == otroMonto) {
			controller.showInformationAlert(R.string.transferir_retirosintarjeta_error_otro_vacio);
		} else if (0 != (otroMonto % 100.0)) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_otro_invalido);
		} else {
			modeloRetiroSinTarjeta.setCelularBeneficiario(telefono);
			modeloRetiroSinTarjeta.setImporte(Tools.formatAmountForServer(String.valueOf(otroMonto)));
			modeloRetiroSinTarjeta.setBeneficiario(beneficiario);
			modeloRetiroSinTarjeta.setConcepto(controller.textConcepto.getText().toString());
			transferencia.setImporte(Tools.formatAmountForServer(String.valueOf(otroMonto)));
			transferencia.setConcepto(controller.textConcepto.getText().toString());

			showConfirmacion();
		}
	}


	/**
	 * @category RegistrarOperacion
	 * @return
	 */
	public boolean registrarOperacion(){
		if(tipoRetiro.equals(TipoRetiro.RETIRO_SIN_TARJETA)){
			return false;
		}
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

		Autenticacion aut = Autenticacion.getInstance();
		boolean value = aut.validaRegistro(tipoOperacion, perfil);
		return value;
	}
    /**
     * envia la actualizacion del frecuente
     */
    private void actualizaFrecuenteAMulticanal() {
        if(Server.ALLOW_LOG) Log.d(getClass().getName(), "actualizaFrecuenteAMulticanal");
        Session session = Session.getInstance(SuiteApp.appContext);
        Hashtable<String, String> paramTable = null;
        paramTable = new Hashtable<String, String>();
        paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
        paramTable.put(ServerConstants.PARAMS_TEXTO_IU, session.getIum());
        paramTable.put("TE", session.getClientNumber());
        paramTable.put("TP", Constants.TPDineroMovil);
        paramTable.put("CA", transferencia.getAliasFrecuente());
        paramTable.put("DE", transferencia.getAliasFrecuente());
        paramTable.put("RF", "");
        paramTable.put("BF", transferencia.getBeneficiario());
        paramTable.put("OA", transferencia.getNombreCompania());
        paramTable.put("CN", transferencia.getIdCanal());
        paramTable.put("CB", "");
        paramTable.put("US", transferencia.getUsuario());
        //frecuente correo electronico
        paramTable.put("CE", transferencia.getCorreoFrecuente());
        BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
        BmovilViewsController bmvc = bmApp.getBmovilViewsController();
        BaseViewController current = bmvc.getCurrentViewControllerApp();
        doNetworkOperation(Server.ACTUALIZAR_FRECUENTE, paramTable, false, new FrecuenteMulticanalResult(), isJsonValueCode.NONE, current);
    }

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;

		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_cuenta_retiro));
		fila.add(Tools.hideAccountNumber(modeloRetiroSinTarjeta.getCuentaOrigen().getNumber()));
		tabla.add(fila);


		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_numero_celular));
		fila.add(modeloRetiroSinTarjeta.getCelularBeneficiario());
		tabla.add(fila);


		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_compania_celular));
		fila.add(modeloRetiroSinTarjeta.getCompania().getNombre());
		tabla.add(fila);

		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_beneficiario));
		fila.add(modeloRetiroSinTarjeta.getBeneficiario());
		tabla.add(fila);
		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_importe));
		fila.add(Tools.formatAmount(modeloRetiroSinTarjeta.getImporte(), false));
		tabla.add(fila);


		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_concepto));
		fila.add(modeloRetiroSinTarjeta.getConcepto());
		tabla.add(fila);


		return tabla;
	}

	@Override
	public int getTextoEncabezado() {
		return R.string.transferir_dineromovil_titulo;
	}


	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.bmovil_dinero_movil_icono;
	}

	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		TipoOtpAutenticacion value = null;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

		if (tipoRetiro.equals(TipoRetiro.RETIRO_SIN_TARJETA)) {
			value = Autenticacion.getInstance().tokenAMostrar(this.tipoOperacion, perfil, Tools.getDoubleAmountFromServerString(altaRetiroSinTarjetaViewController.getDatosBeneficiario().getImporte()));
		} else {
			value = Autenticacion.getInstance().tokenAMostrar(this.tipoOperacion, perfil, Tools.getDoubleAmountFromServerString(transferencia.getImporte()));
		}
		return value;
	}
	@Override
	public boolean mostrarNIP() {
		boolean mostrar = false;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

		if (tipoRetiro.equals(TipoRetiro.RETIRO_SIN_TARJETA)) {
			mostrar = Autenticacion.getInstance().mostrarNIP(this.tipoOperacion, perfil, Tools.getDoubleAmountFromServerString(altaRetiroSinTarjetaViewController.getDatosBeneficiario().getImporte()));
		} else {
			mostrar = Autenticacion.getInstance().mostrarNIP(this.tipoOperacion, perfil, Tools.getDoubleAmountFromServerString(transferencia.getImporte()));
		}

		return mostrar;
	}
	@Override
	public boolean mostrarContrasenia() {
		boolean mostrar = false;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

		if (tipoRetiro.equals(TipoRetiro.RETIRO_SIN_TARJETA)) {
			mostrar = Autenticacion.getInstance().mostrarContrasena(this.tipoOperacion, perfil, Tools.getDoubleAmountFromServerString(altaRetiroSinTarjetaViewController.getDatosBeneficiario().getImporte()));
		} else {
			mostrar = Autenticacion.getInstance().mostrarContrasena(this.tipoOperacion, perfil, Tools.getDoubleAmountFromServerString(transferencia.getImporte()));
		}
		return mostrar;
	}

	@Override
	public boolean mostrarCVV() {
		boolean mostrar = false;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

		if (tipoRetiro.equals(TipoRetiro.RETIRO_SIN_TARJETA)) {
			mostrar = Autenticacion.getInstance().mostrarCVV(this.tipoOperacion, perfil, Tools.getDoubleAmountFromServerString(altaRetiroSinTarjetaViewController.getDatosBeneficiario().getImporte()));
		} else {
			mostrar = Autenticacion.getInstance().mostrarCVV(this.tipoOperacion, perfil, Tools.getDoubleAmountFromServerString(transferencia.getImporte()));
		}
		return mostrar;
	}
	/**
	 * Muestra la pantalla de confirmacion.
	 */
	public void showConfirmacion() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();

		if (registrarOperacion())
			bMovilVC.showConfirmacionRegistroRetiroSinTarjeta(this);
		else
			bMovilVC.showConfirmacionRetiroSinTarjeta(this);

	}


	public void realizaOperacion(ConfirmacionRetiroSinTarjetaViewController confirmacionRetiroSinTarjetaViewController, String contrasenia, String nip, String token, String campoTarjeta, String cvv){
		if(tipoRetiro.equals(TipoRetiro.RETIRO_SIN_TARJETA)){
			operacionAltaRetiroSinTarjeta(confirmacionRetiroSinTarjetaViewController,contrasenia,nip,token,campoTarjeta,cvv);
		}else{
			Hashtable paramTable = informarPeticionAltaFrecuente(contrasenia,nip,token);
			int operacion=Server.ALTA_OPSINTARJETA;

			doNetworkOperation(operacion,paramTable,false,new TransferenciaDineroMovilData(),isJsonValueCode.NONE,confirmacionRetiroSinTarjetaViewController);
		}
	}

	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		ArrayList<Object> datosTabla =  new ArrayList<Object>();
		ArrayList<String> registro = null;
		registro = new ArrayList<String>();
		registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_clave_retiro));
		registro.add(Tools.formatClaveRetiroGuiones(retiroSinTarjetaResult.getClaveRetiro()));
		datosTabla.add(registro);
		registro = new ArrayList<String>();
		registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_codigo_seguridad));
		registro.add(retiroSinTarjetaResult.getClaveRst4());
		datosTabla.add(registro);
		registro = new ArrayList<String>();
		registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_cuenta_retiro));
		registro.add(Tools.hideAccountNumber(modeloRetiroSinTarjeta.getCuentaOrigen().getNumber()));
		datosTabla.add(registro);
//			registro = new ArrayList<String>();
//			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_numero_celular));
//			registro.add(modeloRetiroSinTarjeta.getCelularBeneficiario());
//			datosTabla.add(registro);
//			registro = new ArrayList<String>();
//			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_compania_celular));
//			registro.add(modeloRetiroSinTarjeta.getCompania().getNombre());
//			datosTabla.add(registro);
//
//			registro = new ArrayList<String>();
//			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_beneficiario));
//			registro.add(modeloRetiroSinTarjeta.getBeneficiario());
//			datosTabla.add(registro);
//
		registro = new ArrayList<String>();
		registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_importe));
		registro.add(Tools.formatAmount(retiroSinTarjetaResult.getImporte(),false));
		datosTabla.add(registro);
		registro = new ArrayList<String>();
		registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_concepto));
		registro.add(modeloRetiroSinTarjeta.getConcepto());
		datosTabla.add(registro);
		registro = new ArrayList<String>();
		registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_fecha_alta));
		registro.add(Tools.formatDate(retiroSinTarjetaResult.getFechaAlta()));
		datosTabla.add(registro);
		registro = new ArrayList<String>();
		registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_vigencia));
		registro.add(Tools.formatDate(retiroSinTarjetaResult.getVigencia())+" "+retiroSinTarjetaResult.getHoraOperacion()+" hrs");
		datosTabla.add(registro);
		registro = new ArrayList<String>();
		registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_folio));
		registro.add(retiroSinTarjetaResult.getFolio());
		datosTabla.add(registro);
		return datosTabla;
	}



	@Override
	public String getTextoTituloResultado() {
		return SuiteApp.appContext.getString(R.string.transferir_retirosintarjeta_resultados_titulo);
	}


	@Override
	public int getColorTituloResultado() {
		return R.color.verde_limon;

	}




	@Override
	public int getOpcionesMenuResultados() {
		if (tipoRetiro.equals(TipoRetiro.RETIRO_SIN_TARJETA)) {
			return SHOW_MENU_EMAIL | SHOW_MENU_SMS;
		} else if (tipoRetiro.equals(TipoRetiro.DINERO_MOVIL_FRECUENTE)) {
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF;
		} else {
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_FRECUENTE;
		}
	}


	@Override
	public String getTextoAyudaResultados() {
		return "Para guardar esta información oprime el botón de opciones";
	}







	/* peticion al servidor de alta de retiro sin tarjeta desde registro confirmacion*/
	public void operacionAltaRetiroSinTarjetaRegistro(ConfirmacionRegistroViewController confirmacionRegistroViewController,String contrasenia, String nip, String token, String campoTarjeta, String cvv) {

	/*	altaRetiroSinTarjetaViewController.muestraIndicadorActividad(
				altaRetiroSinTarjetaViewController.getString(R.string.alert_operation),
				altaRetiroSinTarjetaViewController.getString(R.string.alert_connecting));
		*/
		session = Session.getInstance(SuiteApp.appContext);

		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion, session.getClientProfile(), Tools.getDoubleAmountFromServerString(modeloRetiroSinTarjeta.getImporte()));
		String tipoCuentaSeleccionada= modeloRetiroSinTarjeta.getCuentaOrigen().getType();
		String numeroCuentaSeleccionada= modeloRetiroSinTarjeta.getCuentaOrigen().getNumber();

		String importeRetiro= modeloRetiroSinTarjeta.getImporte();
		String conceptoRetiro= modeloRetiroSinTarjeta.getConcepto();

		//prepare data
		Hashtable<String,String> paramTable = new Hashtable<String,String>();
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		paramTable.put("numeroCelular", session.getUsername());
		paramTable.put(ServerConstants.CVE_ACCESO, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);
		paramTable.put(ServerConstants.CODIGO_NIP, Tools.isEmptyOrNull(nip) ? "" : nip);
		paramTable.put(ServerConstants.CODIGO_CVV2, Tools.isEmptyOrNull(cvv) ? "" : cvv);
		paramTable.put(ServerConstants.CADENA_AUTENTICACION, cadenaAutenticacion);
		paramTable.put(ServerConstants.CODIGO_OTP,  Tools.isEmptyOrNull(token) ? "" : token);
		paramTable.put(ServerConstants.TIPO_CUENTA,tipoCuentaSeleccionada);
		paramTable.put("numeroCuenta", numeroCuentaSeleccionada);
		paramTable.put("cuentaDestino", "");



		paramTable.put(ServerConstants.PERFIL_CLIENTE,session.getClientProfile().name());
		paramTable.put(ServerConstants.TARJETA_5DIG,Tools.isEmptyOrNull(campoTarjeta) ? "" : campoTarjeta);


		paramTable.put("importe",importeRetiro);
		paramTable.put("concepto", conceptoRetiro);

		//JAIG NO
		doNetworkOperation(Server.OP_RETIRO_SIN_TAR, paramTable, true, new RetiroSinTarjetaResult(), isJsonValueCode.NONE, confirmacionRegistroViewController);
	}

	private Hashtable<String, String> informarPeticionAltaFrecuente(String contrasenia, String nip, String token) {
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        Session session = Session.getInstance(SuiteApp.appContext);
       String cAut = Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion, session.getClientProfile(), Tools.getDoubleAmountFromServerString(transferencia.getImporte()));
        paramTable.put(ServerConstants.ID_OPERACION, ServerConstants.DINERO_MOVIL);
        paramTable.put(ServerConstants.NUMERO_TELEFONO_ETIQUETA, session.getUsername());
        paramTable.put(ServerConstants.CONTRASENA_ETIQUETA, (null == contrasenia) ? "" : contrasenia);
        paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
        paramTable.put(ServerConstants.NUMERO_CLIENTE_ETIQUETA, session.getClientNumber());
        paramTable.put(ServerConstants.CUENTA_CARGO_ETIQUETA, transferencia.getCuentaOrigen().getType() + transferencia.getCuentaOrigen().getNumber());
        paramTable.put(ServerConstants.IMPORTE_ETIQUETA, transferencia.getImporte());
        paramTable.put(ServerConstants.NOMBRE_BENEFICIARIO_ETIQUETA, transferencia.getBeneficiario());
        paramTable.put(ServerConstants.CONCEPTO_ETIQUETA, transferencia.getConcepto());
        paramTable.put(ServerConstants.NUMERO_CELULAR_TERCERO_ETIQUETA, transferencia.getCelularbeneficiario());
        paramTable.put(ServerConstants.OPERADORA_TELEFONIA_CELULAR_ETIQUETA, transferencia.getNombreCompania());
        paramTable.put(ServerConstants.NIP_ETIQUETA, Tools.isEmptyOrNull(nip) ? "" : nip);
        paramTable.put(ServerConstants.CVV_ETIQUETA, "");
        paramTable.put(ServerConstants.OTP_TOKEN_ETIQUETA, Tools.isEmptyOrNull(token) ? "" : token);
        paramTable.put(ServerConstants.INSTRUCCION_VALIDACION_ETIQUETA,	cAut);
        return paramTable;
	}


	/**
	 *  @category RegistrarOperacion
	 * @return
	 */
	protected ArrayList<Object> getDatosRegistroOp() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;


		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_cuenta_retiro));
		fila.add(Tools.hideAccountNumber(modeloRetiroSinTarjeta.getCuentaOrigen().getNumber()));
		tabla.add(fila);


//		fila = new ArrayList<String>();
//		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_numero_celular));
//		fila.add(modeloRetiroSinTarjeta.getCelularBeneficiario());
//		tabla.add(fila);
//
//
//		fila = new ArrayList<String>();
//		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_compania_celular));
//		fila.add(modeloRetiroSinTarjeta.getCompania().getNombre());
//		tabla.add(fila);
//
//		fila = new ArrayList<String>();
//		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_beneficiario));
//		fila.add(modeloRetiroSinTarjeta.getBeneficiario());
//		tabla.add(fila);
		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_importe));
		fila.add(Tools.formatAmount(modeloRetiroSinTarjeta.getImporte(), false));
		tabla.add(fila);


		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_concepto));
		fila.add(modeloRetiroSinTarjeta.getConcepto());
		tabla.add(fila);

		return tabla;
	}

	/**
	 * @category RegistrarOperacion
	 */
	@Override
	public ArrayList<Object> getDatosRegistroExitoso() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> registro;

		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_op));
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_exito));
		tabla.add(registro);

		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_cuenta));
		registro.add(modeloRetiroSinTarjeta.getCelularBeneficiario());
		tabla.add(registro);

		return tabla;
	}

	/**
	 * @category RegistrarOperacion
	 * realiza el registro de operacion
	 * @param rovc
	 * @param token
	 */
	@Override
	protected void realizaOperacion(RegistrarOperacionViewController rovc,	String token) {
		int operationId = Server.OP_VALIDAR_CREDENCIALES;
		Hashtable<String, String> params = new Hashtable<String, String>();
		session = Session.getInstance(SuiteApp.appContext);


		//String cuentaDestino = modeloRetiroSinTarjeta.getCelularBeneficiario();
		String cuentaDestino = "";

		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.CVE_ACCESO, "" );
		params.put(ServerConstants.COMPANIA_BENEFICIARIO,transferencia.getNombreCompania());
		params.put(ServerConstants.CELULAR_BENEFICIARIO,transferencia.getCelularbeneficiario());
		params.put(Server.J_NIP, "");
		params.put(Server.J_CVV2, "");
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(ServerConstants.IUM,session.getIum());
		params.put(Server.J_AUT, "00010");
		params.put(ServerConstants.ID_OPERACION,ServerConstants.DINERO_MOVIL);
		params.put(ServerConstants.VERSION,Constants.APPLICATION_VERSION);
		params.put("cuentaDestino", cuentaDestino);
		params.put("tarjeta5Dig", "");
		//JAIG NO
		doNetworkOperation(operationId, params,true, new RegistrarOperacionResult(),isJsonValueCode.NONE, rovc);
	}

	/**
	 * @category RegistrarOperacion
	 * Realiza la operacion de tranferencia desde la pantalla de confirmacionRegistro
	 */
	public void realizaOperacion(ConfirmacionRegistroRetiroSinTarjetaViewController viewController, String contrasena, String nip, String asm, String cvv) {
		int operationId = Server.OP_VALIDAR_CREDENCIALES;
		Hashtable<String, String> params = new Hashtable<String, String>();
		session = Session.getInstance(SuiteApp.appContext);

		String cuentaDestino = "";

		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.CVE_ACCESO, "");
		params.put(ServerConstants.COMPANIA_BENEFICIARIO,transferencia.getNombreCompania());
		params.put(ServerConstants.CELULAR_BENEFICIARIO,transferencia.getCelularbeneficiario());
		params.put(Server.J_NIP, nip == null ? "" : nip);
		params.put(Server.J_CVV2, cvv == null ? "" : cvv);
		params.put(ServerConstants.CODIGO_OTP, asm == null ? "" : asm);
		params.put(ServerConstants.IUM,session.getIum());
		params.put(Server.J_AUT, "00010");
		params.put(ServerConstants.ID_OPERACION,ServerConstants.DINERO_MOVIL);
		params.put(ServerConstants.VERSION,Constants.APPLICATION_VERSION);
		params.put("cuentaDestino", cuentaDestino);
		params.put("tarjeta5Dig", "");
		//JAIG NO
		doNetworkOperation(operationId, params, true, new RegistrarOperacionResult(), isJsonValueCode.NONE, viewController);
	}



	@Override
	public String getNumeroCuentaParaRegistroOperacion() {
		String cuentaDestino = modeloRetiroSinTarjeta.getCelularBeneficiario();
		return cuentaDestino.substring(cuentaDestino.length() - 5);
	}




	public void consultarLimites() {
		int operationId = Server.CONSULTAR_LIMITES;
		session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		//JAIG SI
		doNetworkOperation(operationId, params,true, new ConsultarLimitesResult(),isJsonValueCode.NONE, altaRetiroSinTarjetaViewController);

	}


    public Object getCompaniaSeleccionada() {
        if(!tipoRetiro.equals(TipoRetiro.DINERO_MOVIL_FRECUENTE))
            return null;

        Object cuentaSeleccionada = null;

        CatalogoVersionado catalogoDineroMovil = Session.getInstance(SuiteApp.appContext).getCatalogoDineroMovil();
        for (Object comp : catalogoDineroMovil.getObjetos()) {
            Compania compania = (Compania)comp;

            if(compania.getNombre().equalsIgnoreCase(transferencia.getNombreCompania())) {
                cuentaSeleccionada = comp;
                break;
            }
        }

        return cuentaSeleccionada;
    }

    @Override
    public String getTextoSMS() {
        if (tipoRetiro.equals(TipoRetiro.RETIRO_SIN_TARJETA)) {
            StringBuilder builder = new StringBuilder();
            builder.append("Retiro s/tarjeta por ");
            builder.append(Tools.formatAmount(modeloRetiroSinTarjeta.getImporte(), false));
            builder.append(" al ");
            builder.append(modeloRetiroSinTarjeta.getCompania().getNombre());
            builder.append(" ");
            builder.append(Tools.hideAccountNumber(modeloRetiroSinTarjeta.getCelularBeneficiario()));
            builder.append(" de ");
            builder.append(modeloRetiroSinTarjeta.getBeneficiario());
            builder.append(" Folio: ");
            builder.append(retiroSinTarjetaResult.getFolio());
            builder.append(" Codigo seg: ");
            builder.append(retiroSinTarjetaResult.getClaveRst4());
            builder.append(" Vence: ");
            builder.append(Tools.formatDate(retiroSinTarjetaResult.getVigencia()));
            builder.append(" Concepto: ");
            builder.append(modeloRetiroSinTarjeta.getConcepto());
            if (builder.toString().length() >=159) {
                return builder.toString().substring(0, 159);
            } else {
                return builder.toString();
            }
        } else {
            StringBuilder builder = new StringBuilder();
            builder.append("Retiro s/tarjeta por ");
            builder.append(Tools.formatAmount(transferencia.getImporte(), false));
            builder.append(" al ");
            builder.append(transferencia.getNombreCompania());
            builder.append(" ");
            builder.append(Tools.hideAccountNumber(transferencia.getCelularbeneficiario()));
            builder.append(" de ");
            builder.append(transferencia.getBeneficiario());
            builder.append(" Folio: ");
            builder.append(serverResponse.getFolio());
            builder.append(" Codigo seg: ");
            builder.append(serverResponse.getClaveOTP());
            builder.append(" Informe a Beneficiario Vence: ");
            builder.append(serverResponse.getFechaExpiracion() + " " + serverResponse.getHoraOperacion());
            builder.append("h Concepto: ");
            builder.append(transferencia.getConcepto());
            if (builder.toString().length() >=159) {
                return builder.toString().substring(0, 159);
            } else {
                return builder.toString();
            }
        }
    }


    public TransferenciaDineroMovil getTransferencia() {
        if(transferencia == null)
            transferencia = new TransferenciaDineroMovil();
        return transferencia;
    }

    /**
     * @param transferencia Los datos de la transferencia a establecer.
     */
    public void setTransferencia(TransferenciaDineroMovil transferencia) {
        this.transferencia = transferencia;
        //frecuente correo electronico
        aliasFrecuente = null;
        correoFrecuente = null;
    }

    /* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getDatosTablaAltaFrecuentes()
	 */
    @Override
    public ArrayList<Object> getDatosTablaAltaFrecuentes() {
        ArrayList<Object> datosAltaFrecuente = new ArrayList<Object>();
        ArrayList<String> registro;

        registro = new ArrayList<String>();
        registro.add(altaRetiroSinTarjetaViewController.getString(R.string.transferir_dineromovil_confirmacion_cuenta));
        registro.add(Tools.hideAccountNumber(transferencia.getCuentaOrigen().getNumber()));
        datosAltaFrecuente.add(registro);

        registro = new ArrayList<String>();
        registro.add(altaRetiroSinTarjetaViewController.getString(R.string.transferir_dineromovil_confirmacion_celular));
        registro.add(transferencia.getCelularbeneficiario());
        datosAltaFrecuente.add(registro);

        registro = new ArrayList<String>();
        registro.add(altaRetiroSinTarjetaViewController.getString(R.string.transferir_dineromovil_confirmacion_compania));
        registro.add(transferencia.getNombreCompania());
        datosAltaFrecuente.add(registro);

        registro = new ArrayList<String>();
        registro.add(altaRetiroSinTarjetaViewController.getString(R.string.transferir_dineromovil_confirmacion_beneficiario));
        registro.add(transferencia.getBeneficiario());
        datosAltaFrecuente.add(registro);

        registro = new ArrayList<String>();
        registro.add(altaRetiroSinTarjetaViewController.getString(R.string.transferir_dineromovil_confirmacion_concepto));
        registro.add(transferencia.getConcepto());
        datosAltaFrecuente.add(registro);

        return datosAltaFrecuente;
    }

    /* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getParametrosAltaFrecuentes()
	 */
    @Override
    public Hashtable<String, String> getParametrosAltaFrecuentes() {
        //JAIG
        Session session = Session.getInstance(SuiteApp.appContext);
        Hashtable<String, String> paramTable = null;
		paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
        paramTable.put(ServerConstants.PARAMS_TEXTO_IU, session.getIum());
		paramTable.put("TE", session.getClientNumber());
        paramTable.put("CC", "");
		paramTable.put("CA", transferencia.getCelularbeneficiario());
        paramTable.put("BF", transferencia.getBeneficiario());
		paramTable.put("CP", transferencia.getConcepto());
		paramTable.put("TP", Constants.TPDineroMovil);
        paramTable.put("OA", transferencia.getNombreCompania());
        paramTable.put("CB", "");
        paramTable.put("IM", "");
        paramTable.put("RF", "");
        paramTable.put("AP", "");
        paramTable.put("CV", "");
        return paramTable;
    }

	public RetiroSinTarjetaViewDto getDatosConfirmacion() {
		RetiroSinTarjetaViewDto retiroSinTarjetaViewDto = new RetiroSinTarjetaViewDto();
		if (tipoRetiro.equals(TipoRetiro.RETIRO_SIN_TARJETA)) {
			retiroSinTarjetaViewDto.setImporte(Tools.formatAmount(modeloRetiroSinTarjeta.getImporte(), false));
			retiroSinTarjetaViewDto.setCuentaRetiro(Tools.hideAccountNumber(modeloRetiroSinTarjeta.getCuentaOrigen().getNumber()));
			retiroSinTarjetaViewDto.setNumeroCelular(modeloRetiroSinTarjeta.getCelularBeneficiario());
			retiroSinTarjetaViewDto.setConcepto(modeloRetiroSinTarjeta.getConcepto());
		} else {
			retiroSinTarjetaViewDto.setImporte(Tools.formatAmount(transferencia.getImporte(), false));
			retiroSinTarjetaViewDto.setCuentaRetiro(Tools.hideAccountNumber(transferencia.getCuentaOrigen().getNumber()));
			retiroSinTarjetaViewDto.setNumeroCelular(transferencia.getCelularbeneficiario());
			retiroSinTarjetaViewDto.setConcepto(transferencia.getConcepto());
			retiroSinTarjetaViewDto.setNombre(transferencia.getBeneficiario());
			retiroSinTarjetaViewDto.setCompania(transferencia.getNombreCompania());
		}
		return retiroSinTarjetaViewDto;
	}

	public RetiroSinTarjetaViewDto getDatosConfirmacionRegistro() {
		RetiroSinTarjetaViewDto retiroSinTarjetaViewDto = new RetiroSinTarjetaViewDto();
		retiroSinTarjetaViewDto.setNumeroCelular(transferencia.getCelularbeneficiario());
		retiroSinTarjetaViewDto.setCompania(transferencia.getNombreCompania());
		retiroSinTarjetaViewDto.setNombre(transferencia.getBeneficiario());
		return retiroSinTarjetaViewDto;
	}

	public RetiroSinTarjetaViewDto getDatosResultados() {
		RetiroSinTarjetaViewDto retiroSinTarjetaViewDto = getDatosConfirmacion();
		if (tipoRetiro.equals(TipoRetiro.RETIRO_SIN_TARJETA)) {
			retiroSinTarjetaViewDto.setClaveRetiro(Tools.formatClaveRetiroGuiones(retiroSinTarjetaResult.getClaveRetiro()));
			retiroSinTarjetaViewDto.setCodigoSeguridad(retiroSinTarjetaResult.getClaveRst4());
			String date = Tools.formatDate(retiroSinTarjetaResult.getVigencia());
			date = date.substring(0, 6) + date.substring(8);
			retiroSinTarjetaViewDto.setVigencia(date);
			retiroSinTarjetaViewDto.setFolio(retiroSinTarjetaResult.getFolio());
		} else {
			retiroSinTarjetaViewDto.setCodigoSeguridad(serverResponse.getClaveOTP());
			String date = Tools.formatDate(serverResponse.getFechaExpiracion());
			retiroSinTarjetaViewDto.setVigencia(date);
			retiroSinTarjetaViewDto.setFolio(serverResponse.getFolio());
			retiroSinTarjetaViewDto.setCompania(transferencia.getNombreCompania());
			retiroSinTarjetaViewDto.setNombre(transferencia.getBeneficiario());
		}
		return retiroSinTarjetaViewDto;
	}
}
