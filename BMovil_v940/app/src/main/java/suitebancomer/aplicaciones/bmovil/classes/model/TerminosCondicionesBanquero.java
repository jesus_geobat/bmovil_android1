package suitebancomer.aplicaciones.bmovil.classes.model;

        import java.io.IOException;
        import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
        import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
        import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
        import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
/**
 * Created by eluna on 21/06/2016.
 */
public class TerminosCondicionesBanquero implements ParsingHandler {
    /**
     * Terminos de uso en formato HTML.
     */
    private String textoContratoGestor;

    private static TerminosCondicionesBanquero ourInstance = new TerminosCondicionesBanquero();

    public static TerminosCondicionesBanquero getInstance() {
        return ourInstance;
    }

    /**
     * @return Terminos de uso en formato HTML.
     */
    public String getTerminosHtml() {
        return textoContratoGestor;
    }

    /**
     * @param textoContratoGestor Terminos de uso en formato HTML.
     */
    public void setTerminosHtml(final String textoContratoGestor) {
        this.textoContratoGestor = textoContratoGestor;
    }

    public TerminosCondicionesBanquero() {
        textoContratoGestor = null;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        textoContratoGestor = null;
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        textoContratoGestor = parser.parseNextValue("textoContratoGestor").replace("\u0093", "\"").replace("\u0094", "\"");
    }
}
