package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;

//no por el momento
// import suitebancomer.aplicaciones.bmovil.classes.common.ContactoPatrimonialLinearLayout;
import suitebancomer.aplicaciones.bmovil.classes.common.ContactoPatrimonialLinearLayout;
import suitebancomer.aplicaciones.bmovil.classes.common.DetalleDePosicionPatrimonialAdapter;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetallePosicionPatrimonialDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.DetallePosicionPatrimonial;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by OOROZCO on 3/17/16.
 */
public class DetallePosicionPatrimonialViewController extends BaseViewController {

    private ListView listDetallePosion;
    private LinearLayout header;
    private ScrollView body_layout;
    private LinearLayout title_layout;
    private ImageView title_divider;
    private LinearLayout mainContent;
    private View btnContacto;
    private TextView txt_label;
    private TextView txt_amount;

    private ArrayList<String> tags;
    private ArrayList<String> amounts;
    private TextView txtNumContrato;
    private TextView txtCuentaAsociada;

    private DetallePosicionPatrimonialDelegate delegate;
    private DetallePosicionPatrimonial detalle;

    private LinearLayout FatherBackground ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_detalle_posicion_patrimonial);
        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        delegate = (DetallePosicionPatrimonialDelegate) parentViewsController.getBaseDelegateForKey(DetallePosicionPatrimonialDelegate.DETALLE_POSICION_PATRIMONIAL_DELEGATE);

                linkUIElements();
        customizeView();
        setData();
    }

    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        /**
         * Patrimonial Sub-Version
         */
     //   ((ContactoPatrimonialLinearLayout)btnContacto).analyzeResponse(response);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio())
            return;
        getParentViewsController().setCurrentActivityApp(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    private void linkUIElements()
    {
        FatherBackground =(LinearLayout)findViewById(R.id.FatherView);
        header = (LinearLayout) findViewById(R.id.header_layout);
        body_layout = (ScrollView) findViewById(R.id.body_layout);
        title_layout = (LinearLayout) findViewById(R.id.title_layout);
        title_divider = (ImageView) findViewById(R.id.title_divider);
        mainContent = (LinearLayout) findViewById(R.id.main_content);
        listDetallePosion = (ListView)findViewById(R.id.listDetallePosicion);
        txt_amount = (TextView) findViewById(R.id.txt_amount);
        txt_label = (TextView)findViewById(R.id.txt_label);
        txtNumContrato = (TextView) findViewById(R.id.txtNumContrato);
        txtCuentaAsociada = (TextView) findViewById(R.id.txtCuentaAsociada);
        /**
         * Contacto Patrimonial
         */
       btnContacto = LayoutInflater.from(this).inflate(R.layout.layout_bmovil_btn_contacto_patrimonial, mainContent, false);
    }

    private void customizeView()
    {
        setTitle(R.string.detalle_posicion, R.drawable.bmovil_mis_cuentas_icono,true);
        findViewById(R.id.title_divider).setVisibility(View.GONE);

        ArrayList <View> objectsToBeMeasured = new ArrayList<View>();
        //objectsToBeMeasured.add(header);
        objectsToBeMeasured.add(btnContacto);
        objectsToBeMeasured.add(title_layout);
        //objectsToBeMeasured.add(title_divider);

        //This is calculated because of the margins set on layout_base.
        //10 dp from header_layout.
        //12 dp from title_divider.
        int margins = 60;

        //Set fixed height to ScrollView body layout content.
        ViewGroup.LayoutParams params = body_layout.getLayoutParams();
        params.height = suitebancomer.aplicaciones.bmovil.classes.common.Tools.getPatrimonialScrollViewHeight(objectsToBeMeasured,this,margins);
        body_layout.setLayoutParams(params);
        body_layout.requestLayout();
        body_layout.setFillViewport(false);

        //Set new View to layout_base
       mainContent.addView(btnContacto);
        ((ContactoPatrimonialLinearLayout)btnContacto).setValues(this);

        HashMap<Integer,TextView> views = new HashMap<Integer, TextView>();
         views = Tools.getTextViews_id((ViewGroup) findViewById(R.id.FatherView));
        for(Integer id : views.keySet()){
            int id_view = id;
            TextView view = (TextView)findViewById(id);
            view.setTextColor(getResources().getColor(R.color.primer_azul));
            view.setBackgroundColor(Color.parseColor("#00000000"));
        }

    }

    private void setData()
    {
        detalle = delegate.getDetalle();

        populateLists();

        listDetallePosion.setAdapter(new DetalleDePosicionPatrimonialAdapter(this,tags,amounts));

        ListAdapter listAdapter = listDetallePosion.getAdapter();
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listDetallePosion);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listDetallePosion.getLayoutParams();
        params.height = totalHeight;
        listDetallePosion.setLayoutParams(params);
        listDetallePosion.requestLayout();
        //listDetallePosion.setFocusable(false);

        String numContrato = detalle.getContratoBpigo();
        String cuentaEje = detalle.getPromotor().getCuentaEje();

        txtNumContrato.setText("*" + numContrato.substring(numContrato.length()-5));
        txtCuentaAsociada.setText("*" + cuentaEje.substring(cuentaEje.length()-5));

        txt_label.setText(getResources().getString(R.string.bmovil_patrimonial_totalCartera));
        txt_amount.setText(Tools.normalizaerResponce(Tools.formatPatrimonialAmount(detalle.getTotalCartera(), false)));

        txt_amount.setTextColor(getResources().getColor(R.color.celdaSaldos));
        ((ContactoPatrimonialLinearLayout)btnContacto).setNumCartera(detalle.getContratoBpigo());
    }

    private void populateLists()
    {

        tags = new ArrayList<String>();
        amounts = new ArrayList<String>();

        if(detalle.getInversionesBancarias().trim().length()>0 && !detalle.getInversionesBancarias().equals("0.00"))
        {
            tags.add(getResources().getString(R.string.bmovil_patrimonial_inversionesBancarias));
            amounts.add(Tools.normalizaerResponce(Tools.formatPatrimonialAmount(detalle.getInversionesBancarias().replace("$", "").replaceAll(",", ""), false)));
        }

        if(detalle.getFondosDeuda().trim().length()>0 && !detalle.getFondosDeuda().equals("0.00"))
        {
            tags.add(getResources().getString(R.string.bmovil_patrimonial_fondosDeuda));
            amounts.add(Tools.normalizaerResponce(Tools.formatPatrimonialAmount(detalle.getFondosDeuda().replace("$", "").replaceAll(",", ""), false)));
        }

        if(detalle.getFondosRentaVariable().trim().length()>0 && !detalle.getFondosRentaVariable().equals("0.00"))
        {
            tags.add(getResources().getString(R.string.bmovil_patrimonial_fondosRentaVariable));
            amounts.add(Tools.normalizaerResponce(Tools.formatPatrimonialAmount(detalle.getFondosRentaVariable().replace("$", "").replaceAll(",", ""), false)));
        }

        if(detalle.getMdoDineroReportos().trim().length()>0 && !detalle.getMdoDineroReportos().equals("0.00"))
        {
            tags.add(getResources().getString(R.string.bmovil_patrimonial_mdoDineroReportos));
            amounts.add(Tools.normalizaerResponce(Tools.formatPatrimonialAmount(detalle.getMdoDineroReportos().replace("$", "").replaceAll(",", ""), false)));
        }

        if(detalle.getMercadoCapitales().trim().length()>0 && !detalle.getMercadoCapitales().equals("0.00"))
        {
            tags.add(getResources().getString(R.string.bmovil_patrimonial_mercadoCapitales));
            amounts.add(Tools.normalizaerResponce(Tools.formatPatrimonialAmount(detalle.getMercadoCapitales().replace("$", "").replaceAll(",", ""), false)));
        }

        if(detalle.getObligaciones().trim().length()>0 && !detalle.getObligaciones().equals("0.00"))
        {
            tags.add(getResources().getString(R.string.bmovil_patrimonial_obligaciones));
            amounts.add(Tools.normalizaerResponce(Tools.formatPatrimonialAmount(detalle.getObligaciones().replace("$", "").replaceAll(",", ""), false)));
        }

        if(detalle.getEstructurados().trim().length()>0 && !detalle.getEstructurados().equals("0.00"))
        {
            tags.add(getResources().getString(R.string.bmovil_patrimonial_estructurados));
            amounts.add(Tools.normalizaerResponce(Tools.formatPatrimonialAmount(detalle.getEstructurados().replace("$", "").replaceAll(",", ""), false)));
        }

        if(detalle.getOtros().trim().length()>0 && !detalle.getOtros().equals("0.00"))
        {
            tags.add(getResources().getString(R.string.bmovil_patrimonial_otros));
            amounts.add(Tools.normalizaerResponce(Tools.formatPatrimonialAmount(detalle.getOtros().replace("$", "").replaceAll(",", ""), false)));
        }

        if(detalle.getEfectivoDisponible().trim().length()>0 && !detalle.getEfectivoDisponible().equals("0.00"))
        {
            tags.add(getResources().getString(R.string.bmovil_patrimonial_efectivoDisponible));
            amounts.add(Tools.normalizaerResponce(Tools.formatPatrimonialAmount(detalle.getEfectivoDisponible().replace("$", "").replaceAll(",", ""), false)));
        }

    }



}
