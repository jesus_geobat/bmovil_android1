package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

        import bancomer.api.common.commons.Constants;

/**
 * Created by elunag on 29/07/16.
 */
public class ConfirmacionBanqueroDelegate extends DelegateBaseOperacion
{
    @Override
    public boolean mostrarContrasenia() {
        return false;
    }

    @Override
    public Constants.TipoOtpAutenticacion tokenAMostrar() {
        return null;
    }

    @Override
    public boolean mostrarNIP() {
        return false;
    }

    @Override
    public boolean mostrarCVV() {
        return false;
    }

    @Override
    public boolean mostrarCampoTarjeta() {
        return false;
    }

    @Override
    public String getTextoTituloResultado() {
        return null;
    }

    @Override
    public String getTextoAyudaNIP() {
        return null;
    }

    @Override
    public String getTextoAyudaCVV() {
        return null;
    }

    @Override
    public String loadOtpFromSofttoken(Constants.TipoOtpAutenticacion tipoOTP) {
        return loadOtpFromSofttoken(tipoOTP,this);
    }


}
