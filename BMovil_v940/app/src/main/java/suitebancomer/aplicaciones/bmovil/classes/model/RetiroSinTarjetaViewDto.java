package suitebancomer.aplicaciones.bmovil.classes.model;

/**
 * Created by andres.vicentelinare on 28/09/2016.
 */
public class RetiroSinTarjetaViewDto {

    private String importe;
    private String cuentaRetiro;
    private String numeroCelular;
    private String concepto;

    private String claveRetiro;
    private String codigoSeguridad;
    private String vigencia;
    private String folio;

    private String nombre;
    private String compania;

    public RetiroSinTarjetaViewDto() {
        this.importe = "";
        this.cuentaRetiro = "";
        this.numeroCelular = "";
        this.concepto = "";
        this.claveRetiro = "";
        this.codigoSeguridad = "";
        this.vigencia = "";
        this.folio = "";
        this.nombre = "";
        this.compania = "";
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getCuentaRetiro() {
        return cuentaRetiro;
    }

    public void setCuentaRetiro(String cuentaRetiro) {
        this.cuentaRetiro = cuentaRetiro;
    }

    public String getNumeroCelular() {
        return numeroCelular;
    }

    public void setNumeroCelular(String numeroCelular) {
        this.numeroCelular = numeroCelular;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getClaveRetiro() {
        return claveRetiro;
    }

    public void setClaveRetiro(String claveRetiro) {
        this.claveRetiro = claveRetiro;
    }

    public String getCodigoSeguridad() {
        return codigoSeguridad;
    }

    public void setCodigoSeguridad(String codigoSeguridad) {
        this.codigoSeguridad = codigoSeguridad;
    }

    public String getVigencia() {
        return vigencia;
    }

    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }
}
