package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.SessionStoredListener;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BorrarDatosViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;
import android.content.DialogInterface;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class BorrarDatosDelegate extends BaseDelegate implements SessionStoredListener {

	public final static long BORRAR_DATOS_DELEGATE_ID = 0x59e2b4432c15aa46L;
	
	private BorrarDatosViewController borrarDatosViewController;
	
	public void setBorrarDatosViewController(BorrarDatosViewController viewController) {
		this.borrarDatosViewController = viewController;
	}
	
	public void desactivarAplicacion() {
		Session.getInstance(SuiteApp.appContext).clearSession();
		final BorrarDatosDelegate me = this;
		borrarDatosViewController.muestraIndicadorActividad("", borrarDatosViewController.getString(R.string.alert_StoreSession));
		Session.getInstance(SuiteApp.appContext).clearSession();
		Session.getInstance(SuiteApp.appContext).storeSession(me);
	}
	
	@Override
	public void sessionStored() {
		borrarDatosViewController.ocultaIndicadorActividad();
		borrarDatosViewController.runOnUiThread(new Runnable(){
			@Override
			public void run() {
				borrarDatosViewController.showInformationAlert(R.string.borrarDatos_dialogSucceed, 
						new DialogInterface.OnClickListener() {	
					@Override
					public void onClick(DialogInterface dialog, int which) {
						borrarDatosViewController.goBack();
					}
				});				
			}
		});
	}
}
