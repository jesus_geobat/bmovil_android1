package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.AbstractContactMannager;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DineroMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountField;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.SeleccionHorizontalViewController;
import tracking.TrackingHelper;


public class RegistrarParaAlguienMasViewController extends BaseViewController {
	/**
	 * Codigo de la petici�n de un contacto desde la agenda.
	 */
	public static final int ContactsRequestCode = 1;

	/**
	 * Contenedor para el componente SeleccionHorizontal.
	 */
	private LinearLayout seleccionHorizontalLayout;

	/**
	 * El componente SeleccionHorizontal.
	 */
	private SeleccionHorizontalViewController seleccionHorizontal;

	/**
	 * El delegado para este controlador.
	 */
	private DineroMovilDelegate delegate;

	/**
	 * Campo del número de telefono.
	 */
	private EditText campoNumeroTelefono;
    private String telefono;

	/**
	 * Campo de beneficiario.
	 */
	private EditText campoBeneficiario;


	/**
	 * Bandera para indicar si se estan pidiendo contactos.
	 */
	private boolean pidiendoContactos;
	//AMZ
	public BmovilViewsController parentManager;
			//AMZ

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_transferir_alguien_mas);
		//setTitle(R.string.transferir_dineromovil_titulo, R.drawable.bmovil_dinero_movil_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

		TrackingHelper.trackState("nuevo", parentManager.estados);

		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID));
		delegate = (DineroMovilDelegate)getDelegate();

		init();
	}

	/**
	 * Inicializa la vista.
	 */
	private void init() {

		pidiendoContactos = false;
		delegate.setViewController(this);
		delegate.setTipoOperacion(Constants.Operacion.dineroMovil);
		findViews();
		scaleForCurrentScreen();
		cargarSeleccionHorizontal();
		campoBeneficiario.addTextChangedListener(new BmovilTextWatcher(this));
		campoNumeroTelefono.addTextChangedListener(new BmovilTextWatcher(this));
	}

	/**
	 * Busca las vistas en los layouts.
	 */
	private void findViews() {
		seleccionHorizontalLayout = (LinearLayout)findViewById(R.id.ram_seleccionHorizontalLayout);
		campoNumeroTelefono = (EditText)findViewById(R.id.ram_textNumeroCel);
		campoBeneficiario = (EditText)findViewById(R.id.ram_textBeneficiario);
	}

	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scale(findViewById(R.id.ram_rootLayout));

		guiTools.scale(findViewById(R.id.ram_lblCompania), true);
		guiTools.scale(seleccionHorizontalLayout);

		guiTools.scale(findViewById(R.id.ram_lblBeneficiario), true);
		guiTools.scale(campoBeneficiario, true);

		guiTools.scale(findViewById(R.id.ram_lblNumeroCel), true);
		guiTools.scale(findViewById(R.id.ram_btnAgenda));
		guiTools.scale(campoNumeroTelefono, true);

		guiTools.scale(findViewById(R.id.ram_btnContinuar));
	}


	/**
	 * Carga el componente SeleccionHorizontal.
	 */
	private void cargarSeleccionHorizontal() {
		//tituloSeleccionHorizontal.setText(getString(R.string.servicesPayment_servicesComponentTitle));
		@SuppressWarnings("deprecation")
		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		ArrayList<Object> companias = delegate.cargarListaCompanias();
		seleccionHorizontal = new SeleccionHorizontalViewController(this, params, companias, getDelegate(), false);
		seleccionHorizontalLayout.addView(seleccionHorizontal);
	}
	
	/**
	 * Intenta obtener un contacto desde la agenda del dispoitivo.
	 * @param view La vista que manda a llamar este método.
	 */
	public void onBotonContactoClick(View view) {
		if(pidiendoContactos)
			return;
		pidiendoContactos = true;
		AbstractContactMannager.getContactManager().requestContactData(this);
	}
	
	/*
	 * No javadoc. 
	 */
	@Override  
	public void onActivityResult(int reqCode, int resultCode, Intent data) {  
	    super.onActivityResult(reqCode, resultCode, data);
	    pidiendoContactos = false;
		if (data != null) {
			Uri uri = data.getData();
			if (uri != null) {
				Cursor c = null;
				try {
					c = getContentResolver().query(uri, new String[]{
									ContactsContract.CommonDataKinds.Phone.NUMBER,
									ContactsContract.Contacts.DISPLAY_NAME },
							null, null, null);
					if (c != null && c.moveToFirst()) {
						obtenerContacto(c);
					}
				} finally {
					if (c != null) {
						c.close();
					}
				}
			}
		}
	}
	
	/**
	 * Obtiene los datos del contacto del contacto seleccionado.
	 * @param data Los datos del Intent.
	 */
    public void obtenerContacto(Cursor cursor) {
        telefono = cursor.getString(0).replaceAll("\\D", "");
        if (telefono != null && telefono.length() >=10) {
            campoNumeroTelefono.setText(telefono.substring(telefono.length() - 10));
        }
        campoBeneficiario.setText(cursor.getString(1));
    }


    /**
	 * Bandera que indica si se esta operando con un frecuente.
	 */
	private boolean desdeFrecuente = false;
	
	/**
	 * @return La bandera que indica si se esta operando con un frecuente.
	 */
	public boolean isDesdeFrecuente() {
		return desdeFrecuente;
	}

	/**
	 * @param desdeFrecuente Valor a establecer para bandera que indica si se esta operando con un frecuente.
	 */
	public void setDesdeFrecuente(boolean desdeFrecuente) {
		this.desdeFrecuente = desdeFrecuente;
	}

	/**
	 * Carga y bloquea los campos obtenidos desde un frecuente.
	 */
	
	public void botonContinuarClick(View view) {
		if(!isHabilitado())
			return;
		setHabilitado(false);
		if(parentViewsController.isActivityChanging())
			return;
		TransferenciaDineroMovil model = delegate.getTransferencia();
		
		model.setCuentaOrigen(null);
		//model.setConcepto(null);
		
		HashMap<String, String> mapa = new HashMap<String, String>();
		// TODO Verificar este cast.
		Compania compania = (Compania)seleccionHorizontal.getSelectedItem();
		mapa.put(compania.getClave(), compania.getNombre());
		model.setCompania(mapa);
		model.setNombreCompania(compania.getNombre());
		
		delegate.validaDatos(campoNumeroTelefono.getText().toString(),
							 Tools.removeSpecialCharacters(campoBeneficiario.getText().toString()));
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}
		
	@Override
	protected void onResume() {	
		super.onResume();
		setHabilitado(true);
		if(delegate != null)
			delegate.setViewController(this);
		if(!pidiendoContactos) {
			if (parentViewsController.consumeAccionesDeReinicio()) {
				return;
			}
			getParentViewsController().setCurrentActivityApp(this);
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if(!pidiendoContactos) {
			parentViewsController.consumeAccionesDePausa();
		}
	}
	
	@Override
	public void goBack() {
		if(!pidiendoContactos) {
			parentViewsController.removeDelegateFromHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
		}
		super.goBack();
	}

	
	/**
	 * Carga los elementos desde un r�pido.
	 */
	private void cargarDatosDeRapido() {
		// Establece datos.
		seleccionHorizontal.setSelectedItem(delegate.getCompaniaSeleccionada());
		campoNumeroTelefono.setText(delegate.getTransferencia().getCelularbeneficiario());
		campoBeneficiario.setText(delegate.getTransferencia().getBeneficiario());
		
		// Deshbilita los campos precargados.
		(findViewById(R.id.btnAgenda)).setEnabled(false);
		seleccionHorizontal.bloquearComponente();
		campoNumeroTelefono.setEnabled(false);
		campoBeneficiario.setEnabled(false);
	}

}
