package suitebancomer.aplicaciones.bmovil.classes.model;

        import java.io.IOException;
        import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
        import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
        import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
        import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
/**
 * Created by eluna on 22/06/2016.
 */
public class ContratoGestor implements ParsingHandler
{
    private String nombreGestor;
    private String telefono;
    private String extension;
    private String emailGestor;
    private String folio;
    private String fechaOperacion;
    private String horaOperacion;

    private static ContratoGestor ourInstance = new ContratoGestor();

    public static ContratoGestor getInstance() {
        return ourInstance;
    }

    private ContratoGestor() {
    }

    public String getNombreGestor() {
        return nombreGestor;
    }

    public void setNombreGestor(String nombreGestor) {
        this.nombreGestor = nombreGestor;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getEmailGestor() {
        return emailGestor;
    }

    public void setEmailGestor(String emailGestor) {
        this.emailGestor = emailGestor;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(String fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public String getHoraOperacion() {
        return horaOperacion;
    }

    public void setHoraOperacion(String horaOperacion) {
        this.horaOperacion = horaOperacion;
    }

    public static ContratoGestor getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(ContratoGestor ourInstance) {
        ContratoGestor.ourInstance = ourInstance;
    }


    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        nombreGestor = parser.parseNextValue("GESTOR");
        telefono = parser.parseNextValue("TEL1");
        extension = parser.parseNextValue("EXT1");
        emailGestor = parser.parseNextValue("EMAIL");
        folio = parser.parseNextValue("FOLIO");
        fechaOperacion = parser.parseNextValue("FECAUT");
        horaOperacion = parser.parseNextValue("HORAUT");
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub
    }

}
