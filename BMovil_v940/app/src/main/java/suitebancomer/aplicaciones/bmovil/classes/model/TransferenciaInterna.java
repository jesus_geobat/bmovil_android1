package suitebancomer.aplicaciones.bmovil.classes.model;

/**
 * Inner transaction model class.
 */
public class TransferenciaInterna {
	/**
	 * The source account.
	 */
	private Account cuentaOrigen;
	
	/**
	 * The destination account.
	 */
	private Account cuentaDestino;
	
	/**
	 * The amount of the transaction.
	 */
	private String importe;
	
	/**
	 * The frequent nickname.
	 */
	private String aliasFrecuente;
	
	/**
	 * The operation type.
	 */
	private String tipoOperacion;
	
	/**
	 * Motivo de Pago
	 */
	private String motivoPago;
	
	
	/**
	 * @return The transaction source account.
	 */
	public Account getCuentaOrigen() {
		return cuentaOrigen;
	}
	
	/**
	 * @param cuentaOrigen The transaction source account to set.
	 */
	public void setCuentaOrigen(Account cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}
	
	/**
	 * @return The transaction destination account.
	 */
	public Account getCuentaDestino() {
		return cuentaDestino;
	}
	
	/**
	 * @param cuentaDestino The transaction destination account to set.
	 */
	public void setCuentaDestino(Account cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}
	
	/**
	 * @return The transaction amount.
	 */
	public String getImporte() {
		return importe;
	}
	
	/**
	 * @param importe The transaction amount to set.
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}
	
	/**
	 * @return The frequent nickname.
	 */
	public String getAliasFrecuente() {
		return aliasFrecuente;
	}
	
	/**
	 * @param aliasFrecuente The frequent nickname to set.
	 */
	public void setAliasFrecuente(String aliasFrecuente) {
		this.aliasFrecuente = aliasFrecuente;
	}
	
	/**
	 * @return The operation type.
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	
	/**
	 * @param tipoOperacion The operation type to set.
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	/**
	 * Default constructor
	 */
	public TransferenciaInterna() {
		cuentaOrigen = null;
		cuentaDestino = null;
		importe = "";
		aliasFrecuente = "";
		tipoOperacion = "";
	}
	
	/**
	 * Full constructor.
	 * @param cuentaOrigen The source account.
	 * @param cuentaDestino The destination account.
	 * @param importe The transaction amount.
	 * @param aliasFrecuente The frequent nickname.
	 * @param tipoOperacion The operation type.
	 */
	public TransferenciaInterna(Account cuentaOrigen, Account cuentaDestino, String importe, String aliasFrecuente, String tipoOperacion) {
		this.cuentaOrigen = cuentaOrigen;
		this.cuentaDestino = cuentaDestino;
		this.importe = importe;
		this.aliasFrecuente = aliasFrecuente;
		this.tipoOperacion = tipoOperacion;
	}
	public String getMotivoPago() {
		return motivoPago;
	}

	public void setMotivoPago(String motivoPago) {
		this.motivoPago = motivoPago;
	}
}
