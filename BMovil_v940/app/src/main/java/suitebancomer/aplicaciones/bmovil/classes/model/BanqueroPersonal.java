package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by eluna on 01/06/2016.
 */
public class BanqueroPersonal implements ParsingHandler {

    private String nombreGestor;
    private String telefono1;
    private String extension1;
    private String telefono2;
    private String extension2;
    private String email;

    private static BanqueroPersonal ourInstance = new BanqueroPersonal();

    public static BanqueroPersonal getInstance() {
        return ourInstance;
    }

    private BanqueroPersonal() {
    }

    public String getNombreGestor() {
        return nombreGestor;
    }

    public void setNombreGestor(String nombreGestor) {
        this.nombreGestor = nombreGestor;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getExtension1() {
        return extension1;
    }

    public void setExtension1(String extension1) {
        this.extension1 = extension1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getExtension2() {
        return extension2;
    }

    public void setExtension2(String extension2) {
        this.extension2 = extension2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        nombreGestor = parser.parseNextValue("nombreGestor");
        telefono1 = parser.parseNextValue("telefono1");
        extension1 = parser.parseNextValue("extension1");
        telefono2 = parser.parseNextValue("telefono2");
        extension2 = parser.parseNextValue("extension2");
        email = parser.parseNextValue("email");
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub
    }
}
