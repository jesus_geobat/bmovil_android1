package suitebancomer.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bancomer.mbanking.R;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MisCuentasViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.ContratoPatrimonial;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by OOROZCO on 3/22/16.
 */
public class SpinnerPatrimonialListView extends ListView {

    private ArrayList<String> opcionesSpinner;
    private BaseViewController controller;
    private ContratoPatrimonial contrato;

    public void setController(BaseViewController controller){this.controller = controller;}

    public void setContrato(ContratoPatrimonial contrato){this.contrato = contrato;}

    public SpinnerPatrimonialListView(final Context context, AttributeSet attrs)
    {
        super(context,attrs);
        opcionesSpinner = new ArrayList<String>();
        //opcionesSpinner.add(context.getString(R.string.bmovil_patrimonial_op1_spinner));
        opcionesSpinner.add(context.getString(R.string.bmovil_patrimonial_op2_spinner));
        //opcionesSpinner.add(context.getString(R.string.bmovil_patrimonial_op3_spinner));

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                R.layout.bmovil_spinner_contratos_text_view, R.id.txtOpcionSpinner, opcionesSpinner);

        setOnItemClickListener( new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectFlowAccordingOption(position);
            }
        });

        setAdapter(adapter);

    }



    private void selectFlowAccordingOption(int position)
    {

        if(opcionesSpinner.get(position).equals(controller.getString(R.string.bmovil_patrimonial_op2_spinner)))
        {
            ((MisCuentasViewController) controller).showDetalleDePosicionPatrimonial(contrato);
        }


        System.out.println("Seleccion Detalle Pat");

    }
}
