package suitebancomer.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


/**
 * Created by OOROZCO on 4/13/16.
 */
public class TextViewHeaderPatrimonial extends TextView {

    public TextViewHeaderPatrimonial(Context context) {
        super(context,null,0);
    }

    public TextViewHeaderPatrimonial(Context context, AttributeSet attrs) {
        super(context, attrs,0);
    }

    public TextViewHeaderPatrimonial(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface bold = Typeface.createFromAsset(context.getAssets(),"fonts/stag_sans_book.ttf");
        setTypeface(bold);
    }
}
