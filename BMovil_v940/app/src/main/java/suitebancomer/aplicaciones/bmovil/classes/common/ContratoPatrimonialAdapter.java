package suitebancomer.aplicaciones.bmovil.classes.common;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.mbanking.R;

import java.util.ArrayList;
import java.util.Hashtable;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ContactoPatrimonialViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.ContratoPatrimonial;
import suitebancomer.aplicaciones.bmovil.classes.model.DetallePosicionPatrimonial;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by OOROZCO on 3/11/16.
 */
public class ContratoPatrimonialAdapter extends BaseAdapter {


    private ArrayList<ContratoPatrimonial> arregloPatrimonial;
    private Context context;
    private Dialog dialogSpinner;
    private boolean isFromContactoView;
    Typeface light;
    private ContactoPatrimonialViewController controller;


    @Override
    public boolean isEnabled(int position) {
        //return super.isEnabled(position);
        return false;
    }

    public ContratoPatrimonialAdapter(ArrayList<ContratoPatrimonial> data, Context context, boolean flag)
    {
        arregloPatrimonial = data;
        this.context = context;
        isFromContactoView = flag;
        light = Typeface.createFromAsset(context.getAssets(), "fonts/stag_sans_light.otf");
    }

    @Override
    public int getCount() {
        return arregloPatrimonial.size();
    }

    @Override
    public Object getItem(int position) {
        return arregloPatrimonial.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final int innerPosition = position;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        if(convertView == null)
            convertView = inflater.inflate(R.layout.list_item_productos_patrimonial, null);



        TextView celda1 = (TextView) convertView.findViewById(R.id.lista_celda_1);
        TextView celda2 = (TextView) convertView.findViewById(R.id.lista_celda_2);

        LinearLayout.LayoutParams params = new LayoutParams(0, LayoutParams.FILL_PARENT, 0.6f);
        LinearLayout.LayoutParams params2 = new LayoutParams(0, LayoutParams.FILL_PARENT, 0.4f);

        celda1.setText(arregloPatrimonial.get(position).getNumCuentaTabla());
        celda1.setTypeface(light);
        celda1.setLayoutParams(params);
        celda1.setGravity(Gravity.LEFT);
        celda1.setTextAppearance(context, R.style.movimientos_celda_style);

        System.out.println("dam nonne 0 :" + Tools.normalizaerResponce(Tools.formatPatrimonialAmount(arregloPatrimonial.get(position).getTotalCartera(), false)));

        celda2.setText("$"+Tools.normalizaerResponce(Tools.formatPatrimonialAmount(arregloPatrimonial.get(position).getTotalCartera(), false)));
        celda2.setTypeface(light);
        celda1.setLayoutParams(params2);
        celda2.setGravity(Gravity.RIGHT);
        celda2.setSingleLine(true);
        celda2.setTextAppearance(context, R.style.movimientos_celda_style);


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Not Jet
                  */
                if(isFromContactoView){
                    controller = (ContactoPatrimonialViewController)context;
                    if(controller.getDialogContratos().isShowing())
                        controller.getDialogContratos().dismiss();

                    controller.getDelegate().setDetalle(arregloPatrimonial.get(position).getContrato());

                    controller.setDataToView();
                }
                else {
                   // doRequest(innerPosition);

                    dialogAction(innerPosition);
                    System.out.println("Seleccion Interna");
                }

            }

        });

        return convertView;
    }

    public void dismissDialog()
    {
        if(dialogSpinner.isShowing())
            dialogSpinner.dismiss();
    }

    private void dialogAction(int position)
    {
        dialogSpinner = new Dialog(context);
        dialogSpinner.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSpinner.getWindow().setBackgroundDrawable(new ColorDrawable(00000000));
        dialogSpinner.setContentView(R.layout.dialog_bmovil_patrimonial_mis_contratos);
        ((SpinnerPatrimonialListView)dialogSpinner.findViewById(R.id.listSpinnerContratos)).setController(((BaseViewController) context));
        ((SpinnerPatrimonialListView)dialogSpinner.findViewById(R.id.listSpinnerContratos)).setContrato(arregloPatrimonial.get(position));
        dialogSpinner.show();
    }


    private void doRequest(int position)
    {
        Session session = Session.getInstance(context);
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ServerConstants.OPERACION, context.getString(R.string.bmovil_patrimonial_operacion));
        params.put(ServerConstants.TELEFONO_TAG, session.getUsername());
        params.put(ServerConstants.IUM_TAG, session.getIum());
        String numCuenta = arregloPatrimonial.get(position).getNumCuenta();
        int length = numCuenta.length();
        params.put(ServerConstants.NUMCUENTA_TAG, numCuenta.substring(length - 10));
        params.put(ServerConstants.TIPOCONSULTA_TAG, context.getString(R.string.bmovil_patrimonial_tipo_consulta));
     //   ((BmovilViewsController)((BaseViewController)context).getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.OP_PATRIMONIAL, params,true, new DetallePosicionPatrimonial(), Server.isJsonValueCode.NONE, ((BaseViewController) context),false);

    }

}
