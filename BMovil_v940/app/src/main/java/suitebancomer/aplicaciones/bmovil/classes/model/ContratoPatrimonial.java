package suitebancomer.aplicaciones.bmovil.classes.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by OOROZCO on 4/14/16.
 */
public class ContratoPatrimonial {


    private String numCuenta;
    private String totalCartera;
    private String numCuentaTabla;
    private DetallePosicionPatrimonial Contrato ;
    private String ContratoB;



    public String getNumCuenta() {
        return numCuenta;
    }

    public  DetallePosicionPatrimonial getContrato() {
        return Contrato;
    }


    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getTotalCartera() {
        return totalCartera;
    }

    public void setTotalCartera(String totalCartera) {
        this.totalCartera = totalCartera;
    }


    public String getNumCuentaTabla() {
        return numCuentaTabla;
    }

    public void setNumCuentaTabla(String numCuentaTabla) {
        this.numCuentaTabla = numCuentaTabla;
    }

    public  ContratoPatrimonial(String numCuenta, String totalCartera, JSONObject Cuenta)
    {
        setContratoB(Cuenta);
        this.numCuenta = numCuenta;
      //  this.numCuentaTabla = "Contrato MX *"+ numCuenta.substring(numCuenta.length()-5);
        this.numCuentaTabla = "Contrato MX *"+ getContratoB().substring(numCuenta.length() - 5);

        char[] auxiliar = totalCartera.toCharArray();
        int index;

        for(index = 0 ; index < totalCartera.length() ; index ++)
        {
            if(auxiliar[index] != '0')
                break;
        }

        this.totalCartera = totalCartera.substring(index);
        Contrato =new DetallePosicionPatrimonial(Cuenta);



    }

    public String getContratoB() {
        return ContratoB;
    }

    public void setContratoB(JSONObject Cuenta ) {
        ContratoB = newContratoB(Cuenta);
    }

    public String newContratoB(JSONObject Cuenta ){

        try{
        JSONObject detalleContrato = Cuenta.getJSONObject("detalleposicion");


        return detalleContrato.getString("contratoBpigo").trim();
}
catch (JSONException ex){
    System.out.println("Exception Busqueda" + ex.getMessage());
    ex.printStackTrace();
}
        return null;
    }

}
