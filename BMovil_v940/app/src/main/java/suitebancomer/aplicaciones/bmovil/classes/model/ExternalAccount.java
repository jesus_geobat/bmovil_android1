package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.Serializable;

/**
 * Created by elunag on 21/07/16.
 */
public class ExternalAccount implements Serializable
{

    private String externalAccount;
    private String description;
    private String referenceNumber;
    private String referenceNumberType;

    public ExternalAccount(String externalAccount, String description, String referenceNumber, String referenceNumberType) {
        this.externalAccount = externalAccount;
        this.description = description;
        this.referenceNumber = referenceNumber;
        this.referenceNumberType = referenceNumberType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getReferenceNumberType() {
        return referenceNumberType;
    }

    public void setReferenceNumberType(String referenceNumberType) {
        this.referenceNumberType = referenceNumberType;
    }

    public String getExternalAccount() {
        return externalAccount;
    }

    public void setExternalAccount(String externalAccount) {
        this.externalAccount = externalAccount;
    }
}
