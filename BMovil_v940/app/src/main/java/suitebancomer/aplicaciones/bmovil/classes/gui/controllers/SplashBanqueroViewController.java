package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

        import android.app.Activity;
        import android.os.Bundle;
        import android.os.Handler;
        import android.os.Message;
        import android.util.DisplayMetrics;
        import android.view.KeyEvent;
        import android.view.MotionEvent;
        import android.widget.ImageView;
        import com.adobe.mobile.Config;
        import com.bancomer.mbanking.R;
        import com.bancomer.mbanking.SuiteApp;
        import java.util.Timer;
        import java.util.TimerTask;
/**
 * Created by eluna on 21/06/2016.
 */
public class SplashBanqueroViewController extends Activity {

    /**
     * The timer for presenting the splash
     */
    private Timer splashTimer;

    /**
     * The action performed when the timer expires
     */
    private TimerTask splashTimerTask;

    private SuiteApp suiteApp;

    private boolean isAppCanceled;



    /**
     * Default constructor of this activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_bmovil_splash_banquero);

        int imagen = getIntent().getIntExtra("tipoRespuesta",0);

        ImageView img = (ImageView)findViewById(R.id.splashBanquero);
        img.setImageResource(imagen);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        img.getLayoutParams().height = metrics.heightPixels;
        //AMZ
        Config.setContext(this.getApplicationContext());
        //AMZFIN
    }

    /**
     * Method used when this activity awakes from pause state
     */
    @Override
    protected void onResume() {
        super.onResume();
        //AMZ
        Config.collectLifecycleData();

        splashTimer = new Timer();
        splashTimerTask = new SplashTask();

        splashTimer.schedule(splashTimerTask, 5000);
    }

    @Override
    protected void onPause() {
        //goBack();
        super.onPause();
        //AMZ
        Config.pauseCollectingLifecycleData();
        //AMZFIN
        splashTimer.cancel();
    }

    /**
     * Overrides and cancels android's back button, if the button is visible then it does the same
     * as clicking on the application's back button.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode){
            case KeyEvent.KEYCODE_BACK:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }

    }

    /**
     * Use handler to respond to splash timeout.
     */
    private Handler mApplicationHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (!isAppCanceled) {
                finish();
            }
        }
    };

    /**
     * Class that indicates when to start the application
     */
    private class SplashTask extends TimerTask {

        /**
         * The actions to be performed
         */
        @Override
        public void run() {
            Message msg = new Message();
            mApplicationHandler.sendMessage(msg);
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return true;
    }
}
