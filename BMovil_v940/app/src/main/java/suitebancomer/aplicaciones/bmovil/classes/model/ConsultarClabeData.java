package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultarClabeData implements ParsingHandler {
	private String estado;
	private String cuentaClabe;
	private String tarjeta;
	private String codigoMensaje;
	private String descripcionMensaje;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCuentaClabe() {
		return cuentaClabe;
	}

	public void setCuentaClabe(String numeroClabe) {
		this.cuentaClabe = numeroClabe;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String numeroTarjeta) {
		this.tarjeta = numeroTarjeta;
	}

	public String getCodigoMensaje() {
		return codigoMensaje;
	}

	public void setCodigoMensaje(String codigoMensaje) {
		this.codigoMensaje = codigoMensaje;
	}

	public String getDescripcionMensaje() {
		return descripcionMensaje;
	}

	public void setDescripcionMensaje(String descripcionMensaje) {
		this.descripcionMensaje = descripcionMensaje;
	}

	/**public String getNombreBeneficiarioCompleto() {
		String nombreTmp = Tools.isEmptyOrNull(nombre)? "" : nombre;
		String aPatTmp = Tools.isEmptyOrNull(aPaterno)? "" : aPaterno;
		String aMatTmp = Tools.isEmptyOrNull(aMaterno)? "" : aMaterno;
		
		String nombreCompleto = nombreTmp;
		if (aPatTmp != null || !aPatTmp.equals("")) {
			nombreCompleto += " " + aPatTmp;
		}
		if (aMatTmp != null || !aMatTmp.equals("")) {
			nombreCompleto += " " + aMatTmp;
		}
		
		return nombreCompleto;
	}**/


	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		estado=parser.parseNextValue("estado");
		cuentaClabe =parser.parseNextValue("cuentaClabe");
		tarjeta =parser.parseNextValue("tarjeta");
		codigoMensaje=parser.parseNextValue("codigoMensaje");
		descripcionMensaje=parser.parseNextValue("descripcionMensaje");
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}
	
	
}
