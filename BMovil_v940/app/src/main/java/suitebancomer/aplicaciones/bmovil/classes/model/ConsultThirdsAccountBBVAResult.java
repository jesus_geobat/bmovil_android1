package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultThirdsAccountBBVAResult implements ParsingHandler {
	//#region Class fields.
	/**
	 * The BBVA account number.
	 */
	private String numeroCuentaB;
	
	/**
	 * The beneficiary name.
	 */
	private String nombreB;
	
	/**
	 * The beneficiary last name.
	 */
	private String apellidoPaternoB;
	
	/**
	 * The beneficiary second last name.
	 */
	private String apellidoMaternoB;
	
	/**
	 * The account type.
	 */
	private String tipoCuenta;
	//#endregion
	
	//#region Getters and Setters.
	/**
	 * @return The BBVA account number.
	 */
	public String getNumeroCuentaB() {
		//Inicia Código Karen
				if (numeroCuentaB.length() >10){
					 numeroCuentaB = numeroCuentaB.substring(numeroCuentaB.length()-10);
				 }
		//Termina Código Karen
		return numeroCuentaB;
	}

	/**
	 * @return The beneficiary name.
	 */
	public String getNombreB() {
		return nombreB;
	}

	/**
	 * @return The beneficiary last name.
	 */
	public String getApellidoPaternoB() {
		return apellidoPaternoB;
	}

	/**
	 * @return The beneficiary second last name.
	 */
	public String getApellidoMaternoB() {
		return apellidoMaternoB;
	}

	/**
	 * @return The account type.
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	//#endregion

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		numeroCuentaB = parser.parseNextValue("numeroCuentaB", true);
		nombreB = parser.parseNextValue("nombreB", true);
		apellidoPaternoB = parser.parseNextValue("apellidoPaternoB", true);
		apellidoMaternoB = parser.parseNextValue("apellidoMaternoB", true);
		tipoCuenta = parser.parseNextValue("tipoCuenta", true);
	}
}
