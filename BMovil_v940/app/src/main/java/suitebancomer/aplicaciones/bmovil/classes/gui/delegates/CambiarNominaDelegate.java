package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.content.DialogInterface;
import android.text.InputFilter;
import android.util.Log;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.CambiarNominaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.CambiarNominaData;
import suitebancomer.aplicaciones.bmovil.classes.model.Comision;
import suitebancomer.aplicaciones.bmovil.classes.model.GetListBanks;
import suitebancomer.aplicaciones.bmovil.classes.model.NominaData;
import suitebancomer.aplicaciones.bmovil.classes.model.PortabilidadData;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferResult;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaInterbancaria;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.BodyType;
import suitebancomer.aplicaciones.commservice.commons.MethodType;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.commons.PojoGeneral;
import suitebancomer.classes.gui.controllers.BaseViewController;
import tracking.TrackingHelper;

//import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.CambioTelefonoViewController;
//SPEI

public class CambiarNominaDelegate extends DelegateBaseAutenticacion {

	private Operacion tipoOperacion;
	public final static long CAMBIARNOMINA_DELEGATE_ID = 0xcea434c61ca809b2L;
	private CambiarNominaViewController cambiarNominaViewController;
	private TransferenciaInterbancaria transferenciaInterbancaria;
	private TransferResult result;//interbancariosResult
	private CambiarNominaData cambiarNomina;
	protected SolicitarAlertasData sa;
	//protected String tempPassword;
	private BaseViewController viewController;
	private ConfirmacionViewController confirmacionViewController;
	private Account cuentaDestino;

	int longitudNumeroCuenta;
	Comision comision;


	public CambiarNominaDelegate() {
		longitudNumeroCuenta = 0;
	}


	/**public TransferenciaInterbancaria getTransferenciaInterbancaria() {
		return transferenciaInterbancaria;
	}**/

	/**public void setTransferenciaInterbancaria(TransferenciaInterbancaria transferenciaInterbancaria) {
		this.transferenciaInterbancaria = transferenciaInterbancaria;
		//frecuentes correo electronico
		this.aliasFrecuente = null;
		this.correoFrecuente = null;
	}**/




	@Override
	public String getTextoSMS() {

		String folio = cambiarNomina.getFolio();
		//String importe =Tools.formatAmount( transferenciaInterbancaria.getImporte(),false);
		String fecha = Tools.formatDate(cambiarNomina.getCreatedDate());
		String hora = Tools.formatTime(cambiarNomina.getOperationTime());

		StringBuilder msg = new StringBuilder();
		msg.append(SuiteApp.appContext.getString(R.string.cambio_nomina_texto_titulo_sms));
		//msg.append(SuiteApp.appContext.getString(R.string.smsText_firstFrom) + " " + Tools.hideAccountNumber(transferenciaInterbancaria.getCuentaOrigen().getNumber()));
		//msg.append(" "+SuiteApp.appContext.getString(R.string.smsText_a) + " "+Tools.hideAccountNumber( transferenciaInterbancaria.getNumeroCuentaDestino())+" ");
		//msg.append(SuiteApp.appContext.getString(R.string.smsText_secondFrom) +" " +transferenciaInterbancaria.getBeneficiario().substring(0, transferenciaInterbancaria.getBeneficiario().length()>10?10:transferenciaInterbancaria.getBeneficiario().length()));
		//msg.append(" "+SuiteApp.appContext.getString(R.string.smsText_byQuantity) +" " +importe+" "+SuiteApp.appContext.getString(R.string.smsText_folio) +" " +folio +" "+fecha +" "+hora);
		String msgText = msg.toString().replaceAll("\n", "");
//		System.out.println(msgText);
		return msgText;
	}

	@Override
	public String getTextoEmail() {
		// TODO Auto-generated method stub
		//El correo se compondrá del contenido del componente �ListaDatosViewController�
		//mostrado en la pantalla de resultados.
		return super.getTextoEmail();
	}

	@Override
	public String getTextoPDF() {
		// TODO Auto-generated method stub
		//El PDF se compondrá del contenido del componente �ListaDatosViewController�
		//mostrado en la pantalla de resultados
		return super.getTextoPDF();
	}

	@Override
	public String getTextoPantallaResultados() {
		// TODO Auto-generated method stub
		return "";
		//return interbancariosViewController.getString(R.string.transferir_interbancario_texto_titulo_resultados);
	}

	@Override
	public String getTextoTituloResultado() {
		return cambiarNominaViewController.getString(R.string.cambiar_nomina_texto_titulo_resultados);
	}

	@Override
	public String getTextoTituloConfirmacion() {

		return "";//interbancariosViewController.getString(R.string.confirmation_subtitulo);
	}

	@Override
	public int getColorTituloResultado() {
			return R.color.verde_limon;
	}

	@Override
	public String getTextoAyudaResultados() {
		return cambiarNominaViewController.getString(R.string.cambiar_nomina_text_especial_resultados);
	}

	@Override
	public int getOpcionesMenuResultados() {

				return SHOW_MENU_SMS ;

	}
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = true ? Autenticacion.getInstance().tokenAMostrar(Operacion.transferirInterbancariaF,
					Session.getInstance(SuiteApp.appContext).getClientProfile()) : Autenticacion.getInstance().tokenAMostrar(Operacion.cambiarNomina,
					Session.getInstance(SuiteApp.appContext).getClientProfile());
					//Tools.getDoubleAmountFromServerString(transferenciaInterbancaria.getImporte()));
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}

		return tipoOTP;
	}

	@Override
	public boolean mostrarNIP() {
		boolean value =  Autenticacion.getInstance().mostrarNIP(Operacion.transferirInterbancariaF,
				Session.getInstance(SuiteApp.appContext).getClientProfile());

		return value;
	}

	@Override
	public boolean mostrarCVV() {
		boolean value =  Autenticacion.getInstance().mostrarCVV(Operacion.transferirInterbancariaF, Session.getInstance(SuiteApp.appContext).getClientProfile());
		return value;
	}

	public ArrayList<Object> getListadoBancos(){
		ArrayList<Object> listaOpcionesMenu = new ArrayList<Object>();
		ArrayList<Object> registros;

		int size = (GetListBanks.getInstance().getListBanksCatalogs() != null) ? GetListBanks.getInstance().getListBanksCatalogs().size() : 0;

		for (int i = 0; i < size; i++) {
			if (GetListBanks.getInstance().getListBanksCatalogs().get(i).getTipo().equals("O"))
			{
				registros = new ArrayList<Object>(2);
				registros.add(GetListBanks.getInstance().getListBanksCatalogs().get(i).getIdBanco());
				registros.add(GetListBanks.getInstance().getListBanksCatalogs().get(i).getNombreBanco());
				listaOpcionesMenu.add(registros);
			}

		}

		return listaOpcionesMenu;
	}

	public ArrayList<Object> getListaTipoCuentaDestino(){
		ArrayList<Object> listaOpcionesMenu = new ArrayList<Object>();
		ArrayList<Object> registros;

//		if (Constants.CREDIT_TYPE.equals(transferenciaInterbancaria.getCuentaOrigen().getType())) {
//
//			registros = new ArrayList<Object>(2);
//			registros.add(Constants.CREDIT_TYPE);
//			registros.add(SuiteApp.appContext.getString(R.string.cardType_credit));
//			listaOpcionesMenu.add(registros);
//
//        } else {
		// fill the combo
		registros = new ArrayList<Object>(2);
		registros.add(Constants.DEBIT_TYPE);
		registros.add(SuiteApp.appContext.getString(R.string.cardType_debit));
		listaOpcionesMenu.add(registros);
		
		registros = new ArrayList<Object>(2);
		registros.add(Constants.CLABE_TYPE_ACCOUNT);
		registros.add(SuiteApp.appContext.getString(R.string.cardType_clabe));
		listaOpcionesMenu.add(registros);

		//SPEI

		return listaOpcionesMenu;
	}
	@Override
	public void performAction(Object obj) {
		if(cambiarNominaViewController != null)
			cambiarNominaViewController.actualizarSeleccion(obj);


	}

	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.bmovil_comprar_icono;
	}

	@Override
	public int getTextoEncabezado() {
		return R.string.menu_comprar_contratar_title;
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if(operationId == Server.GET_CUSTOMER){
			NominaData nd = new NominaData();

			try {
				nd.process(new ParserJSON(response.getResponsePlain()));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParsingException e) {
				e.printStackTrace();
			}
			if(nd.getCode()!= null && nd.getCode().equalsIgnoreCase(response.CODE_OK)) {
				cambiarNominaViewController.cargaFecha(nd);
			}
			else if (nd.getDescription() != null){
				((BmovilViewsController)cambiarNominaViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(nd.getDescription(), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog, final int which) {
								dialog.dismiss();
								cambiarNominaViewController.finish();
							}
						}
				);
			}
			else{
				((BmovilViewsController)cambiarNominaViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert("Servicio no disponible", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog, final int which) {
								dialog.dismiss();
								cambiarNominaViewController.finish();
							}
						}
				);
			}
		}else if(operationId == Server.REQUEST_PORTABILITY){
            PortabilidadData.renewInstance();
			PortabilidadData pd = PortabilidadData.getInstance();
			try {
				pd.process(new ParserJSON(response.getResponsePlain()));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParsingException e) {
				e.printStackTrace();
			}
				if(pd.getCode()!= null && pd.getCode().equalsIgnoreCase(response.CODE_OK)){
					cambiarNomina.setFolio(pd.getCreatedReferenceNumber());
					cambiarNomina.setCreatedDate(pd.getCreatedDate());
					cambiarNomina.setOperationTime(pd.getOperationTime());
					pd.setPositionBank(cambiarNominaViewController.posionBancos);
					pd.setReferenceNumber(cambiarNomina.getCuentaDestino().getNumber());
					((BmovilViewsController)cambiarNominaViewController.getParentViewsController()).showResultadosViewController(this, -1, -1);
				}else if (pd.getDescription() != null){
					((BmovilViewsController)cambiarNominaViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(pd.getDescription(), new DialogInterface.OnClickListener() {
								@Override
								public void onClick(final DialogInterface dialog, final int which) {
									dialog.dismiss();
									cambiarNominaViewController.finish();
								}
							}
					);
				}
				else{
					((BmovilViewsController)cambiarNominaViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert("Servicio no disponible", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(final DialogInterface dialog, final int which) {
									dialog.dismiss();
									cambiarNominaViewController.finish();
								}
							}
					);
				}
			}else if(operationId == Server.BANKS_CATALOG){
			GetListBanks listBanks=GetListBanks.getInstance();
			try {
				listBanks.process(new ParserJSON(response.getResponsePlain()));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParsingException e) {
				e.printStackTrace();
			}
			if(listBanks.getCode()!= null && listBanks.getCode().equalsIgnoreCase(response.CODE_OK)){
				//((BmovilViewsController)cambiarNominaViewController.getParentViewsController()).showResultadosViewController(this, -1, -1);
			}else if (listBanks.getDescription() != null){
				((BmovilViewsController)cambiarNominaViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(listBanks.getDescription(), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog, final int which) {
								dialog.dismiss();
								cambiarNominaViewController.finish();
							}
						}
				);
			}
			else{
				((BmovilViewsController)cambiarNominaViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert("Servicio no disponible", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog, final int which) {
								dialog.dismiss();
								cambiarNominaViewController.finish();
							}
						}
				);
			}
		}
		}


	public CambiarNominaData getCambiarNomina() {
		return cambiarNomina;
	}

	public void setCambiarNomina(CambiarNominaData cambiarNomina) {
		this.cambiarNomina = cambiarNomina;
	}

	public Operacion getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(Operacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public void setCambiarNominaViewController(CambiarNominaViewController interbancariosViewController) {
		this.cambiarNominaViewController = interbancariosViewController;
	}
	public TransferenciaInterbancaria getTransferenciaInterbancaria() {
		return transferenciaInterbancaria;
	}


	public TransferResult getResult(){
		return result;
	}

	public Account getCuentaDestino() {
		return cuentaDestino;
	}

	public void setCuentaDestino(Account cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

	public String textoMensajeAlertaTC() {
	       return Session.getInstance(SuiteApp.getInstance()).getOpenHoursForExternalTransfersMessageUsingTC();
	}

	public BaseViewController getViewController() {
		return viewController;
	}

	public void setViewController(BaseViewController viewController) {
		this.viewController = viewController;
	}

	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada seg�n sea reguerido.
	 * @return
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();

		ArrayList<Account> accountsArray = new ArrayList<Account>();
		//Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccountsIB();

		if(profile == Perfil.avanzado)
		{
				for(Account acc : accounts) {
					if(acc.getType().equals(Constants.LIBRETON_TYPE) || acc.getType().equals(Constants.SAVINGS_TYPE) || acc.getType().equals(Constants.CHECK_TYPE)) {
						accountsArray.add(acc);
					}
				}

		}


		return accountsArray;
	}

	/**
	 * Verifica si es necesario registrar la operacion o solo confirmar.
	 */
	public void showConfirmacion() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		if(registrarOperacion()){
			bMovilVC.showRegistrarOperacion(this);
		}else{
			bMovilVC.showConfirmacion(this);
		}
	}

	/**
	 * Valida si es necesario hacer el registro de operacion para una
	 * nueva transferencia interbancaria o para una transferencia interbancaria
	 * desde un frecuente.
	 *
	 * @return true si la Operacion actual requiere registro.
	 */
	public boolean registrarOperacion(){
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		Operacion operacion;
		/*if(esFrecuente)
			operacion = Operacion.transferirInterbancariaF;
		else
		*/
		operacion = Operacion.transferirInterbancariaF;
		Autenticacion aut = Autenticacion.getInstance();
		boolean value = aut.validaRegistro(operacion, perfil);
		//if(Server.ALLOW_LOG) Log.d("RegistroOP",value+" InterbancarioF? "+esFrecuente);
		return value;
	}

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		String accountLabel = Constants.EMPTY_STRING;
		if(cambiarNomina.getTipoCuentaOrigen().equals(Constants.DEBIT_TYPE)){accountLabel = "Débito";}
		else if(cambiarNomina.getTipoCuentaOrigen().equals(Constants.CREDIT_TYPE)){accountLabel = "Crédito";}
		else{accountLabel = "Clabe";}


		ArrayList<Object> datosConfirmacion = new ArrayList<Object>();
		ArrayList<String> registro;
		final BaseViewController vc = cambiarNominaViewController;
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_banco_origen));
		registro.add(cambiarNomina.getBancoActual());
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_tipo_de_cuenta_destino));
		registro.add(accountLabel);
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_cuenta_origen));
		registro.add(cambiarNomina.getCuentaActual());
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_cuenta_destino));
		registro.add(Tools.hideAccountNumber(cambiarNomina.getCuentaDestino().getNumber()));
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_fecha_nacimiento));
		registro.add(cambiarNomina.getFechaNacim());
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_fecha_operacion));
		java.util.Date currentDate = Session.getInstance(SuiteApp.appContext).getServerDate();
		registro.add(Tools.dateToString(currentDate));
		datosConfirmacion.add(registro);

		return datosConfirmacion;
	}

	/**
	 * Muestra la pantalla de confirmacion registro
	 */
	public void showConfirmacionRegistro() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		bMovilVC.showConfirmacionRegistro(this);
	}

	public void getCustomer() {
		Session session = Session.getInstance(viewController);
		ParametersTO parametersTO = new ParametersTO();
		final Map<String, String> headers = new HashMap<String, String>();
		headers.put(HTTP.CONTENT_TYPE, "application/json");
		headers.put("ium", session.getIum());
		parametersTO.setHeaders(headers);
		parametersTO.setParameters(new Hashtable<String, String>());
		parametersTO.setMethodType(MethodType.GET);
		parametersTO.setForceArq(true);
		parametersTO.setAddCadena(false);
		this.doNetworkOperation(ApiConstants.GET_CUSTOMER, parametersTO, true, new PojoGeneral(), Server.isjsonvalueCode.NONE, cambiarNominaViewController);
	}

    public void doNetworkOperation(final int operationId, final ParametersTO params, final boolean isJson,
                                   final Object handler, final Server.isJsonValueCode isJsonValueCode,
                                   final BaseViewController caller) {
        ((BmovilViewsController)cambiarNominaViewController.getParentViewsController()).getBmovilApp()
                .invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode,
						caller, true);
    }

	public void validaDatos(){

		//cambiarNomina.setTipoCuentaOrigen(cambiarNominaViewController.txtTipo.getText().toString());
		cambiarNomina.setBancoActual(cambiarNominaViewController.txtBanco.getText().toString());
		cambiarNomina.setCuentaActual(cambiarNominaViewController.txtCuentaOrigen.getText().toString());
		cambiarNomina.setCuentaDestino(CambiarNominaViewController.getListaCuetasMostrar().get(CambiarNominaViewController.getPositionCuenta()));
		cambiarNomina.setFechaNacimiento(cambiarNominaViewController.isFechaAceptado());
		cambiarNomina.setFechaNacim(cambiarNominaViewController.getFechaNacimeinto());
		String typeCard  = cambiarNomina.getTipoCuentaOrigen();
//		String nombreBanco = Session.getInstance(SuiteApp.appContext).obtenerNombreBanco(transferenciaInterbancaria.getInstitucionBancaria(), typeCard);
//	    String codigoBanco = Session.getInstance(SuiteApp.appContext).obtenerCodigoBanco(transferenciaInterbancaria.getInstitucionBancaria(), typeCard);
//		transferenciaInterbancaria.setNombreBanco(nombreBanco);
//		transferenciaInterbancaria.setInstitucionBancaria(codigoBanco);

		if (Tools.isEmptyOrNull(typeCard)){
			cambiarNominaViewController.showInformationAlert("Es necesario ingresar el tipo de tarjeta");
		}
		else if(Tools.isEmptyOrNull(cambiarNomina.getBancoActual())) {
			cambiarNominaViewController.showInformationAlert("Es necesario ingresar el banco actual");
		}//SPEI
		else if (cambiarNominaViewController.txtCuentaOrigen.length() == 0) {
				cambiarNominaViewController.showInformationAlert("Es necesario ingresar la cuenta actual");
		}//TERMINA SPEI
		else if (cambiarNominaViewController.txtCuentaOrigen.length() != longitudNumeroCuenta){
			String mensaje = cambiarNominaViewController.getString(R.string.transferir_interbancario_alerta_numero_cuenta_menor1);
			mensaje += " ";
			mensaje += String.valueOf(longitudNumeroCuenta);
			mensaje += " ";
			//if (transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.DEBIT_TYPE) || transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CREDIT_TYPE)) {
			//mensaje += interbancariosViewController.getString(R.string.transferir_interbancario_alerta_numero_cuenta_menor2);
			//SPEI
			if (cambiarNomina.getTipoCuentaOrigen().equals(Constants.DEBIT_TYPE) || cambiarNomina.getTipoCuentaOrigen().equals(Constants.CREDIT_TYPE)) {
				mensaje += cambiarNominaViewController.getString(R.string.transferir_interbancario_alerta_numero_cuenta_menor2);
			}
			else { //TERMINA SPEI
				mensaje += cambiarNominaViewController.getString(R.string.transferir_interbancario_alerta_numero_cuenta_menor2_clabe);
			}

			cambiarNominaViewController.showInformationAlert(mensaje);
		} else if(!cambiarNominaViewController.isFechaAceptado()){
			cambiarNominaViewController.showInformationAlert("Debes confirmar tu fecha de nacimiento");
		}
		else{
			PortabilidadData.getInstance().setReferenceNumber(cambiarNomina.getCuentaDestino().getNumber());
				//ARR
				Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();

				//ARR
				paso2OperacionMap.put("evento_paso2", "event47");
				paso2OperacionMap.put("&&products", "operaciones;transferencias+otros bancos");
				paso2OperacionMap.put("eVar12", "paso2:cuenta e importe");

				TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
				showConfirmacion();

		}
	}

	@Override
	protected ArrayList<Object> getDatosRegistroOp() {
		ArrayList<Object> datosConfirmacion = new ArrayList<Object>();
		ArrayList<String> registro;
		final BaseViewController vc = cambiarNominaViewController;
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_banco_origen));
		registro.add(cambiarNomina.getBancoActual());
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_tipo_de_cuenta_destino));
		registro.add(cambiarNomina.getTipoCuentaOrigen());
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_cuenta_origen));
		registro.add(cambiarNomina.getCuentaActual());
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_cuenta_destino));
		registro.add(Tools.hideAccountNumber(cambiarNomina.getCuentaDestino().getNumber()));
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_fecha_nacimiento));
		registro.add(cambiarNomina.getFechaNacim());
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_fecha_operacion));
		java.util.Date currentDate = Session.getInstance(SuiteApp.appContext).getServerDate();
		registro.add(Tools.dateToString(currentDate));
		datosConfirmacion.add(registro);

		return datosConfirmacion;
	}

	public ArrayList<Object> getDatosTablaResultados(){
		String accountLabel = Constants.EMPTY_STRING;
		if(cambiarNomina.getTipoCuentaOrigen().equals(Constants.DEBIT_TYPE)){accountLabel = "Débito";}
		else if(cambiarNomina.getTipoCuentaOrigen().equals(Constants.CREDIT_TYPE)){accountLabel = "Crédito";}
		else{accountLabel = "Clabe";}

		ArrayList<Object> datosResultados = new ArrayList<Object>();
		ArrayList<String> registro;

		final BaseViewController vc = cambiarNominaViewController;
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_banco_origen));
		registro.add(cambiarNomina.getBancoActual());
		datosResultados.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_tipo_de_cuenta_destino));
		registro.add(accountLabel);
		datosResultados.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_cuenta_origen));
		registro.add(cambiarNomina.getCuentaActual());
		datosResultados.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_cuenta_destino));
		registro.add(Tools.hideAccountNumber(cambiarNomina.getCuentaDestino().getNumber()));
		datosResultados.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_fecha_nacimiento));
		registro.add(cambiarNomina.getFechaNacim());
		datosResultados.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.bmovil_cambia_nomina_fecha_operacion));
		java.util.Date currentDate = Session.getInstance(SuiteApp.appContext).getServerDate();
		registro.add(Tools.dateToString(currentDate));
		datosResultados.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.cambiar_nomina_folio_operacion));
		registro.add(cambiarNomina.getFolio());
		datosResultados.add(registro);

		return datosResultados;

	}


    @Override
    public void realizaOperacion(ConfirmacionViewController confirmacionViewController,	String contrasenia, String nip, String token, String campoTarjeta, String cvv) {
        Session session = Session.getInstance(viewController);
        ParametersTO parametersTO = new ParametersTO();
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put(HTTP.CONTENT_TYPE, "application/json");
        headers.put("ium", session.getIum());
        headers.put("otp", Tools.isEmptyOrNull(token) ? "" : token);
        parametersTO.setHeaders(headers);
        parametersTO.setParameters(new Hashtable<String, String>());
        parametersTO.setMethodType(MethodType.POST);
        final JsonObject params = new JsonObject();
        params.addProperty("referenceNumber", String.valueOf(cambiarNomina.getCuentaDestino().getNumber()));
        params.addProperty("option", "A");
        String referenceNumberType = suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_STRING;
        final Gson gson = new Gson();
        if(cambiarNomina.getTipoCuentaOrigen().equals("TD")) {
            referenceNumberType = "03";
        } else if ((cambiarNomina.getTipoCuentaOrigen().equals("CL"))){
            referenceNumberType = "40";
        }

		String codigoBanco="";
		for(int i=0;i<GetListBanks.getInstance().getListBanksCatalogs().size();i++){
			if(String.valueOf(cambiarNominaViewController.posionBancos).equals(GetListBanks.getInstance().getListBanksCatalogs().get(i).getIdBanco())) {
				codigoBanco=GetListBanks.getInstance().getListBanksCatalogs().get(i).getClaveSPEI();
			}
		}
        final JsonObject externalAccountJson = new JsonObject();
        externalAccountJson.addProperty("referenceNumberType", referenceNumberType);
        externalAccountJson.addProperty("referenceNumber", String.valueOf(cambiarNomina.getCuentaActual()));

        final JsonObject externalBank = new JsonObject();
        externalBank.addProperty("id", codigoBanco);

        final JsonObject portability = new JsonObject();
        portability.addProperty("type", "R");
        portability.add("externalAccount", externalAccountJson);
        portability.add("externalBank", externalBank);

        params.add("portability", portability);

        java.util.Date currentDate = Session.getInstance(SuiteApp.appContext).getServerDate();
        params.addProperty("createdDate", Tools.dateToString(currentDate));
        params.addProperty("birthDate", cambiarNomina.getFechaNacim());
        if (confirmacionViewController.getPortaibilidad()){
            params.addProperty("agreement", "SI");
        }else{
            params.addProperty("agreement", "NO");
        }
        params.addProperty("user", session.getUsername());
        //params.put("otpKey", Tools.isEmptyOrNull(token) ? "" : token);
        String json = "";
        json = gson.toJson(params);
        parametersTO.setBodyRaw(json);
        parametersTO.setBodyType(BodyType.RAW);
        parametersTO.setForceArq(true);
        parametersTO.setAddCadena(false);
        this.doNetworkOperation(ApiConstants.REQUEST_PORTABILITY,  parametersTO, true, new PojoGeneral(),
                Server.isjsonvalueCode.NONE, confirmacionViewController);

    }

	public void convertStringToValues(final Map<String, String> map) {
		for (final String key : map.keySet()) {
			map.put(key,map.get(key).toString().replace("\"","") );
		}
	}

	public void banksCatalog() {
		Session session = Session.getInstance(viewController);
		ParametersTO parametersTO = new ParametersTO();
		final Map<String, String> headers = new HashMap<String, String>();
		headers.put(HTTP.CONTENT_TYPE, "application/json");
		headers.put("ium", session.getIum());
		parametersTO.setHeaders(headers);
		parametersTO.setParameters(new Hashtable<String, String>());
		parametersTO.setMethodType(MethodType.GET);
		parametersTO.setForceArq(true);
		parametersTO.setAddCadena(false);
		this.doNetworkOperation(ApiConstants.BANKS_CATALOG, parametersTO, true, new PojoGeneral(), Server.isjsonvalueCode.NONE, cambiarNominaViewController);

	}

    public void numeroTarjeta() {
//    	interbancariosViewController.txtNumeroCuenta.setText("");
        if (cambiarNomina.getTipoCuentaOrigen().equals(Constants.DEBIT_TYPE)) {

			// fija el texto y el tama�o del campo adecuados para tarjeta de debito
			InputFilter[] FilterArray = new InputFilter[1];
			FilterArray[0] = new InputFilter.LengthFilter(Constants.CARD_NUMBER_LENGTH);
			longitudNumeroCuenta = Constants.CARD_NUMBER_LENGTH;
			cambiarNominaViewController.txtCuentaOrigen.setFilters(FilterArray);

		} else if (cambiarNomina.getTipoCuentaOrigen().equals(Constants.CREDIT_TYPE)) {
			if (cambiarNomina.getInstitucionBancaria() == null ||
                    cambiarNomina.getInstitucionBancaria().equals("")){
				InputFilter[] FilterArray = new InputFilter[1];
				FilterArray[0] = new InputFilter.LengthFilter(Constants.CARD_NUMBER_LENGTH);
				longitudNumeroCuenta = Constants.CARD_NUMBER_LENGTH;
				cambiarNominaViewController.txtCuentaOrigen.setFilters(FilterArray);
			} else if (cambiarNomina.getInstitucionBancaria().equals(Constants.AMEX_ID)){
				InputFilter[] FilterArray = new InputFilter[1];
				FilterArray[0] = new InputFilter.LengthFilter(Constants.AMEX_CARD_NUMBER_LENGTH);
				longitudNumeroCuenta = Constants.AMEX_CARD_NUMBER_LENGTH;
				cambiarNominaViewController.txtCuentaOrigen.setFilters(FilterArray);
			} else {
				InputFilter[] FilterArray = new InputFilter[1];
				FilterArray[0] = new InputFilter.LengthFilter(Constants.CARD_NUMBER_LENGTH);
				longitudNumeroCuenta = Constants.CARD_NUMBER_LENGTH;
				cambiarNominaViewController.txtCuentaOrigen.setFilters(FilterArray);
			}
		}else if (cambiarNomina.getTipoCuentaOrigen().equals(Constants.CLABE_TYPE_ACCOUNT)) {

			// fija el texto y el tama�o del campo adecuados para cuenta clabe
			InputFilter[] FilterArray = new InputFilter[1];
			FilterArray[0] = new InputFilter.LengthFilter(Constants.CUENTA_CLABE_LENGTH);
			longitudNumeroCuenta = Constants.CUENTA_CLABE_LENGTH;
			cambiarNominaViewController.txtCuentaOrigen.setFilters(FilterArray);
		}
		//Termina SPEI
	}
}

