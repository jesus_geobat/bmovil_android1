package suitebancomer.aplicaciones.bmovil.classes.common;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.bancomer.mbanking.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.LlamadaPatrimonialDelegate;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by marioaguilar on 19/09/16.
 */
public class SpinnerLlamadaPatrimonial extends ListView {

    private ArrayList<String> OpcionesLlamadas;
    private BaseViewController controller;

    public void setController(BaseViewController controller) {

        this.controller = controller;
    }

    public SpinnerLlamadaPatrimonial(Context context, AttributeSet attrs) {
        super(context, attrs);

        OpcionesLlamadas = new ArrayList<String>();
        OpcionesLlamadas.add(context.getString(R.string.bmovil_patrimonial_Llamada_op1_spinner));
        OpcionesLlamadas.add(context.getString(R.string.bmovil_patrimonial_Llamada_op2_spinner));
        OpcionesLlamadas.add(context.getString(R.string.bmovil_patrimonial_Llamada_op3_spinner));


        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                R.layout.bmovil_spinner_contratos_text_view, R.id.txtOpcionSpinner, OpcionesLlamadas);

     setOnItemClickListener(new OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listOpcion(position);
         }
     });


        setAdapter(adapter);



    }



    public void listOpcion (int Position){

        if(OpcionesLlamadas.get(Position).equals(controller.getString(R.string.bmovil_patrimonial_Llamada_op1_spinner))){

            call(controller.getString(R.string.bmovil_opcion_contacto_nacional)+PhoneNumberUtils.PAUSE+PhoneNumberUtils.PAUSE+PhoneNumberUtils.PAUSE+ "#0109");

        }else if (OpcionesLlamadas.get(Position).equals(controller.getString(R.string.bmovil_patrimonial_Llamada_op2_spinner))){
            call(controller.getString(R.string.bmovil_opcion_contacto_EUAyCanadá));
        }
        else if(OpcionesLlamadas.get(Position).equals(controller.getString(R.string.bmovil_patrimonial_Llamada_op3_spinner))){
            call(controller.getString(R.string.bmovil_opcion_contacto_Mundial));
        }


    }


    public void call(String NumCall){
      // (LlamadaPatrimonialDelegate)controller.getDelegate();

        String encodedNumber = "";
      try {
                encodedNumber = URLEncoder.encode(NumCall, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        try {
            Intent my_callIntent = new Intent(Intent.ACTION_CALL);
            my_callIntent.setData(Uri.parse("tel:"+encodedNumber));
            //here the word 'tel' is important for making a call...
            controller.startActivity(my_callIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(controller.getApplicationContext(), "Error en la llamada"+e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
