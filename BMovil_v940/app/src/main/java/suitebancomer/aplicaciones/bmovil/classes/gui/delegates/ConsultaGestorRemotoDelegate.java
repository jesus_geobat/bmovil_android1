package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;

import java.util.Hashtable;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuPrincipalViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.BanqueroPersonal;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.TextoPaperless;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
/**
 * Created by eluna on 31/05/2016.
 */
public class ConsultaGestorRemotoDelegate extends BaseDelegate
{
    MenuPrincipalViewController viewController;

    public void peticionConsultaGestorRemoto(final MenuPrincipalViewController viewController)
    {
        this.viewController = viewController;
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        int operacion = Server.OP_CONSULTA_GESTOR_REMOTO;
        Session session = Session.getInstance(viewController);
        paramTable.put(ServerConstants.NUMERO_TELEFONO,session.getUsername());
        paramTable.put(ServerConstants.IUM, session.getIum());
        doNetworkOperation(operacion, paramTable, Boolean.TRUE, BanqueroPersonal.getInstance(), Server.isJsonValueCode.NONE, viewController);
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler hadler, Server.isJsonValueCode isJsonValue, BaseViewController caller) {
        ((BmovilViewsController) caller
                .getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, hadler, isJsonValue, caller);
    }

}
