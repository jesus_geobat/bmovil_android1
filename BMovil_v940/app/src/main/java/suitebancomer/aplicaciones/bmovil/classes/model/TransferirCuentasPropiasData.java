package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.util.Log;

public class TransferirCuentasPropiasData implements ParsingHandler {
	/**
	 * The transaction reference.
	 */
	String reference = null;
	
	/**
	 * The accounts list occurrences.
	 */
	String accountListOccurrences = null;
	
	/**
	 * The accounts list.
	 */
	ArrayList<Object> accountList = null;
	
	/**
	 * The server date.
	 */
	String serverDate = null;
	
	/**
	 * The server time.
	 */
	String serverTime = null;
	
	String codTrans=null;
	
	public String getCodTrans() {
		return codTrans;
	}

	public void setCodTrans(String codTrans) {
		this.codTrans = codTrans;
	}
	
	/**
	 * @return The transaction reference.
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference The transaction reference to set.
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return The accounts list.
	 */
	public ArrayList<Object> getAccountList() {
		return accountList;
	}

	/**
	 * @param accountList The accounts list to set.
	 */
	public void setAccountList(ArrayList<Object> accountList) {
		this.accountList = accountList;
	}

	/**
	 * @return The server date.
	 */
	public String getServerDate() {
		return serverDate;
	}

	/**
	 * @param serverDate The server date to set.
	 */
	public void setServerDate(String serverDate) {
		this.serverDate = serverDate;
	}

	/**
	 * @return The server time.
	 */
	public String getServerTime() {
		return serverTime;
	}

	/**
	 * @param serverTime The server time to set.
	 */
	public void setServerTime(String serverTime) {
		this.serverTime = serverTime;
	}

	/**
	 * @return The accounts list occurrences.
	 */
	public String getAccountListOcurrences() {
		return accountListOccurrences;
	}

	/**
	 * @param accountListOcurrences The accounts list occurrences to set.
	 */
	public void setAccountListOcurrences(String accountListOcurrences) {
		this.accountListOccurrences = accountListOcurrences;
	}

	/**
	 * Default constructor.
	 */
	public TransferirCuentasPropiasData() {
		reference = "";
		accountList = new ArrayList<Object>();
		serverDate = "";
		serverTime = "";
		accountListOccurrences = "";
		codTrans = "";
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		reference 				= parser.parseNextValue("FO");
		parser.parseNextValue("CT");
		
		accountListOccurrences	= parser.parseNextValue("OC");
		int ocurrences = 0;
		try {
			ocurrences = Integer.parseInt(accountListOccurrences);
		} catch(Exception ex) {
			if(Server.ALLOW_LOG) Log.e("TransferirCuentasPropiasData", "Error al parsear el numero de ocurrencias de la lista de cuentas", ex);
			ocurrences = 0;
		}
		
		for(int i = 0; i < ocurrences; i++) {
			ArrayList<String> accountDetails = new ArrayList<String>();
			accountDetails.add(parser.parseNextValue("AS"));
			accountDetails.add(parser.parseNextValue("IM"));
			accountList.add(accountDetails);
		}
		
		serverDate 				= parser.parseNextValue("FE");
		serverTime 				= parser.parseNextValue("HR");
		codTrans=parser.parseNextValue("TR");
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}
}
