package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.Serializable;

/**
 * Created by elunag on 06/07/16.
 */
public class ListPortability implements Serializable
{
    private ExternalBank externalBank;
    private ExternalAccount externalAccount;
    private String createdReferenceNumber;
    private Company company;
    private String createdDate;
    private String referenceNumber;
    private String status;

    private static ListPortability ourInstance = new ListPortability();

    public static ListPortability getInstance() {
        return ourInstance;
    }

    public ListPortability() {
    }

    public ListPortability(ExternalBank externalBank, ExternalAccount externalAccount, String createdReferenceNumber, Company company, String createdDate, String referenceNumber, String status) {
        this.externalBank = externalBank;
        this.externalAccount = externalAccount;
        this.createdReferenceNumber = createdReferenceNumber;
        this.company = company;
        this.createdDate = createdDate;
        this.referenceNumber = referenceNumber;
        this.status = status;
    }

    public ExternalBank getExternalBank() {
        return externalBank;
    }

    public void setExternalBank(ExternalBank id) {
        this.externalBank = id;
    }

    public String getCreatedReferenceNumber() {
        return createdReferenceNumber;
    }

    public void setCreatedReferenceNumber(String createdReferenceNumber) {
        this.createdReferenceNumber = createdReferenceNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ExternalAccount getExternalAccount() {
        return externalAccount;
    }

    public void setExternalAccount(ExternalAccount externalAccount) {
        this.externalAccount = externalAccount;
    }
}


