package suitebancomer.aplicaciones.bmovil.classes.model;

/**
 * Created by elunag on 07/07/16.
 */
public class ListaConsultaPortabilidad
{
    private String fecha;
    private String nombreBanco;
    private String status;

    public ListaConsultaPortabilidad(){

    }

    public ListaConsultaPortabilidad(final String fecha,final String nombreBanco, final String status){

    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
