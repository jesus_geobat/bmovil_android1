package suitebancomer.aplicaciones.bmovil.classes.model;

import bancomer.api.common.commons.Constants.Perfil;

public class Desbloqueo {

	private String contrasenaNueva;
	private Perfil perfilCliente;
	private String numCelular;
	/**
	 * @return the contrasenaNueva
	 */
	public String getContrasenaNueva() {
		return contrasenaNueva;
	}
	/**
	 * @param contrasenaNueva the contrasenaNueva to set
	 */
	public void setContrasenaNueva(String contrasenaNueva) {
		this.contrasenaNueva = contrasenaNueva;
	}
	/**
	 * @return the perfilCliente
	 */
	public Perfil getPerfilCliente() {
		return perfilCliente;
	}
	/**
	 * @param perfilCliente the perfilCliente to set
	 */
	public void setPerfilCliente(Perfil perfilCliente) {
		this.perfilCliente = perfilCliente;
	}
	/**
	 * @return the numCelular
	 */
	public String getNumCelular() {
		return numCelular;
	}
	/**
	 * @param numCelular the numCelular to set
	 */
	public void setNumCelular(String numCelular) {
		this.numCelular = numCelular;
	}
}
