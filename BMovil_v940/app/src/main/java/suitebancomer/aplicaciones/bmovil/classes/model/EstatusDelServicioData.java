package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class EstatusDelServicioData implements ParsingHandler {

	private String numeroCliente;
	private String companiaCliente;
	private String statusCliente;
	private String instrumentoCliente;
	private String perfilCliente;
	private String nombreCliente;
	private String aPaternoCliente;
	private String aMaternoCliente;
	private String folioAstCliente;
	
	public EstatusDelServicioData() {
		// TODO Auto-generated constructor stub
	}

	public String getNumeroCliente() {
		return numeroCliente;
	}

	public String getCompaniaCliente() {
		return companiaCliente;
	}

	public String getStatusCliente() {
		return statusCliente;
	}

	public String getInstrumentoCliente() {
		return instrumentoCliente;
	}

	public String getPerfilCliente() {
		return perfilCliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public String getaPaternoCliente() {
		return aPaternoCliente;
	}

	public String getaMaternoCliente() {
		return aMaternoCliente;
	}

	public String getFolioAstCliente() {
		return folioAstCliente;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		numeroCliente = parser.parseNextValue("TE");
		companiaCliente = parser.parseNextValue("OA");
		statusCliente = parser.parseNextValue("EM");
		instrumentoCliente = parser.parseNextValue("IT");
		perfilCliente = parser.parseNextValue("PR");
		nombreCliente = parser.parseNextValue("NO");
		aPaternoCliente = parser.parseNextValue("PP", false);
		aMaternoCliente = parser.parseNextValue("PM", false);
		folioAstCliente = parser.parseNextValue("FO");
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
