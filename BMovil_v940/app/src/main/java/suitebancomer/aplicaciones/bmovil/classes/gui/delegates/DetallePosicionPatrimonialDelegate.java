package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import suitebancomer.aplicaciones.bmovil.classes.model.DetallePosicionPatrimonial;
import suitebancomer.classes.gui.delegates.BaseDelegate;

/**
 * Created by OOROZCO on 4/15/16.
 */
public class DetallePosicionPatrimonialDelegate extends BaseDelegate {

    public static final long DETALLE_POSICION_PATRIMONIAL_DELEGATE = 631453334566761234L;

    private DetallePosicionPatrimonial detalle;


    public DetallePosicionPatrimonialDelegate(DetallePosicionPatrimonial detalle)
    {
        this.detalle = detalle;
    }

    public DetallePosicionPatrimonial getDetalle() {
        return detalle;
    }
}
