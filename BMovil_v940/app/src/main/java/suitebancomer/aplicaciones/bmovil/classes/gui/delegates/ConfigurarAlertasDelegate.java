package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import suitebancomer.classes.gui.controllers.BaseViewController;

public class ConfigurarAlertasDelegate extends DelegateBaseOperacion {
	/**
	 * Identificador �nico del delegado.
	 */
	public static final long CONFIGURAR_ALERTAS_DELEGATE_ID = 0x1662a68237f0465aL;
	
	/**
	 * @return La vista a la que esté asignada esté delegado.
	 */
	private BaseViewController ownerController;
	
	/**
	 * @return La vista a la que esté asignada esté delegado.
	 */
	public BaseViewController getOwnerController() {
		return ownerController;
	}

	/**
	 * @param ownerController La vista a la que esté asignada esté delegado.
	 */
	public void setOwnerController(BaseViewController ownerController) {
		this.ownerController = ownerController;
	}

	public ConfigurarAlertasDelegate() {
		// TODO Auto-generated constructor stub
	}

}
