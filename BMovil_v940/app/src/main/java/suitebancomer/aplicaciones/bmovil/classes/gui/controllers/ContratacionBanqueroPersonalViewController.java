package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionBanqueroDelegate;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtp;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtpBmovil;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ContratacionBanqueroPersonalDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MenuPrincipalDelegate;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by eluna on 17/06/2016.
 */
public class ContratacionBanqueroPersonalViewController extends BaseViewController implements View.OnClickListener
{
    private BmovilViewsController parentManager;
    private TextView txtDescBanquero;
    private TextView txtAceptoTerminos;
    private LinearLayout linearToken;
    private LinearLayout linearConfirmarBanquero;
    private ImageButton btnOff;
    private ImageButton btnOnn;
    private ImageButton btnConfirmar;
    private ImageButton btnAtrasLogico;
    private Constants.TipoOtpAutenticacion tokenAMostrar;
    private Constants.TipoInstrumento tipoInstrumentoSeguridad;
    private LinearLayout linearPass;
    private LinearLayout linearNIP;
    private LinearLayout linearCVV;
    private LinearLayout linearMostrarToken;
    private boolean mostrarContrasena, mostratNip, mostrarCVV, mostrarToken;
    private String contrasena = "", nip = "", cvv = "", token = "";
    private EditText txtContrasena, txtNip, txtCvv, txtToken;

    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState, 0, R.layout.layout_bmovil_contratacion_gestor_remoto);
        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        SuiteApp suiteApp = (SuiteApp) getApplication();
        setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
        setDelegate(parentViewsController.getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initValues();
    }

    public void initValues()
    {
        txtDescBanquero = (TextView) findViewById(R.id.txtDescBanqueroHtml);
        txtDescBanquero.setText(Html.fromHtml("<font>El </font><font color='#86c82d' size=2>nuevo servicio</font><font> de asesoría financiera desde donde estés.</font>"));
        txtAceptoTerminos = (TextView) findViewById(R.id.txtAceptoTyCBanquero);
        txtAceptoTerminos.setText(Html.fromHtml("<font>Acepto los </font><font color='#52bcec'><u>Términos y Condiciones</u> </font><font>del servicio &#34Banquero Personal Contigo.&#34</font>"));
        linearToken = (LinearLayout) findViewById(R.id.linearToken);
        linearConfirmarBanquero = (LinearLayout) findViewById(R.id.linearConfirmarBanquero);
        btnOff = (ImageButton) findViewById(R.id.btnchecoOff);
        btnOnn = (ImageButton) findViewById(R.id.btnchecoOnn);
        btnConfirmar = (ImageButton) findViewById(R.id.btnConfirmarBanquero);
        btnAtrasLogico = (ImageButton) findViewById(R.id.btnAtrasLogico);
        btnOff.setOnClickListener(this);
        btnOnn.setOnClickListener(this);
        btnConfirmar.setOnClickListener(this);
        btnAtrasLogico.setOnClickListener(this);
        txtAceptoTerminos.setOnClickListener(this);
        linearPass = (LinearLayout) findViewById(R.id.linearMostrarPass);
        txtContrasena = (EditText) findViewById(R.id.txtPass);
        linearNIP = (LinearLayout) findViewById(R.id.linearMostrarNIP);
        txtNip = (EditText) findViewById(R.id.txtNip);
        linearCVV = (LinearLayout) findViewById(R.id.linearMostrarCVV);
        txtCvv = (EditText) findViewById(R.id.txtCvv);
        linearMostrarToken = (LinearLayout) findViewById(R.id.linearMostrarToken);
        txtToken = (EditText) findViewById(R.id.txtToken);
        revisarAutenticacion();
    }

    @Override
    public void onClick(final View v)
    {
        if (v.equals(btnOff)){
            btnOff.setVisibility(View.GONE);
            btnOnn.setVisibility(View.VISIBLE);
            linearToken.setVisibility(View.VISIBLE);
            linearConfirmarBanquero.setVisibility(View.VISIBLE);

        }
        else if (v.equals(btnOnn)){
            btnOff.setVisibility(View.VISIBLE);
            btnOnn.setVisibility(View.GONE);
            linearToken.setVisibility(View.GONE);
            linearConfirmarBanquero.setVisibility(View.GONE);
        }
        else if (v.equals(btnConfirmar))
        {
            final ContratacionBanqueroPersonalDelegate delegate = new ContratacionBanqueroPersonalDelegate();
            if (validaCamposConfirmacion()){
                ConfirmacionBanqueroDelegate banqueroDelegate = new ConfirmacionBanqueroDelegate();
                String newToken = null;
                if(tokenAMostrar != Constants.TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == Constants.TipoInstrumento.SoftToken && (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService())){
                    if (SuiteAppCRApi.getSofttokenStatus()) {
                        if (ServerCommons.ALLOW_LOG) {
                            Log.d("APP", "softToken local");
                        }
                        newToken = banqueroDelegate.loadOtpFromSofttoken(tokenAMostrar);
                        txtToken.setText(newToken);
                        token=txtToken.getText().toString();
                        delegate.peticionContratacionBanqueroPersonal(this, contrasena, nip, cvv, token);
                    } else if (!SuiteAppCRApi.getSofttokenStatus() && PropertiesManager.getCurrent().getSofttokenService()) {
                        if (ServerCommons.ALLOW_LOG) {
                            Log.d("APP", "softoken compartido");
                        }

                        GetOtpBmovil otpG = new GetOtpBmovil(new GetOtp() {
                            /**
                             *
                             * @param otp
                             */
                            @Override
                            public void setOtpRegistro(String otp) {

                            }

                            /**
                             *
                             * @param otp
                             */
                            @Override
                            public void setOtpCodigo(String otp) {
                                if (ServerCommons.ALLOW_LOG) {
                                    Log.d("APP", "la otp en callback: " + otp);
                                }
                                delegate.peticionContratacionBanqueroPersonal(BaseViewController.getInstance(), contrasena, nip, cvv, otp);
                            }

                            /**
                             *
                             * @param otp
                             */
                            @Override
                            public void setOtpCodigoQR(String otp) {

                            }
                        }, com.bancomer.base.SuiteApp.appContext);

                        otpG.generateOtpCodigo();
                    }
                }else{
                    delegate.peticionContratacionBanqueroPersonal(BaseViewController.getInstance(), contrasena, nip, cvv, token);
                }
            }
        } else if (v.equals(btnAtrasLogico)) {
            goBack();
        }
        else if (v.equals(txtAceptoTerminos))
        {
            final ContratacionBanqueroPersonalDelegate delegate = new ContratacionBanqueroPersonalDelegate();
            delegate.peticionConsultaContratoGestor(this);
        }
    }

    private void revisarAutenticacion()
    {

        if (Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.gestorRemoto, Session.getInstance(this).getClientProfile())) {
            linearPass.setVisibility(View.VISIBLE);
            mostrarContrasena = Boolean.TRUE;
        }
        if (Autenticacion.getInstance().mostrarNIP(Constants.Operacion.gestorRemoto,Session.getInstance(this).getClientProfile()))
        {
            linearNIP.setVisibility(View.VISIBLE);
            mostratNip = Boolean.TRUE;
        }
        if (Autenticacion.getInstance().mostrarCVV(Constants.Operacion.gestorRemoto,Session.getInstance(this).getClientProfile()))
        {
            linearCVV.setVisibility(View.VISIBLE);
            mostrarCVV = Boolean.TRUE;
        }

        tokenAMostrar = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.gestorRemoto, Session.getInstance(this).getClientProfile());
        if (tokenAMostrar != Constants.TipoOtpAutenticacion.ninguno) {
            linearMostrarToken.setVisibility(View.VISIBLE);
        }
        String instrumento = Session.getInstance(SuiteApp.appContext).getSecurityInstrument();
        if (instrumento.equals(Constants.IS_TYPE_DP270)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
        } else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
        } else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)||instrumento.equals(Constants.TYPE_SOFTOKEN.S2.value)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
        } else {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
        }


    }

    private boolean validaCamposConfirmacion()
    {
        if (mostrarContrasena) {
            contrasena = txtContrasena.getText().toString();
            if (contrasena.equals("")) {
                String mensaje = getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += getString(R.string.confirmation_componenteContrasena);
                mensaje += ".";
                showInformationAlert(mensaje);
                return false;
            } else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
                String mensaje = getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.PASSWORD_LENGTH;
                mensaje += " ";
                mensaje += getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += getString(R.string.confirmation_componenteContrasena);
                mensaje += ".";
                showInformationAlert(mensaje);
                return false;
            }
        }
        if (mostratNip) {
            nip = txtNip.getText().toString();
            if (nip.equals("")) {
                String mensaje = getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += getString(R.string.confirmation_componenteNip);
                mensaje += ".";
                showInformationAlert(mensaje);
                return false;
            } else if (nip.length() != Constants.NIP_LENGTH) {
                String mensaje = getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.NIP_LENGTH;
                mensaje += " ";
                mensaje += getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += getString(R.string.confirmation_componenteNip);
                mensaje += ".";
                showInformationAlert(mensaje);
                return false;
            }
        }
        if (mostrarCVV) {
            cvv = txtCvv.getText().toString();
            if (cvv.equals("")) {
                String mensaje = getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += getString(R.string.confirmation_componenteCvv);
                mensaje += ".";
                showInformationAlert(mensaje);
                return false;
            } else if (cvv.length() != Constants.CVV_LENGTH) {
                String mensaje = getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.CVV_LENGTH;
                mensaje += " ";
                mensaje += getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += getString(R.string.confirmation_componenteCvv);
                mensaje += ".";
                showInformationAlert(mensaje);
                return false;
            }
        }
        if (tokenAMostrar != Constants.TipoOtpAutenticacion.ninguno) {
            token = txtToken.getText().toString();
            if (token.length() != Constants.ASM_LENGTH) {
                String mensaje = getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.ASM_LENGTH;
                mensaje += " ";
                mensaje += getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                switch (tipoInstrumentoSeguridad) {
                    case OCRA:
                        mensaje += getEtiquetaCampoOCRA();
                        break;
                    case DP270:
                        mensaje += getEtiquetaCampoDP270();
                        break;
                    case SoftToken:
                        if (SuiteApp.getSofttokenStatus()) {
                            mensaje += getEtiquetaCampoSoftokenActivado();
                        } else {
                            mensaje += getEtiquetaCampoSoftokenDesactivado();
                        }
                        break;
                    default:
                        break;
                }
                mensaje += ".";
                showInformationAlert(mensaje);
                return false;
            }
            else {
                return true;
            }
        }

        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    public String getEtiquetaCampoOCRA() {
        return  parentViewsController.getCurrentViewController().getString(R.string.confirmation_ocra);
    }

    public String getEtiquetaCampoDP270() {
        return  parentViewsController.getCurrentViewController().getString(R.string.confirmation_dp270);
    }

    public String getEtiquetaCampoSoftokenActivado() {
        return  parentViewsController.getCurrentViewController().getString(R.string.confirmation_softtokenActivado);
    }

    public String getEtiquetaCampoSoftokenDesactivado() {
        return parentViewsController.getCurrentViewController().getString(R.string.confirmation_softtokenDesactivado);
    }

    @Override
    public void goBack() {
        final ContratacionBanqueroPersonalDelegate delegate = new ContratacionBanqueroPersonalDelegate();
        delegate.peticionConsultaDetalleContratoGestor(this);
        parentViewsController.removeDelegateFromHashMap(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID);
        super.goBack();
    }

}
