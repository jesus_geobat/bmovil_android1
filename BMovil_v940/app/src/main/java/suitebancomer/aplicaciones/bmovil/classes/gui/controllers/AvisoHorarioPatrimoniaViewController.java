package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import suitebancomer.aplicaciones.bmovil.classes.common.SpinnerLlamadaPatrimonial;
import suitebancomer.aplicaciones.bmovil.classes.common.SpinnerPatrimonialListView;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ContactoPatrimonialDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.LlamadaPatrimonialDelegate;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by marioaguilar IDS on 19/09/16.
 */
public class AvisoHorarioPatrimoniaViewController extends BaseViewController implements View.OnClickListener {

    private DisplayMetrics displayMetrics;
    private LlamadaPatrimonialDelegate delegate;
    private Dialog dialogSpinner;
    private Context context;
    private TextView TVhorarioAtencion ;
    private TextView TVLineabancomer ;
    private ImageView IMCancel ;
    private ImageView IMCall ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState,SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_contacto_llamada_patrimonial);

        context = this;

        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());

        delegate = (LlamadaPatrimonialDelegate) parentViewsController.getBaseDelegateForKey(LlamadaPatrimonialDelegate.LLAMADA_PATRIMONIAL_DELEGATE);
        delegate.setController(this);

        customizeView();
        linkViews();
        SetData();
        Clics();


    }

    public void linkViews (){

        TVhorarioAtencion =  (TextView)findViewById(R.id.text_horarioPatrimonial);
         TVLineabancomer = (TextView)findViewById(R.id.text_horarioLineaBancomer);
         IMCancel =(ImageView)findViewById(R.id.CancelCall);
          IMCall =(ImageView)findViewById(R.id.makingCall);

    }


    private void customizeView()
    {

        setTitle(R.string.bmovil_contactos_cabecera, R.drawable.bmovil_mis_cuentas_icono);
        findViewById(R.id.principal_header_content).setBackgroundResource(0);
        findViewById(R.id.title_divider).setVisibility(View.GONE);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio())
            return;
        getParentViewsController().setCurrentActivityApp(this);
    }


    @Override
    public void onClick(View v) {
      if(v == IMCall ){
          dialogAction();
        }
        if(v == IMCancel){
            goBack();
        }
    }



    private void dialogAction()
    {
        System.out.println("Inside _ Dialog");
        dialogSpinner = new Dialog(context);
        delegate.setCurrentDialog(dialogSpinner);
        dialogSpinner.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSpinner.getWindow().setBackgroundDrawable(new ColorDrawable(00000000));
        dialogSpinner.setContentView(R.layout.dialog_bmovil_patrimonial_llamadas);
        ((SpinnerLlamadaPatrimonial)dialogSpinner.findViewById(R.id.listSpinnerContratos)).setController(((BaseViewController) context ));
        dialogSpinner.show();
    }

    public void SetData ( ){
        String text = "<font color='white'>Estimado cliente, su llamada está fuera del horario de servicio, el horario de atención es de </font><font color='#52bcec'>8:30 - 18:00 hrs.</font>.";
        TVhorarioAtencion.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        text = "<font color='white'>¿Desea que lo comuniquemos con un asesor de </font><font color='#52bcec'>Línea Bancomer Elite</font> <font color='white'>?</font>";
        TVLineabancomer.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

    }


    public void Clics(){
        IMCall.setOnClickListener(this);
        IMCancel.setOnClickListener(this);

    }



}
