package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;


import java.util.ArrayList;

import bancomer.api.alertasdigitales.gui.controllers.AvisoDialogos;
import bancomer.api.codigoqr.config.CodigoQRFacade;
import bancomer.api.codigoqr.config.LeerCodigoQRResultHandler;
import bancomer.api.codigoqr.models.OperacionQR;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.SincronizarSesion;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaInterbancaria;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaOtrosBBVA;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.base.callback.CallBackSession;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelper;

public class TransferViewController extends BaseViewController implements OnClickListener, CallBackSession, LeerCodigoQRResultHandler {

	LinearLayout vista;
	public CuentaOrigenViewController componenteCtaOrigen;
	public ListaSeleccionViewController listaSeleccion;
	ImageButton registrarNuevaCuenta;
	TransferirDelegate delegate;
	//AMZ
		public BmovilViewsController parentManager;
		//AMZ
	private boolean patrimonial= false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_transfer_view);
		setTitle(R.string.opcionesTransfer_menu_otrosbancos,R.drawable.bmovil_transferir_icono);

		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID));
		delegate = (TransferirDelegate)getDelegate();
		int title = 0;
		int icono = R.drawable.bmovil_transferir_icono;

			if (delegate.getTipoOperacion().equals(Constants.Operacion.transferirBancomer.value)) {
				if (delegate.isExpress()) {
					title = R.string.transferir_otrosBBVA_express_title;
				} else if (delegate.isTDC()) {
					title = R.string.transferir_otrosBBVA_TDC_title;
					icono = R.drawable.icono_pagar_servicios;
				} else {
					title = R.string.transferir_otrosBBVA_title;
				}
			} else if (delegate.getTipoOperacion().equals(Constants.Operacion.transferirInterbancaria.value)) {
				title = R.string.opcionesTransfer_menu_otrosbancos;
			} else if (delegate.getTipoOperacion().equals(Constants.Operacion.dineroMovil.value)) {
				title = R.string.transferir_dineromovil_titulo;
				icono = R.drawable.bmovil_dinero_movil_icono;
			}  else if (delegate.getTipoOperacion().equals(Constants.Operacion.retiroSinTarjeta.value)) {
				title = R.string.transferir_retiro_sin_tarjeta;
				icono = R.drawable.bmovil_dinero_movil_icono;
			}

			setTitle(title, icono);





		
		delegate.setControladorTransferencias(this);
		vista = (LinearLayout)findViewById(R.id.transfer_view_controller_layout);
		registrarNuevaCuenta = (ImageButton) findViewById(R.id.transfer_view_controller_btn_cuenta_nueva);
		registrarNuevaCuenta.setOnClickListener(this);
		
		if(Server.ALLOW_LOG) Log.d("TransferViewController", "La operación recibida es: "+ delegate.getTipoOperacion());
		init();
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
				if(title == R.string.transferir_otrosBBVA_express_title){
					TrackingHelper.trackState("express", parentManager.estados);
				}else if(title == R.string.transferir_otrosBBVA_TDC_title){
					TrackingHelper.trackState("tarjeta", parentManager.estados);
				}else if(title == R.string.transferir_otrosBBVA_title){
					TrackingHelper.trackState("otras cuentas", parentManager.estados);
				}else if(title == R.string.opcionesTransfer_menu_otrosbancos){
					TrackingHelper.trackState("otros bancos", parentManager.estados);
				}else if(title == R.string.transferir_dineromovil_titulo){
					TrackingHelper.trackState("movil", parentManager.estados);
				}else if(title == R.string.mis_cuentas_title){
					TrackingHelper.trackState("mis cuentas", parentManager.estados);
				}else if(title == R.string.transferir_retiro_sin_tarjeta){
					TrackingHelper.trackState("retironsintarjeta", parentManager.estados);
				}
	}
	
	public void init(){
		Account cuentaEje = Tools.obtenerCuentaEje();
		if(Server.ALLOW_LOG) System.out.println("Cuenta Eje: "+cuentaEje);
		if(null == cuentaEje) {
			goBack();
			return;
		}
		//No se puede operar si la cuenta eje es TDC
//		if(Constants.Perfil.basico == Session.getInstance(SuiteApp.appContext).getClientProfile() && 
//		   cuentaEje.getType().equalsIgnoreCase(Constants.CREDIT_TYPE)) {
//			showInformationAlert(R.string.alert_unallowed_operation);
//			SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showOpcionesTransferir(Operacion.transferir.value,
//																											R.drawable.bmovil_transferir_icono,
//																											R.string.opciones_transferir_title,
//																											R.string.opcionesTransfer_menu_titulo);
//			return;
//		}
//		
		cargaCuentaOrigenComponent();
		cargaListaSeleccionComponent();
		
		String tipoConsultaFrecuentes = "";
		
		if (delegate.getTipoOperacion().equals(Constants.Operacion.transferirInterbancaria.value)) {
		
			tipoConsultaFrecuentes = Constants.tipoCFOtrosBancos;

		} else if (delegate.getTipoOperacion().equals(Constants.Operacion.transferirBancomer.value)) {
	
			if (delegate.isTDC()) {
				tipoConsultaFrecuentes = Constants.tipoCFTarjetasCredito;
			} else if (delegate.isExpress()) {
				tipoConsultaFrecuentes = Constants.tipoCFCExpress;
			} else

			tipoConsultaFrecuentes = Constants.tipoCFOtrosBBVA;

		} else if (delegate.getTipoOperacion().equals(Constants.Operacion.dineroMovil.value)) {
			
			tipoConsultaFrecuentes = Constants.tipoCFDineroMovil;
		}
		delegate.consultarFrecuentes(tipoConsultaFrecuentes);
		scaleForCurrentScreen();
	}
	
	@SuppressWarnings("deprecation")
	public void cargaCuentaOrigenComponent(){
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		params.topMargin = 8;
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
//		params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
		

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.setDelegate(delegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.init();
		vista.addView(componenteCtaOrigen);
		
	}
	
	public void muestraCombo(){
		
	}
	
	
	public void cargaListaSeleccionComponent(){
		delegate.setCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		
		ArrayList<Object> lista  = delegate.getDatosTablaFrecuentes();
		
		GuiTools guiTools = GuiTools.getCurrent();
		
		LinearLayout.LayoutParams params;
		params = new LayoutParams(guiTools.getEquivalenceInPixels(280.0), LinearLayout.LayoutParams.WRAP_CONTENT);

		ArrayList<Object> registros;
		//ArrayList<Object> lista = new ArrayList<Object>();
		ArrayList<Object> encabezado = new ArrayList<Object>();
		encabezado.add(null);
		if (delegate.getTipoOperacion().equals(Constants.Operacion.pagoServicios.value)) {
			encabezado.add(SuiteApp.appContext.getString(R.string.servicesPayment_frequentList_headerList));
		} else {
			encabezado.add(getString(R.string.altafrecuente_nombrecorto));
		}
		
		if (delegate.getTipoOperacion().equals(Constants.Operacion.transferirInterbancaria.value)) {
			encabezado.add(getString(R.string.bmovil_consultar_frecuentes_header_banco));
		}
		if (delegate.isExpress())
			encabezado.add(getString(R.string.transferir_otrosBBVA_destiny_express));
		else
			encabezado.add(getString(R.string.transferir_lista_seleccion_cuenta));
		
		//LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
		//params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
		//params.rightMargin =  getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
		//params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
		
		if (listaSeleccion == null) {
			listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
			listaSeleccion.setDelegate(delegate);
			 if (delegate.getTipoOperacion().equals(Constants.Operacion.transferirInterbancaria.value)) {
				 listaSeleccion.setNumeroColumnas(3);
			 } else if (delegate.getTipoOperacion().equals(Constants.Operacion.pagoServicios.value)){
				 listaSeleccion.setNumeroColumnas(1);
			 } else {
				listaSeleccion.setNumeroColumnas(2);
			 }
			listaSeleccion.setEncabezado(encabezado);
			listaSeleccion.setTitle(getString(R.string.mainMenu_favoritePayment));
			listaSeleccion.setLista(lista);
			if (lista.size() == 0) {
				listaSeleccion.setTextoAMostrar(getString(R.string.bmovil_consultar_frecuentes_emptylist));
			}else {
				listaSeleccion.setTextoAMostrar(null);
				listaSeleccion.setNumeroFilas(lista.size());		
			}
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setExisteFiltro(true);
			listaSeleccion.setSingleLine(true);
			listaSeleccion.cargarTabla();
//			if (listaCuetasAMostrar.size() == 0){
//				listaSeleccion.setEnabled(false);
//			}
			vista.addView(listaSeleccion);

		}else {
			listaSeleccion.setTextoAMostrar(null);
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.cargarTabla();
		}
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
	}
	
	public void consultaCamposAMostrar(){
		
	}
	
	public void actualizaCombo(){
		delegate.setCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
	}
	
	public void validaCampos(){
		
	}

	@Override
	public void onClick(View v) {
		//ARR
				Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
		if(!isHabilitado())
			return;
		setHabilitado(false);
		if(!("T3".equalsIgnoreCase(Session.getInstance(SuiteApp.appContext).getSecurityInstrument()))) {
			if (v == registrarNuevaCuenta && !parentViewsController.isActivityChanging()) {
				if (delegate.getTipoOperacion().equals(Constants.Operacion.transferirInterbancaria.value)) {
					//ARR
					paso1OperacionMap.put("evento_paso1", "event46");
					paso1OperacionMap.put("&&products", "operaciones;transferencias+otros bancos");
					paso1OperacionMap.put("eVar12", "paso1:eleccion cuenta");

					TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
				((BmovilViewsController)parentViewsController).showInterbancariosViewController(Constants.Operacion.transferirInterbancaria);
				} else if (delegate.getTipoOperacion().equals(Constants.Operacion.transferirBancomer.value)) {
					//Comprobar si es otras cuentas BBVA, express o TDC

					//ARR
					if(delegate.isExpress())
					{
						//ARR
						paso1OperacionMap.put("evento_paso1", "event46");
						paso1OperacionMap.put("&&products", "operaciones;transferencias+cuenta express");
						paso1OperacionMap.put("eVar12", "paso1:eleccion cuenta");

						TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
					}
					else if(delegate.isTDC())
					{
						//ARR
						paso1OperacionMap.put("evento_paso1", "event46");
						paso1OperacionMap.put("&&products", "operaciones;pagar+tarjeta credito");
						paso1OperacionMap.put("eVar12", "paso1:eleccion tarjeta");

						TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
					}
					else
					{

						//ARR
						paso1OperacionMap.put("evento_paso1", "event46");
						paso1OperacionMap.put("&&products", "operaciones;transferencias+otra cuenta bbva bancomer");
						paso1OperacionMap.put("eVar12", "paso1:eleccion cuenta");
						TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
					}
					((BmovilViewsController)parentViewsController).showOtrosBBVAViewController(null,Constants.Operacion.transferirBancomer, delegate.isExpress(), delegate.isTDC());
				} else if (delegate.getTipoOperacion().equals(Constants.Operacion.dineroMovil.value)) {
					//ARR
					paso1OperacionMap.put("evento_paso1", "event46");
					paso1OperacionMap.put("&&products", "operaciones;transferencias+dinero movil");
					paso1OperacionMap.put("eVar12", "paso1:eleccion cuenta");

					TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
			
					((BmovilViewsController)parentViewsController).showTransferirDineroMovil(null,false);
				}
			
				else if (delegate.getTipoOperacion().equals(Constants.Operacion.retiroSinTarjeta.value)) {
					//ARR
					paso1OperacionMap.put("evento_paso1", "event46");
					paso1OperacionMap.put("&&products", "operaciones;transferencias+retiro sin tarjeta");
					paso1OperacionMap.put("eVar12", "paso1:eleccion cuenta");
					TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
					((BmovilViewsController)parentViewsController).showRetiroSinTarjetaViewController(null, AltaRetiroSinTarjetaDelegate.TipoRetiro.RETIRO_SIN_TARJETA);
				}
			}
		} else {
			setHabilitado(true);
			hideCurrentDialog();
			Log.i("Muestra Alert", "Muestra alert");
			FragmentManager fragmentManager = getSupportFragmentManager();
			setActivityChanging(true);
			getParentViewsController().setCurrentActivityApp(this);
			PopUpActivarTokenMovilController alerts_dialogs = new PopUpActivarTokenMovilController();
			alerts_dialogs.setCaller(this);
			alerts_dialogs.show(fragmentManager, null);
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setHabilitado(true);
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		if (delegate != null) {
			delegate.setControladorTransferencias(this);
			if (delegate.getTipoOperacion().equals(Constants.Operacion.transferirInterbancaria.value)) {
				componenteCtaOrigen.setIndiceCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().indexOf(delegate.getTransferenciaInterbancaria().getCuentaOrigen()));
			} else if (delegate.getTipoOperacion().equals(Constants.Operacion.transferirBancomer.value)) {
				componenteCtaOrigen.setIndiceCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().indexOf(delegate.getTransferenciaOtrosBBVA().getCuentaOrigen()));
				if (delegate.isTDC()) {
					listaSeleccion.showButtonBuscar();
					showButtonLeerQR(R.drawable.btn_pagarqr);
				} else if (!delegate.isExpress()) {
					listaSeleccion.showButtonBuscar();
					showButtonLeerQR(R.drawable.btn_transferirqr);
				}
			} else if (delegate.getTipoOperacion().equals(Constants.Operacion.dineroMovil.value)) {
				componenteCtaOrigen.setIndiceCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().indexOf(delegate.getTransferenciaDineroMovil().getCuentaOrigen()));
			}
			
			//componenteCtaOrigen.actualizaComponente();
			componenteCtaOrigen.actualizaComponente(false);
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(TransferirDelegate.TRANSFERIR_DELEGATE_ID);
		super.goBack();
	}	
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}
	
	
	@SuppressWarnings("unchecked")
	public void muestraFrecuentes() {

		//ArrayList<Object> listaEncabezado = null;
		ArrayList<Object> listaDatos = null;

			//listaEncabezado = ((InterbancariosDelegate) delegate).getDatosHeaderTablaFrecuentes();
			listaDatos = delegate.getDatosTablaFrecuentes();
		

		if (listaDatos.size() > 0) {
			listaSeleccion.setTextoAMostrar(null);
			listaSeleccion.setLista(listaDatos);
			listaSeleccion.setNumeroColumnas(((ArrayList<Object>) listaDatos.get(0)).size() - 1) ;
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(listaDatos.size());
			listaSeleccion.getFiltroLista().setListaOriginal(listaDatos);
			listaSeleccion.setSingleLine(true);
			listaSeleccion.cargarTabla();
		}else {
			listaSeleccion.setLista(listaDatos);
			listaSeleccion.setTextoAMostrar(getString(R.string.bmovil_consultar_frecuentes_emptylist));
			listaSeleccion.cargarTabla();
		}
		//listaSeleccion.listview.setVerticalScrollBarEnabled(false);
		//listaSeleccion.recalcularSize();//recalcularTamanioTabla(listaFrecuentes.listview);
	}
	
	public void frecuenteSeleccionado(Object modeloFrecuente, boolean esExpress, boolean esTDC){
		//ARR
				Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
		if (modeloFrecuente instanceof TransferenciaInterbancaria) {
		//	if (delegate.isOpenHours()) {
			boolean flag;
			
			if(Server.ALLOW_LOG) Log.w("Tipo de cuenta de frecuente seleccionado",((TransferenciaInterbancaria) modeloFrecuente).getTipoCuentaDestino());
			
			if(((TransferenciaInterbancaria) modeloFrecuente).getTipoCuentaDestino().equals(Constants.TP_TC_VALUE))
				flag=delegate.isOpenHoursTC();
			else
				flag=delegate.isOpenHours();
			
			if (flag) {
//paso1 otros bancos
				
				//ARR
				paso1OperacionMap.put("evento_paso1", "event46");
				paso1OperacionMap.put("&&products", "operaciones;transferencias+otros bancos");
				paso1OperacionMap.put("eVar12", "paso1:eleccion cuenta");

				TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
				
				((BmovilViewsController)parentViewsController).showInterbancariosViewController(modeloFrecuente);
			} else {
				if(((TransferenciaInterbancaria) modeloFrecuente).getTipoCuentaDestino().equals(Constants.TP_TC_VALUE))
					this.showInformationAlert(delegate.textoMensajeAlertaTC());
				else
					this.showInformationAlert(delegate.textoMensajeAlerta());
	            //this.showInformationAlert(delegate.textoMensajeAlerta());
			}
		} else if (modeloFrecuente instanceof TransferenciaOtrosBBVA) {
			//ver si esExpress es true --> paso 1 express
			//ver si tdc es true --> paso 1 tarjeta de credito
			//ver si los dos son false --> paso 1 otras cuentas bbva 
				
			if(esExpress)
			{
				//ARR
				paso1OperacionMap.put("evento_paso1", "event46");
				paso1OperacionMap.put("&&products", "operaciones;transferencias+cuenta express");
				paso1OperacionMap.put("eVar12", "paso1:eleccion cuenta");
		
				TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
			}
			else if(esTDC)
			{
				//ARR
				paso1OperacionMap.put("evento_paso1", "event46");
				paso1OperacionMap.put("&&products", "operaciones;pagar+tarjeta credito");
				paso1OperacionMap.put("eVar12", "paso1:eleccion tarjeta");
				TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
			}
			else
			{
				//ARR
				paso1OperacionMap.put("evento_paso1", "event46");
				paso1OperacionMap.put("&&products", "operaciones;transferencias+otra cuenta bbva bancomer");
				paso1OperacionMap.put("eVar12", "paso1:eleccion cuenta");
		
				TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
			}
			((BmovilViewsController)parentViewsController).showOtrosBBVAViewController((TransferenciaOtrosBBVA) modeloFrecuente, Constants.Operacion.transferirBancomerF, esExpress, esTDC);
		} else if(modeloFrecuente instanceof TransferenciaDineroMovil) {
			//paso1 dinero movil
			//ARR
			paso1OperacionMap.put("evento_paso1", "event46");
			paso1OperacionMap.put("&&products", "operaciones;transferencias+dinero movil");
			paso1OperacionMap.put("eVar12", "paso1:eleccion cuenta");

			TrackingHelper.trackPaso1Operacion(paso1OperacionMap);

			((BmovilViewsController)parentViewsController).showRetiroSinTarjetaViewController((TransferenciaDineroMovil) modeloFrecuente, AltaRetiroSinTarjetaDelegate.TipoRetiro.DINERO_MOVIL_FRECUENTE);
//			((BmovilViewsController)parentViewsController).showTransferirDineroMovil((TransferenciaDineroMovil) modeloFrecuente, false);
		}
		
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(MotionEvent.ACTION_DOWN == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_DOWN");
			listaSeleccion.setMarqueeEnabled(false);
		}else if(MotionEvent.ACTION_UP == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_UP");
			listaSeleccion.setMarqueeEnabled(true);
		}
		
//		float touchX = ev.getX();
//		float touchY = ev.getY();
//		int[] listaSeleccionPos = new int[2];
//		listaSeleccion.getLocationOnScreen(listaSeleccionPos);
//		float listaSeleccionX2 = listaSeleccionPos[0] + listaSeleccion.getMeasuredWidth();
//		float listaSeleccionY2 = listaSeleccionPos[1] + listaSeleccion.getMeasuredHeight();
//		
//		if ((touchX >= listaSeleccionPos[0] && touchX <= listaSeleccionX2) &&
//			(touchY >= listaSeleccionPos[1] && touchY <= listaSeleccionY2)) {
//			listaSeleccion.getParent().requestDisallowInterceptTouchEvent(true);
//			if(Server.ALLOW_LOG) Log.d("DispatchTouchEvent", "Inside lista, Intercept events disallowed");
//		} else {
		
			listaSeleccion.getParent().requestDisallowInterceptTouchEvent(false);
			if(Server.ALLOW_LOG) Log.d("DispatchTouchEvent", "Outside lista, Intercept events reallowed");
//		}
		
		return super.dispatchTouchEvent(ev);
	}
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.layoutRoot));
		guiTools.scale(registrarNuevaCuenta);
	}



	public void showButtonLeerQR(int drawableButtonID) {
		final ImageButton botonLeerQR = (ImageButton) findViewById(R.id.boton_transferir_leer_qr);
		botonLeerQR.setImageDrawable(getResources().getDrawable(drawableButtonID));
		botonLeerQR.setVisibility(View.VISIBLE);
		botonLeerQR.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				CodigoQRFacade.getInstance().configurar(TransferViewController.this, TransferViewController.this);
				getParentViewsController().setActivityChanging(true);
				CodigoQRFacade.getInstance().showLeerCodigoQR(TransferViewController.this);
			}
		});
	}

	@Override
	public void userInteraction() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();
	}

	@Override
	public void cierraSesionBackground(Boolean isChanging) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		parentViewsController.setActivityChanging(isChanging);
		if (parentManager.consumeAccionesDePausa()) {
			finish();
			if (MenuHamburguesaViewsControllers.getIsOterApp()) {
				MenuHamburguesaViewsControllers.setIsOterApp(Boolean.FALSE);
			}
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	public void cierraSesion() {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication()
				.setApplicationInBackground(Boolean.FALSE);
		SuiteApp.getInstance().getBmovilApplication().logoutApp(Boolean.FALSE);
	}

	@Override
	public void returnMenuPrincipal() {
		Session.getInstance(SuiteApp.appContext).reloadFiles();
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
	}

	@Override
	public void closeSession(String userName, String ium, String client) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBmovilApp().closeSession(userName, ium, client);
	}

	@Override
	public void setClientProfile(Constants.Perfil perfil) {

	}

	@Override
	public void gestionaResultadoLecturaQR(OperacionQR operacionQR) {
		Payment selectedPayment = null;
		String cuentaDestino = null;
		if(operacionQR.getTipoCuentaDestino().equalsIgnoreCase(Constants.CREDIT_TYPE) || operacionQR.getTipoCuentaDestino().equalsIgnoreCase(Constants.DEBIT_TYPE)){
			cuentaDestino = operacionQR.getCuentaDestino().substring(operacionQR.getCuentaDestino().length() - 16);
		}else{
			cuentaDestino = operacionQR.getCuentaDestino().substring(operacionQR.getCuentaDestino().length() - 10);
		}

		operacionQR.setCuentaDestino(cuentaDestino);
		if (delegate.getFrecuentes() != null) {
			for (Payment tmp : delegate.getFrecuentes()) {
				if (tmp.getBeneficiaryAccount().equals(cuentaDestino)) {
					selectedPayment = tmp;
					break;
				}
			}
		}

		TransferenciaOtrosBBVA transferenciaOtrosBBVA = delegate.getTransferenciaOtrosBBVA();

		if (selectedPayment != null) {
			if (delegate.isExpress()) {
				transferenciaOtrosBBVA.setCuentaDestino(selectedPayment.getBeneficiaryAccount().replaceAll("[^0-9]", ""));
				transferenciaOtrosBBVA.setBeneficiario(selectedPayment.getBeneficiary());
				transferenciaOtrosBBVA.setImporte(selectedPayment.getAmount());
				transferenciaOtrosBBVA.setAliasFrecuente(selectedPayment.getNickname());
				transferenciaOtrosBBVA.setTipoCuenta(Constants.EXPRESS_TYPE);
				transferenciaOtrosBBVA.setTipoOperacion(Constants.tipoCFCExpress);
				//multicanal
				transferenciaOtrosBBVA.setFrecuenteMulticanal(selectedPayment.getIdToken());
				transferenciaOtrosBBVA.setIdCanal(selectedPayment.getIdCanal());
				transferenciaOtrosBBVA.setUsuario(selectedPayment.getUsuario());
			} else if (delegate.isTDC()) {
				transferenciaOtrosBBVA.setCuentaDestino(selectedPayment.getBeneficiaryAccount().substring(2));
				transferenciaOtrosBBVA.setBeneficiario(selectedPayment.getBeneficiary());
				transferenciaOtrosBBVA.setTipoCuenta(Constants.CREDIT_TYPE);
				transferenciaOtrosBBVA.setTipoOperacion(Constants.tipoCFTarjetasCredito);
				//multicanal
				transferenciaOtrosBBVA.setFrecuenteMulticanal(selectedPayment.getIdToken());
				transferenciaOtrosBBVA.setIdCanal(selectedPayment.getIdCanal());
				transferenciaOtrosBBVA.setUsuario(selectedPayment.getUsuario());
				transferenciaOtrosBBVA.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());
			} else {
				transferenciaOtrosBBVA.setCuentaDestino(selectedPayment.getBeneficiaryAccount().replaceAll("[^0-9]", ""));
				transferenciaOtrosBBVA.setBeneficiario(selectedPayment.getBeneficiary());
				transferenciaOtrosBBVA.setTipoCuenta(selectedPayment.getBeneficiaryTypeAccount());
				transferenciaOtrosBBVA.setTipoOperacion(null);
				//multicanal
				transferenciaOtrosBBVA.setFrecuenteMulticanal(selectedPayment.getIdToken());
				transferenciaOtrosBBVA.setIdCanal(selectedPayment.getIdCanal());
				transferenciaOtrosBBVA.setUsuario(selectedPayment.getUsuario());
				transferenciaOtrosBBVA.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());
			}
		} else {
			transferenciaOtrosBBVA.setCuentaDestino(operacionQR.getCuentaDestino());
			transferenciaOtrosBBVA.setTipoCuenta(operacionQR.getTipoCuentaDestino());
		}

		transferenciaOtrosBBVA.setAliasFrecuente(operacionQR.getNombreTitular());
		transferenciaOtrosBBVA.setImporte(operacionQR.getImporte());
		transferenciaOtrosBBVA.setConcepto(operacionQR.getMotivoPago());
		if(selectedPayment != null){
			frecuenteSeleccionado(transferenciaOtrosBBVA, delegate.isExpress(), delegate.isTDC());
		} else {
			((BmovilViewsController) parentViewsController).showOtrosBBVAViewController(transferenciaOtrosBBVA, Constants.Operacion.transferirBancomer, delegate.isExpress(), delegate.isTDC());
		}
	}
}
