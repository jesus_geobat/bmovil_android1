package suitebancomer.aplicaciones.bmovil.classes.model;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
public class DetallePagoCreditoData implements ParsingHandler {

    private static final long serialVersionUID = 1L;
    private String estado;
    private String codigoMensaje;
    private String descripcionMensaje;

    private String tipoPago;
    private String numCredito;
    private String fecha;
    private String hora;
    private String importe;
    private String importeMoneda;
    private String tipoMoneda;
    private String cuentaRetiro;
    private String folio;
    private String nomBeneficiario;
    private String tipoCredito;
    private String reciboNumero;
    private String pagoRecibos;
    private String pagoRecibosMoneda;
    private String antDiferido;
    private String antDiferidoMoneda;
    public DetallePagoCreditoData() {
        estado = "";
        codigoMensaje = "";
        descripcionMensaje = "";
        tipoPago = "";
        numCredito = "";
        fecha = "";
        hora = "";
        importe = "";
        importeMoneda = "";
        tipoMoneda = "";
        cuentaRetiro = "";
        folio = "";
        nomBeneficiario = "";
        tipoCredito = "";
        reciboNumero = "";
        pagoRecibos = "";
        pagoRecibosMoneda = "";
        antDiferido = "";
        antDiferidoMoneda = "";
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public String getCodigoMensaje() {
        return codigoMensaje;
    }
    public void setCodigoMensaje(String codigoMensaje) {
        this.codigoMensaje = codigoMensaje;
    }
    public String getDescripcionMensaje() {
        return descripcionMensaje;
    }
    public void setDescripcionMensaje(String descripcionMensaje) {
        this.descripcionMensaje = descripcionMensaje;
    }
    public String getTipoPago() {
        return tipoPago;
    }
    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }
    public String getNumCredito() {
        return numCredito;
    }
    public void setNumCredito(String numCredito) {
        this.numCredito = numCredito;
    }
    public String getFecha() {
        return fecha;
    }
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    public String getHora() {
        return hora;
    }
    public void setHora(String hora) {
        this.hora = hora;
    }
    public String getImporte() {
        return importe;
    }
    public void setImporte(String importe) {
        this.importe = importe;
    }
    public String getImporteMoneda() {
        return importeMoneda;
    }
    public void setImporteMoneda(String importeMoneda) {
        this.importeMoneda = importeMoneda;
    }
    public String getTipoMoneda() {
        return tipoMoneda;
    }
    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }
    public String getCuentaRetiro() {
        return cuentaRetiro;
    }
    public void setCuentaRetiro(String cuentaRetiro) {
        this.cuentaRetiro = cuentaRetiro;
    }
    public String getFolio() {
        return folio;
    }
    public void setFolio(String folio) {
        this.folio = folio;
    }
    public String getNomBeneficiario() {
        return nomBeneficiario;
    }
    public void setNomBeneficiario(String nomBeneficiario) {
        this.nomBeneficiario = nomBeneficiario;
    }
    public String getTipoCredito() {
        return tipoCredito;
    }
    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }
    public String getReciboNumero() {
        return reciboNumero;
    }
    public void setReciboNumero(String reciboNumero) {
        this.reciboNumero = reciboNumero;
    }
    public String getPagoRecibos() {
        return pagoRecibos;
    }
    public void setPagoRecibos(String pagoRecibos) {
        this.pagoRecibos = pagoRecibos;
    }
    public String getPagoRecibosMoneda() {
        return pagoRecibosMoneda;
    }
    public void setPagoRecibosMoneda(String pagoRecibosMoneda) {
        this.pagoRecibosMoneda = pagoRecibosMoneda;
    }
    public String getAntDiferido() {
        return antDiferido;
    }
    public void setAntDiferido(String antDiferido) {
        this.antDiferido = antDiferido;
    }
    public String getAntDiferidoMoneda() {
        return antDiferidoMoneda;
    }
    public void setAntDiferidoMoneda(String antDiferidoMoneda) {
        this.antDiferidoMoneda = antDiferidoMoneda;
    }
    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        estado = parser.parseNextValue("estado");
        if (estado.equals("OK")) {
            tipoPago = parser.parseNextValue("tipoPago");
            numCredito = parser.parseNextValue("numCredito");
            fecha = parser.parseNextValue("fecha");
            hora = parser.parseNextValue("hora");
            importe = parser.parseNextValue("importe");
            importeMoneda = parser.parseNextValue("importeMoneda");
            tipoMoneda = parser.parseNextValue("tipoMoneda");
            cuentaRetiro = parser.parseNextValue("cuentaRetiro");
            folio = parser.parseNextValue("folio");
            nomBeneficiario = parser.parseNextValue("nomBeneficiario");
            tipoCredito = parser.parseNextValue("tipoCredito");
            reciboNumero = parser.parseNextValue("reciboNumero");
            pagoRecibos = parser.parseNextValue("pagoRecibos");
            pagoRecibosMoneda = parser.parseNextValue("pagoRecibosMoneda");
            antDiferido = parser.parseNextValue("antDiferido");
            antDiferidoMoneda = parser.parseNextValue("antDiferidoMoneda");
        } else if (estado.equals("ERROR")) {
            codigoMensaje = parser.parseNextValue("codigoMensaje");
            descripcionMensaje = parser.parseNextValue("descripcionMensaje");
        }
    }
    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub
    }
}