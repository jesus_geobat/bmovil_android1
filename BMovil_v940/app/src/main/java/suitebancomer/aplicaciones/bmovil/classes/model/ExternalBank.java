package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.Serializable;
/**
 * Created by elunag on 07/07/16.
 */
public class ExternalBank implements Serializable
{
    private String name;
    private String id;

    public ExternalBank(){

    }

    public ExternalBank(final String name,final String id){
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
