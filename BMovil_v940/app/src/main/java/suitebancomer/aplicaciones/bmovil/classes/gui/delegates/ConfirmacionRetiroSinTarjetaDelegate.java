package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRetiroSinTarjetaViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjetaViewDto;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtp;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtpBmovil;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;


public class ConfirmacionRetiroSinTarjetaDelegate extends DelegateBaseOperacion {
    public final static long CONFIRMACION_RETIRO_SIN_TARJETA_DELEGATE_DELEGATE_ID = 0x1ef4f4c81ca109bfL;

    private AltaRetiroSinTarjetaDelegate operationDelegate;
    private boolean debePedirContrasena;
    private boolean debePedirNip;
    private Constants.TipoOtpAutenticacion tokenAMostrar;
    private boolean debePedirCVV;


    private Constants.TipoInstrumento tipoInstrumentoSeguridad;
    private ConfirmacionRetiroSinTarjetaViewController confirmacionRetiroSinTarjetaViewController;

    private boolean debePedirTarjeta;
    //AMZ
    public boolean res = false;
   //ehmendezr validacion S2
    public ConfirmacionRetiroSinTarjetaDelegate(AltaRetiroSinTarjetaDelegate altaRetiroSinTarjetaDelegate) {
        this.operationDelegate = altaRetiroSinTarjetaDelegate;
        debePedirContrasena = operationDelegate.mostrarContrasenia();
        debePedirNip = operationDelegate.mostrarNIP();
        debePedirCVV = operationDelegate.mostrarCVV();
        tokenAMostrar = operationDelegate.tokenAMostrar();
        debePedirTarjeta = mostrarCampoTarjeta();
        String instrumento = Session.getInstance(SuiteApp.appContext).getSecurityInstrument();
        if (instrumento.equals(Constants.IS_TYPE_DP270)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
        } else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
        } else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)||instrumento.equals(Constants.TYPE_SOFTOKEN.S2.value)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
        } else {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
        }
    }

    public void setConfirmacionRetiroSinTarjetaViewController(ConfirmacionRetiroSinTarjetaViewController confirmacionRetiroSinTarjetaViewController) {
        this.confirmacionRetiroSinTarjetaViewController = confirmacionRetiroSinTarjetaViewController;
    }

    public void consultaDatos() {
        RetiroSinTarjetaViewDto retiroSinTarjetaViewDto = operationDelegate.getDatosConfirmacion();
        confirmacionRetiroSinTarjetaViewController.setImporte(retiroSinTarjetaViewDto.getImporte());
        confirmacionRetiroSinTarjetaViewController.setCuentaRetiro(retiroSinTarjetaViewDto.getCuentaRetiro());
        confirmacionRetiroSinTarjetaViewController.setNumeroCelular(retiroSinTarjetaViewDto.getNumeroCelular());
        confirmacionRetiroSinTarjetaViewController.setConcepto(retiroSinTarjetaViewDto.getConcepto());
        confirmacionRetiroSinTarjetaViewController.setNombre(retiroSinTarjetaViewDto.getNombre());
        confirmacionRetiroSinTarjetaViewController.setCompania(retiroSinTarjetaViewDto.getCompania());
    }

    public boolean consultaDebePedirContrasena() {
        return debePedirContrasena;
    }

    public boolean consultaDebePedirNIP() {
        return debePedirNip;
    }

    public boolean consultaDebePedirCVV() {
        return debePedirCVV;
    }

    public Constants.TipoInstrumento consultaTipoInstrumentoSeguridad() {
        return tipoInstrumentoSeguridad;
    }

    public Constants.TipoOtpAutenticacion consultaInstrumentoSeguridad() {
        return tokenAMostrar;
    }

    static String contrasena = null;
    static String nip = null;
    static String asm = null;
    static String cvv = null;
    static String newToken = null;
    static String tarjeta = null;
    public void enviaPeticionOperacion() {
        res = false;
        if (debePedirContrasena) {
            contrasena = confirmacionRetiroSinTarjetaViewController.pideContrasena();
            if (contrasena.equals("")) {
                String mensaje = confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_componenteContrasena);
                mensaje += ".";
                confirmacionRetiroSinTarjetaViewController.showInformationAlert(mensaje);
                return;
            } else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
                String mensaje = confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.PASSWORD_LENGTH;
                mensaje += " ";
                mensaje += confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_componenteContrasena);
                mensaje += ".";
                confirmacionRetiroSinTarjetaViewController.showInformationAlert(mensaje);
                return;
            }
        }

        if (debePedirTarjeta) {
            tarjeta = confirmacionRetiroSinTarjetaViewController.pideTarjeta();
            String mensaje = "";
            if (tarjeta.equals("")) {
                mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
                confirmacionRetiroSinTarjetaViewController.showInformationAlert(mensaje);
                return;
            } else if (tarjeta.length() != 5) {
                mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
                confirmacionRetiroSinTarjetaViewController.showInformationAlert(mensaje);
                return;
            }
        }

        if (debePedirNip) {
            nip = confirmacionRetiroSinTarjetaViewController.pideNIP();
            if (nip.equals("")) {
                String mensaje = confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_componenteNip);
                mensaje += ".";
                confirmacionRetiroSinTarjetaViewController.showInformationAlert(mensaje);
                return;
            } else if (nip.length() != Constants.NIP_LENGTH) {
                String mensaje = confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.NIP_LENGTH;
                mensaje += " ";
                mensaje += confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_componenteNip);
                mensaje += ".";
                confirmacionRetiroSinTarjetaViewController.showInformationAlert(mensaje);
                return;
            }
        }
        if (tokenAMostrar != Constants.TipoOtpAutenticacion.ninguno) {
            asm = confirmacionRetiroSinTarjetaViewController.pideASM();
            if (asm.equals("")) {
                String mensaje = confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                switch (tipoInstrumentoSeguridad) {
                    case OCRA:
                        mensaje += getEtiquetaCampoOCRA();
                        break;
                    case DP270:
                        mensaje += getEtiquetaCampoDP270();
                        break;
                    case SoftToken:
                        if (SuiteApp.getSofttokenStatus()) {
                            mensaje += getEtiquetaCampoSoftokenActivado();
                        } else {
                            mensaje += getEtiquetaCampoSoftokenDesactivado();
                        }
                        break;
                    default:
                        break;
                }
                mensaje += ".";
                confirmacionRetiroSinTarjetaViewController.showInformationAlert(mensaje);
                return;
            } else if (asm.length() != Constants.ASM_LENGTH) {
                String mensaje = confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.ASM_LENGTH;
                mensaje += " ";
                mensaje += confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                switch (tipoInstrumentoSeguridad) {
                    case OCRA:
                        mensaje += getEtiquetaCampoOCRA();
                        break;
                    case DP270:
                        mensaje += getEtiquetaCampoDP270();
                        break;
                    case SoftToken:
                        if (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
                            mensaje += getEtiquetaCampoSoftokenActivado();
                        } else {
                            mensaje += getEtiquetaCampoSoftokenDesactivado();
                        }
                        break;
                    default:
                        break;
                }
                mensaje += ".";
                confirmacionRetiroSinTarjetaViewController.showInformationAlert(mensaje);
                return;
            }
        }
        if (debePedirCVV) {
            cvv = confirmacionRetiroSinTarjetaViewController.pideCVV();
            if (cvv.equals("")) {
                String mensaje = confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_componenteCvv);
                mensaje += ".";
                confirmacionRetiroSinTarjetaViewController.showInformationAlert(mensaje);
                return;
            } else if (cvv.length() != Constants.CVV_LENGTH) {
                String mensaje = confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.CVV_LENGTH;
                mensaje += " ";
                mensaje += confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_componenteCvv);
                mensaje += ".";
                confirmacionRetiroSinTarjetaViewController.showInformationAlert(mensaje);
                return;
            }
        }

        if (tokenAMostrar != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken
                && (SuiteApp.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService())){
            if(SuiteAppCRApi.getSofttokenStatus()) {
                if(ServerCommons.ALLOW_LOG){
                    Log.d("APP", "softToken local");

                }
                newToken = loadOtpFromSofttoken(tokenAMostrar);

                finaliceOP(contrasena, nip, newToken, tarjeta, cvv);
            }
            else if(!SuiteAppCRApi.getSofttokenStatus() && PropertiesManager.getCurrent().getSofttokenService()) {
                if(ServerCommons.ALLOW_LOG){
                    Log.d("APP","softoken compartido");
                }
                GetOtpBmovil otp = new GetOtpBmovil(new GetOtp() {
                    /**
                     *
                     * @param otp
                     */
                    @Override
                    public void setOtpRegistro(String otp) {

                    }

                    /**
                     *
                     * @param otp
                     */
                    @Override
                    public void setOtpCodigo(String otp) {
                        if(ServerCommons.ALLOW_LOG) {
                            Log.d("APP", "la otp en callback: " + otp);
                        }
                        finaliceOP(contrasena, nip, otp, tarjeta, cvv);
                    }

                    /**
                     *
                     * @param otp
                     */
                    @Override
                    public void setOtpCodigoQR(String otp) {

                    }
                }, com.bancomer.base.SuiteApp.appContext);

                otp.generateOtpCodigo();
            }
        }
        else if(tokenAMostrar != TipoOtpAutenticacion.ninguno && (tipoInstrumentoSeguridad == TipoInstrumento.DP270 || tipoInstrumentoSeguridad == TipoInstrumento.OCRA)) {
            finaliceOP(contrasena, nip, asm, tarjeta, cvv);
        }
        else if(tokenAMostrar == TipoOtpAutenticacion.ninguno){
            finaliceOP(contrasena, nip, null, tarjeta, cvv);
        }
        res = true;

    }
     private void finaliceOP(String contrasena, String nip, String asm, String tarjeta, String cvv){
         operationDelegate.realizaOperacion(confirmacionRetiroSinTarjetaViewController, contrasena, nip, asm, tarjeta, cvv);
     }


    @Override
    public String getEtiquetaCampoNip() {
        return confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_nip);
    }

    @Override
    public String getTextoAyudaNIP() {
        return confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_ayudaNip);
    }

    @Override
    public String getEtiquetaCampoContrasenia() {
        return confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_contrasena);
    }

    @Override
    public String getEtiquetaCampoOCRA() {
        return confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_ocra);
    }

    @Override
    public String getEtiquetaCampoDP270() {
        return confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_dp270);
    }

    @Override
    public String getEtiquetaCampoSoftokenActivado() {
        return confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_softtokenActivado);
    }

    @Override
    public String getEtiquetaCampoSoftokenDesactivado() {
        return confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_softtokenDesactivado);
    }

    @Override
    public String getEtiquetaCampoCVV() {
        return confirmacionRetiroSinTarjetaViewController.getString(R.string.confirmation_CVV);
    }

    public String getTextoAyudaInstrumentoSeguridad(Constants.TipoInstrumento tipoInstrumento) {
        Constants.TipoOtpAutenticacion tokenAMostrar = tokenAMostrar();
        if (getContext() == null && confirmacionRetiroSinTarjetaViewController != null) {
            setContext(confirmacionRetiroSinTarjetaViewController);
        }
        return getTextoAyudaInstrumentoSeguridad(tipoInstrumento, SuiteApp.getSofttokenStatus(), tokenAMostrar);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        Session session = Session.getInstance(SuiteApp.appContext);
        Constants.Perfil perfil = session.getClientProfile();

        if (response.getStatus() == ServerResponse.OPERATION_ERROR && !Constants.Perfil.recortado.equals(perfil)) {
            confirmacionRetiroSinTarjetaViewController.limpiarCampos();
            confirmacionRetiroSinTarjetaViewController.getParentViewsController().getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
        }
        operationDelegate.analyzeResponse(operationId, response);
    }

    @Override
    public TipoOtpAutenticacion tokenAMostrar() {
        return tokenAMostrar;
    }

    @Override
    public boolean mostrarCampoTarjeta() {
        return operationDelegate.mostrarCampoTarjeta();
    }

    @Override
    public String loadOtpFromSofttoken(TipoOtpAutenticacion tipoOTP) {
        return loadOtpFromSofttoken(tipoOTP, operationDelegate);
    }

    public AltaRetiroSinTarjetaDelegate.TipoRetiro getTipoRetiro() {
        return operationDelegate.getTipoRetiro();
    }

}
