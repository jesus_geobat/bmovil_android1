package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.lang.reflect.Modifier;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaEFIDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ModificarImporteDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaEFI;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountField;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import tracking.TrackingHelper;

public class ModificarImporteEFIViewController extends BaseViewController implements OnClickListener {
	
	OfertaEFI ofertaEFI;
	
	public LinearLayout vista;
	public ListaSeleccionViewController listaSeleccion;
	ImageButton btncalcular;
	private TextView lblTextoImporteMaximo;
	public AmountField txtImporte;
	private static ModificarImporteDelegate modificarImporteDelegate;
	public ModificarImporteDelegate leyendamontosolicitado;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	private DetalleOfertaEFIDelegate detalleOfertaEFIDelegate;
	AmountField amountField;
	private DetalleEFIViewController detalleEFIViewController;
	
	Account accountSeleccionada; 
	


	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_modificar_importe_efi_view);
		setTitle(R.string.bmovil_promocion_title_efi,R.drawable.an_icono_efi);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("modifica importe efi", parentManager.estados);
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(modificarImporteDelegate.MODIFICAR_IMPORTE_DELEGATE_ID));
		modificarImporteDelegate = (ModificarImporteDelegate)getDelegate();
		modificarImporteDelegate.setViewController(this);
		findViews();
		scaleForCurrentScreen();
		lblTextoImporteMaximo = (TextView) findViewById(R.id.lblTextoImporteMaximo);
		
		btncalcular.setOnClickListener(this);
		init();
		montomax();
		//cuentadefault();	
		
		
	}
	

	
	public void montomax(){
		
		//montoDispFijo = montoDispFijo + Tools.formatAmount(modificarImporteDelegate.getOfertaEFI().getMontoDisponible()+"00",false);
		//String dispMax = "Importe máximo disponible\n"+Tools.formatAmount(modificarImporteDelegate.getOfertaEFI().getDisponibleTDC(),false); //Modificacion
			String dispMax = "Importe máximo disponible:\n"+Tools.formatAmount(modificarImporteDelegate.getOfertaEFI().getMontoDisponibleFijo()+"00",false);
		//String saldo=Tools.formatAmount(modificarImporteDelegate.getOfertaEFI().getDisponibleTDC(),false); //Modificacion
		String saldo=Tools.formatAmount(modificarImporteDelegate.getOfertaEFI().getMontoDisponibleFijo()+"00",false);
		
	  
		
		int startSaldo= dispMax.indexOf(saldo);

		
		SpannableString disponibleTDC = new SpannableString(dispMax);
	//	disponibleTDC.setSpan(new UnderlineSpan(), startSaldo,startSaldo+ saldo.length(), 0);
		disponibleTDC.setSpan(new ForegroundColorSpan(Color.rgb(0,158,229)),startSaldo,startSaldo+saldo.length(),0);
		disponibleTDC.setSpan(new RelativeSizeSpan(1.5f),startSaldo,startSaldo+saldo.length(),0);		
		
		lblTextoImporteMaximo.setText(disponibleTDC);
	
	}
	
	public void cuentadefault (){

		//index=modificarImporteDelegate.getIndexcuenta(index);
		
		
		listaSeleccion.setOpcionSeleccionada(modificarImporteDelegate.getIndexcuenta());
		
		//modificarImporteDelegate.setIndexcuenta(index);
				
	}
	
	public void init(){
		//modificarImporteDelegate.montoMostrar();
		txtImporte.setAmountEFI((modificarImporteDelegate.getOfertaEFI().getMontoDisponible()+"00"));
		cargaListaSeleccionComponent();
	
	}
	
	
	public void cargaListaSeleccionComponent(){
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT,	LayoutParams.WRAP_CONTENT);
		
		if (listaSeleccion == null) {
			listaSeleccion = new ListaSeleccionViewController(this, params,parentViewsController);
			listaSeleccion.setDelegate(modificarImporteDelegate);

			ArrayList<Object> listaEncabezado = modificarImporteDelegate.getDatosHeaderTablaFrecuentes();
			ArrayList<Object> listaDatos = modificarImporteDelegate.getDatosTablaFrecuentes();
			listaSeleccion.setEncabezado(listaEncabezado);
			listaSeleccion.setLista(listaDatos);
			listaSeleccion.setNumeroColumnas(listaEncabezado.size() - 1) ;
			if (listaDatos.size() == 0) {
				listaSeleccion.setTextoAMostrar(getString(R.string.bmovil_consultar_frecuentes_emptylist));
			}else {
				listaSeleccion.setTextoAMostrar(null);
				listaSeleccion.setNumeroFilas(listaDatos.size());		
			}	
			listaSeleccion.setOpcionSeleccionada(modificarImporteDelegate.getIndexcuenta());
			listaSeleccion.setSeleccionable(true);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(listaDatos.size());
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.setTitle("Cuenta de depósito");
			listaSeleccion.setSingleLine(true);
			listaSeleccion.cargarTabla();
			vista.addView(listaSeleccion);
			
			
			
			}
			
	}
	
	@Override
	public void onClick(View v) {
		if (v == btncalcular) {			
				if(	modificarImporteDelegate.guardarDatos(txtImporte.getAmount())){	
					if(operarFrecuente()){
						//ARR
						Map<String,Object> paso3OperacionMap = new HashMap<String, Object>();
						
						//ARR
						paso3OperacionMap.put("evento_paso3", "event48");
						paso3OperacionMap.put("&&products", "promociones;oferta efi+modificar efi");
						paso3OperacionMap.put("eVar12", "paso3: calcular importe efi");

						TrackingHelper.trackPaso2Operacion(paso3OperacionMap);
						modificarImporteDelegate.realizaOperacion(Server.SIMULADOR_EFI, this);
						
											
				}
					
					
			}  else {
					txtImporte.reset();
					txtImporte.setAmountEFI((modificarImporteDelegate.getOfertaEFI().getMontoDisponible()+"00"));
					
			}
				
			
		}
	}
		
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		if (modificarImporteDelegate != null) {
			modificarImporteDelegate.setViewController(this);

		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}
	
	public boolean operarFrecuente() {
		if (listaSeleccion.getOpcionSeleccionada() != -1) {
			modificarImporteDelegate.operarFrecuente(listaSeleccion.getOpcionSeleccionada());
			return true;
		} else {
			showInformationAlert(R.string.bmovil_alert_selecione_cuenta);
			return false;
		}
	}

	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		modificarImporteDelegate.analyzeResponse(operationId, response);
	}
	
	private void findViews() {
		vista = (LinearLayout)findViewById(R.id.modificar_importe_efi_view_controller_layout);
		txtImporte= (AmountField)findViewById(R.id.modificar_importe_txt);
		btncalcular = (ImageButton) findViewById(R.id.modificar_importe_efi_btn_calcular);
	}
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(findViewById(R.id.txt_modificar_importe),true);
		guiTools.scale(findViewById(R.id.modificar_importe_txt),true);
		guiTools.scale(findViewById(R.id.lblTextoImporteMaximo));
		guiTools.scale(findViewById(R.id.layout_inicio_modificar_importe));
		guiTools.scale(findViewById(R.id.modificar_importe_efi_view_controller_layout));
		guiTools.scale(findViewById(R.id.modificar_importe_efi_rootLayout));
		guiTools.scale(findViewById(R.id.modificar_importe_efi_btn_calcular));
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(MotionEvent.ACTION_DOWN == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_DOWN");
			listaSeleccion.setMarqueeEnabled(false);
		}else if(MotionEvent.ACTION_UP == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_UP");
			listaSeleccion.setMarqueeEnabled(true);
		}

		return super.dispatchTouchEvent(ev);
	}

}
