package suitebancomer.aplicaciones.bmovil.classes.model;

/**
 * Created by smontesdeoca on 24/06/2016.
 */
public class CambiarNominaData {

    private String tipoCuentaOrigen;
    private String institucionBancaria;
    private String bancoActual;
    private String cuentaActual;
    private Account cuentaDestino;
    private boolean fechaNacimiento;
    private String FechaNacim;
    private String folio;
    private String createdDate;
    private String operationTime;


    public CambiarNominaData() {

    }

    public CambiarNominaData(String tipoCuentaOrigen, String bancoActual, String cuentaActual, Account cuentaDestino, String institucionBancaria, boolean fechaNacimiento,String folio, String createdDate, String operationTime) {
        this.tipoCuentaOrigen = tipoCuentaOrigen;
        this.bancoActual = bancoActual;
        this.cuentaActual = cuentaActual;
        this.cuentaDestino = cuentaDestino;
        this.institucionBancaria=institucionBancaria;
        this.fechaNacimiento=fechaNacimiento;
        this.folio = folio;
        this.createdDate = createdDate;
        this.operationTime = operationTime;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(String operationTime) {
        this.operationTime = operationTime;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getTipoCuentaOrigen() {
        return tipoCuentaOrigen;
    }

    public void setTipoCuentaOrigen(String tipoCuentaOrigen) {
        this.tipoCuentaOrigen = tipoCuentaOrigen;
    }

    public String getBancoActual() {
        return bancoActual;
    }

    public void setBancoActual(String bancoActual) {
        this.bancoActual = bancoActual;
    }

    public String getCuentaActual() {
        return cuentaActual;
    }

    public void setCuentaActual(String cuentaActual) {
        this.cuentaActual = cuentaActual;
    }

    public Account getCuentaDestino() {
        return cuentaDestino;
    }

    public void setCuentaDestino(Account cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    public String getInstitucionBancaria() {
        return institucionBancaria;
    }

    public void setInstitucionBancaria(String institucionBancaria) {
        this.institucionBancaria = institucionBancaria;
    }

    public boolean isFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(boolean fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getFechaNacim() {
        return FechaNacim;
    }

    public void setFechaNacim(String fechaNacim) {
        FechaNacim = fechaNacim;
    }
}
