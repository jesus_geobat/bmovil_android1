package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuAdministrarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuPrincipalDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.BanqueroPersonal;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
/**
 * Created by eluna on 02/06/2016.
 */
public class ConsultaGestorRemotoViewController extends BaseViewController implements View.OnClickListener
{
    private BmovilViewsController parentManager;
    private TextView txtNombreGestor;
    private TextView txtCorreoGestor;
    private ImageView btntoggle_1;
    private ImageView btnatras;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState, 0, R.layout.layout_bmovil_gestor_remoto);
        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        SuiteApp suiteApp = (SuiteApp)getApplication();
        setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
        setDelegate(parentViewsController.getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID));
        if(BaqueroAbanicoViewController.pass){
            ImageView animationTarget = (ImageView) this.findViewById(R.id.btntoggle_1);
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_180);
            animationTarget.startAnimation(animation);
            BaqueroAbanicoViewController.pass= false;
        }
        initValues();
        configurarPantalla();
        txtNombreGestor.setText(BanqueroPersonal.getInstance().getNombreGestor());
        txtCorreoGestor.setText(BanqueroPersonal.getInstance().getEmail().toLowerCase());
    }
    private void initValues()
    {
        txtNombreGestor = (TextView) findViewById(R.id.txtNombreGestor);
        txtCorreoGestor = (TextView) findViewById(R.id.txtCorreoGestor);
        btntoggle_1 = (ImageView) findViewById(R.id.btntoggle_1);
        btntoggle_1.setOnClickListener(this);
        btnatras = (ImageView) findViewById(R.id.btnatras);
        btnatras.setOnClickListener(this);

    }
    private void configurarPantalla()
    {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(findViewById(R.id.layout_base_gestor_remoto), false);
        gTools.scale(findViewById(R.id.layout_title_1), false);
        gTools.scale(findViewById(R.id.layout_title), false);
        gTools.scale(findViewById(R.id.layoutScroll), false);
        gTools.scale(findViewById(R.id.layout_title_gestor_remoto), false);
        gTools.scale(findViewById(R.id.layout_title_2), false);
        gTools.scale(findViewById(R.id.layout_title_3), false);

        gTools.scale(findViewById(R.id.layout_title_gestor_remoto_1), false);
        gTools.scale(findViewById(R.id.layout_title_21), false);
        gTools.scale(findViewById(R.id.layout_title_32), false);
        gTools.scale(findViewById(R.id.layout_title_gestor_remoto_3), false);
    }

    @Override
    public void onClick(View v)
    {
        if (v.equals(btntoggle_1)){
            ImageView animationTarget = (ImageView) this.findViewById(R.id.btntoggle_1);
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around);
            animationTarget.startAnimation(animation);
            parentManager.showBanqueroAbanico();
        }

        if (v.equals(btnatras)){
            parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
            parentManager.showMenuPrincipal();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }
    @Override
    public void goBack() {
        parentManager.showMenuPrincipal();
        //parentViewsController.removeDelegateFromHashMap(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
        //super.goBack();
    }
}
