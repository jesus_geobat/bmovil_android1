package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class NombreCIEData implements ParsingHandler {

	private String nombreCIE;
	
	public String getNombreCIE() {
		return nombreCIE;
	}
	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		nombreCIE = parser.parseNextValue("DE");
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
