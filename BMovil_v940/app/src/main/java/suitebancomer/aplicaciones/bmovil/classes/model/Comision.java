package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class Comision implements ParsingHandler {

	String comision;
	
	/**
	 * @return the comision
	 */
	public String getComision() {
		return comision;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		this.comision = parser.parseNextValue("CD");
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
