package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;
import java.util.Hashtable;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.StartBmovilInBack;
import com.bancomer.mbanking.SuiteApp;

import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.DetalleConsumoViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.CambioTelefono;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;

public class DetalleOfertaConsumoDelegate extends BaseDelegate{
	OfertaConsumo ofertaConsumo;
	Promociones promocion;
	String fechaCatVisual;

	
	public static final long DETALLE_OFERTA_CONSUMO_DELEGATE = 631451334266765688L;
	private BaseViewController controladorDetalleILCView;


	public DetalleOfertaConsumoDelegate(OfertaConsumo ofertaConsumo, Promociones promocion){
		this.ofertaConsumo= ofertaConsumo;
		this.promocion= promocion;

	}	
	
	public OfertaConsumo getOfertaConsumo() {
		return ofertaConsumo;
	}



	public void setOfertaConsumo(OfertaConsumo ofertaConsumo) {
		this.ofertaConsumo = ofertaConsumo;
	}



	public Promociones getPromocion() {
		return promocion;
	}

	public void setPromocion(Promociones promocion) {
		this.promocion = promocion;
	}
	
	
	
	public String getfechaCatVisual() {
		return fechaCatVisual;
	}

	public void setfechaCatVisualt(String fechaCatVisual) {
		this.fechaCatVisual = fechaCatVisual;
	}

	public void realizaOperacion(int idOperacion,
			BaseViewController baseViewController) {			
			if(idOperacion==Server.RECHAZO_OFERTA_CONSUMO) {
			Session session = Session.getInstance(SuiteApp.appContext);
			String ium= session.getIum();
			String numCel= session.getUsername();
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "noAceptacionBMovil");
			params.put("numeroCelular",numCel);
			params.put("cveCamp", promocion.getCveCamp());
			params.put("descripcionMensaje", "0002");
			params.put("folioUG",ofertaConsumo.getFolioUG()); // definir de donde se obtendra
			params.put("IUM",ium);	
			//JAIG SI    	isjsonvalueCode=isJsonValueCode.ONECLICK;
			doNetworkOperation(Server.RECHAZO_OFERTA_CONSUMO, params,true,null,isJsonValueCode.ONECLICK,
					baseViewController);
		}
	
	}

	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if( controladorDetalleILCView != null)
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
	}

	public void analyzeResponse(int operationId, ServerResponse response) {
		if(operationId== Server.RECHAZO_OFERTA_CONSUMO){
			if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session

				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
				ActivityCompat.finishAffinity(controladorDetalleILCView);

				Intent i=new Intent(controladorDetalleILCView, StartBmovilInBack.class);
				i.putExtra(bancomer.api.common.commons.Constants.RESULT_STRING, Constants.TRUE_STRING);
				i.putExtra( bancomer.api.common.commons.Constants.TIMEOUT_STRING, Constants.FALSE_STRING);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				controladorDetalleILCView.startActivity(i);
				controladorDetalleILCView.finish();
			}else {
				((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showMenuPrincipal();
			}
		} 	else if(operationId==Server.CONSULTA_DETALLE_OFERTA_CONSUMO){

			ofertaConsumo = (OfertaConsumo) response.getResponse();
			((DetalleConsumoViewController)controladorDetalleILCView).setValues();

		}
	}
	
	public BaseViewController getControladorDetalleILCView() {
		return controladorDetalleILCView;
	}
	public void setControladorDetalleILCView(
			BaseViewController controladorDetalleILCView) {
		this.controladorDetalleILCView = controladorDetalleILCView;
	}
	
	public void confirmarRechazoatras(){

		controladorDetalleILCView.showYesNoAlert("¿Quieres abandonar el proceso de contratación de tu crédito?",
				new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						if (DialogInterface.BUTTON_POSITIVE == which) {
							realizaOperacion(Server.RECHAZO_OFERTA_CONSUMO,controladorDetalleILCView);
						}else{
							dialog.dismiss();
						}

					}
				});

			}
	
	public void formatoFechaCatMostrar(){
		try{	
			String[] meses = { "enero", "febrero", "marzo", "abril", "mayo", 
			      "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };		
			String fechaCatM= ofertaConsumo.getFechaCat();
			int mesS=Integer.parseInt(fechaCatM.substring(5,7));
			String dia= fechaCatM.substring(fechaCatM.length()-2);
			String anio= fechaCatM.substring(0,4);
			
			fechaCatVisual= dia+" de "+meses[mesS-1].toString()+ " de "+anio;
		    
				}catch(Exception ex){
					if(Server.ALLOW_LOG) Log.e("","error formato"+ex);
			}
		}

	public void realizaOperacionCONSUMO(int idOperacion,BaseViewController baseViewController) {
		Session session = Session.getInstance(SuiteApp.appContext);
		String user = session.getUsername();
		String ium= session.getIum();
		Hashtable<String, String> params = new Hashtable<String, String>();
		// params.put("operacion", "detalleConsumoBMovil");
		params.put("numeroCelular",user );
		params.put("claveCamp", promocion.getCveCamp());
		params.put("IUM", ium);

		if(!session.getImporteParcial().equals("")){
			params.put("importePar", session.getImporteParcial());
		} else {
			params.put("importePar","000");
		}
		//JAIG
		doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA_CONSUMO, params, true, new OfertaConsumo(), Server.isJsonValueCode.CONSUMO,
				baseViewController);
	}


}
