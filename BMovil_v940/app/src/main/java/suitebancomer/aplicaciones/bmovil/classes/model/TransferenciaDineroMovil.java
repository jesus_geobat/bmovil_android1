package suitebancomer.aplicaciones.bmovil.classes.model;

import java.util.HashMap;

/**
 * Modelo para tranferencias de dinero m�vil.
 */
public class TransferenciaDineroMovil extends FrecuenteMulticanalData{
	/**
	 * Cuenta de origen de la transferenc�a.
	 */
	private Account cuentaOrigen;
	
	/**
	 * Datos de la compa�ia.
	 */
	private HashMap<String, String> compania;
	
	/**
	 * Numero de celular del beneficiario.
	 */
	private String celularbeneficiario;
	
	/**
	 * Nombre del beneficiario.
	 */
	private String beneficiario;
	
	/**
	 * Importe de la transacci�n.
	 */
	private String importe;
	
	/**
	 * Concepto de la transacci�n.
	 */
	private String concepto;
	
	/**
	 * Sobrenombre del frecuente.
	 */
	private String aliasFrecuente;
	
	/**
	 * Tipo de operación.
	 */
	private String tipoOperacion;
	
	/**
	 * Frecuente multicanal.
	 */
	private String frecuenteMulticanal;
	
	/**
	 * El nombre de la compa�ia cargada desde un frecuente.
	 */
	private String nombreCompania;

	/**
	 * @return La cuenta de origen de la transacci�n.
	 */
	public Account getCuentaOrigen() {
		return cuentaOrigen;
	}

	/**
	 * @param cuentaOrigen La cuenta de origen a establecer.
	 */
	public void setCuentaOrigen(Account cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	/**
	 * @return Los datos de la compa�ia.
	 */
	public HashMap<String, String> getCompania() {
		return compania;
	}

	/**
	 * @param compania Los datos de la compa�ia a establecer.
	 */
	public void setCompania(HashMap<String, String> compania) {
		this.compania = compania;
	}

	/**
	 * @return El número de celular del beneficiario.
	 */
	public String getCelularbeneficiario() {
		return celularbeneficiario;
	}

	/**
	 * @param celularbeneficiario El número de celular del beneficiario a establecer.
	 */
	public void setCelularbeneficiario(String celularbeneficiario) {
		this.celularbeneficiario = celularbeneficiario;
	}

	/**
	 * @return El nombre del beneficiario.
	 */
	public String getBeneficiario() {
		return beneficiario;
	}

	/**
	 * @param beneficiario El nombre del beneficiario a establecer.
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	/**
	 * @return El importe de la transacci�n.
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * @param importe El importe de la transacci�n a establecer.
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}

	/**
	 * @return El concepto de la transacci�n.
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto El concepto de la transacci�n a establecer.
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return El sobrenombre del frecuente.
	 */
	public String getAliasFrecuente() {
		return aliasFrecuente;
	}

	/**
	 * @param aliasFrecuente El sobrenombre del frecuente a establecer.
	 */
	public void setAliasFrecuente(String aliasFrecuente) {
		this.aliasFrecuente = aliasFrecuente;
	}

	/**
	 * @return El tipo de operación.
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * @param tipoOperacion El tipo de operación a establecer.
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	/**
	 * @return El frecuente multicanal.
	 */
	public String getFrecuenteMulticanal() {
		return frecuenteMulticanal;
	}

	/**
	 * @param frecuenteMulticanal El frecuente multicanal a establecer.
	 */
	public void setFrecuenteMulticanal(String frecuenteMulticanal) {
		this.frecuenteMulticanal = frecuenteMulticanal;
	}
	
	/**
	 * @return El nombre de la compa�ia.
	 */
	public String getNombreCompania() {
		return nombreCompania;
	}
	

	/**
	 * @param nombreCompania El nombre de la compa�ia a establecer.
	 */
	public void setNombreCompania(String nombreCompania) {
		this.nombreCompania = nombreCompania;
	}

	/**
	 * Constructor por defecto.
	 */
	public TransferenciaDineroMovil() {
		cuentaOrigen = null;
		compania = null;
		celularbeneficiario = "";
		beneficiario = "";  
		importe = "";       
		concepto = "";      
		aliasFrecuente = "";
		tipoOperacion = "";
		frecuenteMulticanal = "";
	}

	/**
	 * Contructor completo.
	 * @param cuentaOrigen Cuenta de origen de la transferenc�a.
	 * @param compania Datos de la compa�ia.
	 * @param celularbeneficiario Numero de celular del beneficiario.
	 * @param beneficiario Nombre del beneficiario.
	 * @param importe Importe de la transacci�n.
	 * @param concepto Concepto de la transacci�n.
	 * @param aliasFrecuente Sobrenombre del frecuente.
	 * @param tipoOperacion Tipo de operación.
	 * @param frecuenteMulticanal El frecuente multicanal.
	 */
	public TransferenciaDineroMovil(Account cuentaOrigen,
									HashMap<String, String> compania, 
									String celularbeneficiario,
									String beneficiario, 
									String importe, 
									String concepto,
									String aliasFrecuente, 
									String tipoOperacion,
									String frecuenteMulticanal) {
		super();
		this.cuentaOrigen = cuentaOrigen;
		this.compania = compania;
		this.celularbeneficiario = celularbeneficiario;
		this.beneficiario = beneficiario;
		this.importe = importe;
		this.concepto = concepto;
		this.aliasFrecuente = aliasFrecuente;
		this.tipoOperacion = tipoOperacion;
		this.frecuenteMulticanal = frecuenteMulticanal;
	}
}
