package suitebancomer.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import org.apache.http.protocol.HTTP;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.ContratoPatrimonial;
import suitebancomer.aplicaciones.bmovil.classes.model.ContratosPatrimonial;
import suitebancomer.aplicaciones.bmovil.classes.model.DetallePosicionPatrimonial;
import suitebancomer.aplicaciones.commservice.commons.MethodType;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by OOROZCO on 3/13/16.
 */
public class ContactoPatrimonialLinearLayout extends LinearLayout {


    private BaseViewController controller;
    private Session session;
    private boolean isBtnRequest = false;
    private String numCartera = "";


    public boolean isBtnRequest() {
        return isBtnRequest;
    }

    public void setNumCartera(String numCuenta) {
        this.numCartera = numCuenta;
    }

    public ContactoPatrimonialLinearLayout(Context context, AttributeSet attrs)
    {
        super(context,attrs);
        session = Session.getInstance(controller);

        setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View v) {
                isBtnRequest = true;

                if(session.getListaContratos().isEmpty())
                    doRequest("","");
                else
                {/*
                    if(numCuenta.equals(""))
                    {
                        ContratoPatrimonial contrato = session.getListaContratos().get(0);
                        doRequest(controller.getString(R.string.bmovil_patrimonial_tipo_consulta),contrato.getNumCuenta());
                    }
                    else
                    {
                        for(ContratoPatrimonial contrato : session.getListaContratos())
                        {
                            if(contrato.getNumCuenta().substring(contrato.getNumCuenta().length()-numCuenta.length()).equals(numCuenta))
                            {
                                doRequest(controller.getString(R.string.bmovil_patrimonial_tipo_consulta),contrato.getNumCuenta());
                                break;
                            }
                        }
                    }
                    */


                    if(!numCartera.equals("")){

                        for(ContratoPatrimonial contrato : session.getListaContratos())
                        {
                            if(contrato.getContratoB().equals(numCartera))
                            {
                                ((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showContactoPatrimonial(contrato.getContrato());
                                numCartera="";
                                System.out.println("Lista Llena");
                                break;
                            }
                        }

                    }
                    else {
                        ContratoPatrimonial contrato = session.getListaContratos().get(0);
                        // doRequest(controller.getString(R.string.bmovil_patrimonial_tipo_consulta),contrato.getNumCuenta());
                        ((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showContactoPatrimonial(contrato.getContrato());

                        System.out.println("Lista Llena");

                    }
                }
            }
        });

    }



    public void setValues(BaseViewController controller)
    {
        this.controller = controller;
    }


    public void analyzeResponse(ServerResponse response)
    {

        System.out.println("ServerResponce Contacto_Pt" + response.getResponsePlain() + " ServerMesage :  " +  response.getMessageText()    );
        
        session.setListaContratos(((ContratosPatrimonial)response.getResponse()).getListaContratos());
        ContratoPatrimonial contrato = session.getListaContratos().get(0);
       // doRequest(controller.getString(R.string.bmovil_patrimonial_tipo_consulta),contrato.getNumCuenta());
        ((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showContactoPatrimonial(contrato.getContrato());

    /*    if(session.getListaContratos().isEmpty())
        {

        }
        else
        {
            ((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showContactoPatrimonial((DetallePosicionPatrimonial) response.getResponse());
            isBtnRequest = false;
            numCuenta = "";
        }

        */


    }


    private void doRequest(String tipoConsulta, String numCuenta)
    {



    //    Hashtable<String, String> params = new Hashtable<String, String>();

     /*   Hashtable<String, String> params = new Hashtable<String, String>();
>>>>>>> BMovil_Desarrollo_Patrimmonial
        params.put(ServerConstants.OPERACION, controller.getString(R.string.bmovil_patrimonial_operacion));
        params.put(ServerConstants.TELEFONO_TAG, session.getUsername());
        params.put(ServerConstants.IUM_TAG, session.getIum());
        int length = numCuenta.length();
        if(!numCuenta.equals(""))
            params.put(ServerConstants.NUMCUENTA_TAG, numCuenta.substring(length-10,length));
        else
            params.put(ServerConstants.NUMCUENTA_TAG, numCuenta);
        params.put(ServerConstants.TIPOCONSULTA_TAG, tipoConsulta);

<<<<<<< HEAD
        //  if(session.getListaContratos().isEmpty())

        ParametersTO parametersTO = new ParametersTO();
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put(HTTP.CONTENT_TYPE, "application/json");
=======
        */

        //  if(session.getListaContratos().isEmpty())



        ParametersTO parametersTO = new ParametersTO();
        final Map<String, String> headers = new HashMap<String, String>();
        //headers.put(HTTP.CONTENT_TYPE, "application/json");
        headers.put("ium",Session.getInstance(getContext()).getIum().replace('"',' ').trim());
        parametersTO.setHeaders(headers);
        parametersTO.setParameters(new Hashtable<String, String>());
        parametersTO.setMethodType(MethodType.GET);



     //   ((BmovilViewsController)controller.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.OP_PATRIMONIAL, params,true, new ContratosPatrimonial(), Server.isJsonValueCode.NONE, controller,false);

        ((BmovilViewsController)controller.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.OP_PATRIMONIAL, parametersTO,true, new ContratosPatrimonial(), Server.isJsonValueCode.NONE, controller,false);
       /* else
            ((BmovilViewsController)controller.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.OP_PATRIMONIAL, params,true, new DetallePosicionPatrimonial(), Server.isJsonValueCode.NONE, controller,false);
*/

    }


}
