package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.FrecuentesDineroMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountPlaceholderText;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import tracking.TrackingHelper;

public class FrecuentesDineroMovilViewController extends BaseViewController {

    private FrecuentesDineroMovilDelegate delegate;
    public BmovilViewsController parentManager;
    private Session session;
    public ListaSeleccionViewController listaSeleccion;
    private EditText txtFiltro;
    private LinearLayout listViewFrecuentes;
    private View footerSinFrecuentes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER, R.layout.activity_frecuentes_dinero_movil);

        parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("retiro sin tarjeta", parentManager.estados);

        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
        delegate = (FrecuentesDineroMovilDelegate) parentViewsController.getBaseDelegateForKey(FrecuentesDineroMovilDelegate.FRECUENTES_DINERO_MOVIL_DELEGATE_ID);
        if (delegate == null) {
            delegate = new FrecuentesDineroMovilDelegate();
            parentViewsController.addDelegateToHashMap(FrecuentesDineroMovilDelegate.FRECUENTES_DINERO_MOVIL_DELEGATE_ID, delegate);
        }
        delegate.setFrecuentesDineroMovilViewController(this);

        init();
    }

    public void init() {
        session = Session.getInstance(SuiteApp.appContext);

        findViews();

        delegate.consultarFrecuentes();

        txtFiltro.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                onUserInteraction();
                muestraFrecuentes(txtFiltro.getText().toString());
            }
        });
    }

    private void findViews() {
        txtFiltro = (EditText) findViewById(R.id.txtFiltro);
        listViewFrecuentes = (LinearLayout) findViewById(R.id.lista_frecuentes);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
        txtFiltro.setText("");
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

    public void muestraFrecuentes(String filtro) {
        //Payment[] frecuentesFiltrados = delegate.frecuentesFiltrados(filtro);
        //final FrecuentesDineroMovilAdapter adapter = new FrecuentesDineroMovilAdapter(this,
        //        R.layout.frecuentes_dinero_movil_item_row, frecuentesFiltrados);

        ArrayList<Object> lista  = delegate.getDatosTablaFrecuentes(filtro);

        GuiTools guiTools = GuiTools.getCurrent();

        LinearLayout.LayoutParams params;
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,	LinearLayout.LayoutParams.MATCH_PARENT);

        if (listaSeleccion == null) {
            listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
            listaSeleccion.setDelegate(delegate);

            ArrayList<Object> listaEncabezado = delegate.getDatosHeaderTablaFrecuentes();
            listaSeleccion.setEncabezado(listaEncabezado);
            listaSeleccion.setLista(lista);
            listaSeleccion.setNumeroColumnas(2) ;
            if (lista.size() == 0) {
                listaSeleccion.setTextoAMostrar(getString(R.string.bmovil_consultar_frecuentes_emptylist));
            }else {
                listaSeleccion.setTextoAMostrar(null);
                listaSeleccion.setNumeroFilas(lista.size());
            }
            listaSeleccion.setOpcionSeleccionada(-1);
            listaSeleccion.setSeleccionable(false);
            listaSeleccion.setAlturaFija(true);
            listaSeleccion.setNumeroFilas(7);
            listaSeleccion.setNumeroColumnas(2);
            listaSeleccion.setExisteFiltro(false);
            listaSeleccion.setSingleLine(true);
            listaSeleccion.cargarTabla();
            listViewFrecuentes.addView(listaSeleccion);
        }else {
            if (lista.size() == 0) {
                listaSeleccion.setTextoAMostrar(getString(R.string.bmovil_consultar_frecuentes_emptylist));
            }else {
                listaSeleccion.setTextoAMostrar(null);
                listaSeleccion.setNumeroFilas(lista.size());
            }
            listaSeleccion.setSeleccionable(false);
            listaSeleccion.setAlturaFija(true);
            listaSeleccion.setLista(lista);
            listaSeleccion.setNumeroColumnas(2);
            listaSeleccion.setNumeroFilas(7);
            listaSeleccion.setSingleLine(true);
            listaSeleccion.setOpcionSeleccionada(-1);
            listaSeleccion.cargarTabla();
        }

        /*if (listViewFrecuentes.getHeaderViewsCount() == 0) {
            View header = getLayoutInflater().inflate(R.layout.frecuentes_dinero_movil_header_row, null);
            listViewFrecuentes.addHeaderView(header);
        }

        if (frecuentesFiltrados.length == 0 && listViewFrecuentes.getFooterViewsCount() == 0) {
            footerSinFrecuentes = getLayoutInflater().inflate(R.layout.frecuentes_dinero_movil_footer_row, null);
            listViewFrecuentes.addFooterView(footerSinFrecuentes);
        } else if (frecuentesFiltrados.length != 0 && listViewFrecuentes.getFooterViewsCount() > 0) {
            listViewFrecuentes.removeFooterView(footerSinFrecuentes);
        }

        listViewFrecuentes.setAdapter(adapter);
        listViewFrecuentes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (delegate.getFrecuentes()==null  ||  delegate.getFrecuentes()!=null && delegate.getFrecuentes().length==0) {
                    return;
                }
                TransferenciaDineroMovil transferenciaDineroMovil = new TransferenciaDineroMovil();
                Payment selectedPayment = adapter.getItem(position - 1);

                transferenciaDineroMovil.setCelularbeneficiario(selectedPayment.getBeneficiaryAccount());
                transferenciaDineroMovil.setNombreCompania(selectedPayment.getOperadora());
                transferenciaDineroMovil.setBeneficiario(selectedPayment.getBeneficiary());
                transferenciaDineroMovil.setConcepto(selectedPayment.getConcept());
                //multicanal
                transferenciaDineroMovil.setFrecuenteMulticanal(selectedPayment.getIdToken());
                transferenciaDineroMovil.setIdCanal(selectedPayment.getIdCanal());
                transferenciaDineroMovil.setUsuario(selectedPayment.getUsuario());
                transferenciaDineroMovil.setAliasFrecuente(selectedPayment.getNickname());
                transferenciaDineroMovil.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());

                ((BmovilViewsController) parentViewsController).showRetiroSinTarjetaViewController(transferenciaDineroMovil, AltaRetiroSinTarjetaDelegate.TipoRetiro.DINERO_MOVIL_FRECUENTE);
            }
        });*/
    }

    public void registrarNuevo(View view){

        if(("T3".equalsIgnoreCase(Session.getInstance(SuiteApp.appContext).getSecurityInstrument()))) {
            setHabilitado(true);
            hideCurrentDialog();
            Log.i("Muestra Alert", "Muestra alert");
            FragmentManager fragmentManager = getSupportFragmentManager();
            setActivityChanging(true);
            getParentViewsController().setCurrentActivityApp(this);
            PopUpActivarTokenMovilController alerts_dialogs = new PopUpActivarTokenMovilController();
            alerts_dialogs.setCaller(this);
            alerts_dialogs.show(fragmentManager, null);
        }else{
            ((BmovilViewsController)parentViewsController).showTransferirDineroMovil(null, false);
        }
    }

}
