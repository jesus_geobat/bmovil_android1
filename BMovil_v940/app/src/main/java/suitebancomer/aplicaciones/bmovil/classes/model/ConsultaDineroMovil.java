package suitebancomer.aplicaciones.bmovil.classes.model;


public class ConsultaDineroMovil {
	/**
	 * Codigo de seguridad OTP.
	 */
	private String codigoSeguridad;
	
	/**
	 * Cuenta de origen.
	 */
	private Account cuentaOrigen;
	
	/**
	 * Celular del beneficiario.
	 */
	private String celularBeneficiario;
	
	/**
	 * Compa�ia de celular del beneficiario.
	 */
	private String companiaBeneficiario;
	
	/**
	 * Nombre del beneficiario.
	 */
	private String beneficiario;
	
	/**
	 * Importe de la transferencia.
	 */
	private String importe;
	
	/**
	 * Concepto de la tranaferencia.
	 */
	private String concepto;
	
	/**
	 * Fecha de alta del movimiento.
	 */
	private String fechaAlta;
	
	/**
	 * Fecha de vigencia del movimiento.
	 */
	private String fechaVigencia;
	
	/**
	 * Folio de la operación.
	 */
	private String folioOperacion;
	
	/**
	 * Estatus de la operación.
	 */
	private String estatus;

	/**
	 * Codigo de canal de movimiento.
	 */
	private String codigoCanal;
	
	/**
	 * Hora de expiracion del movimiento.
	 */
	private String horaExpiracion;
	
	/**
	 * @return the codigoSeguridad
	 */
	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	/**
	 * @param codigoSeguridad the codigoSeguridad to set
	 */
	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	/**
	 * @return the cuentaOrigen
	 */
	public Account getCuentaOrigen() {
		return cuentaOrigen;
	}

	/**
	 * @param cuentaOrigen the cuentaOrigen to set
	 */
	public void setCuentaOrigen(Account cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	/**
	 * @return the celularBeneficiario
	 */
	public String getCelularBeneficiario() {
		return celularBeneficiario;
	}

	/**
	 * @param celularBeneficiario the celularBeneficiario to set
	 */
	public void setCelularBeneficiario(String celularBeneficiario) {
		this.celularBeneficiario = celularBeneficiario;
	}

	/**
	 * @return the companiaBeneficiario
	 */
	public String getCompaniaBeneficiario() {
		return companiaBeneficiario;
	}

	/**
	 * @param companiaBeneficiario the companiaBeneficiario to set
	 */
	public void setCompaniaBeneficiario(String companiaBeneficiario) {
		this.companiaBeneficiario = companiaBeneficiario;
	}

	/**
	 * @return the beneficiario
	 */
	public String getBeneficiario() {
		return beneficiario;
	}

	/**
	 * @param beneficiario the beneficiario to set
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	/**
	 * @return the importe
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the fechaAlta
	 */
	public String getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta the fechaAlta to set
	 */
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaVigencia
	 */
	public String getFechaVigencia() {
		return fechaVigencia;
	}

	/**
	 * @param fechaVigencia the fechaVigencia to set
	 */
	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	/**
	 * @return the folioOperacion
	 */
	public String getFolioOperacion() {
		return folioOperacion;
	}

	/**
	 * @param folioOperacion the folioOperacion to set
	 */
	public void setFolioOperacion(String folioOperacion) {
		this.folioOperacion = folioOperacion;
	}

	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the codigoCanal
	 */
	public String getCodigoCanal() {
		return codigoCanal;
	}

	/**
	 * @param codigoCanal the codigoCanal to set
	 */
	public void setCodigoCanal(String codigoCanal) {
		this.codigoCanal = codigoCanal;
	}

	/**
	 * @return the horaExpiracion
	 */
	public String getHoraExpiracion() {
		return horaExpiracion;
	}

	/**
	 * @param horaExpiracion the horaExpiracion to set
	 */
	public void setHoraExpiracion(String horaExpiracion) {
		this.horaExpiracion = horaExpiracion;
	}
}
