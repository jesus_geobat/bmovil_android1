package suitebancomer.aplicaciones.bmovil.classes.model;

/**
 * Created by OOROZCO on 4/15/16.
 */
public class DetallePromotorPatrimonial {

    private String numeroPromotor;
    private String nombrePromotor;
    private String emailPromotor;
    private String centrocosto;
    private String telefonoPromotor;
    private String extensionPromotor;
    private String codigoPostalPromotor;
    private String estadoPromotor;
    private String claveSucursal;
    private String nombreSucursal;
    private String domicilioSucursal;
    private String poblacionSucursal;
    private String cuentaEje;
    private String segmentoCliente;

    public String getNumeroPromotor() {
        return numeroPromotor;
    }

    public void setNumeroPromotor(String numeroPromotor) {
        this.numeroPromotor = numeroPromotor;
    }

    public String getNombrePromotor() {
        return nombrePromotor;
    }

    public void setNombrePromotor(String nombrePromotor) {
        this.nombrePromotor = nombrePromotor;
    }

    public String getCentrocosto() {
        return centrocosto;
    }

    public void setCentrocosto(String centrocosto) {
        this.centrocosto = centrocosto;
    }

    public String getTelefonoPromotor() {
        return telefonoPromotor;
    }

    public void setTelefonoPromotor(String telefonoPromotor) {
        this.telefonoPromotor = telefonoPromotor;
    }

    public String getExtensionPromotor() {
        return extensionPromotor;
    }

    public void setExtensionPromotor(String extensionPromotor) {

        char[] auxiliar = extensionPromotor.toCharArray();
        int index;

        for(index = 0 ; index < extensionPromotor.length() ; index ++)
        {
            if(auxiliar[index] != '0')
                break;
        }
        this.extensionPromotor = extensionPromotor.substring(index);
    }

    public String getCodigoPostalPromotor() {
        return codigoPostalPromotor;
    }

    public void setCodigoPostalPromotor(String codigoPostalPromotor) {
        this.codigoPostalPromotor = codigoPostalPromotor;
    }

    public String getEstadoPromotor() {
        return estadoPromotor;
    }

    public void setEstadoPromotor(String estadoPromotor) {
        this.estadoPromotor = estadoPromotor;
    }

    public String getClaveSucursal() {
        return claveSucursal;
    }

    public void setClaveSucursal(String claveSucursal) {
        this.claveSucursal = claveSucursal;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getDomicilioSucursal() {
        return domicilioSucursal;
    }

    public void setDomicilioSucursal(String domicilioSucursal) {
        this.domicilioSucursal = domicilioSucursal;
    }

    public String getPoblacionSucursal() {
        return poblacionSucursal;
    }

    public void setPoblacionSucursal(String poblacionSucursal) {
        this.poblacionSucursal = poblacionSucursal;
    }

    public String getCuentaEje() {
        return cuentaEje;
    }

    public void setCuentaEje(String cuentaEje) {
        this.cuentaEje = cuentaEje;
    }

    public String getSegmentoCliente() {
        return segmentoCliente;
    }

    public void setSegmentoCliente(String segmentoCliente) {
        this.segmentoCliente = segmentoCliente;
    }

    public String getEmailPromotor() {
        return emailPromotor;
    }

    public void setEmailPromotor(String emailPromotor) {
        this.emailPromotor = emailPromotor;
    }


}
