package suitebancomer.classes.gui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.Button;

import suitebancomer.classes.common.GuiTools;

public class CustomButtonTDC extends Button {

    private Paint m_paint = new Paint();

    public CustomButtonTDC(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        final float scale = getResources().getDisplayMetrics().density;
        double factor = GuiTools.getScaleFactor();
        m_paint.setColor(Color.rgb(9,79,164));
        m_paint.setTextSize((float) (11d * factor + 0.5d));
        m_paint.setAntiAlias(true);
        String text=getText().toString();
        canvas.drawText(text, (float)(145d * factor) + 0.5f, (float)(37d * factor) + 0.5f, m_paint);


    }
}	