package suitebancomer.classes.gui.views;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by andres.vicentelinare on 26/09/2016.
 */
public class AmountPlaceholderText extends AmountField {
    /**
     * Make a new AmountField Object.
     *
     * @param context the context of the field
     * @param attrs
     */
    public AmountPlaceholderText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void reset() {
        this.isResetting = true;
        this.typedString = new StringBuffer();
        this.setText("");
        this.isResetting = false;
    }
}
