package suitebancomer.classes.common;

import android.content.Context;
import android.util.Log;

import com.bancomer.mbanking.SuiteApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Properties;

import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class PropertiesManager {
	// #region Constantes.
	/**
	 * Nombre del archivo de propiedades.
	 */
	private static final String CONFIGURATION_FILE_NAME = "EstatusAplicaciones.prop";
	
	/**
	 * Nombre de la propiedad que determina si BMovil esté o no activado.
	 */
	private static final String PROP_BMOVIL_ACTIVATED = "bmovilActivado";
	
	/**
	 * Nombre de la propiedad que determina si Sottoken esté o no activado.
	 */
	private static final String PROP_SOFTTOKEN_ACTIVATED = "softTokenActivado";
	
	/**
	 * Nombre de la propiedad que determina la fecha de la primera activación de Bmovil.
	 */
	private static final String PROP_BMOVIL_FIRST_ACTIVATION_DATE = "primerActivacionFe";
	
	/**
	 * Nombre de la propiedad que determina la hora de la primera activación de Bmovil.
	 */
	private static final String PROP_BMOVIL_FIRST_ACTIVATION_HOUR = "primerActivacionHr";
	// #endregion
	
	// #region Variables.
	/**
	 * Propiedades de la aplicacion.
	 */
	private Properties properties;
	// #endregion
	
	// #region Singleton.
	/**
	 * La instancia de la clase.
	 */
	private static PropertiesManager manager = null;
	
	/**
	 * @return La instancia de la clase.
	 */
	public static PropertiesManager getCurrent() {
		if(null == manager)
			manager = new PropertiesManager();
		return manager;
	}
	
	/**
	 * Inicializa el administrador de propiedades. 
	 */
	private PropertiesManager() {
		properties = null;
		File file = new File(SuiteApp.appContext.getFilesDir(), CONFIGURATION_FILE_NAME);
		
		if(!file.exists())
			initPropertiesFile(file);
		else
			loadPropertiesFile();
	}
	// #endregion
	
	// #region Carga del archivo de propiedades.
	/**
	 * Inicializa el archivo de propiedades con los valores iniciales para cada propiedad.
	 * @param file
	 */
	private void initPropertiesFile(File file) {
		try {
			file.createNewFile();
		} catch(IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al crear el archivo de propiedades.", ioEx);
			return;
		}
		
		loadPropertiesFile();
		
		if(null != properties) {
			setPropertynValue(PROP_BMOVIL_ACTIVATED, false);
			setPropertynValue(PROP_SOFTTOKEN_ACTIVATED, false);
			setPropertynValue(PROP_BMOVIL_FIRST_ACTIVATION_DATE, "");
			setPropertynValue(PROP_BMOVIL_FIRST_ACTIVATION_HOUR, "");
		}
	}
	
	/**
	 * Carga el archivo de propiedades.
	 */
	private void loadPropertiesFile() {
		InputStream input;
		try {
			input = SuiteApp.appContext.openFileInput(CONFIGURATION_FILE_NAME);
		} catch(FileNotFoundException fnfEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar el archivo de prpiedades para lectura.", fnfEx);
			return;
		}

		properties = new Properties();
		try {
			properties.load(input);
		} catch(IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargas las propiedades.", ioEx);
			properties = null;
			return;
		}
	}
	// #endregion
	
	// #region Generic getters and setters for properties.
	/**
	 * Establece el valor especificado a la propiedad indicada.
	 * @param propertyName El nombre de la propiedad.
	 * @param value El valor a establecer.
	 */
	private void setPropertynValue(String propertyName, String value) {
		if(null == properties || Tools.isEmptyOrNull(propertyName))
			return;
		if(null == value)
			value = "";
		
		properties.setProperty(propertyName, value);
		storeFileForProperty(propertyName, value);
	}
	
	/**
	 * Establece el valor especificado a la propiedad indicada.
	 * @param propertyName El nombre de la propiedad.
	 * @param value El valor a establecer.
	 */
	private void setPropertynValue(String propertyName, boolean value) {
//		if(null == properties || Tools.isEmptyOrNull(propertyName))
//			return;
//		
//		String propertyValue = String.valueOf(value);
//		
//		properties.setProperty(propertyName, propertyValue);
//		storeFileForProperty(propertyName, propertyValue);
		String propertyValue = String.valueOf(value);
		setPropertynValue(propertyName, propertyValue);
	}
	
	/**
	 * Obtiene el valor de la propiedad especificada.
	 * @param propertyName El nombre de la propiedad.
	 * @return El valor de la propiedad.
	 */
	private String getPropertynValue(String propertyName) {
		if(null == properties)
			return null;
		
		String value = properties.getProperty(propertyName);
		return value;
	}
	// #endregion
	
	// #region Setters y Getters del estatus de activación para las aplicaciones.
	/**
	 * Obtiene el valor de la propiedad que indica si bmovil esté o no activada.
	 * <br>
	 * Equivalente a getApplicationActivationValue(PROP_BMOVIL_ACTIVATED).
	 * @return El estado de la bmovil.
	 */
	public boolean getBmovilActivated() {
		boolean result;
		
		String value = getPropertynValue(PROP_BMOVIL_ACTIVATED);
		result = Boolean.parseBoolean(value);
		
		return result;
	}
	
	/**
	 * Obtiene el valor de la propiedad que indica si sottoken esté o no activada.
	 * <br>
	 * Equivalente a getApplicationActivationValue(PROP_SOFTTOKEN_ACTIVATED).
	 * @return El estado de la sottoken.
	 */
	public boolean getSofttokenActivated() {
		boolean result;
		
		String value = getPropertynValue(PROP_SOFTTOKEN_ACTIVATED);
		result = Boolean.parseBoolean(value);
		
		return result;
	}

	/**
	 * Establece el valor especificado a la propiedad que indica si bmovil esté o no activada.
	 * <br>
	 * Equivalente a setApplicationActivationValue(PROP_BMOVIL_ACTIVATED, value).
	 * @param value El valor a establecer.
	 */
	public void setBmovilActivated(boolean value) {
		setPropertynValue(PROP_BMOVIL_ACTIVATED, value);
	}
	
	/**
	 * Establece el valor especificado a la propiedad que indica si sottoken esté o no activada.
	 * <br>
	 * Equivalente a setApplicationActivationValue(PROP_SOFTTOKEN_ACTIVATED, value).
	 * @param value El valor a establecer.
	 */
	public void setSofttokenActivated(boolean value) {
		setPropertynValue(PROP_SOFTTOKEN_ACTIVATED, value);
	}
	// #endregion

	// #region Setters y Getters de las propiedades de primera activación para bmovil.
	public void setBmovilFirtActivationDate(Calendar c) {
		StringBuilder builder = new StringBuilder();
		
		builder.append(String.valueOf(c.get(Calendar.YEAR)));
		builder.append("-");
		builder.append(String.valueOf(c.get(Calendar.MONTH)));
		builder.append("-");
		builder.append(String.valueOf(c.get(Calendar.DAY_OF_MONTH)));
		
		setPropertynValue(PROP_BMOVIL_FIRST_ACTIVATION_DATE, builder.toString());
	}

	public void setBmovilFirtActivationHour(Calendar c) {
		StringBuilder builder = new StringBuilder();
		
		builder.append(String.valueOf(c.get(Calendar.HOUR_OF_DAY)));
		builder.append("-");
		builder.append(String.valueOf(c.get(Calendar.MINUTE)));
		
		setPropertynValue(PROP_BMOVIL_FIRST_ACTIVATION_HOUR, builder.toString());
	}
	
	public Calendar getBmovilFirtActivationDate() {
		String propValue = getPropertynValue(PROP_BMOVIL_FIRST_ACTIVATION_DATE);
		
		if(Tools.isEmptyOrNull(propValue))
			return null;
		
		String[] values = propValue.split("-");
		
		int year = Integer.parseInt(values[0]);
		int month = Integer.parseInt(values[1]);
		int day = Integer.parseInt(values[2]);
		
		Calendar result = Calendar.getInstance();
		result.set(Calendar.YEAR, year);
		result.set(Calendar.MONTH, month);
		result.set(Calendar.DAY_OF_MONTH, day);
		
		return result;
	}

	public Calendar getBmovilFirtActivationHour() {
		String propValue = getPropertynValue(PROP_BMOVIL_FIRST_ACTIVATION_HOUR);
		
		if(Tools.isEmptyOrNull(propValue))
			return null;
		
		String[] values = propValue.split("-");
		
		int hour = Integer.parseInt(values[0]);
		int minute = Integer.parseInt(values[1]);
		
		Calendar result = Calendar.getInstance();
		result.set(Calendar.HOUR_OF_DAY, hour);
		result.set(Calendar.MINUTE, minute);
		
		return result;
	}
	// #endregion
	
	/**
	 * Guarda el archivo de propiedades para preservar el cambio en alguna de las propiedades. 
	 * El nombre y valor de la propiedad modificada con usados para registrar el proceso mediante el log.  
	 * @param propertyName El nombre de la propiedad a guardar.
	 * @param propertyValue El valor de la propiedad a guardar.
	 * @return True si el archivo fue guardado exitosamente, False de otro modo.
	 */
	private boolean storeFileForProperty(String propertyName, String propertyValue)	{
		if(Tools.isEmptyOrNull(propertyName))
			propertyName = "No name indicated.";
		if(Tools.isEmptyOrNull(propertyValue))
			propertyName = "No value indicated.";
		
		OutputStream output;
		try {	
			output = SuiteApp.appContext.openFileOutput(CONFIGURATION_FILE_NAME, Context.MODE_PRIVATE);
		} catch(FileNotFoundException fnfEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al abrir el archivo para guardar la propiedad " + propertyName, fnfEx);
			return false;
		}
		
		try {
			properties.store(output, null);
		} catch (IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al guardar en el archivo de propiedades los valores: " + propertyName + " - " + propertyValue, ioEx);
			return false;
		}
		
		if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Archivo actualizado con los siguientes cambios: " + propertyName + " - " + propertyValue);
		return true;
	}

	public static void reloadFile(){
		manager=null;
	}
}
