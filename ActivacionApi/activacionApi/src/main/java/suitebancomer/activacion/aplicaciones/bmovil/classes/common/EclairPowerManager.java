/**
 * 
 */
package suitebancomer.activacion.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.os.PowerManager;

import com.bancomer.mbanking.activacion.SuiteApp;

/**
 * Implementation of the App power manager for api7 or greater.
 */
public final class EclairPowerManager extends AbstractSuitePowerManager {
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.common.AbstractSuitePowerManager#isScreenOn()
	 */
	@Override
	public boolean isScreenOn() {
		try {
			final PowerManager pm = (PowerManager) SuiteApp.appContext.getSystemService(Context.POWER_SERVICE);
			return pm.isScreenOn();
		}catch(Exception ex) {
//			if(Server.ALLOW_LOG) Log.wtf("EclairPowerManager", "Error while getting the state of the screen.", ex);
			return false;
		}
	}

}
