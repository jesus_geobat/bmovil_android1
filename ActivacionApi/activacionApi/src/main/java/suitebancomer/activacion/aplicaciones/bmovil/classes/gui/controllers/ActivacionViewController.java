package suitebancomer.activacion.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bancomer.mbanking.activacion.R;
import com.bancomer.mbanking.activacion.SuiteApp;

import suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate;
import suitebancomer.activacion.classes.common.GuiTools;
import suitebancomer.activacion.classes.gui.controllers.BaseViewController;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Reactivacion;

public class ActivacionViewController extends BaseViewController implements OnClickListener {

	private static final String TAG = ActivacionViewController.class.getSimpleName();
	private static final String ERROR_CODE_ACTIVATE = "CNE0365";

	private TextView mTexto;
	private EditText mCodigo;
	private Button mBoton;
	private ImageButton btnLlamar;
	private ActivacionDelegate delegate;

	//AMZ
	public BmovilViewsController parentManager;
	//AMZ


	private Boolean esSolicitarNuevaClave;
	SharedPreferences preferences;


	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteApp.getResourceId("layout_bmovil_activacion_act", "layout"));

		com.bancomer.base.SuiteApp.appContext = this;

		setTitle(SuiteApp.getResourceId("activacion.reactivacion.title", "string"), SuiteApp.getResourceId("icono_activacion", "drawable"));

		this.parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

		TrackingHelper.trackState("activacion", this.parentManager.estados);

		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());

		this.delegate = (ActivacionDelegate) getParentViewsController().getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);

		setDelegate(this.delegate);

		SuiteApp.getInstance().getSuiteViewsController().setCurrentActivityApp(this);

		this.findViews();
		this.scaleToScreenSize();
		this.inicializarPantalla();

		this.statusdesactivado = false;
		this.esSolicitarNuevaClave = false;
		this.delegate.setViewController(this);

		 preferences = getSharedPreferences(bancomer.api.common.commons.Constants.SHARED_PREFERENCES, MODE_PRIVATE);
		String type = preferences.getString(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_TYPE, bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT);
		String activationCode = preferences.getString(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_CODE, bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT);
		String fechaHora = preferences.getString(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_TIME, bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT);
		String codeUsed = preferences.getString(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_CODE_USED, bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT);

		//si el tipo es ac y la clave de activacion existe
		if (bancomer.api.common.commons.Constants.FLAG_ST.equals(type)
				&& !bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT.equals(activationCode)
				&& !fechaHora.equals("false")){
			if(ServerCommons.ALLOW_LOG  )
				Log.i(TAG, "onCreate: cp_flag = " + type + " activationCode = " + activationCode);

			//validar con el reutilizable enviando la clave de activiacon
			//ArrayList<String> valueInfoSMS = UtilsSms.fetchInbox(fechaHora, this, activationCode);

			//if (valueInfoSMS != null && valueInfoSMS.size() > 0 ) {
				if(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT.equals(codeUsed)){
					return;
				}
				mCodigo.setText(activationCode);

				if (mCodigo.getText() != null) {
					if (mCodigo.length() >= 10) {
						SharedPreferences settings;
						SharedPreferences.Editor editor;


						settings = this.getSharedPreferences(bancomer.api.common.commons.Constants.SHARED_PREFERENCES, MODE_PRIVATE);
						editor = settings.edit();

						editor.putString(bancomer.api.common.commons.Constants.SHARED_PREFERENCES_CODE_USED,bancomer.api.common.commons.Constants.SHARED_PREFERENCES_FLAG_DEFAULT);
						editor.commit();
						/*if (validaDatos())
							delegate.realizaActivacion();*/
					}
				}
			/*} else {//si tiene la url pero no es de un sms de bancomer
				showErrorMessage(getString(R.string.clave_no_encontrada), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
					}
				});
			}*/
		}
	}

	@Override
	protected void onStop() {
		if(ServerCommons.ALLOW_LOG  )
			Log.d(TAG, "ON STOP");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		if(ServerCommons.ALLOW_LOG  )
			Log.d(TAG, "ON DESTROY");
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		if(ServerCommons.ALLOW_LOG  )
			Log.d(TAG, "ON PAUSE");
		super.onPause();

		this.parentViewsController.consumeAccionesDePausa();
		this.statusdesactivado = true;

		SuiteApp.setCallsAppDesactivada(Boolean.FALSE);
	}

	@Override
	protected void onResume() {
		super.onResume();
		com.bancomer.base.SuiteApp.appContext = this;

		if (this.statusdesactivado && parentViewsController.consumeAccionesDeReinicio())
			return;

		getParentViewsController().setCurrentActivityApp(this);
	}

	private void cleanSharedPreferences() {
		SharedPreferences.Editor editor = preferences.edit();
		editor.clear();
		editor.commit();
	}

	@Override
	public void goBack() {
		super.goBack();
	}

	private void findViews() {
		this.mTexto = (TextView) findViewById(SuiteApp.getResourceId("activacion_texto", "id"));
		this.mCodigo = (EditText) findViewById(SuiteApp.getResourceId("activacion_usuario", "id"));
		this.mBoton = (Button) findViewById(SuiteApp.getResourceId("activacion_boton", "id"));
		this.btnLlamar = (ImageButton) findViewById(SuiteApp.getResourceId("btnSolicitaCodigo", "id"));
	}

	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scale(this.mTexto, true);
		guiTools.scale(this.mCodigo, true);
		guiTools.scale(this.mBoton);
		guiTools.scale(findViewById(SuiteApp.getResourceId("linkReenviarCodigo", "id")));

		guiTools.scale(findViewById(SuiteApp.getResourceId("pagarServicios_cuentaOrigen", "id")));

	}

	private void inicializarPantalla() {
		final InputFilter[] userFilterArray = new InputFilter[1];
		userFilterArray[0] = new InputFilter.LengthFilter(Constants.ACTIVATION_CODE_LENGTH);

		this.mCodigo.setFilters(userFilterArray);
		this.mBoton.setOnClickListener(this);
		this.btnLlamar.setOnClickListener(this);
		this.mTexto.setText(getString(R.string.clave_activacion));

		//this.mCodigo.setEnabled(true);quitar
		//this.mBoton.setEnabled(false);quitar

		this.mCodigo.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(final CharSequence cs, final int arg1, final int arg2, final int arg3) { }

			@Override
			public void beforeTextChanged(final CharSequence arg0, final int arg1, final int arg2, final int arg3) { }

			@Override
			public void afterTextChanged(final Editable arg0) {
				final Integer codeTextLen = mCodigo.getText().toString().length();

				mBoton.setEnabled(codeTextLen >= 10);
			}
		});

	}

	@Override
	public void onClick(final View arg0) {
		if (arg0.equals(this.mBoton) && this.validaDatos()) {
			this.delegate.realizaActivacion();
		}

		if (arg0.equals(this.btnLlamar)) {
			final Intent sIntent = new Intent(Intent.ACTION_CALL,
					Uri.parse(String.format("tel:%s", Uri.encode(this.getString(R.string.activacion_numero_solicita_codigo)))));

			sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(sIntent);
		}
	}

	boolean validaDatos() {
		return this.delegate.validaDatos(this.mCodigo.getText().toString());
	}

	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		if (ERROR_CODE_ACTIVATE.equals(response.getMessageCode())) {
			this.showCodigoIncorrecto();
		}
		else {
			this.delegate.analyzeResponse(operationId, response);
		}
	}

	public String getClaveActivacion() {
		return this.mCodigo.getText().toString();
	}

	public void onLinkReenviarCodigo(final View sender) {
		this.esSolicitarNuevaClave = true;
		this.cleanSharedPreferences();

		final Reactivacion reactivacion = this.delegate.getReactivacion();

		this.delegate.confirmacionCambioCompania(reactivacion);
	}

	public void showCodigoIncorrecto() {
		final Dialog contactDialog = new Dialog(this);
		final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.codigo_incorrecto, null);

		contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// make dialog itself transparent
		contactDialog.setCancelable(false);
		contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		contactDialog.setContentView(layout);

		final int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
		contactDialog.getWindow().setLayout(width, DrawerLayout.LayoutParams.WRAP_CONTENT);

		final Button intentarNuevamente = (Button) layout.findViewById(R.id.btnintentarnuevamente);
		final Button nuevoCodigo = (Button) layout.findViewById(R.id.btnnuevocodigo);


		intentarNuevamente.setHeight(nuevoCodigo.getHeight());

		intentarNuevamente.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						intentarNuevamente();
						contactDialog.dismiss();
					}
				}
		);
		nuevoCodigo.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						generarNuevoCodigo();
						contactDialog.dismiss();
					}
				}
		);

		contactDialog.show();
	}

	private void generarNuevoCodigo() {
		this.esSolicitarNuevaClave = true;
		this.cleanSharedPreferences();

		final Reactivacion reactivacion = this.delegate.getReactivacion();
		this.delegate.generarNuevoCodigo(reactivacion);
	}

	private void intentarNuevamente() {
		this.mCodigo.setText("");
		this.mBoton.setEnabled(false);
	}
}