package com.bancomer.apiilc.gui.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.ListaDatosViewController;
import bancomer.api.common.gui.controllers.ListaSeleccionViewController;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.apiilc.R;
import bancomer.api.common.gui.delegates.DelegateBaseOperacion;
import bancomer.api.common.timer.TimerController;

import com.bancomer.apiilc.commons.ConstantsILC;
import com.bancomer.apiilc.gui.delegates.ResultadosDelegate;
import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.apiilc.tracking.TrackingILC;

public class ResultadosViewController extends Activity implements View.OnClickListener {

	public BaseViewsController parentManager;
	private InitILC init = InitILC.getInstance();
	private ResultadosDelegate delegate;
	private BaseViewController baseViewController;
	
	private TextView tituloResultados;
	private TextView textoResultados;
	private TextView instruccionesResultados;
	private TextView titulotextoEspecialResultados;
	private TextView textoEspecialResultados;
	private ImageButton menuButton;
	private ImageButton compartirButton;



	//etiquetado Adobe
	String screen = "";
	String screen2 = "";
	String screen3 = "";
	ArrayList<String> list= new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		baseViewController = init.getBaseViewController();

		parentManager = init.getParentManager();
		parentManager.setCurrentActivityApp(this);

		delegate=(ResultadosDelegate) parentManager.getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		delegate.setResultadosViewController(this);

		baseViewController.setActivity(this);
		baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, com.bancomer.apiilc.R.layout.layout_bmovil_resultados);
		baseViewController.setTitle(this, com.bancomer.apiilc.R.string.bmovil_pantallailc_title, com.bancomer.apiilc.R.drawable.icono_pagar_servicios);
		baseViewController.setDelegate(delegate);
		init.setBaseViewController(baseViewController);
		
		findViews();
		scaleToScreenSize();
		
		menuButton.setOnClickListener(this);

		compartirButton.setOnClickListener(this);
		setTitulo();
		delegate.consultaDatosLista();
		configuraPantalla();

		//etiquetado Adobe
		screen = "detalle ilc";
		screen2 = "exito ilc";
		screen3 = "comprobante ilc";
		list.add(screen);
		list.add(screen2);
		list.add(screen3);
		TrackingILC.trackState("exito ilc", list);

	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		/*if(!init.getParentManager().isActivityChanging())
		{
			//TimerController.getInstance().getSessionCloser().cerrarSesion();
			init.getParentManager().resetRootViewController();
			init.resetInstance();
		}*/
	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		init.getParentManager().onUserInteraction();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_bmovil_resultados, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		int opcionesMenu = delegate.getOpcionesMenuResultados();
		boolean showMenu = false;
		if ((opcionesMenu & ConstantsILC.SHOW_MENU_SMS) != ConstantsILC.SHOW_MENU_SMS) {
			menu.removeItem(R.id.save_menu_sms_button);
		} else {
			showMenu = true;
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & ConstantsILC.SHOW_MENU_EMAIL) != ConstantsILC.SHOW_MENU_EMAIL) {
			menu.removeItem(R.id.save_menu_email_button);
		} else {
			showMenu = true;
			//menu.removeItem(R.id.save_menu_email_button);//TODO remover para mostrar menu completo
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & ConstantsILC.SHOW_MENU_PDF) != ConstantsILC.SHOW_MENU_PDF) {
			menu.removeItem(R.id.save_menu_pdf_button);
		} else {
			//showMenu = true;
			menu.removeItem(R.id.save_menu_pdf_button);//TODO remover para mostrar menu completo
		}
		if (delegate.getFrecOpOK() || ((opcionesMenu & ConstantsILC.SHOW_MENU_FRECUENTE) != ConstantsILC.SHOW_MENU_FRECUENTE)) {
			menu.removeItem(R.id.save_menu_frecuente_button);
		} else {
			showMenu = true;
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & ConstantsILC.SHOW_MENU_RAPIDA) != ConstantsILC.SHOW_MENU_RAPIDA) {
			menu.removeItem(R.id.save_menu_rapida_button);
		} else {
			//showMenu = true;
			menu.removeItem(R.id.save_menu_rapida_button);//TODO remover para mostrar menu completo
		}
		if ((opcionesMenu & ConstantsILC.SHOW_MENU_BORRAR) != ConstantsILC.SHOW_MENU_BORRAR) {
			menu.removeItem(R.id.save_menu_borrar_button);
		} else {
			showMenu = true;
		}
		
		return showMenu;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Map<String,Object> envioConfirmacionMap = new HashMap<String, Object>();
		int i = item.getItemId();
		if (i == R.id.save_menu_sms_button) {
			delegate.enviaSMS();

			String opcion = "sms";
			list.add(opcion);
			TrackingILC.trackState("sms", list);
			TrackingILC.trackState("resul", list);
			return true;
		} else if (i == R.id.save_menu_email_button) {
			delegate.enviaEmail();

			String opcion = "email";
			list.add(opcion);
			TrackingILC.trackState("email", list);
			TrackingILC.trackState("resul", list);
			return true;
		} else {
			return false;
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
		}
		return true;
	}
	
	public void setTitulo() {
		init.getBaseViewController().setTitle(this, delegate.getTextoEncabezado(), delegate.getNombreImagenEncabezado());
	}

	public void setListaDatos(ArrayList<Object> datos) {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(R.id.resultados_lista_datos));

		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		ListaDatosViewController listaDatos = new ListaDatosViewController(this, params, init.getBaseViewController().getParentViewsController(),false);

		cambiarColores(listaDatos);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.showLista();
		LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.resultados_lista_datos);
		layoutListaDatos.addView(listaDatos);
	}

	private void cambiarColores(ListaDatosViewController listaDatos) {
	}

	public void configuraPantalla() {
		String titulo = delegate.getTextoTituloResultado();
		String texto = delegate.getTextoPantallaResultados();
		String instrucciones = delegate.getTextoAyudaResultados();
		String tituloTextoEspecial = delegate.getTituloTextoEspecialResultados();
		String textoEspecial = delegate.getTextoEspecialResultados();
		
		if (titulo.equals("")) {
			tituloResultados.setVisibility(View.GONE);
		} else {
			tituloResultados.setText(titulo);
			tituloResultados.setTextColor(getResources().getColor(delegate.getColorTituloResultado()));
		}
		
		if (texto.equals("")) {
			textoResultados.setVisibility(View.GONE);
		} else {
			textoResultados.setText(texto);
		}
		
		if (instrucciones.equals("")) {
			instruccionesResultados.setVisibility(View.GONE);
		} else {
			instruccionesResultados.setText(instrucciones);
		}
		
		if (tituloTextoEspecial.equals("")) {
			titulotextoEspecialResultados.setVisibility(View.GONE);
		}else {
			titulotextoEspecialResultados.setText(tituloTextoEspecial);
		}
		
		if (textoEspecial.equals("")) {
			textoEspecialResultados.setVisibility(View.GONE);
		} else {
			textoEspecialResultados.setText(textoEspecial);
		}

		if(instruccionesResultados.getText() == ""){
			compartirButton.setVisibility(View.INVISIBLE);
		}


	if(!textoEspecial.equals("")&& tituloTextoEspecial.equals("")&& !instrucciones.equals("")){
		String text= textoEspecial;
		SpannableString textS= new SpannableString(text);
		textS.setSpan(new ForegroundColorSpan(Color.rgb(77,77,77)),0,text.length(),0);
		textoEspecialResultados.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
		textoEspecialResultados.setText(textS);
		String text2= instrucciones;
		SpannableString textS2= new SpannableString(text2);
		textS2.setSpan(new ForegroundColorSpan(Color.rgb(176,176,176)),0,text2.length(),0);
		instruccionesResultados.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
		instruccionesResultados.setText(textS2);
		titulotextoEspecialResultados.setVisibility(View.GONE);
	}
	}
	
	@Override
	public void onClick(View v) {
		if (v == menuButton) {
			botonMenuClick();
		}else if (v==compartirButton){
			openOptionsMenu();
		}
	}
	
	public void botonMenuClick() {
		init.getParentManager().setActivityChanging(true);
        ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
		init.resetInstance();
	}
	
	public BroadcastReceiver createBroadcastReceiver() {
    	
	     return new BroadcastReceiver() {
			@Override
		     public void onReceive(Context ctx, Intent intent) {
		    	 
		    	 String toastMessage;
		    	 
			     switch (getResultCode()) {
				     case Activity.RESULT_OK:
					     toastMessage = getString(R.string.sms_success);
					     break;
				     case SmsManager.RESULT_ERROR_NO_SERVICE: //"SMS: No service"
				    	 toastMessage = getString(R.string.sms_error_noService);
				    	 break;
				     case SmsManager.RESULT_ERROR_NULL_PDU: //"SMS: Null PDU"
				    	 toastMessage = getString(R.string.sms_error_nullPdu);
				    	 break;
				     case SmsManager.RESULT_ERROR_RADIO_OFF: //"SMS: Radio off"
				    	 toastMessage = getString(R.string.sms_error_radioOff);
				    	 break;
				     case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				     default:
				    	 toastMessage = getString(R.string.sms_error);
				    	 break;
			     }
			     
			      init.getBaseViewController().ocultaIndicadorActividad();
			     Toast.makeText(getBaseContext(), toastMessage,Toast.LENGTH_SHORT).show();
		     }
	     };
	}
	
	private void findViews() {
		tituloResultados = (TextView)findViewById(R.id.resultado_titulo);
		textoResultados = (TextView)findViewById(R.id.resultado_texto);
		instruccionesResultados = (TextView)findViewById(R.id.resultado_instrucciones);
		textoEspecialResultados = (TextView) findViewById(R.id.resultado_texto_especial);
		titulotextoEspecialResultados = (TextView) findViewById(R.id.resultado_titulo_texto_especial);
		menuButton = (ImageButton)findViewById(R.id.resultados_menu_button);
		compartirButton = (ImageButton)findViewById(R.id.resultados_compartir);
	}
	
	private void scaleToScreenSize() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(tituloResultados, true);
		guiTools.scale(textoResultados, true);
		guiTools.scale(instruccionesResultados, true);
		guiTools.scale(textoEspecialResultados, true);
		guiTools.scale(titulotextoEspecialResultados, true);
		guiTools.scale(menuButton);
	}
	
	public void setListaClave(ArrayList<Object> datos) {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(R.id.resultados_lista_clave));
		
		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		
		ListaSeleccionViewController listaSeleccion = new ListaSeleccionViewController(this, params, init.getBaseViewController().getParentViewsController(),false);
			listaSeleccion.setDelegate(delegate);
			listaSeleccion.setFijarLista(true);
			listaSeleccion.setNumeroColumnas(1);
			listaSeleccion.setEncabezado(null);
			listaSeleccion.setLista(datos);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(datos.size());
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
			LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.resultados_lista_clave);
			layoutListaDatos.addView(listaSeleccion);
			
		
			findViewById(R.id.resultados_lista_clave).setVisibility(View.VISIBLE);
	}

}
