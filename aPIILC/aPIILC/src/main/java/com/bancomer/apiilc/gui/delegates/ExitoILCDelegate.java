package com.bancomer.apiilc.gui.delegates;

import android.util.Log;

import com.bancomer.apiilc.commons.ConstantsILC;
import com.bancomer.apiilc.gui.controllers.ExitoOfertaILCViewController;
import com.bancomer.apiilc.implementations.InitILC;

/**
 * Created by RPEREZ on 14/09/15.
 */

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.gui.delegates.DelegateBaseOperacion;
import bancomer.api.common.io.ServerResponse;

import com.bancomer.apiilc.io.AuxConectionFactory;
import com.bancomer.apiilc.io.Server;
import com.bancomer.apiilc.models.OfertaILC;
import com.bancomer.apiilc.models.AceptaOfertaILC;
import com.bancomer.apiilc.R;

import bancomer.api.common.gui.controllers.BaseViewController;

public class ExitoILCDelegate implements DelegateBaseOperacion, BaseDelegate {

	public static final long EXITO_OFERTA_DELEGATE = 631453334566765999L;

	private AceptaOfertaILC aceptaOfertaILC;
	private OfertaILC ofertaILC;
	private ExitoOfertaILCViewController controller;
	private InitILC init = InitILC.getInstance();
	private BaseViewController viewController;
	String fechaCat="";

	public ExitoILCDelegate(AceptaOfertaILC aceptaOfertaILC, OfertaILC ofertaILC){
		this.aceptaOfertaILC= aceptaOfertaILC;
		this.ofertaILC = ofertaILC;
	}

	public ExitoOfertaILCViewController getController() {
		return controller;
	}

	public void setController(ExitoOfertaILCViewController controller) {
		this.controller = controller;
	}

	public BaseViewController getcontroladorExitoILC() {
		return viewController;
	}

	public void setcontroladorExitoILCView(BaseViewController viewController) {
		this.viewController = viewController;
	}

	public AceptaOfertaILC getAceptaOfertaILC() {
		return aceptaOfertaILC;
	}

	public void setAceptaOfertaILC(AceptaOfertaILC aceptaiOfertaILC) {
		this.aceptaOfertaILC = aceptaiOfertaILC;
	}


	public void showMenu() {
		init.getParentManager().setActivityChanging(true);
        ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
		init.resetInstance();
		
	}


	public void realizaOperacion(int idOperacion, BaseViewController baseViewController, boolean isEmail, boolean isSMS){
		if(idOperacion == Server.EXITO_OFERTA){
			ICommonSession session = init.getSession();
			String user = session.getUsername();
			String ium= session.getIum();
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion", "exitoILC");
			params.put("numeroTarjeta",aceptaOfertaILC.getNumeroTarjeta());
			String lineaActual= Tools.formatAmount(aceptaOfertaILC.getLineaActual(), false);
			String lineaA=lineaActual.replace("$", "");
			params.put("lineaActual",lineaA);

			String lineaFinal= Tools.formatAmount(aceptaOfertaILC.getLineaFinal(),false);
			String lineaF=lineaFinal.replace("$", "");
			params.put("lineaFinal",lineaF);
			params.put("numeroCelular",user);
			params.put("email",aceptaOfertaILC.getEmail());
			params.put("folioInternet",aceptaOfertaILC.getFolioInternet());
			params.put("Cat",ofertaILC.getCat());
			params.put("fechaCat",ofertaILC.getFechaCat());
			String mensajeA="";
			if(isEmail && isSMS){
				String importe= Tools.formatAmount(aceptaOfertaILC.getImporte(),false);
				String imp=importe.replace("$", "");
				params.put("importe", imp);
				mensajeA="011";
			}
			else if(isEmail){
				String importe= Tools.formatAmount(aceptaOfertaILC.getImporte(),false);
				String imp=importe.replace("$", "");
				params.put("importe", imp);

				mensajeA="010";
			}
			else if(isSMS){
				String importe= Tools.formatAmount(aceptaOfertaILC.getImporte(),false);
				String imp=importe.replace("$", "");
				params.put("importe", imp);
				mensajeA="001";
			}


			params.put("mensajeA",mensajeA);
			params.put("IUM",ium);

			Log.e("baseView",baseViewController.toString()+"   :P");
			Hashtable<String, String> paramTable2  = AuxConectionFactory.exitoOfertaILC(params);
			doNetworkOperation(Server.EXITO_OFERTA, paramTable2, baseViewController,true,new Object(),Server.isJsonValueCode.ONECLICK);
		}

	}

	@Override
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
		InitILC.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false);
	}
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle,Server.isJsonValueCode isJsonValue) {
		InitILC.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false,isJson,handle,isJsonValue);
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {

	}

	@Override
	public void performAction(Object obj) {

	}

	@Override
	public long getDelegateIdentifier() {
		return 0;
	}

	@Override
	public String getNumeroCuentaParaRegistroOperacion() {
		return "";
	}

	@Override
	public String loadOtpFromSofttoken(Constants.TipoOtpAutenticacion tipoOTP, DelegateBaseOperacion delegate) { return null; }


	public String getTextoTituloResultado() { return ""; }


	public int getColorTituloResultado() {
		return 0;
	}


	public String getTextoPantallaResultados() {
		return "";
	}

	public void showResultados(){
		init.getParentManager().setActivityChanging(true);
		init.showResultadosViewController(this, -1, -1);
	}


	public String getTextoSMS() { return ""; }


	public String getTextoAyudaResultados() {
		return "CAT "+ofertaILC.getCat()+"%"+" sin I.V.A Informativo.\nFecha de Cálculo al "+fechaCat;

	}


	public String getTituloTextoEspecialResultados() {
		return "";
	}


	public String getTextoEspecialResultados() {
		return controller.getResources().getString(R.string.bmovil_result_textoAyuda);
	}


	public int getOpcionesMenuResultados() {
		return ConstantsILC.SHOW_MENU_SMS | ConstantsILC.SHOW_MENU_EMAIL ;
	}


	public ArrayList<Object> getDatosTablaResultados() {

		ArrayList<Object> datosResultados = new ArrayList<Object>();
		ArrayList<String> registro;
		formatoFechaCatMostrar();
		registro = new ArrayList<String>();
		registro.add(controller.getResources().getString(R.string.bmovil_result_credito));
		registro.add(Tools.hideAccountNumber(aceptaOfertaILC.getNumeroTarjeta()));
		datosResultados.add(registro);

		registro = new ArrayList<String>();
		registro.add(controller.getResources().getString(R.string.bmovil_result_beneficiario));
		registro.add(aceptaOfertaILC.getBeneficiario());
		datosResultados.add(registro);

		registro = new ArrayList<String>();
		registro.add(controller.getResources().getString(R.string.bmovil_result_linea1));
		registro.add(Tools.formatAmount(aceptaOfertaILC.getLineaActual().replace(",",""),false));
		datosResultados.add(registro);

		registro = new ArrayList<String>();
		registro.add(controller.getResources().getString(R.string.bmovil_result_linea));
		registro.add(Tools.formatAmount(aceptaOfertaILC.getImporte().replace(",",""),false));
		datosResultados.add(registro);

		registro = new ArrayList<String>();
		registro.add(controller.getResources().getString(R.string.bmovil_result_linea2));
		registro.add(Tools.formatAmount(aceptaOfertaILC.getLineaFinal().replace(",",""),false));
		datosResultados.add(registro);

		registro = new ArrayList<String>();
		registro.add(controller.getResources().getString(R.string.bmovil_result_fecha));
		registro.add(aceptaOfertaILC.getFechaOperacion());
		datosResultados.add(registro);

		registro = new ArrayList<String>();
		registro.add(controller.getResources().getString(R.string.bmovil_result_hora));
		registro.add(aceptaOfertaILC.getHora());
		datosResultados.add(registro);

		registro = new ArrayList<String>();
		registro.add(controller.getResources().getString(R.string.bmovil_result_folio));
		registro.add(aceptaOfertaILC.getFolioInternet());
		datosResultados.add(registro);

		return datosResultados;

	}


	public int getTextoEncabezado() {
		return R.string.bmovil_pantallailc_title;
	}


	public int getNombreImagenEncabezado() {
		return R.drawable.icono_pagar_servicios;
	}

	public void formatoFechaCatMostrar(){
		try{
			String[] meses = { "enero", "febrero", "marzo", "abril", "mayo",
					"junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };
			String fechaCatM= ofertaILC.getFechaCat();
			int mesS=Integer.parseInt(fechaCatM.substring(5,7));
			String dia= fechaCatM.substring(fechaCatM.length()-2);
			String anio= fechaCatM.substring(0,4);

			fechaCat= dia+" de "+meses[mesS-1].toString()+ " de "+anio;

		}catch(Exception ex){
			if(Server.ALLOW_LOG) Log.e("","error formato"+ex);
		}
	}

}
