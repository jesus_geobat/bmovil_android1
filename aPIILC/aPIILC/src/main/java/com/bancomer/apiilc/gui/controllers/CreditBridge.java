package com.bancomer.apiilc.gui.controllers;

import android.os.Bundle;

import com.bancomer.apiilc.gui.delegates.DetalleOfertaCreditDelegate;
import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.apiilc.io.Server;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;

/**
 * Created by OOROZCO on 12/3/15.
 */
public class CreditBridge extends BaseActivity {

    private BaseViewController baseViewController;
    public BaseViewsController parentManager;
    private InitILC init = InitILC.getInstance();
    private DetalleOfertaCreditDelegate delegate;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate=(DetalleOfertaCreditDelegate) parentManager.getBaseDelegateForKey(DetalleOfertaCreditDelegate.DETALLE_OFERTA_CREDIT);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        delegate.realizaOperacion(Server.ACEPTACION_OFERTA, init.getBaseViewController());

    }
}
