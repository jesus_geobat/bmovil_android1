package com.bancomer.apiilc.gui.delegates;

import android.app.Activity;
import android.content.DialogInterface;
import android.util.Log;

import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.apiilc.io.AuxConectionFactory;
import com.bancomer.apiilc.io.Server;
import com.bancomer.apiilc.models.AceptaOfertaILC;
import com.bancomer.apiilc.models.OfertaILC;

import java.util.Hashtable;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by OOROZCO on 12/3/15.
 */
public class DetalleOfertaCreditDelegate implements BaseDelegate {

    public static final long DETALLE_OFERTA_CREDIT = 201512034566765659L;

    AceptaOfertaILC aceptaOfertaILC;
    OfertaILC ofertaILC;

    private Activity controller;
    private InitILC init = InitILC.getInstance();


    public void setController(Activity controller) {
        this.controller = controller;
    }

    public DetalleOfertaCreditDelegate(OfertaILC ofertaILC){
        init = InitILC.getInstance();
        this.ofertaILC = ofertaILC;

    }

    public OfertaILC getOfertaILC() {
        return ofertaILC;
    }


    public void setOfertaILC(OfertaILC ofertaILC) {
        this.ofertaILC = ofertaILC;
    }


    public void realizaOperacion(int idOperacion, BaseViewController baseViewController) {
        if (idOperacion == Server.ACEPTACION_OFERTA){

            String user = init.getSession().getUsername();
            String ium= init.getSession().getIum();
            String cveCamp =  init.getOfertaILC().getCveCamp();

            //Mod 54787
            String montoLineaActual  = Tools.formatAmountForServer(getOfertaILC().getLineaActual());
            String importeSolicitado = Tools.formatAmountForServer(getOfertaILC().getImporte());
            String montoLineaFinal        = Tools.formatAmountForServer(getOfertaILC().getLineaFinal()) ;


            //formatoFechaCat();
            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion", "aceptacionILCBmovil");
            params.put("numeroTarjeta",getOfertaILC().getILCAccount().getNumber());
            params.put("lineaActual", montoLineaActual.substring(0,montoLineaActual.length()-2));    //Mod 54787
            params.put("importe", importeSolicitado.substring(0,importeSolicitado.length()-2));      //Mod 54787
            params.put("lineaFinal",montoLineaFinal.substring(0,montoLineaFinal.length()-2));        //Mod 54787
            params.put("cveCamp",cveCamp);
            params.put("numeroCelular", user);
            params.put("IUM", ium);
            params.put("Cat", (getOfertaILC().getCat()).replace(".",","));                           //Mod 54787
            params.put("fechaCat", getOfertaILC().getFechaCat());// con o sin formato?
/*
            Log.i("modf","Monto linea actual: "+montoLineaActual.substring(0,montoLineaActual.length()-2));
            Log.i("modf","Importe solicitado: "+importeSolicitado.substring(0,importeSolicitado.length()-2));
            Log.i("modf","Monto final: "+montoLineaFinal.substring(0,montoLineaFinal.length()-2));
            Log.i("modf","Cat: "+(getOfertaILC().getCat()).replace(".",","));
*/
            Hashtable<String, String> paramTable2  = AuxConectionFactory.aceptaOfertaILC(params);
            doNetworkOperation(Server.ACEPTACION_OFERTA, paramTable2, InitILC.getInstance().getBaseViewController(),true,new AceptaOfertaILC(),Server.isJsonValueCode.ONECLICK);

        }else if(idOperacion == Server.RECHAZO_OFERTA){

            String user = init.getSession().getUsername();
            String ium= init.getSession().getIum();
            String cveCamp =  init.getOfertaILC().getCveCamp();

            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion", "noAceptacionILCBMovil");
            params.put("numeroCelular", user);
            params.put("cveCamp",cveCamp);
            params.put("descripcionMensaje","0002");
            params.put("contrato",getOfertaILC().getContrato());
            params.put("IUM", ium);
            Hashtable<String, String> paramTable2  = AuxConectionFactory.rechazoOfertaILC(params);
            doNetworkOperation(Server.RECHAZO_OFERTA, paramTable2, InitILC.getInstance().getBaseViewController(),true, new Object(),Server.isJsonValueCode.ONECLICK);
        }

    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        InitILC.getInstance().getBaseSubApp().invokeNetworkOperation(operationId,params,caller,false);
    }
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle,Server.isJsonValueCode isJsonValue) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false,isJson,handle,isJsonValue);

    }

    @Override
    public void performAction(Object obj) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (operationId == Server.ACEPTACION_OFERTA) {
            if(response.getStatus() == ServerResponse.OPERATION_ERROR){
                aceptaOfertaILC = (AceptaOfertaILC) response.getResponse();
                ICommonSession session = init.getSession();
                try {
                    session.setPromocion(aceptaOfertaILC.getPromociones());

                    DialogInterface.OnClickListener clickListener=new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            init.getParentManager().setActivityChanging(true);
                            ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                            init.resetInstance();
                        }
                    };

                    init.getBaseViewController().showInformationAlert(controller, response.getMessageText(), clickListener);
                }catch (NullPointerException e){
                    init.getBaseViewController().showInformationAlert(controller, response.getMessageText());
                }
            }else{
                aceptaOfertaILC = (AceptaOfertaILC) response.getResponse();
                ICommonSession session = init.getSession();
                try {
                    session.setPromocion(aceptaOfertaILC.getPromociones());
                }catch (NullPointerException e){}
                init.getParentManager().setActivityChanging(true);
                init.showExitoCredit(aceptaOfertaILC, ofertaILC);
            }
        }else if(operationId== Server.RECHAZO_OFERTA){
            init.getParentManager().setActivityChanging(true);
            ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
            init.resetInstance();
        }
    }




}
