package com.bancomer.apiilc.models;

/**
 * Created by RPEREZ on 14/09/15.
 */

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;

public class OfertaILC implements ParsingHandler{
	String importe;
	ILCAccount ILCAccount = new ILCAccount();
	String contrato;
	String lineaActual;
	String lineaFinal;
	String fechaCat;
	String cat;
	public ArrayList<ILCAccount> ILCAccountILC;
	
	public ILCAccount getILCAccount() {
		return ILCAccount;
	}

	public void setILCAccount(Account ILCAccount) {
		this.ILCAccount.setBalance(ILCAccount.getBalance());
        this.ILCAccount.setNumber(ILCAccount.getNumber());
        this.ILCAccount.setDate(ILCAccount.getDate());
        this.ILCAccount.setVisible(ILCAccount.isVisible());
        this.ILCAccount.setCurrency(ILCAccount.getCurrency());
        this.ILCAccount.setType(ILCAccount.getType());
        this.ILCAccount.setAlias(ILCAccount.getAlias());
        this.ILCAccount.setConcept(ILCAccount.getConcept());
        this.ILCAccount.setCelularAsociado(ILCAccount.getCelularAsociado());
        this.ILCAccount.setCodigoCompania(ILCAccount.getCodigoCompania());
        this.ILCAccount.setDescripcionCompania(ILCAccount.getDescripcionCompania());
        this.ILCAccount.setFechaUltimaModificacion(ILCAccount.getFechaUltimaModificacion());
        this.ILCAccount.setIndicadorSPEI(ILCAccount.getIndicadorSPEI());

	}

	public ArrayList<ILCAccount> getILCAccountILC() {
		return ILCAccountILC;
	}

	public void setILCAccountILC(ArrayList<ILCAccount> ILCAccountILC) {
		this.ILCAccountILC = ILCAccountILC;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getLineaActual() {
		return lineaActual;
	}

	public void setLineaActual(String lineaActual) {
		this.lineaActual = lineaActual;
	}

	public String getLineaFinal() {
		return lineaFinal;
	}

	public void setLineaFinal(String lineaFinal) {
		this.lineaFinal = lineaFinal;
	}

	public String getFechaCat() {
		return fechaCat;
	}

	public void setFechaCat(String fechaCat) {
		this.fechaCat = fechaCat;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
		importe=parser.parseNextValue("importe");		
		contrato=parser.parseNextValue("numContrato");
		lineaActual=parser.parseNextValue("lineaActual");
		lineaFinal=parser.parseNextValue("lineaFinal");
		fechaCat=parser.parseNextValue("fechaCat");
		cat=parser.parseNextValue("Cat");
		ILCAccount.setAlias(parser.parseNextValue("alias"));
		ILCAccount.setNumber(parser.parseNextValue("numeroTarjeta"));
	}







}
