package com.bancomer.apiilc.gui.controllers;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.apiilc.R;
import com.bancomer.apiilc.gui.delegates.ExitoILCDelegate;
import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.apiilc.tracking.TrackingILC;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;

public class ExitoOfertaILCViewController extends BaseActivity {
	LinearLayout vista;
	private TextView lblTextoOferta;
	private TextView lblTextoImporte;
	private TextView lblTextoAyudaOferta;
	private TextView lblCorreoOferta;
	private EditText editTextemail;
	private TextView lblsmsOferta;
	private EditText editTextNumero;
	private ImageButton btnComprobante;
	private ImageButton btnmMenu;
	private LinearLayout layout_btnpromocion;
	private TextView txtofertasclic;
	private BaseViewsController parentManager;
	private InitILC init = InitILC.getInstance();

	private BaseViewController baseViewController;
	private ExitoILCDelegate delegate;

	//etiquetado Adobe
	String screen = "";
	String screen2 = "";
	ArrayList<String> list= new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		baseViewController = init.getBaseViewController();

		parentManager = init.getParentManager();
		parentManager.setCurrentActivityApp(this);

		delegate = (ExitoILCDelegate) parentManager.getBaseDelegateForKey(ExitoILCDelegate.EXITO_OFERTA_DELEGATE);
		delegate.setController(this);

		baseViewController.setActivity(this);
		baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.layout_bmovil_acepta_oferta_ilc);
		baseViewController.setTitle(this, R.string.bmovil_pantallailc_title, R.drawable.icono_pagar_servicios);
		baseViewController.setDelegate(delegate);
		init.setBaseViewController(baseViewController);

		vista = (LinearLayout) findViewById(R.id.aceptaofertaILC_view_controller_layout);

		relationUI();
		configurarPantalla();
		setData();

		//etiquetado Adobe
		screen = "detalle ilc";
		screen2 = "exito ilc";
		list.add(screen);
		list.add(screen2);
		TrackingILC.trackState("exito ilc", list);
	}

	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (v == btnComprobante) {
				delegate.showResultados();
			} else if (v == btnmMenu) {
				delegate.showMenu();
			}
		}
	};

	private void relationUI() {
		layout_btnpromocion = (LinearLayout) findViewById(R.id.layout_btnpromocion);
		layout_btnpromocion.setVisibility(LinearLayout.GONE);

		lblTextoOferta = (TextView) findViewById(R.id.lblTextoOferta);
		lblTextoImporte = (TextView) findViewById(R.id.lblTextoImporte);
		lblTextoAyudaOferta = (TextView) findViewById(R.id.lblTextoAyudaOferta);
		lblCorreoOferta = (TextView) findViewById(R.id.lblCorreoOferta);
		lblsmsOferta = (TextView) findViewById(R.id.lblsmsOferta);

		editTextNumero = (EditText) findViewById(R.id.editTextNumero);
		editTextemail = (EditText) findViewById(R.id.editTextemail);
		txtofertasclic = (TextView) findViewById(R.id.txtofertasclic);
		btnComprobante = (ImageButton) findViewById(R.id.btnComprobante);
		btnComprobante.setOnClickListener(clickListener);
		btnmMenu = (ImageButton) findViewById(R.id.btnMenu);
		btnmMenu.setOnClickListener(clickListener);

	}

	private void setData(){
		lblTextoImporte.setText(Tools.formatAmount(delegate.getAceptaOfertaILC().getLineaFinal().replace(",", ""), false));

		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(Constants.NUMERO_TELEFONO_LENGTH);
		editTextNumero.setFilters(FilterArray);
		if (delegate.getAceptaOfertaILC().getEmail().compareTo("") == 0) {
			lblTextoAyudaOferta.setText(R.string.bmovil_texto_extoILC3);
			lblCorreoOferta.setVisibility(View.GONE);
			editTextemail.setVisibility(View.GONE);

		} else {

			lblTextoAyudaOferta.setText(R.string.bmovil_texto_extoILC2);
			editTextemail.setText(delegate.getAceptaOfertaILC().getEmail());
			editTextemail.setEnabled(false);
			editTextemail.setFocusable(false);
		}
		String user = init.getSession().getUsername();
		editTextNumero.setText(user);
		editTextNumero.setEnabled(false);

		editTextNumero.setFocusable(false);

	}

	private void configurarPantalla() {
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(lblTextoOferta, true);
		gTools.scale(lblTextoImporte, true);
		gTools.scale(lblTextoAyudaOferta, true);
		gTools.scale(lblCorreoOferta, true);
		gTools.scale(lblsmsOferta, true);
		gTools.scale(editTextemail, true);
		gTools.scale(editTextNumero, true);
		gTools.scale(btnComprobante);
		gTools.scale(btnmMenu);
		gTools.scale(txtofertasclic, true);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		/*if(!init.getParentManager().isActivityChanging())
		{
			//TimerController.getInstance().getSessionCloser().cerrarSesion();
			init.getParentManager().resetRootViewController();
			init.resetInstance();
		}*/

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
		}
		return true;
	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		init.getParentManager().onUserInteraction();
	}
}
