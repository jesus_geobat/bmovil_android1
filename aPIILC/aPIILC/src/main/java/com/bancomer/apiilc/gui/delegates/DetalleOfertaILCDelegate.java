package com.bancomer.apiilc.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import com.bancomer.apiilc.gui.controllers.DetalleILCViewController;
import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.apiilc.io.AuxConectionFactory;
import com.bancomer.apiilc.io.Server;
import com.bancomer.apiilc.models.AceptaOfertaILC;
import com.bancomer.apiilc.models.ILCAccount;
import com.bancomer.apiilc.models.OfertaILC;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.common.model.HostAccounts;

public class DetalleOfertaILCDelegate implements BaseDelegate {

	public static final long DETALLE_OFERTA_DELEGATE = 631453334566765659L;

	AceptaOfertaILC aceptaOfertaILC;
	OfertaILC ofertaILC;
	public String fechaCat="";

	private DetalleILCViewController controller;
	private InitILC init = InitILC.getInstance();


	public void setController(DetalleILCViewController controller) {
		this.controller = controller;
	}

	public DetalleOfertaILCDelegate(OfertaILC ofertaILC){
		init = InitILC.getInstance();
		this.ofertaILC = ofertaILC;

	}

	public OfertaILC getOfertaILC() {
		return ofertaILC;
	}
	

	public void setOfertaILC(OfertaILC ofertaILC) {
		this.ofertaILC = ofertaILC;
	}


	public void realizaOperacion(int idOperacion, BaseViewController baseViewController) {
		if (idOperacion == Server.ACEPTACION_OFERTA){

			String user = init.getSession().getUsername();
			String ium= init.getSession().getIum();
			String cveCamp =  init.getOfertaILC().getCveCamp();

			//Mod 54787
			String montoLineaActual  = getOfertaILC().getLineaActual();
			String importeSolicitado = getOfertaILC().getImporte();
			String montoLineaFinal   = getOfertaILC().getLineaFinal() ;

			//formatoFechaCat();
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion", "aceptacionILCBmovil");
			params.put("numeroTarjeta",getOfertaILC().getILCAccount().getNumber());

			if(montoLineaActual.endsWith(".00"))
				params.put("lineaActual", montoLineaActual.substring(0,montoLineaActual.length()-3));    //Mod 54787
			else
				params.put("lineaActual", montoLineaActual.substring(0,montoLineaActual.length()-2));    //Mod 54787

			if(importeSolicitado.endsWith(".00"))
				params.put("importe", importeSolicitado.substring(0,importeSolicitado.length()-3));      //Mod 54787
			else
				params.put("importe", importeSolicitado.substring(0,importeSolicitado.length()-2));      //Mod 54787

			if(montoLineaFinal.endsWith(".00"))
				params.put("lineaFinal",montoLineaFinal.substring(0, montoLineaFinal.length()-3));        //Mod 54787
			else
				params.put("lineaFinal",montoLineaFinal.substring(0, montoLineaFinal.length()-2));        //Mod 54787

			params.put("cveCamp",cveCamp);
			params.put("numeroCelular", user);
			params.put("IUM", ium);
			params.put("Cat", (getOfertaILC().getCat()).replace(".",","));
			params.put("fechaCat", getOfertaILC().getFechaCat());// con o sin formato?
			Hashtable<String, String> paramTable2  = AuxConectionFactory.aceptaOfertaILC(params);
			doNetworkOperation(Server.ACEPTACION_OFERTA, paramTable2, InitILC.getInstance().getBaseViewController(),true,new AceptaOfertaILC(),Server.isJsonValueCode.ONECLICK);

		}else if(idOperacion == Server.RECHAZO_OFERTA){

			String user = init.getSession().getUsername();
			String ium= init.getSession().getIum();
			String cveCamp =  init.getOfertaILC().getCveCamp();

			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion", "noAceptacionILCBMovil");
			params.put("numeroCelular", user);
			params.put("cveCamp",cveCamp);
			params.put("descripcionMensaje","0002");
			params.put("contrato",getOfertaILC().getContrato());
			params.put("IUM", ium);
			Hashtable<String, String> paramTable2  = AuxConectionFactory.rechazoOfertaILC(params);
			doNetworkOperation(Server.RECHAZO_OFERTA, paramTable2, InitILC.getInstance().getBaseViewController(),true,new Object(),Server.isJsonValueCode.ONECLICK);
		}
	
	}

	@Override
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
		InitILC.getInstance().getBaseSubApp().invokeNetworkOperation(operationId,params,caller,false);
	}
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle,Server.isJsonValueCode isJsonValue) {
		init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false,isJson,handle,isJsonValue);

	}

	@Override
	public void performAction(Object obj) {

	}

	@Override
	public long getDelegateIdentifier() {
		return 0;
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (operationId == Server.ACEPTACION_OFERTA) {
			if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				aceptaOfertaILC = (AceptaOfertaILC) response.getResponse();
				ICommonSession session = init.getSession();
				try {
					session.setPromocion(aceptaOfertaILC.getPromociones());

					DialogInterface.OnClickListener clickListener=new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							init.getParentManager().setActivityChanging(true);
							((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
							init.resetInstance();
						}
					};

					init.getBaseViewController().showInformationAlert(controller, response.getMessageText(), clickListener);
				}catch (NullPointerException e){
					init.getBaseViewController().showInformationAlert(controller, response.getMessageText());
				}
			}else{
				aceptaOfertaILC = (AceptaOfertaILC) response.getResponse();
				ICommonSession session = init.getSession();
				try {
					session.setPromocion(aceptaOfertaILC.getPromociones());
				}catch (NullPointerException e){}
				init.getParentManager().setActivityChanging(true);
				init.showExito(aceptaOfertaILC, ofertaILC);
			}
		}else if(operationId== Server.RECHAZO_OFERTA){
			init.getParentManager().setActivityChanging(true);
            ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
			init.resetInstance();
		}
	}
	
	public AceptaOfertaILC getAceptaOfertaILC() {
		return aceptaOfertaILC;
	}
	public void setAceptaOfertaILC(AceptaOfertaILC aceptaOfertaILC) {
		this.aceptaOfertaILC = aceptaOfertaILC;
	}

	public ArrayList<HostAccounts> cargaCuentasOrigen() {

		cuentaOrigen();
		ArrayList<HostAccounts> accountsArray = new ArrayList<HostAccounts>();

		for (int i=0; i<ofertaILC.getILCAccountILC().size(); i++){
			ILCAccount aux = new ILCAccount();

			aux.setBalance(ofertaILC.getILCAccountILC().get(i).getBalance());
			aux.setNumber(ofertaILC.getILCAccountILC().get(i).getNumber());
			aux.setDate(ofertaILC.getILCAccountILC().get(i).getDate());
			aux.setVisible(ofertaILC.getILCAccountILC().get(i).isVisible());
			aux.setCurrency(ofertaILC.getILCAccountILC().get(i).getCurrency());
			aux.setType(ofertaILC.getILCAccountILC().get(i).getType());
			aux.setAlias(ofertaILC.getILCAccountILC().get(i).getAlias());
			aux.setConcept(ofertaILC.getILCAccountILC().get(i).getConcept());
			aux.setCelularAsociado(ofertaILC.getILCAccountILC().get(i).getCelularAsociado());
			aux.setCodigoCompania(ofertaILC.getILCAccountILC().get(i).getCodigoCompania());
			aux.setDescripcionCompania(ofertaILC.getILCAccountILC().get(i).getDescripcionCompania());
			aux.setFechaUltimaModificacion(ofertaILC.getILCAccountILC().get(i).getFechaUltimaModificacion());
			aux.setIndicadorSPEI(ofertaILC.getILCAccountILC().get(i).getIndicadorSPEI());

			accountsArray.add(aux);
		}
		return accountsArray;
	}

	public void cuentaOrigen(){
		ILCAccount[] ILCAccounts =init.getListaCuentas();
		ArrayList<ILCAccount> ILCAccountILC;

		ILCAccountILC =new ArrayList<ILCAccount>();
		for(int i=0; i< ILCAccounts.length;i++){
			if(ILCAccounts[i].getNumber().compareTo(ofertaILC.getILCAccount().getNumber())==0){
				if(!ILCAccounts[i].getAlias().equals(""))
					ofertaILC.getILCAccount().setAlias(ILCAccounts[i].getAlias());
					ofertaILC.getILCAccount().setBalance(ILCAccounts[i].getBalance());
					ofertaILC.getILCAccount().setType(Constants.CREDIT_TYPE);
					ofertaILC.getILCAccount().setCelularAsociado(ILCAccounts[i].getCelularAsociado());
					ILCAccountILC.add(ofertaILC.getILCAccount());
					ofertaILC.setILCAccountILC(ILCAccountILC);
					break;
			}
		}
		if(ofertaILC.getILCAccountILC()==null){
			ofertaILC.getILCAccount().setType(Constants.CREDIT_TYPE);
			ILCAccountILC.add(ofertaILC.getILCAccount());
			ofertaILC.setILCAccountILC(ILCAccountILC);
		}
	}



	public void formatoFechaCat(){
	try{	
		String[] meses = { "enero", "febrero", "marzo", "abril", "mayo", 
		      "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };		
		String fechaCatM= ofertaILC.getFechaCat();
		String fechaC=fechaCatM.replaceAll("de+", "");
		String fechaCa=fechaC.replaceAll("\\s", "");
		String mesSelec=fechaCa.substring(2,fechaCa.length()-4);
		String dia= fechaCatM.substring(0,2);
		String anio= fechaCatM.substring(fechaCatM.length()-4);
		int mes = -1;
	      for (int i=0; i<meses.length; i++) {
	         if ( meses[i].equalsIgnoreCase(mesSelec ) ) {
	            // El mes coincide con el elemento actual del array
	            mes = i+1;
	            break;
	         }
	      }
	     if(mes==10||mes==11||mes==12){ 
	    	 fechaCat=anio+"-"+mes+"-"+dia;
	     }else{
	    	 fechaCat=anio+"-0"+mes+"-"+dia;
	     }
			}catch(Exception ex){
				if(Server.ALLOW_LOG) Log.e("","error formato"+ex);
		}
	}
	
	public void formatoFechaCatMostrar(){
		try{	
			String[] meses = { "enero", "febrero", "marzo", "abril", "mayo", 
			      "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };		
			String fechaCatM= ofertaILC.getFechaCat();
			int mesS=Integer.parseInt(fechaCatM.substring(5,7));
			String dia= fechaCatM.substring(fechaCatM.length()-2);
			String anio= fechaCatM.substring(0,4);
			
			fechaCat= dia+" de "+meses[mesS-1].toString()+ " de "+anio;
		    
				}catch(Exception ex){
					if(Server.ALLOW_LOG) Log.e("","error formato"+ex);
			}
		}

}
