package com.bancomer.apiilc.gui.controllers;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bancomer.apiilc.R;
import com.bancomer.apiilc.gui.delegates.ExitoCreditDelegate;
import com.bancomer.apiilc.implementations.InitILC;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;

public class CreditExitoILCViewController extends BaseActivity{

    private BaseViewsController parentManager;
    private InitILC init = InitILC.getInstance();
    private BaseViewController baseViewController;
    private ExitoCreditDelegate delegate;



    private TextView lblImporte;
    private ImageView imgModificar;
    private ImageView imgDocumento;
    private TextView edtCorreoCliente;
    private TextView edtNumCelularCliente;
    private ImageButton btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate = (ExitoCreditDelegate) parentManager.getBaseDelegateForKey(ExitoCreditDelegate.EXITO_CREDIT_DELEGATE);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, 0, R.layout.activity_credit_contrato_exito_ilc);

        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        linkUI();
        setViewDate();

    }

    private void linkUI()
    {
        lblImporte = (TextView) findViewById(R.id.lblImporte);
        imgModificar = (ImageView) findViewById(R.id.img_Modificar);
        imgDocumento = (ImageView) findViewById(R.id.img_Documento);
        edtCorreoCliente = (TextView) findViewById(R.id.edtCorreoCliente);
        edtNumCelularCliente = (TextView) findViewById(R.id.edtNumCelularCliente);
        btnCerrar = (ImageButton) findViewById(R.id.btnCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                init.getParentManager().setActivityChanging(true);
                ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                init.resetInstance();
            }
        });
    }


    private void setViewDate()
    {
        String monto = delegate.getAceptaOfertaILC().getLineaFinal();
        lblImporte.setText(Tools.formatAmount(monto, false));
        edtCorreoCliente.setText(init.getSession().getEmail());
        edtCorreoCliente.setFocusable(false);
        String tarjeta = delegate.getAceptaOfertaILC().getNumeroTarjeta();
        edtNumCelularCliente.setText("*****"+tarjeta.substring(tarjeta.length()-5,tarjeta.length()));
        edtNumCelularCliente.setFocusable(false);

    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        init.getParentManager().onUserInteraction();
    }

    @Override
    public void onBackPressed() {
    }
}
