package com.bancomer.apiilc.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.apiilc.io.AuxConectionFactory;
import com.bancomer.apiilc.io.Server;
import com.bancomer.apiilc.models.OfertaILC;

import java.util.Hashtable;

import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by RPEREZ on 23/09/15.
 */
public class ConsultaILCDelegate implements BaseDelegate {

    private OfertaILC ofertaDomiciliacionTDC;
    private InitILC init = InitILC.getInstance();
    private BaseViewController baseViewController;

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false);
    }
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle,Server.isJsonValueCode isJsonValue) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false,isJson,handle,isJsonValue);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

        if (response.getStatus() == ServerResponse.OPERATION_ERROR)
        {


            Log.e("Gets on Error analyze", "ok");
            init.getParentManager().resetRootViewController();
            init.updateHostActivityChangingState();

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
            init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(),response.getMessageText(),listener);
            init.resetInstance();
        }
        else if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)
        {
            Log.e("Gets Successful analyze", "ok");
            init.showDetalle((OfertaILC)response.getResponse());
        }

        Log.e("Gets out of analyze", "ok");
    }

    @Override
    public void performAction(Object obj) {

        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(init.getParentManager().getCurrentViewController());
        baseViewController.setDelegate(this);
        init.setBaseViewController(baseViewController);

        ICommonSession session = init.getSession();
        String user = session.getUsername();
        String ium= session.getIum();
        String cveCamp = init.getOfertaILC().getCveCamp();

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put("operacion", "cDetalleOfertaBMovil");
        params.put("numeroCelular",user );
        params.put("cveCamp", cveCamp);
        params.put("IUM", ium);
        Hashtable<String, String> paramTable2  = AuxConectionFactory.consultaDetalleOferta(params);
        doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA, paramTable2, InitILC.getInstance().getBaseViewController(),true,new OfertaILC(), Server.isJsonValueCode.ONECLICK);


    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
