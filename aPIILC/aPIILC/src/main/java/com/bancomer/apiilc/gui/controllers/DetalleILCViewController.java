package com.bancomer.apiilc.gui.controllers;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.apiilc.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import com.bancomer.apiilc.gui.delegates.DetalleOfertaILCDelegate;
import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.apiilc.io.Server;
import com.bancomer.apiilc.tracking.TrackingILC;

import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.CuentaOrigenController;
import bancomer.api.common.model.HostAccounts;
import bancomer.api.common.timer.TimerController;

public class DetalleILCViewController extends BaseActivity{

	private TextView lblTexto2;
	private TextView lblTexto3;
	private TextView lblTexto4;
	private TextView lblTexto5;
	private TextView lblTexto6;
	private TextView lblTexto7;
	private TextView lblTexto8;	
	private ImageButton btnAceptar;

	LinearLayout vista;
	LinearLayout vistaCtaOrigen;

	public CuentaOrigenController componenteCtaOrigen;

	private BaseViewController baseViewController;
	public BaseViewsController parentManager;
	private InitILC init = InitILC.getInstance();
	private DetalleOfertaILCDelegate delegate;

	//etiquetado Adobe
	String screen = "";
	ArrayList<String> list= new ArrayList<String>();
	Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		baseViewController = init.getBaseViewController();

		parentManager = init.getParentManager();
		parentManager.setCurrentActivityApp(this);

		delegate=(DetalleOfertaILCDelegate) parentManager.getBaseDelegateForKey(DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE);
		delegate.setController(this);

		baseViewController.setActivity(this);
		baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.layout_bmovil_detalleilc);
		baseViewController.setTitle(this, R.string.bmovil_pantallailc_title, R.drawable.icono_pagar_servicios);
		baseViewController.setDelegate(delegate);
		init.setBaseViewController(baseViewController);

		vista = (LinearLayout)findViewById(R.id.detalleofertaILC_view_controller_layout);

		delegate.formatoFechaCatMostrar();

		relationUI();
		setData();
		cargarCuentas();
		configurarPantalla();

		//etiquetado Adobe
		screen = "detalle ilc";
		list.add(screen);
		TrackingILC.trackState("detalle ilc", list);
		
	}
	
	OnClickListener clickListener=new OnClickListener() {
		
		@Override
		public void onClick(View v) {

			Map<String,Object> OperacionMap = new HashMap<String, Object>();
			// TODO Auto-generated method stub
			if(v==btnAceptar){

				OperacionMap.put("evento_realizada","event52");
				OperacionMap.put("&&products","promociones;detalle ilc+exito ilc");
				OperacionMap.put("eVar12","operacion realizada oferta ilc");
				TrackingILC.trackOperacionRealizada(OperacionMap);

				delegate.realizaOperacion(Server.ACEPTACION_OFERTA, init.getBaseViewController());
			}
		}
	};

	public void relationUI(){
		;
		vistaCtaOrigen = (LinearLayout)findViewById(R.id.ofertaILC_cuenta_origen_view);
		lblTexto2 = (TextView) findViewById(R.id.lblTexto2);
		lblTexto3 = (TextView) findViewById(R.id.lblTexto3);
		lblTexto4 = (TextView) findViewById(R.id.lblTexto4);
		lblTexto5 = (TextView) findViewById(R.id.lblTexto5);
		lblTexto6 = (TextView) findViewById(R.id.lblTexto6);
		lblTexto7 = (TextView) findViewById(R.id.lblTexto7);
		lblTexto8 = (TextView) findViewById(R.id.lblTexto8);
	}

	public void setData(){
		lblTexto8.setText(R.string.bmovil_pantallailc_texto1);
		String mystring = Tools.formatAmount(delegate.getOfertaILC().getImporte()+"00",false);
		SpannableString content = new SpannableString(mystring);
		content.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
		lblTexto7.setText(content);
		lblTexto6.setText(R.string.bmovil_pantallailc_texto2);
		lblTexto5.setText(Tools.formatAmount(delegate.getOfertaILC().getLineaFinal() + "00", false));
		lblTexto4.setText(R.string.bmovil_pantallailc_texto3);
		lblTexto3.setText(R.string.bmovil_pantallailc_texto4);
		lblTexto2.setText("CAT " + delegate.getOfertaILC().getCat() + "%" + " Sin I.V.A informativo.\nFecha de cálculo al " + delegate.fechaCat + "\nConsulta términos y condiciones en \nwww.bancomer.com.");
		btnAceptar = (ImageButton)findViewById(R.id.detalleOferta_boton_continuar);
		btnAceptar.setOnClickListener(clickListener);
	}



	private void configurarPantalla() {
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());			
		gTools.scale(lblTexto2, true);
		gTools.scale(lblTexto3, true);
		gTools.scale(lblTexto4, true);
		gTools.scale(lblTexto5, true);
		gTools.scale(lblTexto6, true);
		gTools.scale(lblTexto7, true);
		gTools.scale(lblTexto8, true);
		gTools.scale(btnAceptar);
		gTools.scale(findViewById(R.id.ofertaILC_cuenta_origen_view));
		
	}
	
	@SuppressWarnings("deprecation")
	public void cargarCuentas(){
		ArrayList<HostAccounts> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

		componenteCtaOrigen = new CuentaOrigenController(this, params);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText("");
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
		componenteCtaOrigen.init();
		componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.oneClicDomiTDC);
		vistaCtaOrigen.addView(componenteCtaOrigen);

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {

			paso1OperacionMap.put("evento_paso1", "event46");
			paso1OperacionMap.put("&&products", "promociones;detalle ilc");
			paso1OperacionMap.put("eVar12", "paso1:rechazo oferta ilc");

			TrackingILC.trackPaso1Operacion(paso1OperacionMap);

			delegate.realizaOperacion(Server.RECHAZO_OFERTA, init.getBaseViewController());

		}
		return true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	protected void onPause() {
		super.onPause();
		/*if(!init.getParentManager().isActivityChanging())
		{
			//TimerController.getInstance().getSessionCloser().cerrarSesion();
			init.getParentManager().resetRootViewController();
			init.resetInstance();
		}*/

	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		init.getParentManager().onUserInteraction();
	}

}
