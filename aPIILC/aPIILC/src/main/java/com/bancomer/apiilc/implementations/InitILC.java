package com.bancomer.apiilc.implementations;

import android.app.Activity;
import android.util.Log;

import com.bancomer.apiilc.gui.controllers.CreditExitoILCViewController;
import com.bancomer.apiilc.gui.controllers.DetalleILCViewController;
import com.bancomer.apiilc.gui.controllers.ExitoOfertaILCViewController;
import com.bancomer.apiilc.gui.controllers.ResultadosViewController;
import com.bancomer.apiilc.gui.delegates.ConsultaILCDelegate;
import com.bancomer.apiilc.gui.delegates.DetalleOfertaCreditDelegate;
import com.bancomer.apiilc.gui.delegates.DetalleOfertaILCDelegate;
import com.bancomer.apiilc.gui.delegates.ExitoCreditDelegate;
import com.bancomer.apiilc.gui.delegates.ExitoILCDelegate;
import com.bancomer.apiilc.gui.delegates.ResultadosDelegate;
import com.bancomer.apiilc.io.BaseSubapplication;
import com.bancomer.apiilc.io.Server;
import com.bancomer.apiilc.models.AceptaOfertaILC;
import com.bancomer.apiilc.models.ILCAccount;
import com.bancomer.apiilc.models.OfertaILC;

import org.apache.http.impl.client.DefaultHttpClient;

import bancomer.api.common.commons.IAutenticacion;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.BaseViewsControllerImpl;
import bancomer.api.common.gui.delegates.DelegateBaseOperacion;
import bancomer.api.common.gui.delegates.MainViewHostDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;


public class InitILC {


    /*Components for APIDOMICILIACIONTDC working*/

    private static InitILC mInstance = null;

    private BaseViewController baseViewController;

    private static BaseViewsController parentManager;

    private BaseSubapplication baseSubApp;

    private static DefaultHttpClient client;

    private Promociones ofertaILC;

    private DelegateBaseOperacion delegateOTP;

    private ICommonSession session;

    private IAutenticacion autenticacion;

    private boolean sofTokenStatus;

    private MainViewHostDelegate hostInstance;

    private ILCAccount[] listaCuentas;

    /* End Region */

    /////////////  Getter's & Setter's /////////////////


    public ICommonSession getSession()
    {
        return session;
    }

    public DelegateBaseOperacion getDelegateOTP() {
        return delegateOTP;
    }

    public Promociones getOfertaILC()
    {
        return ofertaILC;
    }

    public BaseViewController getBaseViewController() {
        return baseViewController;
    }

    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }

    public BaseViewsController getParentManager() {
        return parentManager;
    }

    public BaseSubapplication getBaseSubApp() {
        return baseSubApp;
    }

    public IAutenticacion getAutenticacion() {  return autenticacion;  }

    public boolean getSofTokenStatus(){ return sofTokenStatus; }

    public static DefaultHttpClient getClient() {
        return client;
    }

    public ILCAccount[] getListaCuentas() {
        return listaCuentas;
    }

    ///////////// End Region Getter's & Setter's /////////////////

    public void resetInstance() { mInstance = null; }

    public void updateHostActivityChangingState()
    {
        hostInstance.updateActivityChangingState();
    }

    public void isILCFromCredit(boolean value){
        Server.isILCFromCredit = value;
    }

    public static InitILC getInstance(Activity activity, Promociones promocion,DelegateBaseOperacion delegateOTP, ICommonSession session,IAutenticacion autenticacion,boolean softTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance, Account[] listaCuentas, boolean dev, boolean sim, boolean emulator, boolean log)
    {
        if(mInstance == null)
        {
            Log.d("[CGI-Configuracion-Obligatorio] >> ", "[InitTDCAdicional] Se inicializa la instancia del singleton de datos");
            mInstance = new InitILC(activity,promocion,delegateOTP,session,autenticacion,softTokenStatus,client, hostInstance, listaCuentas, dev,  sim,  emulator, log);
        }
        else
            parentManager.setCurrentActivityApp(activity);

        Server.isILCFromCredit = false;
        return mInstance;
    }

    public static InitILC getInstance()
    {
        return mInstance;
    }

    private InitILC(Activity activity, Promociones promocion, DelegateBaseOperacion delegateOTP, ICommonSession session, IAutenticacion autenticacion, boolean sofTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance, Account[] listaCuentas, boolean dev, boolean sim, boolean emulator, boolean log)
    {
        Server.DEVELOPMENT = dev;
        Server.SIMULATION = sim;
        Server.EMULATOR = emulator;
        Server.ALLOW_LOG = log;
        Server.setEnviroment();
        ofertaILC = promocion;
        this.delegateOTP=delegateOTP;
        baseViewController = new BaseViewControllerImpl();
        this.client = client;
        parentManager = new BaseViewsControllerImpl();
        parentManager.setCurrentActivityApp(activity);
        baseSubApp = new BaseSubapplicationImpl(activity, client);
        this.session=session;
        this.autenticacion=autenticacion;
        this.sofTokenStatus = sofTokenStatus;
        this.hostInstance = hostInstance;
        //this.listaCuentas = listaCuentas;
        setAcounts(listaCuentas);

    }

    public void ejecutaAPIILC()
    {
        Log.e("launcher api","ok");
        new ConsultaILCDelegate().performAction(null);
    }

    public void showDetalle(OfertaILC oferta){

        DetalleOfertaILCDelegate delegate = (DetalleOfertaILCDelegate) parentManager.getBaseDelegateForKey(DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE);

        if(delegate != null){
            parentManager.removeDelegateFromHashMap(DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE);
        }

        delegate = new DetalleOfertaILCDelegate(oferta);
        parentManager.addDelegateToHashMap(DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE, delegate);

        parentManager.showViewController(DetalleILCViewController.class,0,false,null,null,parentManager.getCurrentViewController());

    }

    public void showExito(AceptaOfertaILC acepta, OfertaILC ofertaILC){

        ExitoILCDelegate delegate = (ExitoILCDelegate) parentManager.getBaseDelegateForKey(ExitoILCDelegate.EXITO_OFERTA_DELEGATE);
        if(delegate != null){
            parentManager.removeDelegateFromHashMap(ExitoILCDelegate.EXITO_OFERTA_DELEGATE);
        }

        delegate = new ExitoILCDelegate(acepta, ofertaILC);
        parentManager.addDelegateToHashMap(ExitoILCDelegate.EXITO_OFERTA_DELEGATE,delegate);

        parentManager.showViewController(ExitoOfertaILCViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }

    public void showResultadosViewController(DelegateBaseOperacion delegateBaseOperacion, int resIcon, int resTitle) {

        ResultadosDelegate resultadosDelegate = (ResultadosDelegate) parentManager.getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
        if (resultadosDelegate != null) {
            parentManager.removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
        }
        resultadosDelegate = new ResultadosDelegate(delegateBaseOperacion);
        parentManager.addDelegateToHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID,
                resultadosDelegate);
        parentManager.showViewController(ResultadosViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }


    public void setAcounts(Account[] accounts) {
        ILCAccount[] acc = new ILCAccount[accounts.length];

        if (accounts != null){

            for (int i=0; i<accounts.length; i++){
                ILCAccount aux = new ILCAccount();

                aux.setBalance(accounts[i].getBalance());
                aux.setNumber(accounts[i].getNumber());
                aux.setDate(accounts[i].getDate());
                aux.setVisible(accounts[i].isVisible());
                aux.setCurrency(accounts[i].getCurrency());
                aux.setType(accounts[i].getType());
                aux.setAlias(accounts[i].getAlias());
                aux.setConcept(accounts[i].getConcept());
                aux.setCelularAsociado(accounts[i].getCelularAsociado());
                aux.setCodigoCompania(accounts[i].getCodigoCompania());
                aux.setDescripcionCompania(accounts[i].getDescripcionCompania());
                aux.setFechaUltimaModificacion(accounts[i].getFechaUltimaModificacion());
                aux.setIndicadorSPEI(accounts[i].getIndicadorSPEI());
                acc[i]=aux;
                }
            }
        this.listaCuentas = acc;

    }

    public void engagementFromCredit(OfertaILC oferta){

        DetalleOfertaCreditDelegate delegate = (DetalleOfertaCreditDelegate) parentManager.getBaseDelegateForKey(DetalleOfertaCreditDelegate.DETALLE_OFERTA_CREDIT);

        if(delegate != null){
            parentManager.removeDelegateFromHashMap(DetalleOfertaCreditDelegate.DETALLE_OFERTA_CREDIT);
        }

        delegate = new DetalleOfertaCreditDelegate(oferta);
        parentManager.addDelegateToHashMap(DetalleOfertaCreditDelegate.DETALLE_OFERTA_CREDIT, delegate);
        delegate.setController(parentManager.getCurrentViewController());
        getBaseViewController().setActivity(parentManager.getCurrentViewController());
        getBaseViewController().setDelegate(delegate);
        setBaseViewController(getBaseViewController());
        delegate.realizaOperacion(Server.ACEPTACION_OFERTA, getBaseViewController());
        //parentManager.showViewController(CreditBridge.class,0,false,null,null,parentManager.getCurrentViewController());
    }
    public void showExitoCredit(AceptaOfertaILC acepta, OfertaILC ofertaILC){

        ExitoCreditDelegate delegate = (ExitoCreditDelegate) parentManager.getBaseDelegateForKey(ExitoCreditDelegate.EXITO_CREDIT_DELEGATE);

        if(delegate != null){
            parentManager.removeDelegateFromHashMap(ExitoCreditDelegate.EXITO_CREDIT_DELEGATE);
        }

        delegate = new ExitoCreditDelegate(acepta, ofertaILC);
        parentManager.addDelegateToHashMap(ExitoCreditDelegate.EXITO_CREDIT_DELEGATE,delegate);

        parentManager.showViewController(CreditExitoILCViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }


}
