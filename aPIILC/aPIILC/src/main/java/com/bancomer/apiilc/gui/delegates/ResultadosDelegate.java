package com.bancomer.apiilc.gui.delegates;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.util.Log;


import com.bancomer.apiilc.gui.controllers.ResultadosViewController;
import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.apiilc.io.Server;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.gui.delegates.DelegateBaseOperacion;
import com.bancomer.apiilc.R;
import bancomer.api.common.io.ServerResponse;

public class ResultadosDelegate implements DelegateBaseOperacion, BaseDelegate {
	
	public final static long RESULTADOS_DELEGATE_ID = 0x1ef4f4c61ca112bfL;

	private ArrayList<Object> datosLista;
	private DelegateBaseOperacion operationDelegate;
	private int listaOpcionesMenu;
	//One Click
	private boolean isSMS=false;
	private boolean isEmail=false;
	//Termina One Click
	private Boolean frecOpOK = false;

	private InitILC init = InitILC.getInstance();
	
	/**
	 * PendingIntent to tell the SMS app to notify us.
	 */
	private PendingIntent mSentPendingIntent;
	
	/** 
	 * The BroadcastReceiver that we use to listen for the notification back.
	 */
	private BroadcastReceiver mBroadcastReceiver;
	
	private ResultadosViewController resultadosViewController;
	
	public Boolean getFrecOpOK() {
		return frecOpOK;
	}

	public ResultadosViewController getResultadosViewController() {
		return resultadosViewController;
	}

	public void setFrecOpOK(Boolean frecOpOK) {
		this.frecOpOK = frecOpOK;
	}
	
	public ResultadosDelegate(DelegateBaseOperacion operationDelegate) {
		this.operationDelegate = operationDelegate;
		listaOpcionesMenu = ((ExitoILCDelegate)operationDelegate).getOpcionesMenuResultados();
	}
	
	public DelegateBaseOperacion getOperationDelegate() {
		return operationDelegate;
	}
	
	public void setResultadosViewController(ResultadosViewController viewController) {
		this.resultadosViewController = viewController;
	}
	
	public BroadcastReceiver getmBroadcastReceiver() {
		return mBroadcastReceiver;
	}

	public void setmBroadcastReceiver(BroadcastReceiver mBroadcastReceiver) {
		this.mBroadcastReceiver = mBroadcastReceiver;
	}

	public void consultaDatosLista() {
		resultadosViewController.setListaDatos(((ExitoILCDelegate)operationDelegate).getDatosTablaResultados());

	}
	
	public DelegateBaseOperacion consultaOperationDelegate() {
		return operationDelegate;
	}
	
	public void enviaPeticionOperacion() {
		
	}
	
	public void consultaOpcionesMenu() {
		
	}
	
	public void consultaTextoSMS() {
		
	}

	public void enviaSMS() {

		if(operationDelegate instanceof ExitoILCDelegate){
			isSMS=true;
			isEmail=false;
			((ExitoILCDelegate) operationDelegate).setcontroladorExitoILCView(init.getBaseViewController());
			((ExitoILCDelegate) operationDelegate).realizaOperacion(Server.EXITO_OFERTA, init.getBaseViewController(), false, true);
					
		}
		else{
    	String smsText = Tools.removeSpecialCharacters(((ExitoILCDelegate)operationDelegate).getTextoSMS());

		mSentPendingIntent = PendingIntent.getBroadcast(resultadosViewController, 0,  new Intent(Constants.SENT), 0);
        SmsManager smsMgr = SmsManager.getDefault();
        init.getBaseViewController().muestraIndicadorActividad(resultadosViewController, resultadosViewController.getString(R.string.label_information),
				resultadosViewController.getString(R.string.sms_sending));
        String mPhone=init.getSession().getUsername();
        
        if(mBroadcastReceiver == null){
    		mBroadcastReceiver = resultadosViewController.createBroadcastReceiver();
        }
        
        ArrayList<String> messages = smsMgr.divideMessage(smsText);
        for (int i = 0; i < messages.size(); i++) {
		     String text = messages.get(i).trim();
		     if(text.length()>0) {
		    	 if(Server.ALLOW_LOG) Log.d("sms mensaje", text);
	  		     // send the message, passing in the pending intent, sentPI
		    	 smsMgr.sendTextMessage(mPhone, null, text, mSentPendingIntent, null);
		    	 resultadosViewController.registerReceiver(mBroadcastReceiver, new IntentFilter(Constants.SENT));
		     }
	     }
				}
    }

	public void guardaPDF() {
		
	}
	
	public void enviaEmail() {

				if(operationDelegate instanceof ExitoILCDelegate){
					isEmail=true;
					isSMS=false;
					((ExitoILCDelegate) operationDelegate).setcontroladorExitoILCView(init.getBaseViewController());
					((ExitoILCDelegate) operationDelegate).realizaOperacion(Server.EXITO_OFERTA, init.getBaseViewController(), true, false);
				}
	}
	public void guardaFrecuente() {
		// ((BmovilViewsController)resultadosViewController.getParentViewsController()).showAltaFrecuente(operationDelegate);
	}
	
	public void guardaRapido() {
		
	}
	
	public void borraOperacion() {
		
	}
	

	public int getNombreImagenEncabezado() {
		return ((ExitoILCDelegate)operationDelegate).getNombreImagenEncabezado();
	}
	

	public int getTextoEncabezado() {
		return ((ExitoILCDelegate)operationDelegate).getTextoEncabezado();
	}
	

	public String getTextoTituloResultado() {
		return ((ExitoILCDelegate)operationDelegate).getTextoTituloResultado();
	}
	

	public int getColorTituloResultado() {
		return ((ExitoILCDelegate)operationDelegate).getColorTituloResultado();
	}
	

	public String getTextoPantallaResultados() {
		return ((ExitoILCDelegate)operationDelegate).getTextoPantallaResultados();
	}
	

	public String getTituloTextoEspecialResultados() {
		return ((ExitoILCDelegate)operationDelegate).getTituloTextoEspecialResultados();
	}
	

	public String getTextoEspecialResultados() {
		return ((ExitoILCDelegate)operationDelegate).getTextoEspecialResultados();
	}
	

	public int getOpcionesMenuResultados() {
		return listaOpcionesMenu;
	}


	public ArrayList<Object> getDatosTablaResultados() {
		return null;
	}


	public String getTextoSMS() {
		return null;
	}


	public String getTextoAyudaResultados() {
		return ((ExitoILCDelegate)operationDelegate).getTextoAyudaResultados();
	}

	@Override
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {

	}

	//One CLick
		public void analyzeResponse(int operationId, ServerResponse response) {
			if(operationId==Server.EXITO_OFERTA){
				if (response.getStatus() == ServerResponse.OPERATION_ERROR)
				{
					DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					};
					init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(),response.getMessageText(),listener);
					//init.resetInstance();
				}
				else if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
					if (isSMS) {
						init.getBaseViewController().showInformationAlert(resultadosViewController, R.string.bmovil_alert_exitoefi_sms);
					} else if (isEmail) {
						init.getBaseViewController().showInformationAlert(resultadosViewController, R.string.bmovil_alert_exitoefi_email);
					}
				}
			}
		}

	@Override
	public void performAction(Object obj) { }

	@Override
	public long getDelegateIdentifier() {
		return 0;
	}

	@Override
	public String getNumeroCuentaParaRegistroOperacion() {
		return null;
	}

	@Override
	public String loadOtpFromSofttoken(Constants.TipoOtpAutenticacion tipoOTP, DelegateBaseOperacion delegate) {
		return null;
	}
}
