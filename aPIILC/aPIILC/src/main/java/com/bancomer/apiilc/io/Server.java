package com.bancomer.apiilc.io;

import com.bancomer.apiilc.models.AceptaOfertaILC;
import com.bancomer.apiilc.models.OfertaILC;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import bancomer.api.common.commons.NameValuePair;
import bancomer.api.common.io.ParsingException;


/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receives the network operation
 * requests and builds a network operation suitable for ClienteHttp class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponseImpl object, so that the application logic can
 * perform high level checks and processes.
 * 
 *Created by RPEREZ on 14/09/15.
 */

public class Server {
	
	/**
	 * Defines if the application show logs
	 */
	public static boolean ALLOW_LOG = true;

	/**
	 * Defines if the application is running on the emulator or in the device.
	 */
	public static boolean EMULATOR = false;

	/**
	 * Indicates simulation usage.
	 */
	public static boolean SIMULATION = false;

	/**
	 * Indicates if the base URL points to development server or production
	 */
	public static boolean DEVELOPMENT = false;
	// public static boolean DEVELOPMENT = true; //TODO remove for TEST

	/**
	 * Indicates simulation usage.
	 */
	public static final long SIMULATION_RESPONSE_TIME = 5;

	/*
		Verifica si ilc se lanza ILC desde bbvaCredit
	 */

	public static boolean isILCFromCredit;
	/**
	 * Bancomer Infrastructure.
	 */
	public static final int BANCOMER = 0;

	public static int PROVIDER = BANCOMER; // TODO remove for TEST

	// ///////////////////////////////////////////////////////////////////////////
	// Operations identifiers //
	// ///////////////////////////////////////////////////////////////////////////

	/** ILC **/

	public static final int CONSULTA_DETALLE_OFERTA=71;
	public static final int ACEPTACION_OFERTA=72;
	public static final int EXITO_OFERTA=73;
	public static final int RECHAZO_OFERTA=74;


	//one CLick
	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE_ONECLICK = "ONEC001";
	/**
	 *
	 */

	public enum isJsonValueCode{
		ONECLICK, NONE,CONSUMO,DEPOSITOS;
	}

	public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
	//Termina One CLick


	/**
	 * Operation codes.
	 */
	public static final String[] OPERATION_CODES = { "00",
													"cDetalleOfertaBMovil",
													"aceptacionILCBmovil",
													"exitoILC",
													"noAceptacionBMovil"};




	/**
	 * Base URL for providers.
	 */
	public static String[] BASE_URL;

	/**
	 * Path table for provider/operation URLs.
	 */
	public static String[][] OPERATIONS_PATH;

	/**
	 * Operation URLs map.
	 */
	public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

	/**
	 * Operation providers map.
	 */
	public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";


	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";


	//static {
	public static void setEnviroment() {
		if (DEVELOPMENT) {
			setupDevelopmentServer();
		} else {
			setupProductionServer();
		}

		setupOperations(PROVIDER);
	}



	/**
	 * Setup production server paths.
	 */
	private static void setupProductionServer() {
		BASE_URL = new String[] { "https://www.bancomermovil.com",
				// "https://172.17.100.173",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Produccion"
						: "" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "",
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // acepta oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb" // rechazo oferta ilc promociones
				},
				new String[] { "",
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // oferta ilc promociones bmovil
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb" // rechazo oferta ilc promociones

				} };
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupDevelopmentServer() {
		BASE_URL = new String[] {
				"https://www.bancomermovil.net:11443",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Desarrollo"
						: "http://189.254.77.54:8080/servermobile/Servidor/Desarrollo" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "",
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // acepta oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb" // rechazo oferta ilc promocion bmovil
				},
				new String[] { "",
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // acepta oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb" // rechazo oferta ilc promocion bmovil
				} };
	}

	/**
	 * Setup operations for a provider.
	 * 
	 * @param provider
	 *            the provider
	 */
	private static void setupOperations(int provider) {

		setupOperation(CONSULTA_DETALLE_OFERTA, provider);
		setupOperation(ACEPTACION_OFERTA, provider);
		setupOperation(EXITO_OFERTA, provider);
		setupOperation(RECHAZO_OFERTA, provider);

	}

	/**
	 * Setup operation for a provider.
	 * @param operation the operation
	 * @param provider the provider
	 */
	private static void setupOperation(int operation, int provider) {
		int op =0;
		switch (operation){
			case CONSULTA_DETALLE_OFERTA:
				op=1;
				break;
			case ACEPTACION_OFERTA:
				op=2;
				break;
			case EXITO_OFERTA:
				op=3;
				break;
			case RECHAZO_OFERTA:
				op=4;
				break;
		}
		String code = OPERATION_CODES[op];
		OPERATIONS_MAP.put(
				code,
				new StringBuffer(BASE_URL[provider]).append(
						OPERATIONS_PATH[provider][op]).toString());
		OPERATIONS_PROVIDER.put(code, new Integer(provider));
	}

	/**
	 * Get the operation URL.
	 * @param operation the operation id
	 * @return the operation URL
	 */
	public static String getOperationUrl(String operation) {
		String url = (String) OPERATIONS_MAP.get(operation);
		return url;
	}

	/**
	 * Referencia a ClienteHttp, en lugar de HttpInvoker.
	 */
	private ClienteHttp clienteHttp = null;

	
	// protected Context mContext;

	public ClienteHttp getClienteHttp() {
		return clienteHttp;
	}

	public void setClienteHttp(ClienteHttp clienteHttp) {
		this.clienteHttp = clienteHttp;
	}


	
	public Server() {
		clienteHttp = new ClienteHttp();
	}

	/**
	 * perform the network operation identifier by operationId with the
	 * parameters passed.
	 * @param operationId the identifier of the operation
	 * @param params Hashtable with the parameters as key-value pairs.
	 * @return the response from the server
	 * @throws IOException exception
	 * @throws ParsingException exception
	 */
	public ServerResponseImpl doNetworkOperation(int operationId, Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = null;
		long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
		Integer provider = (Integer) OPERATIONS_PROVIDER.get(OPERATION_CODES[operationId]);
		if (provider != null) {
			PROVIDER = provider.intValue();
		}

		switch (operationId) {
			case CONSULTA_DETALLE_OFERTA:
				response = consultaDetalleOferta(params);
				break;
			case ACEPTACION_OFERTA:
				response = aceptaOfertaILC(params);
				break;
			case EXITO_OFERTA:
				response = exitoOfertaILC(params);
				break;
			case RECHAZO_OFERTA:
				response = rechazoOfertaILC(params);
				break;
			/*case CONSULTA_DETALLE_OFERTA_EFI:
				response = consultaDetalleOfertaEFI(params);
				break;
			case ACEPTACION_OFERTA_EFI:
				response = consultaAceptaOfertaEFI(params);
				break;
			case EXITO_OFERTA_EFI:
				response = exitoOfertaEFI(params);
				break;
			case SIMULADOR_EFI:
				response = simuladorEFI(params);
				break;*/
		}

		if (sleep > 0) {
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
			}
		}
		return response;
	}



	private ServerResponseImpl consultaDetalleOferta(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		OfertaILC ofertaILC=new OfertaILC();
		ServerResponseImpl response = new ServerResponseImpl(ofertaILC);
		isjsonvalueCode=isJsonValueCode.ONECLICK;

		String numeroCelular = (String) params.get("numeroCelular");
		String cveCamp = (String) params.get("cveCamp");
		String IUM = (String) params.get("IUM");

		NameValuePair[] parameters = new NameValuePair[3];
		parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[1] = new NameValuePair("cveCamp", cveCamp);
		parameters[2] = new NameValuePair("IUM",IUM);


		if (SIMULATION) {
			String mensaje = "{\"estado\":\"OK\",\"importe\":\"15000\",\"numeroTarjeta\":\"1234123412341234\",\"numContrato\":\"1234123ASDFN123\",\"lineaActual\":\"10000\",\"lineaFinal\":\"25000\",\"fechaCat\":\"2014-03-18\",\"Cat\":\"50.40\",\"alias\":\"\"}";
			this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DETALLE_OFERTA],parameters,response,mensaje, true);
		}else{

			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DETALLE_OFERTA], parameters, response, false, true);

		}
		return response;
	}

	private ServerResponseImpl aceptaOfertaILC(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		AceptaOfertaILC aceptaOfertaILC=new AceptaOfertaILC();
		ServerResponseImpl response = new ServerResponseImpl(aceptaOfertaILC);
		isjsonvalueCode=isJsonValueCode.ONECLICK;

		String numeroTarjeta = (String) params.get("numeroTarjeta");
		String lineaActual = (String) params.get("lineaActual");
		String importe = (String) params.get("importe");
		String lineaFinal = (String) params.get("lineaFinal");
		String cveCamp = (String) params.get("cveCamp");
		String numeroCelular = (String) params.get("numeroCelular");
		String IUM = (String) params.get("IUM");
		String Cat = (String) params.get("Cat");
		String fechaCat = (String) params.get("fechaCat");
		NameValuePair[] parameters = null;
		if(isILCFromCredit) {
			parameters = new NameValuePair[10];
			parameters[9] = new NameValuePair("version", "1");
		}else
			parameters = new NameValuePair[9];
		parameters[0] = new NameValuePair("numeroTarjeta", numeroTarjeta);
		parameters[1] = new NameValuePair("lineaActual", lineaActual);
		parameters[2] = new NameValuePair("importe",importe);
		parameters[3] = new NameValuePair("lineaFinal",lineaFinal);
		parameters[4] = new NameValuePair("cveCamp",cveCamp);
		parameters[5] = new NameValuePair("numeroCelular",numeroCelular);
		parameters[6] = new NameValuePair("IUM",IUM);
		parameters[7] = new NameValuePair("Cat",Cat);
		parameters[8] = new NameValuePair("fechaCat",fechaCat);


		if (SIMULATION) {

			String mensaje;

			//TRAZA OK1
			mensaje = "{\"estado\":\"OK\",\"numeroTarjeta\":\"1234123412341234\",\"lineaActual\":\"10000\",\"importe\":\"15000\",\"lineaFinal\":\"25000\",\"fechaOperacion\":\"12/12/2013\",\"folioInternet\":\"134234abcd\",\"email\":\"doroteo.zapata@gmail.com\",\"hora\":\"13:21:12\",\"beneficiario\":\"Doroteo_Zapata_Lopez\",\"ofertaCruzada\":\"EFI\",\"LO\":{\"cveCamp\":\"0377POIOIU23434\",\"desOferta\":\"Efectivoinmediato\",\"monto\":\"3000\"},\"PM\":\"SI\",\"LP\":{\"campanias\":[{\"cveCamp\":\"0130ABCDEGAS123\",\"desOferta\":\"Incremento de l?ea de credito\",\"monto\":\"50000\"},{\"cveCamp\":\"0377POIOIU23434\",\"desOferta\":\"Efectivoinmediato\",\"monto\":\"3000\"}]}}";

			//TRAZA OK2
			//mensaje = "{\"estado\":\"OK\",\"numeroTarjeta\":\"4931628011058637\",\"lineaActual\":\"4030000\",\"importe\":\"6500000\",\"lineaFinal\":\"10530000\",\"fechaOperacion\":\"11-09-2014\",\"folioInternet\":\"0080051007\",\"email\":\"\",\"hora\":\"16:34:34\",\"beneficiario\":\"ROBERTO GARZO TRECETA\",\"ofertaCruzada\":\"NO\",\"LO\":{\"cveCamp\":\"\",\"desOferta\":\"\",\"monto\":\"\"},\"LP\":[{\"cveCamp\":\"0377CAM2204980002\",\"desOferta\":\"DISPOSICION DE EFECTIVO INMEDIATO EFI\",\"monto\":\"1800000\"}],\"PM\":\"SI\"}";

			//TRAZA ERROR
			//mensaje = "{\"estado\":\"ERROR\",\"codigoMensaje\":\"KNZS1628\",\"descripcionMensaje\":\"SERVICIO FUERA DE SERVICIO.\",\"PM\":\"SI\",\"LP\":{\"campanias\":[{\"cveCamp\":\"0130ABCDEGAS555\",\"desOferta\":\"Incremento de linea de credito\",\"monto\":\"20000\"},{\"cveCamp\":\"0377POIOIU23888\",\"desOferta\":\"Efectivoinmediato\",\"monto\":\"12000\"}]}}";

			this.clienteHttp.simulateOperation(OPERATION_CODES[ACEPTACION_OFERTA],parameters,response, mensaje, true);

		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[ACEPTACION_OFERTA], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponseImpl exitoOfertaILC(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {
		//ResultExitoILC resultExitoILC= new ResultExitoILC();
		ServerResponseImpl response = new ServerResponseImpl();
		isjsonvalueCode=isJsonValueCode.ONECLICK;

		String numeroTarjeta = (String) params.get("numeroTarjeta");
		String lineaActual = (String) params.get("lineaActual");
		String Importe = (String) params.get("importe");
		String lineaFinal = (String) params.get("lineaFinal");
		String numeroCelular = (String) params.get("numeroCelular");
		String email = (String) params.get("email");
		String folioInternet = (String) params.get("folioInternet");
		String Cat = (String) params.get("Cat");
		String fechaCat = (String) params.get("fechaCat");
		String mensajeA = (String) params.get("mensajeA");
		String IUM = (String) params.get("IUM");

		NameValuePair[] parameters = new NameValuePair[11];
		parameters[0] = new NameValuePair("numeroTarjeta", numeroTarjeta);
		parameters[1] = new NameValuePair("lineaActual", lineaActual);
		parameters[2] = new NameValuePair("importe",Importe);
		parameters[3] = new NameValuePair("lineaFinal",lineaFinal);
		parameters[4] = new NameValuePair("numeroCelular",numeroCelular);
		parameters[5] = new NameValuePair("email",email);
		parameters[6] = new NameValuePair("folioInternet",folioInternet);
		parameters[7] = new NameValuePair("Cat",Cat);
		parameters[8] = new NameValuePair("fechaCat",fechaCat);
		parameters[9] = new NameValuePair("mensajeA",mensajeA);
		parameters[10] = new NameValuePair("IUM",IUM);

		if (SIMULATION) {
			String mensaje = "{\"estado\":\"OK\",\"codigoMensaje\":\" KZE1628\",\"descripcionMensaje\":\"ERROR AL ENVIAR SMS.\"}";
			this.clienteHttp.simulateOperation(OPERATION_CODES[EXITO_OFERTA],parameters,response, mensaje, true);
		}else{


			clienteHttp.invokeOperation(OPERATION_CODES[EXITO_OFERTA], parameters, response, false, true);
		}
		return response;
	}



	private ServerResponseImpl rechazoOfertaILC(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {
		ServerResponseImpl response = new ServerResponseImpl();
		isjsonvalueCode=isJsonValueCode.ONECLICK;

		String numeroCelular = (String) params.get("numeroCelular");
		String cveCamp = (String) params.get("cveCamp");
		String descripcionMensaje = (String) params.get("descripcionMensaje");
		String contrato = (String) params.get("contrato");
		String IUM = (String) params.get("IUM");

		NameValuePair[] parameters = new NameValuePair[5];
		parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[1] = new NameValuePair("cveCamp", cveCamp);
		parameters[2] = new NameValuePair("descripcionMensaje",descripcionMensaje);
		parameters[3] = new NameValuePair("contrato",contrato);
		parameters[4] = new NameValuePair("IUM",IUM);


		if (SIMULATION) {
			String mensaje = "{\"estado\":\"OK\",\"descripcionMensaje\":\"OPERACION REALIZADA CORRECTAMENTE\"}";
			this.clienteHttp.simulateOperation(OPERATION_CODES[RECHAZO_OFERTA],parameters,response, mensaje, true);
		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[RECHAZO_OFERTA], parameters, response, false, true);
		}
		return response;
	}


	/**
	 * Cancel the ongoing network operation.
	 */
	public void cancelNetworkOperations() {
		clienteHttp.abort();
	}



}


/********************************                ********************************/
/******************************** GENERIC THINGS ********************************/
/********************************                ********************************/

			/*

			public ServerResponseImpl doNetworkOperationGeneric(String operationCode, HashMap<String, String> params,  ArrayList<String> urlsProd, ArrayList<String> urlsDev, Boolean isJSON, Object responseModel, String simulatedResponse) throws IOException, ParsingException, URISyntaxException {

				ServerResponseImpl response = null;

				if(!containOperationCode(operationCode)){

					Integer lengthZero = OPERATIONS_PATH[0].length;
					Integer lengthOne = OPERATIONS_PATH[1].length;
					String url0 = urlsProd.get(0);
					String url1 = urlsProd.get(1);
					if (DEVELOPMENT) {
						url0 = urlsDev.get(0);
						url1 = urlsDev.get(1);
					}

					OPERATIONS_PATH[0][lengthZero] = url0;
					OPERATIONS_PATH[1][lengthOne] = url1;

					OPERATION_CODES[OPERATION_CODES.length] = operationCode;

					setupOperation(OPERATION_CODES.length, PROVIDER);

				}

				long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
				Integer provider = (Integer) OPERATIONS_PROVIDER.get(operationCode);
				if (provider != null) {
					PROVIDER = provider.intValue();
				}

				response = genericOperation(params, operationCode, isJSON, responseModel, simulatedResponse);

				if (sleep > 0) {
					try {
						Thread.sleep(sleep);
					} catch (InterruptedException e) {
					}
				}

				return response;
			}

			private Boolean containOperationCode(String operation){
				Boolean ret = false;
				for(String op : OPERATION_CODES){
					if(op.equalsIgnoreCase(operation)) ret = true;
				}

				return ret;
			}

			private ServerResponseImpl genericOperation(HashMap<String, String> params, String operationCode, Boolean isJSON, Object responseModel, String simulatedResponse)
					throws IOException, ParsingException, URISyntaxException {

				//FIXME null ?
				ServerResponseImpl response = new ServerResponseImpl(null);

				if (SIMULATION) {

					this.httpClient.simulateOperation(simulatedResponse,(ParsingHandler) response, true);

				}else{

					Iterator<String> it = params.keySet().iterator();
					NameValuePair[] parameters = new NameValuePair[params.keySet().size()];

					Integer cont = 0;
					while(it.hasNext()){
						String key = it.next();
						parameters[cont] = new NameValuePair(key, params.get(key));

						++cont;
					}

					clienteHttp.invokeOperation(operationCode, parameters, response, false, isJSON);
				}

				return response;
			}


			*/

/********************************                ********************************/
/********************************                ********************************/
/********************************                ********************************/



//one Click
/**
 * clase de enum para cambio de url;
 */
			/*public enum isJsonValueCode{
				ONECLICK, NONE,CONSUMO,DEPOSITOS;
			}

			public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
			//Termina One CLick*/