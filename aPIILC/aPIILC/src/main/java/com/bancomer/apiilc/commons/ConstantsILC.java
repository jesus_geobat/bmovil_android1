package com.bancomer.apiilc.commons;


public class ConstantsILC {

    public static final String OK_2 = "Ok_2";

    //---DATOS aceptaOfertaILC()
    public static final String ACEPTAOFERTAILC_OK1 = "aceptaOfertaILC_OK1";
    public static final String ACEPTAOFERTAILC_OK2 = "aceptaOfertaILC_OK2";
    public static final String ACEPTAOFERTAILC_ERROR = "aceptaOfertaILC_ERROR";

    //---DATOS exitoOfertaILC()
    public static final String EXITOOFERTAILC_ERROR = "exitoOfertaILC_ERROR";

    //---DATOS rechazoOfertaILC()
    public static final String RECHAZOOFERTAILC_OK = "rechazoOfertaILC_OK";

    //---DATOS detalleOfertaEFI()
    public static final String CONDETALLEOFERTAEFI_OK = "conDetalleOfertaEFI_OK";

    //---DATOS aceptaOfertaEFI()
    public static final String CONACEPTAOFERTAEFI_OK2 = "conAceptaOfertaEFI_OK2";

    //---DATOS exitoOfertaEFI()
    public static final String EXITOOFERTAEFI_ERROR = "exitoOfertaEFI_ERROR";

    //---DATOS simuladorEFI()
    public static final String SIMULADOREFI_OK = "simuladorEFI_OK";



    public final static int SHOW_MENU_SMS = 2;

    public final static int SHOW_MENU_EMAIL = 4;

    public final static int SHOW_MENU_PDF = 8;

    public final static int SHOW_MENU_FRECUENTE = 16;

    public final static int SHOW_MENU_RAPIDA = 32;

    public final static int SHOW_MENU_BORRAR = 64;






}
