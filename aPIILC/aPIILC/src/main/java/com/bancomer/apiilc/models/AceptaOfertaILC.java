package com.bancomer.apiilc.models;

import com.bancomer.apiilc.io.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

public class AceptaOfertaILC implements ParsingHandler{
	String numeroTarjeta;
	String lineaActual;
	String importe;
	String lineaFinal;
	String fechaOperacion;
	String folioInternet;
	String email;
	String hora;
	String beneficiario;
	String ofertaCruzada;
	Promociones promocion;
	Promociones[] promociones;
	String PM;

	public Promociones getPromocion() {
		return promocion;
	}

	public void setPromocion(Promociones promocion) {
		this.promocion = promocion;
	}

	public Promociones[] getPromociones() {
		return promociones;
	}

	public void setPromociones(Promociones[] promociones) {
		this.promociones = promociones;
	}

	public String getOfertaCruzada() {
		return ofertaCruzada;
	}

	public void setOfertaCruzada(String ofertaCruzada) {
		this.ofertaCruzada = ofertaCruzada;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getLineaActual() {
		return lineaActual;
	}

	public void setLineaActual(String lineaActual) {
		this.lineaActual = lineaActual;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getLineaFinal() {
		return lineaFinal;
	}

	public void setLineaFinal(String lineaFinal) {
		this.lineaFinal = lineaFinal;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getFolioInternet() {
		return folioInternet;
	}

	public void setFolioInternet(String folioInternet) {
		this.folioInternet = folioInternet;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPM() {
		return PM;
	}

	public void setPM(String pM) {
		PM = pM;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		if(parser.hasValue("numeroTarjeta"))
			numeroTarjeta = parser.parseNextValue("numeroTarjeta", false);
		if(parser.hasValue("lineaActual"))
			lineaActual=parser.parseNextValue("lineaActual",false);
		if(parser.hasValue("importe"))
			importe=parser.parseNextValue("importe",false);
		if(parser.hasValue("lineaFinal"))
			lineaFinal=parser.parseNextValue("lineaFinal",false);
		if(parser.hasValue("fechaOperacion"))
			fechaOperacion=parser.parseNextValue("fechaOperacion",false);
		if(parser.hasValue("folioInternet"))
			folioInternet=parser.parseNextValue("folioInternet",false);
		if(parser.hasValue("email"))
			email=parser.parseNextValue("email",false);
		if(parser.hasValue("hora"))
			hora=parser.parseNextValue("hora",false);
		if(parser.hasValue("beneficiario"))
			beneficiario=parser.parseNextValue("beneficiario",false);
		if(parser.hasValue("ofertaCruzada"))
			ofertaCruzada=parser.parseNextValue("ofertaCruzada",false);
		
		PM=parser.parseNextValue("PM");
		try {
			if(ofertaCruzada!=null){
				if(!ofertaCruzada.equals("NO")){
					JSONObject jsonObject= new JSONObject(parser.parseNextValue("LO"));
					promocion= new Promociones();
					String cveCamp=jsonObject.getString("cveCamp");
					promocion.setCveCamp(cveCamp);
					String desOferta= jsonObject.getString("desOferta");
					promocion.setDesOferta(desOferta);
					String monto=jsonObject.getString("monto");
					promocion.setMonto(monto);
				}
			}
			
			if(PM.equals("SI")){
			JSONObject jsonObjectPromociones= new JSONObject(parser.parseNextValue("LP"));			
			JSONArray arrPromocionesJson =jsonObjectPromociones.getJSONArray("campanias");
			ArrayList<Promociones> arrPromociones = new ArrayList<Promociones>();
			for (int i = 0; i < arrPromocionesJson.length(); i++) {
				JSONObject jsonPromocion = (JSONObject) arrPromocionesJson.get(i);
				Promociones promo = new Promociones();
				promo.setCveCamp(jsonPromocion.getString("cveCamp"));
				promo.setDesOferta(jsonPromocion.getString("desOferta"));
				promo.setMonto(jsonPromocion.getString("monto"));
				arrPromociones.add(promo);
			}
			
			this.promociones = arrPromociones.toArray(new Promociones[arrPromociones.size()]);
			}			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			if(Server.ALLOW_LOG) e.printStackTrace();
		}
		
	}

}
