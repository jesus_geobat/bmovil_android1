package com.bancomer.apiilc.gui.delegates;

import com.bancomer.apiilc.gui.controllers.CreditExitoILCViewController;
import com.bancomer.apiilc.implementations.InitILC;
import com.bancomer.apiilc.models.AceptaOfertaILC;
import com.bancomer.apiilc.models.OfertaILC;

import java.util.Hashtable;


import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by OOROZCO on 12/3/15.
 */
public class ExitoCreditDelegate implements BaseDelegate {

    public static final long EXITO_CREDIT_DELEGATE = 201553334566765999L;

    private AceptaOfertaILC aceptaOfertaILC;
    private OfertaILC ofertaILC;
    private CreditExitoILCViewController controller;
    private InitILC init = InitILC.getInstance();
    private BaseViewController viewController;

    public AceptaOfertaILC getAceptaOfertaILC() {
        return aceptaOfertaILC;
    }

    public OfertaILC getOfertaILC() {
        return ofertaILC;
    }

    public ExitoCreditDelegate(AceptaOfertaILC aceptaOfertaILC, OfertaILC ofertaILC){
        this.aceptaOfertaILC= aceptaOfertaILC;
        this.ofertaILC = ofertaILC;
    }

    public void setController(CreditExitoILCViewController controller) {
        this.controller = controller;
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        InitILC.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

    }

    @Override
    public void performAction(Object obj) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }



}
