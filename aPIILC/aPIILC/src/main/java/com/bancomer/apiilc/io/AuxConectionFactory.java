package com.bancomer.apiilc.io;
import java.util.Hashtable;

public class AuxConectionFactory {
    public static Hashtable consultaDetalleOferta(Hashtable<String, ?> params){

        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("cveCamp",validaNull(params.get("cveCamp")));
        paramNew.put("IUM",validaNull(params.get("IUM")));

        return paramNew;
    }

    public static Hashtable aceptaOfertaILC(Hashtable<String, ?> params){

        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        if(Server.isILCFromCredit) {
            paramNew.put("version", "1");
        }
            paramNew.put("numeroTarjeta",validaNull(params.get("numeroTarjeta")));
            paramNew.put("lineaActual",validaNull(params.get("lineaActual")));
            paramNew.put("importe",validaNull(params.get("importe")));
            paramNew.put("lineaFinal",validaNull(params.get("lineaFinal")));
            paramNew.put("cveCamp",validaNull(params.get("cveCamp")));
            paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
            paramNew.put("IUM",validaNull(params.get("IUM")));
            paramNew.put("Cat",validaNull(params.get("Cat")));
            paramNew.put("fechaCat",validaNull(params.get("fechaCat")));

        return paramNew;
    }

    public static Hashtable exitoOfertaILC(Hashtable<String, ?> params){

        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        paramNew.put("numeroTarjeta",validaNull(params.get("numeroTarjeta")));
        paramNew.put("lineaActual",validaNull(params.get("lineaActual")));
        paramNew.put("importe",validaNull(params.get("importe")));
        paramNew.put("lineaFinal",validaNull(params.get("lineaFinal")));
        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("email",validaNull(params.get("email")));
        paramNew.put("folioInternet",validaNull(params.get("folioInternet")));
        paramNew.put("Cat",validaNull(params.get("Cat")));
        paramNew.put("fechaCat",validaNull(params.get("fechaCat")));
        paramNew.put("mensajeA",validaNull(params.get("mensajeA")));
        paramNew.put("IUM",validaNull(params.get("IUM")));

        return paramNew;
    }


    public static Hashtable rechazoOfertaILC(Hashtable<String, ?> params) {

        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("cveCamp",validaNull(params.get("cveCamp")));
        paramNew.put("descripcionMensaje",validaNull(params.get("descripcionMensaje")));
        paramNew.put("contrato",validaNull(params.get("contrato")));
        paramNew.put("IUM",validaNull(params.get("IUM")));

        return paramNew;

    }

    private static String validaNull(Object valor){
        if(null==valor){
            return "";
        }else{
            return String.valueOf(valor);
        }
    }



}
