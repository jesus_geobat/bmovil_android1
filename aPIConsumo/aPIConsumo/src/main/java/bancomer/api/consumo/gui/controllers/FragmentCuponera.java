package bancomer.api.consumo.gui.controllers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import bancomer.api.consumo.R;
import bancomer.api.consumo.models.ListaCupones;

/**
 * Created by isaigarciamoso on 09/09/16.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class FragmentCuponera extends Fragment {

    private ListView listaViewCupones;
    private CuponAdapter<String> adaptador;
    private ListaCupones listaCupones;
    private List<ListaCupones.Cupon> cupon;
    private final String LOG_TAG = "test";
    private static FragmentCuponera fragmentCuponera;
    private static  Bundle bundle;

    public static FragmentCuponera newInstance(Bundle arguments) {
        FragmentCuponera f = new FragmentCuponera();
        if (arguments != null) {
            f.setArguments(arguments);
        }
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.v(LOG_TAG, "onAttach");

    }

    public static FragmentCuponera newInstance(ListaCupones listaCuponesobject) {
        fragmentCuponera = new FragmentCuponera();
        bundle = new Bundle();
        bundle.putSerializable("lista", listaCuponesobject);
        fragmentCuponera.setArguments(bundle);
        return fragmentCuponera;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(LOG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(LOG_TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.cuponera_fragment, container, false);
        listaViewCupones = (ListView)rootView.findViewById(R.id.lista_fragmento);

        final Bundle args = bundle;
        if (args != null) {
            if (args.getSerializable("lista") != null) {
                Log.d("NO SOY NULLO", "sy nullo");
                listaCupones = (ListaCupones) args.getSerializable("lista");
                Log.d("CUPONES ENTRO",listaCupones+"");
                adaptador = new CuponAdapter<String>(getActivity(), listaCupones.getListaCupon());
                listaViewCupones.setAdapter(adaptador);
                Utility.setListViewHeightBasedOnChildren(listaViewCupones);
            }
        }

        bundle = null;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.v(LOG_TAG, "onActivityCreated");

    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.v(LOG_TAG, "onViewStateRestored");
    }

    /*Setter y getter**/

    public ListaCupones getListaCupones() {
        return listaCupones;
    }


    public void setListaCupones(ListaCupones listaCupones) {
        this.listaCupones = listaCupones;
    }
}
