/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package bancomer.api.consumo.io;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import bancomer.api.common.commons.NameValuePair;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.consumo.implementations.InitConsumo;
//import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;


/**
 * TODO: COMENTARIO DE LA CLASE
 *
 * @author BBVA Bancomer, DyD.
 */

public class ClienteHttp {
	
    /**
     * The field separator in the response.
     */
    private static final String PARAMETER_SEPARATOR = "*";
    
    /**
     * Se utiliza un httpclient de Apache Jakarta
     */
    DefaultHttpClient client;
    
    //protected Context mContext;
    
    public DefaultHttpClient getClient() {
		return client;
	}

	public void setClient(DefaultHttpClient client) {
		this.client = client;
	}

	/**
     * Constructor.
     */
    public ClienteHttp() {
    	createClient();
    }
    
    /**
     * Creates a new instance for the client.
     */
    private void createClient(){

        if(InitConsumo.getClient() == null)
            client = new DefaultHttpClient();
        else
            client = InitConsumo.getClient();

        HttpParams params = new BasicHttpParams();
        // Establece el timeout de la conexión y del socket a 30 segundos
        HttpConnectionParams.setConnectionTimeout(params, 30 * 1000);
        HttpConnectionParams.setSoTimeout(params, 30 * 1000);
        client.setParams(params);

    }
    
	/**
	 * Sets the activity context for this delegate
	 * @param context the activity using the delegate
	 */
	//public void setContext(Context context){
	//	mContext = context;
	//}
    
    /**
     * Invoke a network operation.
     * @param operation operation code
     * @param parameters parameters
     * @param handler instance capable of parsing and storing the result
     * @throws IOException
     * @throws ParsingException
     */

    //This gonna be used when new methods TDC Adicional

    public void invokeOperation(String operation, NameValuePair[] parameters, ParsingHandler handler, boolean clearCookies, boolean isJSON)
    		throws IOException, ParsingException, URISyntaxException {
    	
        String url = getUrl(operation);
        String params = null;
		params = getParameterStringWithJSON(operation, parameters);
		if(Server.ALLOW_LOG) Log.e("REQUEST", "params: " + params);
        if(Server.ALLOW_LOG) Log.e("REQUEST", "url: " + url + "?" + params);
        retrieve(url, null, null, handler,isJSON,params);
    }
    
    
    /**
     * Retrieve the response from the server and store it in the ParsingHandler.
     * @param url the URL to invoke
     * @param method the HTTP method
     * @param type the POST body content type
     * @param handler instance capable of parsing the result
     * @throws IOException
     * @throws ParsingException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void retrieve(String url, String type, byte[] body,
    		ParsingHandler handler, boolean isJSON,String params) throws IOException, ParsingException, URISyntaxException {
    	
    	// Realiza la conexión http y manda la cadena de operación
    	BufferedReader in = null;
    	String result = null;
    	
    	try {
	        HttpPost request = new HttpPost();

			request.setURI(new URI(url) );
			// Si la siguiente línea de código esté comentada entonces la aplicación se comunica por la vía 1 .173 cuando esté en producci�n, 
			// Si la línea de código no esté comentada entonces se comunica por la vía 2 .174
			request.addHeader("Lan", "android");
			
			//New line
			
			String data[] = params.split("&");
					
			String firstData[]= data[0].split("=");
			String secondData[]= data[1].split("=");
			String thirdData[]= data[2].split("=");

			List postParams = new ArrayList();
			postParams.add(new BasicNameValuePair(firstData[0],firstData[1]));
			postParams.add(new BasicNameValuePair(secondData[0],secondData[1]));
			postParams.add(new BasicNameValuePair(thirdData[0],thirdData[1]));
			request.setEntity(new UrlEncodedFormEntity(postParams));

			
			HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, "OK");
			
			Log.d("[CGI-Configuracion-Obligatorio]  >> ", client.toString());
				
			response = client.execute(request);
			
	        in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "ISO-8859-1"));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			
			in.close();
			result = sb.toString(); //respuesta
			

			//TODO niko fix
			if(Server.ALLOW_LOG) Log.d("debugRespuesta", result);
			if(Server.ALLOW_LOG) Log.d("debugRespuestaParseada", result.replaceAll("\\<.*?>", ""));
    		
		} catch (Exception e) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Ha ocurrido una excepción en la respuesta.", e);
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					if(Server.ALLOW_LOG) e.printStackTrace();
				}
			}
		}
    	
    	if (result.length() > Tools.TAM_FRAGMENTO_TEXTO) {
    		if(Server.ALLOW_LOG) Log.e("RESULT", "result: " + result);
    	} else {
    		if(Server.ALLOW_LOG) Log.e("RESULT", "result: " + result);
    	}
    	
		if (isJSON) {
			parseJSON(result, handler);
		}else {
			parse(result, handler);
		}
    }

    /**
     * Get the URL to invoke the server.
     * @param operation operation code
     * @return a string with the response
     */
    public static String getUrl(String operation) {
        return Server.getOperationUrl(operation);
    }
    
    public static String getParameterStringWithJSON(String operation, NameValuePair[] parameters) {
    	
    	if(Server.ALLOW_LOG) Log.e("Entra", "aqui con jsOn");
    	
    	//Creamos un objeto de tipo JSON
    	JSONObject parametros = new JSONObject();

    	try {
			parametros.put("operacion",operation);
		} catch (JSONException e1) {
			if(Server.ALLOW_LOG) e1.printStackTrace();
		}
        if (parameters != null) {
            NameValuePair parameter;
            int size = parameters.length;
            for (int i = 0; i < size; i++) {
                parameter = parameters[i];
		        try 
		        {
		        	parametros.put(parameter.getName(), parameter.getValue());
		        }
		        catch (JSONException e)
		        {
		        	if(Server.ALLOW_LOG) e.printStackTrace();
		        }
            }
        }
        
        String params = parametros.toString().replaceAll("\\\\", "");
        //params = encode(params);

        StringBuffer sb = new StringBuffer();

        sb.append(Server.OPERATION_CODE_PARAMETER);
        sb.append('=');



        // Aquí se agrega el código para Consumo o OneClick.
        if(Server.isjsonvalueCode.name()=="ONECLICK")
            sb.append(Server.JSON_OPERATION_CODE_VALUE_ONECLICK);
        else if(Server.isjsonvalueCode.name()=="CONSUMO")
            sb.append(Server.JSON_OPERATION_CODE_VALUE_CONSUMO);
        else if(Server.isjsonvalueCode.name()=="SIMULADOR")
            sb.append(Server.JSON_OPERATION_CODE_VALUE_CONSUMO_CREDIT);
        else if(Server.isjsonvalueCode.name()=="BMOVIL")
            sb.append(Server.JSON_OPERATION_CODE_VALUE);

        sb.append('&');

        sb.append(Server.OPERATION_LOCALE_PARAMETER);
        sb.append('=');
        sb.append(Server.OPERATION_LOCALE_VALUE);

        sb.append('&');

        sb.append(Server.OPERATION_DATA_PARAMETER);
        sb.append('=');
        sb.append(params);


        return sb.toString();

    }

    
    /**
     * URL encoding.
     * @param s the input string
     * @return the encodec string
     */
    public static String encode(String s) {
    	String encoded = s;
        if (s != null) {
            try {
                StringBuffer sb = new StringBuffer(s.length() * 3);
                char c;
                int size = s.length();
                for (int i = 0; i < size; i++) {
                    c = s.charAt(i);
                    if (c == '&') {
                        sb.append("%26");
                    } else if (c == ' ') {
                        sb.append('+');
                    } else if (c == '/'){
                    	sb.append("%2F");
                    } else if ((c >= ',' && c <= ';')
                    		|| (c >= 'A' && c <= 'Z')
                    		|| (c >= 'a' && c <= 'z')
                    		|| c == '_' || c == '?' || c == '*') {
                        sb.append(c);
                    } else {
                        sb.append('%');
                        if (c > 15) {
                            sb.append(Integer.toHexString((int) c));
                        } else {
                            sb.append("0" + Integer.toHexString((int) c));
                        }
                    }
                }
                encoded = sb.toString();
            } catch (Exception ex) {
                encoded = null;
            }
        }
        return (encoded);
    }
    
    /**
     * Read and parse the response.
     * @param response the response from the server
     * @param handler instance capable of parsing the result
     * @throws IOException
     * @throws ParsingException
     */
    private static void parse(String response, ParsingHandler handler) throws IOException, ParsingException {

        Reader reader = null;
        try {
            reader = new StringReader(response);
            Parser parser = new ParserImpl(reader);
            handler.process(parser);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Throwable ignored) {
                }
            }
        }
    }
    
    private static void parseJSON(String response, ParsingHandler handler) throws IOException, ParsingException {

        Reader reader = null;
        try {
            ParserJSON parser = new ParserJSONImpl(response);
            handler.process(parser);
        }
        catch (Exception e)
        {
            Log.w("Fail from ClienteHtpp", e.getLocalizedMessage());
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Throwable ignored) {
                }
            }
        }
    }
    
    
    /**
     * Gets and creates an image from the server, 
     * located in a specific URL
     * 
     * @param url the server image
     * @throws IOException
     */
    public Bitmap getImageFromServer(String url) throws IOException {
    	
    	HttpPost mHttppost = new HttpPost(url);
    	HttpResponse mHttpResponse = client.execute(mHttppost);
    	Bitmap image = null;
    	
    	if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
    	  HttpEntity entity = mHttpResponse.getEntity();
    	    if ( entity != null) {
    	    	 byte[] buffer = EntityUtils.toByteArray(entity);
    	    	 image = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
    	    }
    	}
    	
        return image;
    }

    /**
     * Closes all current connections
     */
    public synchronized void abort(){
    	synchronized (client) {
    		client.getConnectionManager().shutdown();
        	createClient();
		}
    }
    
    /**
     * Simula invocación de operación de Red.
     * @param operation operation code
     * @param parameters parameters
     * @param handler instance capable of parsing the result
     * @param response respuesta simulada
     * @param isJSON define si la respuesta debe ocupar un parser de JSON.
     * @throws IOException
     * @throws ParsingException
     * @throws URISyntaxException
     */
    public void simulateOperation(String operation, NameValuePair[] parameters,
    		ParsingHandler handler, String response, boolean isJSON) throws IOException,
            ParsingException, URISyntaxException {

        String url = getUrl(operation);
        String params;
        //if(isJSON)
        	params = getParameterStringWithJSON(operation, parameters);
        //else
        	//params = getParameterString(operation, parameters);
        char separator = ((url.indexOf('?') == -1) ? '?' : '&');
        url = new StringBuffer(url).append(separator).append(params).toString();
        if(Server.ALLOW_LOG) Log.e("REQUEST", "url: " + url);
        
        if (response.length() > Tools.TAM_FRAGMENTO_TEXTO) {
        	if(Server.ALLOW_LOG) Log.e("RESULT", "result: " + response);
    	} else {
    		if(Server.ALLOW_LOG) Log.e("RESULT", "result: " + response);
    	}
        
        if(isJSON)
        	parseJSON(response, handler);
        else
        	parse(response, handler);
    }
    
    
    /**
     * Simula invocaión de operación de Red.
     * @param operation operation code
     * @param parameters parameters
     * @param handler instance capable of parsing the result
     * @param response respuesta simulada
     * @throws IOException
     * @throws ParsingException
     * @throws URISyntaxException
     */
    public void simulateOperation(String operation, NameValuePair[] parameters,
    		ParsingHandler handler, String response) throws IOException,
            ParsingException, URISyntaxException {
    	simulateOperation(operation, parameters, handler, response, false);
    }

}
