/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package bancomer.api.consumo.io;

import android.util.Log;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import bancomer.api.common.commons.NameValuePair;
import bancomer.api.common.io.ParsingException;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.models.AceptaOfertaConsumo;
import bancomer.api.consumo.models.ConsultaPoliza;
import bancomer.api.consumo.models.OfertaConsumo;
import bancomer.api.consumo.models.ActualizarCuentasResult;


/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receives the network operation
 * requests and builds a network operation suitable for ClienteHttp class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponseImpl object, so that the application logic can
 * perform high level checks and processes.
 * 
 * @author GoNet.
 */

public class Server {
	
	/**
	 * Defines if the application show logs
	 */
	public static boolean ALLOW_LOG = true;

	/**
	 * Defines if the application is running on the emulator or in the device.
	 */
	public static boolean EMULATOR = false;

	/**
	 * Indicates simulation usage.
	 */
	public static boolean SIMULATION = false;

	/**
	 * Indicates if the base URL points to development server or production
	 */
	public static boolean DEVELOPMENT = false;
	// public static boolean DEVELOPMENT = true; //TODO remove for TEST

	/**
	 * Indicates simulation usage.
	 */
	public static final long SIMULATION_RESPONSE_TIME = 5;

	/**
	 * Bancomer Infrastructure.
	 */
	public static final int BANCOMER = 0;

	public static int PROVIDER = BANCOMER; // TODO remove for TEST


	// ///////////////////////////////////////////////////////////////////////////
	// Operations identifiers //
	// ///////////////////////////////////////////////////////////////////////////

    /**
     * One Click Consumo
     **/
    public static final int CONSULTA_DETALLE_OFERTA_CONSUMO = 79;//1
    public static final int POLIZA_OFERTA_CONSUMO = 80;//2
    public static final int TERMINOS_OFERTA_CONSUMO = 81;//3
    public static final int EXITO_OFERTA_CONSUMO = 82;//4
    public static final int DOMICILIACION_OFERTA_CONSUMO = 83;//5
    public static final int CONTRATO_OFERTA_CONSUMO = 84;//6
    public static final int RECHAZO_OFERTA_CONSUMO = 85;//7
    public static final int SMS_OFERTA_CONSUMO = 86;//8
    public static final int CONTRATA_ALTERNATIVA_CONSUMO = 148;//9
    public static final int ACTUALIZAR_CUENTAS = 41;//10
    public static final int OP_AYUDA_PRESTAMO_CONSUMO = 191; //call me back
    public static final int CONSULTA_SIMULADOR_OFERTA_CONSUMO = 79;
    /***
     * Datos agregados para la cuponera
     */
    public static final int CONSULTA_STATUS_CUPONERA = 220;
	public static final int CONSULTA_DETALLE_CUPONERA = 221;
    public static final int ENVIA_SMSYMESSAGE_CUPONERA = 222;
    public static final int CONSULTAR_CREDITOS_CUPONERA= 223;




	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE = "BAN2O05";

	//one CLick
	/**
	 * Operation code parameter for new ops "contrataAlternativaConsumo" CREDIT .
	 */
	public static final String JSON_OPERATION_CODE_VALUE_CONSUMO_CREDIT = "BCRD001";

	//one CLick
	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE_ONECLICK = "ONEC001";
	/**
	 *
	 */
	//one CLick
	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE_CONSUMO = "ONEC002";

	//one Click
	/**
	 * clase de enum para cambio de url;
	 */
	public enum isJsonValueCode{
		ONECLICK, NONE,CONSUMO,SIMULADOR,BMOVIL;
	}

	public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
	//Termina One CLick


    /**
     * Operation codes.
     */
    public static final String[] OPERATION_CODES = {"00", "detalleConsumoBMovil",
			                                              "polizaConsumoBMovil",
                                                          "consultaContratoConsumoBMovil",
                                                          "oneClickBmovilConsumo",
                                                          "consultaDomiciliacionBovedaConsumoBMovil",
                                                          "consultaContratoBovedaConsumoBmovil",
                                                          "noAceptacionBMovil",
                                                          "exitoConsumoBMovil",
                                                          "contrataAlternativaConsumo",
                                                          "actualizarCuentas",
                                                          "ayudaPrestamoConsumo",
                                                          "consultaDetalleCuponera",
			                                              "envioCuponeraConsumo",
                                                          "consultaCuponeraConsumo",
                                                          "consultarCreditos"
    };

	/**
	 * Base URL for providers.
	 */
	public static String[] BASE_URL;

	/**
	 * Path table for provider/operation URLs.
	 */
	public static String[][] OPERATIONS_PATH;

	/**
	 * Operation URLs map.
	 */
	public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

	/**
	 * Operation providers map.
	 */
	public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";


	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";


	//static {
	public static void setEnviroment() {
		if (DEVELOPMENT) {
			setupDevelopmentServer();
		} else {
			setupProductionServer();
		}

		setupOperations(PROVIDER);
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupProductionServer() {
		BASE_URL = new String[] { "https://www.bancomermovil.com",
				// "https://172.17.100.173",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Produccion"
						: "" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "",
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // poliza consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // terminos consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // domiciliacion consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // contrato consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // rechazo consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // sms consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // contrataAlternativaConsumo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OPcallmeback
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // Operación detalle Cuponera
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // Operación enviarMail
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // Operación consulCuponera
						"/mbhxp_mx_web/servlet/ServletOperacionWeb"   // consultar creditos
				},
				new String[] { "",
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // poliza consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // terminos consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // domiciliacion consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // contrato consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // rechazo consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // sms consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // contrataAlternativaConsumo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcallmeback
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // Operación detalle Cuponera
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // Operación enviarMail
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // Operación consulCuponera
						"/mbhxp_mx_web/servlet/ServletOperacionWeb"   // consultar creditos

				} };
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupDevelopmentServer() {
		BASE_URL = new String[] {
				"https://www.bancomermovil.net:11443",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Desarrollo"
						: "http://189.254.77.54:8080/servermobile/Servidor/Desarrollo" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "",
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // poliza oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // terminos oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // domiciliacion oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // contrato oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // rechazo oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // sms oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // contrataAlternativaConsumo
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OPactualizarCuentas
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OPcallmeback
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // Operación detalle Cuponera
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // Operación enviaMail
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // consultaCuponera
						"/eexd_mx_web/servlet/ServletOperacionWeb"  // consultaCreditos
				},
				new String[] { "",
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // poliza oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // terminos oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // domiciliacion oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // contrato oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // rechazo oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // sms oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // contrataAlternativaConsumo
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OPactualizarCuentas
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OPCallmeback
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Operación detalle Cuponera
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // Operación enviaMail
						"/eexd_mx_web/servlet/ServletOperacionWeb",   // consultaCuponera
						"/eexd_mx_web/servlet/ServletOperacionWeb"   // consultaCreditos
				} };
	}

	/**
	 * Setup operations for a provider.
	 * 
	 * @param provider
	 *            the provider
	 */
	private static void setupOperations(int provider) {

		setupOperation(CONSULTA_DETALLE_OFERTA_CONSUMO, provider);
		setupOperation(POLIZA_OFERTA_CONSUMO, provider);
		setupOperation(TERMINOS_OFERTA_CONSUMO, provider);
		setupOperation(EXITO_OFERTA_CONSUMO, provider);
		setupOperation(DOMICILIACION_OFERTA_CONSUMO, provider);
		setupOperation(CONTRATO_OFERTA_CONSUMO, provider);
		setupOperation(RECHAZO_OFERTA_CONSUMO, provider);
		setupOperation(SMS_OFERTA_CONSUMO, provider);
		setupOperation(CONTRATA_ALTERNATIVA_CONSUMO, provider);
		setupOperation(ACTUALIZAR_CUENTAS, provider);
		setupOperation(OP_AYUDA_PRESTAMO_CONSUMO, provider); //call me back
		setupOperation(CONSULTA_DETALLE_CUPONERA,provider); //Detalle CUPONERA
		setupOperation(ENVIA_SMSYMESSAGE_CUPONERA,provider); //Envia sms y correo electronico
		setupOperation(CONSULTA_STATUS_CUPONERA,provider);
		setupOperation(CONSULTAR_CREDITOS_CUPONERA,provider);

	}

	/**
	 * Setup operation for a provider.
	 * @param operation the operation
	 * @param provider the provider
	 */
	private static void setupOperation(int operation, int provider) {
		int op=0;
		switch (operation){
			case CONSULTA_DETALLE_OFERTA_CONSUMO:
				op=1;
				break;
			case POLIZA_OFERTA_CONSUMO:
				op=2;
				break;
			case TERMINOS_OFERTA_CONSUMO:
				op=3;
				break;
			case EXITO_OFERTA_CONSUMO:
				op=4;
				break;
			case DOMICILIACION_OFERTA_CONSUMO:
				op=5;
				break;
			case CONTRATO_OFERTA_CONSUMO:
				op=6;
				break;
			case RECHAZO_OFERTA_CONSUMO:
				op=7;
				break;
			case SMS_OFERTA_CONSUMO:
				op=8;
				break;
			case CONTRATA_ALTERNATIVA_CONSUMO:
				op=9;
				break;
			case ACTUALIZAR_CUENTAS:
				op=10;
				break;
			case OP_AYUDA_PRESTAMO_CONSUMO:
				op=11;
				break;
			case CONSULTA_DETALLE_CUPONERA:
				op=12;
				break;
			case ENVIA_SMSYMESSAGE_CUPONERA:
				op=13;
				break;
			case CONSULTA_STATUS_CUPONERA:
				op= 14;
				break;
			case CONSULTAR_CREDITOS_CUPONERA:
				op= 15;
				break;
		}
		String code = OPERATION_CODES[op];
		OPERATIONS_MAP.put(
				code,
				new StringBuffer(BASE_URL[provider]).append(
						OPERATIONS_PATH[provider][op]).toString());
		OPERATIONS_PROVIDER.put(code, new Integer(provider));
	}

	/**
	 * Get the operation URL.
	 * @param operation the operation id
	 * @return the operation URL
	 */
	public static String getOperationUrl(String operation) {
		String url = (String) OPERATIONS_MAP.get(operation);
		return url;
	}

	/**
	 * Referencia a ClienteHttp, en lugar de HttpInvoker.
	 */
	private ClienteHttp clienteHttp = null;

	
	// protected Context mContext;

	public ClienteHttp getClienteHttp() {
		return clienteHttp;
	}

	public void setClienteHttp(ClienteHttp clienteHttp) {
		this.clienteHttp = clienteHttp;
	}

	public Server() {
		clienteHttp = new ClienteHttp();
	}

	/**
	 * perform the network operation identifier by operationId with the
	 * parameters passed.
	 * @param operationId the identifier of the operation
	 * @param params Hashtable with the parameters as key-value pairs.
	 * @return the response from the server
	 * @throws IOException exception
	 * @throws ParsingException exception
	 */
	public ServerResponseImpl doNetworkOperation(int operationId, Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = null;
		long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
		Integer provider = (Integer) OPERATIONS_PROVIDER.get(OPERATION_CODES[operationId]);
		if (provider != null) {
			PROVIDER = provider.intValue();
		}

		switch (operationId) {

			case CONSULTA_DETALLE_OFERTA_CONSUMO:
				response = consultaDetalleConsumo(params);
				break;
			case POLIZA_OFERTA_CONSUMO:
				response = polizaConsumo(params);
				break;
			case TERMINOS_OFERTA_CONSUMO:
				response = terminosConsumo(params);
				break;
			case EXITO_OFERTA_CONSUMO:
				response = exitoConsumo(params);
				break;
			case DOMICILIACION_OFERTA_CONSUMO:
				response = domiciliacionConsumo(params);
				break;
			case CONTRATO_OFERTA_CONSUMO:
				response = contratoConsumo(params);
				break;
			case RECHAZO_OFERTA_CONSUMO:
				response = rechazoOfertaConsumo(params);
				break;
			case SMS_OFERTA_CONSUMO:
				response = envioSMSOfertaConsumo(params);
				break;
			case CONTRATA_ALTERNATIVA_CONSUMO:
				Log.i("contratoanom","llama metodo contrataAlternativaConsumo");
				response = contrataAlternativaConsumo(params);
				break;
			case ACTUALIZAR_CUENTAS:
				response = actualizarCuentas(params);
				break;
			//call me back
			case OP_AYUDA_PRESTAMO_CONSUMO:
				response = llamameConsumo(params);
				break;
			//Detalle Cuponera
			case CONSULTA_DETALLE_CUPONERA:
				response = llamameConsumo(params);
				break;
			//Envia mail y correo
			case ENVIA_SMSYMESSAGE_CUPONERA:
				response = llamameConsumo(params);
				break;
			case CONSULTA_STATUS_CUPONERA:
				response = llamameConsumo(params);
				break;
			case CONSULTAR_CREDITOS_CUPONERA:
				response = llamameConsumo(params);
				break;
		}

		if (sleep > 0) {
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
			}
		}
		return response;
	}


	private ServerResponseImpl consultaDetalleConsumo(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		OfertaConsumo ofertaConsumo=new OfertaConsumo();
		ServerResponseImpl response = new ServerResponseImpl(ofertaConsumo);

		isjsonvalueCode=isJsonValueCode.CONSUMO;

		String numeroCelular = (String) params.get("numeroCelular");
		String claveCamp = (String) params.get("claveCamp");
		String IUM = (String) params.get("IUM");
        String importePar = (String) params.get("importePar");

		NameValuePair[] parameters = new NameValuePair[4];
		parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[1] = new NameValuePair("claveCamp", claveCamp);
		parameters[2] = new NameValuePair("IUM",IUM);
        parameters[3] = new NameValuePair("importePar", importePar);

		if (SIMULATION) {
			String mensaje;

			mensaje = "{\"estado\":\"OK\",\"plazo\":\"2060\",\"importe\":\"26670000\",\"cuentaVinc\":\"00740010140178686777\",\"plazoDes\":\"60 MESES\",\"Cat\":\"38.58\",\"fechaCat\":\"2014-12-30\",\"pagoMenFijo\":\"988876\",\"estatusOferta\":\"F\",\"folioUG\":\"00740960003863467000\",\"totalPagos\":\"60\",\"descDiasPago\":\"1RO\",\"bscPagar\":\"SEGURO COLECTIVO DE VIDA\",\"impSegSal\":\"528060\",\"tasaAnual\":\"37.1\",\"tasaMensual\":\"3.09\",\"producto\":\"PBCCMPPI01\",\"montoMinimo\":\"200156\"}";

			this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DETALLE_OFERTA_CONSUMO],parameters,response,mensaje, true);

		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DETALLE_OFERTA_CONSUMO], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponseImpl polizaConsumo(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = new ServerResponseImpl(new ConsultaPoliza());

		isjsonvalueCode=isJsonValueCode.CONSUMO;
		Log.i("poliza","poliza 1");

		String IdProducto = (String) params.get("IdProducto");

		NameValuePair[] parameters = new NameValuePair[1];
		parameters[0] = new NameValuePair("IdProducto", IdProducto);

		if (SIMULATION) {
			String mensaje;

			mensaje = "{\"estado\": \"OK\",\"txtHTML1\":\"<html><body><p>Poliza Consumo</p><p>CONTRATO QUE CELEBRAN, POR UNA PARTE, BBVA  \\u0093BANCOMER\\u0094, SOCIEDAD ANONIMA, INSTITUCION DE BANCA MULTIPLE, GRUPO FINANCIERO BBVA BANCOMER, A LA QUE EN LO SUCESIVO SE LE DENOMINARA COMO \\u0093BANCOMER\\u0094, Y POR OTRA LA(S) PERSONA(S) CUYO(S) NOMBRE(S) SE PRECISA(N)EN EL ANEXO DATOS GENERALES DEL CLIENTE DEL PRESENTE INSTRUMENTO, EN ADELANTE \\u0093EL CLIENTE\\u0094, Y CONJUNTAMENTE CON \\u0093BANCOMER\\u0094 COMO LAS PARTES,AL TENOR DE LAS SIGUIENTES:</p></body></html>\"}";
			this.clienteHttp.simulateOperation(OPERATION_CODES[POLIZA_OFERTA_CONSUMO],parameters,response,mensaje, true);

		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[POLIZA_OFERTA_CONSUMO], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponseImpl terminosConsumo(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = new ServerResponseImpl(new ConsultaPoliza());

		Log.i("poliza","poliza 2");

		isjsonvalueCode=isJsonValueCode.CONSUMO;

		String opcion = (String) params.get("opcion");
		String IdProducto = (String) params.get("IdProducto");
		String versionE = (String) params.get("versionE");
		String numeroCelular = (String) params.get("numeroCelular");
		String IUM = (String) params.get("IUM");

		NameValuePair[] parameters = new NameValuePair[5];
		parameters[0] = new NameValuePair("opcion", opcion);
		parameters[1] = new NameValuePair("IdProducto", IdProducto);
		parameters[2] = new NameValuePair("versionE", versionE);
		parameters[3] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[4] = new NameValuePair("IUM", IUM);


		if (SIMULATION) {
			String mensaje;

			//trama terminos
            mensaje = "{\"estado\": \"OK\",\"txtHTML1\":\"<html><body><p>Terminos y condiciones Consumo</p><p>CONTRATO QUE CELEBRAN, POR UNA PARTE, BBVA  \\u0093BANCOMER\\u0094, SOCIEDAD ANONIMA, INSTITUCION DE BANCA MULTIPLE, GRUPO FINANCIERO BBVA BANCOMER, A LA QUE EN LO SUCESIVO SE LE DENOMINARA COMO \\u0093BANCOMER\\u0094, Y POR OTRA LA(S) PERSONA(S) CUYO(S) NOMBRE(S) SE PRECISA(N)EN EL ANEXO DATOS GENERALES DEL CLIENTE DEL PRESENTE INSTRUMENTO, EN ADELANTE \\u0093EL CLIENTE\\u0094, Y CONJUNTAMENTE CON \\u0093BANCOMER\\u0094 COMO LAS PARTES,AL TENOR DE LAS SIGUIENTES:</p></body></html>\"}";
			this.clienteHttp.simulateOperation(OPERATION_CODES[TERMINOS_OFERTA_CONSUMO],parameters,response,mensaje, true);

		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[TERMINOS_OFERTA_CONSUMO], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponseImpl exitoConsumo(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		AceptaOfertaConsumo aceptaofertaConsumo=new AceptaOfertaConsumo();
		ServerResponseImpl response = new ServerResponseImpl(aceptaofertaConsumo);

        String numeroCelular = (String) params.get("numeroCelular");
        String claveCamp = (String) params.get("claveCamp");
        String estatus = (String) params.get("estatus");
        //String IdProductoBuro = (String) params.get("IdProductoBuro");
        String codigoOTP = (String) params.get("codigoOTP");
        String cadenaAutenticacion = (String) params.get("cadenaAutenticacion");
        String seguroObli = (String) params.get("seguroObli");
        String IUM = (String) params.get("IUM");
        //Modif. 63685
        String importePar = (String) params.get("importePar");

        String codigoNIP = (String) params.get("codigoNIP");
        String codigoCVV2 = (String) params.get("codigoCVV2");
        String cveAcceso = (String) params.get("cveAcceso");
        String tarjeta5Dig = (String) params.get("tarjeta5Dig");

		InitConsumo init = InitConsumo.getInstance();
        init.setImporteParcial("");//Doubt about this and about the parameters Array length.

		Log.e("exitoCONSUMO-oferDelSim",String.valueOf(init.getOfertaDelSimulador()));

		NameValuePair[] parameters;
		if(!init.getOfertaDelSimulador()){
			isjsonvalueCode=isJsonValueCode.CONSUMO;

            parameters = new NameValuePair[12];
            parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
            parameters[1] = new NameValuePair("claveCamp", claveCamp);
            parameters[2] = new NameValuePair("estatus", estatus);
            parameters[3] = new NameValuePair("cadenaAutenticacion", cadenaAutenticacion);
            parameters[4] = new NameValuePair("seguroObli", seguroObli);
            parameters[5] = new NameValuePair("IUM", IUM);
            parameters[6] = new NameValuePair("codigoNIP", codigoNIP);
            parameters[7] = new NameValuePair("codigoCVV2", codigoCVV2);
            parameters[8] = new NameValuePair("codigoOTP", codigoOTP);
            parameters[9] = new NameValuePair("cveAcceso", cveAcceso);
            parameters[10] = new NameValuePair("tarjeta5Dig", tarjeta5Dig);
            parameters[11] = new NameValuePair("importePar ", importePar);

		}
		else {
			isjsonvalueCode=isJsonValueCode.SIMULADOR;
            parameters = new NameValuePair[12];
            parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
            parameters[1] = new NameValuePair("claveCamp", claveCamp);
            parameters[2] = new NameValuePair("estatus", estatus);
            parameters[3] = new NameValuePair("cadenaAutenticacion", cadenaAutenticacion);
            parameters[4] = new NameValuePair("seguroObli", seguroObli);
            parameters[5] = new NameValuePair("IUM", IUM);
            parameters[6] = new NameValuePair("codigoNIP", codigoNIP);
            parameters[7] = new NameValuePair("codigoCVV2", codigoCVV2);
            parameters[8] = new NameValuePair("codigoOTP", codigoOTP);
            parameters[9] = new NameValuePair("cveAcceso", cveAcceso);
            parameters[10] = new NameValuePair("tarjeta5Dig", tarjeta5Dig);
            parameters[11] = new NameValuePair("importePar ", importePar);

		}

		if (SIMULATION) {

			String mensaje;

            mensaje = "{\\\"estado\\\":\\\"OK\\\",\\\"productoSal\\\":\\\"96\\\",\\\"desProSal\\\":\\\"C NOMINA\\\",\\\"numeroCredito\\\":\\\"00740960003863467900\\\",\\\"impsegSal\\\":”2656.80”,\\\"montoSal\\\":\\\"273104.40\\\",\\\"tasaSal\\\":\\\"32.48000\\\",\\\"proBovSal\\\":\\\"PBCCMNOM02\\\",\\\"montoMinSal\\\":\\\"2000.00\\\",\\\"plazoSal\\\":\\\"120 QUINCENAS\\\",\\\"tipoPagoSal\\\":\\\"QUINCENAL\\\",\\\"estatusSal\\\":\\\"8\\\",\\\"scoreSal\\\":\\\"DI\\\",\\\"PM\\\":\\\"SI\\\",\\\"LP\\\":{\\\"campanias\\\":[{\\\"cveCamp\\\":\\\"0377CAM2204980003\\\",\\\"desOferta\\\":\\\"DISPOSICION DE EFECTIVO INMEDIATO EFI\\\",\\\"monto\\\":\\\"49609900\\\",\\\"carrusel\\\":\\\"SI\\\"},{\\\"cveCamp\\\":\\\"0060CAM2218770001\\\",\\\"desOferta\\\":\\\"PIDE\\\",\\\"monto\\\":\\\"2050050\\\",\\\"carrusel\\\":\\\"SI\\\"},{\\\"cveCamp\\\":\\\"0130CAM2202100001\\\",\\\"desOferta\\\":\\\"INCREMENTO LINEA BANCARIA TDC BANCARIA\\\",\\\"monto\\\":\\\"1000000\\\",\\\"carrusel\\\":\\\"SI\\\"}]}}";
			if(!init.getOfertaDelSimulador())
				this.clienteHttp.simulateOperation(OPERATION_CODES[EXITO_OFERTA_CONSUMO],parameters,response,mensaje, true);
			else
				this.clienteHttp.simulateOperation(OPERATION_CODES[CONTRATA_ALTERNATIVA_CONSUMO],parameters,response,mensaje, true);

		}else{
			if(!init.getOfertaDelSimulador())
			   clienteHttp.invokeOperation(OPERATION_CODES[EXITO_OFERTA_CONSUMO], parameters, response, false, true);
			else
				clienteHttp.invokeOperation(OPERATION_CODES[CONTRATA_ALTERNATIVA_CONSUMO], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponseImpl domiciliacionConsumo(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		Log.i("poliza","poliza 3");

		ServerResponseImpl response = new ServerResponseImpl(new ConsultaPoliza());

		isjsonvalueCode=isJsonValueCode.CONSUMO;

		String IdProducto = (String) params.get("IdProducto");
		String numeroCelular = (String) params.get("numeroCelular");
		String numCredito = (String) params.get("numCredito");
		String indicad = (String) params.get("indicad");
		String bscPagar = (String) params.get("bscPagar");
		String IUM = (String) params.get("IUM");

		NameValuePair[] parameters = new NameValuePair[6];
		parameters[0] = new NameValuePair("IdProducto", IdProducto);
		parameters[1] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[2] = new NameValuePair("numCredito", numCredito);
		parameters[3] = new NameValuePair("indicad", indicad);
		parameters[4] = new NameValuePair("bscPagar", bscPagar);
		parameters[5] = new NameValuePair("IUM", IUM);

		if (SIMULATION) {
			String mensaje;

			//trama domiciliacion
			mensaje = "{\"estado\": \"OK\",\"txtHTML1\":\"<html><body><p>domiciliacion Consumo</p><p>CONTRATO QUE CELEBRAN, POR UNA PARTE, BBVA  \\u0093BANCOMER\\u0094, SOCIEDAD ANONIMA, INSTITUCION DE BANCA MULTIPLE, GRUPO FINANCIERO BBVA BANCOMER, A LA QUE EN LO SUCESIVO SE LE DENOMINARA COMO \\u0093BANCOMER\\u0094, Y POR OTRA LA(S) PERSONA(S) CUYO(S) NOMBRE(S) SE PRECISA(N)EN EL ANEXO DATOS GENERALES DEL CLIENTE DEL PRESENTE INSTRUMENTO, EN ADELANTE \\u0093EL CLIENTE\\u0094, Y CONJUNTAMENTE CON \\u0093BANCOMER\\u0094 COMO LAS PARTES,AL TENOR DE LAS SIGUIENTES:</p></body></html>\"}" ;
			this.clienteHttp.simulateOperation(OPERATION_CODES[DOMICILIACION_OFERTA_CONSUMO],parameters,response,mensaje, true);

		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[DOMICILIACION_OFERTA_CONSUMO], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponseImpl contratoConsumo(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		Log.i("poliza","poliza 6");

		ServerResponseImpl response = new ServerResponseImpl(new ConsultaPoliza());

		isjsonvalueCode=isJsonValueCode.CONSUMO;

		Log.i("poliza","poliza 5");

		String IdProducto = (String) params.get("IdProducto");
		String numeroCelular = (String) params.get("numeroCelular");
		String numCredito = (String) params.get("numCredito");
		String indicad = (String) params.get("indicad");
		String plazoSal = (String) params.get("plazoSal");
		String IUM = (String) params.get("IUM");

		NameValuePair[] parameters = new NameValuePair[6];
		parameters[0] = new NameValuePair("IdProducto", IdProducto);
		parameters[1] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[2] = new NameValuePair("numCredito", numCredito);
		parameters[3] = new NameValuePair("indicad", indicad);
		parameters[4] = new NameValuePair("plazoSal", plazoSal);
		parameters[5] = new NameValuePair("IUM", IUM);

		if (SIMULATION) {
			String mensaje;

			//trama contrato
			mensaje = "{\"estado\": \"OK\",\"txtHTML1\":\"<html><body><p>contrato Consumo</p><p>CONTRATO QUE CELEBRAN, POR UNA PARTE, BBVA  \\u0093BANCOMER\\u0094, SOCIEDAD ANONIMA, INSTITUCION DE BANCA MULTIPLE, GRUPO FINANCIERO BBVA BANCOMER, A LA QUE EN LO SUCESIVO SE LE DENOMINARA COMO \\u0093BANCOMER\\u0094, Y POR OTRA LA(S) PERSONA(S) CUYO(S) NOMBRE(S) SE PRECISA(N)EN EL ANEXO DATOS GENERALES DEL CLIENTE DEL PRESENTE INSTRUMENTO, EN ADELANTE \\u0093EL CLIENTE\\u0094, Y CONJUNTAMENTE CON \\u0093BANCOMER\\u0094 COMO LAS PARTES,AL TENOR DE LAS SIGUIENTES:</p></body></html>\"}";
			this.clienteHttp.simulateOperation(OPERATION_CODES[CONTRATO_OFERTA_CONSUMO],parameters,response,mensaje, true);

		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[CONTRATO_OFERTA_CONSUMO], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponseImpl rechazoOfertaConsumo(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = new ServerResponseImpl();

		isjsonvalueCode=isJsonValueCode.ONECLICK;

		String numeroCelular = (String) params.get("numeroCelular");
		String cveCamp = (String) params.get("cveCamp");
		String descripcionMensaje = (String) params.get("descripcionMensaje");
		String folioUG = (String) params.get("folioUG");
		String IUM = (String) params.get("IUM");

		NameValuePair[] parameters = new NameValuePair[5];
		parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[1] = new NameValuePair("cveCamp", cveCamp);
		parameters[2] = new NameValuePair("descripcionMensaje",descripcionMensaje);
		parameters[3] = new NameValuePair("folioUG",folioUG);
		parameters[4] = new NameValuePair("IUM",IUM);

		if (SIMULATION) {
			String mensaje;

			//trama rechazo oferta
			mensaje = "{\"estado\":\"OK\",\"descripcionMensaje\":\"OPERACION REALIZADA CORRECTAMENTE\"}";
			this.clienteHttp.simulateOperation(OPERATION_CODES[RECHAZO_OFERTA_CONSUMO],parameters,response,mensaje, true);

		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[RECHAZO_OFERTA_CONSUMO], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponseImpl envioSMSOfertaConsumo(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = new ServerResponseImpl();

		isjsonvalueCode=isJsonValueCode.CONSUMO;

		String numeroCelular = (String) params.get("numeroCelular");
		String numCredito = (String) params.get("numCredito");
		String idProducto = (String) params.get("idProducto");
		String estatusContratacion = (String) params.get("estatusContratacion");
		String cuentaVinc = (String) params.get("cuentaVinc");
		String fechaCat = (String) params.get("fechaCat");
		String Importe = (String) params.get("Importe");
		String plazoDes = (String) params.get("plazoDes");
		String pagoMensualFijo = (String) params.get("pagoMensualFijo");
		String email = (String) params.get("email");
		String Cat = (String) params.get("Cat");
		String mensajeA = (String) params.get("mensajeA");
		String IUM = (String) params.get("IUM");

		NameValuePair[] parameters = new NameValuePair[13];
		parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[1] = new NameValuePair("numCredito", numCredito);
		parameters[2] = new NameValuePair("idProducto", idProducto);
		parameters[3] = new NameValuePair("estatusContratacion",estatusContratacion);
		parameters[4] = new NameValuePair("cuentaVinc", cuentaVinc);
		parameters[5] = new NameValuePair("fechaCat",fechaCat);
		parameters[6] = new NameValuePair("Importe",Importe);
		parameters[7] = new NameValuePair("plazoDes",plazoDes);
		parameters[8] = new NameValuePair("pagoMensualFijo",pagoMensualFijo);
		parameters[9] = new NameValuePair("email",email);
		parameters[10] = new NameValuePair("Cat",Cat);
		parameters[11] = new NameValuePair("mensajeA",mensajeA);
		parameters[12] = new NameValuePair("IUM",IUM);


		if (SIMULATION) {
			String mensaje;

			//trama envio SMS error
			mensaje = "{\"estado\":\"ERROR\",\"codigoMensaje\":\"KZE1310\",\"descripcionMensaje\":\"ERROR AL ENVIAR SMS Y CORREO ELECTRONICO\"}";
			this.clienteHttp.simulateOperation(OPERATION_CODES[SMS_OFERTA_CONSUMO],parameters,response,mensaje, true);

		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[SMS_OFERTA_CONSUMO], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponseImpl contrataAlternativaConsumo(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = new ServerResponseImpl();

		isjsonvalueCode=isJsonValueCode.SIMULADOR;

		String operacion 	 = (String)params.get("operacion");
		String numeroCelular = (String) params.get("numeroCelular");
		String claveCamp	 = (String)params.get("claveCamp");
		String estatus 		 = (String)params.get("estatus");
		String codigoOTP 	 = (String)params.get("codigoOTP");
		String seguroObli 	 = (String)params.get("seguroObli");
		String IUM 			 = (String) params.get("IUM");
		String producto 	 = (String)params.get("producto");
		String libre 		 = (String)params.get("libre");

		NameValuePair[] parameters = new NameValuePair[9];
		parameters[0] = new NameValuePair("operacion", operacion);
		parameters[1] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[2] = new NameValuePair("claveCamp", claveCamp);
		parameters[3] = new NameValuePair("estatus", estatus);
		parameters[4] = new NameValuePair("codigoOTP", codigoOTP);
		parameters[5] = new NameValuePair("seguroObli", seguroObli);
		parameters[6] = new NameValuePair("IUM", IUM);
		parameters[7] = new NameValuePair("producto", producto);
		parameters[8] = new NameValuePair("libre", libre);


		if (SIMULATION) {
			String mensaje;
			Log.i("contratoanom","Respuesta simulada de contratacion ANOM exitosa");

			//trama envio SMS error
			mensaje = "{\"estado\":\"OK\",\"productoSal\":\"96\",\"desProSal\":\"C NOMINA\",\"numeroCredito\":\"00740960003863467900\",\"impsegSal\":”2656.80”,\"montoSal\":\"273104.40\",\"tasaSal\":\"32.48000\",\"proBovSal\":\"PBCCMNOM02\",\"montoMinSal\":\"2000.00\",\"plazoSal\":\"120 QUINCENAS\",\"tipoPagoSal\":\"QUINCENAL\",\"estatusSal\":\"8\",\"scoreSal\":\"DI\",\"PM\":\"SI\",\"LP\":{\"campanias\":[{\"cveCamp\":\"0377CAM2204980003\",\"desOferta\":\"DISPOSICION DE EFECTIVO INMEDIATO EFI\",\"monto\":\"49609900\"},{\"cveCamp\":\"0060CAM2218770001\",\"desOferta\":\"PIDE\",\"monto\":\"2050050\"},{\"cveCamp\":\"0130CAM2202100001\",\"desOferta\":\"INCREMENTO LINEA BANCARIA TDC BANCARIA\",\"monto\":\"1000000\"}]}}";
			this.clienteHttp.simulateOperation(OPERATION_CODES[CONTRATA_ALTERNATIVA_CONSUMO],parameters,response,mensaje, true);

		}else{
			Log.i("contratoanom","No es simuilacion");
			clienteHttp.invokeOperation(OPERATION_CODES[CONTRATA_ALTERNATIVA_CONSUMO], parameters, response, false, true);
		}

		return response;
	}

	/**
	 * Invoca al server para actualizar las cuentas del usuario
	 * @param params
	 * @return
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponseImpl actualizarCuentas(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ActualizarCuentasResult data = new ActualizarCuentasResult();
		ServerResponseImpl response = new ServerResponseImpl(data);

		isjsonvalueCode=isJsonValueCode.BMOVIL;

		NameValuePair[] parameters = new NameValuePair[3];
		parameters[0] = new NameValuePair("numeroTelefono", (String) params.get("numeroTelefono"));
		parameters[1] = new NameValuePair("numeroCliente", (String) params.get("numeroCliente"));
		parameters[2] = new NameValuePair("IUM", (String) params.get("IUM"));


		if (SIMULATION) {
			String mensaje;
			mensaje = "{\"estado\":\"OK\",\"asuntos\":[{\"tipoCuenta\":\"AH\",\"alias\":\"\",\"divisa\":\"MX\",\"asunto\":\"00740010002800101680\",\"saldo\":\"986941\",\"concepto\":\"\",\"visible\":\"S\",\"celularAsociado\":\"\",\"codigoCompania\":\"\",\"descripcionCompania\":\"\",\"fechaUltimaModificacion\":\"\",\"indicadorSPEI\":\"FALSE\"}]}";
			this.clienteHttp.simulateOperation(OPERATION_CODES[ACTUALIZAR_CUENTAS],parameters,response,mensaje, true);
		} else {

			clienteHttp.invokeOperation(OPERATION_CODES[ACTUALIZAR_CUENTAS], parameters, response, false, true);
		}

		return response;
	}

	//call me back

	private ServerResponseImpl llamameConsumo(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {

		OfertaConsumo ofertaConsumo=new OfertaConsumo();
		ServerResponseImpl response = new ServerResponseImpl(ofertaConsumo);

		isjsonvalueCode=isJsonValueCode.CONSUMO;

		String numeroCelular = (String) params.get("numeroCelular");
		String claveCamp = (String) params.get("claveCamp");
		String IUM = (String) params.get("IUM");
		String importePar = (String) params.get("importePar");

		NameValuePair[] parameters = new NameValuePair[4];
		parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[1] = new NameValuePair("claveCamp", claveCamp);
		parameters[2] = new NameValuePair("IUM",IUM);
		parameters[3] = new NameValuePair("importePar", importePar);

		if (SIMULATION) {
			String mensaje;

			// trama exitosa
			mensaje = "{\"estado\":\"OK\"}";
			this.clienteHttp.simulateOperation(OPERATION_CODES[OP_AYUDA_PRESTAMO_CONSUMO],parameters,response,mensaje, true);

		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[OP_AYUDA_PRESTAMO_CONSUMO], parameters, response, false, true);
		}
		return response;
	}

	/**
	 * Cancel the ongoing network operation.
	 */
	public void cancelNetworkOperations() {
		clienteHttp.abort();
	}
}
