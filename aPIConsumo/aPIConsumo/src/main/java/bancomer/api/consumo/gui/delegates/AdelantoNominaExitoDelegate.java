package bancomer.api.consumo.gui.delegates;

import android.util.Log;
import java.util.Hashtable;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.commons.ConsumoSession;
import bancomer.api.consumo.gui.controllers.AdelantoNominaExitoViewController;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.AceptaOfertaConsumo;
import bancomer.api.consumo.models.ActualizarCuentasResult;
import bancomer.api.consumo.models.ConsultaPoliza;
import bancomer.api.consumo.models.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by FHERNANDEZ on 29/02/16.
 */
public class AdelantoNominaExitoDelegate implements BaseDelegate {

    public static final long ADELANTO_NOMINA_EXITO_DELEGATE = 1133426676568L;

    private Constants.Operacion tipoOperacion;
    private AdelantoNominaExitoViewController controller;

    public OfertaConsumo ofertaConsumo;
    public Promociones promocion;
    private AceptaOfertaConsumo aceptaOfertaConsumo;

    public InitConsumo init;
    private String montoSolicitado;

    public String getMontoSolicitado() {
        return montoSolicitado;
    }

    public void setMontoSolicitado(String montoSolicitado) {
        this.montoSolicitado = montoSolicitado;
    }

    public InitConsumo getInit() {
        return init;
    }

    public void setInit(InitConsumo init) {
        this.init = init;
    }

    public OfertaConsumo getOfertaConsumo() {
        return ofertaConsumo;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public Constants.Operacion getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(Constants.Operacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public AdelantoNominaExitoDelegate(Promociones promocion, OfertaConsumo ofertaConsumo, AceptaOfertaConsumo aceptaOfertaConsumo){
        this.ofertaConsumo  = ofertaConsumo;
        this.promocion      = promocion;
        this.aceptaOfertaConsumo = aceptaOfertaConsumo;

        setMontoSolicitado(ofertaConsumo.getImporte());

        Log.i("camon","exitoDelegate, getMontoSolicitado: "+getMontoSolicitado());

        init = InitConsumo.getInstance();

        if(InitConsumo.getmInstance() != null) {
            android.util.Log.i("contrato","delegate en exito, seteo init no nulo");
            setInit(InitConsumo.getInstance());
        }else{
            android.util.Log.i("contrato","init en delegate exito, es nulo");
        }

        android.util.Log.i("contrato","Delegate init: "+init);
    }

    public AdelantoNominaExitoViewController getController() {
        return controller;
    }

    public void setController(AdelantoNominaExitoViewController controller) {
        this.controller = controller;
    }

    public void realizaOperacion(int idOperacion, boolean isSMSEMail) {
        ICommonSession session = init.getSession();

        String ium = session.getIum();
        String numCel= session.getUsername();

        if(idOperacion== Server.SMS_OFERTA_CONSUMO){
            String estatus ="";

            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion", "exitoConsumoBMovil");
            params.put("numeroCelular",numCel);
            params.put("numCredito",aceptaOfertaConsumo.getNumeroCredito());
            Log.w("contratoanom","número de crédito: "+aceptaOfertaConsumo.getNumeroCredito());
            params.put("idProducto",ofertaConsumo.getProducto());

            if(aceptaOfertaConsumo.getEstatusSal().equals("4")&& ofertaConsumo.getEstatusOferta().equals(Constants.PREABPROBADO)){
                estatus="E";
            }else if(!aceptaOfertaConsumo.getEstatusSal().equals("4")&& ofertaConsumo.getEstatusOferta().equals(Constants.PREABPROBADO)){
                estatus="T";
            }else if(ofertaConsumo.getEstatusOferta().equals(Constants.PREFORMALIZADO)){
                estatus="E";
            }

            params.put("estatusContratacion",estatus); //definir de donde se obtendra

            params.put("cuentaVinc",ofertaConsumo.getCuentaVinc());
            params.put("fechaCat",ofertaConsumo.getFechaCat());
            String importe= Tools.formatAmount(ofertaConsumo.getImporte(), false);
            importe=importe.replace("$", "");
            params.put("Importe", importe);
            params.put("plazoDes", ofertaConsumo.getPlazo());
//            System.out.println("Valor plazoDes: "+ aceptaOfertaConsumo.getPlazoSal());
            String pagoMensualFijo= Tools.formatUserAmount(ofertaConsumo.getPlazo());
            params.put("pagoMensualFijo",pagoMensualFijo);
            String email= session.getEmail();
            params.put("email",email);
            params.put("Cat",ofertaConsumo.getCat());

            if(isSMSEMail)
                params.put("mensajeA","011");
            else if(email.equals(""))
                params.put("mensajeA","001");
            else
                params.put("mensajeA","001");

            params.put("IUM",ium);

            doNetworkOperation(Server.SMS_OFERTA_CONSUMO, params, init.getBaseViewController(),true,new Object(),Server.isJsonValueCode.CONSUMO);

        }else if (idOperacion == Server.ACTUALIZAR_CUENTAS){//codigo actualizacion de cuentas

            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("numeroTelefono", session.getUsername());
            params.put("numeroCliente", ((ConsumoSession) session).getClientNumber());
            params.put("IUM", session.getIum());

            doNetworkOperation(Server.ACTUALIZAR_CUENTAS, params, init.getBaseViewController(),true,new ActualizarCuentasResult(),Server.isJsonValueCode.BMOVIL);

        }else if(idOperacion == Server.DOMICILIACION_OFERTA_CONSUMO){

            Log.i("canom", "se consultará domiciliacion");

            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion", "consultaDomiciliacionBovedaConsumoBMovil");
            params.put("IdProducto", aceptaOfertaConsumo.getProBovSal());
            params.put("numeroCelular", numCel);
            params.put("bscPagar", ofertaConsumo.getbscPagar());
            params.put("numCredito", aceptaOfertaConsumo.getNumeroCredito());// de donde se obtiene
            params.put("indicad", "D");
            params.put("IUM", ium);
            Hashtable<String, String> paramTable2  = AuxConectionFactory.domiciliacionConsumo(params);
            doNetworkOperation(Server.DOMICILIACION_OFERTA_CONSUMO, paramTable2, init.getBaseViewController(),true,new ConsultaPoliza(),Server.isJsonValueCode.CONSUMO);

        }else if (idOperacion == Server.CONTRATO_OFERTA_CONSUMO){

            Log.i("canom","se consultará contrato");

            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion", "consultaContratoBovedaConsumoBmovil");
            params.put("IdProducto", aceptaOfertaConsumo.getProBovSal());
            params.put("numeroCelular", numCel);
            params.put("numCredito",aceptaOfertaConsumo.getNumeroCredito());// de donde se obtiene
            params.put("indicad", "C");
            params.put("plazoSal", aceptaOfertaConsumo.getPlazoSal());
            params.put("IUM", ium);
            Hashtable<String, String> paramTable2  = AuxConectionFactory.contratoConsumo(params);
            doNetworkOperation(Server.CONTRATO_OFERTA_CONSUMO, paramTable2, init.getBaseViewController(),true,new ConsultaPoliza(),Server.isJsonValueCode.CONSUMO);

        }
    }


    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle,Server.isJsonValueCode isJsonValue) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false,isJson,handle,isJsonValue);

    }

    @Override
    public void doNetworkOperation(int i, Hashtable<String, ?> hashtable, BaseViewController baseViewController) {
        InitConsumo.getInstance().getBaseSubApp().invokeNetworkOperation(i, hashtable, baseViewController,false);
    }

    @Override
    public void analyzeResponse(int i, ServerResponse serverResponse) {

        Log.i("canom", "analyzeResponse: " + i);

        if(i == Server.DOMICILIACION_OFERTA_CONSUMO){
            Log.i("canom", "analyzeResponse: " + serverResponse.getStatus());
            if (serverResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {

                ConsultaPoliza polizaResponse = (ConsultaPoliza)serverResponse.getResponse();
                init.showformatodomiciliacion(polizaResponse.getTerminosHtml());
            }
        }else if(i == Server.CONTRATO_OFERTA_CONSUMO) {
            Log.i("canom","analyzeResponse: "+serverResponse.getStatus());
            if (serverResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                ConsultaPoliza polizaResponse = (ConsultaPoliza)serverResponse.getResponse();
                init.showContrato(polizaResponse.getTerminosHtml(), 1);
                Log.i("canom", "consulta exitosa de contrato");
            }

        }else if(i == Server.SMS_OFERTA_CONSUMO){
            if (serverResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                this.realizaOperacion(Server.ACTUALIZAR_CUENTAS, false);//operacion de actualizacion de cuentas
            }else if ((serverResponse.getStatus() == ServerResponse.OPERATION_WARNING) ||
                    (serverResponse.getStatus() == ServerResponse.OPERATION_ERROR) ) {
                init.getBaseViewController().showInformationAlert(controller,serverResponse.getMessageText());
            }

        }else if(i==Server.ACTUALIZAR_CUENTAS){  //Actualizacion de cuentas
            if (serverResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                if(serverResponse.getResponse() instanceof ActualizarCuentasResult){
                    ActualizarCuentasResult result = (ActualizarCuentasResult) serverResponse.getResponse();
                    actualizacionDeCuentasExitosa(result.getAsuntos());
                }
            }	//fin successfull
            else if (serverResponse.getStatus() == ServerResponse.OPERATION_WARNING) {
                init.getBaseViewController().showInformationAlert(controller,serverResponse.getMessageText());

            }
        }
    }

    public void exitAnom(){
        init.getParentManager().setActivityChanging(true);
        ((CallBackModule) init.getParentManager().getRootViewController()).returnToPrincipal();
        init.resetInstance();
    }



    private void actualizacionDeCuentasExitosa(Account[] accounts) {
        ((ConsumoSession)init.getSession()).setAccounts(accounts);
    }
    @Override
    public void performAction(Object o) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
