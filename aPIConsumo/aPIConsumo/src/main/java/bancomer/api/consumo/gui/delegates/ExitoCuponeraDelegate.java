package bancomer.api.consumo.gui.delegates;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import java.util.Hashtable;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.gui.controllers.ExitoCuponeraViewController;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.AceptaOfertaConsumo;
import bancomer.api.consumo.models.ModelMessage;

/**
 * Created by isaigarciamoso on 07/09/16.
 * Modified by OmarOrozco on 22/09/16
 */
public class ExitoCuponeraDelegate implements BaseDelegate {

    private ExitoCuponeraViewController controller;
    private AceptaOfertaConsumo aceptaOfertaConsumo;
    public static final long KEY_UNIQUE_DELEGATE_EXITO_CUPONERA = 631451334266723456L;
    private InitConsumo consumo = InitConsumo.getInstance();
    private String numerocredito;
    private int counter=0;
    private Timer timerTask;
    private Handler handler = new Handler();
    private boolean isFromOneClick;

    public ExitoCuponeraDelegate(AceptaOfertaConsumo aceptaOfertaConsumo, boolean isFromOneClick) {
        this.aceptaOfertaConsumo =  aceptaOfertaConsumo;
        this.isFromOneClick = isFromOneClick;
    }
    public ExitoCuponeraDelegate(String numerocredito){
        this.numerocredito = numerocredito;
        this.isFromOneClick = false;
    }
    public ExitoCuponeraDelegate(){

    }
    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {

    }
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle, Server.isJsonValueCode isJsonValue) {
        consumo.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false, isJson, handle, isJsonValue);

    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if(operationId == Server.ENVIA_SMSYMESSAGE_CUPONERA){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

                int sdkVersion = Build.VERSION.SDK_INT;

                if(sdkVersion >= Build.VERSION_CODES.HONEYCOMB)
                {
                    controller.getSplash().setVisibility(View.VISIBLE);
                    controller.getSplash().setAlpha(1f);
                    timerTask = new Timer();
                    handler.postDelayed(timerTask, 300);
                }
                else
                {
                    controller.getSplash().setVisibility(View.VISIBLE);
                    timerTask = new Timer();
                    handler.postDelayed(timerTask, 3000);
                    counter = 10;
                }


            }else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
                controller.getSplash().setVisibility(View.GONE);
                controller.showErrorMessage("No se pudo enviar el correo electrónico.");
            }
        }
    }
    @Override
    public void performAction(Object obj) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

    public ExitoCuponeraViewController getController() {
        return controller;
    }

    public void setController(ExitoCuponeraViewController controller) {
        this.controller = controller;
    }
    public void realizaOperacion(int idOperacion,String mail) {
        String  numeroCredito = "";
        ICommonSession session = consumo.getSession();

        String ium = session.getIum();
        String numCel = session.getUsername();
        String operacion = "envioCuponeraConsumo";
        if(getAceptaOfertaConsumo() != null) {
            if((getAceptaOfertaConsumo().getNumeroCredito()) != null){
                numeroCredito = getAceptaOfertaConsumo().getNumeroCredito();
            }
        }else if(getAceptaOfertaConsumo() == null){
            numeroCredito = this.numerocredito;
        }
        if(idOperacion == Server.ENVIA_SMSYMESSAGE_CUPONERA){
            Hashtable<String, String> params = new Hashtable<String, String>();

            params.put("numeroCelular",numCel);
            params.put("operacion",operacion);
            params.put("IUM",ium);
            params.put("email",mail);
            if(isFromOneClick)
                params.put("numCredito",reEstructuraNumeroCredito(numeroCredito));
            else
                params.put("numCredito",numeroCredito);
            params.put("crCredito",InitConsumo.getInstance().getCrCredito());

            Hashtable<String, String> paramTable2 = AuxConectionFactory.smsMailCuponera(params);
            doNetworkOperation(Server.ENVIA_SMSYMESSAGE_CUPONERA,
                    paramTable2, consumo.getBaseViewController(), true, new ModelMessage(),
                    Server.isjsonvalueCode.CONSUMO);
        }
    }
    public void showMenu() {
        consumo.getParentManager().setActivityChanging(true);
        ((CallBackModule) consumo.getParentManager().getRootViewController()).returnToPrincipal();
        consumo.resetInstance();

    }

    public AceptaOfertaConsumo getAceptaOfertaConsumo() {
        return aceptaOfertaConsumo;
    }
    public void setAceptaOfertaConsumo(AceptaOfertaConsumo aceptaOfertaConsumo) {
        this.aceptaOfertaConsumo = aceptaOfertaConsumo;
    }
    //Da un nuevo formato al numero de credito
    private String reEstructuraNumeroCredito(String numeroCredito) {
        String parte1 = numeroCredito.substring(0, 8);
        String parte3 = numeroCredito.substring(8, 18);
        String parte2 = numeroCredito.substring(18, 20);
        String reStructura = parte1 + parte2 + parte3;
        return reStructura;
    }

    public String getNumerocredito() {
        return numerocredito;
    }

    public void setNumerocredito(String numerocredito) {
        this.numerocredito = numerocredito;
    }



    public class Timer implements  Runnable
    {

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void run() {

            if(counter <10)
            {
                switch (counter)
                {
                    case 0:
                        controller.getSplash().setAlpha(0.9f);
                        break;
                    case 1:
                        controller.getSplash().setAlpha(0.8f);
                        break;
                    case 2:
                        controller.getSplash().setAlpha(0.7f);
                        break;
                    case 3:
                        controller.getSplash().setAlpha(0.6f);
                        break;
                    case 4:
                        controller.getSplash().setAlpha(0.5f);
                        break;
                    case 5:
                        controller.getSplash().setAlpha(0.4f);
                        break;
                    case 6:
                        controller.getSplash().setAlpha(0.3f);
                        break;
                    case 7:
                        controller.getSplash().setAlpha(0.2f);
                        break;
                    case 8:
                        controller.getSplash().setAlpha(0.1f);
                        break;
                    case 9:
                        counter = 0;
                        controller.getSplash().setVisibility(View.GONE);
                        return;

                }

                counter++;
                timerTask = new Timer();
                handler.postDelayed(timerTask, 300);
            }
            else
            {
                controller.getSplash().setVisibility(View.GONE);
            }
        }
    }
}

