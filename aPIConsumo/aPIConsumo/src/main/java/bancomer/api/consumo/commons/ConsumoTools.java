package bancomer.api.consumo.commons;

import android.util.Log;

/**
 * Created by OOROZCO on 1/3/16.
 */
public class ConsumoTools {

    public static String formatCredit(String monto){
        String format = "";
        int aux = monto.length();
        if(aux <= 8) {
            format = monto.substring(0, (aux - 5))
                    + ","
                    + monto.substring((aux - 5), (aux - 2))
                    + "."
                    + monto.substring((aux - 2), monto.length());
        } else if( aux > 8){
            format = monto.substring(0, (aux - 8))
                    + ","
                    + monto.substring((aux - 8) , (aux - 5))
                    + ","
                    + monto.substring((aux - 5), (aux - 2))
                    + "."
                    + monto.substring((aux - 2), aux);
        }
        return format;
    }

    public static String formatAmount(String monto){
        String format = "";
        int aux = monto.length();
        format = monto.substring( 0, (aux - 2))
                + "."
                + monto.substring((aux - 2), monto.length());
        return format;
    }

    public static String formatSplitAmount(String monto){
        String format = "";   // 500.00    228,400.00
        int aux = monto.length(); //6
        String[] parts = monto.split(","); //5,000.00    228
        if(aux <= 6){
            format = parts[0]; //500.00
            System.out.println("Parte cero ----->"+parts[0]);
            Log.d("FORMATO DE DATOS","PARTE CERO"+parts[0]);
        } else if(aux > 6 && aux <= 10) {
            format = parts[0] + parts[1];
        } else if( aux > 10){
            format = parts[0]+parts[1]+parts[2];
        }
        return format;
    }

    public static String formatSplitReverse(String monto){
        String format = "000";
        String parts[] = monto.split("\\.");
        format = parts[0]+parts[1];

        return format;
    }

    public static String getMoneyString(String amount) {

        if ((amount == null) || (amount.length() == 0)) {

            return "0.00";
        }

        StringBuffer decimal = new StringBuffer();

        StringBuffer integer = new StringBuffer();


        // format the decimal part

        int decimalPos = amount.indexOf(".");

        if ((decimalPos < 0) || (decimalPos == amount.length() - 1)) {

            // there is not decimal point or it is the last character of the string

            decimal.append("00");

        } else {

            // get the decimal positions

            String auxDecimal = amount.substring(decimalPos + 1);

            if (auxDecimal.length() >= 2) {

                decimal.append(auxDecimal.substring(0, 2));

            } else {

                // add 0s to complete 2 decimals

                decimal.append(auxDecimal);

                for (int i = decimal.length(); i < 2; i++) {

                    decimal.append("0");

                }
            }
        }


        // format the integer part

        String auxInteger = amount;

        if (decimalPos == 0) {

            auxInteger = "0";

        } else if (decimalPos > 0) {

            auxInteger = amount.substring(0, decimalPos);
        }

        int curPos;

        for (int i = auxInteger.length(); i > 0; i = i - 3) {

            curPos = i - 3;

            if (curPos < 0) {

                curPos = 0;

            }

            if (curPos > 0) {

                integer = new StringBuffer(",").

                        append(auxInteger.substring(curPos, i)).

                        append(integer);
            } else {

                integer = new StringBuffer(auxInteger.

                        substring(0, i)).append(integer);

            }
        }

        StringBuffer result = integer.append(".").append(decimal.toString());

        return "$"+result.toString();
    }
}
