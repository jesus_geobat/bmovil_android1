package bancomer.api.consumo.models;

import java.io.IOException;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by isaigarciamoso on 09/09/16.
 */
public class ModelMessage  implements ParsingHandler {

    private String estado;
    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parserJSON) throws IOException, ParsingException {
        estado = parserJSON.parseNextValue("estado");
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
