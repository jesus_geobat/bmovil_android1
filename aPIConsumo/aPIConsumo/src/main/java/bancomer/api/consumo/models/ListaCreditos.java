package bancomer.api.consumo.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by isaigarciamoso on 07/09/16.
 */
public class ListaCreditos  implements ParsingHandler {

    private  static  final String LISTA_CREDITOS = "listaCreditos";
    private List<Credito> listaCreditos;
    private Credito credito;
    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }
    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        try {
            JSONArray jsonCredito = parser.parseNextValueWithArray(LISTA_CREDITOS);
            listaCreditos = new ArrayList<Credito>();
            for (int i = 0; i < jsonCredito.length(); i++) {
                JSONObject jsonDatos = jsonCredito.getJSONObject(i);
                credito = new Credito(
                        jsonDatos.getString("numeroCredito"),
                        jsonDatos.getString("tipoCredito"),
                        jsonDatos.getString("adeudo"),
                        jsonDatos.getString("estatusCredito"));
                listaCreditos.add(credito);
                System.out.println(""+credito.getNumeroCredito()+" "+credito.getEstatusCredito()+""+credito.getAdeudo());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public class Credito{

        private String numeroCredito;
        private String tipoCredito;
        private String adeudo;
        private String estatusCredito;

        public Credito(String numeroCredito, String tipoCredito, String adeudo, String estatusCredito) {
            this.numeroCredito = numeroCredito;
            this.tipoCredito = tipoCredito;
            this.adeudo = adeudo;
            this.estatusCredito = estatusCredito;
        }
        public  Credito(){

        }
        public String getNumeroCredito() {
            return numeroCredito;
        }

        public void setNumeroCredito(String numeroCredito) {
            this.numeroCredito = numeroCredito;
        }

        public String getTipoCredito() {
            return tipoCredito;
        }

        public void setTipoCredito(String tipoCredito) {
            this.tipoCredito = tipoCredito;
        }

        public String getAdeudo() {
            return adeudo;
        }

        public void setAdeudo(String adeudo) {
            this.adeudo = adeudo;
        }

        public String getEstatusCredito() {
            return estatusCredito;
        }

        public void setEstatusCredito(String estatusCredito) {
            this.estatusCredito = estatusCredito;
        }
    }

    public List<Credito> getListaCreditos() {
        return listaCreditos;
    }

    public void setListaCreditos(List<Credito> listaCreditos) {
        this.listaCreditos = listaCreditos;
    }

    public Credito getCredito() {
        return credito;
    }

    public void setCredito(Credito credito) {
        this.credito = credito;
    }
}
