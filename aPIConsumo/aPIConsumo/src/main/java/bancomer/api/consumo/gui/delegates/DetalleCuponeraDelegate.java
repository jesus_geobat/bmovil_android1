package bancomer.api.consumo.gui.delegates;

import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.gui.controllers.DetalleCuponeraViewController;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.AceptaOfertaConsumo;
import bancomer.api.consumo.models.ListaCupones;
import bancomer.api.consumo.models.OfertaConsumo;

/**
 * Created by isaigarciamoso on 19/08/16.
 */
public class DetalleCuponeraDelegate implements BaseDelegate {

    public static final long KEY_UNIQUE_DELEGATE_DETALLE_CUPONERA = 631451334266712345L;
    private InitConsumo init;
    private ListaCupones listaCupones;
    private ListaCupones.Cupon cupon;
    private DetalleCuponeraViewController controller;
    private AceptaOfertaConsumo aceptaOferta;
    private OfertaConsumo ofertaConsumo;

    public DetalleCuponeraDelegate(ListaCupones listaCupones, ListaCupones.Cupon cupon, AceptaOfertaConsumo aceptaOferta) {
        init = InitConsumo.getInstance();
        this.listaCupones = listaCupones;
        this.cupon = cupon;
        this.aceptaOferta = aceptaOferta;
    }
    public DetalleCuponeraDelegate(ListaCupones listaCupones, ListaCupones.Cupon cupon, AceptaOfertaConsumo aceptaOferta,OfertaConsumo ofertaConsumo) {
        init = InitConsumo.getInstance();
        this.listaCupones = listaCupones;
        this.cupon = cupon;
        this.aceptaOferta = aceptaOferta;
        this.ofertaConsumo = ofertaConsumo;
    }

    public DetalleCuponeraDelegate(ListaCupones listaCupones, AceptaOfertaConsumo aceptaOferta) {
        this.listaCupones = listaCupones;
        this.aceptaOferta = aceptaOferta;
    }
    public DetalleCuponeraDelegate() {
    }

    public void setController(DetalleCuponeraViewController controller) {
        this.controller = controller;
    }


    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {

    }

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle, Server.isJsonValueCode isJsonValue) {
        InitConsumo.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false, isJson, handle, isJsonValue);

    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

    }

    @Override
    public void performAction(Object obj) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }


    public InitConsumo getInit() {
        return init;
    }

    public void setInit(InitConsumo init) {
        this.init = init;
    }

    public ListaCupones getListaCupones() {
        return listaCupones;
    }

    public void setListaCupones(ListaCupones listaCupones) {
        this.listaCupones = listaCupones;
    }

    public ListaCupones.Cupon getCupon() {
        return cupon;
    }

    public void setCupon(ListaCupones.Cupon cupon) {
        this.cupon = cupon;
    }

    public DetalleCuponeraViewController getController() {
        return controller;
    }


    public AceptaOfertaConsumo getAceptaOferta() {
        return aceptaOferta;
    }

    public void setAceptaOferta(AceptaOfertaConsumo aceptaOferta) {
        this.aceptaOferta = aceptaOferta;
    }

    public OfertaConsumo getOfertaConsumo() {
        return ofertaConsumo;
    }

    public void setOfertaConsumo(OfertaConsumo ofertaConsumo) {
        this.ofertaConsumo = ofertaConsumo;
    }
}
