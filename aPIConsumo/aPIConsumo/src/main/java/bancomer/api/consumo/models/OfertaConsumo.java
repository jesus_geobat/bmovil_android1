package bancomer.api.consumo.models;

import java.io.IOException;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;


public class OfertaConsumo implements ParsingHandler {

    String plazo;
    String importe;
    String cuentaVinc;
    String plazoDes;
    String Cat;
    String fechaCat;
    String pagoMenFijo;
    String estatusOferta;
    String folioUG;
    String totalPagos;
    String descDiasPago;
    String bscPagar;
    String impSegSal;
    String tasaAnual;
    String tasaMensual;
    String producto;
    String montoMinimo;
	String montoMaximo;
	String tasaRevire;
	String cveProd;
	String tasa;
	String tasaOri;
	String IndRev;

	public String getIndRev() {
		return IndRev;
	}

	public void setIndRev(String indRev) {
		IndRev = indRev;
	}

	public String getTasa() {
		return tasa;
	}

	public void setTasa(String tasa) {
		this.tasa = tasa;
	}

	public String getTasaOri() {
		return tasaOri;
	}

	public void setTasaOri(String tasaOri) {
		this.tasaOri = tasaOri;
	}

	public String getCveProd() {
		return cveProd;
	}

	public void setCveProd(String cveProd) {
		this.cveProd = cveProd;
	}

   	public String getTasaRevire() {
		return tasaRevire;
	}

	public void setTasaRevire(String tasaRevire) {
		this.tasaRevire = tasaRevire;
	}

	public String getPlazo() {
		return plazo;
	}

	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getCuentaVinc() {
		return cuentaVinc;
	}

	public void setCuentaVinc(String cuentaVinc) {
		this.cuentaVinc = cuentaVinc;
	}

	public String getCat() {
		return Cat;
	}

	public void setCat(String cat) {
		Cat = cat;
	}

	public String getFechaCat() {
		return fechaCat;
	}

	public void setFechaCat(String fechaCat) {
		this.fechaCat = fechaCat;
	}

	public String getPagoMenFijo() {
		return pagoMenFijo;
	}

	public void setPagoMenFijo(String pagoMenFijo) {
		this.pagoMenFijo = pagoMenFijo;
	}

	public String getEstatusOferta() {
		return estatusOferta;
	}

	public void setEstatusOferta(String estatusOferta) {
		this.estatusOferta = estatusOferta;
	}

	public String getFolioUG() {
		return folioUG;
	}

	public void setFolioUG(String folioUG) {
		this.folioUG = folioUG;
	}

	public String getDescDiasPago() {
		return descDiasPago;
	}

	public void setDescDiasPago(String descDiasPago) {
		this.descDiasPago = descDiasPago;
	}

	public String getbscPagar() {
		return bscPagar;
	}

	public void setTipoSeg(String bscPagar) {
		this.bscPagar = bscPagar;
	}

	public String getTasaAnual() {
		return tasaAnual;
	}

	public void setTasaAnual(String tasaAnual) {
		this.tasaAnual = tasaAnual;
	}

	public String getTasaMensual() {
		return tasaMensual;
	}

	public void setTasaMensual(String tasaMensual) {
		this.tasaMensual = tasaMensual;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getPlazoDes() {
		return plazoDes;
	}

	public void setPlazoDes(String plazoDes) {
		this.plazoDes = plazoDes;
	}

	public String getImpSegSal() {
		return impSegSal;
	}

	public void setImpSegSal(String impSegSal) {
		this.impSegSal = impSegSal;
	}

	public String getTotalPagos() {
		return totalPagos;
	}

	public void setTotalPagos(String totalPagos) {
		this.totalPagos = totalPagos;
	}

    public String getMontoMinimo() {
        return montoMinimo;
    }

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub}
		plazo=parser.parseNextValue("plazo");		
		importe=parser.parseNextValue("importe");
		cuentaVinc=parser.parseNextValue("cuentaVinc");
		plazoDes=parser.parseNextValue("plazoDes");
		Cat=parser.parseNextValue("Cat");
		fechaCat=parser.parseNextValue("fechaCat");
		pagoMenFijo=parser.parseNextValue("pagoMenFijo");
		estatusOferta=parser.parseNextValue("estatusOferta");
		folioUG=parser.parseNextValue("folioUG");
		totalPagos=parser.parseNextValue("totalPagos");
		descDiasPago=parser.parseNextValue("descDiasPago");
		bscPagar=parser.parseNextValue("bscPagar");
		impSegSal=parser.parseNextValue("impSegSal");
		tasaAnual=parser.parseNextValue("tasaAnual");
		tasaMensual=parser.parseNextValue("tasaMensual");
		producto=parser.parseNextValue("producto");
        montoMinimo=parser.parseNextValue("montoMinimo");
		tasaRevire = parser.parseNextValue("tasaRevire");

	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}
}
