package bancomer.api.consumo.gui.delegates;

import android.util.Log;

import java.util.Hashtable;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.commons.ConsumoSession;
import bancomer.api.consumo.gui.controllers.ExitoOfertaConsumoViewController;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.AceptaOfertaConsumo;
import bancomer.api.consumo.models.ActualizarCuentasResult;
import bancomer.api.consumo.models.ConsultaPoliza;
import bancomer.api.consumo.models.ListaCupones;
import bancomer.api.consumo.models.OfertaConsumo;
import bancomer.api.consumo.models.StatusCuponera;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;

public class ExitoOfertaConsumoDelegate implements BaseDelegate{

	public static final long EXITO_OFERTA_CONSUMO_DELEGATE = 631811334266765318L;

	OfertaConsumo ofertaConsumo;
	AceptaOfertaConsumo aceptaOfertaConsumo;
	ExitoOfertaConsumoViewController controller;
	InitConsumo init;
	ListaCupones listaCupones;
	ListaCupones.Cupon cupon;
	StatusCuponera statusCuponera;
	boolean cuponeraValue = false;

	public ExitoOfertaConsumoDelegate(AceptaOfertaConsumo aceptaOfertaConsumo, OfertaConsumo ofertaConsumo,ListaCupones listaCupones,ListaCupones.Cupon cupon){

		this.ofertaConsumo = ofertaConsumo;
		this.aceptaOfertaConsumo = aceptaOfertaConsumo;
		this.listaCupones = listaCupones;
		this.cupon = cupon;
		init = InitConsumo.getInstance();
	}

	public ExitoOfertaConsumoDelegate(AceptaOfertaConsumo aceptaOfertaConsumo, OfertaConsumo ofertaConsumo){

		this.ofertaConsumo = ofertaConsumo;
		this.aceptaOfertaConsumo = aceptaOfertaConsumo;
		init = InitConsumo.getInstance();
	}

	public boolean getCuponeraValue() {
		return cuponeraValue;
	}

	public void setCuponeraValue(boolean cuponeraValue) {
		this.cuponeraValue = cuponeraValue;
	}

	public AceptaOfertaConsumo getAceptaOfertaConsumo() {
		return aceptaOfertaConsumo;
	}

	public void setAceptaOfertaConsumo(AceptaOfertaConsumo aceptaOfertaConsumo) {
		this.aceptaOfertaConsumo = aceptaOfertaConsumo;
	}

	public OfertaConsumo getOfertaConsumo() {
		return ofertaConsumo;
	}

	public void setOfertaConsumo(OfertaConsumo ofertaConsumo) {
		this.ofertaConsumo = ofertaConsumo;
	}

	public void setController(ExitoOfertaConsumoViewController controller) {
		this.controller = controller;
	}

	public void realizaOperacion(int idOperacion, boolean isSMSEMail) {
        ICommonSession session = init.getSession();

		String ium = session.getIum();
		String numCel= session.getUsername();

		if(idOperacion == Server.DOMICILIACION_OFERTA_CONSUMO){
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion", "consultaDomiciliacionBovedaConsumoBMovil");
			params.put("IdProducto", "PBCCMDOM01");
			params.put("numeroCelular", numCel);
			params.put("bscPagar", ofertaConsumo.getbscPagar());
			params.put("numCredito",aceptaOfertaConsumo.getNumeroCredito());
			params.put("indicad", "D");
			params.put("IUM", ium);
			Hashtable<String, String> paramTable2  = AuxConectionFactory.domiciliacionConsumo(params);
			doNetworkOperation(Server.DOMICILIACION_OFERTA_CONSUMO, paramTable2,init.getBaseViewController(),true,new ConsultaPoliza(),Server.isJsonValueCode.CONSUMO);
		}

		else if (idOperacion == Server.ACTUALIZAR_CUENTAS){//codigo actualizacion de cuentas
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("numeroTelefono", session.getUsername());
			params.put("numeroCliente", ((ConsumoSession) session).getClientNumber());
			params.put("IUM", session.getIum());
			Hashtable<String, String> paramTable2  = AuxConectionFactory.actualizarCuentas(params);
			doNetworkOperation(Server.ACTUALIZAR_CUENTAS, params, init.getBaseViewController(),true,new ActualizarCuentasResult(),Server.isJsonValueCode.BMOVIL);
		}
		
		else if(idOperacion == Server.CONTRATO_OFERTA_CONSUMO){
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion", "consultaContratoBovedaConsumoBmovil");
			params.put("IdProducto", ofertaConsumo.getProducto());
			params.put("numeroCelular", numCel);
			params.put("numCredito",aceptaOfertaConsumo.getNumeroCredito());
			params.put("indicad", "C");
			params.put("plazoSal", aceptaOfertaConsumo.getPlazoSal());
			params.put("IUM", ium);
			Hashtable<String, String> paramTable2  = AuxConectionFactory.contratoConsumo(params);
			doNetworkOperation(Server.CONTRATO_OFERTA_CONSUMO, paramTable2, init.getBaseViewController(),true,new ConsultaPoliza(),Server.isJsonValueCode.CONSUMO);
		}

		else if(idOperacion==Server.SMS_OFERTA_CONSUMO){
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion", "exitoConsumoBMovil");
			params.put("numeroCelular",numCel);
			params.put("numCredito",aceptaOfertaConsumo.getNumeroCredito());
			params.put("idProducto",ofertaConsumo.getProducto());
			String estatus="";

			if(aceptaOfertaConsumo.getEstatusSal().equals("4")&& ofertaConsumo.getEstatusOferta().equals(Constants.PREABPROBADO)){
				estatus="E";
			}else if(!aceptaOfertaConsumo.getEstatusSal().equals("4")&& ofertaConsumo.getEstatusOferta().equals(Constants.PREABPROBADO)){
				estatus="T";
			}else if(ofertaConsumo.getEstatusOferta().equals(Constants.PREFORMALIZADO)){
				estatus="E";
			}

			params.put("estatusContratacion",estatus); //definir de donde se obtendra
			params.put("cuentaVinc",ofertaConsumo.getCuentaVinc());
			params.put("fechaCat",ofertaConsumo.getFechaCat());
			String importe= Tools.formatAmount(ofertaConsumo.getImporte(), false);
			importe=importe.replace("$", "");
			params.put("Importe",importe);
			params.put("plazoDes",aceptaOfertaConsumo.getPlazoSal());
			System.out.println("Valor plazoDes: "+ aceptaOfertaConsumo.getPlazoSal());
			String pagoMensualFijo= Tools.formatUserAmount(ofertaConsumo.getPlazo());
			params.put("pagoMensualFijo",pagoMensualFijo);
			String email= session.getEmail();
			params.put("email",email);
			params.put("Cat",ofertaConsumo.getCat());

			if(isSMSEMail)
				params.put("mensajeA","011");
			else if(email.equals(""))
				params.put("mensajeA","001");
			else
				params.put("mensajeA","001");

			params.put("IUM",ium);
			Hashtable<String, String> paramTable2  = AuxConectionFactory.envioSMSOfertaConsumo(params);
			doNetworkOperation(Server.SMS_OFERTA_CONSUMO, paramTable2, init.getBaseViewController(),true,new Object(),Server.isJsonValueCode.CONSUMO);
		}
		else if(idOperacion == Server.CONSULTA_DETALLE_CUPONERA){

			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion", "consultaDetalleCuponera");
			params.put("numeroCelular", numCel);
			params.put("numCredito",aceptaOfertaConsumo.getNumeroCredito());
			params.put("crCredito","0010");

			doNetworkOperation(Server.CONSULTA_DETALLE_CUPONERA,
					params,init.getBaseViewController(),true,new ListaCupones(),
					Server.isjsonvalueCode.CONSUMO);
		}
	}

	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params, BaseViewController caller) {
		InitConsumo.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params, caller,false);
	}

	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle,Server.isJsonValueCode isJsonValue) {
		init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false,isJson,handle,isJsonValue);

	}

	@Override
	public void performAction(Object obj) {

	}

	@Override
	public long getDelegateIdentifier() {
		return 0;
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {

		if (operationId == Server.DOMICILIACION_OFERTA_CONSUMO){
			ConsultaPoliza polizaResponse = (ConsultaPoliza)response.getResponse();
			init.showformatodomiciliacion(polizaResponse.getTerminosHtml());
		}
		else if(operationId == Server.CONTRATO_OFERTA_CONSUMO){
			ConsultaPoliza polizaResponse = (ConsultaPoliza)response.getResponse();
			init.showContrato(polizaResponse.getTerminosHtml(), 1);
		}
		else if(operationId == Server.SMS_OFERTA_CONSUMO){
			this.realizaOperacion(Server.ACTUALIZAR_CUENTAS, false);
		}
		else if(operationId==Server.ACTUALIZAR_CUENTAS){
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if(response.getResponse() instanceof ActualizarCuentasResult){
					ActualizarCuentasResult result = (ActualizarCuentasResult) response.getResponse();
					actualizacionDeCuentasExitosa(result.getAsuntos());
				}
				//Si se Actulizo la cuenta entonces consulto el status de la cuponera
				this.realizaOperacionConsultarStatusCuponera(Server.CONSULTA_STATUS_CUPONERA,aceptaOfertaConsumo.getNumeroCredito());
			}
			else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				init.getBaseViewController().showInformationAlert(controller,response.getMessageText());
			}
		}
		else if (operationId == Server.CONSULTA_DETALLE_CUPONERA) {
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				listaCupones = (ListaCupones) response.getResponse();
				init.showDetalleCuponera(listaCupones, cupon,aceptaOfertaConsumo,ofertaConsumo);
			}
		}else if(operationId == Server.CONSULTA_STATUS_CUPONERA){
			if(response.getStatus() ==  ServerResponse.OPERATION_SUCCESSFUL){
				statusCuponera = (StatusCuponera)response.getResponse();
				controller.setTextCuponera("Cuponera");
			}else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
			}
		}
	}
	//Metodo que actualiza las cuentas
	private void actualizacionDeCuentasExitosa(Account[] accounts) {
        ((ConsumoSession)init.getSession()).setAccounts(accounts);
    }

	public void showMenu() {
		init.getParentManager().setActivityChanging(true);
        ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
		init.resetInstance();
		
	}
	/**Operación de Consultar Stuatus de la cuponera*/
	public void realizaOperacionConsultarStatusCuponera(int operacion, String numeroCredido){
		if (operacion == Server.CONSULTA_STATUS_CUPONERA){
			ICommonSession session = init.getSession();
			String numCel = session.getUsername();

			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion","consultaCuponeraConsumo");
			params.put("numeroCelular",numCel);
			params.put("numCredito",reEstructuraNumeroCredito(numeroCredido));
			doNetworkOperation(Server.CONSULTA_STATUS_CUPONERA, params,
					init.getBaseViewController(), true, new StatusCuponera(), Server.isJsonValueCode.CONSUMO);
		}
	}

	private String reEstructuraNumeroCredito(String numeroCredito) {
		String parte1 = numeroCredito.substring(0, 8);
		String parte3 = numeroCredito.substring(8, 18);
		String parte2 = numeroCredito.substring(18, 20);
		String reStructura = parte1 + parte2 + parte3;
		return reStructura;
	}
}
