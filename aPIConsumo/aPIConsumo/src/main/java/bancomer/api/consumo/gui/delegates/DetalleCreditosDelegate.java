package bancomer.api.consumo.gui.delegates;

import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.util.Log;

import java.util.Hashtable;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.R;
import bancomer.api.consumo.gui.controllers.DetalleCuponera;
import bancomer.api.consumo.gui.controllers.FragmentCuponera;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.ListaCreditos;
import bancomer.api.consumo.models.ListaCupones;
import bancomer.api.consumo.models.StatusCuponera;

/**
 * Created by isaigarciamoso on 09/09/16.
 * Modified by OmarOrozco on 22/09/16
 */
public class DetalleCreditosDelegate implements BaseDelegate {
    public static final long KEY_UNIQUE_DELEGATE_DETALLE_CREDITOS = 631451334266725790L;
    private DetalleCuponera controller;
    private ListaCreditos listaCreditos;
    private ListaCupones listaCupones;
    private InitConsumo init = InitConsumo.getInstance();
    private BaseViewController baseViewController;
    private static String numeroCredito;


    public DetalleCuponera getController() {
        return controller;
    }
    public void setController(DetalleCuponera controller) {
        this.controller = controller;
    }

    public DetalleCreditosDelegate() {
    }
    public DetalleCreditosDelegate(ListaCreditos listaCreditos){
        this.listaCreditos = listaCreditos;
    }
    @Override
    public void doNetworkOperation(int i, Hashtable<String, ?> hashtable, BaseViewController baseViewController) {

    }

    /***----------- Convivencia ---**/
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle,Server.isJsonValueCode isJsonValue) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false, isJson, handle, isJsonValue);
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void analyzeResponse(int operationId, ServerResponse serverResponse) {

        if(operationId == Server.CONSULTA_STATUS_CUPONERA){
            if(serverResponse.getStatus() ==  ServerResponse.OPERATION_SUCCESSFUL){
                StatusCuponera statusCuponera = (StatusCuponera)serverResponse.getResponse();
                InitConsumo.getInstance().setCrCredito(statusCuponera.getCrCredito());
                realizaOperacionConsultaCuponFromCredito(numeroCredito);
            }else if(serverResponse.getStatus() == ServerResponse.OPERATION_ERROR){
                controller.hideProgressDialog();

                init.getBaseViewController()
                        .showInformationAlert(
                                controller,
                                serverResponse.getMessageText());
            }
        }else if(operationId == Server.CONSULTA_DETALLE_CUPONERA){
            if(serverResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                listaCupones = (ListaCupones)serverResponse.getResponse();
                int cantidadCupones = listaCupones.getListaCupon().size();
                //Paso 1 obtener la instancia del administrador de los fragmentos
                FragmentManager fragmentManager = controller.getFragmentManager();
                //Pasoo 2 crear una nueva Trasaccion
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                //Crear un nuevo Fragmento y anadirlo
                FragmentCuponera fragmentCuponera = new FragmentCuponera();
                FragmentCuponera.newInstance(listaCupones);
                transaction.add(R.id.fragment_cupones, fragmentCuponera);
                //Confirmar el cambio
                transaction.commit();
                controller.setCantidadCupones(String.valueOf(cantidadCupones));
            }else if(serverResponse.getStatus() == ServerResponse.OPERATION_ERROR){
                controller.hideProgressDialog();
                init.getBaseViewController()
                        .showInformationAlert(
                                controller,
                                serverResponse.getMessageText());
            }
        }
    }
    @Override
    public void performAction(Object o) {


    }
    public ListaCreditos getListaCreditos() {
        return listaCreditos;
    }
    public void setListaCreditos(ListaCreditos listaCreditos) {
        this.listaCreditos = listaCreditos;
    }

    public ListaCupones getListaCupones() {
        return listaCupones;
    }

    public void setListaCupones(ListaCupones listaCupones) {
        this.listaCupones = listaCupones;
    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

    private void realizaOperacionConsultaCuponFromCredito(String credito){

        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(init.getParentManager().getCurrentViewController());
        baseViewController.setDelegate(this);
        init.setBaseViewController(baseViewController);

        String user = init.getSession().getUsername();
        String numCredito = credito;
        Hashtable<String,String> params =  new Hashtable<String, String>();
        params.put("operacion","consultarCreditos");
        params.put("numeroCelular",user);
        params.put("numCredito",numCredito);
        params.put("crCredito",InitConsumo.getInstance().getCrCredito());
        //Aqui es donde lleno el elºº objeto de la mi modeloListaCuponera
        doNetworkOperation(Server.CONSULTA_DETALLE_CUPONERA,params, InitConsumo.getInstance().getBaseViewController(),true,
                new ListaCupones(), Server.isJsonValueCode.CONSUMO);
    }

    public void realizaOperacionConsultarStatusCuponera(String numeroCredido){
        String numCel = init.getSession().getUsername();
        numeroCredito = numeroCredido;

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put("operacion","consultaCuponeraConsumo");
        params.put("numeroCelular",numCel);
        params.put("numCredito",numeroCredido);
        //params.put("numCredito","86556475587964251756");
        doNetworkOperation(Server.CONSULTA_STATUS_CUPONERA, params,
                init.getBaseViewController(), true, new StatusCuponera(), Server.isJsonValueCode.CONSUMO);

    }
}
