package bancomer.api.consumo.models;

import java.io.IOException;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by isaigarciamoso on 07/09/16.
 */
public class StatusCuponera implements ParsingHandler {

    private String estado;
    private String aplicaCup;
    private String crCredito;
    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }
    @Override
    public void process(ParserJSON parserJSON) throws IOException, ParsingException {

        estado = parserJSON.parseNextValue("estado");
        aplicaCup = parserJSON.parseNextValue("aplicaCup");
        crCredito = parserJSON.parseNextValue("crCredito");
    }
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getAplicaCup() {
        return aplicaCup;
    }

    public void setAplicaCup(String aplicaCup) {
        this.aplicaCup = aplicaCup;
    }

    public String getCrCredito() {
        return crCredito;
    }

    public void setCrCredito(String crCredito) {
        this.crCredito = crCredito;
    }
}
