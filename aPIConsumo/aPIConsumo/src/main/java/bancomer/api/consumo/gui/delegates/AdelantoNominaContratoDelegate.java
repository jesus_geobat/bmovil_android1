package bancomer.api.consumo.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.util.Hashtable;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;

import bancomer.api.common.gui.delegates.SecurityComponentDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.R;
import bancomer.api.consumo.gui.controllers.AdelantoNominaContratoViewController;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.AceptaOfertaConsumo;
import bancomer.api.consumo.models.ConsultaPoliza;
import bancomer.api.consumo.models.OfertaConsumo;
//import bancomer.api.consumo.commons.
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtp;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtpBmovil;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;
//import bancomer.api.consumo.models.

/**
 * Created by Packo on 01/03/2016.
 */
public class AdelantoNominaContratoDelegate extends SecurityComponentDelegate implements BaseDelegate {

    public static final long ADELANTO_NOMINA_CONTRATO_DELEGATE = 151451334266765688L;

    private Constants.Operacion tipoOperacion;
    private AdelantoNominaContratoViewController controller;
    private Constants.TipoInstrumento tipoInstrumentoSeguridad;
    private InitConsumo init = InitConsumo.getInstance();
    private String token;
    private String CveProd;

    public OfertaConsumo ofertaConsumo;
    public Promociones promocion;

    public String getCveProd() {
        return CveProd;
    }

    public void setCveProd(String cveProd) {
        CveProd = cveProd;
    }

    public void setController(AdelantoNominaContratoViewController controller) {
        this.controller = controller;
        setContext(controller);
    }

    public Constants.TipoInstrumento consultaTipoInstrumentoSeguridad(){
        return tipoInstrumentoSeguridad;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public OfertaConsumo getOfertaConsumo() {
        return ofertaConsumo;
    }

    public AdelantoNominaContratoDelegate(OfertaConsumo ofertaConsumo, Promociones promocion){
        this.ofertaConsumo  = ofertaConsumo;
        this.promocion      = promocion;

        ofertaConsumo.getImporte();

        Log.i("Contrato","producto: "+ofertaConsumo.getProducto());
        Log.i("Contrato","getEstatusOferta: "+ofertaConsumo.getEstatusOferta());
        Log.i("Contrato","getCveCamp: "+promocion.getCveCamp());

        init = InitConsumo.getInstance();
        String instrumento = init.getSession().getSecurityInstrument();
        //ehmendezr se agrega modificacion de S2
        if (instrumento.equals(Constants.IS_TYPE_DP270)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
        } else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
        } else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)||instrumento.equals(Constants.TYPE_SOFTOKEN.S2.value)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
        } else {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
        }
    }

    public void setTipoOperacion(Constants.Operacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public Constants.Operacion getTipoOperacion(){
        return this.tipoOperacion;
    }

    public boolean mostrarContrasenia(){
        if(getTipoOperacion() == null)
        if(init.getSession().getClientProfile() == null)
        if(operacion() == null){
        }

        return init.getAutenticacion().mostrarContrasena(operacion(), init.getSession().getClientProfile());
    }

    public boolean mostrarNIP(){
        return init.getAutenticacion().mostrarNIP(operacion(), init.getSession().getClientProfile());
    }

    public Constants.TipoOtpAutenticacion tokenAMostrar(){
        Constants.TipoOtpAutenticacion tipoOTP;

        try{
            tipoOTP = init.getAutenticacion().tokenAMostrar(operacion(), init.getSession().getClientProfile());
        }catch (Exception ex){
            if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.mostrarNIP execution.", ex);
            tipoOTP = null;
        }
        return tipoOTP;
    }

    public boolean mostrarCVV(){
        return init.getAutenticacion().mostrarCVV(operacion(), init.getSession().getClientProfile());
    }

    public boolean mostrarCampoTarjeta(){
        return false;
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController baseViewController) {
        InitConsumo.getInstance().getBaseSubApp().invokeNetworkOperation(operationId,params,baseViewController,false);
    }
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle,Server.isJsonValueCode isJsonValue) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false,isJson,handle,isJsonValue);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        Log.v("Analyze", "AnalyzeResponseAdelantoNomina");
        Log.w("anom","operacion successful");
        if (operationId == Server.TERMINOS_OFERTA_CONSUMO) {
        ConsultaPoliza polizaResponse = (ConsultaPoliza) response.getResponse();
        if (init == null)
            Log.i("anom", "init nulo");

        if (polizaResponse.getTerminosHtml() == null)
            Log.i("anom", "terminos nulos");

        init.showTerminosConsumo(polizaResponse.getTerminosHtml());

        } else if (operationId == Server.CONTRATA_ALTERNATIVA_CONSUMO) {
        if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
            Log.i("Contrato", "Contratación exitosa");

            AceptaOfertaConsumo oAceptaOC = (AceptaOfertaConsumo)response.getResponse();


            init.showExitoContratoViewController(promocion, ofertaConsumo,oAceptaOC);
        }
        } else if (operationId == Server.EXITO_OFERTA_CONSUMO) {
        if (response.getStatus() == ServerResponse.OPERATION_ERROR) {/*
        aceptaOferta= (AceptaOfertaConsumo)response.getResponse();

        if(aceptaOferta.getPromociones()!=null){
            init.getSession().setPromocion(aceptaOferta.getPromociones());

            init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick( DialogInterface dialog,int which) {
                            init.getParentManager().setActivityChanging(true);
                            ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                            init.resetInstance();
                        }
                    });
        }else{

            init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(
                                DialogInterface dialog,
                                int which) {
                        }
                    });

        }*/
        } else {/*
        aceptaOferta= (AceptaOfertaConsumo)response.getResponse();
        ICommonSession session = init.getSession();
        session.setPromocion(aceptaOferta.getPromociones());
        if(ofertaConsumo.getEstatusOferta().equals(Constants.PREFORMALIZADO)){
            init.showExitoConsumo(aceptaOferta, ofertaConsumo);//pantalla exito
        }else if(ofertaConsumo.getEstatusOferta().equals(Constants.PREABPROBADO)){
            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int arg1) {
                    dialog.dismiss();
                    init.getParentManager().setActivityChanging(true);
                    ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                        init.resetInstance();
                    }
                };
            }*/
            }
        }
    }

    @Override
    public void performAction(Object o) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

    public void confirmaRechazoAtras(){
        init.getBaseViewController().showYesNoAlert(controller, controller.getResources().getString(R.string.api_contrato_adelanto_nomina_confirma_rechazo), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(DialogInterface.BUTTON_POSITIVE == which){
                    init.getParentManager().setActivityChanging(true);
                    ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                    init.resetInstance();
                    Log.i("Detalle", "Confirma rechazo atras");
                }
                else
                    dialog.dismiss();
            }
        });
    }

    public Constants.Operacion operacion(){
        return Constants.Operacion.oneClickBmovilConsumo;
    }

    public void realizaOperacion(int idOperacion, int tipoSegBBVA) {
        ICommonSession session = init.getSession();

        String ium = session.getIum();
        String numCel = session.getUsername();

        if(idOperacion == Server.TERMINOS_OFERTA_CONSUMO){
            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion","consultaContratoConsumoBMovil");
            params.put("opcion","1");
            params.put("IdProducto",ofertaConsumo.getProducto());
            params.put("versionE","");
            params.put("numeroCelular",numCel);
            params.put("IUM",ium);
            Hashtable<String, String> paramTable2  = AuxConectionFactory.terminosConsumo(params);
            doNetworkOperation(Server.TERMINOS_OFERTA_CONSUMO,paramTable2,init.getBaseViewController(),true,new ConsultaPoliza(),Server.isJsonValueCode.CONSUMO);

        }else if(idOperacion== Server.EXITO_OFERTA_CONSUMO){
            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion", "contrataAlternativaConsumo");
            params.put("numeroCelular", numCel);

            if(token!=null)
                params.put("codigoOTP",token);
            else
                params.put("codigoOTP","");

            params.put("operacion", "oneClickBmovilConsumo");
            String cadAutenticacion = init.getAutenticacion().getCadenaAutenticacion(this.tipoOperacion, init.getSession().getClientProfile());
            params.put("cadenaAutenticacion", cadAutenticacion);

            params.put("seguroObli", ""); //tiporadioburron

            params.put("IUM",ium);
            params.put("importePar","000");

            Log.i("Contrato", "cel: " + numCel);
            Log.i("Contrato", "token: " + token);
            Log.i("Contrato", "cadAutenticacion: " + cadAutenticacion);
            Log.i("Contrato", "IUM: " + ium);
            params.put("codigoNIP", "");
            params.put("codigoCVV2", "");
            params.put("cveAcceso", "");
            params.put("tarjeta5Dig", "");
            params.put("claveCamp","");
            params.put("estatus", "");
            Hashtable<String, String> paramTable2  = AuxConectionFactory.exitoConsumo(params,false);
            doNetworkOperation(Server.EXITO_OFERTA_CONSUMO, paramTable2, init.getBaseViewController(),true,new AceptaOfertaConsumo(),Server.isJsonValueCode.CONSUMO);

        }else if(idOperacion == Server.CONTRATA_ALTERNATIVA_CONSUMO){

            String ium2 = session.getIum();
            String numCel2 = session.getUsername();
            String operacionANOM = "contrataAlternativaConsumo";
            String cadAutenticacion = init.getAutenticacion().getCadenaAutenticacion(this.tipoOperacion, init.getSession().getClientProfile());

            Hashtable<String, String> params = new Hashtable<String, String>();

            params.put("operacion",operacionANOM);
            params.put("numeroCelular",numCel2);
            params.put("claveCamp",promocion.getCveCamp());
            params.put("estatus","A"); //fijo en A, se especificó en junta con José

            if(token!=null)
                params.put("codigoOTP",token);
            else
                params.put("codigoOTP","");

            params.put("seguroObli","");
            params.put("IUM",ium2);
            params.put("producto","ANOM");
            params.put("libre", "");
            params.put("cadenaAutenticacion", cadAutenticacion);

            Hashtable<String, String> paramTable2  = AuxConectionFactory.exitoConsumo(params,true);
            doNetworkOperation(Server.CONTRATA_ALTERNATIVA_CONSUMO, paramTable2,init.getBaseViewController(),true,new AceptaOfertaConsumo(),Server.isJsonValueCode.CONSUMO);

//            doNetworkOperation(Server.CONTRATA_ALTERNATIVA_CONSUMO, params, init.getBaseViewController());
        }
    }

    public boolean enviaPeticionOPeracion(){

        String contrasena = null;
        String nip = null;
        String asm = null;
        String cvv = null;
        if(controller instanceof AdelantoNominaContratoViewController){
            if (mostrarContrasenia()) {
                contrasena = controller.pideContrasena();
                if (contrasena.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.PASSWORD_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            String tarjeta = null;
            if(mostrarCampoTarjeta()){
                tarjeta = controller.pideTarjeta();
                String mensaje = "";
                if(tarjeta.equals("")){
                    mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }else if(tarjeta.length() != 5){
                    mensaje =  "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            if (mostrarNIP()) {
                nip = controller.pideNIP();
                if (nip.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (nip.length() != Constants.NIP_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.NIP_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) {
                asm = controller.pideASM();
                if (asm.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (asm.length() != Constants.ASM_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.ASM_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (mostrarCVV()) {
                cvv = controller.pideCVV();
                if (cvv.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (cvv.length() != Constants.CVV_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.CVV_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

//            String newToken = null;
//            if(tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == Constants.TipoInstrumento.SoftToken && init.getSofTokenStatus())
//                newToken= init.getDelegateOTP().loadOtpFromSofttoken(tokenAMostrar(),init.getDelegateOTP());
//            if(null != newToken)
//                asm = newToken;
//
//            //guardar variable en token a enviar en peticion
//            token=asm;
//
//            return true;
            String newToken = null;
            if(tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == Constants.TipoInstrumento.SoftToken && (init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService())) {
                if (init.getSofTokenStatus()) {
                    newToken = init.getDelegateOTP().loadOtpFromSofttoken(tokenAMostrar(), init.getDelegateOTP());
                    if (null != newToken)
                        asm = newToken;
                    token = asm;
//                    performAction(null);
                    realizaOperacion(Server.CONTRATA_ALTERNATIVA_CONSUMO, 1);
                } else if (!init.getSofTokenStatus() && PropertiesManager.getCurrent().getSofttokenService()) {

                    GetOtpBmovil otpG = new GetOtpBmovil(new GetOtp() {
                        /**
                         * @param otp
                         */
                        @Override
                        public void setOtpRegistro(String otp) {

                        }

                        /**
                         * @param otp
                         */
                        @Override
                        public void setOtpCodigo(String otp) {
                            if (ServerCommons.ALLOW_LOG) {
                                Log.d("APP", "la otp en callback: " + otp);
                            }
                            token = String.valueOf(otp);
//                            performAction(null);
                            realizaOperacion(Server.CONTRATA_ALTERNATIVA_CONSUMO, 1);
                        }

                        /**
                         * @param otp
                         */
                        @Override
                        public void setOtpCodigoQR(String otp) {

                        }
                    }, SuiteApp.appContext);

                    otpG.generateOtpCodigo();
                }
            }
            else if(tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno ) {
                token = asm;
//                performAction(null);
                realizaOperacion(Server.CONTRATA_ALTERNATIVA_CONSUMO, 1);
            }
            else if(tokenAMostrar() == Constants.TipoOtpAutenticacion.ninguno){
                token = asm;
//                performAction(null);
                realizaOperacion(Server.CONTRATA_ALTERNATIVA_CONSUMO, 1);
            }
            return true;
        }
        return false;
    }

}
