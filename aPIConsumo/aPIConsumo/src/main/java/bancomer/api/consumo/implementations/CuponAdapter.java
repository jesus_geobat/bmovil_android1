package bancomer.api.consumo.implementations;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bancomer.api.consumo.R;
import bancomer.api.consumo.models.ListaCupones;

/**
 * Created by isaigarciamoso on 22/08/16.
 */
public class CuponAdapter<T> extends ArrayAdapter<T> {
    ArrayList items;

    public CuponAdapter(Context context, List<T> objects) {
        super(context, 0, objects);
    }

    public void addTodo(ArrayList arrayList) {
        items.addAll(arrayList);
        notifyDataSetChanged();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //Salvando la referencia del View de la Fila
        View listItemView = convertView;
        ///Comprobando si el View no existe
        if (convertView == null) {
            //Si no existe, entonces inflarlo con two_line_list_item.xml
            listItemView = inflater.inflate(
                    R.layout.item_cupon,
                    parent, false);
        }
        //Obteniendo instancias desde los textview
        TextView titulo_cupon = (TextView) listItemView.findViewById(R.id.titulo_cupon);
        TextView descripcion_cupon = (TextView) listItemView.findViewById(R.id.detalle_cupon);
        TextView categoria_cupon = (TextView) listItemView.findViewById(R.id.tipo_cupon);
        TextView porcentaje_descuento_cupon = (TextView) listItemView.findViewById(R.id.porcentaje_cupon);
        TextView descuento_txtstatic_cupon = (TextView) listItemView.findViewById(R.id.descuento_cupon);
        ImageView imagen = (ImageView) listItemView.findViewById(R.id.imagen_cupon);

        //Obteniendo instancias de la Tarea en la posicion actual
        //T item = (T) getItem(posicion);
        //Dividir la cadena en nombre y hora
        //Obteniendo instancia de la Tarea en la posición actual
        ListaCupones.Cupon cupon = (ListaCupones.Cupon) getItem(position);
        titulo_cupon.setText(cupon.getNombreComercio());
        descripcion_cupon.setText(cupon.getDesCupon());
        categoria_cupon.setText(cupon.getDesCategoria());
        porcentaje_descuento_cupon.setText(cupon.getPorcentajeDes());
        descuento_txtstatic_cupon.setText("descuento");


        //Devolder al listView la fila creaada
        return listItemView;
    }

}
