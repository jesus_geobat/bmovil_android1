package bancomer.api.consumo.commons;

import java.util.Date;

import suitebancomer.aplicaciones.bmovil.classes.model.Account;

/**
 * Created by OOROZCO on 1/4/16.
 */
public interface ConsumoSession {

    public String getClientNumber();
    public void setAccounts(Account[] accounts);
    public Date getServerDate();
}
