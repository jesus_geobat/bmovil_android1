package bancomer.api.consumo.gui.delegates;

import android.util.Log;

import java.util.Hashtable;

import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.gui.controllers.PolizaViewController;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;


public class PolizaDelegate implements BaseDelegate{

    OfertaConsumo ofertaConsumo;
    Promociones promociones;
    InitConsumo initConsumo = InitConsumo.getInstance();
    private String poliza;
    private PolizaViewController controller;
    public static final long POLIZA_CONSUMO_DELEGATE =   851451334266797425L;

    /**Construcctor vacio*/
    public PolizaDelegate(){

    }
   public PolizaDelegate(String poliza,OfertaConsumo ofertaConsumo,Promociones promociones){
        initConsumo =  InitConsumo.getInstance(); //Inicializando Objeto
        this.ofertaConsumo = ofertaConsumo;
        this.promociones = promociones;
        this.poliza =  poliza;
    }

    public PolizaViewController getController() {
        return controller;
    }

    public void setController(PolizaViewController controller) {
        this.controller = controller;
    }

    public String getPoliza() {
        return poliza;
    }

    public void setPoliza(String poliza) {
        this.poliza = poliza;
    }

    /**NetWortOpreacion*/
    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {

    }
   /**Analiza la respuesta**/
    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

    }
   /**Perfomac Action*/
    @Override
    public void performAction(Object obj) {
       /**En este metodo creo un JSON que enviara información*/
    }
   /*Delegados**/
    @Override
    public long getDelegateIdentifier() {
        return 0;
    }


    public OfertaConsumo getOfertaConsumo() {
        return ofertaConsumo;
    }

    public void setOfertaConsumo(OfertaConsumo ofertaConsumo) {
        this.ofertaConsumo = ofertaConsumo;
    }

    public Promociones getPromociones() {
        return promociones;
    }

    public void setPromociones(Promociones promociones) {
        this.promociones = promociones;
    }

    public InitConsumo getInitConsumo() {
        return initConsumo;
    }

    public void setInitConsumo(InitConsumo initConsumo) {
        this.initConsumo = initConsumo;
    }
}
