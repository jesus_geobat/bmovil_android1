package bancomer.api.consumo.gui.controllers;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bancomer.api.common.commons.GuiTools;
import bancomer.api.consumo.R;
import bancomer.api.consumo.commons.ConstantsCredit;
import bancomer.api.consumo.commons.ImageDownloader;
import bancomer.api.consumo.models.ListaCupones;


/**
 * Created by OmarOrozco on 22/09/16
 */
public class CuponAdapter<T> extends ArrayAdapter<ListaCupones.Cupon> {
    private  ArrayList items;
    private Context context;
    private List<ListaCupones.Cupon> cupons;
    private List<Object> itemsList;
    private final static ImageDownloader imageDownloader = new ImageDownloader();
    private final static String _URL_IMAGEN = "https://www.bancomermovil.com/imgsp_mx_web/imgsp_mx_web/web/ImagenesOllin/ofertas/";

    public List<ListaCupones.Cupon> getCupons() {
        return cupons;
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    public CuponAdapter(Context context,List<ListaCupones.Cupon> cupons){
        super(context, 0, cupons);
        this.context = context;
        this.cupons = cupons;
        itemSorting();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View listItemView;

        if(itemsList.get(position) instanceof  String)
        {
            listItemView = inflater.inflate(
                    R.layout.listview_item_cuponera_header,
                    parent, false);

            switch (obtenerVive((String) itemsList.get(position)))
            {
                case 1:

                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setText("Vive Bienestar");
                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setTextColor(context.getResources().getColor(R.color.vive_bien));
                    ((ImageView)listItemView.findViewById(R.id.image_view_image_vive)).setImageResource(R.drawable.vive_bien);

                    break;
                case 2:

                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setText("Vive en Línea");
                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setTextColor(context.getResources().getColor(R.color.vive_enlinea));
                    ((ImageView)listItemView.findViewById(R.id.image_view_image_vive)).setImageResource(R.drawable.vive_enlinea);

                    break;
                case 3:

                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setText("Vive Ideas");
                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setTextColor(context.getResources().getColor(R.color.vive_ideas));
                    ((ImageView)listItemView.findViewById(R.id.image_view_image_vive)).setImageResource(R.drawable.vive_ideas);

                    break;
                case 4:

                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setText("Vive Placeres");
                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setTextColor(context.getResources().getColor(R.color.vive_placeres));
                    ((ImageView)listItemView.findViewById(R.id.image_view_image_vive)).setImageResource(R.drawable.vive_placeres);

                    break;
                case 5:

                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setText("Vive Sabores");
                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setTextColor(context.getResources().getColor(R.color.vive_sabores));
                    ((ImageView)listItemView.findViewById(R.id.image_view_image_vive)).setImageResource(R.drawable.vive_sabores);

                    break;
                case 6:

                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setText("Vive Soluciones");
                    ((TextView)listItemView.findViewById(R.id.text_view_label_vive)).setTextColor(context.getResources().getColor(R.color.vive_soluciones));
                    ((ImageView)listItemView.findViewById(R.id.image_view_image_vive)).setImageResource(R.drawable.vive_soluciones);

                    break;
            }
        }
        else
        {
            listItemView = inflater.inflate(
                    R.layout.item_cupon,
                    parent, false);

            TextView titulo_cupon = (TextView) listItemView.findViewById(R.id.titulo_cupon);
            TextView descripcion_cupon = (TextView) listItemView.findViewById(R.id.detalle_cupon);
            TextView categoria_cupon = (TextView) listItemView.findViewById(R.id.tipo_cupon);
            TextView porcentaje_descuento_cupon = (TextView) listItemView.findViewById(R.id.porcentaje_cupon);

            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/unicode.futurabb.ttf");
            porcentaje_descuento_cupon.setTypeface(typeface);
            Typeface tahoma = Typeface.createFromAsset(getContext().getAssets(), "fonts/tahoma.ttf");

            descripcion_cupon.setTypeface(tahoma);
            categoria_cupon.setTypeface(tahoma);

            TextView descuento_txtstatic_cupon = (TextView) listItemView.findViewById(R.id.descuento_cupon);
            ImageView imagen = (ImageView) listItemView.findViewById(R.id.imagen_cupon);


            ListaCupones.Cupon cupon = (ListaCupones.Cupon)itemsList.get(position);
            titulo_cupon.setText(cupon.getNombreComercio());
            descripcion_cupon.setText(cupon.getDesCupon());

            categoria_cupon.setText(cupon.getDesGiro());

            porcentaje_descuento_cupon.setText(cupon.getPorcentajeDes());
            descuento_txtstatic_cupon.setText("descuento");
            descuento_txtstatic_cupon.setTypeface(tahoma);
            imageDownloader.download(_URL_IMAGEN + cupon.getCodigoLogo() + ".jpg", imagen);

            GuiTools gTools = GuiTools.getCurrent();

            gTools.scale(titulo_cupon,true);
            gTools.scale(descripcion_cupon,true);
            gTools.scale(categoria_cupon,true);
            gTools.scale(porcentaje_descuento_cupon,true);
            gTools.scale(descuento_txtstatic_cupon,true);
            gTools.scale(imagen);
            gTools.scale(((ImageView)listItemView.findViewById(R.id.image_view_image_vive)), true);


            if(position == itemsList.size()-1 && context instanceof DetalleCuponera)
            {
                ((DetalleCuponera)context).showBottomElements();
            }
        }
        return listItemView;
    }



    private void itemSorting()
    {
        List<ListaCupones.Cupon> viveSoluciones = null;
        List<ListaCupones.Cupon> viveIdeas = null;
        List<ListaCupones.Cupon> viveBienestar = null;
        List<ListaCupones.Cupon> viveEnLinea = null;
        List<ListaCupones.Cupon> vivePlaceres = null;
        List<ListaCupones.Cupon> viveSabores = null;
        List<ListaCupones.Cupon> noneList = null;

        for (ListaCupones.Cupon currentCupon : cupons)
        {
            switch (obtenerVive(currentCupon.getDesCategoria()))
            {
                case 1:
                    if(viveBienestar == null)
                        viveBienestar = new ArrayList<ListaCupones.Cupon>();

                    viveBienestar.add(currentCupon);

                    break;
                case 2:
                    if(viveEnLinea == null)
                        viveEnLinea = new ArrayList<ListaCupones.Cupon>();

                    viveEnLinea.add(currentCupon);

                    break;
                case 3:
                    if(viveIdeas == null)
                        viveIdeas = new ArrayList<ListaCupones.Cupon>();

                    viveIdeas.add(currentCupon);

                    break;
                case 4:
                    if(vivePlaceres == null)
                        vivePlaceres = new ArrayList<ListaCupones.Cupon>();

                    vivePlaceres.add(currentCupon);

                    break;
                case 5:
                    if(viveSabores == null)
                        viveSabores = new ArrayList<ListaCupones.Cupon>();

                    viveSabores.add(currentCupon);

                    break;
                case 6:
                    if(viveSoluciones == null)
                        viveSoluciones = new ArrayList<ListaCupones.Cupon>();

                    viveSoluciones.add(currentCupon);

                    break;
                default:
                    if(noneList == null)
                        noneList = new ArrayList<ListaCupones.Cupon>();

                    noneList.add(currentCupon);

                    break;
            }
        }

        itemsList = new ArrayList<Object>();

        if(noneList != null)
        {
            for(ListaCupones.Cupon cupon : noneList)
            {
                itemsList.add(cupon);
            }
        }

        if(viveSoluciones != null)
        {
            itemsList.add(ConstantsCredit.VIVE_SOLUCIONES);

            for(ListaCupones.Cupon cupon : viveSoluciones)
            {
                itemsList.add(cupon);
            }
        }

        if(viveBienestar != null)
        {
            itemsList.add(ConstantsCredit.VIVE_BIEN);

            for(ListaCupones.Cupon cupon : viveBienestar)
            {
                itemsList.add(cupon);
            }
        }

        if(viveEnLinea != null)
        {
            itemsList.add(ConstantsCredit.VIVE_EN_LINEA);

            for(ListaCupones.Cupon cupon : viveEnLinea)
            {
                itemsList.add(cupon);
            }
        }

        if(viveIdeas != null)
        {
            itemsList.add(ConstantsCredit.VIVE_IDEAS);

            for(ListaCupones.Cupon cupon : viveIdeas)
            {
                itemsList.add(cupon);
            }
        }

        if(vivePlaceres != null)
        {
            itemsList.add(ConstantsCredit.VIVE_PLACERES);

            for(ListaCupones.Cupon cupon : vivePlaceres)
            {
                itemsList.add(cupon);
            }
        }

        if(viveSabores != null)
        {
            itemsList.add(ConstantsCredit.VIVE_SABORES);

            for(ListaCupones.Cupon cupon : viveSabores)
            {
                itemsList.add(cupon);
            }
        }
    }

    private int obtenerVive(String desCategoria) {
        Log.e("obtenerVive", desCategoria);
        if(desCategoria.equals(ConstantsCredit.VIVE_BIEN)){
            Log.e("obtenerVive", "1");
            return 1;
        }else if(desCategoria.equals(ConstantsCredit.VIVE_EN_LINEA)){
            Log.e("obtenerVive", "2");
            return 2;
        }else if(desCategoria.equals(ConstantsCredit.VIVE_IDEAS)){
            Log.e("obtenerVive", "3");
            return 3;
        }else if(desCategoria.equals(ConstantsCredit.VIVE_PLACERES)){
            Log.e("obtenerVive", "4");
            return 4;
        }else if(desCategoria.equals(ConstantsCredit.VIVE_SABORES)){
            Log.e("obtenerVive", "5");
            return 5;
        }else if(desCategoria.equals(ConstantsCredit.VIVE_SOLUCIONES)){
            Log.e("obtenerVive", "6");
            return 6;
        }
        return 0;
    }
}

