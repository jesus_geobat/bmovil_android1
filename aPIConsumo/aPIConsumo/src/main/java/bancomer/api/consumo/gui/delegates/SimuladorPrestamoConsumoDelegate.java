package bancomer.api.consumo.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import java.util.Hashtable;

import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.R;
import bancomer.api.consumo.gui.controllers.SimuladorPrestamoConsumoViewController;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;

/**
 * Created by isaigarciamoso on 26/07/16.
 */
public class SimuladorPrestamoConsumoDelegate implements BaseDelegate {


   // public static final long SIMULADOR_OFERTA_CONSUMO = 631811334266765318L;
   public static final long SIMULADOR_OFERTA_CONSUMO = 631811334266778890L;


    private SimuladorPrestamoConsumoViewController controller;
    private InitConsumo initConsumo = InitConsumo.getInstance();
    private BaseViewController baseViewController;
    //Dos nuevas variables  para tomar en cuenta.
    private String montoMinimo;
    private String montoMaximo;
    private Session session;
    private boolean isFromAdelantoNomina;
    private Promociones promocion;
    private OfertaConsumo ofertaConsumo;
    private String descripcionMensaje;
    private int opFlujo;
    private String modificarImporte = " ";
    private String importePar = "";
    private boolean isCallmeBack;


    public String getImportePar() {
        return importePar;
    }

    public void setImportePar(String importePar) {
        this.importePar = importePar;
    }

    public String getDescripcionMensaje() {
        return descripcionMensaje;
    }

    public void setDescripcionMensaje(String descripcionMensaje) {
        this.descripcionMensaje = descripcionMensaje;
    }

    public int getOpFlujo() {
        return opFlujo;
    }

    public void setOpFlujo(int opFlujo) {
        this.opFlujo = opFlujo;
    }

    public String getModificarImporte() {
        return modificarImporte;
    }

    public void setModificarImporte(String modificarImporte) {
        this.modificarImporte = modificarImporte;
    }

    public OfertaConsumo getOfertaConsumo() {
        return ofertaConsumo;
    }

    public void setOfertaConsumo(OfertaConsumo ofertaConsumo) {
        this.ofertaConsumo = ofertaConsumo;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public void setPromocion(Promociones promocion) {
        this.promocion = promocion;
    }


    public void setIsFromAdelantoNomina(boolean isFromAdelantoNomina) {
        this.isFromAdelantoNomina = isFromAdelantoNomina;
    }

    //Contructor de la clase
    public SimuladorPrestamoConsumoDelegate(String montoMinimo, String montoMaximo, Promociones promocion, OfertaConsumo ofertaConsumo) {

        initConsumo = InitConsumo.getInstance();
        this.montoMinimo = montoMinimo;
        this.montoMaximo = montoMaximo;
        this.promocion = promocion;
        this.ofertaConsumo = ofertaConsumo;

    }

    public SimuladorPrestamoConsumoDelegate() {

    }

    public void setController(SimuladorPrestamoConsumoViewController controller) {
        this.controller = controller;
    }

    public boolean getIsCallmeBack() {
        return isCallmeBack;
    }

    public void setIsCallmeBack(boolean isCallmeBack) {
        this.isCallmeBack = isCallmeBack;
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        String modificarImporteRegreso = "< Modificar importe";

        if(operationId == Server.OP_AYUDA_PRESTAMO_CONSUMO){
            initConsumo.getBaseViewController().showInformationAlert(controller, R.string.api_exitocallmeback_consumo_descripcion);


        }else if(operationId == Server.RECHAZO_OFERTA_CONSUMO){


        }else if(operationId == Server.CONSULTA_DETALLE_OFERTA_CONSUMO){
            if (response.getStatus() == ServerResponse.OPERATION_ERROR) {

                initConsumo.getParentManager().resetRootViewController();
                initConsumo.updateHostActivityChangingState();
            }else if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                OfertaConsumo ofertaConsumo = (OfertaConsumo) response.getResponse();
                Log.d("VALOR NUEVO IMPORT PAR", controller.devuelveDatoSimulador()+"");
                initConsumo.showDetalleConsumoFromSimulador(ofertaConsumo, initConsumo.getOfertaConsumo(),
                        controller.devuelveDatoSimulador(),modificarImporteRegreso, getIsCallmeBack());
            }

        }

    }

    @Override
    public void performAction(Object obj) {
        // metodo implementado por la interfaz de BaseDelegate

        Log.d("mensaje de isai", "estoy entrando en performAction");
        /**Comente esta linea de abajo y agregue estas nuevas las de abajo**/

        baseViewController = initConsumo.getBaseViewController();
        baseViewController.setActivity(initConsumo.getParentManager().getCurrentViewController());
        baseViewController.setDelegate(this);
        initConsumo.setBaseViewController(baseViewController);

        String user = initConsumo.getSession().getUsername();
        String ium = initConsumo.getSession().getIum();
        String cveCamp = initConsumo.getOfertaConsumo().getCveCamp();

        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put("numeroCelular", user);
        params.put("claveCamp", cveCamp);
        params.put("IUM", ium);
        params.put("importePar", initConsumo.getImporteParcial());

        Hashtable<String, String> paramTable2 = AuxConectionFactory.consultaDetalleConsumo(params);
        doNetworkOperation(Server.CONSULTA_SIMULADOR_OFERTA_CONSUMO,
                paramTable2, InitConsumo.getInstance().getBaseViewController(),
                true, new OfertaConsumo(), Server.isJsonValueCode.CONSUMO);

    }

    @Override
    public long getDelegateIdentifier() {
        // metodo implementado por la interfaz de BaseDelegate

        return 0;
    }

    //Metodo que inicializa la simulación
    public void saveOrDeleteSimulationRequest(boolean isSavingOperation) {
    }


    public String getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(String montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public String getMontoMaximo() {
        return montoMaximo;
    }

    public void setMontoMaximo(String montoMaximo) {
        this.montoMaximo = montoMaximo;
    }


    public void requestSimulation() {
        session.getIum();
        session.getNombreCliente();
        session.getClientNumber();
        session.getEmail();
        session.storeSession();
        session.getPassword();

    }


    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        initConsumo.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false);
    }
   /**METODO QUE REALLIZA LA AOPREACIÓN DE CONNECCION QUE SE UTILIZA EN  METODO (realizaOperacionCONSUMOSimulador)**/
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle, Server.isJsonValueCode isJsonValue) {
        initConsumo.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false, isJson, handle, isJsonValue);
    }
    public void realizaOperacionCONSUMOSimulador() {

        Log.d("CONSUMO_SIMULADOR","ESTOY ENTRANDO EN OPERACION CONSUMO SIMULADOR");
        String userPhone = initConsumo.getSession().getUsername();
        String ium = initConsumo.getSession().getIum();

        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put("numeroCelular",userPhone);
        params.put("claveCamp", promocion.getCveCamp());
        params.put("IUM", ium);


        if (!initConsumo.getImporteParcial().equals("")) {
            params.put("importePar", initConsumo.getImporteParcial());
        } else {
            params.put("importePar","000");
        }
        Hashtable<String, String> paramTable2 = AuxConectionFactory.consultaDetalleConsumo(params);

        doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA_CONSUMO,
                paramTable2, initConsumo.getBaseViewController(),
                true, new OfertaConsumo(), Server.isJsonValueCode.CONSUMO);


    }
    /*** ::::::::::::::::::::::: boton de llamada   :::::::::::::::::::::::::: **/
    public void peticionBtnLlamenme() {

        ICommonSession session = initConsumo.getSession();

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put("operacion", "noAceptacionBMovil");
        params.put("numeroCelular", session.getUsername());
        params.put("cveCamp", promocion.getCveCamp());
        params.put("descripcionMensaje", "0038");
        params.put("folioUG", ofertaConsumo.getFolioUG()); // definir de donde se obtendra
        params.put("IUM", session.getIum());
        Hashtable<String, String> paramTable2 = AuxConectionFactory.rechazoOfertaConsumo(params);

        this.doNetworkOperation(Server.RECHAZO_OFERTA_CONSUMO, paramTable2, initConsumo.getBaseViewController(),
                true, new Object(), Server.isJsonValueCode.ONECLICK);
    }

    /**
     * :::::::::::::::: PETICION DE LLAMADA AL SERVIDOR DE AYUDA :::::::::::
     **/

    public void peticionLlamame(BaseViewController baseViewController, String importePar) {

        ICommonSession session = initConsumo.getSession();
        String ium = session.getIum();
        String numCel = session.getUsername();
        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put("operacion", "ayudaPrestamoConsumo");
        params.put("numeroCelular", numCel);
        params.put("claveCamp", promocion.getCveCamp());
        params.put("IUM", ium);
        params.put("importePar", "000");

        Hashtable<String, String> paramTable2 = AuxConectionFactory.llamameConsumo(params);

        doNetworkOperation(Server.OP_AYUDA_PRESTAMO_CONSUMO, paramTable2,
                initConsumo.getBaseViewController(), true, new Object(), Server.isJsonValueCode.CONSUMO);

    }
}
