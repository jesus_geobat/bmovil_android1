package bancomer.api.consumo.gui.controllers;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consumo.R;
import bancomer.api.consumo.gui.delegates.DetalleCuponeraDelegate;
import bancomer.api.consumo.implementations.InitConsumo;

/**
 * Created by isaigarciamoso on 19/08/16.
 */
public class DetalleCuponeraViewController extends BaseActivity {

    private ImageButton imgBack;
    private ImageButton imgCuponera;
    private ImageButton consumo_ic_back;
    private Button btnObtenerCupones;
    private TextView tittleHeader;
    private TextView cantidadCupones;
    private TextView linkBancomerVida;
    private HorizontalScrollView layoutHorizontalScroll;
    private LinearLayout layoutPrincipal;
    private LinearLayout layoutCupon;
    private RelativeLayout anteriorHeader;
    private RelativeLayout itemCuponCredito;
    private BaseViewController baseViewController;
    private BaseViewsController parentManager;
    private DetalleCuponeraDelegate delegateCuponera;
    private ListView listaVivaIdeas;
    private TextView titulo_cupon;
    private TextView detalle_cupon;
    private TextView tipo_cupon;
    private TextView porcentaje_cupon;
    private TextView descuento_cupon;
    private InitConsumo init = InitConsumo.getInstance();
    private Button buttonItemCuponera;
    private TextView numeroCredito;
    private TextView textExample;
    private CuponAdapter<String> adaptador;
    private ArrayList lista = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegateCuponera = (DetalleCuponeraDelegate) parentManager.getBaseDelegateForKey(
                DetalleCuponeraDelegate.KEY_UNIQUE_DELEGATE_DETALLE_CUPONERA);

        delegateCuponera.setController(this);
        baseViewController.setActivity(this);
        baseViewController.onCreate(this, baseViewController.SHOW_HEADER, R.layout.cuponera_detalle);
        baseViewController.setDelegate(delegateCuponera);
        init.setBaseViewController(baseViewController);
        initViews();

    }
    @Override
    protected void onResume() {
        super.onResume();
        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        init.getParentManager().setActivityChanging(true);
        init.showExitoConsumo(delegateCuponera.getAceptaOferta(),delegateCuponera.getOfertaConsumo()
                ,delegateCuponera.getListaCupones(),delegateCuponera.getCupon());
    }

    public void findViews() {

        imgBack = (ImageButton) findViewById(R.id.consumo_ic_back);
        imgCuponera = (ImageButton) findViewById(R.id.cuponera_icon);
        tittleHeader = (TextView) findViewById(R.id.cuponera_tittle);
        cantidadCupones = (TextView) findViewById(R.id.cantidad_cupones);
        cantidadCupones.setText("" + delegateCuponera.getListaCupones().getListaCupon().size());
        layoutCupon = (LinearLayout) findViewById(R.id.layoutcuponera_cupones);
        layoutHorizontalScroll = (HorizontalScrollView) findViewById(R.id.horizontalscroll_cupones);
        layoutPrincipal = (LinearLayout) findViewById(R.id.layout_principal);
        anteriorHeader = (RelativeLayout) findViewById(R.id.relativo_layout);
        listaVivaIdeas = (ListView) findViewById(R.id.lista_viva_ideas);
        titulo_cupon = (TextView) findViewById(R.id.titulo_cupon);
        detalle_cupon = (TextView) findViewById(R.id.detalle_cupon);
        porcentaje_cupon = (TextView) findViewById(R.id.porcentaje_cupon);
        descuento_cupon = (TextView) findViewById(R.id.descuento_cupon);

        btnObtenerCupones = (Button) findViewById(R.id.btn_obtener_cupones);
        numeroCredito = (TextView) findViewById(R.id.numeroCredito);
        linkBancomerVida = (TextView) findViewById(R.id.link_bancomer_vida);
        consumo_ic_back  = (ImageButton)findViewById(R.id.consumo_ic_back);

        if (delegateCuponera.getAceptaOferta().getNumeroCredito() != null) {
            numeroCredito.setText(reEstructuraNumeroCredito(
                    delegateCuponera.getAceptaOferta().getNumeroCredito()));
        } else {
            numeroCredito.setText("NULLO");
        }

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/tahoma.ttf");
        tittleHeader.setTypeface(typeface);


        adaptador = new CuponAdapter<String>(this,delegateCuponera.getListaCupones().getListaCupon());
        listaVivaIdeas.setAdapter(adaptador);

        Utility.setListViewHeightBasedOnChildren(listaVivaIdeas);

        btnObtenerCupones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init.getParentManager().setActivityChanging(true);
                init.showExitoCuponera(delegateCuponera.getAceptaOferta());
            }
        });
        consumo_ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init.getParentManager().setActivityChanging(true);
                init.showExitoConsumo(delegateCuponera.getAceptaOferta(),delegateCuponera.getOfertaConsumo()
                        ,delegateCuponera.getListaCupones(),delegateCuponera.getCupon());
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init.getParentManager().setActivityChanging(true);
                init.showExitoConsumo(delegateCuponera.getAceptaOferta(), delegateCuponera.getOfertaConsumo()
                        , delegateCuponera.getListaCupones(), delegateCuponera.getCupon());
            }
        });

        TextView text_view_link = (TextView)findViewById(R.id.link_bancomer_vida);

        String url = getString(R.string.detalle_cuponera_url_link_aviso);
        SpannableString content = new SpannableString(getString(R.string.detalle_cuponera_link_aviso));
        content.setSpan(new UnderlineSpan(), content.length() - (url.length()+1), content.length()-1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        content.setSpan(new ForegroundColorSpan(Color.BLUE), content.length() - (url.length()+1), content.length()-1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text_view_link.setText(content);
    }


    public void configurarPantalla() {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());

        gTools.scale(imgBack);
        gTools.scale(imgCuponera);
        gTools.scale(tittleHeader, true);
        gTools.scale(cantidadCupones, true);
        gTools.scale(layoutCupon);
        gTools.scale(layoutHorizontalScroll);
        gTools.scale(layoutPrincipal);
        gTools.scale(anteriorHeader);
        gTools.scale(titulo_cupon, true);
        gTools.scale(detalle_cupon, true);
        gTools.scale(porcentaje_cupon, true);
        gTools.scale(descuento_cupon, true);
        gTools.scale(numeroCredito, true);
        gTools.scale(linkBancomerVida, true);
        gTools.scale(itemCuponCredito);
        gTools.scale(listaVivaIdeas);
    }

    public void initViews(){

        findViews();
        configurarPantalla();

    }

    /****** :::::::::::::::::OTROS METODOS::::::::::*********/
    //Da un nuevo formato al numero de credito
    private String reEstructuraNumeroCredito(String numeroCredito) {
        String parte1 = numeroCredito.substring(0, 8);
        String parte3 = numeroCredito.substring(8, 18);
        String parte2 = numeroCredito.substring(18, 20);
        String reStructura = parte1 + parte2 + parte3;
        return reStructura;
    }

    @Override
    public void onUserInteraction() {
       // super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }
}
