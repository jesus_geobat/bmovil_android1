package bancomer.api.consumo.gui.controllers;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consumo.R;
import bancomer.api.consumo.commons.ConsumoTools;
import bancomer.api.consumo.gui.delegates.AdelantoNominaContratoDelegate;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.tracking.TrackingConsumo;

/**
 * Created by Packo on 01/03/2016.
 */
public class AdelantoNominaContratoViewController extends BaseActivity implements View.OnClickListener {

    //  Componentes de la vista Contratacion

    private TextView lblTextoOferta;
    private TextView contrato1;
    private TextView contrato2;
    private TextView contrato3;
    private TextView txtaviso_token;
    private TextView campoContrasena;
    private TextView campoTarjeta;
    private TextView instruccionesTarjeta;
    private TextView campoNIP;
    private TextView instruccionesNIP;
    private TextView campoASM;
    private TextView instruccionesASM;
    private TextView campoCVV;
    private TextView instruccionesCVV;
    private TextView instruccionesContrasena;

    private EditText contrasena;
    private EditText tarjeta;
    private EditText nip;
    private EditText asm;
    private EditText cvv;

    private CheckBox cbConsumo1;
    private CheckBox cbConsumo2;

    private LinearLayout linearchecKbox1;
    private LinearLayout layout_contrato2;
    private LinearLayout layout_avisotoken;
    private LinearLayout confirmacion_campos_layout;
    private LinearLayout contenedorContrasena;
    private LinearLayout contenedorCampoTarjeta;
    private LinearLayout contenedorNIP;
    private LinearLayout contenedorASM;
    private LinearLayout contenedorCVV;
    private ImageButton btnconfirmar;

    private InitConsumo init = InitConsumo.getInstance();
    private ImageButton imgaviso_token;
    private Context oContext;
    private AdelantoNominaContratoDelegate delegate;

    private ImageButton iconBack;
    private ImageButton iconCallmeBack;
    private ImageView pGlobalHeader;

    private BaseViewController baseViewController;
    public BaseViewsController parentManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        parentManager.setCurrentActivityApp(this,true);

        delegate = (AdelantoNominaContratoDelegate) parentManager.getBaseDelegateForKey(AdelantoNominaContratoDelegate.ADELANTO_NOMINA_CONTRATO_DELEGATE);
        delegate.setController(this);
        oContext = this;

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.adelanto_nomina_contrato);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        initView();

        cbConsumo1.setClickable(true);
        cbConsumo2.setClickable(false);

        String monto = Tools.formatAmount(delegate.getOfertaConsumo().getImporte(),false);
        String contratoTxt2 = getResources().getString(R.string.api_contrato_adelanto_nomina_termino_2)+" ";
        String auxMonto = "<font color=\"#000000\">"+contratoTxt2+"</font>"+"" +
                          "<font color=\"#f6891e\">"+monto+"</font> <br>";

        contrato2.setText(Html.fromHtml(auxMonto),TextView.BufferType.SPANNABLE);
    }

    private void findViews() {

        layout_avisotoken = (LinearLayout) findViewById(R.id.layout_avisotoken);
        confirmacion_campos_layout = (LinearLayout) findViewById(R.id.confirmacion_campos_layout);
        linearchecKbox1 = (LinearLayout) findViewById(R.id.linearchecKbox1);
        layout_contrato2 = (LinearLayout) findViewById(R.id.layout_contrato2);
        lblTextoOferta = (TextView) findViewById(R.id.lblTextoOferta);
        contrato1 = (TextView) findViewById(R.id.contrato1);
        contrato2 = (TextView) findViewById(R.id.contrato2);

        contrato3 = (TextView) findViewById(R.id.contrato5);
        contrato3.setTextColor(getResources().getColor(R.color.tercer_azul));
        contrato3.setOnClickListener(this);

        txtaviso_token = (TextView) findViewById(R.id.txtaviso_token);
        imgaviso_token = (ImageButton) findViewById(R.id.imgaviso_token);
        cbConsumo1 = (CheckBox) findViewById(R.id.cbConsumo1);
        cbConsumo2 = (CheckBox) findViewById(R.id.cbConsumo2);

        contenedorContrasena = (LinearLayout) findViewById(R.id.campo_confirmacion_contrasena_layout);
        contenedorNIP = (LinearLayout) findViewById(R.id.campo_confirmacion_nip_layout);
        contenedorASM = (LinearLayout) findViewById(R.id.campo_confirmacion_asm_layout);
        contenedorCVV = (LinearLayout) findViewById(R.id.campo_confirmacion_cvv_layout);

        contrasena = (EditText) contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
        nip = (EditText) contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
        asm = (EditText) contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
        cvv = (EditText) contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);

        campoContrasena = (TextView) contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
        campoNIP = (TextView) contenedorNIP.findViewById(R.id.confirmacion_nip_label);
        campoASM = (TextView) contenedorASM.findViewById(R.id.confirmacion_asm_label);
        campoCVV = (TextView) contenedorCVV.findViewById(R.id.confirmacion_cvv_label);

        instruccionesContrasena = (TextView) contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
        instruccionesNIP = (TextView) contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
        instruccionesASM = (TextView) contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
        instruccionesCVV = (TextView) contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);

        contenedorCampoTarjeta = (LinearLayout) findViewById(R.id.campo_confirmacion_campotarjeta_layout);
        tarjeta = (EditText) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
        campoTarjeta = (TextView) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
        instruccionesTarjeta = (TextView) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);

        btnconfirmar = (ImageButton) findViewById(R.id.confirmacion_confirmar_button);
        btnconfirmar.setOnClickListener(this);

        iconBack = (ImageButton) findViewById(R.id.consumo_ic_back);
        iconCallmeBack = (ImageButton) findViewById(R.id.consumo_ic_callmeback);
        pGlobalHeader = (ImageView) findViewById(R.id.pGlobalHeader);

        iconBack.setVisibility(View.GONE);
        iconCallmeBack.setVisibility(View.GONE);
    }

    public void initView() {
        findViews();
        setTextToView();
        configurarPantalla();
    }

    public void setTextToView() {

        mostrarContrasena(delegate.mostrarContrasenia());
        mostrarNIP(delegate.mostrarNIP());
        mostrarASM(delegate.tokenAMostrar());
        mostrarCVV(delegate.mostrarCVV());
        mostrarCampoTarjeta(delegate.mostrarCampoTarjeta());
    }

    public void mostrarContrasena(boolean isVisible){

        contenedorContrasena.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        campoContrasena.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        contrasena.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        if(isVisible){
            campoContrasena.setText(delegate.getEtiquetaCampoContrasenia());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
            contrasena.setFilters(userFilterArray);
            contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
        }else {
            contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
        instruccionesContrasena.setVisibility(View.GONE);
    }

    public void mostrarNIP(boolean isVisible){

        contenedorNIP.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        campoNIP.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        nip.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        if (isVisible) {
            campoNIP.setText(delegate.getEtiquetaCampoNip());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
            nip.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(tarjeta);
            nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = delegate.getTextoAyudaNIP();
            if (instrucciones.equals("")) {
                instruccionesNIP.setVisibility(View.GONE);
            } else {
                instruccionesNIP.setVisibility(View.VISIBLE);
                instruccionesNIP.setText(instrucciones);
            }
        } else {
            nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    private void cambiarAccionTexto(EditText campo) {
        if (campo.getVisibility() == View.VISIBLE) {
            campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        }
    }

    public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){

        switch(tipoOTP) {
            case ninguno:
                contenedorASM.setVisibility(View.GONE);
                campoASM.setVisibility(View.GONE);
                asm.setVisibility(View.GONE);
                asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
                break;
            case codigo:
            case registro:
                contenedorASM.setVisibility(View.VISIBLE);
                campoASM.setVisibility(View.VISIBLE);
                asm.setVisibility(View.VISIBLE);
                InputFilter[] userFilterArray = new InputFilter[1];
                userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
                asm.setFilters(userFilterArray);
                cambiarAccionTexto(contrasena);
                cambiarAccionTexto(tarjeta);
                cambiarAccionTexto(nip);
                asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
                break;
        }

        Constants.TipoInstrumento tipoInstrumento = delegate.consultaTipoInstrumentoSeguridad();

        switch (tipoInstrumento) {
            case OCRA:
                campoASM.setText(delegate.getEtiquetaCampoOCRA());
                asm.setTransformationMethod(null);
                break;
            case DP270:
                campoASM.setText(delegate.getEtiquetaCampoDP270());
                asm.setTransformationMethod(null);
                break;
            case SoftToken:
                if(init.getSofTokenStatus()) {
                    asm.setText(Constants.DUMMY_OTP);
                    asm.setEnabled(false);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
                } else {
                    asm.setText("");
                    asm.setEnabled(true);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
                    asm.setTransformationMethod(null);
                }

                break;
            default:
                break;
        }
        String instrucciones = delegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento,init.getSofTokenStatus(),delegate.tokenAMostrar());
        if (instrucciones.equals("")) {
            instruccionesASM.setVisibility(View.GONE);
        } else {
            instruccionesASM.setVisibility(View.VISIBLE);
            instruccionesASM.setText(instrucciones);
        }
    }

    public void mostrarCVV(boolean isVisible){

        contenedorCVV.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        campoCVV.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        cvv.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        instruccionesCVV.setVisibility(isVisible ? View.VISIBLE : View.GONE);

        if (isVisible) {
            campoCVV.setText(delegate.getEtiquetaCampoCVV());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
            cvv.setFilters(userFilterArray);
            String instrucciones = delegate.getTextoAyudaCVV();
            instruccionesCVV.setText(instrucciones);
            instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(nip);
            cambiarAccionTexto(asm);
            cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    public void mostrarCampoTarjeta(boolean isVisible){

        contenedorCampoTarjeta.setVisibility(isVisible ? View.VISIBLE: View.GONE);
        campoTarjeta.setVisibility(isVisible ? View.VISIBLE: View.GONE);
        tarjeta.setVisibility(isVisible ? View.VISIBLE: View.GONE);
        if (isVisible) {
            campoTarjeta.setText(delegate.getEtiquetaCampoTarjeta());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(5);
            tarjeta.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = delegate.getTextoAyudaTarjeta();
            if (instrucciones.equals("")) {
                instruccionesTarjeta.setVisibility(View.GONE);
            } else {
                instruccionesTarjeta.setVisibility(View.VISIBLE);
                instruccionesTarjeta.setText(instrucciones);
            }
        } else {
            tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    public void configurarPantalla() {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(lblTextoOferta, true);
        gTools.scale(contrato1, true);
        gTools.scale(contrato2, true);
        gTools.scale(contrato3, true);
        gTools.scale(cbConsumo1);
        gTools.scale(cbConsumo2);
        gTools.scale(btnconfirmar);
        gTools.scale(layout_avisotoken);
        gTools.scale(linearchecKbox1);
        gTools.scale(txtaviso_token, true);
        gTools.scale(imgaviso_token);
        gTools.scale(confirmacion_campos_layout);

        gTools.scale(contenedorContrasena);
        gTools.scale(contenedorNIP);
        gTools.scale(contenedorASM);
        gTools.scale(contenedorCVV);

        gTools.scale(contrasena, true);
        gTools.scale(nip, true);
        gTools.scale(asm, true);
        gTools.scale(cvv, true);

        gTools.scale(campoContrasena, true);
        gTools.scale(campoNIP, true);
        gTools.scale(campoASM, true);
        gTools.scale(campoCVV, true);

        gTools.scale(instruccionesContrasena, true);
        gTools.scale(instruccionesNIP, true);
        gTools.scale(instruccionesASM, true);
        gTools.scale(instruccionesCVV, true);

        gTools.scale(contenedorCampoTarjeta);
        gTools.scale(tarjeta, true);
        gTools.scale(campoTarjeta, true);
        gTools.scale(instruccionesTarjeta, true);

    }

    public void onCheckboxClick(View sender){
        if(sender == cbConsumo1){
            if(cbConsumo1.isChecked()){
                cbConsumo1.setClickable(false);
                cbConsumo1.setBackground(getResources().getDrawable(R.drawable.ic_checkon));
                cbConsumo2.setClickable(true);
            }
        }else if(sender == cbConsumo2){
            if(cbConsumo2.isChecked()){
                cbConsumo2.setClickable(false);
                cbConsumo2.setBackground(getResources().getDrawable(R.drawable.ic_checkon));
            }else{
                baseViewController.showInformationAlert(AdelantoNominaContratoViewController.this,"Debes aceptar primero el cargo automático");
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            delegate.confirmaRechazoAtras();
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        if(btnconfirmar == v) {

            if (!cbConsumo1.isChecked() && !cbConsumo2.isChecked()) { //ninguno está activado
                baseViewController.showInformationAlert(AdelantoNominaContratoViewController.this, "No se ha aceptado el cargo automático");
            } else if (cbConsumo1.isChecked() && !cbConsumo2.isChecked()) {
                baseViewController.showInformationAlert(AdelantoNominaContratoViewController.this, "No se ha aceptado los términos y condiciones");
            } else {
                if (delegate.enviaPeticionOPeracion()) {
                    init.getParentManager().setActivityChanging(true);
//                    delegate.realizaOperacion(Server.CONTRATA_ALTERNATIVA_CONSUMO, 1);
                }
            }
        }

        if(v == contrato3){
            android.util.Log.i("Contrato", "mostrar terminos y condiciones");
//            TrackingConsumo.trackPaso1Operacion(paso1OperacionMap);
            init.getParentManager().setActivityChanging(true);
            delegate.realizaOperacion(Server.TERMINOS_OFERTA_CONSUMO, 0);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        /*if(!init.getParentManager().isActivityChanging()) {
          //  TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }*/
    }

    @Override
    public void onUserInteraction() {
        //super.onUserInteraction();
      //  init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);
    }

    public String pideContrasena(){
        if(contrasena.getVisibility() == View.GONE)
            return "";
        else return contrasena.getText().toString();
    }

    public String pideNIP(){
        if(nip.getVisibility() == View.GONE)
            return "";
        else return nip.getText().toString();
    }

    public String pideASM(){
        if(asm.getVisibility() == View.GONE)
            return "";
        else return asm.getText().toString();
    }

    public String pideCVV(){
        if(cvv.getVisibility() == View.GONE)
            return "";
        else return cvv.getText().toString();
    }

    public String pideTarjeta(){
        if(tarjeta.getVisibility() == View.GONE)
            return "";
        else return tarjeta.getText().toString();
    }
}
