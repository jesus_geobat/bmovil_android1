package bancomer.api.consumo.gui.controllers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.concurrent.atomic.AtomicBoolean;

import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.consumo.R;

import bancomer.api.consumo.gui.delegates.ContratoOfertaConsumoDelegate;
import bancomer.api.consumo.gui.delegates.DetalleOfertaConsumoDelegate;
import bancomer.api.consumo.gui.delegates.PolizaDelegate;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consumo.models.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ControlEventos;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;


public class PolizaViewController extends Activity {

    private BaseViewController baseViewController;
    private InitConsumo init = InitConsumo.getInstance();
    public BaseViewsController parentManager;
    private AlertDialog mAlertDialog;
    private WebView wvpoliza;
    private GestureDetector gestureDetector;
    private AtomicBoolean mPreventAction = new AtomicBoolean(false);
    private Activity oActivity;
    private ContratoOfertaConsumoDelegate delegateContrato;
    private DetalleOfertaConsumoDelegate delegadoDetalle;
    private ImageView divider;
    TextView lblTitulo;
    String poliza;
    String termino;
    String contrato;
    String domiciliacion;
    private ImageButton iconBack;
    private ImageButton iconCallmeBack;
    private int posPantallaEntrante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentManager = init.getParentManager();
        oActivity = parentManager.getCurrentViewControllerApp();

        Log.i("poliza", "recuperando getCurrentViewControllerApp: " + parentManager.getCurrentViewControllerApp());
        Log.i("poliza", "recuperando getRootViewController: " + parentManager.getRootViewController());
        Log.i("poliza", "recuperando getLastDelegateKey: " + parentManager.getLastDelegateKey());

        init.getParentManager().setCurrentActivityApp(this);

        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_consumo_poliza);
        baseViewController.setTitle(this, R.string.bmovil_promocion_title_consumo, R.drawable.icono_consumo);
        init.setBaseViewController(baseViewController);

        poliza = (String) this.getIntent().getExtras().get(Constants.TERMINOS_DE_USO_EXTRA);
        termino = (String) this.getIntent().getExtras().get(Constants.TERMINOS_DE_USO_CONSUMO);
        contrato = (String) this.getIntent().getExtras().get(Constants.CONTRATO_CONSUMO);
        domiciliacion = (String) this.getIntent().getExtras().get(Constants.DOMICILIACION_CONSUMO);

        ViewGroup scroll = (ViewGroup) findViewById(R.id.body_layout);
        ViewGroup parent = (ViewGroup) scroll.getParent();


        parent.removeView(scroll);

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.api_consumo_poliza, parent, true);

        divider = (ImageView) findViewById(R.id.title_divider);
        divider.setVisibility(View.GONE);

        findViews();
        scaleToScreenSize();


        WebSettings settings = wvpoliza.getSettings();
        String result;
        if (null != poliza) {
            wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            wvpoliza.loadUrl(poliza);

        } else if (null != termino) {
            wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            wvpoliza.getSettings().setBuiltInZoomControls(true);
            result = parse(termino);
            settings.setUseWideViewPort(true);
            double factor = GuiTools.getScaleFactor();
            wvpoliza.setInitialScale((int) (35 * factor));
            wvpoliza.loadData(result, "text/html", "utf-8");
        } else if (null != contrato) {
            result = parse(contrato);
            settings.setUseWideViewPort(true);
            double factor = GuiTools.getScaleFactor();
            wvpoliza.setInitialScale((int) (122 * factor));
            wvpoliza.loadData(result, "text/html", "utf-8");
        } else if (null != domiciliacion) {
            wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            wvpoliza.getSettings().setBuiltInZoomControls(true);
            wvpoliza.loadData(domiciliacion, "text/html", "utf-8");

        }

        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                mPreventAction.set(true);
                return true;
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                mPreventAction.set(true);
                return true;
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                mPreventAction.set(true); // this is the key! this would block double tap to zoom to fire
                return false;
            }

        });
        gestureDetector.setIsLongpressEnabled(true);
        wvpoliza.setOnTouchListener(new View.OnTouchListener() {
            @TargetApi(Build.VERSION_CODES.FROYO)
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int pointId = 0;
                if (Build.VERSION.SDK_INT > 7) {
                    int index = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                    pointId = event.getPointerId(index);
                }
                if (pointId == 0) {
                    gestureDetector.onTouchEvent(event);
                    if (mPreventAction.get()) {
                        mPreventAction.set(false);
                        return true;
                    }
                    return wvpoliza.onTouchEvent(event);
                } else return true;
            }
        });

    }
    private String parse(String source) {
        StringBuilder data = new StringBuilder();
        data.append(source);
        data.insert(data.indexOf("head") + 7, "<meta name=\"viewport\" content=\"height=device-height,width=1280\"/>");
        return data.toString().replace("divstyle", "div style").replace("tdcolspan", "td colspan")
                .replace("tdwidth", "td width")
                .replace("solid1px", "solid 1px")
                .replace("width:801px", " width:800px")
                .replace("</body></html><html><head><title>Contrato - Contrato Credit&oacute;n N&oacute;mina sin Seguro</title></head><body>", "")
                ;
    }
    private void findViews() {
        wvpoliza = (WebView) findViewById(R.id.poliza);
        lblTitulo = (TextView) findViewById(R.id.lblTitulo);
        iconBack =  (ImageButton)findViewById(R.id.consumo_ic_back);
        iconCallmeBack = (ImageButton)findViewById(R.id.consumo_ic_callmeback);
        iconCallmeBack.setVisibility(View.INVISIBLE);
        /****  Icon CallmeBack    **/
        iconCallmeBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showErrorMessage("¡En breve, uno de nuestros asesores se pondrá en contacto contigo!",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
            }
        });
        iconBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init.getParentManager().setActivityChanging(true);
                onBackPressed();
            }
        });


        if (null != termino)
            lblTitulo.setText(R.string.bmovil_contratoconsumo_terminosconsumo_web);
        else if (null != contrato)
            lblTitulo.setText(R.string.bmovil_contratoconsumo_link1);
        else if (null != domiciliacion)
            lblTitulo.setText(R.string.bmovil_contratoconsumo_link4);

        validaSimulador();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void validaSimulador(){
        if(init.getOfertaDelSimulador()){
            iconBack.setVisibility(View.GONE);
            iconCallmeBack.setVisibility(View.GONE);
        }
    }

    private void scaleToScreenSize() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.layoutBaseContainer));
        guiTools.scale(findViewById(R.id.lblTitulo), true);
        guiTools.scale(wvpoliza);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        float touchX = ev.getX();
        float touchY = ev.getY();
        int[] webViewPos = new int[2];

        wvpoliza.getLocationOnScreen(webViewPos);

        float listaSeleccionX2 = webViewPos[0] + wvpoliza.getMeasuredWidth();
        float listaSeleccionY2 = webViewPos[1] + wvpoliza.getMeasuredHeight();

        if ((touchX >= webViewPos[0] && touchX <= listaSeleccionX2) &&
                (touchY >= webViewPos[1] && touchY <= listaSeleccionY2)) {
            wvpoliza.getParent().requestDisallowInterceptTouchEvent(true);
        }

        return super.dispatchTouchEvent(ev);
    }
    @Override
    protected void onPause() {
        super.onPause();
        /*if (!init.getParentManager().isActivityChanging()) {
            //TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
        }*/

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            if(oActivity instanceof AdelantoNominaContratoViewController){
                init.showContratoANViewController(init.getResOfertaConsumo(),init.getRespPromociones());
                init.getParentManager().setActivityChanging(true);

            }else {
                init.getParentManager().setActivityChanging(true);
                super.onBackPressed();//cambio boton back
            }
        }
        return true;
    }

    @Override
    public void onUserInteraction() {
        //super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }
    /****:::::::::::::::::::::: Muestra Contrato ::::::::::::::: **/
    public void showContratoFromPoliza(){

        delegadoDetalle = (DetalleOfertaConsumoDelegate) parentManager.getBaseDelegateForKey(
                DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE);

        init.setChanged(true);
        init.getParentManager().setActivityChanging(true);
        init.showContratoConsumo(delegadoDetalle.getOfertaConsumo(), delegadoDetalle.getPromocion());
    }

    /***:::::::::::::: Dialog Error:::::::::::::::::::::: **/
    public void showErrorMessage(String errorMessage, DialogInterface.OnClickListener listener) {

        if (errorMessage.length() > 0) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.label_error);
            alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
            alertDialogBuilder.setMessage(errorMessage);
            alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.show();
        }
    }

    @Override
    protected void onStop() {
        if(ControlEventos.isAppIsInBackground(this.getBaseContext())){ //Valida el estado de la app si es change Activity no hace nada
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "La app se fue a Background");
            }
            TimerController.getInstance().initTimer(this, TimerController.getInstance().getClase());

        }
        super.onStop();
    }

}
