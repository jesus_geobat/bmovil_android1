package bancomer.api.consumo.models;

import android.text.Html;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by OmarOrozco on 14/10/16.
 */
public class ListaCupones implements ParsingHandler {

    private  static  final String LISTA_CUPONES = "listaCupones";
    private List<Cupon> listaCupon;
    private Cupon cupon;

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        try {
            JSONArray jsonCupon = parser.parseNextValueWithArray(LISTA_CUPONES);
            listaCupon = new ArrayList<Cupon>();
            for (int i = 0; i < jsonCupon.length(); i++) {
                JSONObject jsonDatos = jsonCupon.getJSONObject(i);
                cupon = new Cupon(
                        jsonDatos.getString("nombreComercio"),
                        jsonDatos.getString("desCupon"),
                        jsonDatos.getString("desCategoria"),
                        jsonDatos.getString("porcentajeDes"),
                        jsonDatos.getString("codLogo"),
                        jsonDatos.getString("desGiro"));
                listaCupon.add(cupon);
                System.out.println(cupon.codigoLogo);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public Cupon getCupon() {
        return cupon;
    }

    public void setCupon(Cupon cupon) {
        this.cupon = cupon;
    }

    public static class  Cupon{

        private String nombreComercio;
        private String desCupon;
        private String desCategoria;
        private String porcentajeDes;
        private String codigoLogo;
        private String desGiro;


        public Cupon(){

        }
        public Cupon(String nombreComercio,String desCupon, String desCategoria,String porcentajeDes, String codigoLogo, String desGiro){
            this.nombreComercio = Html.fromHtml(nombreComercio).toString();
            this.desCupon =  Html.fromHtml(desCupon).toString();
            this.desCategoria = desCategoria;
            this.porcentajeDes =  porcentajeDes;
            this.codigoLogo =  codigoLogo;
            this.desGiro = formatDesGiro(desGiro);

        }

        public String getNombreComercio() {
            return nombreComercio;
        }

        public void setNombreComercio(String nombreComercio) {
            this.nombreComercio = nombreComercio;
        }

        public String getDesCupon() {
            return desCupon;
        }

        public void setDesCupon(String desCupon) {
            this.desCupon = desCupon;
        }

        public String getDesCategoria() {
            return desCategoria;
        }

        public void setDesCategoria(String desCategoria) {
            this.desCategoria = desCategoria;
        }

        public String getPorcentajeDes() {
            return porcentajeDes;
        }

        public void setPorcentajeDes(String porcentajeDes) {
            this.porcentajeDes = porcentajeDes;
        }

        public String getCodigoLogo() {
            return codigoLogo;
        }

        public void setCodigoLogo(String codigoLogo) {
            this.codigoLogo = codigoLogo;
        }

        public String getDesGiro() {
            return desGiro;
        }

        public void setDesGiro(String desGiro) {
            this.desGiro = desGiro;
        }


        private String formatDesGiro(String desGiro)
        {
            if(desGiro != null)
            {
                if(desGiro.trim().length()>0)
                {
                    if(desGiro.contains("/"))
                    {
                        String auxiliar [] = desGiro.split("/");
                        StringBuffer buffer = new StringBuffer();

                        int index;

                        for(index = 0; index < auxiliar.length-1; index ++)
                        {
                            buffer.append(auxiliar[index].charAt(0)+auxiliar[index].substring(1,auxiliar[index].length()).toLowerCase()+",");
                        }

                        buffer.append(auxiliar[index].charAt(0)+auxiliar[index].substring(1,auxiliar[index].length()).toLowerCase());

                        desGiro = buffer.toString();

                    }
                    else
                    {
                        desGiro = desGiro.charAt(0) + desGiro.substring(1,desGiro.length()).toLowerCase();
                    }

                    return desGiro;
                }
                else
                {
                    return  "";
                }
            }
            else
                return "";

        }
    }

    public List<Cupon> getListaCupon() {
        return listaCupon;
    }

    public void setListaCupon(List<Cupon> listaCupon) {
        this.listaCupon = listaCupon;
    }
}
