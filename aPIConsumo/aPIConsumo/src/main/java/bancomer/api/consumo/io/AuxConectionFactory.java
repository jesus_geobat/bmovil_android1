package bancomer.api.consumo.io;

import java.util.Hashtable;

public class AuxConectionFactory {

    public static Hashtable consultaDetalleConsumo(Hashtable<String, ?> params) {

        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("claveCamp",validaNull(params.get("claveCamp")));
        paramNew.put("IUM",validaNull(params.get("IUM")));
        paramNew.put("importePar",validaNull(params.get("importePar")));

        return paramNew;
    }

    public static Hashtable polizaConsumo(Hashtable<String, ?> params){

        Hashtable<String, String> paramNew= new Hashtable<String, String>();
        paramNew.put("IdProducto",validaNull(params.get("IdProducto")));

        return paramNew;
    }

    public static Hashtable terminosConsumo(Hashtable<String, ?> params){

        Hashtable<String, String> paramNew= new Hashtable<String, String>();
        paramNew.put("opcion",validaNull(params.get("opcion")));
        paramNew.put("IdProducto",validaNull(params.get("IdProducto")));
        paramNew.put("versionE",validaNull(params.get("versionE")));
        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("IUM",validaNull(params.get("IUM")));

        return paramNew;
    }

    public static Hashtable exitoConsumo(Hashtable<String, ?> params, boolean isFromSimulador){

        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("claveCamp",validaNull(params.get("claveCamp")));
        paramNew.put("estatus",validaNull(params.get("estatus")));
        //paramNew.put("IdProductoBuro",validaNull(params.get("IdProductoBuro")));
        paramNew.put("codigoOTP",validaNull(params.get("codigoOTP")));
        paramNew.put("cadenaAutenticacion",validaNull(params.get("cadenaAutenticacion")));
        paramNew.put("seguroObli",validaNull(params.get("seguroObli")));
        paramNew.put("IUM",validaNull(params.get("IUM")));
        paramNew.put("importePar",validaNull(params.get("importePar")));
        paramNew.put("codigoNIP",validaNull(params.get("codigoNIP")));
        paramNew.put("codigoCVV2",validaNull(params.get("codigoCVV2")));
        paramNew.put("cveAcceso",validaNull(params.get("cveAcceso")));
        paramNew.put("tarjeta5Dig",validaNull(params.get("tarjeta5Dig")));
        if(isFromSimulador) {
            paramNew.put("producto", validaNull(params.get("producto")));
            paramNew.put("IndRev",validaNull(params.get("IndRev")));
        }

        return paramNew;
    }


    public static Hashtable domiciliacionConsumo(Hashtable<String, ?> params) {

        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        paramNew.put("IdProducto",validaNull(params.get("IdProducto")));
        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("numCredito",validaNull(params.get("numCredito")));
        paramNew.put("indicad",validaNull(params.get("indicad")));
        paramNew.put("bscPagar",validaNull(params.get("bscPagar")));
        paramNew.put("IUM",validaNull(params.get("IUM")));

        return paramNew;
    }


    public static Hashtable contratoConsumo(Hashtable<String, ?> params){

        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        paramNew.put("IdProducto",validaNull(params.get("IdProducto")));
        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("numCredito",validaNull(params.get("numCredito")));
        paramNew.put("indicad",validaNull(params.get("indicad")));
        paramNew.put("plazoSal",validaNull(params.get("plazoSal")));
        paramNew.put("IUM",validaNull(params.get("IUM")));

        return paramNew;
    }


    public static Hashtable rechazoOfertaConsumo(Hashtable<String, ?> params){

        Hashtable<String, String> paramNew= new Hashtable<String, String>();


        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("cveCamp",validaNull(params.get("cveCamp")));
        paramNew.put("descripcionMensaje",validaNull(params.get("descripcionMensaje")));
        paramNew.put("folioUG",validaNull(params.get("folioUG")));
        paramNew.put("IUM",validaNull(params.get("IUM")));

        return paramNew;
    }

    public static Hashtable envioSMSOfertaConsumo(Hashtable<String, ?> params){

        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("numCredito",validaNull(params.get("numCredito")));
        paramNew.put("idProducto",validaNull(params.get("idProducto")));
        paramNew.put("estatusContratacion",validaNull(params.get("estatusContratacion")));
        paramNew.put("cuentaVinc",validaNull(params.get("cuentaVinc")));
        paramNew.put("fechaCat",validaNull(params.get("fechaCat")));
        paramNew.put("Importe",validaNull(params.get("Importe")));
        paramNew.put("plazoDes",validaNull(params.get("plazoDes")));
        paramNew.put("pagoMensualFijo",validaNull(params.get("pagoMensualFijo")));
        paramNew.put("email",validaNull(params.get("email")));
        paramNew.put("Cat",validaNull(params.get("Cat")));
        paramNew.put("mensajeA",validaNull(params.get("mensajeA")));
        paramNew.put("IUM",validaNull(params.get("IUM")));

        return paramNew;
    }


    public static Hashtable actualizarCuentas(Hashtable<String, ?> params) {
        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        paramNew.put("numeroTelefono",validaNull(params.get("numeroTelefono")));
        paramNew.put("numeroCliente",validaNull(params.get("numeroCliente")));
        paramNew.put("IUM",validaNull(params.get("IUM")));

        return paramNew;
    }

   /**Añadido formato de datos de llamameConsumo*/
    public static Hashtable llamameConsumo(Hashtable<String, ?> params) {
        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("claveCamp",validaNull(params.get("claveCamp")));
        paramNew.put("IUM",validaNull(params.get("IUM")));
        paramNew.put("importePar",validaNull(params.get("importePar")));

        return paramNew;
    }
    //enviar mail y SMS cuponera
    public static Hashtable smsMailCuponera(Hashtable<String, ?> params) {
        Hashtable<String, String> paramNew= new Hashtable<String, String>();
        paramNew.put("operacion",validaNull(params.get("operacion")));
        paramNew.put("IUM",validaNull(params.get("IUM")));
        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("numCredito",validaNull(params.get("numCredito")));
        paramNew.put("email",validaNull(params.get("email")));
        paramNew.put("crCredito",validaNull(params.get("crCredito")));

        return paramNew;
    }
    //obtener Numero de creditos de la cuponera
    public static Hashtable creditosCuponeras(Hashtable<String, ?> params) {
        Hashtable<String, String> paramNew= new Hashtable<String, String>();
        paramNew.put("codigoOTP",validaNull(params.get("codigoOTP")));
        paramNew.put("codigoNIP",validaNull(params.get("codigoNIP")));
        paramNew.put("operacion",validaNull(params.get("operacion")));
        paramNew.put("numeroCelular",validaNull(params.get("numeroCelular")));
        paramNew.put("tarjeta5Dig",validaNull(params.get("tarjeta5Dig")));
        paramNew.put("IUM",validaNull(params.get("IUM")));
        paramNew.put("codigoCVV2",validaNull(params.get("codigoCVV2")));
        paramNew.put("cadenaAutenticacion",validaNull(params.get("cadenaAutenticacion")));
        paramNew.put("cveAcceso",validaNull(params.get("cveAcceso")));

        return paramNew;
    }

    private static String validaNull(Object valor){
        if(null==valor){
            return "";
        }else{
            return String.valueOf(valor);
        }
    }

}



