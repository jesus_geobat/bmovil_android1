package bancomer.api.consumo.gui.controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consumo.R;
import bancomer.api.common.commons.GuiTools;

import bancomer.api.consumo.gui.delegates.ExitoOfertaConsumoDelegate;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.tracking.TrackingConsumo;


public class ExitoOfertaConsumoViewController extends BaseActivity {

    private BaseViewController baseViewController;
    private InitConsumo init = InitConsumo.getInstance();
    private ExitoOfertaConsumoDelegate delegate;
    public BaseViewsController parentManager;

    private TextView lblTextoOferta;
    private TextView lblTextoAyudaOfertaPreformalizada;
    private TextView lblTextoAyudaOferta;
    private TextView lblCorreoOferta;
    private EditText editTextemail;
    private TextView lblsmsOferta;
    private EditText editTextNumero;
    private ImageButton btnmMenu;
    private TextView link1;
    private TextView link2;
    private TextView lblinf1;
    private TextView lblinf2;
    private TextView lblTextoAyuda2;
    private TextView imprimir;
    private LinearLayout vista;
    private ImageView divider;
    public boolean isSMSEMail;
    private TextView textoMonto;
    private TextView emailText;
    private TextView numeroText;
    private ImageButton backActivity;
    private ImageButton callmeBack;
    private TextView cuponeraLink;

    private LinearLayout lyt_contrato;
    private LinearLayout lyt_domiciliacion;

    //etiquetado Adobe
    String screen = "";
    String screen2 = "";
    String screen3 = "";
    ArrayList<String> list = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        borrarSharedPrefences();

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate = (ExitoOfertaConsumoDelegate) parentManager.getBaseDelegateForKey(ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_consumo_exito);
        baseViewController.setTitle(this, R.string.bmovil_promocion_title_consumo, R.drawable.icono_consumo);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        vista = (LinearLayout) findViewById(R.id.aceptaofertaConsumo_view_controller_layout);

        //etiquetado Adobe
        screen = "detalle consumo";
        screen2 = "clausulas consumo";
        screen3 = "exito consumo";
        list.add(screen);
        list.add(screen2);
        list.add(screen3);
        TrackingConsumo.trackState("exito consumo", list);

        initView();
    }

    public void initView() {

        delegate.realizaOperacionConsultarStatusCuponera(Server.CONSULTA_STATUS_CUPONERA,
                delegate.getAceptaOfertaConsumo().getNumeroCredito());

        findViews();
        setTextToView();
        setDataEmailSms();
        configurarPantalla();
    }

    public void findViews() {

        lblTextoOferta = (TextView) findViewById(R.id.lblTextoOferta);
        lblTextoAyudaOfertaPreformalizada = (TextView) findViewById(R.id.lblTextoAyudaOfertaPreformalizada);
        lblTextoAyudaOferta = (TextView) findViewById(R.id.lblTextoAyudaOferta);
        lblCorreoOferta = (TextView) findViewById(R.id.lblCorreoOferta);
        lblsmsOferta = (TextView) findViewById(R.id.lblsmsOferta);
        link1 = (TextView) findViewById(R.id.link1);
        link2 = (TextView) findViewById(R.id.link2);
        lblinf1 = (TextView) findViewById(R.id.lblinf1);
        lblinf2 = (TextView) findViewById(R.id.lblinf2);
        lblTextoAyuda2 = (TextView) findViewById(R.id.lblTextoAyuda2);
        textoMonto = (TextView) findViewById(R.id.text_monto_prestamo);
        imprimir = (TextView) findViewById(R.id.imprimir);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/unicode.futurabb.ttf");
        textoMonto.setTypeface(typeface);

        emailText = (TextView) findViewById(R.id.email_user);
        numeroText = (TextView) findViewById(R.id.phone_number_user);

        editTextNumero = (EditText) findViewById(R.id.editTextNumero);
        editTextemail = (EditText) findViewById(R.id.editTextemail);
        btnmMenu = (ImageButton) findViewById(R.id.btnMenu);
        cuponeraLink = (TextView) findViewById(R.id.cuponera);
        btnmMenu.setOnClickListener(clickListener);

        divider = (ImageView) findViewById(R.id.title_divider);
        divider.setVisibility(View.GONE);

        backActivity = (ImageButton) findViewById(R.id.consumo_ic_back);
        callmeBack = (ImageButton) findViewById(R.id.consumo_ic_callmeback);

        backActivity.setVisibility(View.GONE);
        callmeBack.setVisibility(View.GONE);

        lyt_contrato = (LinearLayout) findViewById(R.id.lyt_contrato);
        lyt_domiciliacion = (LinearLayout)findViewById(R.id.lyt_domiciliacion);

        cuponeraLink.setOnClickListener(clickListenerLinkCuponera);
    }

    public void setTextToView() {

        if (delegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREFORMALIZADO)) {
            String text = "¡Felicidades! \nYa tienes en tu cuenta:";
            String importe = Tools.formatAmount(delegate.getOfertaConsumo().getImporte(), false);
            int indexIm = text.indexOf(importe);
            AbsoluteSizeSpan headerSpan = new AbsoluteSizeSpan(40);
            lblTextoOferta.setText(text);
            lblTextoOferta.setPadding(0, 40, 0, 40);
            textoMonto.setText(importe);
            lblTextoAyudaOfertaPreformalizada.setVisibility(View.GONE);
            lblTextoAyudaOferta.setVisibility(View.GONE);
            textoMonto.setPadding(0, 0, 0, 90);
            imprimir.setPadding(0, 20, 0, 20);
            isSMSEMail = true;
        } else if (delegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREABPROBADO)) {

            if (delegate.getAceptaOfertaConsumo().getEstatusSal().equals("4") && delegate.getAceptaOfertaConsumo().getScoreSal().equals("AP")) {
                Log.e("dentro del IF", "PREAPROBADO AP");
                String text = "¡Felicidades! \nYa tienes en tu cuenta:";
                String importe = Tools.formatAmount(delegate.getOfertaConsumo().getImporte(), false);
                int indexIm = text.indexOf(importe);
                AbsoluteSizeSpan headerSpan = new AbsoluteSizeSpan(40);
                lblTextoOferta.setText(text);
                lblTextoOferta.setPadding(0, 40, 0, 40);
                textoMonto.setText(importe);
                lblTextoAyudaOfertaPreformalizada.setVisibility(View.GONE);
                lblTextoAyudaOferta.setVisibility(View.GONE);
                textoMonto.setPadding(0, 0, 0, 90);
                imprimir.setPadding(0, 20, 0, 20);
                isSMSEMail = true;
            }else{
                String text = "¡Felicidades!\nEn breve recibirás información\nsobre tu préstamo por:" + Tools.formatAmount(delegate.getOfertaConsumo().getImporte(), false);
                String importe = Tools.formatAmount(delegate.getOfertaConsumo().getImporte(), false);
                int indexIm = text.indexOf(importe);
                SpannableString textImporte = new SpannableString(text);
                AbsoluteSizeSpan headerSpan = new AbsoluteSizeSpan(10);
                textImporte.setSpan(headerSpan, indexIm, indexIm + importe.length(), 0);
                textImporte.setSpan(new ForegroundColorSpan(Color.rgb(255, 255, 255)), indexIm, indexIm + importe.length(), 0);
                lblTextoOferta.setText(textImporte);
                lblTextoOferta.setPadding(0, 20, 0, 10);
                String onlyCantidad[] = textImporte.toString().split(":");
                textoMonto.setText(onlyCantidad[1]);
                textoMonto.setPadding(0, 10, 0, 10);
                Log.d("Mensaje textoImporte", textImporte.toString());
                lblTextoAyudaOfertaPreformalizada.setVisibility(View.GONE);
                lyt_contrato.setVisibility(View.GONE);
                lyt_domiciliacion.setVisibility(View.GONE);
                imprimir.setText("Podrás imprimir tu Contrato, Domiciliación y Tabla de amortización en sucursal");
                imprimir.setPadding(0, 20, 0, 20);
                lblinf1.setPadding(0, 20, 0, 0);
                isSMSEMail = true;
            }
        }

        String txtlink1 = link1.getText().toString();
        SpannableString txtlink1s = new SpannableString(txtlink1);
        link1.setText(txtlink1s);
        link1.setOnClickListener(clickListener);

        String txtlink2 = link2.getText().toString();
        SpannableString txtlink2s = new SpannableString(txtlink2);
        txtlink2s.setSpan(new UnderlineSpan(), 0, txtlink2.length(), 0);
        link2.setText(txtlink2s);
        link2.setOnClickListener(clickListener);

        lblCorreoOferta.setOnClickListener(clickListener);

        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(Constants.NUMERO_TELEFONO_LENGTH);
        editTextNumero.setFilters(FilterArray);
    }

    public void setDataEmailSms() {
        ICommonSession session = init.getSession();

        if (isSMSEMail) {
            if (session.getEmail().compareTo("") == 0) {
                lblTextoAyudaOferta.setText(R.string.bmovil_texto_extoILC3);
                lblCorreoOferta.setVisibility(View.GONE);
                editTextemail.setVisibility(View.GONE);
                lblinf1.setVisibility(View.GONE);
            } else {
                emailText.setText(session.getEmail());
                numeroText.setText(session.getUsername()); //Teléfono del usuario
                editTextemail.setText(session.getEmail());
                editTextemail.setEnabled(false);
                editTextemail.setFocusable(false);
            }
        } else {
            lblCorreoOferta.setVisibility(View.GONE);
            editTextemail.setVisibility(View.GONE);
            lblinf1.setVisibility(View.GONE);
        }

        lblCorreoOferta.setVisibility(View.VISIBLE);

        String user = session.getUsername();
        StringBuffer sb = new StringBuffer("*****");
        sb.append(user.substring(user.length() - Constants.VISIBLE_NUMBER_ACCOUNT));

        editTextNumero.setText(sb);
        editTextNumero.setEnabled(false);
        editTextNumero.setFocusable(false);

        delegate.realizaOperacion(Server.SMS_OFERTA_CONSUMO, isSMSEMail);
    }

    //Onclick cuponera
    OnClickListener clickListenerLinkCuponera = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == cuponeraLink) {

                init.getParentManager().setActivityChanging(true);
                delegate.realizaOperacion(Server.CONSULTA_DETALLE_CUPONERA, false);
            }

        }
    };

    OnClickListener clickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {

            Map<String, Object> OperacionMap = new HashMap<String, Object>();
            if (v == btnmMenu) {

                OperacionMap.put("evento_realizada", "event52");
                OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo+exito consumo");
                OperacionMap.put("eVar12", "operacion realizada oferta consumo");
                TrackingConsumo.trackOperacionRealizada(OperacionMap);

                init.getParentManager().setActivityChanging(true);
                delegate.showMenu();

            } else if (v == link1) {

                OperacionMap.put("evento_realizada", "event52");
                OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo+exito consumo");
                OperacionMap.put("eVar12", "operacion realizada oferta consumo");
                TrackingConsumo.trackOperacionRealizada(OperacionMap);

                init.getParentManager().setActivityChanging(true);
                delegate.realizaOperacion(Server.CONTRATO_OFERTA_CONSUMO, false);

            } else if (v == lblCorreoOferta) {

                OperacionMap.put("evento_realizada", "event52");
                OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo+exito consumo");
                OperacionMap.put("eVar12", "operacion realizada oferta consumo");
                TrackingConsumo.trackOperacionRealizada(OperacionMap);

                init.getParentManager().setActivityChanging(true);
                delegate.realizaOperacion(Server.DOMICILIACION_OFERTA_CONSUMO, false);

            }
        }
    };

    private void configurarPantalla() {

        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());

        gTools.scale(lblTextoOferta, true);
        gTools.scale(lblTextoAyudaOferta, true);
        gTools.scale(lblCorreoOferta, true);
        gTools.scale(lblsmsOferta, true);
        gTools.scale(editTextemail, true);
        gTools.scale(editTextNumero, true);
        gTools.scale(link1, true);
        gTools.scale(link2, true);
        gTools.scale(lblinf1, true);
        gTools.scale(lblinf2, true);
        gTools.scale(lblTextoAyuda2, true);
        gTools.scale(btnmMenu);
        gTools.scale(lblTextoAyudaOfertaPreformalizada, true);
        gTools.scale(imprimir, true);
        gTools.scale(cuponeraLink, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);
    }

    @Override
    protected void onPause() {
        super.onPause();
       /* if (!init.getParentManager().isActivityChanging()) {
           // TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }*/

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
        }
        return true;
    }

    @Override
    public void onUserInteraction() {
       // super.onUserInteraction();
       // init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    /***
     * ::::::: Borrando lo del sharedPrefente ::::::
     **/
    public void borrarSharedPrefences() {
        Log.d("Borrando", "VARIABLE BUTTON CALLMEBACK");
        String preferencia = "MIS_PREFRENCIAS";
        int modoPrivacidad = Context.MODE_PRIVATE;
        SharedPreferences preferences = getSharedPreferences(preferencia, modoPrivacidad);
        preferences.edit().clear().commit();
    }

    public void setTextCuponera(String cadena){
        cuponeraLink.setVisibility(View.VISIBLE);
        cuponeraLink.setText(cadena);
    }
}
