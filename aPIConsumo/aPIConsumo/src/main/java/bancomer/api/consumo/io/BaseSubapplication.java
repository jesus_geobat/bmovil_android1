package bancomer.api.consumo.io;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.Reader;
import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.R;
import bancomer.api.consumo.commons.AbstractSuitePowerManager;
import bancomer.api.consumo.implementations.InitConsumo;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;


public abstract class BaseSubapplication {
	// #region Variables.
	/**
	 * Determines if this activity is in process of changing screen.
	 */
	protected boolean changingActivity = false;
	
	protected boolean applicationLogged = false;

	protected boolean applicationInBackground = false;

	/**
	 * Reference to the main application.
	 */
	protected Activity suiteApp = null;
	
	/**
	 * Referencia al controlador de vistas.
	 */
	protected BaseViewsController viewsController;

	/**
	 * Reference to a pending network operation, in case that an
	 * operation launched a session expired error. This operation
	 * will be relaunched after a successful login
	 */
	protected NetworkOperation pendingOperation = null;

	/**
	 * Reference to server communication handler.
	 */
	protected Server server;

	/**
	 * Pending network operations.
	 */
	protected Hashtable<Integer, NetworkOperation> networkOperationsTable = null;
	
	protected boolean isHomeKeyPressed = false;
	// #endregion
	
	// #region Getters y Setters.
	
	/**
	 * @return the changingActivity
	 */
	public boolean isChangingActivity() {
		return changingActivity;
	}

	private boolean isHomeKeyPressed() {
		return isHomeKeyPressed;
	}

	protected void setHomeKeyPressed(boolean isHomeKeyPressed) {
		this.isHomeKeyPressed = isHomeKeyPressed;
	}

	/**
	 * @param changingActivity the changingActivity to set
	 */
	public void setChangingActivity(boolean changingActivity) {
		this.changingActivity = changingActivity;
	}

	/**
	 * @return the applicationLogged
	 */
	public boolean isApplicationLogged() {
		return applicationLogged;
	}

	/**
	 * @param applicationLogged the applicationLogged to set
	 */
	public void setApplicationLogged(boolean applicationLogged) {
		this.applicationLogged = applicationLogged;
	}

	/**
	 * @return the applicationInBackground
	 */
	public boolean isApplicationInBackground() {
		return applicationInBackground;
	}

	/**
	 * @param applicationInBackground the applicationInBackground to set
	 */
	public void setApplicationInBackground(boolean applicationInBackground) {
		this.applicationInBackground = applicationInBackground;
	}

	/**
	 * @return the suiteApp
	 */
	public Activity getSuiteApp() {
		return suiteApp;
	}

	/**
	 * @param suiteApp the suiteApp to set
	 */
	public void setSuiteApp(Activity suiteApp) {
		this.suiteApp = suiteApp;
	}

	/**
	 * @return the viewsController
	 */
	public BaseViewsController getViewsController() {
		return viewsController;
	}

	/**
	 * @param viewsController the viewsController to set
	 */
	public void setViewsController(BaseViewsController viewsController) {
		this.viewsController = viewsController;
	}

	/**
	 * @return the pendingOperation
	 */
	public NetworkOperation getPendingOperation() {
		return pendingOperation;
	}

	/**
	 * @param pendingOperation the pendingOperation to set
	 */
	public void setPendingOperation(NetworkOperation pendingOperation) {
		this.pendingOperation = pendingOperation;
	}

	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(Server server) {
		this.server = server;
	}

	/**
	 * @return the networkOperationsTable
	 */
	public Hashtable<Integer, NetworkOperation> getNetworkOperationsTable() {
		return networkOperationsTable;
	}

	/**
	 * @param networkOperationsTable the networkOperationsTable to set
	 */
	public void setNetworkOperationsTable(
			Hashtable<Integer, NetworkOperation> networkOperationsTable) {
		this.networkOperationsTable = networkOperationsTable;
	}
	// #endregion

	// #region Network.
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * predefined texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 */
	public void invokeNetworkOperation(int operationId, Hashtable<String,?> params, BaseViewController caller) {
		invokeNetworkOperation(operationId, params, caller, false);
	}
	public void invokeNetworkOperation(int operationId, Hashtable<String,?> params, BaseViewController caller,final Boolean isJson,final Object hadler,final Server.isJsonValueCode isJsonValue) {
		invokeNetworkOperation(operationId, params, caller, false,isJson,hadler,isJsonValue);
	}
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * predefined texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	public void invokeNetworkOperation(int operationId, Hashtable<String,?> params, BaseViewController caller,
										boolean callerHandlesError) {
		invokeNetworkOperation(operationId, params, caller,
				caller.getActivity().getString(R.string.alert_operation), 
				caller.getActivity().getString(R.string.alert_connecting), callerHandlesError);
	}
	public void invokeNetworkOperation(int operationId, Hashtable<String,?> params, BaseViewController caller,
									   boolean callerHandlesError,final Boolean isJson,final Object hadler,final Server.isJsonValueCode isJsonValue) {
		invokeNetworkOperation(operationId, params, caller,
				caller.getActivity().getString(R.string.alert_operation),
				caller.getActivity().getString(R.string.alert_connecting), callerHandlesError,isJson,hadler,isJsonValue);
	}


	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void invokeNetworkOperation(int operationId, final Hashtable<String, ?> params,
										  final BaseViewController caller,
										  String progressLabel,
										  String progressMessage,
										  final boolean callerHandlesError) {
		final Integer opId = Integer.valueOf(operationId);

		if (isNotAlreadyCalling(opId)) {

			try {

				NetworkOperation operation = new NetworkOperation(operationId, params, caller);
				this.networkOperationsTable.put(opId, operation);
				this.pendingOperation = operation;

//				//TODO quitar el numero de operacion en hardcode.
//				if (caller != null && 11 != operationId)
//				    caller.muestraIndicadorActividad(progressLabel, progressMessage);
				
				if (caller != null) {
					if(AbstractSuitePowerManager.getSuitePowerManager().isScreenOn() && (!isHomeKeyPressed())) {
						InitConsumo.getInstance().getBaseViewController().muestraIndicadorActividad(caller.getActivity(),progressLabel, progressMessage);
					} else {
						Log.d(this.getClass().getSimpleName(), "La aplicación estaba bloqueada.");
					}
				}

				Thread thread = new Thread(new Runnable() {
					public void run() {
						try {
							ServerResponseImpl response = server.doNetworkOperation(opId.intValue(), params);
							returnFromNetworkOperation(opId, response, caller, null, callerHandlesError);
						} catch (Throwable t) {
							returnFromNetworkOperation(opId, null, caller, t, callerHandlesError);
						}
					}

				});

				// Starts the worker thread to perform network operation
				thread.start();

			} catch (Throwable th) {
				Log.d("gets on ", "catch invoke");
				returnFromNetworkOperation(opId, null, caller, th, callerHandlesError);
			}

		} else {
			// unable to process request, show error and return
			return;
		}

	}



	/** New Metod **/
	protected void invokeNetworkOperation(int operationId, final Hashtable<String, ?> params,
										  final BaseViewController caller,
										  String progressLabel,
										  String progressMessage,
										  final boolean callerHandlesError, final Boolean isJson, final Object hadler,final Server.isJsonValueCode isJsonValue) {
		final Integer opId = Integer.valueOf(operationId);

		if (isNotAlreadyCalling(opId)) {

			try {

				NetworkOperation operation = new NetworkOperation(operationId, params, caller);
				this.networkOperationsTable.put(opId, operation);
				this.pendingOperation = operation;

//				//TODO quitar el numero de operacion en hardcode.
//				if (caller != null && 11 != operationId)
//				    caller.muestraIndicadorActividad(progressLabel, progressMessage);

				if (caller != null) {
					if(AbstractSuitePowerManager.getSuitePowerManager().isScreenOn() && (!isHomeKeyPressed())) {
						InitConsumo.getInstance().getBaseViewController().muestraIndicadorActividad(caller.getActivity(),progressLabel, progressMessage);
					} else {
						Log.d(this.getClass().getSimpleName(), "La aplicación estaba bloqueada.");
					}
				}

				Thread thread = new Thread(new Runnable() {
					public void run() {
						try {
							//ServerResponseImpl response = server.doNetworkOperation(opId.intValue(), params);
							//returnFromNetworkOperation(opId, response, caller, null, callerHandlesError);
							ServerResponseImpl response=null;
							ParametersTO parameters=new ParametersTO();
							parameters.setSimulation(Server.SIMULATION);
							parameters.setDevelopment(Server.DEVELOPMENT);
							parameters.setOperationId(opId);
							parameters.setParameters(params.clone());
							parameters.setJson(isJson);
							IResponseService resultado = new ResponseServiceImpl() ;
							//Convirtiendo el Enum al esperado por API SERVER
							int ordinalEnum=isJsonValue.ordinal();
							suitebancomer.aplicaciones.commservice.commons.ApiConstants.isJsonValueCode isJsonTransformValue=suitebancomer.aplicaciones.commservice.commons.ApiConstants.isJsonValueCode.values()[ordinalEnum];
							Hashtable<String, ?> parameters2 = ((Hashtable<String, ?>)parameters.getParameters());
							Log.e("isJson", String.valueOf(isJson));
							Log.e("ordinalEnum", String.valueOf(ordinalEnum));
							Log.e("isJsonTransformValue", String.valueOf(isJsonTransformValue));
							ConnectionFactory cf= new ConnectionFactory(parameters.getOperationId(), parameters2, parameters.isJson(), hadler,(Context)caller.getActivity(),isJsonTransformValue);
							resultado=cf.processConnectionWithParams();
							response=cf.parserConnection(resultado, hadler);

							returnFromNetworkOperation(opId, response, caller, null,callerHandlesError);
						} catch (Throwable t) {
							returnFromNetworkOperation(opId, null, caller, t, callerHandlesError);
						}
					}

				});

				// Starts the worker thread to perform network operation
				thread.start();

			} catch (Throwable th) {
				Log.d("gets on ", "catch invoke");
				returnFromNetworkOperation(opId, null, caller, th, callerHandlesError);
			}

		} else {
			// unable to process request, show error and return
			return;
		}

	}

	/**
	 * Called internally after a network operation has returned. It ends the progress
	 * popup and invokes method to analyze the result
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message from the server or method should handle in the default
	 * implementation
	 */
	protected void returnFromNetworkOperation(final Integer operationId,
											  final ServerResponseImpl response, 
											  final BaseViewController caller,
											  final Throwable throwable,
											  final boolean callerHandlesError) {

		if (caller != null) {
			//if(!(operationId.equals(Server.MOVEMENTS_OPERATION))){
			caller.ocultaIndicadorActividad();
			//}
			
			caller.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						Log.d("Caller != null", "ok");
						analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);
					} catch (Throwable t) {
						Log.d(this.getClass().getSimpleName(), "Exception on analyzeNetworkResponse.", t);
						if (caller != null) {
							String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
							Log.d("Exception on analyzeNetworkResponse.", message);
						}
					}
				}
			});
			
		} else { //TODO session timer expired, go to login screen
			Thread thread = new Thread(new Runnable() {
				public void run() {
					try {
						Log.d("Caller == null", "ok");
						analyzeNetworkResponse(operationId, response, throwable, null, callerHandlesError);
					} catch (Throwable t) {
						String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
						Log.d(this.getClass().getSimpleName() + " :: returnFromNetworkOperation", message);
					}
				}
			});
			thread.run();
		}
	}

	// TODO revisar en hijos.
	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void analyzeNetworkResponse(Integer operationId,
										  ServerResponseImpl response, 
										  Throwable throwable,
										  final BaseViewController caller,
										  boolean callerHandlesError) {
		// get the caller           
		NetworkOperation operation = this.networkOperationsTable.get(operationId);
		if (operation != null) {
			// remove the operation id from the table
			this.networkOperationsTable.remove(operationId);
			if (operation.isActive()) {	
				if (response != null) {
//					BaseViewController baseViewController = operation.getCaller();
					int resultCode = response.getStatus();
					switch (resultCode) {
					case ServerResponseImpl.OPERATION_SUCCESSFUL:
					case ServerResponseImpl.OPERATION_WARNING:
						
						caller.processNetworkResponse(operationId.intValue(), response);						
											
						break;
					case ServerResponseImpl.OPERATION_OPTIONAL_UPDATE:
						caller.processNetworkResponse(operationId.intValue(), response);
						break;
					case ServerResponseImpl.OPERATION_ERROR:
						this.pendingOperation = null;
						String errorCode = response.getMessageCode();
						int operationError = operationId.intValue();
						
								if (callerHandlesError) {
									caller.processNetworkResponse(operationId.intValue(), response);
								} else {
									caller.processNetworkResponse(operationId.intValue(), response);
						}
						break;
					default:
						caller.ocultaIndicadorActividad();
						InitConsumo.getInstance().getBaseViewController().showErrorMessage(caller.getActivity(), R.string.error_format);
						

						response = unexpectedErrorResponse(caller.getActivity().getString(R.string.error_format));
						caller.processNetworkResponse(operationId.intValue(), response);
						
						break;
					}
				} else {
					if (throwable != null) {
						InitConsumo.getInstance().getBaseViewController().ocultaIndicadorActividad();

						if(!InitConsumo.getInstance().getOfertaDelSimulador()) {
							response = unexpectedErrorResponse(getErrorMessage(throwable));
							caller.processNetworkResponse(operationId.intValue(), response);
						}
					} else {
						InitConsumo.getInstance().getBaseViewController().ocultaIndicadorActividad();
						if(InitConsumo.getInstance().getOfertaDelSimulador())
							InitConsumo.getInstance().getBaseViewController().showErrorMessage(caller.getActivity(), R.string.error_communications);
						
						response = unexpectedErrorResponse(caller.getActivity().getString(R.string.error_communications));
						caller.processNetworkResponse(operationId.intValue(), response);
					}
				}
			}
		} else {
			response = unexpectedErrorResponse(caller.getActivity().getString(R.string.error_unexpected));
			caller.processNetworkResponse(operationId.intValue(), response);
		}		
	}
	
	private ServerResponseImpl unexpectedErrorResponse(String mensaje){
		String msg = "{\"estado\":\"ERROR\",\"codigoMensaje\":\"\",\"descripcionMensaje\":\""+mensaje+"\"}";
		
		ServerResponseImpl res = new ServerResponseImpl();
		
		try {
			
			parseJSON(msg, res);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParsingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return res;
	}
	
	private void parseJSON(String response, ParsingHandler handler) throws IOException, ParsingException {

        Reader reader = null;
        try {
            ParserJSON parser = new ParserJSONImpl(response);
            handler.process(parser);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Throwable ignored) {
                }
            }
        }
    }

	/**
	 * Cancel any ongoing network operation, and clears the network
	 * operation table to prevent any other network operation from
	 * executing
	 */
	protected void cancelOngoingNetworkOperations() {
		this.networkOperationsTable.clear();
		this.pendingOperation = null;
		this.server.cancelNetworkOperations();
	}
	
	/**
	 * Determines if the requested operation is not being called already
	 * @param opId the operation id
	 * @return true if the operation isn't already waiting for a response
	 */
	private boolean isNotAlreadyCalling(int opId){
		return !this.networkOperationsTable.containsKey(opId);
	}
	// #endregion
	
	// #region Otros métodos.
	// TODO revisar en hijos.
	public void cierraAplicacion() {
		viewsController.cierraViewsController();
		viewsController = null;
		server.cancelNetworkOperations();
		server = null;
		applicationLogged = false;
	}
	
	/**
	 * Shows the Main menu screen, removing any activity on top
	 */
	public void requestMenu(Context activityContext){
		changingActivity = true;
		viewsController.showMenuInicial();
	}

	/**
	 * Get the operation error message
	 * @param throwable the throwable
	 * @return the operation error message 
	 */
	protected String getErrorMessage(Throwable throwable) {
		StringBuffer sb = new StringBuffer();
		if (throwable != null) {
			if (throwable instanceof NumberFormatException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_format));
			} else if (throwable instanceof ParsingException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_format));
			} else if (throwable instanceof IOException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_communications));
			} else {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_communications));
			}
		}
		return sb.toString();
	}
	// #endregion
	
	// TODO revisar en hijos.
	public BaseSubapplication(Activity suiteApp) {
		this.suiteApp = suiteApp;
		server = new Server();
		networkOperationsTable = new Hashtable<Integer, NetworkOperation>();
		viewsController = null;
		applicationLogged = false;
	}
}
