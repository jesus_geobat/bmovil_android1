package bancomer.api.consumo.implementations;

import android.app.Activity;
import android.util.Log;

import org.apache.http.impl.client.DefaultHttpClient;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.IAutenticacion;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.BaseViewsControllerImpl;
import bancomer.api.common.gui.delegates.DelegateBaseOperacion;
import bancomer.api.common.gui.delegates.MainViewHostDelegate;
import bancomer.api.consumo.gui.controllers.AdelantoNominaContratoViewController;
import bancomer.api.consumo.gui.controllers.AdelantoNominaExitoViewController;
import bancomer.api.consumo.gui.controllers.ContratoConsumoViewController;
import bancomer.api.consumo.gui.controllers.DetalleConsumoViewController;
import bancomer.api.consumo.gui.controllers.DetalleCuponera;
import bancomer.api.consumo.gui.controllers.DetalleCuponeraViewController;
import bancomer.api.consumo.gui.controllers.ExitoCuponeraViewController;
import bancomer.api.consumo.gui.controllers.ExitoOfertaConsumoViewController;
import bancomer.api.consumo.gui.controllers.PolizaViewController;
import bancomer.api.consumo.gui.controllers.SimuladorPrestamoConsumoViewController;
import bancomer.api.consumo.gui.delegates.AdelantoNominaContratoDelegate;
import bancomer.api.consumo.gui.delegates.AdelantoNominaExitoDelegate;
import bancomer.api.consumo.gui.delegates.ConsultaCreditosDelegate;
import bancomer.api.consumo.gui.delegates.ConsultaDetalleConsumoDelegate;
import bancomer.api.consumo.gui.delegates.ContratoOfertaConsumoDelegate;
import bancomer.api.consumo.gui.delegates.DetalleCreditosDelegate;
import bancomer.api.consumo.gui.delegates.DetalleCuponeraDelegate;
import bancomer.api.consumo.gui.delegates.DetalleOfertaConsumoDelegate;
import bancomer.api.consumo.gui.delegates.ExitoCuponeraDelegate;
import bancomer.api.consumo.gui.delegates.ExitoOfertaConsumoDelegate;
import bancomer.api.consumo.gui.delegates.SimuladorPrestamoConsumoDelegate;
import bancomer.api.consumo.io.BaseSubapplication;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.AceptaOfertaConsumo;
import bancomer.api.consumo.models.ListaCreditos;
import bancomer.api.consumo.models.ListaCupones;
import bancomer.api.consumo.models.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;


public class InitConsumo {

    /*Components for API CONSUMO working*/

    private static InitConsumo mInstance = null;

    private BaseViewController baseViewController;

    public static BaseViewsController parentManager;

    private BaseSubapplication baseSubApp;

    private static DefaultHttpClient client;

    private Promociones ofertaConsumo;

    private DelegateBaseOperacion delegateOTP;

    private ICommonSession session;

    private IAutenticacion autenticacion;

    private boolean sofTokenStatus;

    private MainViewHostDelegate hostInstance;

    private boolean ofertaDelSimulador;

    private Promociones respPromociones;

    private OfertaConsumo resOfertaConsumo;

    private String importeParcial = "";

    private String importeInicial = "";

    private String importeMinimo = "";

    private String importeMaximo = "";

    private ListaCupones listaCupones;

    private ListaCupones.Cupon cupon;

    private String crCredito="";

    private CallBackModule callBackModule;

    private boolean changed = false;



    public ListaCupones getListaCupones() {
        return listaCupones;
    }

    public void setListaCupones(ListaCupones listaCupones) {
        this.listaCupones = listaCupones;
    }

    public ListaCupones.Cupon getCupon() {
        return cupon;
    }

    public void setCupon(ListaCupones.Cupon cupon) {
        this.cupon = cupon;
    }

    public static InitConsumo getmInstance() {
        return mInstance;
    }

    public static void setmInstance(InitConsumo mInstance) {
        InitConsumo.mInstance = mInstance;
    }

    public String getImporteParcial() {
        return importeParcial;
    }

    public void setImporteParcial(String importeParcial) {
        this.importeParcial = importeParcial;
    }

    public String getImporteInicial() {
        return importeInicial;
    }

    public void setImporteInicial(String importeInicial) {
        this.importeInicial = importeInicial;
    }

    public String getImporteMinimo() {
        return importeMinimo;
    }

    public void setImporteMinimo(String importeMinimo) {
        this.importeMinimo = importeMinimo;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public String getCrCredito() {
        return crCredito;
    }

    public void setCrCredito(String crCredito) {
        this.crCredito = crCredito;
    }

    public CallBackModule getCallBackModule() {
        return callBackModule;
    }

    public void setCallBackModule(CallBackModule callBackModule) {
        this.callBackModule = callBackModule;
    }

    ///////////// End Region Getter's & Setter's /////////////////

    public ICommonSession getSession() {
        return session;
    }

    public DelegateBaseOperacion getDelegateOTP() {
        return delegateOTP;
    }

    public Promociones getOfertaConsumo() {
        return ofertaConsumo;
    }

    public BaseViewController getBaseViewController() {
        return baseViewController;
    }

    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }

    public BaseViewsController getParentManager() {
        return parentManager;
    }

    public BaseSubapplication getBaseSubApp() {
        return baseSubApp;
    }

    public IAutenticacion getAutenticacion() {
        return autenticacion;
    }

    public boolean getSofTokenStatus() {
        return sofTokenStatus;
    }

    public static DefaultHttpClient getClient() {
        return client;
    }

    public boolean getOfertaDelSimulador() {
        return ofertaDelSimulador;
    }

    ///////////// End Region Getter's & Setter's /////////////////

    public void resetInstance() {
        mInstance = null;
    }

    public void updateHostActivityChangingState() {
        hostInstance.updateActivityChangingState();
    }

    public static InitConsumo respInit;

    public static InitConsumo getInstance(Activity activity, Promociones promocion,
                                          DelegateBaseOperacion delegateOTP, ICommonSession session,
                                          IAutenticacion autenticacion, boolean softTokenStatus, DefaultHttpClient client,
                                          MainViewHostDelegate hostInstance, boolean ofertaDelSimulador,
                                          boolean dev, boolean sim, boolean emulator, boolean log) {
        if (mInstance == null) {
            mInstance = new InitConsumo(activity, promocion, delegateOTP, session, autenticacion, softTokenStatus, client, hostInstance, ofertaDelSimulador, dev, sim, emulator, log);
        } else
            parentManager.setCurrentActivityApp(activity);
        return mInstance;
    }

    public static InitConsumo getInstance() {
        return mInstance;
    }

    public static InitConsumo getInstance(Activity activity, DelegateBaseOperacion delegateOTP,
                                          ICommonSession session, IAutenticacion autenticacion,
                                          boolean softTokenStatus, DefaultHttpClient client,
                                          MainViewHostDelegate hostInstance,
                                          boolean dev, boolean sim, boolean emulator, boolean log){
        if (mInstance == null) {
            mInstance = new InitConsumo(activity, delegateOTP, session, autenticacion,
                    softTokenStatus, client, hostInstance, dev, sim, emulator, log);
        } else
            parentManager.setCurrentActivityApp(activity);

        return mInstance;
    }

    private InitConsumo(Activity activity, Promociones promocion, DelegateBaseOperacion delegateOTP, ICommonSession session,
                        IAutenticacion autenticacion, boolean sofTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance,
                        boolean ofertaDelSimulador
            , boolean dev, boolean sim, boolean emulator, boolean log) {

        setServerParams(dev, sim, emulator, log);
        ofertaConsumo = promocion;
        this.delegateOTP = delegateOTP;
        baseViewController = new BaseViewControllerImpl();
        this.client = client;
        parentManager = new BaseViewsControllerImpl();
        parentManager.setCurrentActivityApp(activity);
        baseSubApp = new BaseSubapplicationImpl(activity, client);
        this.session = session;
        this.autenticacion = autenticacion;
        this.sofTokenStatus = sofTokenStatus;
        this.hostInstance = hostInstance;
        this.ofertaDelSimulador = ofertaDelSimulador;
    }
    /*** para cuponera ****/
    private InitConsumo(Activity activity, DelegateBaseOperacion delegateOTP, ICommonSession session,
                        IAutenticacion autenticacion, boolean sofTokenStatus, DefaultHttpClient client,
                        MainViewHostDelegate hostInstance,boolean dev,
                        boolean sim, boolean emulator, boolean log){

        setServerParams(dev, sim, emulator, log);
        this.delegateOTP = delegateOTP;
        baseViewController = new BaseViewControllerImpl();
        this.client = client;
        parentManager = new BaseViewsControllerImpl();
        parentManager.setCurrentActivityApp(activity);
        baseSubApp = new BaseSubapplicationImpl(activity,client);
        this.session = session;
        this.autenticacion = autenticacion;
        this.sofTokenStatus = sofTokenStatus;
        this.hostInstance = hostInstance;
        Log.d("OK","CONTRUCTOR PARA IR A CUPONERA");
    }

    public void setServerParams(boolean dev, boolean sim, boolean emulator, boolean log) {
        Server.DEVELOPMENT = dev;
        Server.SIMULATION = sim;
        Server.EMULATOR = emulator;
        Server.ALLOW_LOG = log;
    }

    public Promociones getRespPromociones() {
        return respPromociones;
    }

    public void setRespPromociones(Promociones respPromociones) {
        this.respPromociones = respPromociones;
    }

    public OfertaConsumo getResOfertaConsumo() {
        return resOfertaConsumo;
    }

    public void setResOfertaConsumo(OfertaConsumo resOfertaConsumo) {
        this.resOfertaConsumo = resOfertaConsumo;
    }


    public void ejecutaAPIConsumo() {
        new ConsultaDetalleConsumoDelegate().performAction(null);
    }

    public void ejecutaCuponera(){
        new ConsultaCreditosDelegate().performAction(null);
    }

    public void showDetalleConsumo(OfertaConsumo ofertaConsumo, Promociones promocion, boolean isCallmeBack) {

        DetalleOfertaConsumoDelegate delegate = (DetalleOfertaConsumoDelegate)
                parentManager.getBaseDelegateForKey(DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE);
        if (null != delegate)
            parentManager.removeDelegateFromHashMap(DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE);

        delegate = new DetalleOfertaConsumoDelegate(ofertaConsumo, promocion, isCallmeBack);
        parentManager.addDelegateToHashMap(DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE, delegate);
        parentManager.showViewController(DetalleConsumoViewController.class, 0, false, null, null,
                parentManager.getCurrentViewController());
    }

    public void showDetalleConsumoFromSimulador(OfertaConsumo ofertaConsumo, Promociones promocion, String imporPar, String mensaje, boolean isCallmeBack) {

        DetalleOfertaConsumoDelegate delegate = (DetalleOfertaConsumoDelegate)
                parentManager.getBaseDelegateForKey(DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE);
        if (null != delegate)
            parentManager.removeDelegateFromHashMap(DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE);

        delegate = new DetalleOfertaConsumoDelegate(ofertaConsumo, promocion, imporPar, mensaje, isCallmeBack);
        parentManager.addDelegateToHashMap(DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE, delegate);
        parentManager.showViewController(DetalleConsumoViewController.class, 0, false, null, null,
                parentManager.getCurrentViewController());
    }

    /**
     * mostrar la actividad del simulador 27 de Julio 2016 Isai
     */
    public void showSimuladorConsumo(String montoMin, String montoMax, OfertaConsumo resOfertaConsumo) {
        SimuladorPrestamoConsumoDelegate delegate = (SimuladorPrestamoConsumoDelegate)
                parentManager.getBaseDelegateForKey(SimuladorPrestamoConsumoDelegate.SIMULADOR_OFERTA_CONSUMO);

        if (null != delegate)
            parentManager.removeDelegateFromHashMap(SimuladorPrestamoConsumoDelegate.SIMULADOR_OFERTA_CONSUMO);

        delegate = new SimuladorPrestamoConsumoDelegate(montoMin, montoMax, ofertaConsumo, resOfertaConsumo);
        parentManager.addDelegateToHashMap(SimuladorPrestamoConsumoDelegate.SIMULADOR_OFERTA_CONSUMO, delegate);
        parentManager.showViewController(SimuladorPrestamoConsumoViewController.class,
                0, false, null, null, parentManager.getCurrentViewController());
    }

    //Muestra el contrato una vez presionado el el boton de lo quiero.
    public void showContratoConsumo(OfertaConsumo ofertaConsumo, Promociones promocion) {

        ContratoOfertaConsumoDelegate delegate = (ContratoOfertaConsumoDelegate)
                parentManager.getBaseDelegateForKey(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE);

        if (null != delegate)
            parentManager.removeDelegateFromHashMap(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE);

        delegate = new ContratoOfertaConsumoDelegate(ofertaConsumo, promocion);
        delegate.setTipoOperacion(Constants.Operacion.oneClickBmovilConsumo);

        parentManager.addDelegateToHashMap(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE, delegate);
        parentManager.showViewController(ContratoConsumoViewController.class, 0, false, null, null, parentManager.getCurrentViewController());
    }

    public void showContratoANViewController(OfertaConsumo ofertaConsumo, Promociones promocion) {

        AdelantoNominaContratoDelegate delegate = (AdelantoNominaContratoDelegate) parentManager.
                getBaseDelegateForKey(AdelantoNominaContratoDelegate.ADELANTO_NOMINA_CONTRATO_DELEGATE);
        if (delegate != null)
            parentManager.removeDelegateFromHashMap(AdelantoNominaContratoDelegate.ADELANTO_NOMINA_CONTRATO_DELEGATE);

        delegate = new AdelantoNominaContratoDelegate(ofertaConsumo, promocion);
        delegate.setTipoOperacion(Constants.Operacion.oneClickBmovilConsumo);

        parentManager.addDelegateToHashMap(AdelantoNominaContratoDelegate.ADELANTO_NOMINA_CONTRATO_DELEGATE, delegate);
        parentManager.showViewController(AdelantoNominaContratoViewController.class, 0, false, null, null, parentManager.getCurrentViewController());
    }


    public void showExitoContratoViewController(Promociones oPromocion, OfertaConsumo oOfertaConsumo, AceptaOfertaConsumo aceptaOfertaConsumo){
        AdelantoNominaExitoDelegate delegate = (AdelantoNominaExitoDelegate)parentManager.
                getBaseDelegateForKey(AdelantoNominaExitoDelegate.ADELANTO_NOMINA_EXITO_DELEGATE);

        if(delegate != null){
            parentManager.removeDelegateFromHashMap(AdelantoNominaExitoDelegate.ADELANTO_NOMINA_EXITO_DELEGATE);
        }else{
        }

        delegate = new AdelantoNominaExitoDelegate(oPromocion, oOfertaConsumo, aceptaOfertaConsumo);
        delegate.setTipoOperacion(Constants.Operacion.oneClickBmovilConsumo);

        parentManager.addDelegateToHashMap(AdelantoNominaExitoDelegate.ADELANTO_NOMINA_EXITO_DELEGATE, delegate);
        parentManager.showViewController(AdelantoNominaExitoViewController.class, 0, false, null, null, parentManager.getCurrentViewController());
    }

    public void showPoliza(String poliza) {
        parentManager.showViewController(PolizaViewController.class, 0, false,
                new String[]{Constants.TERMINOS_DE_USO_EXTRA},
                new Object[]{poliza}, parentManager.getCurrentViewController());
    }

    public void showExitoCuponera(AceptaOfertaConsumo aceptaOfertaConsumo){
        ExitoCuponeraDelegate delegate = (ExitoCuponeraDelegate)
                parentManager.getBaseDelegateForKey(ExitoCuponeraDelegate.KEY_UNIQUE_DELEGATE_EXITO_CUPONERA);

        if (delegate != null)
            parentManager.removeDelegateFromHashMap(ExitoCuponeraDelegate.KEY_UNIQUE_DELEGATE_EXITO_CUPONERA);
        delegate = new ExitoCuponeraDelegate(aceptaOfertaConsumo,true);

        parentManager.addDelegateToHashMap(ExitoCuponeraDelegate.KEY_UNIQUE_DELEGATE_EXITO_CUPONERA, delegate);
        parentManager.showViewController(
                ExitoCuponeraViewController.class, 0, false, null, null
                , parentManager.getCurrentViewController());
    }

    public void showExitoCuponeraFromMenuConsultar(String numeroCreditoElejido){
        ExitoCuponeraDelegate delegate = (ExitoCuponeraDelegate)
                parentManager.getBaseDelegateForKey(ExitoCuponeraDelegate.KEY_UNIQUE_DELEGATE_EXITO_CUPONERA);

        if (delegate != null)
            parentManager.removeDelegateFromHashMap(ExitoCuponeraDelegate.KEY_UNIQUE_DELEGATE_EXITO_CUPONERA);
        delegate = new ExitoCuponeraDelegate(numeroCreditoElejido);

        parentManager.addDelegateToHashMap(ExitoCuponeraDelegate.KEY_UNIQUE_DELEGATE_EXITO_CUPONERA, delegate);
        parentManager.showViewController(
                ExitoCuponeraViewController.class, 0, false, null, null
                , parentManager.getCurrentViewController());
    }

    public void showTerminosConsumo(String terminos) {
        parentManager.showViewController(PolizaViewController.class, 0, false,
                new String[]{Constants.TERMINOS_DE_USO_CONSUMO},
                new Object[]{terminos}, parentManager.getCurrentViewController());
    }


    public void showformatodomiciliacion(String domiciliacion) {
        parentManager.showViewController(PolizaViewController.class, 0, false,
                new String[]{Constants.DOMICILIACION_CONSUMO},
                new Object[]{domiciliacion}, parentManager.getCurrentViewController());
    }

    public void showContrato(String contrato, int pos) {
        parentManager.showViewController(PolizaViewController.class, 0, false,
                new String[]{Constants.CONTRATO_CONSUMO, "PANTALLA_ENTRANTE"},
                new Object[]{contrato, pos}, parentManager.getCurrentViewController());
    }

    public void showExitoConsumo(AceptaOfertaConsumo aceptaOferta, OfertaConsumo ofertaConsumo) {

        ExitoOfertaConsumoDelegate delegate = (ExitoOfertaConsumoDelegate)
                parentManager.getBaseDelegateForKey(ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE);
        if (null != delegate)

            parentManager.removeDelegateFromHashMap(ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE);

        delegate = new ExitoOfertaConsumoDelegate(aceptaOferta, ofertaConsumo,listaCupones,cupon);

        parentManager.addDelegateToHashMap(ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE, delegate);
        parentManager.showViewController(ExitoOfertaConsumoViewController.class,
                0, false, null, null, parentManager.getCurrentViewController());
    }

    public void showExitoConsumo(AceptaOfertaConsumo aceptaOferta, OfertaConsumo ofertaConsumo,ListaCupones listaCupones,ListaCupones.Cupon cupon) {
        ExitoOfertaConsumoDelegate delegate = (ExitoOfertaConsumoDelegate)
                parentManager.getBaseDelegateForKey(ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE);
        if (null != delegate)

            parentManager.removeDelegateFromHashMap(ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE);

        delegate = new ExitoOfertaConsumoDelegate(aceptaOferta, ofertaConsumo,listaCupones,cupon);

        parentManager.addDelegateToHashMap(ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE, delegate);
        parentManager.showViewController(ExitoOfertaConsumoViewController.class,
                0, false, null, null, parentManager.getCurrentViewController());
    }

    public void showDetalleCuponera(ListaCupones listaCupones,ListaCupones.Cupon cupon,AceptaOfertaConsumo aceptaOferta,OfertaConsumo resOfertaConsumo) {
        DetalleCuponeraDelegate delegate = (DetalleCuponeraDelegate)
                parentManager.getBaseDelegateForKey(DetalleCuponeraDelegate.KEY_UNIQUE_DELEGATE_DETALLE_CUPONERA);

        if (delegate != null)
            parentManager.removeDelegateFromHashMap(DetalleCuponeraDelegate.KEY_UNIQUE_DELEGATE_DETALLE_CUPONERA);
        delegate = new DetalleCuponeraDelegate(listaCupones, cupon,aceptaOferta,resOfertaConsumo );

        parentManager.addDelegateToHashMap(DetalleCuponeraDelegate.KEY_UNIQUE_DELEGATE_DETALLE_CUPONERA, delegate);
        parentManager.showViewController(
                DetalleCuponeraViewController.class, 0, false, null, null
                , parentManager.getCurrentViewController());
    }


    public void showDetalleCuponeraFromMenuConsulta(ListaCreditos listaCreditos){
        DetalleCreditosDelegate delegate = (DetalleCreditosDelegate)
                parentManager.getBaseDelegateForKey(DetalleCreditosDelegate.KEY_UNIQUE_DELEGATE_DETALLE_CREDITOS);

        if (delegate != null)
            parentManager.removeDelegateFromHashMap(DetalleCreditosDelegate.KEY_UNIQUE_DELEGATE_DETALLE_CREDITOS);
        delegate = new DetalleCreditosDelegate(listaCreditos);

        parentManager.addDelegateToHashMap(DetalleCreditosDelegate.KEY_UNIQUE_DELEGATE_DETALLE_CREDITOS, delegate);
        parentManager.showViewController(
                DetalleCuponera.class, 0, false, null, null
                , parentManager.getCurrentViewController());
    }

    public String getImporteMaximo() {
        return importeMaximo;
    }

    public void setImporteMaximo(String importeMaximo) {
        this.importeMaximo = importeMaximo;
    }
}
