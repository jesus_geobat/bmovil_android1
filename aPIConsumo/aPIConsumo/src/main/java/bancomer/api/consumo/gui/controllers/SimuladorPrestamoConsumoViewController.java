package bancomer.api.consumo.gui.controllers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consumo.R;
import bancomer.api.consumo.commons.ConsumoTools;
import bancomer.api.consumo.commons.Tools;

import bancomer.api.consumo.gui.delegates.SimuladorPrestamoConsumoDelegate;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.models.OfertaConsumo;
import bancomer.api.consumo.tracking.TrackingConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;


public class SimuladorPrestamoConsumoViewController extends BaseActivity {


    // ------ SeekArc Attributes ---- //
    private SeekArc mSeekArc;
    /**
     * Pendiente de definir entre EditTEXT
     */
    private LabelMontoTotalCredit lblMontoTotalCredito;

    private String monto;

    // ------ User Information Attributes ---- //
    private TextView txtTarjeta;
    private TextView txtTarjetaT;
    private ImageView imgCredito;
    // ------ Low Part Attributes ---- //
    private ImageButton btnSimular;

    private String amountString = null;
    private StringBuffer typedString = new StringBuffer();
    private boolean isSettingText = false;
    private boolean isResetting = false;
    private boolean mAcceptCents = false;
    private boolean flag = true;
    private boolean editable;
    private int staticRevire = 1;

    private TextView txtStaticPagamenos;
    private TextView txtStaticTasaMejorada;
    private TextView txtStaticAntes;
    private TextView txtStaticTasaMensual;
    private TextView txtStaticSeleccionaImporte;
    private TextView porcentaje_tasa_antes;
    private TextView txtRevire;
    private AlertDialog mAlertDialog;
    private LinearLayout lineGrenn;
    private int revire = 0;

    /*Objeto del delegate */
    private SimuladorPrestamoConsumoDelegate delegate;
    private BaseViewController baseViewController;
    private InitConsumo initConsumo = InitConsumo.getInstance();
    public BaseViewsController parentManager;
    private OfertaConsumo ofertaConsumo;
    private Promociones promocion;


    public OfertaConsumo getOfertaConsumo() {
        return ofertaConsumo;
    }

    public void setOfertaConsumo(OfertaConsumo ofertaConsumo) {
        this.ofertaConsumo = ofertaConsumo;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public void setPromocion(Promociones promocion) {
        this.promocion = promocion;
    }

    /**
     * Botones de back y llamada hacia atras
     */

    private ImageButton btnImgBack;
    private ImageButton callmeImgBack;
    private int valorStaticBtnCallmeBack = 0;


    /*OnCreate de la vista*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = initConsumo.getBaseViewController();
        parentManager = initConsumo.getParentManager();
        parentManager.setCurrentActivityApp(this);
        delegate = (SimuladorPrestamoConsumoDelegate)
                parentManager.getBaseDelegateForKey(SimuladorPrestamoConsumoDelegate.SIMULADOR_OFERTA_CONSUMO);

        delegate.setController(this);

        /**
         * SETEANDO VALORES DEL MAXIMO Y MINIMO PERMITIDO
         * */

        SeekArc.mMax = Integer.parseInt(delegate.getMontoMaximo());
        SeekArc.mMin = Integer.parseInt(delegate.getMontoMinimo());


        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_consumo_container_simulador);
        baseViewController.setDelegate(delegate);
        initConsumo.setBaseViewController(baseViewController);

        /**Cargando vistas y la configuración de pantalla */
        initConsumoViews();
        validaSimulacion(); //valida Simulador
        lblMontoTotalCredito.setMontoMin(delegate.getMontoMinimo());
        lblMontoTotalCredito.setMontoMax(delegate.getMontoMaximo());

        lblMontoTotalCredito.setText(bancomer.api.common.commons.Tools.formatAmount(monto, false));
        lblMontoTotalCredito.setLongClickable(false);


        lblMontoTotalCredito.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                reset();
                mAcceptCents = true;
                isSettingText = true;
                lblMontoTotalCredito.setText("");
                Log.d("ENTRO EN TOUCH LISTENER", "PRIMERA");
                return false;
            }
        });
        /**::::::::::::::: BOTON DE HACIA ATRAS ::::::::::::::::::::::**/
        btnImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblMontoTotalCredito.setText("");
                initConsumo.getParentManager().setActivityChanging(true);
                initConsumo.showDetalleConsumo(delegate.getOfertaConsumo(), delegate.getPromocion(),true);


            }
        });
        /**** :::::::::: TextWatcher :::::::::::::::*/
        lblMontoTotalCredito.addTextChangedListener(new TextWatcher() {
            /**
             * Save the value of string before changing the text
             */
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                String amountField = lblMontoTotalCredito.getText().toString();
                Log.d("ENTRO EN TOUCH LISTENE", "PRIMERA");

                if (!isSettingText) {
                    amountString = amountField;
                }
            }

            /**
             * When the value string has changed
             */
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String amountField = lblMontoTotalCredito.getText().toString();
                Log.d("ENTRO EN TOUCH LISTENER", "PRIMERA");

                if (s.toString().equals(".")) {
                    Log.w("eliminar", "se ingresó un un punto, eliminalo, toS");
                    lblMontoTotalCredito.setText("");
                } else {
                    Log.w("eliminar", "Se ingresó bien el número");


                    Log.d("ENTROOUCHLISTENER 3", "PRIMERA");


                    if (!isSettingText && !isResetting) {
                        if (!flag) {
                            String aux;
                            try {
                                aux = lblMontoTotalCredito.getText().toString().substring(0, lblMontoTotalCredito.length() - 3);
                                typedString = new StringBuffer();

                                if (aux.charAt(0) == '0')
                                    aux = aux.substring(1, aux.length());
                            } catch (Exception ex) {
                                aux = "";

                            }
                            for (int i = 0; i < aux.length(); i++) {
                                if (aux.charAt(i) != ',')
                                    typedString.append(aux.charAt(i));
                            }
                            flag = true;

                            setFormattedText();
                            amountField = lblMontoTotalCredito.getText().toString();
                            amountString = amountField;
                        } else {
                            try {
                                if (lblMontoTotalCredito.length() < amountString.length()) {
                                    reset();
                                } else if (lblMontoTotalCredito.length() > amountString.length()) {

                                    int newCharIndex = lblMontoTotalCredito.getSelectionEnd() - 1;
                                    //there was no selection in the field
                                    if (newCharIndex == -2) {
                                        newCharIndex = lblMontoTotalCredito.length() - 1;
                                    }

                                    char num = amountField.charAt(newCharIndex);
                                    if (!(num == '0' && typedString.length() == 0)) {
                                        typedString.append(num);
                                    }
                                    setFormattedText();
                                }
                            } catch (StringIndexOutOfBoundsException ex) {
                                //ex.printStackTrace();
                            }
                        }
                    }
                }
            }

            public void afterTextChanged(Editable s) {
                Log.d("ENTROTOUCH LISTENER 4", "PRIMERA");

                isSettingText = false;
                lblMontoTotalCredito.setSelection(lblMontoTotalCredito.length());
            }
        });

        /*** ############# Listener del SeeArk ##########**/
        mSeekArc.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekArc seekArc) {
            }

            @Override
            public void onStartTrackingTouch(SeekArc seekArc) {
            }

            @Override
            public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
                monto = String.valueOf(progress);
                //Log.d("label progress: -------->", monto);
                Log.i("label", "Montonormal: " + monto);
                Log.d("label", "MontoNormal:" + bancomer.api.common.commons.Tools.formatAmount(monto, false));

                lblMontoTotalCredito.setText(bancomer.api.common.commons.Tools.formatAmount(monto, false));
                lblMontoTotalCredito.setSelection(monto.length());
                editable = false;

                InputMethodManager manager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                Log.d("Manager::::::: ", manager.toString());
                if (manager != null)
                    manager.hideSoftInputFromWindow(lblMontoTotalCredito.getWindowToken(), 0);
                //Ejecunatando la funcionalidad del simulador
                //actualizaGrafica(monto);
                lblMontoTotalCredito.setCursorVisible(true);
            }
        });
        /*Metodo onClick del boton Simular*/
        /***Botton de simular*/
        btnSimular.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String montoTxt = lblMontoTotalCredito.getText().toString();
                System.out.print("El monto es: " + montoTxt);

                if (montoTxt.equals(" ") || montoTxt.equals("")) {

                    showErrorMessage("El importe esta vacio", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                } else {
                    String valorConFormato$ = lblMontoTotalCredito.getText().toString();
                    String valorSinFormato$ = ((valorConFormato$).replace("$", ""));
                    String valorSinComa = (valorSinFormato$.replace(",",""));

                    double montoSimulador = Double.parseDouble(valorSinComa);

                    String montoMinimo1 =
                            suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.formatAmount(delegate.getMontoMinimo(), false);

                    double montoMinimo2 = Double.parseDouble(ConsumoTools.formatSplitAmount(montoMinimo1.replace("$", "")));
                    String montoMaximo1 =
                            suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.formatAmount(delegate.getMontoMaximo(), false);
                    double montoMaximo2 = Double.parseDouble(ConsumoTools.formatSplitAmount(montoMaximo1.replace("$", "")));


                    if (montoSimulador < montoMinimo2) {

                        showErrorMessage("El importe esta por debajo del rango, el mínimo a ingresar es de " + montoMinimo1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                lblMontoTotalCredito.setText(bancomer.api.common.commons.Tools.formatAmount(delegate.getMontoMinimo(), false));

                            }
                        });


                    } else if (montoSimulador <= montoMaximo2) {
                        /**Manda a la siguiente actividad */
                        initConsumo.getBaseViewController().muestraIndicadorActividad(
                                SimuladorPrestamoConsumoViewController.this, "", "Calculando");
                        String textDuro = lblMontoTotalCredito.getText().toString();
                        String textoSin$ =  (textDuro.replace("$", ""));
                        String textoSinComa = (textoSin$.replace(",", ""));
                        String textoSinPunto = (textoSinComa.replace(".",""));

                        initConsumo.setImporteParcial(textoSinPunto);
                        initConsumo.setChanged(true);

                        initConsumo.getParentManager().setActivityChanging(true);
                        delegate.realizaOperacionCONSUMOSimulador();


                        lblMontoTotalCredito.setHint(
                                suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.formatAmount(
                                        initConsumo.getImporteParcial(), false));

                    } else if (montoSimulador > montoMaximo2) {

                        showErrorMessage("El importe supera el límite, el máximo a ingresar es de " + montoMaximo1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                lblMontoTotalCredito.setText(bancomer.api.common.commons.Tools.formatAmount(delegate.getMontoMaximo(), false));
                            }
                        });
                    }
                }
            }
        });
        /**Button de regresar*/
        btnImgBack.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                }
                return false;
            }
        });

    }//fin delonCreate

    public void findViews() {
        mSeekArc = (SeekArc) findViewById(R.id.seekArc);

        lblMontoTotalCredito = (LabelMontoTotalCredit) findViewById(R.id.lblMontoTotalCredito);
        txtTarjeta = (TextView) findViewById(R.id.txtTarjeta);
        txtTarjetaT = (TextView) findViewById(R.id.txtTerminacion);
        btnSimular = (ImageButton) findViewById(R.id.btn_simular);
        imgCredito = (ImageView) findViewById(R.id.imgCredito);
        lineGrenn = (LinearLayout) findViewById(R.id.banda_verde_simulador);
        txtTarjeta.setVisibility(View.GONE);
        txtTarjetaT.setVisibility(View.GONE);

        /**Vistas del Banner Verde****/
        txtStaticPagamenos = (TextView) findViewById(R.id.paga_menos_simulador);
        txtStaticTasaMejorada = (TextView) findViewById(R.id.tasa_mejorada_simulador);
        txtStaticAntes = (TextView) findViewById(R.id.antes_simulador);
        txtStaticTasaMensual = (TextView) findViewById(R.id.tasa_mensual_simulador);
        txtRevire = (TextView) findViewById(R.id.textRevireSimulador);
        porcentaje_tasa_antes = (TextView) findViewById(R.id.porcentaje_tasa_antes);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/libre2.ttf");
        txtStaticAntes.setTypeface(typeface);
        txtStaticPagamenos.setTypeface(typeface);
        porcentaje_tasa_antes.setTypeface(typeface);

        porcentaje_tasa_antes.setPaintFlags(porcentaje_tasa_antes.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        seekArcSettlement();

        /*Botones de callmeBack y hacias atras*/
        btnImgBack = (ImageButton) findViewById(R.id.consumo_ic_back);
        callmeImgBack = (ImageButton) findViewById(R.id.consumo_ic_callmeback);
        callmeImgBack.setVisibility(View.GONE);
        delegate.setIsCallmeBack(true);
        if (obtenerSharedPreferences()) {  //Si es valor obtenido es = a true muestra el boton en la de simulador
            callmeImgBack.setVisibility(View.VISIBLE);
        }
        /**::::::::::::::::BUTTON DE LLAMAR AL CALLMEBACK::::::::::::::::**/
        callmeImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**Operación llamenme por teléfono***/
                delegate.peticionBtnLlamenme();
                try {
                    Thread.sleep(250);
                    Log.e("EJECUTANDO 2 ", "OPERACION");
                    initConsumo.getBaseViewController().ocultaIndicadorActividad();
                    delegate.peticionLlamame(baseViewController, delegate.getOfertaConsumo().getImporte());
                }catch(InterruptedException e){ }
            }
        });
         txtRevire.setVisibility(View.GONE);
         lineGrenn.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            if (delegate.getOfertaConsumo().getTasaRevire().isEmpty() ||
                    delegate.getOfertaConsumo().getTasaRevire().equals("0") ||
                    delegate.getOfertaConsumo().getTasaRevire().equals("0.0") ||
                    delegate.getOfertaConsumo().getTasaRevire().equals(" ") ||
                    delegate.getOfertaConsumo().getTasaRevire().equals("00.00")) {

                txtRevire.setVisibility(View.GONE);
                lineGrenn.setVisibility(View.GONE);
                imgCredito.setPadding(0,70,0,0);
            } else {
                txtRevire.setVisibility(View.VISIBLE);
                lineGrenn.setVisibility(View.VISIBLE);
                txtRevire.setText(delegate.getOfertaConsumo().getTasaAnual()+"%");
                porcentaje_tasa_antes.setText(delegate.getOfertaConsumo().getTasaRevire()+"%");
                imgCredito.setPadding(0,20,0,0);
            }
        }


    }
    private void seekArcSettlement() {
        /**Seteo las configuración del simulador*/
        mSeekArc.setArcRotation(345);
        mSeekArc.setSweepAngle(330);
        mSeekArc.setArcWidth(40);
        mSeekArc.setProgressWidth(20);
        /**monto = es el monto maximo que puede recibir prestado una persona*/
        monto = String.valueOf(delegate.getMontoMaximo());
        lblMontoTotalCredito.setText(bancomer.api.common.commons.Tools.formatAmount(monto, false));
        mSeekArc.updateProgressText(Integer.parseInt(monto));

    }

    private void configurarPantalla() {

        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());

        gTools.scale(lineGrenn);
        gTools.scale(txtStaticPagamenos, true);
        gTools.scale(txtStaticTasaMejorada, true);
        gTools.scale(txtStaticAntes, true);
        gTools.scale(txtStaticTasaMensual, true);
        gTools.scale(txtStaticSeleccionaImporte, true);
        gTools.scale(txtRevire, true);
        gTools.scale(porcentaje_tasa_antes, true);
    }

    public void showErrorMessage(String errorMessage, DialogInterface.OnClickListener listener) {

        if (errorMessage.length() > 0) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.label_error);
            alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
            alertDialogBuilder.setMessage(errorMessage);
            alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.show();
        }
    }

    /**
     * Metodo Reset
     */
    public void reset() {
        this.isResetting = true;
        this.typedString = new StringBuffer();
        this.isResetting = false;
    }

    /***
     * Metodo para formatear texto
     */
    private void setFormattedText() {
        isSettingText = true;
        String text = typedString.toString();

        if (mAcceptCents) {
            text += "00";
        }
        lblMontoTotalCredito.setText(bancomer.api.common.commons.Tools.formatAmount(text, false));
    }

    /**
     * Metodo Actuliza Grafica
     **/
    public void actualizaGrafica(String monto) {
        monto = monto.replace("$", "");
        mSeekArc.setArcRotation(345);
        mSeekArc.setSweepAngle(330);
        mSeekArc.setArcWidth(40);
        mSeekArc.setProgressWidth(20);
        mSeekArc.updateProgressText(Integer.parseInt(Tools.validateValueMonto(monto)));

    }

    /**
     * Metodo isFlag  (getFlag)
     */
    public boolean isFlag() {
        return flag;
    }

    /**
     * Metodo seFlag
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    //***Metodo de cargas vistas y configuración de pantall
    public void initConsumoViews() {
        findViews();
        configurarPantalla();
    }

    //Control de back hacia atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            lblMontoTotalCredito.setText("");
            initConsumo.getParentManager().setActivityChanging(true);
            initConsumo.showDetalleConsumo(delegate.getOfertaConsumo(), delegate.getPromocion(), true);

        }
        return true;
    }

    private void validaSimulacion() {
        String monto = String.valueOf(delegate.getMontoMaximo());
        setSimulacionGrafica(monto);
    }

    /**
     * Simulación de la Gráfica
     */
    public void setSimulacionGrafica(String monto) {
        lblMontoTotalCredito.setText(monto);
        actualizaGrafica(monto);
    }

    @Override
    protected void onPause() {
        super.onPause();
       /* if (!initConsumo.getParentManager().isActivityChanging()) {
            //TimerController.getInstance().getSessionCloser().cerrarSesion();
            *//*initConsumo.getParentManager().resetRootViewController();
            initConsumo.resetInstance();*//*
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        baseViewController = initConsumo.getBaseViewController();
        parentManager = initConsumo.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        initConsumo.setBaseViewController(baseViewController);
    }

    @Override
    public void onUserInteraction() {
       // super.onUserInteraction();
      //  initConsumo.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    public String devuelveDatoSimulador() {
        return lblMontoTotalCredito.getText().toString();
    }

    //Metodo para guardar en el ciclo de vida del consumo el boton de callmeBack
    public void guardarSharedPreferences(boolean variableBoolean) {
        String preferencia = "MIS_PREFRENCIAS";
        int modoPrivacidad = Context.MODE_PRIVATE;
        SharedPreferences.Editor editor = getSharedPreferences(preferencia, modoPrivacidad).edit();
        editor.putBoolean("keyButtoncall", variableBoolean);
        editor.commit();
    }

    //Metodo para sacar el valor de shared preference
    public boolean obtenerSharedPreferences() {
        String preferencia = "MIS_PREFRENCIAS";
        int modoPrivacidad = Context.MODE_PRIVATE;
        SharedPreferences preferences = getSharedPreferences(preferencia, modoPrivacidad);
        boolean valorObtenido = preferences.getBoolean("keyButtoncall", false);
        return valorObtenido;
    }

    public void initView() {
        findViews();
        configurarPantalla();
    }
}
