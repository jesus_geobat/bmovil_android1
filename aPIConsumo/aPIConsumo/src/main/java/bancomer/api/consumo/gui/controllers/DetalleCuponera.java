package bancomer.api.consumo.gui.controllers;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consumo.R;
import bancomer.api.consumo.gui.delegates.DetalleCreditosDelegate;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.models.ListaCupones;


/**
 * Created by isaigarciamoso on 09/09/16.
 * Modified by OmarOrozco on 22/09/16
 */
public class DetalleCuponera extends BaseActivity {

    private ImageButton imgBack;
    private ImageButton imgCuponera;
    private Button btnObtenerCupones;
    private TextView tittleHeader;
    private TextView cantidadCupones;
    private TextView subTittle;
    private TextView linkBancomerVida;
    private LinearLayout layoutSubTittle;
    private HorizontalScrollView layoutHorizontalScroll;
    private LinearLayout layoutPrincipal;
    private LinearLayout layoutCupon;
    private RelativeLayout anteriorHeader;
    private RelativeLayout itemCuponCredito;
    private BaseViewController baseViewController;
    private BaseViewsController parentManager;
    private DetalleCreditosDelegate delegateCreditos;
    private ListView listaVivaIdeas;

    private TextView titulo_cupon;
    private TextView detalle_cupon;
    private TextView tipo_cupon;
    private TextView porcentaje_cupon;
    private TextView descuento_cupon;
    private InitConsumo init = InitConsumo.getInstance();
    private Button buttonItemCuponera;
    private TextView numeroCredito;
    private TextView textExample;
    private CuponAdapter<String> adaptador;
    private ArrayList lista = new ArrayList<String>();
    //Vista del item
    private RelativeLayout item_relative;
    private LinearLayout item_credito;
    private ArrayList<String> arrayListCreditos;
    private ArrayList<String> arrayLisCrs;
    private FragmentCuponera fragmentCuponera;
    private ServerResponse serverResponse;
    private ListaCupones listaCupones;
    private String valorNumeroCredito;
    private LinearLayout linearLayoutejemplo;
    private ProgressDialog mProgressDialog;

    private int currentView;

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegateCreditos = (DetalleCreditosDelegate) parentManager.getBaseDelegateForKey(
                DetalleCreditosDelegate.KEY_UNIQUE_DELEGATE_DETALLE_CREDITOS);

        delegateCreditos.setController(this);
        baseViewController.setActivity(this);
        baseViewController.onCreate(this, baseViewController.SHOW_HEADER, R.layout.cuponera_creditos);
        baseViewController.setDelegate(delegateCreditos);
        init.setBaseViewController(baseViewController);
        //vistas vinculadas
        initViews();
        currentView = -1;
    }
    @Override
    protected void onResume() {
        super.onResume();
        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);
    }


    @Override
    protected void onPause() {
        super.onPause();
        /*if (!init.getParentManager().isActivityChanging()) {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }*/

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
       // init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    public void initViews() {
        findViews();
        configurarPantalla();
    }

    public void findViews() {

        imgBack = (ImageButton) findViewById(R.id.consumo_ic_back);
        imgCuponera = (ImageButton) findViewById(R.id.cuponera_icon);
        tittleHeader = (TextView) findViewById(R.id.cuponera_tittle);
        cantidadCupones = (TextView) findViewById(R.id.cantidad_cupones);
        subTittle = (TextView) findViewById(R.id.label_subtitle);
        layoutSubTittle = (LinearLayout) findViewById(R.id.layout_subtittle);
        layoutCupon = (LinearLayout) findViewById(R.id.layoutcuponera_cupones);
        layoutHorizontalScroll = (HorizontalScrollView) findViewById(R.id.horizontalscroll_cupones);
        layoutPrincipal = (LinearLayout) findViewById(R.id.layout_principal);
        anteriorHeader = (RelativeLayout) findViewById(R.id.relativo_layout);
        listaVivaIdeas = (ListView) findViewById(R.id.lista_viva_ideas);
        titulo_cupon = (TextView) findViewById(R.id.titulo_cupon);
        detalle_cupon = (TextView) findViewById(R.id.detalle_cupon);
        porcentaje_cupon = (TextView) findViewById(R.id.porcentaje_cupon);
        descuento_cupon = (TextView) findViewById(R.id.descuento_cupon);
        item_relative = (RelativeLayout) findViewById(R.id.listView2);

        btnObtenerCupones = (Button) findViewById(R.id.btn_obtener_cupones);
        linkBancomerVida = (TextView) findViewById(R.id.link_bancomer_vida);
        item_credito = (LinearLayout) findViewById(R.id.item_credito);
        btnObtenerCupones.setVisibility(View.GONE);
        linkBancomerVida.setVisibility(View.GONE);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/tahoma.ttf");
        tittleHeader.setTypeface(typeface);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float density = metrics.density;
        int right =(int) (10* density);


        arrayListCreditos = new ArrayList<String>();
        if(delegateCreditos.getListaCreditos().getListaCreditos().size() < 3)
        {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            item_credito.setLayoutParams(params);
            item_credito.requestLayout();

            if(delegateCreditos.getListaCreditos().getListaCreditos().size() == 1)
                findViewById(R.id.layout_subtittle).setVisibility(View.GONE);
        }

        for (int i = 0; i < delegateCreditos.getListaCreditos().getListaCreditos().size(); i++) {

            arrayListCreditos.add(delegateCreditos.getListaCreditos().getListaCreditos().
                    get(i).getNumeroCredito().toString());
            Log.d("VIEWCONTROLLER", delegateCreditos.getListaCreditos().getCredito().getNumeroCredito());


            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.view_credito_scroll, null);

            ((TextView)view.findViewById(R.id.text_view_credito_numero)).setText(delegateCreditos.getListaCreditos().getListaCreditos().get(i).getNumeroCredito());
            view.setId(i);
            view.setOnClickListener(clickListener);
            int w = (int) (150 * density);
            int h = (int) (70* density);
            LinearLayout.LayoutParams lyParam = new LinearLayout.LayoutParams(w,h);
            lyParam.setMargins(0, 0, right, 0);
            view.setLayoutParams(lyParam);

            item_credito.addView(view);
        }
        //Button obtener cu
        btnObtenerCupones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init.getParentManager().setActivityChanging(true);
                init.showExitoCuponeraFromMenuConsultar(valorNumeroCredito);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextView text_view_link = (TextView)findViewById(R.id.link_bancomer_vida);

        String url = getString(R.string.detalle_cuponera_url_link_aviso);
        SpannableString content = new SpannableString(getString(R.string.detalle_cuponera_link_aviso));
        content.setSpan(new UnderlineSpan(), content.length() - (url.length()+1), content.length()-1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        content.setSpan(new ForegroundColorSpan(Color.BLUE), content.length() - (url.length()+1), content.length()-1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text_view_link.setText(content);
    }

    public void configurarPantalla() {

        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());

        gTools.scale(imgBack);
        gTools.scale(imgCuponera);
        gTools.scale(tittleHeader, true);
        gTools.scale(cantidadCupones, true);
        gTools.scale(subTittle, true);
        gTools.scale(layoutSubTittle);
        gTools.scale(layoutCupon);
        gTools.scale(layoutHorizontalScroll);
        gTools.scale(layoutPrincipal);
        gTools.scale(anteriorHeader);
        gTools.scale(listaVivaIdeas);
        gTools.scale(titulo_cupon, true);
        gTools.scale(detalle_cupon, true);
        gTools.scale(porcentaje_cupon, true);
        gTools.scale(descuento_cupon, true);
        gTools.scale(item_relative);
        gTools.scale(linkBancomerVida, true);
        gTools.scale(itemCuponCredito);
        gTools.scale(item_credito);
    }

    //One Click
    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int identificador = v.getId();
            for(int i = 0; i< arrayListCreditos.size(); i++){
                if(i == identificador ){
                    delegateCreditos.realizaOperacionConsultarStatusCuponera(arrayListCreditos.get(i));
                    valorNumeroCredito = arrayListCreditos.get(i);
                    //Current Min Api Level needs to uses this Deprecated Methods. (Level 8)
                    v.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_fill));
                    setNewBorderToLastCurrent();
                    currentView = v.getId();
                }
            }
        }
    };

    public void setCantidadCupones(String cantidadCupones) {
        this.cantidadCupones.setText(cantidadCupones);
    }

    public void showBottomElements()
    {
        if(btnObtenerCupones != null)
            btnObtenerCupones.setVisibility(View.VISIBLE);
        if(linkBancomerVida != null)
            linkBancomerVida.setVisibility(View.VISIBLE);
    }

    public void hideProgressDialog()
    {
        if(mProgressDialog != null)
            if(mProgressDialog.isShowing())
                mProgressDialog.dismiss();
    }

    public void showErrorAlert(String message)
    {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.detalle_cuponera_titulo_alerta))
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onBackPressed() {
        baseViewController.goBack(this, parentManager);
        init.resetInstance();
    }

    private void setNewBorderToLastCurrent()
    {
        if(currentView != -1)
        {
            findViewById(currentView).setBackgroundDrawable(getResources().getDrawable(R.drawable.border_fill_cuponera));
        }
    }
}
