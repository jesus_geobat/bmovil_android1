package bancomer.api.consumo.gui.controllers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;
import java.util.logging.Handler;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.consumo.R;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consumo.commons.ConsumoTools;
import bancomer.api.consumo.gui.delegates.ConsultaDetalleConsumoDelegate;
import bancomer.api.consumo.gui.delegates.DetalleOfertaConsumoDelegate;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.OfertaConsumo;
import bancomer.api.consumo.tracking.TrackingConsumo;

public class DetalleConsumoViewController extends BaseActivity {

    private BaseViewController baseViewController;
    private InitConsumo init = InitConsumo.getInstance();
    private DetalleOfertaConsumoDelegate delegate;
    public BaseViewsController parentManager;

    private AlertDialog mAlertDialog;

    private EditText edtTextoMonto;

    private ImageButton btnmeInteresa;
    private ImageButton iconBack;
    private ImageButton iconCallmeBack;

    private ImageView imgFlecha;
    private ImageView iconPesos;
    private ImageView iconCalendar;
    private ImageView iconTasaMensual;

    private ImageView divider;
    private LinearLayout flecha;
    private LinearLayout layoutLHeader;
    private LinearLayout layoutLFleca;
    private LinearLayout layoutLCat;

    private RelativeLayout lineaVerde;
    private RelativeLayout tasa_interes;
    private RelativeLayout quincenas_layout;
    private RelativeLayout quincenaTextFromLayout;
    private RelativeLayout layoutRPagoMaximo;
    private RelativeLayout layoutPagamenos;
    private RelativeLayout layoutRQuicenas;
    private RelativeLayout relativeTasaMensual;
    private RelativeLayout relativeTextMensual;

    private TextView lblTexto2;
    private TextView lblTexto3;
    private TextView lblTexto5;
    private TextView lblTexto6;
    private TextView lblTexto7;
    private TextView lblTexto8;
    private TextView simularConsumo;
    private TextView textuAyudaTasaSinIva;
    private TextView textoAyudatasaMensual;
    private TextView lblTasaMensual;
    private TextView lblQuincenas;
    private TextView lblRevire;
    private TextView modificarImporte;
    private TextView textViewTasaAntes;
    private TextView textPagaMenos;
    private TextView textAntes;
    private TextView lblTexto9;
    private TextView lblTasamejorada;
    private TextView lblPagamenos;
    private TextView quincenas;
    private TextView newCat;
    private TextView nuevoTextoAyuda;
    private TextView txtvMesOQuincena;
    private TextView txtPlazo;

    private boolean isBtnLlamar = true;
    private boolean editable = false;

    private DecimalFormat df;
    private DecimalFormat dfnd;

    private double montoInicial;
    private double montoMinimo;
    private double montoModificado;

    private int valorStaticoBtnCallmeBack = 1;
    private int staticRevire = 0;

    private String customAmount;

    //etiquetado Adobe
    String screen = "";
    ArrayList<String> list = new ArrayList<String>();
    Map<String, Object> paso1OperacionMap = new HashMap<String, Object>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        delegate = (DetalleOfertaConsumoDelegate) parentManager.getBaseDelegateForKey(
                DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE);

        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, baseViewController.SHOW_HEADER | baseViewController.SHOW_TITLE, R.layout.api_consumo_detalle);
        baseViewController.setTitle(this, R.string.bmovil_promocion_title_consumo, R.drawable.icono_consumo);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        initView();

        //etiquetado Adobe
        screen = "detalle consumo";
        list.add(screen);
        TrackingConsumo.trackState("detalle consumo", list);

        if(!init.getOfertaDelSimulador()) {
            if (delegate.getIsCallmeBack()) {
                if (obtenerSharedPreferences()) {
                    iconCallmeBack.setVisibility(View.VISIBLE);
                }
            } else {
                aleatorioMostrarBoton();
            }
        }
    }

    public void initView() {
        findViews();
        setTextToView();
        configurarPantalla();
        setEditTextListener();
    }

    private void findViews() {

        edtTextoMonto           = (EditText) findViewById(R.id.edtTextoMonto);

        btnmeInteresa           = (ImageButton) findViewById(R.id.btn_meinteresa);
        iconBack                = (ImageButton) findViewById(R.id.consumo_ic_back);
        iconCallmeBack          = (ImageButton) findViewById(R.id.consumo_ic_callmeback);

        imgFlecha               = (ImageView) findViewById(R.id.imgFlecha);
        iconPesos               = (ImageView) findViewById(R.id.pesos);
        iconTasaMensual         = (ImageView) findViewById(R.id.img_tasa_mensual);
        divider                 = (ImageView) findViewById(R.id.title_divider);

        layoutLHeader           = (LinearLayout) findViewById(R.id.pGlobalCreditoLayoutConsumo);
        layoutLFleca            = (LinearLayout) findViewById(R.id.image_flecha);
        layoutLCat              = (LinearLayout) findViewById(R.id.cat);
        flecha                  = (LinearLayout) findViewById(R.id.barrita_flecha);

        layoutRPagoMaximo       = (RelativeLayout) findViewById(R.id.pago_maximo);
        lineaVerde              = (RelativeLayout) findViewById(R.id.banda_verde);
        tasa_interes            = (RelativeLayout) findViewById(R.id.tasa_mensual);
        quincenas_layout        = (RelativeLayout) findViewById(R.id.linearLayout_quincena);
        quincenaTextFromLayout  = (RelativeLayout) findViewById(R.id.quincena_texto_layout);

        lblTexto2               = (TextView) findViewById(R.id.lblTexto2);
        lblTexto3               = (TextView) findViewById(R.id.lblTexto3);
        lblTexto5               = (TextView) findViewById(R.id.lblTexto5);
        lblTexto6               = (TextView) findViewById(R.id.lblTexto6);
        lblTexto7               = (TextView) findViewById(R.id.lblTexto7);
        lblTexto8               = (TextView) findViewById(R.id.lblTexto8);
        lblTexto9               = (TextView) findViewById(R.id.lblTexto9);
        txtPlazo                = (TextView) findViewById(R.id.txtvMesOQuincena);
        lblRevire               = (TextView) findViewById(R.id.textViewRevire);
        textPagaMenos           = (TextView) findViewById(R.id.paga_menos);
        textAntes               = (TextView) findViewById(R.id.textView2);
        lblTasamejorada         = (TextView) findViewById(R.id.mejorado_tasa);
        textoAyudatasaMensual   = (TextView) findViewById(R.id.textView);
        nuevoTextoAyuda         = (TextView) findViewById(R.id.nuevoTextoAyuda);
        modificarImporte        = (TextView) findViewById(R.id.modificar_importe_consumo);
        textuAyudaTasaSinIva    = (TextView) findViewById(R.id.tasa_mensual_siniva);
        newCat                  = (TextView) findViewById(R.id.newCat);
        lblRevire               = (TextView) findViewById(R.id.textViewRevire);
        lblTasaMensual          = (TextView) findViewById(R.id.lblTasaMensual);
        lblQuincenas            = (TextView) findViewById(R.id.lblQuincenas);
        textViewTasaAntes       = (TextView) findViewById(R.id.textView3);
        txtvMesOQuincena        = (TextView)findViewById(R.id.txtvMesOQuincena);

        divider.setVisibility(View.GONE);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/unicode.futurabb.ttf");
        lblTexto7.setTypeface(typeface);
        lblTexto5.setTypeface(typeface);
        lblQuincenas.setTypeface(typeface);
        lblTasaMensual.setTypeface(typeface);

        Typeface typeface1 = Typeface.createFromAsset(getAssets(), "fonts/libre2.ttf");
        textPagaMenos.setTypeface(typeface1);
        textAntes.setTypeface(typeface1);
        textViewTasaAntes.setTypeface(typeface1);
        textViewTasaAntes.setPaintFlags(textViewTasaAntes.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        btnmeInteresa.setOnClickListener(clickListener);
        modificarImporte.setOnClickListener(clickListenerModificarImporte);
        iconBack.setOnClickListener(clickListener);

        txtvMesOQuincena.setText(init.getOfertaDelSimulador() ? getResources().getString(R.string.api_consumo_detalle_pago_mensual) :
                getResources().getString(R.string.api_consumo_detalle_pago_quincenal));

        iconCallmeBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                init.getBaseViewController().muestraIndicadorActividad(
                        DetalleConsumoViewController.this, "", "Calculando");

                delegate.setDescripcionMensaje("0038");
                delegate.realizaOperacion(85, 0);
                delegate.peticionLlamame(baseViewController, delegate.getOfertaConsumo().getImporte());
            }
        });

        if (init.getOfertaDelSimulador()) {
            if(delegate.getOfertaConsumo().getIndRev() != null){
                if(delegate.getOfertaConsumo().getIndRev().equalsIgnoreCase("S")){

                    String tasaOri  = delegate.getOfertaConsumo().getTasaOri();
                    String tasa     = delegate.getOfertaConsumo().getTasa() + "%";

                    lblRevire.setText(tasa);            //tasa nueva
                    textViewTasaAntes.setText(tasaOri); //tasa vieja

                    lineaVerde.setVisibility(View.VISIBLE);
                    edtTextoMonto.setVisibility(View.GONE);
                    modificarImporte.setVisibility(View.GONE);
                    iconCallmeBack.setVisibility(View.GONE);
                    flecha.setVisibility(View.GONE);

                }else{
                    hideRevire();
                }
            }else{
                hideRevire();
            }
        }
    }

    private void hideRevire(){
        lblTexto9.setVisibility(View.GONE);
        edtTextoMonto.setVisibility(View.GONE);
        lineaVerde.setVisibility(View.GONE);
        modificarImporte.setVisibility(View.GONE);
        iconCallmeBack.setVisibility(View.GONE);
    }

    public void setTextToView() {

        if (!init.isChanged()) {
            init.setImporteMinimo(delegate.getOfertaConsumo().getMontoMinimo());
            init.setImporteInicial(delegate.getOfertaConsumo().getImporte());
        }

        if (!init.getOfertaDelSimulador()) {
            montoMinimo = Double.parseDouble(init.getImporteMinimo());
            montoInicial = Double.parseDouble(init.getImporteInicial());
        }

        String lblText7 = Tools.formatAmount(delegate.getOfertaConsumo().getImporte(), false);
        SpannableString texto7 = new SpannableString(lblText7);
        lblTexto7.setText(texto7);

        if (!delegate.getModificarImporte().equals(" ")) {
            modificarImporte.setText(delegate.getModificarImporte());
        }

        String pagoMen = Tools.formatAmount(delegate.getOfertaConsumo().getPagoMenFijo(), false);
        String quincenas = delegate.getOfertaConsumo().getPlazoDes();
        String tasaMen = delegate.getOfertaConsumo().getTasaMensual() + "%";
        String tasaAnual =  delegate.getOfertaConsumo().getTasaAnual()+"%";
        delegate.formatoFechaCatMostrar();
        String cat = delegate.getOfertaConsumo().getCat();
        String catFormat = cat.substring(0, 4);

        String[] arrayStringQuincenaCut = quincenas.split(" ");
        lblTexto3.setText("  Tasa fija anual del " + delegate.getOfertaConsumo().getTasaAnual() + "% sin IVA con un");
        newCat.setText("  CAT " + catFormat + "%");
        lblTexto6.setText("CAT " + catFormat + "%");
        lblTexto2.setText("sin IVA Informativo. Fecha de cálculo al " + delegate.getfechaCatVisual() + ". *El IVA \n  puede variar en cada pago.");
        lblTexto5.setText(pagoMen);
        lblTasaMensual.setText(tasaMen);
        lblQuincenas.setText(arrayStringQuincenaCut[0]);

        if(arrayStringQuincenaCut[1].equals("quincenas")) {
            txtPlazo.setText("Q"+arrayStringQuincenaCut[1].substring(1,arrayStringQuincenaCut[1].length()));
        }else if(arrayStringQuincenaCut[1].equals("meses")){
            txtPlazo.setText("M"+arrayStringQuincenaCut[1].substring(1,arrayStringQuincenaCut[1].length()));
        }

        edtTextoMonto.setHint(ConsumoTools.formatCredit(init.getImporteInicial()));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            if(delegate.getOfertaConsumo().getTasaRevire() != null) {
                if (delegate.getOfertaConsumo().getTasaRevire().isEmpty() ||
                        delegate.getOfertaConsumo().getTasaRevire().equals("0") ||
                        delegate.getOfertaConsumo().getTasaRevire().equals("0.0") ||
                        delegate.getOfertaConsumo().getTasaRevire().equals(" ") ||
                        delegate.getOfertaConsumo().getTasaRevire().equals("00.00")) {

                    lblTexto7.setTextColor(getResources().getColor(R.color.verde));
                    lineaVerde.setVisibility(View.GONE);
                    flecha.setVisibility(View.VISIBLE);
                    lblTexto8.setText("Toma tu Préstamo por :");
                    nuevoTextoAyuda.setVisibility(View.GONE);

                } else {
                    lineaVerde.setVisibility(View.VISIBLE);
                    flecha.setVisibility(View.GONE);
                    String revirePorcentaje = delegate.getOfertaConsumo().getTasaRevire() + "%";
                    lblRevire.setText(tasaAnual);
                    textViewTasaAntes.setText(revirePorcentaje);
                }
            }
        }

        if (delegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREFORMALIZADO)) {
            lblTexto8.setText("Toma tu Préstamo");
            nuevoTextoAyuda.setText("y te depositamos de inmediato");
            nuevoTextoAyuda.setVisibility(View.VISIBLE);
        } else if (delegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREABPROBADO)) {
            lblTexto8.setText("Toma tu Préstamo por:");
            nuevoTextoAyuda.setVisibility(View.GONE);
        } else {
            lblTexto8.setText("Toma tu Préstamo por:");
            nuevoTextoAyuda.setVisibility(View.GONE);
        }
    }

    private void configurarPantalla() {

        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());

        gTools.scale(lblTexto2, true);
        gTools.scale(lblTexto3, true);
        gTools.scale(lblTexto5, true);
        gTools.scale(lblQuincenas, true);
        gTools.scale(lblTasaMensual, true);
        gTools.scale(lblRevire, true);
        gTools.scale(lblTexto6, true);
        gTools.scale(lblTexto7, true);
        gTools.scale(lblTexto8, true);
        gTools.scale(btnmeInteresa);
        gTools.scale(newCat);
        gTools.scale(iconBack);
        gTools.scale(iconCallmeBack);
        gTools.scale(nuevoTextoAyuda);

        gTools.scale(textPagaMenos);
        gTools.scale(lblTasamejorada);
        gTools.scale(textAntes);
        gTools.scale(textViewTasaAntes);
        gTools.scale(textoAyudatasaMensual);

    }

    OnClickListener clickListenerModificarImporte = new OnClickListener() {
        @Override
        public void onClick(View vista) {
            if (vista == modificarImporte) {

                init.getParentManager().setActivityChanging(true);

                String minimoDineros$ = delegate.getOfertaConsumo().getMontoMinimo();
                String maximoDineros$ = delegate.getOfertaConsumo().getImporte();
                ConsultaDetalleConsumoDelegate detalleConsumoDelegate = new ConsultaDetalleConsumoDelegate();
                detalleConsumoDelegate.getValor();

                init.showSimuladorConsumo(minimoDineros$, init.getImporteInicial(), delegate.getOfertaConsumo());

            }
        }
    };

    OnClickListener clickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (v == btnmeInteresa) {

                if(init.getOfertaDelSimulador()) {
                    init.getParentManager().setActivityChanging(true);
                    init.showContratoConsumo(delegate.getOfertaConsumo(), delegate.getPromocion());
                }else{

                    if (init.getImporteParcial().equals("")) {
                        init.setImporteInicial(delegate.getOfertaConsumo().getImporte());
                        init.setImporteMinimo(delegate.getOfertaConsumo().getMontoMinimo());
                    }

                    paso1OperacionMap.put("evento_paso1", "event46");
                    if (init.getOfertaDelSimulador()) {
                        paso1OperacionMap.put("&&products", "simulador:contratacion consumo");
                        paso1OperacionMap.put("eVar12", "paso1:consulta detalle");
                    } else {
                        paso1OperacionMap.put("&&products", "promociones;detalle consumo");
                        paso1OperacionMap.put("eVar12", "paso1:clausulas consumo");
                    }
                    try {
                        customAmount = lblTexto7.getText().toString();
                        if (customAmount.length() > 0) {
                            String value = "";

                            if (customAmount.length() == 11) {
                                String valor2 = delegate.getOfertaConsumo().getImporte();
                                String valor3 = ConsumoTools.formatAmount(valor2);
                                value = valor3;
                            } else {
                                String value2 = ConsumoTools.formatSplitReverse(value);
                                String value3 = value2.replace("$", "");
                                montoModificado = Double.parseDouble(value3);
                                init.setImporteParcial(ConsumoTools.formatSplitReverse(value));
                            }
                        } else {
                            montoModificado = 0;
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (montoModificado == montoInicial) {
                        init.getParentManager().setActivityChanging(true);
                        init.showContratoConsumo(delegate.getOfertaConsumo(), delegate.getPromocion());
                        TrackingConsumo.trackPaso1Operacion(paso1OperacionMap);

                    } else if (montoModificado <= montoInicial) {

                        init.getBaseViewController().muestraIndicadorActividad(DetalleConsumoViewController.this, "", "Calculando");

                        String valor2 = delegate.getOfertaConsumo().getImporte();
                        String valor3 = ConsumoTools.formatAmount(valor2);
                        init.setImporteInicial(valor3.replace(".", ""));

                        init.setChanged(true);
                        init.getParentManager().setActivityChanging(true);
                        init.showContratoConsumo(delegate.getOfertaConsumo(), delegate.getPromocion());
                        TrackingConsumo.trackPaso1Operacion(paso1OperacionMap);
                    }
                }
            }
            if(v == iconBack){
                if(!init.getOfertaDelSimulador()) {
                    delegate.confirmarRechazoatras();
                }else{
                    showAlert();
                }
            }
        }
    };

    private void showAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DetalleConsumoViewController.this);
        alertDialog.setTitle("Aviso");
        alertDialog.setMessage("¿Quieres abandonar el proceso de contratación de tu crédito?");
        alertDialog.setIcon(R.drawable.anicalertaaviso);

        alertDialog.setPositiveButton("Salir", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                delegate.setDescripcionMensaje("");
                delegate.setOpFlujo(1);
                init.getParentManager().setActivityChanging(true);
                ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                init.resetInstance();
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            paso1OperacionMap.put("evento_paso1", "event46");
            paso1OperacionMap.put("&&products", "promociones;detalle consumo");
            paso1OperacionMap.put("eVar12", "paso1:rechazo oferta consumo");

            TrackingConsumo.trackPaso1Operacion(paso1OperacionMap);

            if(!init.getOfertaDelSimulador()) {
                delegate.confirmarRechazoatras();
            }else{
                showAlert();
            }
            borrarSharedPrefences();
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);
    }

    @Override
    protected void onPause() {
        super.onPause();
       /* if (!init.getParentManager().isActivityChanging()) {
           // TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }*/
    }

    @Override
    public void onUserInteraction() {
        //super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    private void setEditTextListener() {
        df = new DecimalFormat("#,###.##");
        df.setDecimalSeparatorAlwaysShown(true);
        dfnd = new DecimalFormat("#,###");

        edtTextoMonto.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                edtTextoMonto.removeTextChangedListener(this);

                try {

                    String v = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
                    Number n = df.parse(v);
                    edtTextoMonto.setText(dfnd.format(n).toString() + ".00");

                    if (edtTextoMonto.getText().length() > 2)
                        edtTextoMonto.setSelection(edtTextoMonto.getText().length() - 3);
                    else
                        edtTextoMonto.setSelection(edtTextoMonto.getText().length());

                    String strEnteredVal = edtTextoMonto.getText().toString();

                    if (strEnteredVal.equals("") || strEnteredVal.equals("0.00") && editable == true) {

                        String credito = delegate.getOfertaConsumo().getImporte();
                        btnmeInteresa.setBackgroundResource(R.drawable.btn_meinteresa);
                        edtTextoMonto.setHint(ConsumoTools.formatCredit(credito));
                        editable = false;
                    } else if (!strEnteredVal.equals("") && !strEnteredVal.equals("0.00")) {
                        btnmeInteresa.setBackgroundResource(R.drawable.btn_modificar_monto_calcular);
                    } else if (strEnteredVal.equals("0.00") && editable == false) {
                        btnmeInteresa.setBackgroundResource(R.drawable.btn_meinteresa);
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                    Log.w("After click on Calcular", "is normal to see this exception");
                }
                edtTextoMonto.addTextChangedListener(this);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        edtTextoMonto.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (editable == false) {
                    if (edtTextoMonto.getText().length() > 2)
                        edtTextoMonto.setSelection(edtTextoMonto.getText().length() - 3);
                    else
                        edtTextoMonto.setSelection(edtTextoMonto.getText().length());
                    editable = true;
                }
                return false;
            }
        });

        edtTextoMonto.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    InputMethodManager manager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (manager != null)
                        manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }

    public void aleatorioMostrarBoton() {
        borrarSharedPrefences();
        if (!obtenerSharedPreferences()) {
            Random numAleatorio = new Random();
            int valor = 2;
            int numObtenido;
            numObtenido = numAleatorio.nextInt(50);
            if (valor == numObtenido) { // == es de simulador
                iconCallmeBack.setVisibility(View.INVISIBLE);
                String num = Integer.toString(numObtenido);
                isBtnLlamar = false;
                guardarSharedPreferences(isBtnLlamar);
            } else {
                isBtnLlamar = true;
                guardarSharedPreferences(isBtnLlamar);
                iconCallmeBack.setVisibility(View.VISIBLE);
                paso1OperacionMap.put("evento_paso1", "event46_aparece botón");
            }
        } else {

        }
    }

    //Metodo para guardar en el ciclo de vida del consumo el boton de callmeBack
    public void guardarSharedPreferences(boolean variableBoolean) {
        String preferencia = "MIS_PREFRENCIAS";
        int modoPrivacidad = Context.MODE_PRIVATE;
        SharedPreferences.Editor editor = getSharedPreferences(preferencia, modoPrivacidad).edit();
        editor.putBoolean("keyButtoncall", variableBoolean);
        editor.commit();
    }

    //Metodo para sacar el valor de shared preference
    public boolean obtenerSharedPreferences() {
        String preferencia = "MIS_PREFRENCIAS";
        int modoPrivacidad = Context.MODE_PRIVATE;
        SharedPreferences preferences = getSharedPreferences(preferencia, modoPrivacidad);
        boolean valorObtenido = preferences.getBoolean("keyButtoncall", false);
        return valorObtenido;
    }

    /***
     * ::::::: Borrando lo del sharedPrefente ::::::
     **/
    public void borrarSharedPrefences() {
        Log.d("Borrando", "VARIABLE BUTTON CALLMEBACK");
        String preferencia = "MIS_PREFRENCIAS";
        int modoPrivacidad = Context.MODE_PRIVATE;
        SharedPreferences preferences = getSharedPreferences(preferencia, modoPrivacidad);
        preferences.edit().clear().commit();
    }

    public void showErrorMessage(String errorMessage, DialogInterface.OnClickListener listener) {

        if (errorMessage.length() > 0) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.label_error);
            alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
            alertDialogBuilder.setMessage(errorMessage);
            alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.show();
        }
    }
}
