package bancomer.api.consumo.gui.delegates;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.util.Hashtable;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.gui.delegates.SecurityComponentDelegate;

import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.R;
import bancomer.api.consumo.gui.controllers.ContratoConsumoViewController;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.AceptaOfertaConsumo;
import bancomer.api.consumo.models.ConsultaPoliza;
import bancomer.api.consumo.models.ListaCupones;
import bancomer.api.consumo.models.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtp;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtpBmovil;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;

public class ContratoOfertaConsumoDelegate extends SecurityComponentDelegate implements BaseDelegate {

    public static final long CONTRATO_OFERTA_CONSUMO_DELEGATE = 851451334266765688L;

    private Constants.Operacion tipoOperacion;
    private Constants.TipoInstrumento tipoInstrumentoSeguridad;
    private InitConsumo init = InitConsumo.getInstance();
    private BaseViewController baseViewControllerInstance;
    private ContratoConsumoViewController controller;
    OfertaConsumo ofertaConsumo;
    Promociones promocion;
    ListaCupones listaCupones;
    ListaCupones.Cupon cupon;
    String token;
    AceptaOfertaConsumo aceptaOferta;
    Constants.TipoOtpAutenticacion tipoOTP;
    private int opFlujo;
    private String descripcionMensaje;
    private boolean bandBack = false;
    String textCuponera = "";

    public void setController(ContratoConsumoViewController controller) {
        this.controller = controller;
        setContext(controller);
    }

    public ContratoOfertaConsumoDelegate(OfertaConsumo ofertaConsumo, Promociones promocion) {
        init = InitConsumo.getInstance();
        this.ofertaConsumo = ofertaConsumo;
        this.promocion = promocion;
        String instrumento = init.getSession().getSecurityInstrument();
        //ehmendezr validacion de S2
        if (instrumento.equals(Constants.IS_TYPE_DP270)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
        } else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
        } else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)||instrumento.equals(Constants.TYPE_SOFTOKEN.S2.value)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
        } else {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
        }
    }

    public OfertaConsumo getOfertaConsumo() {
        return ofertaConsumo;
    }

    public void realizaOperacion(int operacion, String numeroCredido){
        if (operacion == Server.CONSULTA_STATUS_CUPONERA){
            ICommonSession session = init.getSession();
            String numCel = session.getUsername();

            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion","consultaCuponeraConsumo");
            params.put("numeroCelular",numCel);
            params.put("numCredito",numeroCredido);
            doNetworkOperation(Server.CONSULTA_STATUS_CUPONERA, params,
                    init.getBaseViewController(), true, new Object(), Server.isJsonValueCode.CONSUMO);
        }
    }

    public void realizaOperacion(int idOperacion, int tipoSegBBVA) {
        ICommonSession session = init.getSession();

        String ium = session.getIum();
        String numCel = session.getUsername();

        if (idOperacion == Server.TERMINOS_OFERTA_CONSUMO) {
            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion", "consultaContratoConsumoBMovil");
            params.put("opcion", "1");
            params.put("IdProducto", ofertaConsumo.getProducto());
            params.put("versionE", " ");
            params.put("numeroCelular", numCel);
            params.put("IUM", ium);
            Hashtable<String, String> paramTable2 = AuxConectionFactory.terminosConsumo(params);
            doNetworkOperation(Server.TERMINOS_OFERTA_CONSUMO, paramTable2, init.getBaseViewController(), true, new ConsultaPoliza(), Server.isJsonValueCode.CONSUMO);
        } else if (idOperacion == Server.EXITO_OFERTA_CONSUMO) {

            String cadAutenticacion = init.getAutenticacion().getCadenaAutenticacion(this.tipoOperacion, init.getSession().getClientProfile());

            Hashtable<String, String> params = new Hashtable<String, String>();

            params.put("numeroCelular", numCel);
            params.put("claveCamp", promocion.getCveCamp());
            params.put("estatus", ofertaConsumo.getEstatusOferta());

            if (token != null)
                params.put("codigoOTP", token);
            else
                params.put("codigoOTP", "");

            //Modificacion 50870
            if (!init.getOfertaDelSimulador()) {
                params.put("operacion", "oneClickBmovilConsumo");
            } else {
                Log.i("revire","contratación, getIndR: "+ofertaConsumo.getIndRev());
                params.put("IndRev",ofertaConsumo.getIndRev());
                params.put("operacion", "contrataAlternativaConsumo");
                params.put("producto", ofertaConsumo.getCveProd());
            }

            params.put("cadenaAutenticacion", cadAutenticacion);

            if (tipoSegBBVA == 1)
                params.put("seguroObli", "BBVA SEG");
            else if (tipoSegBBVA == 2)
                params.put("seguroObli", "OTRA ASEG");
            else
                params.put("seguroObli", ""); //tiporadioburron

            params.put("IUM", ium);
            params.put("importePar", ofertaConsumo.getImporte().toString());
            params.put("codigoNIP", "");
            params.put("codigoCVV2", "");
            params.put("cveAcceso", "");
            params.put("tarjeta5Dig", "");
            InitConsumo init = InitConsumo.getInstance();
            if (!init.getOfertaDelSimulador()) {
                Hashtable<String, String> paramTable2 = AuxConectionFactory.exitoConsumo(params,false);
                doNetworkOperation(Server.EXITO_OFERTA_CONSUMO, paramTable2, init.getBaseViewController(), true, new AceptaOfertaConsumo(), Server.isJsonValueCode.CONSUMO);
            } else {
                Hashtable<String, String> paramTable2 = AuxConectionFactory.exitoConsumo(params,true);
                doNetworkOperation(Server.CONTRATA_ALTERNATIVA_CONSUMO, paramTable2, init.getBaseViewController(), true, new AceptaOfertaConsumo(), Server.isJsonValueCode.SIMULADOR);
            }

        } else if (idOperacion == Server.RECHAZO_OFERTA_CONSUMO) {
            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion", "noAceptacionBMovil");
            params.put("numeroCelular", numCel);
            params.put("cveCamp", promocion.getCveCamp());
            params.put("descripcionMensaje", getDescripcionMensaje());
            params.put("folioUG", ofertaConsumo.getFolioUG()); // definir de donde se obtendra
            params.put("IUM", ium);
            Hashtable<String, String> paramTable2 = AuxConectionFactory.rechazoOfertaConsumo(params);
            doNetworkOperation(Server.RECHAZO_OFERTA_CONSUMO, paramTable2, init.getBaseViewController(), true, new Object(), Server.isJsonValueCode.ONECLICK);
        }
    }

    public void peticionBtnLlamenme() {
        ICommonSession session = init.getSession();

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put("operacion", "noAceptacionBMovil");
        params.put("numeroCelular", session.getUsername());
        params.put("cveCamp", promocion.getCveCamp());
        params.put("descripcionMensaje", getDescripcionMensaje());
        params.put("folioUG", ofertaConsumo.getFolioUG()); // definir de donde se obtendra
        params.put("IUM", session.getIum());
        Hashtable<String, String> paramTable2 = AuxConectionFactory.rechazoOfertaConsumo(params);
        doNetworkOperation(Server.RECHAZO_OFERTA_CONSUMO, paramTable2, init.getBaseViewController(), true, new Object(), Server.isJsonValueCode.ONECLICK);
    }

    public void peticionLlamame(BaseViewController baseViewController, String importePar) {
        ICommonSession session = init.getSession();
        String ium = session.getIum();
        String numCel = session.getUsername();
        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put("operacion", "ayudaPrestamoConsumo");
        params.put("numeroCelular", numCel);
        params.put("claveCamp", promocion.getCveCamp());
        params.put("IUM", ium);
        params.put("importePar", "000");

        Hashtable<String, String> paramTable2 = AuxConectionFactory.llamameConsumo(params);

        doNetworkOperation(Server.OP_AYUDA_PRESTAMO_CONSUMO, paramTable2,
                init.getBaseViewController(), true, new Object(), Server.isJsonValueCode.CONSUMO);
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        InitConsumo.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false);
    }

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle, Server.isJsonValueCode isJsonValue) {
        InitConsumo.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false, isJson, handle, isJsonValue);

    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

        if(operationId == Server.CONTRATA_ALTERNATIVA_CONSUMO) {
            if(response.getStatus() == ServerResponse.OPERATION_ERROR){
                aceptaOferta= (AceptaOfertaConsumo)response.getResponse();
                if(aceptaOferta.getPromociones()!=null){
                    init.getSession().setPromocion(aceptaOferta.getPromociones());

                    init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
                            new OnClickListener() {
                                @Override
                                public void onClick( DialogInterface dialog,int which) {
                                    init.getParentManager().setActivityChanging(true);
                                    ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                                    init.resetInstance();
                                }
                            });
                }else{

                    init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
                            new OnClickListener() {
                                @Override
                                public void onClick(
                                        DialogInterface dialog,
                                        int which) {
                                }
                            });
                }
            }else{
                aceptaOferta= (AceptaOfertaConsumo)response.getResponse();
                ICommonSession session = init.getSession();
                session.setPromocion(aceptaOferta.getPromociones());
                if(ofertaConsumo.getEstatusOferta().equals(Constants.PREFORMALIZADO)){
                    init.showExitoConsumo(aceptaOferta, ofertaConsumo);//pantalla exito
                }else if(ofertaConsumo.getEstatusOferta().equals(Constants.PREABPROBADO)){
                    OnClickListener listener = new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int arg1) {
                            dialog.dismiss();
                            init.getParentManager().setActivityChanging(true);
                            ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                            init.resetInstance();
                        }
                    };
                    if(aceptaOferta.getScoreSal().equals("RE")){
                        init.getBaseViewController().showInformationAlert(controller, "Su crédito fue rechazado por buró.", listener);

                    }else if(aceptaOferta.getScoreSal().equals("DI")){
                        init.getBaseViewController().showInformationAlert(controller,"Su crédito no fue autorizado, favor de acudir a sucursal.",listener);

                    }else if(aceptaOferta.getScoreSal().equals("AP")||aceptaOferta.getScoreSal().equals("NB")){
                        init.showExitoConsumo(aceptaOferta,ofertaConsumo);//pantalla exito
                    }
                }
            }
        }
        if (operationId == Server.TERMINOS_OFERTA_CONSUMO) {
            ConsultaPoliza polizaResponse = (ConsultaPoliza) response.getResponse();
            init.showTerminosConsumo(polizaResponse.getTerminosHtml());
        } else if (operationId == Server.EXITO_OFERTA_CONSUMO) {
            if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
                aceptaOferta = (AceptaOfertaConsumo) response.getResponse();

                if (aceptaOferta.getPromociones() != null) {
                    init.getSession().setPromocion(aceptaOferta.getPromociones());

                    init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
                            new OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    init.getParentManager().setActivityChanging(true);
                                    ((CallBackModule) init.getParentManager().getRootViewController()).returnToPrincipal();
                                    init.resetInstance();
                                }
                            });
                } else {

                    init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
                            new OnClickListener() {
                                @Override
                                public void onClick(
                                        DialogInterface dialog,
                                        int which) {
                                }
                            });

                }
            } else {
                aceptaOferta = (AceptaOfertaConsumo) response.getResponse();
                ICommonSession session = init.getSession();
                session.setPromocion(aceptaOferta.getPromociones());

                if (ofertaConsumo.getEstatusOferta().equals(Constants.PREFORMALIZADO)) {
                    init.showExitoConsumo(aceptaOferta, ofertaConsumo,listaCupones,cupon);//pantalla exito
                } else if (ofertaConsumo.getEstatusOferta().equals(Constants.PREABPROBADO)) {
                    OnClickListener listener = new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int arg1) {
                            dialog.dismiss();
                            init.getParentManager().setActivityChanging(true);
                            ((CallBackModule) init.getParentManager().getRootViewController()).returnToPrincipal();
                            init.resetInstance();
                        }
                    };
                    if (aceptaOferta.getScoreSal().equals("RE")) {

                        init.getBaseViewController().showInformationAlert(controller, "Su crédito fue rechazado por buró.", listener);

                    } else if (aceptaOferta.getScoreSal().equals("DI")) {
                        init.getBaseViewController().showInformationAlert(controller, "Su crédito no fue autorizado, favor de acudir a sucursal.", listener);

                    } else if (aceptaOferta.getScoreSal().equals("AP") || aceptaOferta.getScoreSal().equals("NB")) {
                        Log.d("ISAI", "LA OFERTA ES IGUAL :"+ aceptaOferta.getScoreSal().equals("AP") +" O "+ aceptaOferta.getScoreSal().equals("NB"));
                        init.showExitoConsumo(aceptaOferta, ofertaConsumo,listaCupones,cupon);//pantalla exito
                    }
                }
            }
        } else if (operationId == Server.RECHAZO_OFERTA_CONSUMO) {
            if(bandBack){
                confirmaAtras();
            }
        } else if (operationId == Server.OP_AYUDA_PRESTAMO_CONSUMO) {
            init.getBaseViewController().ocultaIndicadorActividad();
                init.getBaseViewController().showInformationAlert(controller, R.string.api_exitocallmeback_consumo_descripcion);
        } else if (operationId == Server.CONSULTA_STATUS_CUPONERA) {
            textCuponera = " ";
            //Respuesta del consulta Cuponera
            System.out.print("OK, ENTRO CONSULTA_STATUS_CUPONERA");
            System.out.print("OK, TODO OK");
            textCuponera = "Cuponera";
        }
    }

    public void confirmaAtras(){
        init.getParentManager().setActivityChanging(true);
        ((CallBackModule) init.getParentManager().getRootViewController()).returnToPrincipal();
        init.getParentManager().resetRootViewController();
        init.resetInstance();
    }

    @Override
    public void performAction(Object obj) {

        ICommonSession session = init.getSession();
        String ium = session.getIum();
        String numCel = session.getUsername();

        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put("numeroCelular", numCel);
        params.put("cveCamp", promocion.getCveCamp());
        params.put("descripcionMensaje", "0027");
        params.put("folioUG", ofertaConsumo.getFolioUG()); // definir de donde se obtendra
        params.put("IUM", ium);
        Hashtable<String, String> paramTable2 = AuxConectionFactory.rechazoOfertaConsumo(params);

        doNetworkOperation(Server.RECHAZO_OFERTA_CONSUMO, paramTable2, InitConsumo.getInstance().getBaseViewController(), true, new Object(),
                Server.isJsonValueCode.CONSUMO);

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

    public void showPoliza() {
        if (ofertaConsumo.getProducto().equals("PBCCMNOM01"))
            init.showPoliza("https://www.bancomermovil.com/imgsp_mx_web/imgsp_mx_web/web/ConsumoOneClic/Polizas/PolizaDeDesempleo/SeguroDesempleo.html");
        else if (ofertaConsumo.getProducto().equals("PBCCMNOM02"))
            init.showPoliza("https://www.bancomermovil.com/imgsp_mx_web/imgsp_mx_web/web/ConsumoOneClic/Polizas/PolizaSeguroDeVida.html");
        else if (ofertaConsumo.getProducto().equals("PBCCMPPI01"))
            init.showPoliza("https://www.bancomermovil.com/imgsp_mx_web/imgsp_mx_web/web/ConsumoOneClic/Polizas/PolizaSeguroDeVida.html");
    }


    public Constants.Operacion getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(Constants.Operacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public boolean mostrarContrasenia() {
        return init.getAutenticacion().mostrarContrasena(this.tipoOperacion,
                init.getSession().getClientProfile());
    }

    public Constants.TipoOtpAutenticacion tokenAMostrar() {
        Constants.TipoOtpAutenticacion tipoOTP;
        try {
            tipoOTP = init.getAutenticacion().tokenAMostrar(this.tipoOperacion,
                    init.getSession().getClientProfile());

        } catch (Exception ex) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "Error on Autenticacion.mostrarNIP execution.", ex);
            tipoOTP = null;
        }

        return tipoOTP;
    }

    public boolean mostrarNIP() {
        return init.getAutenticacion().mostrarNIP(this.tipoOperacion,
                init.getSession().getClientProfile());
    }

    public boolean mostrarCampoTarjeta() {
        return false;
    }

    public boolean mostrarCVV() {

        return init.getAutenticacion().mostrarCVV(this.tipoOperacion,
                init.getSession().getClientProfile());
    }


    public boolean enviaPeticionOperacion() {
        String contrasena = null;
        String nip = null;
        String asm = null;
        String cvv = null;
        if (controller instanceof ContratoConsumoViewController) {
            if (mostrarContrasenia()) {
                contrasena = controller.pideContrasena();
                if (contrasena.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.PASSWORD_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            String tarjeta = null;
            if (mostrarCampoTarjeta()) {
                tarjeta = controller.pideTarjeta();
                String mensaje = "";
                if (tarjeta.equals("")) {
                    mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (tarjeta.length() != 5) {
                    mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            if (mostrarNIP()) {
                nip = controller.pideNIP();
                if (nip.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (nip.length() != Constants.NIP_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.NIP_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) {
                asm = controller.pideASM();
                if (asm.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (asm.length() != Constants.ASM_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.ASM_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (mostrarCVV()) {
                cvv = controller.pideCVV();
                if (cvv.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (cvv.length() != Constants.CVV_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.CVV_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            String newToken = null;
            if (tokenAMostrar() != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken && (init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService())) {
                if (init.getSofTokenStatus()) {
                    newToken = init.getDelegateOTP().loadOtpFromSofttoken(tokenAMostrar(), init.getDelegateOTP());
                    if (null != newToken)
                        asm = newToken;
                    token = asm;
                    controller.finaliceOTP();
                } else if (!init.getSofTokenStatus() && PropertiesManager.getCurrent().getSofttokenService()) {

                    GetOtpBmovil otpG = new GetOtpBmovil(new GetOtp() {
                        /**
                         * @param otp
                         */
                        @Override
                        public void setOtpRegistro(String otp) {

                        }

                        /**
                         * @param otp
                         */
                        @Override
                        public void setOtpCodigo(String otp) {
                            if (ServerCommons.ALLOW_LOG) {
                                Log.d("APP", "la otp en callback: " + otp);
                            }
                            token = String.valueOf(otp);
                            controller.finaliceOTP();
                        }

                        /**
                         * @param otp
                         */
                        @Override
                        public void setOtpCodigoQR(String otp) {

                        }
                    }, SuiteApp.appContext);

                    otpG.generateOtpCodigo();
                }
            }
            else if(tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno ) {
                token = asm;
                controller.finaliceOTP();
            }
            else if(tokenAMostrar() == Constants.TipoOtpAutenticacion.ninguno){
                token = asm;
                controller.finaliceOTP();
            }
            return true;
        }
        return false;
    }

    public Constants.TipoInstrumento consultaTipoInstrumentoSeguridad() {
        return tipoInstrumentoSeguridad;
    }


    public void confirmarRechazoatras() {
        init.getBaseViewController().showYesNoAlert(controller, "¿Quieres abandonar el proceso de contratación de tu crédito?",
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (DialogInterface.BUTTON_POSITIVE == which) {
                            bandBack=true;
                            setDescripcionMensaje("0002");
                            realizaOperacion(Server.RECHAZO_OFERTA_CONSUMO, 0);
                            Log.e("BUTTON_POSITIVE", "ok-" + bandBack);
                            //dialog.dismiss();

                        } else if (DialogInterface.BUTTON_NEGATIVE == which) {
                            Log.e("BUTTON_NEGATIVE", "ok-");
                            dialog.dismiss();
                        }

                    }
                });
    }


    public String getDescripcionMensaje() {
        return descripcionMensaje;
    }

    public void setDescripcionMensaje(String descripcionMensaje) {
        this.descripcionMensaje = descripcionMensaje;
    }

    public int getOpFlujo() {
        return opFlujo;
    }

    public void setOpFlujo(int opFlujo) {
        this.opFlujo = opFlujo;
    }
}
