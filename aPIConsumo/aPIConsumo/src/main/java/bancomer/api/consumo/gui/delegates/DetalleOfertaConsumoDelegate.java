package bancomer.api.consumo.gui.delegates;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;
import android.widget.Toast;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.gui.delegates.BaseDelegate;

import java.util.Hashtable;

import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.consumo.R;
import bancomer.api.consumo.gui.controllers.DetalleConsumoViewController;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.AceptaOfertaConsumo;
import bancomer.api.consumo.models.ConsultaPoliza;
import bancomer.api.consumo.models.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

public class DetalleOfertaConsumoDelegate implements BaseDelegate {

    public static final long DETALLE_OFERTA_CONSUMO_DELEGATE = 631451334266765688L;

    private Constants.Operacion tipoOperacion;
    private DetalleConsumoViewController controller;
    private OfertaConsumo ofertaConsumo; //oferta consumo
    private BaseViewController baseViewControllerInstance;
    private Promociones promocion;
    private String fechaCatVisual;
    private InitConsumo init;
    private String importe;
    String token;
    private String importePar = "";
    private String descripcionMensaje;
    private int opFlujo;
    private String modificarImporte = " ";
    private boolean isCallmeBack;
    private boolean bandBack = false;


    public String getModificarImporte() {
        return modificarImporte;
    }

    public void setModificarImporte(String modificarImporte) {
        this.modificarImporte = modificarImporte;
    }

    public int getOpFlujo() {
        return opFlujo;
    }

    public void setOpFlujo(int opFlujo) {
        this.opFlujo = opFlujo;
    }

    public String getDescripcionMensaje() {
        return descripcionMensaje;
    }

    public void setDescripcionMensaje(String descripcionMensaje) {
        this.descripcionMensaje = descripcionMensaje;
    }

    public DetalleOfertaConsumoDelegate(OfertaConsumo ofertaConsumo, Promociones promocion, boolean isCallmeBack) {

        init = InitConsumo.getInstance();
        this.ofertaConsumo = ofertaConsumo;
        this.promocion = promocion;
        this.isCallmeBack = isCallmeBack;
    }

    public DetalleOfertaConsumoDelegate(OfertaConsumo ofertaConsumo, Promociones promocion, String importePar, String modificarImporte, boolean isCallmeBack) {

        init = InitConsumo.getInstance();
        this.ofertaConsumo = ofertaConsumo;
        this.promocion = promocion;
        this.importePar = importePar;
        this.modificarImporte = modificarImporte;
        this.isCallmeBack = isCallmeBack;
    }

    public String getImportePar() {
        return importePar;
    }

    public void setImportePar(String importePar) {
        this.importePar = importePar;
    }

    public OfertaConsumo getOfertaConsumo() {
        return ofertaConsumo;
    }

    public void setOfertaConsumo(OfertaConsumo ofertaConsumo) {
        this.ofertaConsumo = ofertaConsumo;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public void setPromocion(Promociones promocion) {
        this.promocion = promocion;
    }

    public String getfechaCatVisual() {
        return fechaCatVisual;
    }

    public void setfechaCatVisualt(String fechaCatVisual) {
        this.fechaCatVisual = fechaCatVisual;
    }


    public void setController(DetalleConsumoViewController controller) {
        this.controller = controller;
    }

    public boolean getIsCallmeBack() {
        return isCallmeBack;
    }

    public void setIsCallmeBack(boolean isCallmeBack) {
        this.isCallmeBack = isCallmeBack;
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        InitConsumo.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false);
    }

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle, Server.isJsonValueCode isJsonValue) {
        InitConsumo.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false, isJson, handle, isJsonValue);

    }

    public void analyzeResponse(int operationId, ServerResponse response) {
        if (operationId == Server.RECHAZO_OFERTA_CONSUMO) {
            if(bandBack){
                init.getParentManager().setActivityChanging(true);
                ((CallBackModule) init.getParentManager().getRootViewController()).returnToPrincipal();
                init.getParentManager().resetRootViewController();
                init.resetInstance();
            }
        } else if (operationId == Server.CONSULTA_DETALLE_OFERTA_CONSUMO) { // si la operacion es consuta detalle
            ofertaConsumo = (OfertaConsumo) response.getResponse();
            controller.setTextToView();
        } else if (operationId == Server.OP_AYUDA_PRESTAMO_CONSUMO) {
            init.getBaseViewController().showInformationAlert(controller, R.string.api_exitocallmeback_consumo_descripcion);
        }
    }
    @Override
    public void performAction(Object obj) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

    public void confirmarRechazoatras() {
        init.getBaseViewController().showYesNoAlert(controller, "¿Quieres abandonar el proceso de contratación de tu crédito?",
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (DialogInterface.BUTTON_POSITIVE == which) {
                            bandBack=true;
                            setDescripcionMensaje("0002");
                            realizaOperacion(Server.RECHAZO_OFERTA_CONSUMO, 0);
                            //dialog.dismiss();

                        } else if (DialogInterface.BUTTON_NEGATIVE == which) {
                            dialog.dismiss();
                        }

                    }
                });
    }

    public void formatoFechaCatMostrar() {
        try {
            String[] meses = {"enero", "febrero", "marzo", "abril", "mayo",
                    "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"};
            String fechaCatM = ofertaConsumo.getFechaCat();
            int mesS = Integer.parseInt(fechaCatM.substring(5, 7));
            String dia = fechaCatM.substring(fechaCatM.length() - 2);
            String anio = fechaCatM.substring(0, 4);

            fechaCatVisual = dia + " de " + meses[mesS - 1].toString() + " de " + anio;

        } catch (Exception ex) {
            if (Server.ALLOW_LOG) Log.e("", "error formato" + ex);
        }
    }

    public void peticionLlamame(BaseViewController baseViewController, String importePar) {

        ICommonSession session = init.getSession();
        String ium = session.getIum();
        String numCel = session.getUsername();
        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put("operacion", "ayudaPrestamoConsumo");
        params.put("numeroCelular", numCel);
        params.put("claveCamp", promocion.getCveCamp());
        params.put("IUM", ium);
        params.put("importePar", "000");

        Hashtable<String, String> paramTable2 = AuxConectionFactory.llamameConsumo(params);

        doNetworkOperation(Server.OP_AYUDA_PRESTAMO_CONSUMO, paramTable2,
                init.getBaseViewController(), true, new Object(), Server.isJsonValueCode.CONSUMO);
    }


    public void realizaOperacion(int idOperacion, int tipoSegBBVA) {
        ICommonSession session = init.getSession();

        String ium = session.getIum();
        String numCel = session.getUsername();

        if (idOperacion == Server.TERMINOS_OFERTA_CONSUMO) {
            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion", "consultaContratoConsumoBMovil");
            params.put("opcion", "1");
            params.put("IdProducto", ofertaConsumo.getProducto());
            params.put("versionE", " ");
            params.put("numeroCelular", numCel);
            params.put("IUM", ium);
            Hashtable<String, String> paramTable2 = AuxConectionFactory.terminosConsumo(params);
            doNetworkOperation(Server.TERMINOS_OFERTA_CONSUMO, paramTable2, init.getBaseViewController(), true,
                    new ConsultaPoliza(), Server.isJsonValueCode.CONSUMO);
        } else if (idOperacion == Server.EXITO_OFERTA_CONSUMO) {
            Hashtable<String, String> params = new Hashtable<String, String>();

            params.put("numeroCelular", numCel);
            params.put("claveCamp", promocion.getCveCamp());
            params.put("estatus", ofertaConsumo.getEstatusOferta());
            if (token != null)
                params.put("codigoOTP", token);
            else
                params.put("codigoOTP", "");

            //Modificacion 50870
            if (!init.getOfertaDelSimulador()) {
                params.put("operacion", "oneClickBmovilConsumo");
                String cadAutenticacion = init.getAutenticacion().getCadenaAutenticacion(this.tipoOperacion, init.getSession().getClientProfile());
                params.put("cadenaAutenticacion", cadAutenticacion);
            } else {
                params.put("operacion", "contrataAlternativaConsumo");
                params.put("producto", ofertaConsumo.getCveProd());
            }

            if (tipoSegBBVA == 1)
                params.put("seguroObli", "BBVA SEG");
            else if (tipoSegBBVA == 2)
                params.put("seguroObli", "OTRA ASEG");
            else
                params.put("seguroObli", ""); //tiporadioburron


            params.put("IUM", ium);
            params.put("importePar", ofertaConsumo.getImporte());
            params.put("codigoNIP", "");
            params.put("codigoCVV2", "");
            params.put("cveAcceso", "");
            params.put("tarjeta5Dig", "");
            InitConsumo init = InitConsumo.getInstance();
            if (!init.getOfertaDelSimulador()) {
                Hashtable<String, String> paramTable2 = AuxConectionFactory.exitoConsumo(params,false);
                doNetworkOperation(Server.EXITO_OFERTA_CONSUMO, paramTable2, init.getBaseViewController(), true, new AceptaOfertaConsumo(), Server.isJsonValueCode.CONSUMO);
            } else {
                Hashtable<String, String> paramTable2 = AuxConectionFactory.exitoConsumo(params, false);
                doNetworkOperation(Server.CONTRATA_ALTERNATIVA_CONSUMO, paramTable2, init.getBaseViewController(), true, new AceptaOfertaConsumo(), Server.isJsonValueCode.SIMULADOR);
            }

        } else if (idOperacion == Server.RECHAZO_OFERTA_CONSUMO) {
            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put("operacion", "noAceptacionBMovil");
            params.put("numeroCelular", numCel);
            params.put("cveCamp", promocion.getCveCamp());
            params.put("descripcionMensaje", getDescripcionMensaje());
            params.put("folioUG", ofertaConsumo.getFolioUG()); // definir de donde se obtendra
            params.put("IUM", ium);
            Hashtable<String, String> paramTable2 = AuxConectionFactory.rechazoOfertaConsumo(params);
            doNetworkOperation(Server.RECHAZO_OFERTA_CONSUMO, paramTable2,
                    init.getBaseViewController(), true, new Object(), Server.isJsonValueCode.ONECLICK);
        }
    }
}

