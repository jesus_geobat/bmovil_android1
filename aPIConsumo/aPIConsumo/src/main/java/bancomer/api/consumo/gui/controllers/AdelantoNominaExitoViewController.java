package bancomer.api.consumo.gui.controllers;

import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consumo.R;
import bancomer.api.consumo.gui.delegates.AdelantoNominaExitoDelegate;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.Server;

/**
 * Created by FHERNANDEZ on 29/02/16.
 */
public class AdelantoNominaExitoViewController extends BaseActivity implements View.OnClickListener {

    private AdelantoNominaExitoDelegate delegate;

    private ImageView imgheaderANOMExito;
    private ImageView img_aprobadoANOM;
    private ImageView img_DocumentoANOM;

    private ImageButton btnCerrarANOM;

    private TextView lblFelicidadesANOM;
    private TextView lblFelicidadesANOM2;
    private TextView lblImporteANOM;
    private TextView lblHiperContratoANOM;
    private TextView lblHiperDomiciliacionANOM;
    private TextView edtCorreoClienteANOM;
    private TextView edtNumCelularClienteANOM;

    private InitConsumo init = null;
    private BaseViewController baseViewController;
    public BaseViewsController parentManager;
    public  boolean isSMSEMail = true;
//    public Opinator opinator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            init = InitConsumo.getInstance();

            android.util.Log.w("Contrato", "Anom Exito, nuevoInit: "+init);

            baseViewController = init.getBaseViewController();
            parentManager = init.getParentManager();
            parentManager.setCurrentActivityApp(this);
            parentManager.setCurrentActivityApp(this,true);

            delegate = (AdelantoNominaExitoDelegate) parentManager.getBaseDelegateForKey(AdelantoNominaExitoDelegate.ADELANTO_NOMINA_EXITO_DELEGATE);
            delegate.setController(this);

            baseViewController.setActivity(this);
            baseViewController.onCreate(this, 0, R.layout.activity_credit_contrato_exito_adelanto_nomina);
            baseViewController.setDelegate(delegate);
            init.setBaseViewController(baseViewController);

            initView();

            Log.i("canom","exito, getimporte: "+delegate.getMontoSolicitado());
            String importe = Tools.formatAmount(delegate.getMontoSolicitado(),false);
            lblImporteANOM.setText(importe);

        }catch(NullPointerException ex){
            ex.printStackTrace();
        }
    }

    private void initView(){
        findViews();
        setDataEmailSms();
//        configurarPantalla();
    }

    public void setDataEmailSms(){
        ICommonSession session = init.getSession();

        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(Constants.NUMERO_TELEFONO_LENGTH);
        edtNumCelularClienteANOM.setFilters(FilterArray);

        if(isSMSEMail){
            if(!session.getEmail().equals("")){ //si tiene email guardado
                String user = session.getUsername();
                StringBuffer sb = new StringBuffer("*****");
                sb.append(user.substring(user.length() - Constants.VISIBLE_NUMBER_ACCOUNT));

                edtCorreoClienteANOM.setText(session.getEmail());
                edtCorreoClienteANOM.setEnabled(false);
                edtCorreoClienteANOM.setFocusable(false);

                edtNumCelularClienteANOM.setText(sb);
                edtNumCelularClienteANOM.setEnabled(false);
                edtNumCelularClienteANOM.setFocusable(false);
            }
        }

        delegate.realizaOperacion(Server.SMS_OFERTA_CONSUMO, isSMSEMail);
    }


    public void findViews(){
        imgheaderANOMExito          = (ImageView)findViewById(R.id.imgheaderANOMExito);
        img_aprobadoANOM            = (ImageView)findViewById(R.id.img_aprobadoANOM);
        img_DocumentoANOM           = (ImageView)findViewById(R.id.img_DocumentoANOM);

        lblFelicidadesANOM          = (TextView)findViewById(R.id.lblFelicidadesANOM);
        lblFelicidadesANOM2         = (TextView)findViewById(R.id.lblFelicidadesANOM2);
        lblImporteANOM              = (TextView)findViewById(R.id.lblImporteANOM);
        lblHiperContratoANOM        = (TextView)findViewById(R.id.lblHiperContratoANOM);
        lblHiperContratoANOM.setOnClickListener(this);
        lblHiperDomiciliacionANOM   = (TextView)findViewById(R.id.lblHiperDomiciliacionANOM);
        lblHiperDomiciliacionANOM.setOnClickListener(this);
        edtCorreoClienteANOM        = (TextView)findViewById(R.id.edtCorreoClienteANOM);
        edtNumCelularClienteANOM    = (TextView)findViewById(R.id.edtNumCelularClienteANOM);

        btnCerrarANOM               = (ImageButton)findViewById(R.id.btnCerrarANOM);

        btnCerrarANOM.setOnClickListener(this);
    }

    private void configurarPantalla() {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());

        gTools.scale(imgheaderANOMExito, false);
        gTools.scale(img_aprobadoANOM,false);
        gTools.scale(img_DocumentoANOM, false);

        gTools.scale(lblFelicidadesANOM, true);
        gTools.scale(lblFelicidadesANOM2, true);
        gTools.scale(lblImporteANOM,true);
        gTools.scale(lblHiperContratoANOM,true);
        gTools.scale(edtCorreoClienteANOM, true);
        gTools.scale(edtNumCelularClienteANOM, true);

    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onPause() {
        super.onPause();
       /* if(!init.getParentManager().isActivityChanging()) {
           // TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
        }*/
    }

    @Override
    public void onUserInteraction() {
       // super.onUserInteraction();
      //  init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);
        android.util.Log.w("Contrato", "fin onresume");

    }
/*
    private void showEncuesta(){
        String url =Server.BASE_OPINATOR_ENCUESTA+Server.CONSUMO_ILC_TDC_EXITO;

        Log.i("opinator","opinator a mostrar: "+url);
        LayoutInflater inflater = getLayoutInflater();

        final AlertDialog alertDialog;
        View dialoglayout = inflater.inflate(R.layout.activity_webview_opinator, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(AdelantoNominaExitoViewController.this);
        builder.setView(dialoglayout);
        alertDialog = builder.create();
        alertDialog.show();

        ImageButton btnCerrar = (ImageButton) dialoglayout.findViewById(R.id.btnCerrar);
        WebView webView = (WebView) dialoglayout.findViewById(R.id.webViewOpinator);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDisplayZoomControls(false);

        webView.loadUrl(url);


        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delegate.exitAnom();
                alertDialog.dismiss();
            }
        });

    }
 */

    @Override
    public void onClick(View view) {

        if (view == btnCerrarANOM) {
            init.getParentManager().setActivityChanging(true);
            delegate.exitAnom();
        }
        if (view == lblHiperContratoANOM) {
            Log.i("canom","consumo contrato");
            init.getParentManager().setActivityChanging(true);
            delegate.realizaOperacion(Server.CONTRATO_OFERTA_CONSUMO, false);
        }
        if (view == lblHiperDomiciliacionANOM) {
            Log.i("canom","consumo domiciliacion");
            init.getParentManager().setActivityChanging(true);
            delegate.realizaOperacion(Server.DOMICILIACION_OFERTA_CONSUMO, false);
        }

    }

}