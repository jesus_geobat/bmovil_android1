package bancomer.api.consumo.gui.controllers;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.EditText;

import bancomer.api.consumo.commons.Tools;
import bancomer.api.consumo.commons.ConstantsCredit;
/**
 * Created by isaigarciamoso on 29/07/16.
 */
public class LabelMontoTotalCredit extends EditText {


    private String montoSolicitado;
    private String montoMax;
    private String montoMin;

    public String getMontoMax() {
        return montoMax;
    }

    public void setMontoMax(String montoMax) {
        this.montoMax = montoMax;
    }

    public String getMontoMin() {
        return montoMin;
    }

    public void setMontoMin(String montoMin) {
        this.montoMin = montoMin;
    }
    /**
     * Contructores de la clase
     * */
    public LabelMontoTotalCredit(Context context) {
        super(context);
    }

    public LabelMontoTotalCredit(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LabelMontoTotalCredit(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected boolean getDefaultEditable() {
        return super.getDefaultEditable();
    }
    /**
     * Metodo que  me permite controlar el boton de back del Telefono
     * este código se eimplemeto por
     * */
    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {

        if(keyCode == KeyEvent.KEYCODE_BACK){
            montoSolicitado = getText().toString();
            montoMax = getMontoMax();
            montoMin = getMontoMin();

            if(!getText().toString().trim().equals("")) {
                String respuesta = Tools.validateMont(montoMax, montoMin, montoSolicitado, null);

                if (respuesta.equals(ConstantsCredit.MONTO_MAYOR)) {
                    setText(Tools.formatAmount(Tools.validateValueMonto(montoMax), false));
                    //((MenuPrincipalActivity)getContext()).actualizaGrafica(montoMax);
                } else if (respuesta.equals(ConstantsCredit.MONTO_MENOR)) {
                    setText(Tools.formatAmount(Tools.validateValueMonto(montoMin), false));
                    //((MenuPrincipalActivity)getContext()).actualizaGrafica(montoMin);
                } else if (respuesta.equals(ConstantsCredit.MONTO_EN_RANGO)) {
                    Log.w("validacion", "Monto en rango");
                    if(!getText().toString().startsWith("$"))
                        setText("$"+getText());
                    //((MenuPrincipalActivity)getContext()).actualizaGrafica(getText().toString());
                }
            }else
                Log.e("validacion", "Monto vacio");
        }
        return super.onKeyPreIme(keyCode, event);

    }
}
