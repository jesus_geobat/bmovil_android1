package bancomer.api.consumo.gui.delegates;

import android.util.Log;

import java.util.Hashtable;

import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.ListaCreditos;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;


/**
 * Created by isaigarciamoso on 08/09/16.
 */
public class ConsultaCreditosDelegate implements BaseDelegate {

    private InitConsumo init = InitConsumo.getInstance();
    private BaseViewController baseViewController;
    private ListaCreditos listaCreditos;

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController baseViewController) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, baseViewController, false);
    }

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle, Server.isJsonValueCode isJsonValue) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false, isJson, handle, isJsonValue);
    }

    @Override
    public void analyzeResponse(int operation, ServerResponse serverResponse) {

        if(operation == Server.CONSULTAR_CREDITOS_CUPONERA){
          if(serverResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

              listaCreditos = (ListaCreditos) serverResponse.getResponse();
              if(listaCreditos.getListaCreditos().size()>0) {
                  init.showDetalleCuponeraFromMenuConsulta(listaCreditos);
              }else{
                  init.getCallBackModule().returnDataFromModule("",serverResponse);
                  init.resetInstance();
              }
          }else if(serverResponse.getStatus() == ServerResponse.OPERATION_ERROR){
              init.getCallBackModule().returnDataFromModule("",serverResponse);
              init.resetInstance();
          }
        }
    }
    @Override
    public void performAction(Object o) {
        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(init.getParentManager().getCurrentViewController());
        baseViewController.setDelegate(this);
        init.setBaseViewController(baseViewController);

        ICommonSession session = init.getSession();
        String numCel = session.getUsername();
        String IUM = session.getIum();

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put("codigoOTP","");
        params.put("codigoNIP","");
        params.put("operacion","consultarCreditos");
        params.put("numeroCelular",numCel);
        params.put("tarjeta5Dig","");
        params.put("IUM", IUM);
        params.put("codigoCVV2","");
        params.put("cadenaAutenticacion","00000");
        params.put("cveAcceso","");
        Hashtable<String, String> paramTable2  = AuxConectionFactory.creditosCuponeras(params);

         doNetworkOperation(Server.CONSULTAR_CREDITOS_CUPONERA, paramTable2, init.getBaseViewController(), true, new ListaCreditos(), Server.isJsonValueCode.CONSUMO);

    }
    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
