package bancomer.api.consumo.gui.delegates;

import android.util.Log;

import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.ListaCupones;

public class ConsultaDetalleDelegateCuponera implements BaseDelegate {

    private InitConsumo init = InitConsumo.getInstance();;
    private BaseViewController baseViewController;
    private static final String CONSTANT_CONSULTA_DETALLE_CUPONERA = "consultaDetalleCuponera";
    private ListaCupones listaCupones;


    public ConsultaDetalleDelegateCuponera(ListaCupones cupones){
        init = InitConsumo.getInstance();
        this.listaCupones = cupones;
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {

    }
    /***----------- Convivencia ---**/
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle,Server.isJsonValueCode isJsonValue) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false, isJson, handle, isJsonValue);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if(operationId == Server.CONSULTA_DETALLE_CUPONERA){
          if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
          }else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
              Log.d("Error: ","No entro a la respuesta");
          }
        }
    }
    @Override
    public void performAction(Object obj) {

        /**varibles de Inicialización */
        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(init.getParentManager().getCurrentViewController());
        baseViewController.setDelegate(this);
        init.setBaseViewController(baseViewController);


        String user = init.getSession().getUsername();
        String numCredito = "86556475587964251756";
        String crCredito = "2898";

        Hashtable<String,String> params =  new Hashtable<String, String>();

        params.put("operacion",CONSTANT_CONSULTA_DETALLE_CUPONERA);
        params.put("numeroCelular",user);
        params.put("numCredito",numCredito);
        params.put("crCredito",crCredito);

        //Aqui es donde lleno el el objeto de la mi modeloListaCuponera
        doNetworkOperation(Server.CONSULTA_DETALLE_CUPONERA,params,baseViewController,true,
                new ListaCupones(), Server.isJsonValueCode.CONSUMO);
    }
    @Override
    public long getDelegateIdentifier() {
        return 0;
    }


    public ListaCupones getListaCupones() {
        return listaCupones;
    }

    public void setListaCupones(ListaCupones listaCupones) {
        this.listaCupones = listaCupones;
    }
}
