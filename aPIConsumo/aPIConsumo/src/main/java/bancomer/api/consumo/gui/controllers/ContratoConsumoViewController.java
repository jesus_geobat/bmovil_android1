package bancomer.api.consumo.gui.controllers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;

import bancomer.api.consumo.R;
import bancomer.api.consumo.gui.delegates.ContratoOfertaConsumoDelegate;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.tracking.TrackingConsumo;
import suitebancomercoms.classes.common.PropertiesManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ContratoConsumoViewController extends BaseActivity implements View.OnClickListener{

    private BaseViewController baseViewController;
    private InitConsumo init = InitConsumo.getInstance();
    private ContratoOfertaConsumoDelegate delegate;
    public BaseViewsController parentManager;

    private AlertDialog mAlertDialog;

    private CheckBox cbConsumo1;
    private CheckBox cbConsumo2;
    private CheckBox cbConsumo3;
    private CheckBox cbConsumo4;
    private CheckBox cbConsumo5;

    private EditText tarjeta;

    private ImageButton btnconfirmar;
    private ImageButton imgaviso_seguro;
    private ImageButton imgaviso;
    private ImageButton imgaviso_token;
    private ImageButton btnImgBack;
    private ImageButton btnImgCallMeBack;

    private ImageView divider;

    private LinearLayout vista;
    private LinearLayout contenedorBotones;
    private LinearLayout layout_contrato_seguros;
    private LinearLayout layout_aviso_seguros;
    private LinearLayout layout_aviso_dom;
    private LinearLayout layout_avisotoken;
    private LinearLayout linearchecKbox1;
    private LinearLayout layout_contrato2;
    private LinearLayout contenedorCampoTarjeta;

    private RadioButton radioButton1;
    private RadioButton radioButton2;

	private TextView lblTextoOferta;
	private TextView contrato1;
	private TextView contrato2;
	private TextView contrato3;
	private TextView contrato4;
	private TextView contrato5;
	private TextView txtaviso_seguros_link;
	private TextView txtaviso_seguros;
	private TextView contrato6;
	private TextView txtseguros1;
	private TextView txtseguros2;
	private TextView txtaviso3;
	private TextView lblmensajecheck4;
    private TextView txtaviso_token;
    private TextView campoTarjeta;
    private TextView instruccionesTarjeta;

	/**todo lo de token **/
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;

	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;

	private EditText contrasena;
	private EditText nip;
	private EditText asm;
	private EditText cvv;

	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;
    /**Termina todo lo de token **/

	private boolean isAseguradora=true;
	private boolean isOpcionesSelec;
	private boolean isBuro=true;
	private boolean isSeguro=true;
    private boolean isBtnLlamar = true;
    private boolean consu1;
    private boolean consu2;
    private boolean consu3;
    private boolean consu4;
    private boolean consu5;
    /*private boolean v1;
    private boolean v2;
    private boolean v3;
    private boolean v4;
    private boolean v5;*/


    /**Boton de callmeBack & back*/
    private int valorStaticoBtnCallmeBack = 0;

    //etiquetado Adobe
    String screen = "";
    String screen2 = "";
    ArrayList<String> list= new ArrayList<String>();
	Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate = (ContratoOfertaConsumoDelegate) parentManager.getBaseDelegateForKey(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_consumo_contrato);
        baseViewController.setTitle(this, R.string.bmovil_promocion_title_consumo, R.drawable.icono_consumo);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);


		vista = (LinearLayout)findViewById(R.id.contratoofertaConsumo_view_controller_layout);
        initView();

        //etiquetado Adobe
        screen = "detalle consumo";
        screen2 = "clausulas consumo";
        list.add(screen);
        list.add(screen2);
        TrackingConsumo.trackState("clausulas consumo", list);
	}

    public void initView(){
        findViews();
        setTextToView();
        configurarPantalla();
        setSecurityComponents();
        clearCheckB();
    }
	
	public void setTextToView(){

        String txtCon2= "Acepto contratar el seguro de mi\npréstamo con: Seguros Bancomer.";
        String colortxtS= "Seguros Bancomer.";
        int con2= txtCon2.indexOf(colortxtS);
        SpannableString cont2 = new SpannableString(txtCon2);
        cont2.setSpan(new ForegroundColorSpan(Color.rgb(0, 158, 229)), con2, txtCon2.length(), 0);
        contrato2.setText(cont2);

        txtseguros1.setText("Seguros Bancomer, con costo de\n"+Tools.formatAmount(delegate.getOfertaConsumo().getImpSegSal(),false)+" por única vez.");

        if(delegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREFORMALIZADO)){

            String contrat5 =contrato5.getText().toString();
            SpannableString content5 = new SpannableString(contrat5);
            content5.setSpan(new UnderlineSpan(), 0, contrat5.length(), 0);
            contrato5.setText(content5);

            lblTextoOferta.setText("Estás a un paso de tu depósito, sólo\nrevisa, autoriza y confirma las\n siguientes condiciones:");
        }

        //validacion buro credito
        if(delegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREFORMALIZADO)||
                delegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREABPROBADO)){

            cbConsumo1.setChecked(false);
            linearchecKbox1.setVisibility(View.VISIBLE);
            cbConsumo1.setClickable(true);
            isBuro=false;
        }else{
            linearchecKbox1.setVisibility(View.GONE);
            isBuro=false;
        }
        //validacion aseguradora
        if(delegate.getOfertaConsumo().getProducto().equals(Constants.IDBOVEDA1)||delegate.getOfertaConsumo().getProducto().equals(Constants.IDBOVEDA2)){
            cbConsumo2.setChecked(true);
            layout_contrato2.setVisibility(View.GONE);
            isAseguradora=false;
            cbConsumo2.setClickable(false);
            isSeguro=false;
        }

        layout_contrato_seguros.setVisibility(View.GONE);
        layout_aviso_dom.setVisibility(View.GONE);
        txtaviso3.setVisibility(View.GONE);
        lblmensajecheck4.setVisibility(View.GONE);

        txtaviso3.setText("El pago se realizará de forma automática (Domiciliación) durante "+delegate.getOfertaConsumo().getTotalPagos()+" pagos los días "+delegate.getOfertaConsumo().getDescDiasPago()+" de cada mes, por un importe de "+Tools.formatAmount(delegate.getOfertaConsumo().getPagoMenFijo(),false)+" con cargo a la cuenta BBVA Bancomer terminación"+Tools.hideAccountNumber(delegate.getOfertaConsumo().getCuentaVinc())+".");

        String importe = Tools.formatAmount(delegate.getOfertaConsumo().getImporte(),false);
        String contrat4="Acepto contratar el préstamo por\n"+importe;
        int cont4= contrat4.indexOf(importe);
        SpannableString conte4 = new SpannableString(contrat4);
        conte4.setSpan(new ForegroundColorSpan(Color.rgb(0, 158, 229)), cont4, contrat4.length(), 0);
        contrato4.setText(conte4);

        String aviso =txtaviso_seguros_link.getText().toString();
        SpannableString contentAviso = new SpannableString(aviso);
        contentAviso.setSpan(new UnderlineSpan(), 0, aviso.length(), 0);
        txtaviso_seguros_link.setText(contentAviso);
        txtaviso_seguros_link.setOnClickListener(this);
    }

	private void configurarPantalla() {

		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());

		gTools.scale(lblTextoOferta, true);
		gTools.scale(contrato1, true);
		gTools.scale(contrato2, true);
		gTools.scale(contrato3, true);
		gTools.scale(contrato4, true);
		gTools.scale(contrato5, true);
		gTools.scale(contrato6, true);
		gTools.scale(txtseguros1, true);
		gTools.scale(txtseguros2, true);
		gTools.scale(txtaviso_seguros, true);
		gTools.scale(imgaviso_seguro);
		gTools.scale(lblmensajecheck4, true);
		gTools.scale(cbConsumo1);
		gTools.scale(txtaviso_seguros_link);
		gTools.scale(cbConsumo2);
		gTools.scale(cbConsumo3);
		gTools.scale(cbConsumo4);
		gTools.scale(cbConsumo5);
		gTools.scale(radioButton1);
		gTools.scale(radioButton2);
		gTools.scale(btnconfirmar);
		gTools.scale(layout_aviso_dom);
		gTools.scale(layout_aviso_seguros);
		gTools.scale(layout_avisotoken);
		gTools.scale(layout_contrato_seguros);
		gTools.scale(linearchecKbox1);
		gTools.scale(txtaviso_token, true);
		gTools.scale(imgaviso_token);
		gTools.scale(imgaviso);
		gTools.scale(txtaviso3,true);
		gTools.scale(findViewById(R.id.confirmacion_campos_layout));
		gTools.scale(findViewById(R.id.txtaviso_aviso_dom));
        gTools.scale(findViewById(R.id.layout_botones));
		
		gTools.scale(contenedorContrasena);
		gTools.scale(contenedorNIP);
		gTools.scale(contenedorASM);
		gTools.scale(contenedorCVV);
		
		gTools.scale(contrasena, true);
		gTools.scale(nip, true);
		gTools.scale(asm, true);
		gTools.scale(cvv, true);
		
		gTools.scale(campoContrasena, true);
		gTools.scale(campoNIP, true);
		gTools.scale(campoASM, true);
        gTools.scale(campoCVV, true);

        gTools.scale(instruccionesContrasena, true);
        gTools.scale(instruccionesNIP, true);
        gTools.scale(instruccionesASM, true);
		gTools.scale(instruccionesCVV, true);

		gTools.scale(contenedorCampoTarjeta);
        gTools.scale(tarjeta, true);
		gTools.scale(campoTarjeta, true);
        gTools.scale(instruccionesTarjeta, true);

        gTools.scale(contenedorBotones);
	}
	
	private void findViews() {

        cbConsumo1              =(CheckBox) findViewById(R.id.cbConsumo1);
        cbConsumo2              =(CheckBox) findViewById(R.id.cbConsumo2);
        cbConsumo3              =(CheckBox) findViewById(R.id.cbConsumo3);
        cbConsumo4              =(CheckBox) findViewById(R.id.cbConsumo4);
        cbConsumo5              =(CheckBox) findViewById(R.id.cbConsumo5);

        imgaviso_token          = (ImageButton) findViewById(R.id.imgaviso_token);
        imgaviso                = (ImageButton) findViewById(R.id.imgaviso);
        imgaviso_seguro         = (ImageButton)findViewById(R.id.imgaviso_seguro);
        btnconfirmar 		    = (ImageButton)findViewById(R.id.confirmacion_confirmar_button);
        btnImgBack              = (ImageButton)findViewById(R.id.consumo_ic_back);

        divider                 = (ImageView) findViewById(R.id.title_divider);

        layout_contrato_seguros = (LinearLayout)findViewById(R.id.layout_contrato_seguros);
        layout_aviso_seguros    = (LinearLayout)findViewById(R.id.layout_aviso_seguros);
        layout_aviso_dom        = (LinearLayout)findViewById(R.id.layout_aviso_dom);
        layout_avisotoken       = (LinearLayout)findViewById(R.id.layout_avisotoken);
        linearchecKbox1         =(LinearLayout)findViewById(R.id.linearchecKbox1);
        layout_contrato2        =(LinearLayout)findViewById(R.id.layout_contrato2);
        contenedorBotones       = (LinearLayout) findViewById(R.id.layout_botones);

        radioButton1            =(RadioButton) findViewById(R.id.radioButton1);
        radioButton2            =(RadioButton) findViewById(R.id.radioButton2);

        lblTextoOferta          = (TextView) findViewById(R.id.lblTextoOferta);
        contrato1               = (TextView) findViewById(R.id.contrato1);
        contrato2               = (TextView) findViewById(R.id.contrato2);
        contrato3               = (TextView) findViewById(R.id.contrato3);
        contrato4               = (TextView) findViewById(R.id.contrato4);
        contrato5               = (TextView) findViewById(R.id.contrato5);
        contrato6               = (TextView) findViewById(R.id.contrato6);
        txtseguros1             = (TextView) findViewById(R.id.txtseguros1);
        txtseguros2             = (TextView) findViewById(R.id.txtseguros2);
        txtaviso_token          = (TextView) findViewById(R.id.txtaviso_token);
        txtaviso3               = (TextView) findViewById(R.id.txtaviso3);
        lblmensajecheck4        = (TextView) findViewById(R.id.lblmensajecheck4);
        txtaviso_seguros_link   = (TextView) findViewById(R.id.txtaviso_seguros_link);
        txtaviso_seguros        = (TextView)findViewById(R.id.txtaviso_seguros);

        contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_confirmacion_contrasena_layout);
		contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_confirmacion_nip_layout);
		contenedorASM 			= (LinearLayout)findViewById(R.id.campo_confirmacion_asm_layout);
		contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_confirmacion_cvv_layout_consumo);
		
		contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
		nip 					= (EditText)contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
		asm						= (EditText)contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
		cvv						= (EditText)contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);
		
		campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
		campoNIP				= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_label);
		campoASM				= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_label);
		campoCVV				= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_label);
	
		instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
		instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
		instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
		instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);
		
		contenedorCampoTarjeta  = (LinearLayout)findViewById(R.id.campo_confirmacion_campotarjeta_layout);
		tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
		campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
		instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);

        contrato5.setOnClickListener(this);
        btnconfirmar.setOnClickListener(this);
        btnImgBack.setOnClickListener(clickListenerBack);

        divider.setVisibility(View.GONE);

        /**Vinculación de boton hacias atras y callmeBack 1 Agosto*/
        if(init.getOfertaDelSimulador())
            btnImgBack.setVisibility(View.GONE);
            btnImgCallMeBack = (ImageButton)findViewById(R.id.consumo_ic_callmeback);
            btnImgCallMeBack.setVisibility(View.GONE);

        if(!init.getOfertaDelSimulador()) { //si nova del simulador, muestra btnllama
            if (obtenerSharedPreferences()) {
                btnImgCallMeBack.setVisibility(View.VISIBLE);
            }
            btnImgCallMeBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /**Operación llamenme por teléfono***/
                    delegate.setDescripcionMensaje("0038");
                    delegate.peticionBtnLlamenme();
                    try {
                        Thread.sleep(250);
                        Log.e("EJECUTANDO 2 ", "OPERACION");
                        init.getBaseViewController().ocultaIndicadorActividad();
                        delegate.peticionLlamame(baseViewController, delegate.getOfertaConsumo().getImporte());
                    } catch (InterruptedException e) {
                    }
                }
            });
        }

        final ScrollView sv = (ScrollView) findViewById(R.id.body_layout);
        sv.postDelayed(new Runnable() {
            @Override
            public void run() {
                sv.smoothScrollTo(0, 0);
            }
        }, 300);
    }

    /**Boton de retroceso a la actividad anterior*/
    OnClickListener clickListenerBack = new OnClickListener() {
        @Override
        public void onClick(View v) {

            borrarSharedPrefences();

            paso1OperacionMap.put("evento_paso2", "event47");
            paso1OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo");
            paso1OperacionMap.put("eVar12", "paso2:rechazo oferta consumo");
            TrackingConsumo.trackPaso1Operacion(paso1OperacionMap);

            if(init.getOfertaDelSimulador())
                delegate.confirmaAtras();
            else
                delegate.confirmarRechazoatras();
        }
    };

    public void onCheckboxClick(View sender){
        if(sender == cbConsumo1){
            if(cbConsumo1.isChecked()){
                cbConsumo1.setClickable(false);
                consu1 = true;
                cbConsumo1.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            }
        }else if(sender==cbConsumo2){
            if(!cbConsumo1.isClickable()){
                layout_contrato_seguros.setVisibility(View.VISIBLE);
                cbConsumo2.setClickable(false);
                consu2 = true;
                cbConsumo2.setBackgroundResource(R.drawable.icn_checkhab_consumo);

            }else{
                if(isBuro)
                    baseViewController.showInformationAlert(this,"Debes aceptar consulta de mi historial crediticio antes.");
                    cbConsumo2.setBackgroundResource(R.drawable.ic_checkoff_consumo);
            }
        }else if(sender==cbConsumo3){
            if(!cbConsumo2.isClickable()&&!cbConsumo1.isClickable()){
                layout_contrato_seguros.setVisibility(View.GONE);
                layout_aviso_dom.setVisibility(View.VISIBLE);
                txtaviso3.setVisibility(View.VISIBLE);
                cbConsumo3.setClickable(false);
                consu3 = true;
                cbConsumo3.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            }else{
                if(isBuro&& isSeguro)
                    baseViewController.showInformationAlert(this,"Debes aceptar contratar el seguro de mi crédito antes.");
                else if(isBuro && !isSeguro)
                    baseViewController.showInformationAlert(this,"Debes aceptar consulta de mi historial crediticio antes.");
                else if(isSeguro&& !isBuro)
                    baseViewController.showInformationAlert(this,"Debes aceptar contratar el seguro de mi crédito antes.");
                cbConsumo3.setBackgroundResource(R.drawable.ic_checkoff_consumo);
            }
        }else if(sender==cbConsumo4){
            if(!cbConsumo3.isClickable()&&!cbConsumo2.isClickable()&&!cbConsumo1.isClickable()){
                layout_aviso_dom.setVisibility(View.GONE);
                txtaviso3.setVisibility(View.GONE);
                if(delegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREFORMALIZADO)){
                   lblmensajecheck4.setVisibility(View.GONE);
                }else if(delegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREABPROBADO)){
                    lblmensajecheck4.setVisibility(View.VISIBLE);
                }
                cbConsumo4.setClickable(false);
                consu4 = true;
                cbConsumo4.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            }else{
                baseViewController.showInformationAlert(this,"Para continuar, selecciona la opción de Pago automático de tu crédito. Si no requieres el pago automático, puedes acudir a Sucursal para modificar las condiciones del préstamo..");
                cbConsumo4.setBackgroundResource(R.drawable.ic_checkoff_consumo);
            }

        }else if(sender==cbConsumo5){
            if(!cbConsumo4.isClickable() && !cbConsumo3.isClickable()&&!cbConsumo2.isClickable()&&!cbConsumo1.isClickable()){
                cbConsumo5.setClickable(false);
                consu5   = true;
                isOpcionesSelec=true;
                lblmensajecheck4.setVisibility(View.GONE);
                cbConsumo5.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            }else{
                baseViewController.showInformationAlert(this,"Debes aceptar contratar el crédito antes.");
                cbConsumo5.setBackgroundResource(R.drawable.ic_checkoff_consumo);
            }
        }else if(sender==radioButton1){
            if(radioButton1.isChecked()){
                radioButton2.setChecked(false);
            }
        }else if(sender==radioButton2){
            if(radioButton2.isChecked()){
                radioButton1.setChecked(false);
            }
        }
    }


    private TextWatcher listener = new TextWatcher() {

        public void afterTextChanged(Editable s) {
            ContratoConsumoViewController.this.onUserInteraction();
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        public void onTextChanged(CharSequence s, int start, int before, int count) {}
    };

    private void setSecurityComponents(){

        mostrarContrasena(delegate.mostrarContrasenia());
        mostrarNIP(delegate.mostrarNIP());
        mostrarASM(delegate.tokenAMostrar());
        mostrarCVV(delegate.mostrarCVV());
        mostrarCampoTarjeta(delegate.mostrarCampoTarjeta());

        LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);

        if (contenedorContrasena.getVisibility() == View.GONE &&
                contenedorNIP.getVisibility() == View.GONE &&
                contenedorASM.getVisibility() == View.GONE &&
                contenedorCVV.getVisibility() == View.GONE &&
                contenedorCampoTarjeta.getVisibility() == View.GONE) {

            contenedorPadre.setBackgroundColor(0);
            contenedorPadre.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        }

        contrasena.addTextChangedListener(listener);
        nip.addTextChangedListener(listener);
        asm.addTextChangedListener(listener);
        cvv.addTextChangedListener(listener);
        tarjeta.addTextChangedListener(listener);
    }

    public String pideContrasena() {
        if (contrasena.getVisibility() == View.GONE) {
            return "";
        } else {
            return contrasena.getText().toString();
        }
    }

    public String pideNIP() {
        if (nip.getVisibility() == View.GONE) {
            return "";
        } else {
            return nip.getText().toString();
        }
    }

    public String pideASM() {
        if (asm.getVisibility() == View.GONE) {
            return "";
        } else {
            return asm.getText().toString();
        }
    }

    public String pideCVV() {
        if (cvv.getVisibility() == View.GONE) {
            return "";
        } else {
            return cvv.getText().toString();
        }
    }

    public String pideTarjeta(){
        if (tarjeta.getVisibility() == View.GONE) {
            return "";
        }else
            return tarjeta.getText().toString();
    }

	public void mostrarContrasena(boolean visibility){
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE: View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE: View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE: View.GONE);
		if (visibility) {
            campoContrasena.setText(delegate.getEtiquetaCampoContrasenia());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}

	public void mostrarNIP(boolean visibility){
		contenedorNIP.setVisibility(visibility ? View.VISIBLE: View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE: View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
			campoNIP.setText(delegate.getEtiquetaCampoNip());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			String instrucciones = delegate.getTextoAyudaNIP();
			if (instrucciones.equals("")) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
            }
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}

	public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){
		switch(tipoOTP) {
			case ninguno:
				contenedorASM.setVisibility(View.GONE);
                campoASM.setVisibility(View.VISIBLE);
				asm.setVisibility(View.GONE);
				asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
				break;
			case codigo:
			case registro:
				contenedorASM.setVisibility(View.VISIBLE);
				campoASM.setVisibility(View.VISIBLE);
				asm.setVisibility(View.VISIBLE);
				InputFilter[] userFilterArray = new InputFilter[1];
				userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
				asm.setFilters(userFilterArray);
				cambiarAccionTexto(contrasena);
				cambiarAccionTexto(tarjeta);
				cambiarAccionTexto(nip);
				asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
		}
		
		Constants.TipoInstrumento tipoInstrumento = delegate.consultaTipoInstrumentoSeguridad();

		switch (tipoInstrumento) {
			case OCRA:
				campoASM.setText(delegate.getEtiquetaCampoOCRA());
				asm.setTransformationMethod(null);
				break;
			case DP270:
				campoASM.setText(delegate.getEtiquetaCampoDP270());
				asm.setTransformationMethod(null);
				break;
			case SoftToken:
				if(init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
					asm.setText(Constants.DUMMY_OTP);
					asm.setEnabled(false);
					campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
				} else {
					asm.setText("");
					asm.setEnabled(true);
					campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
					asm.setTransformationMethod(null);
				}

				break;
			default:
				break;
		}
		String instrucciones = delegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento,(init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()),delegate.tokenAMostrar());
		if (instrucciones.equals("")) {
			instruccionesASM.setVisibility(View.VISIBLE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}	
	}

	private void mostrarCampoTarjeta(boolean visibility) {
		contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE: View.GONE);
		campoTarjeta.setVisibility(visibility ? View.VISIBLE: View.GONE);
		tarjeta.setVisibility(visibility ? View.VISIBLE: View.GONE);
		if (visibility) {
			campoTarjeta.setText(delegate.getEtiquetaCampoTarjeta());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(5);
			tarjeta.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
			String instrucciones = delegate.getTextoAyudaTarjeta();
			if (instrucciones.equals("")) {
				instruccionesTarjeta.setVisibility(View.GONE);
			} else {
				instruccionesTarjeta.setVisibility(View.VISIBLE);
				instruccionesTarjeta.setText(instrucciones);
			}
		} else {
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		
	}

	private void cambiarAccionTexto(EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}

	public void mostrarCVV(boolean visibility) {
        contenedorCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE : View.GONE);
        instruccionesCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
		
		if (visibility) {
			campoCVV.setText(delegate.getEtiquetaCampoCVV());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			String instrucciones = delegate.getTextoAyudaCVV();
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	public void limpiarCampos(){
		contrasena.setText("");
		nip.setText("");
		asm.setText("");
		cvv.setText("");
		tarjeta.setText("");
	}

    private void clearCheckB(){
        borrarSharedPrefences();
        cbConsumo1.setBackground(getResources().getDrawable(R.drawable.ic_checkoff_consumo));
        cbConsumo2.setBackground(getResources().getDrawable(R.drawable.ic_checkoff_consumo));
        cbConsumo3.setBackground(getResources().getDrawable(R.drawable.ic_checkoff_consumo));
        cbConsumo4.setBackground(getResources().getDrawable(R.drawable.ic_checkoff_consumo));
        cbConsumo5.setBackground(getResources().getDrawable(R.drawable.ic_checkoff_consumo));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            borrarSharedPrefences();

            paso1OperacionMap.put("evento_paso2", "event47");
            paso1OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo");
            paso1OperacionMap.put("eVar12", "paso2:rechazo oferta consumo");

            TrackingConsumo.trackPaso1Operacion(paso1OperacionMap);
            if(!init.getOfertaDelSimulador()) { //si viene de oneClick
                delegate.confirmarRechazoatras();
            }else{
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ContratoConsumoViewController.this);
                alertDialog.setTitle("Aviso");
                alertDialog.setMessage("¿Quieres abandonar el proceso de contratación de tu crédito?");
                alertDialog.setIcon(R.drawable.anicalertaaviso);

                alertDialog.setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        delegate.setDescripcionMensaje("");
                        delegate.setOpFlujo(1);
                        //delegate.realizaOperacion(Server.RECHAZO_OFERTA_CONSUMO, 0);
                        clearCheckB();
                        init.getParentManager().setActivityChanging(true);
                        ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                        init.resetInstance();
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
            borrarSharedPrefences();
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        guardarSharedPreferenceCheck(consu1, "CONSU1");
        guardarSharedPreferenceCheck(consu2, "CONSU2");
        guardarSharedPreferenceCheck(consu3, "CONSU3");
        guardarSharedPreferenceCheck(consu4, "CONSU4");
        guardarSharedPreferenceCheck(consu5, "CONSU5");

        /*if(!init.getParentManager().isActivityChanging()) {
            //TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }*/

    }

    @Override
    public void onUserInteraction() {
      //  super.onUserInteraction();
       // init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    private void validaChecks(){

        if(obtenerSharedPreferencesCheck("CONSU1") == true){
            cbConsumo1.setClickable(false);
            cbConsumo1.setBackgroundResource(R.drawable.icn_checkhab_consumo);
        }else{
            cbConsumo1.setBackgroundResource(R.drawable.ic_checkoff_consumo);
        }
        if(obtenerSharedPreferencesCheck("CONSU2")== true){
            cbConsumo2.setClickable(false);
            cbConsumo2.setBackgroundResource(R.drawable.icn_checkhab_consumo);
        }else{
            cbConsumo2.setBackgroundResource(R.drawable.ic_checkoff_consumo);
        }
        if(obtenerSharedPreferencesCheck("CONSU3")== true){
            cbConsumo3.setClickable(false);
            cbConsumo3.setBackgroundResource(R.drawable.icn_checkhab_consumo);
        }else{
            cbConsumo3.setBackgroundResource(R.drawable.ic_checkoff_consumo);
        }
        if(obtenerSharedPreferencesCheck("CONSU4")== true){
            cbConsumo4.setClickable(false);
            cbConsumo4.setBackgroundResource(R.drawable.icn_checkhab_consumo);
        }else{
            cbConsumo4.setBackgroundResource(R.drawable.ic_checkoff_consumo);
        }
        if(obtenerSharedPreferencesCheck("CONSU5")== true){
            cbConsumo5.setClickable(false);
            cbConsumo5.setBackgroundResource(R.drawable.icn_checkhab_consumo);
        }else{
            cbConsumo5.setBackgroundResource(R.drawable.ic_checkoff_consumo);
        }

        if(obtenerSharedPreferencesCheck("CONSU5") == true){

            cbConsumo1.setClickable(false);
            cbConsumo1.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            cbConsumo2.setClickable(false);
            cbConsumo2.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            cbConsumo3.setClickable(false);
            cbConsumo3.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            cbConsumo4.setClickable(false);
            cbConsumo4.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            cbConsumo5.setClickable(false);
            cbConsumo5.setBackgroundResource(R.drawable.icn_checkhab_consumo);

            cbConsumo1.isChecked();
            cbConsumo2.isChecked();
            cbConsumo3.isChecked();
            cbConsumo4.isChecked();
            cbConsumo5.isChecked();

            isOpcionesSelec=true;

        } else if(obtenerSharedPreferencesCheck("CONSU4") == true){
            cbConsumo1.setClickable(false);
            cbConsumo1.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            cbConsumo2.setClickable(false);
            cbConsumo2.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            cbConsumo3.setClickable(false);
            cbConsumo3.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            cbConsumo4.setClickable(false);
            cbConsumo4.setBackgroundResource(R.drawable.icn_checkhab_consumo);

            cbConsumo1.isChecked();
            cbConsumo2.isChecked();
            cbConsumo3.isChecked();
            cbConsumo4.isChecked();

        }  else if(obtenerSharedPreferencesCheck("CONSU3") == true){

            cbConsumo1.setClickable(false);
            cbConsumo1.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            cbConsumo2.setClickable(false);
            cbConsumo2.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            cbConsumo3.setClickable(false);
            cbConsumo3.setBackgroundResource(R.drawable.icn_checkhab_consumo);

            cbConsumo1.isChecked();
            cbConsumo2.isChecked();
            cbConsumo3.isChecked();

        }else if(obtenerSharedPreferencesCheck("CONSU2") == true){

            cbConsumo1.setClickable(false);
            cbConsumo1.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            cbConsumo2.setClickable(false);
            cbConsumo2.setBackgroundResource(R.drawable.icn_checkhab_consumo);

            cbConsumo1.isChecked();
            cbConsumo2.isChecked();

        }else if(obtenerSharedPreferencesCheck("CONSU2") == true){
            cbConsumo1.setClickable(false);
            cbConsumo1.setBackgroundResource(R.drawable.icn_checkhab_consumo);
            cbConsumo1.isChecked();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);

        validaChecks();
    }

    //Metodo para sacar el valor de shared preference
    public boolean obtenerSharedPreferences(){

        String preferencia = "MIS_PREFRENCIAS";
        int modoPrivacidad = Context.MODE_PRIVATE;
        SharedPreferences preferences = getSharedPreferences(preferencia, modoPrivacidad);
        boolean valorObtenido =  preferences.getBoolean("keyButtoncall",false);
        return  valorObtenido;
    }

    /***:::::::::::::: Dialog Error:::::::::::::::::::::: **/
    public void showErrorMessage(String errorMessage, DialogInterface.OnClickListener listener) {

        if (errorMessage.length() > 0) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.label_error);
            alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
            alertDialogBuilder.setMessage(errorMessage);
            alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.show();
        }
    }

    public void borrarSharedPrefences() {

        Log.d("Borrando", "VARIABLE BUTTON CALLMEBACK");
        String preferencia = "MIS_PREFRENCIAS";
        int modoPrivacidad = Context.MODE_PRIVATE;
        SharedPreferences preferences = getSharedPreferences(preferencia, modoPrivacidad);
        preferences.edit().clear().commit();
    }

    public void guardarSharedPreferenceCheck(boolean variableBoolean,String clave) {
        String preferencia = "MIS_PREFRENCIAS";
        int modoPrivacidad = Context.MODE_PRIVATE;
        SharedPreferences.Editor editor = getSharedPreferences(preferencia, modoPrivacidad).edit();
        editor.putBoolean(clave, variableBoolean);
        editor.commit();
    }

    //Metodo para sacar el valor de shared preference
    public boolean obtenerSharedPreferencesCheck(String clave) {
        String preferencia = "MIS_PREFRENCIAS";
        int modoPrivacidad = Context.MODE_PRIVATE;
        SharedPreferences preferences = getSharedPreferences(preferencia, modoPrivacidad);
        boolean valorObtenido = preferences.getBoolean(clave, false);
        return valorObtenido;
    }

    @Override
    public void onClick(View v) {
        if(v==btnconfirmar){
            paso1OperacionMap.put("evento_paso2", "event47");
            paso1OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo");
            paso1OperacionMap.put("eVar12", "paso2:acepta oferta consumo");

            TrackingConsumo.trackPaso1Operacion(paso1OperacionMap);

            if(isOpcionesSelec){
                if(delegate.enviaPeticionOperacion()){
                    init.getParentManager().setActivityChanging(true);
                }
            }else{
                baseViewController.showInformationAlert(ContratoConsumoViewController.this,"Es necesario autorizar todas las condiciones para continuar.");
            }
        }
        if(v==txtaviso_seguros_link){
            paso1OperacionMap.put("evento_paso2", "event47");
            paso1OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo");
            paso1OperacionMap.put("eVar12", "paso2:caracteristicas poliza");

            TrackingConsumo.trackPaso1Operacion(paso1OperacionMap);
            init.getParentManager().setActivityChanging(true);
            delegate.showPoliza();
        }
        if(v==contrato5){
            paso1OperacionMap.put("evento_paso2", "event47");
            paso1OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo");
            paso1OperacionMap.put("eVar12", "paso2:terminos oferta consumo");

            TrackingConsumo.trackPaso1Operacion(paso1OperacionMap);
            init.getParentManager().setActivityChanging(true);
            delegate.realizaOperacion(Server.TERMINOS_OFERTA_CONSUMO, 0);
        }
        if(v == btnImgBack){
            delegate.confirmarRechazoatras();
        }
    }

    public void finaliceOTP(){
        if(isAseguradora){
            if(radioButton1.isChecked()){
                init.getParentManager().setActivityChanging(true);
                delegate.realizaOperacion(Server.EXITO_OFERTA_CONSUMO, 1);
            }else{
                init.getParentManager().setActivityChanging(true);
                delegate.realizaOperacion(Server.EXITO_OFERTA_CONSUMO, 2);
            }
        }else{
            init.getParentManager().setActivityChanging(true);
            delegate.realizaOperacion(Server.EXITO_OFERTA_CONSUMO, 3);
        }
    }
}
