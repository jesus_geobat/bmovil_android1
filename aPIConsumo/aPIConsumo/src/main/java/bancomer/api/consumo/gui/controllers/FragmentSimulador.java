package bancomer.api.consumo.gui.controllers;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
/**
 * Created by isaigarciamoso on 26/07/16.
 */


@TargetApi(Build.VERSION_CODES.HONEYCOMB)

public class FragmentSimulador extends Fragment {
     /**  onCreateView: Se llama cuando el
     * fragmento será dibujado por primera vez en la interfaz de usuario.
     * En este método crearemos el view que representa al fragmento para retornarlo hacia la actividad
     **/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    /**
     * onCreate: Este método es llamado cuando el fragmento se está creando. En el puedes inicializar
     * todos los componentes que deseas guardar si el fragmento fue pausado o detenido.
     **/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
