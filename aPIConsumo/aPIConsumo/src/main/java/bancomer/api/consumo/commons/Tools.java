package bancomer.api.consumo.commons;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

/**
 * Created by isaigarciamoso on 29/07/16.
 */
public class Tools {

    public final static int TAM_FRAGMENTO_TEXTO = 1000;


    public static String validateMont(String max, String min, String mont, Context context){
        int longitud;
        String value = "";

        max = max.replace(",","");
        min = min.replace(",","");
        mont = mont.replace(",","");

        longitud = max.length();
        max = max.substring(0, longitud - 3);

        longitud = min.length();
        min = min.substring(0, longitud - 3);

        longitud = mont.length();

        if(mont.startsWith("$"))
            mont = mont.substring(1, longitud -3);
        else
            mont = mont.substring(0, longitud - 3);

        try {
            Log.i("monto","mont: "+mont);
            if(Integer.parseInt(mont) > Integer.parseInt(max)) //si es mayor
                value = ConstantsCredit.MONTO_MAYOR;
            else if (Integer.parseInt(mont)< Integer.parseInt(min))
                value = ConstantsCredit.MONTO_MENOR;
            else
                value = ConstantsCredit.MONTO_EN_RANGO;

        }catch (Exception ex){
            ex.printStackTrace();
        }

        return value;
    }

    public static void showAlert(String mensaje, Context context){
        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage(mensaje)
                .setPositiveButton("Seleccionar", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public static String validateValueMonto(String monto){
        String auxMonto = monto;

        if(auxMonto.contains(",")){
            auxMonto = auxMonto.replace(",","");
            Log.i("monto","auxMonto: "+auxMonto);
        }

        if(auxMonto.endsWith(".00")) {
            auxMonto = auxMonto.replace(".00", "");
            Log.i("monto", "auxMonto2: " + auxMonto);
        }

        if(auxMonto.contains(".")){
            auxMonto = auxMonto.replace(".","");
        }

        return auxMonto;
    }

    public static String formatAmount(String amount, boolean negative) {
        amount = amount + "00";
        StringBuffer result = new StringBuffer();
        if (amount != null) {
            result.append('$');
            if (negative) {
                if(amount.startsWith("-"))
                    amount = amount.substring(1);
                result.append('-');
            }
            int size = amount.length();
            switch (size) {
                case 0:
                    result.append("0.00");
                    break;
                case 1:
                    result.append("0.0");
                    result.append(amount);
                    break;
                case 2:
                    result.append("0.");
                    result.append(amount);
                    break;
                default:
                    String intPart = amount.substring(0, size - 2);
                    String decPart = amount.substring(size - 2);
                    result.append(formatPositiveIntegerAmount(intPart));
                    result.append('.');
                    result.append(decPart);
                    break;
            }
        }
        return result.toString();
    }

    private static String formatPositiveIntegerAmount(String amount) {
        StringBuffer result = new StringBuffer();
        if (amount != null) {
            int size = amount.length();
            int remaining = size % 3;
            if (remaining == 0) {
                remaining = 3;
            }
            int start = 0;
            for (int end = remaining; end <= size; end += 3) {
                result.append(amount.substring(start, end));
                if (end < size) {
                    result.append(',');
                }
                start = end;
            }
        }
        return result.toString();
    }
}
