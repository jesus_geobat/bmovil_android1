package bancomer.api.consumo.gui.controllers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consumo.R;
import bancomer.api.consumo.gui.delegates.ExitoCuponeraDelegate;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.Server;

/**
 * Created by omar on 21/09/16.
 */
public class ExitoCuponeraViewController extends BaseActivity {


    private ImageButton splash;
    private InitConsumo init = InitConsumo.getInstance();
    private BaseViewController baseViewController;
    private BaseViewsController parentManager;
    private ExitoCuponeraDelegate delegate;
    private AlertDialog mAlertDialog;
    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private TextView textViewFirstMessage;
    private EditText editTextMail;
    private Button buttonCuponeraMenu;
    private Button buttonCuponeraEnviar;
    private TextView tittleHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate = (ExitoCuponeraDelegate) parentManager.getBaseDelegateForKey(
                ExitoCuponeraDelegate.KEY_UNIQUE_DELEGATE_EXITO_CUPONERA);
        delegate.setController(this);
        baseViewController.setActivity(this);

        baseViewController.onCreate(this, baseViewController.SHOW_HEADER, R.layout.cuponera_exito);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        getUIElements();
        customizeView();
        setListeners();
    }
    private void getUIElements()
    {
        textViewFirstMessage = (TextView) findViewById(R.id.text_view_first_message);
        editTextMail = (EditText) findViewById(R.id.edit_text_mail);
        buttonCuponeraMenu = (Button) findViewById(R.id.button_cuponera_menu);
        buttonCuponeraEnviar = (Button) findViewById(R.id.button_cuponera_enviar);
        splash = (ImageButton) findViewById(R.id.splashImagen);
    }

    private void customizeView()
    {
        //Hide Top Elements.
        findViewById(R.id.consumo_ic_back).setVisibility(View.GONE);
        findViewById(R.id.cuponera_icon).setVisibility(View.GONE);
        findViewById(R.id.cantidad_cupones).setVisibility(View.GONE);

        tittleHeader = (TextView) findViewById(R.id.cuponera_tittle);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/tahoma.ttf");
        tittleHeader.setTypeface(typeface);

        splash.setVisibility(View.GONE);


        String messageA= getString(R.string.exito_cuponera_first_message_a);
        Spannable spannableWord = new SpannableString(getString(R.string.exito_cuponera_first_message_b));
        spannableWord.setSpan(new ForegroundColorSpan(Color.rgb(1, 87, 155)), 0, messageA.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textViewFirstMessage.setText(spannableWord);
        textViewFirstMessage.setTypeface(typeface);


        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(tittleHeader, true);

        validacionInicialCorreo();
    }

    private void setListeners()
    {
        buttonCuponeraEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mail = editTextMail.getText().toString().trim();
                if (mail.equals("") || mail.equals(" ")) {
                    showErrorMessage(getString(R.string.exito_cuponera_non_mail_alert));
                } else {
                    if (validateEmail(mail)) {
                        hideKeyboard();
                        editTextMail.clearFocus();
                        delegate.realizaOperacion(Server.ENVIA_SMSYMESSAGE_CUPONERA, mail);

                    } else {
                        showErrorMessage(getString(R.string.exito_cuponera_invalid_mail_alert));
                    }
                }
            }
        });

        buttonCuponeraMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init.getParentManager().setActivityChanging(true);
                delegate.showMenu();
            }
        });
    }

    public void showErrorMessage(String errorMessage) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.label_error);
        alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
        alertDialogBuilder.setMessage(errorMessage);
        alertDialogBuilder.setPositiveButton(R.string.label_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAlertDialog.dismiss();
            }
        });
        alertDialogBuilder.setCancelable(false);
        mAlertDialog = alertDialogBuilder.show();
    }

    private void validacionInicialCorreo(){
        ICommonSession session = init.getSession();
        if (session.getEmail() != null) {
            String emailSession = session.getEmail();
            editTextMail.setText(emailSession);
        } else {
            editTextMail.setText("");
        }
    }

    private boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);
    }
    @Override
    protected void onPause() {
        super.onPause();
        /*if (!init.getParentManager().isActivityChanging()) {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }*/
    }
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }
    /**Getter & Setter**/
    public ImageButton getSplash() {
        return splash;
    }

    public void setSplash(ImageButton splash) {
        this.splash = splash;
    }

    private void hideKeyboard()
    {
        if(getCurrentFocus() != null)
        {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editTextMail.getWindowToken(), 0);
        }
    }
}




