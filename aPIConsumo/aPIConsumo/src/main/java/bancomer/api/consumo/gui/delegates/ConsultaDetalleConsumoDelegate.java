package bancomer.api.consumo.gui.delegates;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.service.textservice.SpellCheckerService;
import android.util.Log;

import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consumo.implementations.InitConsumo;
import bancomer.api.consumo.io.AuxConectionFactory;
import bancomer.api.consumo.io.Server;
import bancomer.api.consumo.models.OfertaConsumo;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;

public class ConsultaDetalleConsumoDelegate implements BaseDelegate {

    private InitConsumo init = InitConsumo.getInstance();
    private BaseViewController baseViewController;
    private String valor;


    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false);

    }
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, Boolean isJson, Object handle,Server.isJsonValueCode isJsonValue) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false, isJson, handle, isJsonValue);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        Log.v("Analyze", "AnalyzeResponseConsultaDetalle");

        if (response.getStatus() == ServerResponse.OPERATION_ERROR){
            init.getParentManager().resetRootViewController();
            init.updateHostActivityChangingState();

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
            init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(),response.getMessageText(),listener);
            init.resetInstance();
        }
        else if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
            OfertaConsumo ofertaConsumo = (OfertaConsumo)response.getResponse();
           // System.out.print("Respuesta: --------------------------------------->"+ofertaConsumo.getTasaRevire());
            System.out.println("STATUS DETALLE " + response.getStatus());
            init.showDetalleConsumo(ofertaConsumo, init.getOfertaConsumo(), false);
        }

    }
  /**Consulto el detalle de la oferta*/
    @Override
    public void performAction(Object obj) {

        Log.d("mensaje de isai","AQUI SE TRANFORMA LA URL");
        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(init.getParentManager().getCurrentViewController());
        baseViewController.setDelegate(this);
        init.setBaseViewController(baseViewController);

        String user = init.getSession().getUsername();
        String ium  = init.getSession().getIum();
        String cveCamp = init.getOfertaConsumo().getCveCamp();

        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put("numeroCelular",user );
        params.put("claveCamp",cveCamp);
        params.put("IUM", ium);
        params.put("importePar", "000");
        Hashtable<String, String> paramTable2  = AuxConectionFactory.consultaDetalleConsumo(params);

        doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA_CONSUMO, paramTable2, InitConsumo.getInstance().getBaseViewController(), true, new OfertaConsumo(),
                Server.isJsonValueCode.CONSUMO);
    }
    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
