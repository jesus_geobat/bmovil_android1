package bancomer.api.consumo.models;

import android.util.Log;

import java.io.IOException;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;


public class ConsultaPoliza implements ParsingHandler {
	/**
	 * Terminos de uso en formato HTML.
	 */
	private String terminosHtml;
	
	/**
	 * @return Terminos de uso en formato HTML.
	 */
	public String getTerminosHtml() {
		return terminosHtml;
	}

	/**
	 * @param terminosHtml Terminos de uso en formato HTML.
	 */
	public void setTerminosHtml(String terminosHtml) {
		this.terminosHtml = terminosHtml;
	}

	public ConsultaPoliza() {
//		terminosHtml = null;
		super();
	}


	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		Log.w("anom","parseando procesJSON");
		terminosHtml = parser.parseNextValue("txtHTML1").replace("\u0093", "\"").replace("\u0094", "\"");
		Log.w("anom","terminosHTml: "+terminosHtml);
		setTerminosHtml(terminosHtml);
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		Log.w("anom","parseando proces");

	}
}
