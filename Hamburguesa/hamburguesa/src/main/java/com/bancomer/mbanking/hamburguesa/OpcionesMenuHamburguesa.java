package com.bancomer.mbanking.hamburguesa;

/**
 * @author eluna
 * @version 1.0
 * IDS Comercial S.A. de C.V
 */
import java.io.Serializable;

public class OpcionesMenuHamburguesa implements Serializable
{
	private long id;
	private static final long serialVersionUID = 1L;
	private String nombreOpcion;
	private String nombreApi;
	private boolean visible;
	private boolean session;
	
	public OpcionesMenuHamburguesa(){
		this.nombreOpcion = "";
		this.nombreApi = "";
		this.visible = false;
		this.session = false;
	}
	
	public String getNombreOpcion() {
		return nombreOpcion;
	}
	public void setNombreOpcion(final String nombreOpcion) {
		this.nombreOpcion = nombreOpcion;
	}
	public String getNombreApi() {
		return nombreApi;
	}
	public void setNombreApi(final String nombreApi) {
		this.nombreApi = nombreApi;
	}
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(final boolean visible) {
		this.visible = visible;
	}
	public boolean isSession() {
		return session;
	}
	public void setSession(final boolean session) {
		this.session = session;
	}

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public OpcionesMenuHamburguesa(final String nombreOpcion,final String nombreApi){
		this.nombreOpcion = nombreOpcion;
		this.nombreApi = nombreApi;
	}

}
