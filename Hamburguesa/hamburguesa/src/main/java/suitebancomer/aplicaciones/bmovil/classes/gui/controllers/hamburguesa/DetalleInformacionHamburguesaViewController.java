package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import android.app.Activity;
import android.widget.ListView;
import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.hamburguesa.MenuHamburguesaData;
import com.bancomer.mbanking.hamburguesa.OpcionesMenuHamburguesa;
import com.google.gson.*;
import java.util.*;
import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.ConstanstMenuOpcionesHamburguesa;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.isNull;

/**
 * Created by eluna on 22/10/2015.
 */
public final class DetalleInformacionHamburguesaViewController {

    private DetalleInformacionHamburguesaViewController(){}

    public static boolean isDifNull(final Object value) {
        return value != null;
    }

    public static boolean isDiff(final Object value) {
        return value != Boolean.TRUE;
    }

    public static MenuHamburguesaData procesarCatalogo(final String catalogoHamburguesa) {
        final String catalogoHamburguesaCorrecto = catalogoHamburguesa.replace("session ", "session");
        final JsonObject json = (JsonObject) new JsonParser().parse(catalogoHamburguesaCorrecto).getAsJsonObject();
        final JsonArray array = (JsonArray) json.get(ConstanstMenuOpcionesHamburguesa.OPCIONES);
        final Gson gson = new Gson();

        final ArrayList<OpcionesMenuHamburguesa> opciones = new ArrayList<OpcionesMenuHamburguesa>();
        OpcionesMenuHamburguesa opcHamburguesa;
        final Iterator<JsonElement> iterator = array.iterator();

        while (iterator.hasNext()) {
            opcHamburguesa = gson.fromJson((JsonObject) iterator.next(), OpcionesMenuHamburguesa.class);
            opciones.add(opcHamburguesa);
        }
        final MenuHamburguesaData data = new MenuHamburguesaData();
        data.setEstado(json.get(ConstanstMenuOpcionesHamburguesa.ESTADO)
                .getAsString());
        data.setNombreApp(json.get(ConstanstMenuOpcionesHamburguesa.NOMBRE_APP)
                .getAsString());
        data.setVersionMenuHam(json.get(
                ConstanstMenuOpcionesHamburguesa.VERSION_MENU_HAM)
                .getAsString());
        data.setOpciones(opciones);
        return data;
    }

    public static void mostrarOpcionesConSession(final OpcionesMenuHamburguesa opciones,final List<OpcionesMenuHamburguesa> elementosMenu)
    {
        if (opciones.isVisible())
        {
            if(opciones.getNombreOpcion().equals(ConstanstMenuOpcionesHamburguesa.ADMINISTRAR_ALERTAS))
            {
                if (Tools.determinarPerfil(Session.getInstance(SuiteApp.appContext).getClientProfile()).equals("MF03")) {
                    elementosMenu.add(new OpcionesMenuHamburguesa(opciones.getNombreOpcion(), opciones.getNombreApi()));
                }
            }
            else{
                elementosMenu.add( new OpcionesMenuHamburguesa(opciones.getNombreOpcion(), opciones.getNombreApi()));
            }
        }
    }

    public static void mostrarOpcionesSinSession(final OpcionesMenuHamburguesa opciones,final List<OpcionesMenuHamburguesa> elementosMenu)
    {
        if (!opciones.isSession() && opciones.isVisible())
        {
            elementosMenu.add( new OpcionesMenuHamburguesa(opciones.getNombreOpcion(), opciones.getNombreApi()));
        }
    }

    public static void agregaOpciones(final List<OpcionesMenuHamburguesa> arreglo,final ListView listaOpciones,final Activity context) {
        AdapterOpcionesMenu adapter = null;
        if (MenuHamburguesaViewsControllers.getBanderaSession().equals(ConstanstMenuOpcionesHamburguesa.SIN_SESSION)){
            arreglo.add(new OpcionesMenuHamburguesa("Novedades", "bmovil"));
        }
        if (MenuHamburguesaViewsControllers.getBanderaSession().equals(ConstanstMenuOpcionesHamburguesa.CON_SESSION)){
            arreglo.add(new OpcionesMenuHamburguesa("Salir", "bmovil"));
        }
        adapter = new AdapterOpcionesMenu(context, arreglo);
        listaOpciones.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    /**
     * @return the datosBmovil
     */
    public static DatosBmovilFileManager getDatosBmovil() {
        if (isNull(MenuHamburguesaViewsControllers.getDatosBmovil())){
            MenuHamburguesaViewsControllers.setDatosBmovil(DatosBmovilFileManager.getCurrent());
            if(isNull(MenuHamburguesaViewsControllers.getDatosBmovil().getVersionCatalogoHamburguesa())){
                MenuHamburguesaViewsControllers.getDatosBmovil().setVersionCatalogoHamburguesa("0");
            }
        }
        return MenuHamburguesaViewsControllers.getDatosBmovil();
    }

    /**
     * @return the paramTable
     */
    public static Map<Object, Object> getParamTable() {
        if (isNull(MenuHamburguesaViewsControllers.getParamTable())) {
            MenuHamburguesaViewsControllers.setParamTable(new Hashtable<Object, Object>());
        }
        return MenuHamburguesaViewsControllers.getParamTable();
    }

    /**
     * Metodo que revisa si la respuesta del server fue correcta
     *
     * @return Regresa TRUE si hubo algun error en la peticion del server
     */
    public static Boolean checkRespuesta(final DatosBmovilFileManager datosBmovil,final MenuHamburguesaData responseObject) {
        if (isNull(responseObject) || isNull(responseObject.getVersionMenuHam())
                && isNull(datosBmovil.getCatalogoHamburguesa())) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * Metodo que valida la respuesta del server fue incorrecta y existe el
     * catalogo precargado o bien si la respuesta del server fue correcta
     *
     * @return Regresa TRUE si esta el server respondio OK o si ya se tiene el
     *         catalogo precargado
     */
    public static Boolean checkCatalogo(final DatosBmovilFileManager datosBmovil,final MenuHamburguesaData responseObject) {
        if (isNull(responseObject.getEstado()) && !isNull(datosBmovil.getCatalogoHamburguesa())
                || ApiConstants.OK.equals(responseObject.getEstado())) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}