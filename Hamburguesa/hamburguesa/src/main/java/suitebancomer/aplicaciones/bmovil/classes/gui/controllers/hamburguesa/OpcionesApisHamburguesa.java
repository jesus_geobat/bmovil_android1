package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import android.content.Context;
import android.content.Intent;
import com.bancomer.mbanking.apipush.MainActivity;
import com.bancomer.mbanking.compartirvalorarapi.InitValorarCompartir;

import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.ConstanstMenuOpcionesHamburguesa;

/**
 * Created by eluna on 12/01/2016.
 */
public final class OpcionesApisHamburguesa {

    private OpcionesApisHamburguesa(){}

    public static void showAcercaDe(final Context context){
        final Intent intentAcercaDe = new Intent(context,AcercaDeViewController.class);
        context.startActivity(intentAcercaDe);
    }

    public static void showNotificaciones(final Context context) {
        final Intent intent = new Intent(context,MainActivity.class);
        context.startActivity(intent);
    }

    public static void showCompartir(final Context context,final String appOrigen){
        final com.bancomer.constants.compartirvalorarapi.Constants.Aplicaciones aplicacion=getAppOrigenCode(appOrigen);
        final InitValorarCompartir compartirApi=new InitValorarCompartir(context);
        compartirApi.startCompartir(aplicacion);
    }

    public static void showValorar(final Context context,final String appOrigen){
        final com.bancomer.constants.compartirvalorarapi.Constants.Aplicaciones aplicacion=getAppOrigenCode(appOrigen);
        final InitValorarCompartir valorarApi=new InitValorarCompartir(context);
        valorarApi.startValorar(aplicacion);
    }

    private  static com.bancomer.constants.compartirvalorarapi.Constants.Aplicaciones getAppOrigenCode(final String appOrigen){
        com.bancomer.constants.compartirvalorarapi.Constants.Aplicaciones aplicacion=null;
        if("bmovil".equals(appOrigen)){
            aplicacion= com.bancomer.constants.compartirvalorarapi.Constants.Aplicaciones.BMOVIL;
        }else if("wallet".equals(appOrigen)){
            aplicacion= com.bancomer.constants.compartirvalorarapi.Constants.Aplicaciones.WALLET;
        }else if("vidaBancomer".equals(appOrigen)){
            aplicacion= com.bancomer.constants.compartirvalorarapi.Constants.Aplicaciones.VIDABANCOMER;
        }else if("lineaBancomer".equals(appOrigen)){
            aplicacion= com.bancomer.constants.compartirvalorarapi.Constants.Aplicaciones.LINEABANCOMER;
        }
        return aplicacion;
    }

    public static String definirAplicativo(final String app)
    {
        if("bmovil".equals(app))
        {
            return ConstanstMenuOpcionesHamburguesa.BMOVIL;
        }else if("wallet".equals(app))
        {
            return ConstanstMenuOpcionesHamburguesa.WALLET;
        }else if("vidaBancomer".equals(app))
        {
            return ConstanstMenuOpcionesHamburguesa.VIDABANCOMER;
        }else if("send".equals(app)){
            return ConstanstMenuOpcionesHamburguesa.SEND;
        }
        return null;
    }
}
