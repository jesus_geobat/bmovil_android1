package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.bancomer.mbanking.hamburguesa.R;
import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.ConstanstMenuOpcionesHamburguesa;
import suitebancomer.aplicaciones.bmovil.classes.delegates.hamburguesa.SelectPhotoPerfilDelegate;
/**
 * Created by eluna on 26/01/2016.
 */
public class SeleccionaFondoPerfilViewController extends Activity implements View.OnClickListener
{
    private ImageButton btnFondo1;
    private ImageButton btnFondo2;
    private ImageButton btnFondo3;
    private ImageButton btnFondo4;
    private ImageButton btnFondo5;
    private ImageButton btnFondo6;
    private ImageButton btnAceptar;
    private static ImageView linearFondoAct;
    private int banderaFondoPerfil = 0;
    private static ImageView img;
    public static SharedPreferences.Editor editor;
    public static SharedPreferences pref;
    final SelectPhotoPerfilDelegate delegate = new SelectPhotoPerfilDelegate();

    public void setBanderaFondoPerfil(final int banderaFondoPerfil) {
        this.banderaFondoPerfil = banderaFondoPerfil;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        hideTitle();
        setContentView(R.layout.bmovil_fondo_perfil_hamburguesa);
        initValues();
        pref = getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
    }

    public void initValues()
    {
        img = (ImageView) findViewById(R.id.profile_imageFondo);
        btnFondo1 = (ImageButton) findViewById(R.id.btnPerfil1);
        btnFondo1.setOnClickListener(this);
        btnFondo2 = (ImageButton) findViewById(R.id.btnPerfil2);
        btnFondo2.setOnClickListener(this);
        btnFondo3 = (ImageButton) findViewById(R.id.btnPerfil3);
        btnFondo3.setOnClickListener(this);
        btnFondo4 = (ImageButton) findViewById(R.id.btnPerfil4);
        btnFondo4.setOnClickListener(this);
        btnFondo5 = (ImageButton) findViewById(R.id.btnPerfil5);
        btnFondo5.setOnClickListener(this);
        btnFondo6 = (ImageButton) findViewById(R.id.btnPerfil6);
        btnFondo6.setOnClickListener(this);
        btnAceptar = (ImageButton) findViewById(R.id.btnAceptarHamburguesa);
        btnAceptar.setOnClickListener(this);
        linearFondoAct = (ImageView) findViewById(R.id.layout_fondo_perfil);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (DetalleInformacionHamburguesaViewController.isDifNull(pref.getString(ConstanstMenuOpcionesHamburguesa.PERFIL_IMAGE_URI, null))){

            delegate.buscarImagenPerfil(Boolean.TRUE,Boolean.FALSE, Uri.parse(pref.getString("perfilImageURI", null)));
        }else {
            delegate.resetearImagen(3);
        }
        if (null != pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA,null)){
            if (null != pref.getString("banderaGaleriaF", null) && pref.getString("banderaGaleriaF",null).equals("predeterminadas")){
                linearFondoAct.setBackgroundResource(Integer.parseInt(pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA, null)));
            }else{
                delegate.buscarImagenFondo(Boolean.FALSE,Boolean.FALSE,Uri.parse(pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA,null)),this);
            }
        }
    }

    private void hideTitle() {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    /**
     *
      * @param v
     */
    @Override
    public void onClick(final View v)
    {
        if(v.equals(btnFondo1))
        {
            cambioFondo(linearFondoAct, R.drawable.an_ic_fondo_perfil);
            mostrarOk(1);
            setBanderaFondoPerfil(R.drawable.an_ic_fondo_perfil);
        }
        else if(v.equals(btnFondo2))
        {
            cambioFondo(linearFondoAct, R.drawable.im_3);
            mostrarOk(2);
            setBanderaFondoPerfil(R.drawable.im_3);
        }
        else if (v.equals(btnFondo3))
        {
            cambioFondo(linearFondoAct, R.drawable.im_5);
            mostrarOk(3);
            setBanderaFondoPerfil(R.drawable.im_5);
        }
        else if (v.equals(btnFondo4))
        {
            cambioFondo(linearFondoAct, R.drawable.im_2);
            mostrarOk(4);
            setBanderaFondoPerfil(R.drawable.im_2);
        }
        else if (v.equals(btnFondo5))
        {
            cambioFondo(linearFondoAct, R.drawable.im_4);
            mostrarOk(5);
            setBanderaFondoPerfil(R.drawable.im_4);
        }
        else if (v.equals(btnFondo6))
        {
            cambioFondo(linearFondoAct, R.drawable.im_6);
            mostrarOk(6);
            setBanderaFondoPerfil(R.drawable.im_6);
        }
        else if (v.equals(btnAceptar))
        {
            if (banderaFondoPerfil == 0){
                MenuHamburguesaViewsControllers.getEditor().putString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA,String.valueOf(R.drawable.an_ic_fondo_perfil));
                MenuHamburguesaViewsControllers.getEditor().commit();
                finish();
            }else {
                MenuHamburguesaViewsControllers.getEditor().putString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA,String.valueOf(banderaFondoPerfil));
                MenuHamburguesaViewsControllers.getEditor().commit();
                finish();
            }
            editor.putString("banderaGaleriaF", "predeterminadas");
            editor.commit();
        }
        else {
            cambioFondo(linearFondoAct, R.drawable.an_ic_fondo_perfil);
            mostrarOk(1);
            setBanderaFondoPerfil(R.drawable.an_ic_fondo_perfil);
        }
    }

    public void mostrarOk(final int posicion)
    {
        switch (posicion)
        {
            case 1:
                btnFondo1.setBackgroundResource(R.drawable.img_faultok);
                btnFondo2.setBackgroundResource(R.drawable.im_3off);
                btnFondo3.setBackgroundResource(R.drawable.im_5off);
                btnFondo4.setBackgroundResource(R.drawable.im_2off);
                btnFondo5.setBackgroundResource(R.drawable.im_4off);
                btnFondo6.setBackgroundResource(R.drawable.im_6off);
                break;
            case 2:
                btnFondo1.setBackgroundResource(R.drawable.img_faultoff);
                btnFondo2.setBackgroundResource(R.drawable.im_3ok);
                btnFondo3.setBackgroundResource(R.drawable.im_5off);
                btnFondo4.setBackgroundResource(R.drawable.im_2off);
                btnFondo5.setBackgroundResource(R.drawable.im_4off);
                btnFondo6.setBackgroundResource(R.drawable.im_6off);
                break;
            case 3:
                btnFondo1.setBackgroundResource(R.drawable.img_faultoff);
                btnFondo2.setBackgroundResource(R.drawable.im_3off);
                btnFondo3.setBackgroundResource(R.drawable.im_5ok);
                btnFondo4.setBackgroundResource(R.drawable.im_2off);
                btnFondo5.setBackgroundResource(R.drawable.im_4off);
                btnFondo6.setBackgroundResource(R.drawable.im_6off);
                break;
            case 4:
                btnFondo1.setBackgroundResource(R.drawable.img_faultoff);
                btnFondo2.setBackgroundResource(R.drawable.im_3off);
                btnFondo3.setBackgroundResource(R.drawable.im_5off);
                btnFondo4.setBackgroundResource(R.drawable.im_2ok);
                btnFondo5.setBackgroundResource(R.drawable.im_4off);
                btnFondo6.setBackgroundResource(R.drawable.im_6off);
                break;
            case 5:
                btnFondo1.setBackgroundResource(R.drawable.img_faultoff);
                btnFondo2.setBackgroundResource(R.drawable.im_3off);
                btnFondo3.setBackgroundResource(R.drawable.im_5off);
                btnFondo4.setBackgroundResource(R.drawable.im_2off);
                btnFondo5.setBackgroundResource(R.drawable.im_4ok);
                btnFondo6.setBackgroundResource(R.drawable.im_6off);
                break;
            default:
                btnFondo1.setBackgroundResource(R.drawable.img_faultoff);
                btnFondo2.setBackgroundResource(R.drawable.im_3off);
                btnFondo3.setBackgroundResource(R.drawable.im_5off);
                btnFondo4.setBackgroundResource(R.drawable.im_2off);
                btnFondo5.setBackgroundResource(R.drawable.im_4off);
                btnFondo6.setBackgroundResource(R.drawable.im_6ok);
                break;

        }
    }

    public void cambioFondo(final ImageView linearLayout,final int background)
    {
        linearLayout.setBackgroundResource(background);
    }

    public static ImageView getImg() {
        return img;
    }

    public static ImageView getLinearFondoAct() {
        return linearFondoAct;
    }
}
