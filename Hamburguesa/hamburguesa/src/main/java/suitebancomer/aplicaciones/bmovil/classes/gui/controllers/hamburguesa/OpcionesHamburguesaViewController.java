package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import android.app.Activity;
import android.content.Context;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;
import com.bancomer.mbanking.softtoken.SuiteAppApi;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import suitebancomer.aplicaciones.bmovil.classes.alertas.impl.GeneraOTPS;
import suitebancomer.aplicaciones.bmovil.classes.entrada.InitializeApiAdmon;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MenuAdministrarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.BanderasServer;
import suitebancomer.classes.gui.controllers.administracion.BaseViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by eluna on 22/10/2015.
 */
public final class OpcionesHamburguesaViewController
{
    private OpcionesHamburguesaViewController(){}

    public static void showMenuAdministrar(final Context context) {
        InitializeApiAdmon.getInstance(context, new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT)).setCallBackSession(InitHamburguesa.getInstance().getCallBackSession());
        InitializeApiAdmon.getInstance(context, new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT)).initializeDependenceApis();
        final BaseViewsController baseViewsController = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();
        final MenuAdministrarDelegate delegate = new MenuAdministrarDelegate();
        baseViewsController.addDelegateToHashMap(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID, delegate);
        SuiteAppAdmonApi.getInstance().getBmovilApplication().setApplicationLogged(true);
        SuiteAppAdmonApi.setCallBackSession(InitHamburguesa.getInstance().getCallBackSession());
        SuiteAppAdmonApi.setCallBackBConnect(null);

    }

    public static void showAdministrarAlertas(final Activity context,final String tipoAlertas)
    {
        final GeneraOTPS generaOtps=new GeneraOTPS();
        generaOtps.inicializaCore(context);
        final InitAlertasDigitales init = InitAlertasDigitales.getInstance(context);

        // Pasar el IUM a API Alertas Digitales
        init.getConsultaSend().setIUM(Session.getInstance(context).getIum());
        // Pasar clave de acceso a API Alertas Digitales
        init.getConsultaSend().setCveAcceso(Session.getInstance(context).getPassword());
        // Pasar el username a API Alertas Digitales
        init.getConsultaSend().setUsername(Session.getInstance(context).getUsername());

        // Pasar el timeout a API Alertas Digitales
        init.getConsultaSend().setTimeout(Session.getInstance(context).getTimeout());

		/* Bloque de parametros de autenticacion*/
        // Pasar generador de OTPs a API Alertas para autenticacion
        init.getConsultaSend().setGeneraOtps(generaOtps);

        init.getConsultaSend().setCatalogoTelefonicas(Session.getInstance(context).getCatalogoTelefonicas());
        init.getConsultaSend().setPerfil(Session.getInstance(context).getClientProfile().toString());
        init.getConsultaSend().setSoftTokenStatus(SuiteAppApi.getSofttokenStatus());
        init.getConsultaSend().setSecurityInstrument(Session.getInstance(context).getSecurityInstrument());

        // Pasar los catalogos de operaciones para cada perfil y el limite de operacion API Alertas Digitales
        init.getConsultaSend().getAutenticacion().setAvanzado(Autenticacion.getInstance().getAvanzado());
        init.getConsultaSend().getAutenticacion().setBasico(Autenticacion.getInstance().getBasico());
        init.getConsultaSend().getAutenticacion().setRecortado(Autenticacion.getInstance().getRecortado());
        init.getConsultaSend().getAutenticacion().setLimiteOperacion(Autenticacion.getInstance().getLimiteOperacion());

        init.getConsultaSend().setCallBackModule(InitHamburguesa.getInstance().getCallBackModule());

        init.getConsultaSend().setServerParams(ServerCommons.DEVELOPMENT, ServerCommons.SIMULATION, ServerCommons.EMULATOR);
        Session.getInstance(context).setCmbPerfilHamburguesa(true);
        init.setAlertasDigitales(tipoAlertas);
        init.showAlertasDigitales();
    }
}
