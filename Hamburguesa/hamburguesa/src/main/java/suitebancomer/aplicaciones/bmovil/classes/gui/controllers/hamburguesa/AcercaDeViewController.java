package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.bancomer.mbanking.hamburguesa.R;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.ConstanstMenuOpcionesHamburguesa;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.controllers.WebViewController;

/**
 * @author Francisco.Garcia
 * @author IDS Comercial eluna
 *
 */
public class AcercaDeViewController extends Activity{

	TextView lblVersion;
	TextView lblIdentificador;
	TextView lblTerminal;
	TextView lblAcercaDeLegal;

	/**
	 * Textview clickeable que muestra el aviso de privacidad
	 */
	private TextView consultaAvisoPrivacidad;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		hideTitle();
		setContentView(R.layout.layout_bmovil_acercade);

		lblVersion = (TextView) findViewById(R.id.acerca_de_version_value_label);
		lblIdentificador = (TextView) findViewById(R.id.acerca_de_identificador_value_label);
		lblTerminal = (TextView) findViewById(R.id.acerca_de_terminal_value_label);
		lblAcercaDeLegal = (TextView) findViewById(R.id.acerca_de_legal_label);
		lblAcercaDeLegal.setText(Html.fromHtml(getString(R.string.aviso_privacidad)));

		consultaAvisoPrivacidad = (TextView) findViewById(R.id.consulta_aviso_privacidad);
		consultaAvisoPrivacidad.setPaintFlags(consultaAvisoPrivacidad.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

		showDescripcion();
		configurarPantalla();
	}
	
	private void configurarPantalla(){
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(lblVersion, true);
		gTools.scale(lblIdentificador, true);
		gTools.scale(lblTerminal, true);
		gTools.scale(findViewById(R.id.acerca_de_legal_label), true);
		gTools.scale(findViewById(R.id.acerca_de_info_sistema_label), true);
		gTools.scale(findViewById(R.id.title_text_acercadeh));
		gTools.scale(findViewById(R.id.title_icon_acercadeh));
		gTools.scale(findViewById(R.id.title_divider_acercadeh));
		gTools.scale(consultaAvisoPrivacidad, true);
	}

	private void hideTitle() {
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

	}

	private void showDescripcion() {
		inicializaAcercaDe();
	}

			public void inicializaAcercaDe() {
				int parseColor = getResources().getColor(R.color.primer_azul);
				String colorHexa = Integer.toHexString(parseColor);
				colorHexa = colorHexa.replace("ff", "");
				String version = getString(R.string.administrar_acercade_version)
						+ " " + getString(R.string.font_color_open) + colorHexa + getString(R.string.font_color_close)
						+ Tools.getDoubleAmountFromServerString(Constants.APPLICATION_VERSION)
						+ getString(R.string.font_color_final);
				if(ServerCommons.ARQSERVICE) {
					version = version + " A";
				}else{
					version = version + " N";
				}
				final String identificador = getString(R.string.administrar_acercade_identificador)
						+ " " + getString(R.string.font_color_open) + colorHexa + getString(R.string.font_color_close)
						+ getString(R.string.app_identifier)
						+ getString(R.string.font_color_final);
				final String terminal = getString(R.string.administrar_acercade_terminal)
						+ " " + getString(R.string.font_color_open) + colorHexa + getString(R.string.font_color_close)
						+ Tools.getDeviceForAcercaDe()
						+ getString(R.string.font_color_final);
				getLblVersion().setText(Html.fromHtml(version), TextView.BufferType.SPANNABLE);
				getLblIdentificador().setText(Html.fromHtml(identificador), TextView.BufferType.SPANNABLE);
				getLblTerminal().setText(Html.fromHtml(terminal), TextView.BufferType.SPANNABLE);
			}

	public TextView getLblVersion() {
		return lblVersion;
	}

	public TextView getLblIdentificador() {
		return lblIdentificador;
	}

	public TextView getLblTerminal() {
		return lblTerminal;
	}

	/**
	 * Muestra los terminos y condiciones.
	 */
	public void onConsultaAvisoPrivacidadLinkClik(final View sernder) {
		String textURL = ConstanstMenuOpcionesHamburguesa.URL_AVISO_PRIVACIDAD;
		Intent intent = new Intent(this, WebViewController.class);
		intent.putExtra(suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.URL, textURL);
		startActivity(intent);
		finish();

	}


}