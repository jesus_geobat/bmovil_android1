package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;
/**
 * @author eluna
 * @version 1.0
 * IDS Comercial S.A. de C.V
 */
import com.bancomer.mbanking.hamburguesa.R;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;

public final class IndicadorHamburguesa
{
    private static ProgressDialog mProgressDialog;
	private IndicadorHamburguesa() {}

	public static void muestraIndicadorActividad(final Context context,final String strTitle, final String strMessage) {
		mProgressDialog = ProgressDialog.show(context, strTitle,strMessage, true);
		mProgressDialog.setCancelable(false);
    }

    public static void ocultaIndicadorActividad() {
    	if(mProgressDialog != null){
    		mProgressDialog.dismiss();
    	}
    }
    
    public static void showErrorMessage(final Context context, final String errorMessage, final OnClickListener listener){
    	if(errorMessage.length() > 0){
    		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
    		alertDialogBuilder.setTitle(R.string.label_error);
    		alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
    		alertDialogBuilder.setMessage(errorMessage);
    		alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
    		alertDialogBuilder.setCancelable(false);
        	alertDialogBuilder.show();
    	}
    }
}