package com.bancomer.oneclickinversionliquida.io;

/**
 * Created by sandradiaz on 21/07/16.
 */
public class ServerException extends Exception {

    /**
     * Default constructor.
     * @param message the message
     */
    public ServerException(String message) {
        super(message);
    }
}
