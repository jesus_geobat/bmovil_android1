package com.bancomer.oneclickinversionliquida.commons;

/**
 * Created by sandradiaz on 22/07/16.
 */
public class ConstantsIL {

    public static String DETALLE_INEVRSION_LIQUIDA = "detalle inversion liquida";
    public static String CLAUSULAS_INEVRSION_LIQUIDA = "clausulas inversion liquida";
    public static String EXITO_INEVRSION_LIQUIDA = "exito inversion liquida";
    public static String TERMINOS_DE_USO_IL = "terminosConsumo";
    public static String CONTRATO_IL = "contratoConsumo";
    public static String SUCCESS_IL = "SuccessIL";

    public static String TAG_ACCOUNT_INDICATOR = "accountIndicator";
    public static String TAG_INITIAL_AMOUNT = "initialAmount";
    public static String TAG_INITIAL_RATE = "initialRate";
    public static String TAG_ACCOUNT_PER_SOL = "acountPerSol";
    public static String TAG_CR = "cr";
    public static String TAG_STATUS_CHECK = "statusCheck";
    public static String TAG_GAT_IL = "gatIL";
    public static String TAG_LIST_RATES = "listRates";
    public static String TAG_RATE = "rate";
    public static String TAG_MINIMUN_AMOUNT = "minimunAmount";
    public static String TAG_MAXIMUN_AMOUNT = "maximunAmount";
    public static String TAG_MAGNAGEMENT_UNIT = "managementUnit";

    public static String TAG_ACCOUNT_OPENING = "accountOpening";
    public static String TAG_AMOUNT = "amount";
    public static String TAG_CELLPHONE = "cellPhoneNumber";
    public static String TAG_CELLPHONE_NUMBER = "cellphoneNumber";
    public static String TAG_CODE_OTP = "otp";
    public static String TAG_CHAIN_AUTHENTICATION = "chainAuthentication";
    public static String TAG_CODE_NIP = "codeNip";
    public static String TAG_CODE_CVV2 = "codeCVV2";
    public static String TAG_CARD_5DIG = "card5Dig";
    public static String TAG_CVE_ACCESS = "cveAccess";

    public static String TAG_ACCOUNT_SENDER = "accountSender";
    public static String TAG_ACCOUNT_RECIEVER = "accountReciever";
    public static String TAG_FOLIO = "folio";

    public static String TAG_PERSONAL_SOL = "personalSolution";
    public static String TAG_NUM_CONTRACT = "numContract";
    public static String TAG_DATE_CONTRACT = "date";
    public static String TAG_SENDING_EMAIL = "sendingEmail";
    public static String TAG_SENDING_SMS = "sendingSms";

    public static String TAG_PM = "PM";
    public static String TAG_LP = "LP";
    public static String TAG_CAMPANIAS = "Campanias";
    public static String TAG_CVE_CAMP = "cveCamp";
    public static String TAG_DESOFERTA = "desOferta";
    public static String TAG_MONTO = "amount";
    public static String TAG_CARRUSEL = "carrusel";

    public static String TAG_ACCOUNT_NUMBER = "accountNumber";
    public static String TAG_NOMINAL_GAT = "nominalGat";
    public static String TAG_REAL_GAT = "realGat";

    public static String TAG_SAVE_IUM = "saveIum";
    public static String TAG_CREATE_CONTRACT = "createContract";
    public static String FOLIO_CONTRATO_IL = "PBINVLIQ01";//"PBCCMNOM02";//"PBINVLIQ01";

    public static String TAG_STATUS_IL = "status";
    public static String TAG_DESCRIPTION_IL = "description";
    public static String TAG_ERROR_CODE_IL = "errorCode";

    //------- Seguros -----

    public static String EXITO_SEGUROS = "exito seguros";
    public static String DETALLE_IL = "detalle IL";

    public static String CONTRATO_HTML_SEGUROS = "ContratoHTMLSeguros";
    public static String VALOR_ID_SEGUROS = "PBSEGGFU02";
    public static String VALOR_IDE_SEGUROS_DOMICILIACION = "PBCCMDOM01";

    public static String CUENTA_CARGO = "cuentaCargo";

    public static String TAG_CUENTA_SUGERIDA = "cuentaSugerida";
    public static String TAG_PRODUCTO = "producto";
    public static String TAG_IMPORTE_PRIMA = "importePrima";
    public static String TAG_COBERTURA_SEGURO = "coberturaSeguro";
    public static String TAG_COBERTURAS = "coberturas";
    public static String TAG_DESCOBERTURA = "desCobertura";
    public static String TAG_IMPORTE_SUMA_ASEGURADA = "importeSumaAsegurada";
    public static String TAG_FECHA_ALTA = "fechaAlta";
    public static String TAG_FECHA_VENCIMIENTO = "fechaVencimiento";
    public static String TAG_NUMERO_POLIZA = "numeroPoliza";
    public static String TAG_ENVIO_CORREO_ELECTRONICO = "envioCorreoElectronico";
    public static String TAG_ENVIO_SMS = "envioSMS";

    public static String OP_TERMINOS = "TERMINOS";
    public static String OP_DOMICILIA = "DOMICILIA";

    public static final String OPERACION_TAG="operacion";
    public static final String NUMERO_CELULAR_TAG="numeroCelular";
    public static final String CVE_CAMP_TAG="cveCamp";
    public static final String IUM_TAG="ium";
    public static final String OPCION_TAG="opcion";


    public static final String OPCION_DETALLE_VALUE="D";
    public static final String OPCION_FORMALIZACION_VALUE="F";

    public static final String CODIGO_OTP_TAG="codigoOTP";
    public static final String CADENA_AUTENTICACION_TAG="cadenaAutenticacion";
    public static final String CODIGO_NIP_TAG="codigoNip";
    public static final String CODIGO_CVV2_TAG="codigoCVV2";
    public static final String TARJETA_5IG_TAG="Tarjeta5ig";

    public static final String ID_PRODUCTO_TAG="idProducto";
}
