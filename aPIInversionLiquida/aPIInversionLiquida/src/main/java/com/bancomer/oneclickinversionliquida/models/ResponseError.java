package com.bancomer.oneclickinversionliquida.models;

import android.util.Log;

import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by sandradiaz on 05/12/16.
 */
public class ResponseError implements ParsingHandler {
    private String status;
    private String description;
    private String errorCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

    }

    public void process(String parser) throws IOException, ParsingException, JSONException {
        JSONObject obj = new JSONObject(parser);

        setStatus((String.valueOf(obj.get(ConstantsIL.TAG_STATUS_IL)).length() > 0) ? String.valueOf(obj.get(ConstantsIL.TAG_STATUS_IL)) : "");
        setDescription((String.valueOf(obj.get(ConstantsIL.TAG_DESCRIPTION_IL)).length() > 0) ? String.valueOf(obj.get(ConstantsIL.TAG_DESCRIPTION_IL)) : "");
        setErrorCode((String.valueOf(obj.get(ConstantsIL.TAG_ERROR_CODE_IL)).length() > 0) ? String.valueOf(obj.get(ConstantsIL.TAG_ERROR_CODE_IL)) : "");
    }
}
