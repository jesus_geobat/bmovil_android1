package com.bancomer.oneclickinversionliquida.models;

/**
 * Created by sandradiaz on 31/07/16.
 */
public class RatesIL {
    private String rate;
    private String minimunAmount;
    private String maximunAmount;

    public RatesIL(){
        super();
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getMinimunAmount() {
        return minimunAmount;
    }

    public void setMinimunAmount(String minimunAmount) {
        this.minimunAmount = minimunAmount;
    }

    public String getMaximunAmount() {
        return maximunAmount;
    }

    public void setMaximunAmount(String maximunAmount) {
        this.maximunAmount = maximunAmount;
    }
}
