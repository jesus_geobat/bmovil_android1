package com.bancomer.oneclickinversionliquida.gui.controllers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;
import com.bancomer.oneclickinversionliquida.models.GatIL;
import com.bancomer.oneclickinversionliquida.models.SuccessIL;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicBoolean;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.timer.TimerController;

/**
 * Created by sandradiaz on 22/07/16.
 */
public class TerminosYCondicionesViewController extends Activity implements View.OnClickListener{

    private WebView wvpoliza;
    private ImageView imgBack;
    private GestureDetector gestureDetector;
    private String termino;
    private String contrato;
    private BaseViewController baseViewController;
    private InitOneClickIL init = InitOneClickIL.getInstance();

    private AtomicBoolean mPreventAction = new AtomicBoolean(false);
    private ImageView img_encabezadoDetalle;
    private TextView txtEncabezado;

    private String montInv;
    private String tasaInv;
    private String cuentaSP;
    private String cr;
    private boolean[] statusCheck;
    private GatIL gatIL;
    private String managementUnit;
    private SuccessIL successIL;
    private String saveIum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init.getParentManager().setCurrentActivityApp(this);

        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_seguros_terminos_condiciones);
        init.setBaseViewController(baseViewController);

        montInv = (String)this.getIntent().getExtras().get(ConstantsIL.TAG_AMOUNT);
        tasaInv = (String)this.getIntent().getExtras().get(ConstantsIL.TAG_RATE);
        cuentaSP = (String)this.getIntent().getExtras().get(ConstantsIL.TAG_ACCOUNT_PER_SOL);
        cr = (String)this.getIntent().getExtras().get(ConstantsIL.TAG_CR);
        statusCheck = (boolean[])this.getIntent().getExtras().get(ConstantsIL.TAG_STATUS_CHECK);
        gatIL = (GatIL)this.getIntent().getExtras().get(ConstantsIL.TAG_GAT_IL);
        managementUnit = (String)this.getIntent().getExtras().get(ConstantsIL.TAG_MAGNAGEMENT_UNIT);
        successIL = (SuccessIL)this.getIntent().getExtras().get(ConstantsIL.SUCCESS_IL);
        saveIum = (String)this.getIntent().getExtras().get(ConstantsIL.TAG_SAVE_IUM);

        termino = (String)this.getIntent().getExtras().get(ConstantsIL.TERMINOS_DE_USO_IL);
        if(termino != null)
            Log.e("Terminos Controller", termino);
        contrato = (String)this.getIntent().getExtras().get(ConstantsIL.CONTRATO_IL);
        if(contrato != null)
            Log.e("Contrato Controller", contrato);
        /******/
        ViewGroup scroll = (ViewGroup) findViewById(R.id.body_layout);
        ViewGroup parent = (ViewGroup) scroll.getParent();
        parent.removeView(scroll);

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.api_seguros_terminos_condiciones, parent, true);
        findViews();
        scaleToScreenSize();
        /******/
        if(termino != null) {
            showTerminos(termino);

            img_encabezadoDetalle.setImageResource(R.drawable.il_contratacion);
        }else if (contrato != null){
            showTerminos(contrato);

            img_encabezadoDetalle.setVisibility(View.GONE);
            txtEncabezado.setVisibility(View.VISIBLE);
            txtEncabezado.setText("Contratación exitosa");
        }


    }

    private void showTerminos(String html){
        html =leerArchivo(html);
        String html1;
        wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        wvpoliza.getSettings().setBuiltInZoomControls(true);
        double factor = GuiTools.getScaleFactor();
        wvpoliza.setInitialScale((int) (45 * factor));
        html1= html.replace("FECHADIA","________").replace("FECHAMES","______").replace("FECANO","____").replace("MONTOMAX","______").replace("FECVENCI","\"\"];").replace("BSCPAGAR","_____________").replace("<u>CONTRATO</u>","________").replace("<u>PERIODICIDAD</u"," ________").replace("FECEXIGI","________").replace("CVEBANCO","________").replace("RECA","______").replace("BBVA BANCOMER S.A. GRUPO FINANCIERO BBVA BANCOMER","_______________________________")
                .replace("<\\\\/div>", "</div>").replace("<\\\\/", "</")
                .replace("tdcolspan", "td colspan")
                .replace("tdwidth", "td width")
                .replace("solid1px", "solid 1px")
                .replace("width:801px", " width:800px").replace("\\\"","\"")
                .replace("{\"response\":{\"versiones\":[{\"verCatalogoContratoIL\":\"1\"}],\"catalogos\":{\"ContratoIL\":\"\\r\\n{\"estado\":\"OK\",\"txtHTML1\":\"", "")
                .replace("\"}\"}},\"status\":{\"code\":200,\"description\":\"Exito\"}}","");
        Log.e("HTML", html1);
        wvpoliza.loadData(html1, "text/html", "utf-8");

        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                mPreventAction.set(true);
                return true;
            }
            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                mPreventAction.set(true);
                return true;
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                mPreventAction.set(true); // this is the key! this would block double tap to zoom to fire
                return false;
            }

        });
        gestureDetector.setIsLongpressEnabled(true);
        wvpoliza.setOnTouchListener(new View.OnTouchListener() {
            @TargetApi(Build.VERSION_CODES.FROYO)
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int pointId = 0;
                if (Build.VERSION.SDK_INT > 7) {
                    int index = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                    pointId = event.getPointerId(index);
                }
                if (pointId == 0) {
                    gestureDetector.onTouchEvent(event);
                    if (mPreventAction.get()) {
                        mPreventAction.set(false);
                        return true;
                    }
                    return wvpoliza.onTouchEvent(event);
                } else return true;
            }
        });
    }

    private String leerArchivo(String url){
        String leerHTML = "";
        try {
            FileInputStream fIn = new FileInputStream(url);
            InputStreamReader archivo = new InputStreamReader(fIn);
            BufferedReader br = new BufferedReader(archivo);
            String linea = br.readLine();
            while (linea != null) {
                leerHTML += linea + "";
                linea = br.readLine();
            }
            br.close();
            archivo.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return leerHTML;
    }

    /**
     * Busca las referencias a las vistas.
     */
    private void findViews() {
        img_encabezadoDetalle = (ImageView) findViewById(R.id.img_encabezadoDetalle);
        wvpoliza = (WebView)findViewById(R.id.poliza);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        txtEncabezado = (TextView) findViewById(R.id.txtEncabezado);

        imgBack.setOnClickListener(this);
    }

    /**
     * Escala las vistas para acomodarse a la pantalla actual.
     */
    private void scaleToScreenSize() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.layoutBaseContainer));
        guiTools.scale(img_encabezadoDetalle);
        guiTools.scale(imgBack);
        guiTools.scale(wvpoliza);
        guiTools.scale(findViewById(R.id.title_layout));
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        float touchX = ev.getX();
        float touchY = ev.getY();
        int[] webViewPos = new int[2];

        wvpoliza.getLocationOnScreen(webViewPos);

        float listaSeleccionX2 = webViewPos[0] + wvpoliza.getMeasuredWidth();
        float listaSeleccionY2 = webViewPos[1] + wvpoliza.getMeasuredHeight();

        if ((touchX >= webViewPos[0] && touchX <= listaSeleccionX2) &&
                (touchY >= webViewPos[1] && touchY <= listaSeleccionY2)) {
            wvpoliza.getParent().requestDisallowInterceptTouchEvent(true);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!init.getParentManager().isActivityChanging()) {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    @Override
    public void onBackPressed() {
        init.getParentManager().setActivityChanging(true);
        super.onBackPressed();
        if(termino != null) {
            init.showClausulas(montInv, tasaInv, cuentaSP, cr, statusCheck, gatIL, managementUnit);
        }else if(contrato != null){
            init.showExito(montInv, tasaInv, saveIum, successIL);
        }
    }

    View.OnClickListener listener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };

    @Override
    public void onClick(View v) {
        if(v == imgBack){
            onBackPressed();
        }
    }
}
