package com.bancomer.oneclickinversionliquida.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.adobe.mobile.Config;
import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.commons.Tracker;
import com.bancomer.oneclickinversionliquida.gui.delegates.ExitoILDelegate;
import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;

/**
 * Created by SDIAZ on 05/07/16.
 */
public class ExitoILViewController extends Activity implements View.OnClickListener {

    private BaseViewController baseViewController;
    private InitOneClickIL init = InitOneClickIL.getInstance();
    private BaseViewsController parentManager;
    private ExitoILDelegate delegate;

    public ImageView imgHeader;
    private ImageView imgBack;
    public TextView linkContrato;
    public ImageButton btnMenu;
    private TextView txtMonto;
    private TextView txtTasa;
    private TextView txt_cuenta_sp;
    private TextView txt_cuenta_il;
    private TextView txt_fecha_alta;
    private TextView txt_fecha_corte;
    private TextView txt_hora_contratado;
    private TextView txt_folio_operacion;
    private TextView txt_correo_usuario;
    private TextView txt_celular_usuario;


    ArrayList<String> list= new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate = (ExitoILDelegate) parentManager.getBaseDelegateForKey(ExitoILDelegate.EXITO_IL_DELEGATE);
        baseViewController.setActivity(this);
        baseViewController.onCreate(this, 0, R.layout.api_inversion_liquida_exito);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        //Adobe
        Config.setContext(this.getApplicationContext());

        list.add(ConstantsIL.EXITO_INEVRSION_LIQUIDA);
        Tracker.trackState(list);

        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        Tracker.trackEntrada(eventoEntrada);

        init();
    }

    private void init() {
        findViews();
        scaleViews();
        confScreen();
    }

    private void findViews() {
        imgHeader = (ImageView) findViewById(R.id.img_encabezadoDetalle);
        imgHeader.setVisibility(View.GONE);

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setVisibility(View.GONE);

        linkContrato = (TextView) findViewById(R.id.link_contrato);
        linkContrato.setOnClickListener(this);

        btnMenu = (ImageButton) findViewById(R.id.btnMenu_il);
        btnMenu.setOnClickListener(this);

        txtMonto = (TextView) findViewById(R.id.txtMontoFin);
        txtTasa = (TextView) findViewById(R.id.txtTasaFin);
        txt_cuenta_sp = (TextView) findViewById(R.id.txt_cuenta_sp);
        txt_cuenta_il = (TextView) findViewById(R.id.txt_cuenta_il);
        txt_fecha_alta = (TextView) findViewById(R.id.txt_fecha_alta);
        txt_fecha_corte = (TextView) findViewById(R.id.txt_fecha_corte);
        txt_hora_contratado = (TextView) findViewById(R.id.txt_hora_contratado);
        txt_folio_operacion = (TextView) findViewById(R.id.txt_folio_operacion);
        txt_correo_usuario = (TextView) findViewById(R.id.txt_correo_usuario);
        txt_celular_usuario = (TextView) findViewById(R.id.txt_celular_usuario);
    }

    @Override
    public void onClick(View v) {
        if(v == btnMenu){
            init.getParentManager().setActivityChanging(true);
            ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }else if(v == linkContrato){
            init.getParentManager().setActivityChanging(true);
            delegate.realizaOperacionTerminosYCondiciones();
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!init.getParentManager().isActivityChanging()) {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    private void scaleViews() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.imgCorreo));
        guiTools.scale(findViewById(R.id.imgCel));
    }

    public  void confScreen(){
        txt_cuenta_sp.setText(delegate.getSuccessIL().getPersonalSolution());
        txt_cuenta_il.setText(delegate.getSuccessIL().getNumContract());
        txtMonto.setText("$" + Tools.formatUserAmount(delegate.getSuccessIL().getAmount()));
        txtTasa.setText(delegate.getTasaInv());

        String[] dateContract = delegate.getSuccessIL().getDateContract().split("T");
        String[] time = dateContract[1].split("\\.");
        txt_fecha_alta.setText(Tools.dateToString(Date.valueOf(dateContract[0])));

        Calendar calendar = Calendar.getInstance();
        String[] date = dateContract[0].split("-");
        calendar.set(Integer.valueOf(date[0]), Integer.valueOf(date[1]) - 1, Integer.valueOf(date[2]));
        String lastDay = String.valueOf(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        txt_fecha_corte.setText(lastDay+"/"+date[1]+"/"+date[0]);
        txt_hora_contratado.setText(time[0]);

        txt_folio_operacion.setText(delegate.getSuccessIL().getFolio());
        txt_correo_usuario.setText(init.getSession().getEmail());
        txt_celular_usuario.setText(String.valueOf(Tools.hideUsername(init.getSession().getUsername())));
    }
}
