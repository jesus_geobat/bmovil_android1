package com.bancomer.oneclickinversionliquida.gui.delegates;
import android.util.Log;

import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.gui.controllers.DetalleILViewController;
import com.bancomer.oneclickinversionliquida.gui.controllers.R;
import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;
import com.bancomer.oneclickinversionliquida.io.Server;
import com.bancomer.oneclickinversionliquida.models.DetailIL;
import com.bancomer.oneclickinversionliquida.models.GatIL;
import com.bancomer.oneclickinversionliquida.models.RatesIL;
import java.util.ArrayList;
import java.util.Hashtable;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.gui.delegates.SecurityComponentDelegate;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.common.model.HostAccounts;
/**
 * Created by SDIAZ on 23/06/16.
 */
public class DetalleILDelegate extends SecurityComponentDelegate implements BaseDelegate {
    public static final long INVERSION_LIQUIDA_DETALLE_DELEGATE = 10602003111230813L;
    InitOneClickIL init;
    private BaseViewController baseViewController;
    private DetalleILViewController controller;
    private Constants.TipoInstrumento tipoInstrumentoSeguridad;
    private DetailIL detailIL;
    private String montoIL;
    private String tasaIL;
    //Seekbar
    private Integer montoMin;
    private Integer montoMax;
    private Integer montoProgress;
    private RatesIL[] ratesil;
    private Integer montoSP;
    public DetalleILDelegate(DetailIL detailIL){
        init = InitOneClickIL.getInstance().getInstance();
        this.detailIL = detailIL;
        String instrumento = init.getSession().getSecurityInstrument();
        if (instrumento.equals(Constants.IS_TYPE_DP270)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
        } else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
        } else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)||instrumento.equals(Constants.TYPE_SOFTOKEN.S2.value)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
        } else {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
        }
    }
    public DetalleILDelegate(){
        init = InitOneClickIL.getInstance().getInstance();
    }
    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
    }
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller,Boolean isJson, ParsingHandler handler) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false, isJson, handler);
    }
    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if(operationId == Server.CONSULTA_DETALLE_IL){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                DetailIL detailResponse = (DetailIL) response.getResponse();
                if(cargaCuentaSP(detailResponse)) {
                    init.showDetalleIL(detailResponse);
                }else{
                    init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(), "Tu cuenta Solución Personal no cuenta con el saldo suficiente para poder aperturar la Inversión");
                }
            }
            else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
                try {
                    init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(), response.getMessageText());
                }catch (NullPointerException e){
                    init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(), response.getMessageText());
                }
            }else{
                init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(), response.getMessageText());
            }
        } else if(operationId == Server.CONSULTA_GATS_IL){
            GatIL gatIL = (GatIL) response.getResponse();
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                init.getParentManager().setActivityChanging(true);
                init.showClausulas(getMontoIL(), getTasaIL(),
                        getDetailIL().getAccountPerSol(), getDetailIL().getCr(),
                        null, gatIL, getDetailIL().getManagementUnit());
            }else{
                gatIL = new GatIL(" "," ");
                init.getParentManager().setActivityChanging(true);
                init.showClausulas(getMontoIL(), getTasaIL(),
                        getDetailIL().getAccountPerSol(), getDetailIL().getCr(),
                        null, gatIL, getDetailIL().getManagementUnit());
            }
        }
    }
    @Override
    public void performAction(Object o) {
        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(init.getParentManager().getCurrentViewController());
        baseViewController.setDelegate(this);
        init.setBaseViewController(baseViewController);
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ConstantsIL.TAG_CVE_CAMP, init.getOfertaSeguros().getCveCamp().replace("\"", ""));
        params.put(ConstantsIL.IUM_TAG, init.getSession().getIum());
        doNetworkOperation(Server.CONSULTA_DETALLE_IL, params, InitOneClickIL.getInstance().getBaseViewController(), false, new DetailIL());
    }
    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
    public Integer getMontoMin() {
        return montoMin;
    }
    public void setMontoMin(Integer montoMin) {
        this.montoMin = montoMin;
    }
    public Integer getMontoMax() {
        return montoMax;
    }
    public void setMontoMax(Integer montoMax) {
        this.montoMax = montoMax;
    }
    public Integer getMontoProgress() {
        return montoProgress;
    }
    public void setMontoProgress(Integer montoProgress) {
        this.montoProgress = montoProgress;
    }
    public String getMontoIL() {
        return montoIL;
    }
    public void setMontoIL(String montoIL) {
        this.montoIL = montoIL;
    }
    public String getTasaIL() {
        return tasaIL;
    }
    public void setTasaIL(String tasaIL) {
        this.tasaIL = tasaIL;
    }
    public static Integer getAmountThFormatted(Integer data){
        Integer min = 1000;
        String ret = min.toString();
        if(data > min){
            ret = data.toString();
            ret = ret.substring(0, (ret.length()-3));
            ret = ret+"000";
        }
        return Integer.valueOf(ret);
    }
    public DetailIL getDetailIL() {
        return detailIL;
    }
    public Integer getMontoSP() {
        ArrayList<HostAccounts> accountsArray = obtieneCuentas();
        for(HostAccounts acc : accountsArray) {
            String cuenta = acc.getNumber().substring(acc.getNumber().length()-10, acc.getNumber().length());
            String cuentaSP = detailIL.getAccountPerSol().substring(detailIL.getAccountPerSol().length()-10, detailIL.getAccountPerSol().length());
            if(cuenta.equals(cuentaSP)) {
                Double balanceSP = acc.getBalance();
                montoSP = balanceSP.intValue();
            }
        }
        return montoSP;
    }
    public void setMontoSP(Integer montoSP) {
        this.montoSP = montoSP;
    }
    /* Obtener cuenta de Solución Personal*/
    public Boolean cargaCuentaSP(DetailIL detailResponse){
        ArrayList<HostAccounts> accountsArray = obtieneCuentas();
        HostAccounts accountCargo = null;
        Boolean banSP = false;
        DetailIL detail = detailResponse;
        for(HostAccounts acc : accountsArray) {
            String cuenta = acc.getNumber().substring(acc.getNumber().length()-10, acc.getNumber().length());
            String cuentaSP = detail.getAccountPerSol().substring(detail.getAccountPerSol().length()-10, detail.getAccountPerSol().length());
            if(cuenta.equals(cuentaSP)){
                accountCargo = acc;
                if(accountCargo.getBalance() >= Double.parseDouble(detail.getInitialAmount()))
                    banSP = true;
                break;
            }
        }
        return banSP;
    }
    /** ordena las cuentas y pone en la posicion 0 la cuenta cargo que viene por default
     * en la oferta de domiciliacion, en caso de no tener en el arreglo de cuentas la cuenta cargo
     * se obtiene la cuenta eje y es la que se setea en la posicion 0
     */
    public ArrayList<HostAccounts> cargaCuentasOrigen(){
        ArrayList<HostAccounts> accountsArray = obtieneCuentas();
        ArrayList<HostAccounts> accArrayOrdenado = new ArrayList<HostAccounts>();
        HostAccounts accountCargo = null;
        HostAccounts accountEje = null;
        for(HostAccounts acc : accountsArray) {
            accountEje = Tools.obtenerCuentaEje(cuentaOrigen());
            String cuenta = acc.getNumber().substring(acc.getNumber().length()-10, acc.getNumber().length());
            String cuentaSP = detailIL.getAccountPerSol().substring(detailIL.getAccountPerSol().length()-10, detailIL.getAccountPerSol().length());
            if(cuenta.equals(cuentaSP)) {
                accountCargo = acc;
                accArrayOrdenado.add(0, accountCargo);
                break;
            }
        }
        return accArrayOrdenado;
    }
    //obtiene las cuentas de tipo AH, LI y CH
    public ArrayList<HostAccounts> obtieneCuentas(){
        ArrayList<HostAccounts> accountsArray = new ArrayList<HostAccounts>();
        HostAccounts[] accounts = cuentaOrigen();
        for(HostAccounts acc : accounts) {
            accountsArray.add(acc);
        }
        return accountsArray;
    }
    public HostAccounts[] cuentaOrigen(){
        return init.getListaCuentas();
    }
    public String fijarTasa(int monto){
        ratesil = getDetailIL().getListRates();
        int pos=0;
        for (int i=0; i<ratesil.length; i++){
            if(!ratesil[i].getRate().equals("0")) {
                if (i == 0) {
                    if (monto >= 0 && monto <= Float.parseFloat(ratesil[i].getMinimunAmount())) {
                        pos = i;
                        break;
                    }
                } else {
                    if (monto >= Float.parseFloat(ratesil[i - 1].getMinimunAmount()) && monto <= Float.parseFloat(ratesil[i].getMinimunAmount())) {
                        pos = i;
                        break;
                    }
                }
            }
        }
        return String.valueOf(ratesil[pos].getRate());
    }
    public void realizaOperacionGats(){
        init = InitOneClickIL.getInstance().getInstance();
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ConstantsIL.TAG_ACCOUNT_NUMBER, detailIL.getAccountPerSol().substring(detailIL.getAccountPerSol().length()-10, detailIL.getAccountPerSol().length()));
        params.put(ConstantsIL.TAG_AMOUNT, getMontoIL().replace("$","").replace(",", "").replace(".00", ""));
        params.put(ConstantsIL.TAG_RATE, getTasaIL().replace("$","").replace("%", ""));
        params.put(ConstantsIL.IUM_TAG, init.getSession().getIum());
        doNetworkOperation(Server.CONSULTA_GATS_IL, params, init.getBaseViewController(),false, new GatIL());
    }
}
