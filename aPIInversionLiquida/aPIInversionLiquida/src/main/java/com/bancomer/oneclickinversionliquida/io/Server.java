package com.bancomer.oneclickinversionliquida.io;

import com.bancomer.oneclickinversionliquida.models.SuccessIL;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import bancomer.api.common.io.ParsingException;

/**
 * Created by sandradiaz on 21/07/16.
 */
public class Server {
    /**
     * Defines if the application show logs
     */
    public static boolean ALLOW_LOG = true;

    /**
     * Defines if the application is running on the emulator or in the device.
     */
    public static boolean EMULATOR = false;

    /**
     * Indicates simulation usage.
     */
    public static boolean SIMULATION = false;

    /**
     * Indicates if the base URL points to development server or production
     */
    public static boolean DEVELOPMENT = true;
    // public static boolean DEVELOPMENT = true; //TODO remove for TEST

    /**
     * Indicates simulation usage.
     */
    public static final long SIMULATION_RESPONSE_TIME = 5;


    //public static final String JSON_OPERATION_SEGUROS = "SGRS001";
    public static final String JSON_OPERATION_IL = "";

    /**
     * Bancomer Infrastructure.
     */
    public static final int BANCOMER = 0;

    public static int PROVIDER = BANCOMER; // TODO remove for TEST


    // ///////////////////////////////////////////////////////////////////////////
    // Operations identifiers //
    // ///////////////////////////////////////////////////////////////////////////


    /**ONE CLICK
     * IL*/
    public static final int CONSULTA_DETALLE_IL = 224;
    public static final int CONSULTA_TERMINOS_CONDICIONES_IL = 225;
    public static final int CONSULTA_CONTRATACION_IL = 226;
    public static final int ACTUALIZAR_CUENTAS = 41;
    public static final int TRANSFERENCIAS_IL = 227;
    public static final int CONSULTA_GATS_IL = 228;
    public static final int CONSULTA_CONTRATO_IL = 220;



    /**
     * Operation codes.
     */
    public static final String[] OPERATION_CODES = { "detalleOneClickIL",
                                                    "oneClicSeguros",
                                                    "ContratoHTMLSeguros",
                                                    "oneClicSeguros",
                                                    "actualizarCuentas",
                                                    "oneClicSeguros",
                                                    "oneClicSeguros",
                                                    "oneClicSeguros"};
    /**
     * Base URL for providers.
     */
    public static String[] BASE_URL;

    /**
     * Path table for provider/operation URLs.
     */
    public static String[][] OPERATIONS_PATH;

    /**
     * Operation URLs map.
     */
    public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

    /**
     * Operation providers map.
     */
    public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

    /**
     * Operation data parameter.
     */
    public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

    /**
     * Operation code parameter.
     */
    public static final String OPERATION_CODE_PARAMETER = "OPERACION";


    /**
     * Operation data parameter.
     */
    public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

    /**
     * Operation data parameter.
     */
    public static final String OPERATION_LOCALE_VALUE = "es_ES";



    public static void setEnviroment() {
        if (DEVELOPMENT) {
            setupDevelopmentServer();
        } else {
            setupProductionServer();
        }

        setupOperations(PROVIDER);
    }

    /**
     * Setup production server paths.
     */
    private static void setupProductionServer() {
        BASE_URL = new String[] { "https://www.bancomermovil.com",
                // "https://172.17.100.173",
                EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Produccion"
                        : "" };
        OPERATIONS_PATH = new String[][] {
                new String[] { "", "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/", // OP1: Consulta Detalle ventaTDC *ONE CLICK*
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
                        "/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP4: Formatos finales ventaTDC *ONE CLICK*
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP4: Formatos finales ventaTDC *ONE CLICK*
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP4: Formatos finales ventaTDC *ONE CLICK*

                },
                new String[] { "", "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/", // OP1: Consulta Detalle ventaTDC *ONE CLICK*
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
                        "/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP4: Formatos finales ventaTDC *ONE CLICK*
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP4: Formatos finales ventaTDC *ONE CLICK*
                        "/prmbmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP4: Formatos finales ventaTDC *ONE CLICK*


                } };
    }

    /**
     * Setup production server paths.
     */
    private static void setupDevelopmentServer() {
        BASE_URL = new String[] {
                "https://www.bancomermovil.net:11443",
                EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Desarrollo"
                        : "http://189.254.77.54:8080/servermobile/Servidor/Desarrollo" };
        OPERATIONS_PATH = new String[][] {
                new String[] { "", "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP1: Consulta Detalle ventaTDC *ONE CLICK*
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
                        "/eexd_mx_web/servlet/ServletOperacionWeb",  // OPactualizarCuentas
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP4: Formatos finales ventaTDC *ONE CLICK*
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP4: Formatos finales ventaTDC *ONE CLICK*
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP4: Formatos finales ventaTDC *ONE CLICK*

                },
                new String[] { "", "/eexd_mx_web/servlet/ServletOperacionWeb",  // OP1: Consulta Detalle ventaTDC *ONE CLICK*
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil/",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
                        "/eexd_mx_web/servlet/ServletOperacionWeb",  // OPactualizarCuentas
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil",  // OP4: Formatos finales ventaTDC *ONE CLICK*
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil",  // OP4: Formatos finales ventaTDC *ONE CLICK*
                        "/dembmv_mx_web/mbmv_mult_web_mbmv_01/services/bmovil",  // OP4: Formatos finales ventaTDC *ONE CLICK*


                } };
    }

    /**
     * Setup operations for a provider.
     *
     * @param provider
     *            the provider
     */
    private static void setupOperations(int provider) {

        setupOperation(CONSULTA_DETALLE_IL, provider); // Detalle
        setupOperation(CONSULTA_TERMINOS_CONDICIONES_IL, provider); //Terminos y Condiciones
        setupOperation(CONSULTA_CONTRATACION_IL,provider); // Formalizar cuenta
        setupOperation(ACTUALIZAR_CUENTAS, provider); // Actualizar cuentas
        setupOperation(TRANSFERENCIAS_IL,provider); // Transferencias
        setupOperation(CONSULTA_GATS_IL,provider); // Gats
        setupOperation(CONSULTA_CONTRATO_IL,provider); // Contrato HTML

    }

    /**
     * Setup operation for a provider.
     * @param operation the operation
     * @param provider the provider
     */
    private static void setupOperation(int operation, int provider) {
        int op=0;
        switch (operation){
            case CONSULTA_DETALLE_IL:
                op=1;
                break;
            case CONSULTA_TERMINOS_CONDICIONES_IL:
                op=2;
                break;
            case CONSULTA_CONTRATACION_IL:
                op=3;
                break;
            case ACTUALIZAR_CUENTAS:
                op=4;
            case TRANSFERENCIAS_IL:
                op=5;
                break;
            case CONSULTA_GATS_IL:
                op=6;
                break;
            case CONSULTA_CONTRATO_IL:
                op=7;
                break;
        }

        String code = OPERATION_CODES[op];
        OPERATIONS_MAP.put(
                code,
                new StringBuffer(BASE_URL[provider]).append(
                        OPERATIONS_PATH[provider][op]).toString());
        OPERATIONS_PROVIDER.put(code, new Integer(provider));
    }

    /**
     * Get the operation URL.
     * @param operation the operation id
     * @return the operation URL
     */
    public static String getOperationUrl(String operation) {
        String url = (String) OPERATIONS_MAP.get(operation);
        return url;
    }

    /**
     * Referencia a ClienteHttp, en lugar de HttpInvoker.
     */
    private ClienteHttp clienteHttp = null;


    // protected Context mContext;

    public ClienteHttp getClienteHttp() {
        return clienteHttp;
    }

    public void setClienteHttp(ClienteHttp clienteHttp) {
        this.clienteHttp = clienteHttp;
    }



    public Server() {
        clienteHttp = new ClienteHttp();
    }

    /**
     * perform the network operation identifier by operationId with the
     * parameters passed.
     * @param operationId the identifier of the operation
     * @param params Hashtable with the parameters as key-value pairs.
     * @return the response from the server
     * @throws IOException exception
     * @throws ParsingException exception
     */
    public ServerResponseImpl doNetworkOperation(int operationId, Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

        ServerResponseImpl response = null;
        long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
        Integer provider = (Integer) OPERATIONS_PROVIDER.get(OPERATION_CODES[operationId]);
        if (provider != null) {
            PROVIDER = provider.intValue();
        }

        switch (operationId) {

            case CONSULTA_DETALLE_IL: // ONE CLICK venta TDC
                response = formalizacionIL(params);
                break;

            case CONSULTA_TERMINOS_CONDICIONES_IL:
                response = formalizacionIL(params);
                break;

            case CONSULTA_CONTRATACION_IL:
                response = formalizacionIL(params);
                break;

            case ACTUALIZAR_CUENTAS:
                response = formalizacionIL(params);
                break;

            case TRANSFERENCIAS_IL:
                response = formalizacionIL(params);
                break;

            case CONSULTA_GATS_IL:
                response = formalizacionIL(params);
                break;

            case CONSULTA_CONTRATO_IL:
                response = formalizacionIL(params);
                break;
        }
        //Termina SPEI
        if (sleep > 0) {
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
            }
        }
        return response;
    }


    /**
     * Cancel the ongoing network operation.
     */
    public void cancelNetworkOperations() {
        clienteHttp.abort();
    }

    private ServerResponseImpl formalizacionIL(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

        SuccessIL successIL = new SuccessIL();
        ServerResponseImpl response = new ServerResponseImpl(successIL);

        return response;
    }
}
