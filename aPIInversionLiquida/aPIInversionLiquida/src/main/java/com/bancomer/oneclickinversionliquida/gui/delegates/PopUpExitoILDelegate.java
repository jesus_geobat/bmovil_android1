package com.bancomer.oneclickinversionliquida.gui.delegates;

import android.util.Log;

import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.gui.controllers.ClausulasILViewController;
import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;
import com.bancomer.oneclickinversionliquida.models.SuccessIL;

import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by sandradiaz on 02/08/16.
 */
public class PopUpExitoILDelegate implements BaseDelegate {
    public static final long POP_UP_IL_DELEGATE = 10602003105294853L;
    private ClausulasILViewController controller;
    private InitOneClickIL init;
    private Constants.Operacion tipoOperacion;
    private SuccessIL successIL;
    private String montoInv;
    private String tasaInv;
    private String saveIum;

    public PopUpExitoILDelegate(SuccessIL successIL, String montoInv, String tasaInv, String saveIum){
        init = InitOneClickIL.getInstance().getInstance();
        tipoOperacion = Constants.Operacion.oneClicSeguros;
        this.successIL = successIL;
        this.montoInv = montoInv;
        this.tasaInv = tasaInv;
        this.saveIum = saveIum;
    }

    public SuccessIL getSuccessIL() {
        return successIL;
    }

    public String getMontoInv() {
        return montoInv;
    }

    public String getTasaInv() {
        return tasaInv;
    }

    public String getSaveIum() {
        return saveIum;
    }

    @Override
    public void doNetworkOperation(int i, Hashtable<String, ?> hashtable, BaseViewController baseViewController) {
    }

    @Override
    public void analyzeResponse(int i, ServerResponse serverResponse) {

    }

    @Override
    public void performAction(Object o) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
