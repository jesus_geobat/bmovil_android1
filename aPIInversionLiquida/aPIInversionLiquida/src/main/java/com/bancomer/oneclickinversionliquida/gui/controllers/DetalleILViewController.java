package com.bancomer.oneclickinversionliquida.gui.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.adobe.mobile.Config;
import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.commons.Tracker;
import com.bancomer.oneclickinversionliquida.gui.delegates.DetalleILDelegate;
import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.CuentaOrigenController;
import bancomer.api.common.model.HostAccounts;
import bancomer.api.common.timer.TimerController;

/**
 * Created by SDIAZ on 23/06/16.
 */
public class DetalleILViewController extends Activity implements View.OnClickListener,
        SeekBar.OnSeekBarChangeListener{
    private BaseViewController baseViewController;
    private InitOneClickIL init = InitOneClickIL.getInstance();
    public BaseViewsController parentManager;
    private DetalleILDelegate delegate;
    public CuentaOrigenController componenteCtaOrigen;

    private ImageView img_encabezadoDetalle;
    private ImageView btnmeInteresa;
    private ImageView imgBack;
    private TextView txtMonto;
    private TextView txtTasa;
    private TextView linkMonto;
    private LinearLayout lytSeekbar;
    private LinearLayout vistaCtaOrigen;

    // Seekbar
    private Button seekPlus;
    private Button seekMinus;
    private SeekBar seekbar;

    /*  View PAger  */
    ViewPager viewPager;
    PagerAdapter adapter;
    LinearLayout llDots;
    String[] beneficios;
    int posicion;

    ArrayList<String> list= new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate = (DetalleILDelegate) parentManager.getBaseDelegateForKey(DetalleILDelegate.INVERSION_LIQUIDA_DETALLE_DELEGATE);
        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_inversion_liquida_detalle);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        //Adobe
        Config.setContext(this.getApplicationContext());

        list.add(ConstantsIL.DETALLE_IL);
        Tracker.trackState(list);

        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        Tracker.trackEntrada(eventoEntrada);

        delegate.setMontoMin(Integer.parseInt(delegate.getDetailIL().getInitialAmount()));
        delegate.setMontoMax(delegate.getMontoSP());
        delegate.setMontoProgress(Integer.parseInt(delegate.getDetailIL().getInitialAmount()));

        beneficios = new String[] { getResources().getString(R.string.beneficio_uno_detalle_il),
                                    getResources().getString(R.string.beneficio_dos_detalle_il),
                                    getResources().getString(R.string.beneficios_tres_detalle_il)};

        init();
        ShowBeneficio(2000);
    }

    public void init(){
        findViews();
        scaleViews();
        cargarCuentas();
        confScreen();
    }
    private void findViews() {
        img_encabezadoDetalle = (ImageView) findViewById(R.id.img_encabezadoDetalle);
        viewPager = (ViewPager) findViewById(R.id.pager);
        llDots=(LinearLayout) findViewById(R.id.llDots);
        vistaCtaOrigen = (LinearLayout)findViewById(R.id.ofertaILC_cuenta_origen_view);
        lytSeekbar = (LinearLayout) findViewById(R.id.simTCSeekbarLayout);
        btnmeInteresa = (ImageView) findViewById(R.id.btn_interesa_il);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        txtMonto = (TextView) findViewById(R.id.txt_monto_inversion);
        txtTasa = (TextView) findViewById(R.id.txt_tasa_inversion);
        linkMonto = (TextView) findViewById(R.id.link_monto);

        //Seekbar
        seekPlus = (Button) findViewById(R.id.simTCSeekbarPlus);
        seekMinus = (Button) findViewById(R.id.simTCSeekbarMin);
        seekbar = (SeekBar) findViewById(R.id.simTCSeekBar);

        seekbar.setMax(delegate.getMontoMax());
        seekbar.setProgress(delegate.getMontoProgress());
        seekbar.setOnSeekBarChangeListener(this);
        seekbar.refreshDrawableState();

        txtMonto.setText("$" + Tools.formatUserAmount(delegate.getDetailIL().getInitialAmount()));
        txtTasa.setText(Tools.formatToCurrencyAmount(Float.valueOf(delegate.fijarTasa(Integer.valueOf(delegate.getDetailIL().getInitialAmount())))).replace("$","")+"%");

        btnmeInteresa.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        linkMonto.setOnClickListener(this);
        seekPlus.setOnClickListener(this);
        seekMinus.setOnClickListener(this);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int pos) {
                if(pos == beneficios.length) {
                    pos = pos - beneficios.length;
                }
                posicion = pos;
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (i != pos) {
                        ((ImageView) llDots.findViewWithTag(i)).setSelected(false);
                    }
                }
                ((ImageView) llDots.findViewWithTag(pos)).setSelected(true);
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
    }

    private void scaleViews() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(img_encabezadoDetalle);
        guiTools.scale(btnmeInteresa);
        guiTools.scale(imgBack);
        guiTools.scale(llDots);
        guiTools.scale(findViewById(R.id.simTCSeekBar));
        guiTools.scale(findViewById(R.id.simTCSeekbarLayout));
        guiTools.scale(findViewById(R.id.simTCSeekbarPlus));
        guiTools.scale(findViewById(R.id.simTCSeekbarMin));
    }

    public void cargarCuentas(){
        ArrayList<HostAccounts> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        componenteCtaOrigen = new CuentaOrigenController(this, params);
        componenteCtaOrigen.getTituloComponenteCtaOrigen().setText("");
        componenteCtaOrigen.getTituloComponenteCtaOrigen().setVisibility(View.GONE);
        componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
        componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
        componenteCtaOrigen.init();
        componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.oneClicSeguros);
        vistaCtaOrigen.addView(componenteCtaOrigen);
    }

    public void confScreen(){

        String txtlinkmonto =linkMonto.getText().toString();
        SpannableString splinkmonto = new SpannableString(txtlinkmonto);
        splinkmonto.setSpan(new UnderlineSpan(), 0, txtlinkmonto.length(), 0);
        linkMonto.setText(splinkmonto);

        /** View Pager **/
        adapter = new ViewPagerAdapter(this, beneficios);
        viewPager.setAdapter(adapter);

        for (int i = 0; i < adapter.getCount(); i++)
        {
            ImageButton imgDot = new ImageButton(this);
            imgDot.setTag(i);
            imgDot.setImageResource(R.drawable.dot_selector);
            imgDot.setBackgroundResource(0);
            imgDot.setPadding(5, 5, 5, 5);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20, 20);
            imgDot.setLayoutParams(params);
            if(i == 0)
                imgDot.setSelected(true);

            llDots.addView(imgDot);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == btnmeInteresa){
            delegate.setMontoIL(txtMonto.getText().toString());
            delegate.setTasaIL(txtTasa.getText().toString().replace("$",""));
            delegate.realizaOperacionGats();
            onDestroy();
        }else if(v == linkMonto){
            lytSeekbar.setVisibility(View.VISIBLE);
            linkMonto.setVisibility(View.GONE);
        }else if(v == seekMinus){
            seekbar = (SeekBar)findViewById(R.id.simTCSeekBar);
            if(seekbar.getProgress() < seekbar.getMax()){
                Integer val = seekbar.getMax() - seekbar.getProgress();
                if(val>=1000){
                    seekbar.setProgress(seekbar.getProgress()+1000);
                }else{
                    seekbar.setProgress(seekbar.getProgress()+val);
                }
                if(seekbar.getProgress()<=delegate.getMontoMax()){
                }
            }
        }else if(v == seekPlus){
            seekbar = (SeekBar)findViewById(R.id.simTCSeekBar);
            if(seekbar.getProgress() > delegate.getMontoMin()){
                if((seekbar.getProgress() >= 1000)&&(delegate.getMontoMin() <= (seekbar.getProgress()-1000))){
                    seekbar.setProgress(seekbar.getProgress()-1000);
                }else{
                    seekbar.setProgress(delegate.getMontoMin());
                }
                if(seekbar.getProgress()>=delegate.getMontoMin()){
                }
            }
        }else if(v == imgBack){
            onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!init.getParentManager().isActivityChanging()) {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (DialogInterface.BUTTON_POSITIVE == which) {
                    init.getParentManager().setActivityChanging(true);
                    ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                    init.getParentManager().resetRootViewController();
                    init.resetInstance();
                }else{
                    dialog.dismiss();
                }
            }
        };
        init.getBaseViewController().showYesNoAlert(DetalleILViewController.this, R.string.salir_inversion_liquida, listener);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        txtMonto.setText("$" + Tools.formatUserAmount(String.valueOf(seekbar.getProgress())));
        txtTasa.setText(Tools.formatToCurrencyAmount(Float.valueOf(delegate.fijarTasa(Integer.valueOf(String.valueOf(seekbar.getProgress()))))).replace("$","")+"%");

        if(seekbar.getProgress() == delegate.getMontoMax()){
            seekbar.setProgress(seekbar.getProgress());
        }else{
            seekbar.setProgress(delegate.getAmountThFormatted(seekbar.getProgress()));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if(seekbar.getProgress()<delegate.getMontoMin()){
            seekbar.setProgress(delegate.getMontoMin());
        }
    }

    public SeekBar getSeekbar() {
        return seekbar;
    }

    public void setSeekbar(SeekBar seekbar) {
        this.seekbar = seekbar;
    }

    public void ShowBeneficio(int milisegundos) {
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {
                // acciones que se ejecutan tras los milisegundos
                posicion = posicion+1;
                if(posicion == beneficios.length) {
                    posicion = posicion - beneficios.length;
                }
                viewPager.setCurrentItem(posicion);
                ShowBeneficio(5000);
            }
        }, milisegundos);
    }

    public class ViewPagerAdapter extends PagerAdapter
    {
        Context context;

        String[] beneficios;
        LayoutInflater inflater;

        public ViewPagerAdapter(Context context, String[] beneficios)
        {
            this.context = context;
            this.beneficios = beneficios;
        }

        @Override
        public int getCount()
        {
            return beneficios.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object)
        {
            return view == ((LinearLayout) object);
        }

        public float getPageWidth(int position)
        {
            return 1f;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {

            TextView txtBeneficioUno;

            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.api_inversion_liquida_beneficios, container,false);
            // Locate the ImageView in viewpager_item.xml
            txtBeneficioUno = (TextView) itemView.findViewById(R.id.txtBeneficioUno);
            // Capture position and set to the ImageView
            txtBeneficioUno.setText(beneficios[position]);
            // Add viewpager_item.xml to ViewPager
            ((ViewPager) container).addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            // Remove viewpager_item.xml from ViewPager
            ((ViewPager) container).removeView((LinearLayout) object);
        }
    }
}
