package com.bancomer.oneclickinversionliquida.gui.controllers;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.method.PasswordTransformationMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adobe.mobile.Config;
import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.commons.Tracker;
import com.bancomer.oneclickinversionliquida.gui.delegates.ClausulasILDelegate;
import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;
import suitebancomercoms.classes.common.PropertiesManager;


/**
 * Created by SDIAZ on 05/07/16.
 */
public class ClausulasILViewController extends Activity implements View.OnClickListener, ViewPager.OnPageChangeListener{
    private BaseViewController baseViewController;
    private InitOneClickIL init = InitOneClickIL.getInstance();
    private BaseViewsController parentManager;
    private ClausulasILDelegate delegate;

    private CheckBox cbAutorizo;
    private CheckBox cbGat;
    private CheckBox cbBeneficiarios;
    private CheckBox cbTerminos;

    private TextView txtEncabezado;
    private TextView txtDesGat;
    private TextView txtGatNom;
    private TextView txtGatReal;
    private TextView txtDesBeneficiarios;
    private TextView txtAceptoTyCuno;
    private TextView txtAceptoTyCdos;
    private TextView txtAceptoTyCtres;
    private TextView txtTerminos;
    private TextView txtAutorizoUno;
    private TextView txtAutorizoDos;
    private TextView txtGat;
    private TextView txtBeneficiarios;

    private ImageView imgBack;
    private ImageView img_encabezadoDetalle;
    private ImageButton btnConfirmar;
    private ImageButton btnLlamame;
    private TextView montoInv;
    private LinearLayout lytDesGat;

    /**
     * TOKEN
     */
    private LinearLayout layout_avisotoken;
    private LinearLayout contenedorContrasena;
    private LinearLayout contenedorNIP;
    private LinearLayout contenedorASM;
    private LinearLayout contenedorCVV;
    private LinearLayout contenedorCampoTarjeta;

    private TextView campoContrasena;
    private TextView campoNIP;
    private TextView campoASM;
    private TextView campoCVV;
    private TextView campoTarjeta;

    private EditText contrasena;
    private EditText nip;
    private EditText asm;
    private EditText cvv;
    private EditText tarjeta;

    private TextView instruccionesContrasena;
    private TextView instruccionesNIP;
    private TextView instruccionesASM;
    private TextView instruccionesCVV;
    private TextView instruccionesTarjeta;

    ArrayList<String> list= new ArrayList<String>();
    boolean[] checks = new boolean[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate = (ClausulasILDelegate) parentManager.getBaseDelegateForKey(ClausulasILDelegate.CLAUSULAS_IL_DELEGATE);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_inversion_liquida_clausulas);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        //Adobe
        Config.setContext(this.getApplicationContext());

        list.add(ConstantsIL.CLAUSULAS_INEVRSION_LIQUIDA);
        Tracker.trackState(list);

        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        Tracker.trackEntrada(eventoEntrada);

        init();
    }

    public void init(){
        findViews();
        scaleViews();
        setSecurityComponents();
        confStatusChecks();
        confScreen();
    }
    private void findViews() {
        img_encabezadoDetalle = (ImageView) findViewById(R.id.img_encabezadoDetalle);
        txtEncabezado = (TextView) findViewById(R.id.txtEncabezado);
        btnConfirmar = (ImageButton) findViewById(R.id.btn_confirmacion_il);
        cbAutorizo = (CheckBox) findViewById(R.id.cbAutorizo);
        cbGat = (CheckBox) findViewById(R.id.cbGat);
        cbBeneficiarios = (CheckBox) findViewById(R.id.cbBeneficiarios);
        cbTerminos = (CheckBox) findViewById(R.id.cbTerminos);
        txtDesGat = (TextView) findViewById(R.id.txtDesGat);
        txtGatNom = (TextView) findViewById(R.id.txtGatNom);
        txtGatReal = (TextView) findViewById(R.id.txtGatReal);
        txtDesBeneficiarios = (TextView) findViewById(R.id.txtDesBeneficirios);
        txtAceptoTyCuno = (TextView) findViewById(R.id.txtAceptoTyCuno);
        txtAceptoTyCdos = (TextView) findViewById(R.id.txtAceptoTyCdos);
        txtAceptoTyCtres = (TextView) findViewById(R.id.txtAceptoTyCtres);
        txtTerminos = (TextView) findViewById(R.id.txtTerminos);
        txtAutorizoUno = (TextView) findViewById(R.id.txtAutorizoUno);
        txtAutorizoDos = (TextView) findViewById(R.id.txtAutorizoDos);
        txtGat = (TextView) findViewById(R.id.txtGat);
        txtBeneficiarios = (TextView) findViewById(R.id.txtBeneficiarios);
        montoInv = (TextView) findViewById(R.id.txtAceptoAutorizo);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        lytDesGat = (LinearLayout) findViewById(R.id.lytDesGat);

        txtTerminos.setOnClickListener(this);
        btnConfirmar.setOnClickListener(this);
        imgBack.setOnClickListener(this);

        /** Token **/
        contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_confirmacion_contrasena_layout);
        contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_confirmacion_nip_layout);
        contenedorASM 			= (LinearLayout)findViewById(R.id.campo_confirmacion_asm_layout);
        contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_confirmacion_cvv_layout);

        contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
        nip 					= (EditText)contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
        asm						= (EditText)contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
        cvv						= (EditText)contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);

        campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
        campoNIP				= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_label);
        campoASM				= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_label);
        campoCVV				= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_label);

        instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
        instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
        instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
        instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);

        contenedorCampoTarjeta  = (LinearLayout) findViewById(R.id.campo_confirmacion_campotarjeta_layout);
        tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
        campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
        instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);

    }

    private void scaleViews() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(img_encabezadoDetalle);
        guiTools.scale(btnConfirmar);
        guiTools.scale(btnLlamame);
        guiTools.scale(imgBack);

        guiTools.scale(contenedorContrasena);
        guiTools.scale(contenedorNIP);
        guiTools.scale(contenedorASM);
        guiTools.scale(contenedorCVV);

        guiTools.scale(contrasena, true);
        guiTools.scale(nip, true);
        guiTools.scale(asm, true);
        guiTools.scale(cvv, true);

        guiTools.scale(campoContrasena);
        guiTools.scale(campoNIP);
        guiTools.scale(campoASM);
        guiTools.scale(campoCVV);

        guiTools.scale(instruccionesContrasena, true);
        guiTools.scale(instruccionesNIP, true);
        guiTools.scale(instruccionesASM, true);
        guiTools.scale(instruccionesCVV, true);

        guiTools.scale(contenedorCampoTarjeta);
        guiTools.scale(tarjeta, true);
        guiTools.scale(campoTarjeta, true);
        guiTools.scale(instruccionesTarjeta, true);
    }

    public void confStatusChecks(){
        if(delegate.getStatusCheck() != null){
            checks = delegate.getStatusCheck();
            for(int i=0;i<checks.length;i++){
                switch (i){
                    case 0:
                        if(checks[i]) {
                            cbAutorizo.setChecked(true);
                            cbAutorizo.setClickable(false);
                        }else
                            cbAutorizo.setChecked(false);
                        break;
                    case 1:
                        if(checks[i]) {
                            cbGat.setChecked(true);
                            cbGat.setClickable(false);
                        }else
                            cbGat.setChecked(false);
                        break;
                    case 2:
                        if(checks[i]) {
                            cbBeneficiarios.setChecked(true);
                            cbBeneficiarios.setClickable(false);
                        }else
                            cbBeneficiarios.setChecked(false);
                        break;
                    case 3:
                        if(checks[i]) {
                            cbTerminos.setChecked(true);
                            cbTerminos.setClickable(false);
                        }else
                            cbTerminos.setChecked(false);
                        break;
                }
            }
        }else{
            cbAutorizo.setChecked(false);
            cbGat.setChecked(false);
            cbBeneficiarios.setChecked(false);
            cbTerminos.setChecked(false);
            checks[0] = false;
            checks[1] = false;
            checks[2] = false;
            checks[3] = false;
        }
    }

    public void confScreen(){

        String txtlinkTerminos =txtTerminos.getText().toString();
        SpannableString splinkTerminos = new SpannableString(txtlinkTerminos);
        splinkTerminos.setSpan(new UnderlineSpan(), 0, txtlinkTerminos.length(), 0);
        txtTerminos.setText(splinkTerminos);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/opensanslight.ttf");
        txtTerminos.setTypeface(typeface);
        txtAceptoTyCuno.setTypeface(typeface);
        txtAceptoTyCdos.setTypeface(typeface);
        txtAceptoTyCtres.setTypeface(typeface);
        txtAutorizoUno.setTypeface(typeface);
        txtAutorizoDos.setTypeface(typeface);
        txtGat.setTypeface(typeface);
        txtBeneficiarios.setTypeface(typeface);

        campoASM.setTypeface(typeface);
        campoContrasena.setTypeface(typeface);
        campoCVV.setTypeface(typeface);
        campoNIP.setTypeface(typeface);
        campoTarjeta.setTypeface(typeface);

        instruccionesASM.setTypeface(typeface);
        instruccionesContrasena.setTypeface(typeface);
        instruccionesCVV.setTypeface(typeface);
        instruccionesNIP.setTypeface(typeface);
        instruccionesTarjeta.setTypeface(typeface);

        txtEncabezado.setVisibility(View.GONE);
        txtEncabezado.setText("Inicia tu contratación");
        montoInv.setText(delegate.getMontInv());
        img_encabezadoDetalle.setImageResource(R.drawable.il_contratacion);

        Calendar calendar = Calendar.getInstance();
        String currentDate = Tools.getCurrentDate(); //dd/MM/yyyy
        String[] date = currentDate.split("/");
        calendar.set(Integer.valueOf(date[2]), Integer.valueOf(date[1]) - 1, Integer.valueOf(date[0]));
        String lastDay = String.valueOf(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));


        txtGatNom.setText(delegate.getGatIL().getNominalGat());
        txtGatReal.setText(delegate.getGatIL().getRealGat());
        String txtGatsCompleto = txtDesGat.getText() + " " + currentDate + ".\nVigencia de la oferta " + lastDay + "/" + date[1] + "/" + date[2] + ".";

        txtDesGat.setText(txtGatsCompleto);
        contrasena.setTransformationMethod(new PasswordTransformationMethod());
        nip.setTransformationMethod(new PasswordTransformationMethod());
        asm.setTransformationMethod(new PasswordTransformationMethod());
        cvv.setTransformationMethod(new PasswordTransformationMethod());
        tarjeta.setTransformationMethod(new PasswordTransformationMethod());
    }

    @Override
    public void onClick(View v) {
        if(v == btnConfirmar){
            if(aceptarClausulas()){
                if(delegate.enviaPeticionOperacion())
                {
                    init.getParentManager().setActivityChanging(true);
                }else{
                    init.getBaseViewController().showInformationAlert(this, R.string.ingresar_clave_acceso);
                }
            }
        }else if (v == txtTerminos){
            delegate.setStatusCheck(checks);
            init.getParentManager().setActivityChanging(true);
            delegate.realizaOperacionTerminosYCondiciones();
        }else if(v == imgBack){
            onBackPressed();
        }
    }

    public void onCheckboxClick(View v) {
        if (v == cbAutorizo) {
            if (cbAutorizo.isChecked()) {
                cbAutorizo.setClickable(false);
                lytDesGat.setVisibility(View.VISIBLE);
                txtDesGat.setVisibility(View.VISIBLE);
            } else{
                cbAutorizo.setChecked(false);
            }
            checks[0]=true;
        }if (v == cbGat) {
            if (cbAutorizo.isChecked()) {
                cbGat.setClickable(false);
                lytDesGat.setVisibility(View.GONE);
                txtDesGat.setVisibility(View.GONE);
                txtDesBeneficiarios.setVisibility(View.VISIBLE);
            } else{
                init.getBaseViewController().showInformationAlert(ClausulasILViewController.this, R.string.aceptar_autorizo);
                cbGat.setChecked(false);
            }
            checks[1]=true;
        }if (v == cbBeneficiarios) {
            if (cbGat.isChecked()) {
                cbBeneficiarios.setClickable(false);
                txtDesBeneficiarios.setVisibility(View.GONE);
            } else{
                init.getBaseViewController().showInformationAlert(delegate.getController(), R.string.aceptar_Gat);
                cbBeneficiarios.setChecked(false);
            }
            checks[2]=true;
        } else if (v == cbTerminos) {
            if(cbBeneficiarios.isChecked()) {
                if (cbTerminos.isChecked()) {
                    cbTerminos.setClickable(false);
                }
            }else{
                init.getBaseViewController().showInformationAlert(ClausulasILViewController.this, R.string.aceptar_beneficiarios);
                cbTerminos.setChecked(false);
            }
            checks[3]=true;
        }
    }

    public boolean aceptarClausulas(){

        if(!cbAutorizo.isChecked() || !cbGat.isChecked() || !cbBeneficiarios.isChecked() || !cbTerminos.isChecked()){
            init.getBaseViewController().showInformationAlert(this, R.string.aceptar_autorizacion);
            return false;
        }
        return true;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!init.getParentManager().isActivityChanging()) {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (DialogInterface.BUTTON_POSITIVE == which) {
                    init.getParentManager().setActivityChanging(true);
                    ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                    init.getParentManager().resetRootViewController();
                    init.resetInstance();
                }else{
                    dialog.dismiss();
                }

            }
        };
        init.getBaseViewController().showYesNoAlert(ClausulasILViewController.this, R.string.salir_inversion_liquida, listener);
    }

    private void setSecurityComponents() {
        mostrarContrasena(false);//delegate.mostrarContrasenia());
        mostrarNIP(false);//delegate.mostrarNIP());
        mostrarASM(delegate.tokenAMostrar());
        mostrarCVV(false);//delegate.mostrarCVV());
        mostrarCampoTarjeta(false);//delegate.mostrarCampoTarjeta());

        LinearLayout parentContainer = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);

        if (contenedorContrasena.getVisibility() == View.GONE &&
                contenedorNIP.getVisibility() == View.GONE &&
                contenedorASM.getVisibility() == View.GONE &&
                contenedorCVV.getVisibility() == View.GONE &&
                contenedorCampoTarjeta.getVisibility() == View.GONE) {
            parentContainer.setBackgroundColor(0);
            parentContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        }


        if (contenedorContrasena.getVisibility() == View.VISIBLE ||
                contenedorNIP.getVisibility() == View.VISIBLE ||
                contenedorASM.getVisibility() == View.VISIBLE ||
                contenedorCVV.getVisibility() == View.VISIBLE ||
                contenedorCampoTarjeta.getVisibility() == View.VISIBLE) {
        }
    }


    public void mostrarCVV(boolean visibility){
        contenedorCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
        campoCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
        cvv.setVisibility(visibility ? View.VISIBLE : View.GONE);
        instruccionesCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);

        if (visibility) {
            campoCVV.setText(delegate.getEtiquetaCampoCVV());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
            cvv.setFilters(userFilterArray);
            String instrucciones = delegate.getTextoAyudaCVV();
            instruccionesCVV.setText(instrucciones);
            instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(nip);
            cambiarAccionTexto(asm);
            cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    public void mostrarContrasena(boolean visibility){
        contenedorContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        campoContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
            campoContrasena.setText(delegate.getEtiquetaCampoContrasenia());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
            contrasena.setFilters(userFilterArray);
            contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
        instruccionesContrasena.setVisibility(View.GONE);
    }


    public void mostrarNIP(boolean visibility){
        contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
        campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
        nip.setVisibility(visibility ? View.VISIBLE:View.GONE);
        if (visibility) {
            campoNIP.setText(delegate.getEtiquetaCampoNip());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
            nip.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(tarjeta);
            nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = delegate.getTextoAyudaNIP();
            if (instrucciones.equals("")) {
                instruccionesNIP.setVisibility(View.GONE);
            } else {
                instruccionesNIP.setVisibility(View.VISIBLE);
                instruccionesNIP.setText(instrucciones);
            }
        } else {
            nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }


    public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){
        switch(tipoOTP) {
            case ninguno:
                contenedorASM.setVisibility(View.GONE);
                campoASM.setVisibility(View.GONE);
                asm.setVisibility(View.GONE);
                asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
                break;
            case codigo:
            case registro:
                contenedorASM.setVisibility(View.VISIBLE);
                campoASM.setVisibility(View.VISIBLE);
                asm.setVisibility(View.VISIBLE);
                InputFilter[] userFilterArray = new InputFilter[1];
                userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
                asm.setFilters(userFilterArray);
                cambiarAccionTexto(contrasena);
                cambiarAccionTexto(tarjeta);
                cambiarAccionTexto(nip);
                asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
                break;
        }

        Constants.TipoInstrumento tipoInstrumento = delegate.getTipoInstrumentoSeguridad();

        switch (tipoInstrumento) {
            case OCRA:
                campoASM.setText(delegate.getEtiquetaCampoOCRA());
                asm.setTransformationMethod(null);
                break;
            case DP270:
                campoASM.setText(delegate.getEtiquetaCampoDP270());
                asm.setTransformationMethod(null);
                break;
            case SoftToken:
                if(init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
                    asm.setText(Constants.DUMMY_OTP);
                    asm.setEnabled(false);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
                } else {
                    asm.setText("");
                    asm.setEnabled(true);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
                    asm.setTransformationMethod(null);
                }

                break;
            default:
                break;
        }
        String instrucciones = delegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento,(init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()),delegate.tokenAMostrar());

        if (instrucciones.equals("")) {
            instruccionesASM.setVisibility(View.GONE);
        } else {
            instruccionesASM.setVisibility(View.VISIBLE);
            instruccionesASM.setText(instrucciones);
        }
    }


    private void mostrarCampoTarjeta(boolean visibility) {
        contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
        campoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
        tarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
        if (visibility) {
            //layout_avisotoken.setVisibility(View.VISIBLE);
            campoTarjeta.setText(delegate.getEtiquetaCampoTarjeta());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(5);
            tarjeta.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = delegate.getTextoAyudaTarjeta();
            if (instrucciones.equals("")) {
                instruccionesTarjeta.setVisibility(View.GONE);
            } else {
                instruccionesTarjeta.setVisibility(View.VISIBLE);
                instruccionesTarjeta.setText(instrucciones);
            }
        } else {
            tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    private void cambiarAccionTexto(EditText campo) {
        if (campo.getVisibility() == View.VISIBLE) {
            campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        }
    }

    public String pideContrasena() {
        if (contrasena.getVisibility() == View.GONE) {
            return "";
        } else {
            return contrasena.getText().toString();
        }
    }

    public String pideNIP() {
        if (nip.getVisibility() == View.GONE) {
            return "";
        } else {
            return nip.getText().toString();
        }
    }

    public String pideASM() {
        if (asm.getVisibility() == View.GONE) {
            return "";
        } else {
            return asm.getText().toString();
        }
    }

    public String pideCVV() {
        if (cvv.getVisibility() == View.GONE) {
            return "";
        } else {
            return cvv.getText().toString();
        }
    }

    public String pideTarjeta(){
        if (tarjeta.getVisibility() == View.GONE) {
            return "";
        }else
            return tarjeta.getText().toString();
    }
}
