package com.bancomer.oneclickinversionliquida.models;

import android.util.Log;

import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.io.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by sandradiaz on 27/07/16.
 */
public class DetailIL implements ParsingHandler{
    private String initialAmount;
    private String initialRate;
    private String accountPerSol;
    private RatesIL[] listRates;
    private String cr;
    private String managementUnit;

    public DetailIL() {
        super();
    }

    public DetailIL(String initialAmount, String initialRate, String accountPerSol, RatesIL[] listRates, String cr, String managementUnit){
        this.initialAmount = initialAmount;
        this.initialRate = initialRate;
        this.accountPerSol = accountPerSol;
        this.listRates = listRates;
        this.cr =cr;
        this.managementUnit = managementUnit;
    }

    public String getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(String initialAmount) {
        this.initialAmount = initialAmount;
    }

    public String getInitialRate() {
        return initialRate;
    }

    public void setInitialRate(String initialRate) {
        this.initialRate = initialRate;
    }

    public String getAccountPerSol() {
        return accountPerSol;
    }

    public void setAccountPerSol(String accountPerSol) {
        this.accountPerSol = accountPerSol;
    }

    public RatesIL[] getListRates() {
        return listRates;
    }

    public void setListRates(RatesIL[] listRates) {
        this.listRates = listRates;
    }

    public String getCr() {
        return cr;
    }

    public void setCr(String cr) {
        this.cr = cr;
    }

    public String getManagementUnit() {
        return managementUnit;
    }

    public void setManagementUnit(String managementUnit) {
        this.managementUnit = managementUnit;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
    }

    public void process(String parser) throws IOException, ParsingException, JSONException {

        JSONObject obj = new JSONObject(parser);

        setInitialAmount((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_INITIAL_AMOUNT)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_INITIAL_AMOUNT) : "");
        setInitialRate((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_INITIAL_RATE))? obj.getJSONObject("response").getString(ConstantsIL.TAG_INITIAL_RATE):"");
        setAccountPerSol((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_ACCOUNT_PER_SOL)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_ACCOUNT_PER_SOL) : "");

        try {
            JSONArray arrRatesJson = new JSONArray(obj.getJSONObject("response").getString(ConstantsIL.TAG_LIST_RATES));
            ArrayList<RatesIL> arrRates = new ArrayList<RatesIL>();
            for (int i = 0; i < arrRatesJson.length(); i++) {
                JSONObject jsonPromocion = (JSONObject) arrRatesJson.get(i);
                RatesIL rate = new RatesIL();

                rate.setRate(jsonPromocion.getString(ConstantsIL.TAG_RATE));
                rate.setMinimunAmount(jsonPromocion.getString(ConstantsIL.TAG_MINIMUN_AMOUNT));

                arrRates.add(rate);
            }
            this.listRates = arrRates.toArray(new RatesIL[arrRates.size()]);

            setCr((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_CR)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_CR) : "");
            setManagementUnit((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_MAGNAGEMENT_UNIT)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_MAGNAGEMENT_UNIT) : "");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            if(Server.ALLOW_LOG) e.printStackTrace();
        }
    }
}
