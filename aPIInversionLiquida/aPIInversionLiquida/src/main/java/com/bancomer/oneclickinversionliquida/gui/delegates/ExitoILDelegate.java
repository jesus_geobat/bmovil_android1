package com.bancomer.oneclickinversionliquida.gui.delegates;

import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.gui.controllers.ExitoILViewController;
import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;
import com.bancomer.oneclickinversionliquida.io.Server;
import com.bancomer.oneclickinversionliquida.models.ConsultarContrato;
import com.bancomer.oneclickinversionliquida.models.SuccessIL;

import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by SDIAZ on 05/07/16.
 */
public class ExitoILDelegate implements BaseDelegate {

    public static final long EXITO_IL_DELEGATE = 10602003562340813L;
    private ExitoILViewController controller;
    private InitOneClickIL init;
    private SuccessIL successIL;
    private String opWeb;
    private String montoInv;
    private String tasaInv;
    private String saveIum;

    public ExitoILDelegate(String montoInv, String tasaInv, String saveIum, SuccessIL successIL){
        init = InitOneClickIL.getInstance().getInstance();
        this.successIL = successIL;
        this.montoInv = montoInv;
        this.tasaInv = tasaInv;
        this.saveIum = saveIum;
    }

    public SuccessIL getSuccessIL() {
        return successIL;
    }

    public String getMontoInv() {
        return montoInv;
    }

    public void setMontoInv(String montoInv) {
        this.montoInv = montoInv;
    }

    public String getTasaInv() {
        return tasaInv;
    }

    public void setTasaInv(String tasaInv) {
        this.tasaInv = tasaInv;
    }

    @Override
    public void doNetworkOperation(int i, Hashtable<String, ?> hashtable, BaseViewController baseViewController) {

    }

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller,Boolean isJson, ParsingHandler handler){//, String dateIUM) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false,isJson,handler);//, dateIUM);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (operationId == Server.CONSULTA_TERMINOS_CONDICIONES_IL){

            ConsultarContrato contrato = (ConsultarContrato)response.getResponse();
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                if(opWeb.equalsIgnoreCase("Contrato")) {
                    init.showContrato(getMontoInv(), getTasaInv(), getSuccessIL(), saveIum, contrato.getFormatoHTML());
                }
                opWeb = "";
            }else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
                init.getBaseViewController().showInformationAlert(controller, response.getMessageText());
            }
        }
    }

    @Override
    public void performAction(Object o) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

    public void realizaOperacionTerminosYCondiciones(){
        init = InitOneClickIL.getInstance().getInstance();
        opWeb = "Contrato";

        Hashtable<String, String> params = new Hashtable<String, String>();
        boolean banContrato = true;
        params.put(ConstantsIL.TAG_ACCOUNT_NUMBER, getSuccessIL().getNumContract());
        params.put(ConstantsIL.TAG_CVE_CAMP, "");
        params.put(ConstantsIL.TAG_CREATE_CONTRACT, String.valueOf(banContrato).replace("\"", ""));
        params.put(ConstantsIL.TAG_NUM_CONTRACT, ConstantsIL.FOLIO_CONTRATO_IL);

        params.put(ConstantsIL.IUM_TAG, init.getSession().getIum());

        doNetworkOperation(Server.CONSULTA_TERMINOS_CONDICIONES_IL, params, init.getBaseViewController(),false, new ConsultarContrato());//,IUM);
    }
}
