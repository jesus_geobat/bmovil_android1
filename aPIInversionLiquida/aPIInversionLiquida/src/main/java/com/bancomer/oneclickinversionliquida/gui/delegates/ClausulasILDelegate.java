package com.bancomer.oneclickinversionliquida.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import com.bancomer.base.SuiteApp;
import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.commons.ILSession;
import com.bancomer.oneclickinversionliquida.gui.controllers.ClausulasILViewController;
import com.bancomer.oneclickinversionliquida.gui.controllers.R;
import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;
import com.bancomer.oneclickinversionliquida.io.AuxConectionFactory;
import com.bancomer.oneclickinversionliquida.io.Server;
import com.bancomer.oneclickinversionliquida.models.ActualizarCuentasResult;
import com.bancomer.oneclickinversionliquida.models.ConsultarContrato;
import com.bancomer.oneclickinversionliquida.models.GatIL;
import com.bancomer.oneclickinversionliquida.models.SuccessIL;

import java.util.Hashtable;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.gui.delegates.SecurityComponentDelegate;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtp;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtpBmovil;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;

/**
 * Created by SDIAZ on 05/07/16.
 */
public class ClausulasILDelegate extends SecurityComponentDelegate implements BaseDelegate{

    public static final long CLAUSULAS_IL_DELEGATE = 10602003105230813L;
    private ClausulasILViewController controller;
    private InitOneClickIL init = InitOneClickIL.getInstance().getInstance();
    private Constants.Operacion tipoOperacion;
    private Constants.TipoInstrumento tipoInstrumentoSeguridad;
    private String opWeb;

    private String montInv;
    private String tasaInv;
    private String cuentaSP;
    private String cr;
    private boolean[] statusCheck;
    private GatIL gatIL;
    private String managementUnit;
    private SuccessIL successIL;
    private String message;

    private String token;
    private String contrasena;
    private String nip;
    private String cvv;
    private String tarjeta;

    private boolean showExito = false;
    private boolean errorContratacion = false;
    String saveIum = init.getSession().getIum();


    public ClausulasILDelegate(String montoInv, String tasaInv, String cuentaSP, String cr, boolean[] statusChecks, GatIL gatIL, String managementUnit){
        this.montInv = montoInv;
        this.tasaInv = tasaInv;
        this.cuentaSP = cuentaSP;
        this.cr = cr;
        this.statusCheck = statusChecks;
        this.gatIL = gatIL;
        this.managementUnit = managementUnit;
        init = InitOneClickIL.getInstance().getInstance();
        tipoOperacion = Constants.Operacion.oneClicSeguros;

        String instrumento = init.getSession().getSecurityInstrument();

        if (instrumento.equals(Constants.IS_TYPE_DP270)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
        } else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
        } else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)||instrumento.equals(Constants.TYPE_SOFTOKEN.S2.value)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
        } else {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
        }

        setContext(this.controller);
    }

    public void setController(ClausulasILViewController controller) {
        this.controller = controller;
        setContext(controller);
    }

    public ClausulasILViewController getController() {
        return controller;
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
    }

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller,Boolean isJson, ParsingHandler handler){//, String dateIUM) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false,isJson,handler);//, dateIUM);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (operationId == Server.CONSULTA_CONTRATACION_IL){

            successIL = (SuccessIL)response.getResponse();

            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)
            {
                init.getSession().setPromocion(successIL.getPromociones());
                message = "Tu nueva cuenta de Inversión Líquida " + Tools.hideAccountNumber(successIL.getNumContract()) + " ya está disponible, para disfrutar de sus beneficios realiza un traspaso a partir de $25,000.00 desde tu cuenta de Solución Personal " + Tools.hideAccountNumber(successIL.getPersonalSolution());
                realizaOperacionActualizarCuentas();
            }
            else if(response.getStatus() == ServerResponse.OPERATION_ERROR || response.getStatus() == ServerResponse.OPERATION_WARNING)
            {
                try {

                    if(!successIL.getPM().equalsIgnoreCase("SI")){
                        init.getBaseViewController().showErrorMessage(controller,response.getMessageText());
                    }else
                    {
                        init.getSession().setPromocion(successIL.getPromociones());

                        DialogInterface.OnClickListener clickListener=new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                init.getParentManager().setActivityChanging(true);
                                ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                                init.resetInstance();
                            }
                        };
                        init.getBaseViewController().showInformationAlert(controller, response.getMessageText(), clickListener);

                    }
                }catch(NullPointerException ex)
                {
                    init.getBaseViewController().showErrorMessage(controller,response.getMessageText());
                }
                if(response.getMessageText().contains("INVL001")) {
                    errorContratacion = true;
                    realizaOperacionActualizarCuentas();
                }
            }
        }else if (operationId == Server.CONSULTA_TERMINOS_CONDICIONES_IL){

            ConsultarContrato contrato = (ConsultarContrato)response.getResponse();
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                if(opWeb.equalsIgnoreCase("Terminos")) {
                    init.showTerminosYCondiciones(getMontInv(), getTasaInv(), cuentaSP, cr, statusCheck, gatIL, managementUnit, contrato.getFormatoHTML());//, getMontInv(), getTasaInv());
                }
                opWeb = "";
            }else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
                init.getBaseViewController().showInformationAlert(controller, response.getMessageText());
            }
        } else if(operationId == Server.TRANSFERENCIAS_IL){
            successIL = (SuccessIL)response.getResponse();
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
                showExito = true;
                realizaOperacionActualizarCuentas();
            }
            else if (response.getStatus() == ServerResponse.OPERATION_ERROR || response.getStatus() == ServerResponse.OPERATION_WARNING) {
                    init.getBaseViewController().showInformationAlert(controller, message);
            }
        }else if(operationId == Server.ACTUALIZAR_CUENTAS){
            if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                ActualizarCuentasResult result = (ActualizarCuentasResult) response.getResponse();
                actualizacionDeCuentasExitosa(result.getAsuntos());

                if(!errorContratacion) {
                    if (showExito) {
                        init.showPopUpExito(getMontInv(), getTasaInv(), saveIum, successIL);
                    } else {
                        realizaOperacionTranferencia(successIL);
                    }
                }
            }
            else if (response.getStatus() == ServerResponse.OPERATION_WARNING || response.getStatus() == ServerResponse.OPERATION_ERROR) {
                if(!showExito) {
                    init.getBaseViewController().showInformationAlert(controller, message);
                }else{
                    init.showPopUpExito(getMontInv(), getTasaInv(), saveIum, successIL);
                }
            }
        }
    }

    @Override
    public void performAction(Object o) {
        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put(ConstantsIL.TAG_ACCOUNT_OPENING, this.cuentaSP);
        params.put(ConstantsIL.TAG_AMOUNT, getMontInv().replace("$","").replace(",", "").replace(".00", ""));
        params.put(ConstantsIL.TAG_RATE, getTasaInv().replace("$", "").replace("%", ""));
        params.put(ConstantsIL.TAG_CELLPHONE, init.getSession().getUsername());
        params.put(ConstantsIL.TAG_CR, this.cr);
        params.put(ConstantsIL.TAG_MAGNAGEMENT_UNIT, this.managementUnit);
        params.put(ConstantsIL.TAG_NOMINAL_GAT, getGatIL().getNominalGat());
        params.put(ConstantsIL.TAG_REAL_GAT, getGatIL().getRealGat());

        params.put(ConstantsIL.IUM_TAG, init.getSession().getIum());
        params.put(ConstantsIL.TAG_CODE_OTP, String.valueOf((token != null) ? token : ""));
        params.put(ConstantsIL.TAG_CVE_CAMP, init.getOfertaSeguros().getCveCamp().replace("\"", ""));

        doNetworkOperation(Server.CONSULTA_CONTRATACION_IL, params, InitOneClickIL.getInstance().getBaseViewController(), false, new SuccessIL());//, IUM);
    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

    public String getMontInv() {
        return montInv;
    }

    public void setMontInv(String montInv) {
        this.montInv = montInv;
    }

    public String getTasaInv() {
        return tasaInv;
    }

    public void setTasaInv(String tasaInv) {
        this.tasaInv = tasaInv;
    }

    public boolean[] getStatusCheck() {
        return statusCheck;
    }

    public void setStatusCheck(boolean[] statusCheck) {
        this.statusCheck = statusCheck;
    }

    public GatIL getGatIL() {
        return gatIL;
    }

    public boolean mostrarContrasenia() {
        return init.getAutenticacion().mostrarContrasena(this.tipoOperacion,
                init.getSession().getClientProfile());
    }

    public boolean mostrarNIP() {
        return init.getAutenticacion().mostrarNIP(this.tipoOperacion,
                init.getSession().getClientProfile());
    }

    public boolean mostrarCampoTarjeta(){
        return false;
    }

    public Constants.TipoOtpAutenticacion tokenAMostrar() {
        Constants.TipoOtpAutenticacion tipoOTP;
        try {
            tipoOTP = init.getAutenticacion().tokenAMostrar(this.tipoOperacion,
                    init.getSession().getClientProfile());

        } catch (Exception ex) {
            if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "Error on Autenticacion.mostrarNIP execution.", ex);
            tipoOTP = null;
        }

        return tipoOTP;
    }

    public boolean mostrarCVV() {
        return init.getAutenticacion().mostrarCVV(this.tipoOperacion,
                init.getSession().getClientProfile());
    }

    public Constants.TipoInstrumento getTipoInstrumentoSeguridad() {
        return tipoInstrumentoSeguridad;
    }

    public boolean enviaPeticionOperacion() {
        String asm = null;
        if(controller instanceof ClausulasILViewController){
            if (mostrarContrasenia()) {
                contrasena = controller.pideContrasena();
                if (contrasena.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.PASSWORD_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            if(mostrarCampoTarjeta()){
                tarjeta = controller.pideTarjeta();
                String mensaje = "";
                if(tarjeta.equals("")){
                    mensaje = controller.getString(R.string.confirmation_componente_digitos_tarjeta_alerta);
                    init.getBaseViewController().showInformationAlert(controller,mensaje);
                    return false;
                }else if(tarjeta.length() != 5){
                    mensaje =  controller.getString(R.string.confirmation_componente_digitos_tarjeta_alerta);
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            if (mostrarNIP()) {
                nip = controller.pideNIP();
                if (nip.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (nip.length() != Constants.NIP_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.NIP_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) {
                asm = controller.pideASM();
                if (asm.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (asm.length() != Constants.ASM_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.ASM_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (mostrarCVV()) {
                cvv = controller.pideCVV();
                if (cvv.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (cvv.length() != Constants.CVV_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.CVV_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            String newToken = null;
            if(tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == Constants.TipoInstrumento.SoftToken && (init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService())) {
                if (init.getSofTokenStatus()) {
                    newToken = init.getDelegateOTP().loadOtpFromSofttoken(tokenAMostrar(), init.getDelegateOTP());
                    if (null != newToken)
                        asm = newToken;
                    token = asm;
                    performAction(null);
                } else if (!init.getSofTokenStatus() && PropertiesManager.getCurrent().getSofttokenService()) {

                    GetOtpBmovil otpG = new GetOtpBmovil(new GetOtp() {
                        /**
                         * @param otp
                         */
                        @Override
                        public void setOtpRegistro(String otp) {

                        }

                        /**
                         * @param otp
                         */
                        @Override
                        public void setOtpCodigo(String otp) {
                            if (ServerCommons.ALLOW_LOG) {
                                Log.d("APP", "la otp en callback: " + otp);
                            }
                            token = String.valueOf(otp);
                            performAction(null);
                        }

                        /**
                         * @param otp
                         */
                        @Override
                        public void setOtpCodigoQR(String otp) {

                        }
                    }, SuiteApp.appContext);

                    otpG.generateOtpCodigo();
                }
            }
            else if(tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno ) {
                token = asm;
                performAction(null);
            }
            else if(tokenAMostrar() == Constants.TipoOtpAutenticacion.ninguno){
                token = asm;
                performAction(null);
            }
            return true;
        }
        return false;
    }

    //Metodo que actualiza las cuentas
    private void actualizacionDeCuentasExitosa(Account[] accounts) {
        final Session session = Session.getInstance(init.appContext);
        session.updateAccounts(accounts);
    }

    public void realizaOperacionActualizarCuentas(){
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put("numeroTelefono", init.getSession().getUsername());
        params.put("numeroCliente", ((ILSession) init.getSession()).getClientNumber());
        params.put("IUM", init.getSession().getIum());

        Hashtable<String, String> paramTable2  =  AuxConectionFactory.actualizarCuentas(params);
        doNetworkOperation(Server.ACTUALIZAR_CUENTAS, paramTable2, init.getBaseViewController(),true,new ActualizarCuentasResult());
    }

    public void realizaOperacionTranferencia(SuccessIL successIL){
        Hashtable<String, String> params = new Hashtable<String, String>();

        String cuentaSP = successIL.getPersonalSolution();
        String cuentaSPCompleta = cuentaSP.substring(0, 8)+"00"+cuentaSP.substring(10, cuentaSP.length());

        String cuentaIL = successIL.getNumContract();
        String cuentaILCompleta = cuentaIL.substring(0, 8)+"00"+cuentaIL.substring(10, cuentaIL.length());

        params.put(ConstantsIL.TAG_ACCOUNT_SENDER, cuentaSPCompleta);
        params.put(ConstantsIL.TAG_ACCOUNT_RECIEVER, cuentaILCompleta);
        params.put(ConstantsIL.TAG_AMOUNT, successIL.getAmount().replace("$","").replace(",", "").replace(".00", ""));
        params.put(ConstantsIL.TAG_CELLPHONE_NUMBER, init.getSession().getUsername());

        params.put(ConstantsIL.IUM_TAG, init.getSession().getIum());

        doNetworkOperation(Server.TRANSFERENCIAS_IL, params, InitOneClickIL.getInstance().getBaseViewController(), false, successIL);
    }

    public void realizaOperacionTerminosYCondiciones(){
        init = InitOneClickIL.getInstance().getInstance();
        opWeb = "Terminos";

        Hashtable<String, String> params = new Hashtable<String, String>();
        boolean banContrato = false;
        params.put(ConstantsIL.TAG_ACCOUNT_NUMBER, "");
        params.put(ConstantsIL.TAG_CVE_CAMP, "");
        params.put(ConstantsIL.TAG_CREATE_CONTRACT, String.valueOf(banContrato).replace("\"", ""));
        params.put(ConstantsIL.TAG_NUM_CONTRACT, ConstantsIL.FOLIO_CONTRATO_IL);

        params.put(ConstantsIL.IUM_TAG, init.getSession().getIum());

        doNetworkOperation(Server.CONSULTA_TERMINOS_CONDICIONES_IL, params, init.getBaseViewController(),false, new ConsultarContrato());//,IUM);
    }
}
