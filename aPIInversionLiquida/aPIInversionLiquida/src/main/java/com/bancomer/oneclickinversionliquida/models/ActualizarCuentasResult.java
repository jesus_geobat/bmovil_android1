package com.bancomer.oneclickinversionliquida.models;

import android.util.Log;

import com.bancomer.oneclickinversionliquida.commons.ILSession;
import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;
import com.bancomer.oneclickinversionliquida.io.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;


@SuppressWarnings("serial")
public class ActualizarCuentasResult implements ParsingHandler {

	private Account[] asuntos;

	/**
	 *
	 * @return lista de Accounts del usuario
	 */
	public Account[] getAsuntos() {
		return asuntos;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		throw new ParsingException("Invalid process.");
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {

		JSONArray arrayAsuntos = parser.parseNextValueWithArray("asuntos", false);
		int numAsuntos = arrayAsuntos.length();
		asuntos = new Account[numAsuntos];
		for (int i = 0; i < numAsuntos; i++) {


			try {
				JSONObject asuntoObj = arrayAsuntos.getJSONObject(i);
				String tipoCuenta = asuntoObj.getString("tipoCuenta");
				String alias = asuntoObj.getString("alias");
				String divisa = asuntoObj.getString("divisa");
				String asunto = asuntoObj.getString("asunto");
				String saldo = asuntoObj.getString("saldo");

				String concepto = asuntoObj.getString("concepto");
				String visible = asuntoObj.getString("visible");
				String celularAsociado = asuntoObj.getString("celularAsociado");
				String codigoCompania = asuntoObj.getString("codigoCompania");
				String descripcionCompania = asuntoObj.getString("descripcionCompania");
				String fechaUltimaModificacion = asuntoObj.getString("fechaUltimaModificacion");
				String indicadorSPEI = asuntoObj.getString("indicadorSPEI");

				ICommonSession session = InitOneClickIL.getInstance().getSession();
				String date = Tools.dateForReference(((ILSession) session).getServerDate());
				if(Server.ALLOW_LOG) Log.e("value of this parser",String.valueOf(Double.valueOf(saldo)));

				asuntos[i] = new Account(asunto,
						Tools.getDoubleAmountFromServerString(saldo),
						Tools.formatDate(date), "S".equals(visible), divisa,
						tipoCuenta, concepto, alias, celularAsociado,
						codigoCompania, descripcionCompania, fechaUltimaModificacion , indicadorSPEI);

			} catch (JSONException e) {
				if(Server.ALLOW_LOG) throw new ParsingException("Error formato");
			}
		}
	}
}
