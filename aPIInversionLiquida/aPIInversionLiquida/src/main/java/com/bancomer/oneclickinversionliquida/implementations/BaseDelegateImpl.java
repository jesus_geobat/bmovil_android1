package com.bancomer.oneclickinversionliquida.implementations;

import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by sandradiaz on 22/07/16.
 */
public class BaseDelegateImpl implements BaseDelegate {

    @Override
    public void doNetworkOperation(int operationId,
                                   Hashtable<String, ?> params, BaseViewController caller) {
        // TODO Auto-generated method stub

    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        // TODO Auto-generated method stub

    }

    @Override
    public void performAction(Object obj) {
        // TODO Auto-generated method stub

    }

    @Override
    public long getDelegateIdentifier() {
        // TODO Auto-generated method stub
        return 0;
    }
}
