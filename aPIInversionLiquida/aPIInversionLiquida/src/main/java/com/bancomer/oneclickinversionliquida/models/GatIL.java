package com.bancomer.oneclickinversionliquida.models;

import android.util.Log;

import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by sandradiaz on 03/11/16.
 */
public class GatIL implements ParsingHandler {
    private String nominalGat;
    private String realGat;

    public GatIL(String nominalGat, String realGat){
        this.nominalGat = nominalGat;
        this.realGat = realGat;
    }

    public GatIL() {
        super();
    }

    public String getNominalGat() {
        return nominalGat;
    }

    public void setNominalGat(String nominalGat) {
        this.nominalGat = nominalGat;
    }

    public String getRealGat() {
        return realGat;
    }

    public void setRealGat(String realGat) {
        this.realGat = realGat;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

    }

    public void process(String parser) throws IOException, ParsingException, JSONException {
        JSONObject obj = new JSONObject(parser);

        setNominalGat((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_NOMINAL_GAT)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_NOMINAL_GAT) : "");
        setRealGat((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_REAL_GAT)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_REAL_GAT) : "");
    }
}
