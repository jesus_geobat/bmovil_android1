package com.bancomer.oneclickinversionliquida.commons;

/**
 * Created by sandradiaz on 22/07/16.
 */
public class Tools {
    public static String formatPositiveIntegerAmount(String amount) {
        StringBuffer result = new StringBuffer();
        if (amount != null) {
            int size = amount.length();
            int remaining = size % 3;
            if (remaining == 0) {
                remaining = 3;
            }
            int start = 0;
            for (int end = remaining; end <= size; end += 3) {
                result.append(amount.substring(start, end));
                if (end < size) {
                    result.append(',');
                }
                start = end;
            }
        }
        return result.toString();
    }
}
