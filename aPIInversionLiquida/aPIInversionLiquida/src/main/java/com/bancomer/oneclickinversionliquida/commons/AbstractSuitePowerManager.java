package com.bancomer.oneclickinversionliquida.commons;

/**
 * Created by sandradiaz on 22/07/16.
 */
public abstract class AbstractSuitePowerManager {
    /**
     * Gets an AbstractContactMannager instance in accordance to the current api level.
     * @return The AbstractContactMannager instance.
     */
    public static AbstractSuitePowerManager getSuitePowerManager() {
        return (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.ECLAIR_MR1) ? new DonutPowerManager() : new EclairPowerManager();
    }

    /**
     * Gets a boolean than indicates the current state of the screen (On or Off).
     * @return True if the screen is On, false otherwise.
     */
    public abstract boolean isScreenOn();
}
