package com.bancomer.oneclickinversionliquida.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.adobe.mobile.Config;
import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.gui.delegates.PopUpExitoILDelegate;
import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;
import com.bancomer.oneclickinversionliquida.models.SuccessIL;

import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;

/**
 * Created by sandradiaz on 08/07/16.
 */
public class PopUpExitoILViewController extends Activity {
    private BaseViewController baseViewController;
    private InitOneClickIL init = InitOneClickIL.getInstance();
    private BaseViewsController parentManager;
    private PopUpExitoILDelegate delegate;

    private LinearLayout lytExitoIL;
    private ImageView imgExito;
    public ImageView imgHeader;
    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate = (PopUpExitoILDelegate) parentManager.getBaseDelegateForKey(PopUpExitoILDelegate.POP_UP_IL_DELEGATE);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_inversion_liquida_pop_up_exito);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        //Adobe
        Config.setContext(this.getApplicationContext());

        init();

        ShowContentExito(3000);
    }

    public void init(){
        findViews();
        scaleViews();
    }

    private void findViews() {
        lytExitoIL = (LinearLayout) findViewById(R.id.lytExitoIL);
        imgExito = (ImageView) findViewById(R.id.imgExito);
        imgHeader = (ImageView) findViewById(R.id.img_encabezadoDetalle);
        imgBack = (ImageView) findViewById(R.id.imgBack);

        imgHeader.setVisibility(View.GONE);
        imgBack.setVisibility(View.GONE);
        lytExitoIL.getBackground().setAlpha(128);
    }

    private void scaleViews() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(imgExito);
    }

    public void ShowContentExito(int milisegundos) {
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {
                // acciones que se ejecutan tras los milisegundos
                init.getParentManager().setActivityChanging(true);
                overridePendingTransition(R.anim.fade_out, R.anim.fade_out);
                init.showExito(delegate.getMontoInv(), delegate.getTasaInv(), delegate.getSaveIum(), delegate.getSuccessIL());
            }
        }, milisegundos);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!init.getParentManager().isActivityChanging()) {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }
}
