package com.bancomer.oneclickinversionliquida.commons;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

import com.bancomer.oneclickinversionliquida.implementations.InitOneClickIL;

/**
 * Created by sandradiaz on 22/07/16.
 */
public final class EclairPowerManager extends AbstractSuitePowerManager {
    /* (non-Javadoc)
     * @see suitebancomer.aplicaciones.bmovil.classes.common.AbstractSuitePowerManager#isScreenOn()
     */
    @Override
    public boolean isScreenOn() {
        try {
            PowerManager pm = (PowerManager) InitOneClickIL.appContext.getSystemService(Context.POWER_SERVICE);
            return pm.isScreenOn();
        }catch(Exception ex) {
            Log.wtf("EclairPowerManager", "Error while getting the state of the screen.", ex);
            return false;
        }
    }
}
