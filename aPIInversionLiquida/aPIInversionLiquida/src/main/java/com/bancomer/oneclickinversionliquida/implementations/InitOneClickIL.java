package com.bancomer.oneclickinversionliquida.implementations;

import android.app.Activity;
import android.content.Context;

import com.bancomer.oneclickinversionliquida.commons.AccountIL;
import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.gui.controllers.ClausulasILViewController;
import com.bancomer.oneclickinversionliquida.gui.controllers.DetalleILViewController;
import com.bancomer.oneclickinversionliquida.gui.controllers.ExitoILViewController;
import com.bancomer.oneclickinversionliquida.gui.controllers.PopUpExitoILViewController;
import com.bancomer.oneclickinversionliquida.gui.controllers.TerminosYCondicionesViewController;
import com.bancomer.oneclickinversionliquida.gui.delegates.ClausulasILDelegate;
import com.bancomer.oneclickinversionliquida.gui.delegates.DetalleILDelegate;
import com.bancomer.oneclickinversionliquida.gui.delegates.ExitoILDelegate;
import com.bancomer.oneclickinversionliquida.gui.delegates.PopUpExitoILDelegate;
import com.bancomer.oneclickinversionliquida.io.BaseSubapplication;
import com.bancomer.oneclickinversionliquida.io.Server;
import com.bancomer.oneclickinversionliquida.models.DetailIL;
import com.bancomer.oneclickinversionliquida.models.GatIL;
import com.bancomer.oneclickinversionliquida.models.SuccessIL;

import org.apache.http.impl.client.DefaultHttpClient;

import bancomer.api.common.commons.IAutenticacion;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.BaseViewsControllerImpl;
import bancomer.api.common.gui.delegates.DelegateBaseOperacion;
import bancomer.api.common.gui.delegates.MainViewHostDelegate;
import bancomer.api.common.model.HostAccounts;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by sandradiaz on 22/07/16.
 */
public class InitOneClickIL {
    /*Components for APIVENTATDC working*/

    private static InitOneClickIL mInstance = null;

    private BaseViewController baseViewController;

    private static BaseViewsController parentManager;

    private BaseSubapplication baseSubApp;

    public static DefaultHttpClient client;

    private Promociones ofertaSeguros;

    private DelegateBaseOperacion delegateOTP;

    private ICommonSession session;

    private IAutenticacion autenticacion;

    private boolean sofTokenStatus;

    private String cuentaOneClick;

    private MainViewHostDelegate hostInstance;

    private AccountIL[] listaCuentas;

    public static Context appContext;

    /* End Region */

    /////////////  Getter's & Setter's /////////////////


    public ICommonSession getSession()
    {
        return session;
    }

    public DelegateBaseOperacion getDelegateOTP() {
        return delegateOTP;
    }

    public Promociones getOfertaSeguros()
    {
        return ofertaSeguros;
    }

    public BaseViewController getBaseViewController() {
        return baseViewController;
    }

    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }

    public BaseViewsController getParentManager() {
        return parentManager;
    }

    public BaseSubapplication getBaseSubApp() {
        return baseSubApp;
    }

    public IAutenticacion getAutenticacion() {  return autenticacion;  }

    public boolean getSofTokenStatus(){ return sofTokenStatus; }

    public String getCuentaOneClick() {
        return cuentaOneClick;
    }

    public AccountIL[] getListaCuentas() {
        return listaCuentas;
    }

///////////// End Region Getter's & Setter's /////////////////

    public static InitOneClickIL getInstance(Activity activity, Promociones promocion, DelegateBaseOperacion delegateOTP, ICommonSession session, IAutenticacion autenticacion, boolean softTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance, HostAccounts[] listaCuentas,boolean dev, boolean sim, boolean emulator, boolean log, Context appContext) {
        if(mInstance == null)
            mInstance = new InitOneClickIL(activity, promocion, delegateOTP, session, autenticacion, softTokenStatus, client, hostInstance, listaCuentas,dev, sim, emulator, log, appContext);
        else
            parentManager.setCurrentActivityApp(activity);

        return mInstance;
    }

    public static InitOneClickIL getInstance() {
        return mInstance;
    }

    private InitOneClickIL(Activity activity, Promociones promocion, DelegateBaseOperacion delegateOTP, ICommonSession session, IAutenticacion autenticacion, boolean sofTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance, HostAccounts[] listaCuentas, boolean dev, boolean sim, boolean emulator, boolean log, Context appContext) {
        this.setServerParams(dev, sim, emulator, log);
        this.appContext = appContext;
        Server.setEnviroment();
        this.ofertaSeguros = promocion;
        this.delegateOTP = delegateOTP;
        this.baseViewController = new BaseViewControllerImpl();
        this.client = client;
        parentManager = new BaseViewsControllerImpl();
        parentManager.setCurrentActivityApp(activity);
        this.baseSubApp = new BaseSubapplicationImpl(activity, this.client);
        this.session = session;
        this.autenticacion = autenticacion;
        this.sofTokenStatus = sofTokenStatus;
        this.hostInstance = hostInstance;
        setAcounts(listaCuentas);
    }



    public void setServerParams(boolean dev, boolean sim, boolean emulator, boolean log)
    {
        Server.DEVELOPMENT = dev;
        Server.SIMULATION = sim;
        Server.EMULATOR = emulator;
        Server.ALLOW_LOG = log;
    }

    public void showTerminosYCondiciones(String monto, String tasa, String cuentaSP, String cr, boolean[] statusCheck, GatIL gatIL, String managementUnit, String terminos) {
        parentManager.showViewController(TerminosYCondicionesViewController.class, 0, false,
                new String[]{ConstantsIL.TAG_AMOUNT,
                             ConstantsIL.TAG_RATE,
                             ConstantsIL.TAG_ACCOUNT_PER_SOL,
                             ConstantsIL.TAG_CR,
                             ConstantsIL.TAG_STATUS_CHECK,
                             ConstantsIL.TAG_GAT_IL,
                             ConstantsIL.TAG_MAGNAGEMENT_UNIT,
                             ConstantsIL.TERMINOS_DE_USO_IL},
                new Object[]{monto, tasa, cuentaSP, cr, statusCheck, gatIL, managementUnit, terminos}
                ,parentManager.getCurrentViewController());
    }

    public void showContrato(String monto, String tasa, SuccessIL successIL, String saveIum, String contrato) {
        parentManager.showViewController(TerminosYCondicionesViewController.class, 0, false,
                new String[]{ConstantsIL.TAG_AMOUNT,
                             ConstantsIL.TAG_RATE,
                             ConstantsIL.SUCCESS_IL,
                             ConstantsIL.TAG_SAVE_IUM,
                             ConstantsIL.CONTRATO_IL},
                new Object[]{monto, tasa, successIL, saveIum, contrato}
                ,parentManager.getCurrentViewController());
    }

    public void showDetalleIL(DetailIL datailIL){
        DetalleILDelegate delegate = (DetalleILDelegate) parentManager.getBaseDelegateForKey(DetalleILDelegate.INVERSION_LIQUIDA_DETALLE_DELEGATE);

        if(delegate == null){
            delegate = new DetalleILDelegate(datailIL);
            parentManager.addDelegateToHashMap(DetalleILDelegate.INVERSION_LIQUIDA_DETALLE_DELEGATE,delegate);
        }
        parentManager.showViewController(DetalleILViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }

    public void showClausulas(String monto, String tasa, String cuentaSP, String cr, boolean[] statusChecks, GatIL gatIL, String managementUnit) {

        ClausulasILDelegate delegate = (ClausulasILDelegate) parentManager.getBaseDelegateForKey(ClausulasILDelegate.CLAUSULAS_IL_DELEGATE);

        if(delegate == null){
            delegate = new ClausulasILDelegate(monto, tasa, cuentaSP, cr, statusChecks, gatIL, managementUnit);
            parentManager.addDelegateToHashMap(ClausulasILDelegate.CLAUSULAS_IL_DELEGATE,delegate);
        }

        parentManager.showViewController(ClausulasILViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }

    public void showPopUpExito(String monto, String tasa, String saveIum, SuccessIL successIL) {
        PopUpExitoILDelegate delegate = (PopUpExitoILDelegate) parentManager.getBaseDelegateForKey(PopUpExitoILDelegate.POP_UP_IL_DELEGATE);

        if(delegate == null){
            delegate = new PopUpExitoILDelegate(successIL, monto, tasa, saveIum);
            parentManager.addDelegateToHashMap(PopUpExitoILDelegate.POP_UP_IL_DELEGATE,delegate);
        }
        parentManager.showViewController(PopUpExitoILViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }

    public void showExito(String monto, String tasa, String saveIum, SuccessIL successIL) {

        ExitoILDelegate delegate = (ExitoILDelegate) parentManager.getBaseDelegateForKey(ExitoILDelegate.EXITO_IL_DELEGATE);

        if(delegate == null){
            delegate = new ExitoILDelegate(monto, tasa, saveIum, successIL);
            parentManager.addDelegateToHashMap(ExitoILDelegate.EXITO_IL_DELEGATE,delegate);
        }

        parentManager.showViewController(ExitoILViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }

    public void resetInstance()
    {   mInstance = null; }

    public void startInvLiq()
    {
        new DetalleILDelegate().performAction(null);
    }

    public void setAcounts(HostAccounts[] accounts) {
        AccountIL[] acc = new AccountIL[accounts.length];

        if (accounts != null){
            for (int i=0; i<accounts.length; i++){
                AccountIL aux = new AccountIL();

                aux.setBalance(accounts[i].getBalance());
                aux.setNumber(accounts[i].getNumber());
                aux.setDate("");
                aux.setVisible(accounts[i].isVisible());
                aux.setCurrency("");
                aux.setType(accounts[i].getType());
                aux.setAlias(accounts[i].getAlias());
                aux.setConcept("");
                aux.setCelularAsociado("");
                aux.setCodigoCompania("");
                aux.setDescripcionCompania("");
                aux.setFechaUltimaModificacion("");
                aux.setIndicadorSPEI("");
                acc[i]=aux;
            }
        }
        this.listaCuentas = acc;

    }
}
