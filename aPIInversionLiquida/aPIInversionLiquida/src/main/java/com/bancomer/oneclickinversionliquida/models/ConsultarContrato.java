package com.bancomer.oneclickinversionliquida.models;

import android.util.Log;

import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.io.ParserJSONImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by sandradiaz on 22/07/16.
 */
public class ConsultarContrato implements ParsingHandler{

    /**
     * Terminos de uso en formato HTML.
     */
    private String formatoHTML;


    /**
     * Tipo Producto solo para Venta TDC
     */
    private String tipoProducto;


    /**
     * @return Terminos de uso en formato HTML.
     */
    public String getFormatoHTML() {
        return formatoHTML;
    }

    /**
     * @param formatoHTML Terminos de uso en formato HTML.
     */
    public void setFormatoHTML(String formatoHTML) {
        this.formatoHTML = formatoHTML;
    }

    /**
     * getter and setter tipoProducto
     */

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public ConsultarContrato() {
        formatoHTML = "";
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        formatoHTML = "";
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

    }

    public void process(String parser) throws IOException, ParsingException, JSONException {
        formatoHTML = parser;
        grabarArchivo();
    }

    private void grabarArchivo(){

        try{
            File f = new File("//data/data/com.bancomer.mbanking/");
            File newFile = new File(f, "terminos");
            OutputStreamWriter osw = new OutputStreamWriter(
                    new FileOutputStream(newFile));
            osw.write(formatoHTML);
            osw.close();
            formatoHTML = newFile.getAbsolutePath();
        }catch(IOException e){
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
