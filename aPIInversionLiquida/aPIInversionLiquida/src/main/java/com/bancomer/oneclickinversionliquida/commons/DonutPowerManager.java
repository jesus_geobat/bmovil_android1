package com.bancomer.oneclickinversionliquida.commons;

/**
 * Created by sandradiaz on 22/07/16.
 */
public final class DonutPowerManager extends AbstractSuitePowerManager {

    /* (non-Javadoc)
     * @see suitebancomer.aplicaciones.bmovil.classes.common.AbstractSuitePowerManager#isScreenOn()
     */
    @Override
    public boolean isScreenOn() {
        return false;
    }
}
