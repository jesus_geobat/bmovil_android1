package com.bancomer.oneclickinversionliquida.commons;

import java.util.Date;

import suitebancomer.aplicaciones.bmovil.classes.model.Account;

/**
 * Created by sandradiaz on 26/10/16.
 */
public interface ILSession {

    public String getClientNumber();
    public void setAccounts(Account[] accounts);
    public Date getServerDate();
}
