package com.bancomer.oneclickinversionliquida.io;

import java.util.Hashtable;

public class AuxConectionFactory {

    public static Hashtable actualizarCuentas(Hashtable<String, ?> params) {
        Hashtable<String, String> paramNew= new Hashtable<String, String>();

        paramNew.put("numeroTelefono",validaNull(params.get("numeroTelefono")));
        paramNew.put("numeroCliente",validaNull(params.get("numeroCliente")));
        paramNew.put("IUM",validaNull(params.get("IUM")));

        return paramNew;
    }

    private static String validaNull(Object valor){
        if(null==valor){
            return "";
        }else{
            return String.valueOf(valor);
        }
    }

}



