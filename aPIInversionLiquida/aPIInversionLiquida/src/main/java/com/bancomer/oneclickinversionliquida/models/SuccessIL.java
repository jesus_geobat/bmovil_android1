package com.bancomer.oneclickinversionliquida.models;

import android.util.Log;

import com.bancomer.oneclickinversionliquida.commons.ConstantsIL;
import com.bancomer.oneclickinversionliquida.io.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by sandradiaz on 01/08/16.
 */
public class SuccessIL implements ParsingHandler {
    private String personalSolution;
    private String numContract;
    private String amount;
    private String rate;
    private String dateContract;
    private String sendingEmail;
    private String sendingSMS;
    Promociones promocion;
    Promociones[] promociones;
    String PM;
    private String folio;

    public String getPersonalSolution() {
        return personalSolution;
    }

    public void setPersonalSolution(String personalSolution) {
        this.personalSolution = personalSolution;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDateContract() {
        return dateContract;
    }

    public void setDateContract(String dateContract) {
        this.dateContract = dateContract;
    }

    public String getSendingEmail() {
        return sendingEmail;
    }

    public void setSendingEmail(String sendingEmail) {
        this.sendingEmail = sendingEmail;
    }

    public String getSendingSMS() {
        return sendingSMS;
    }

    public void setSendingSMS(String sendingSMS) {
        this.sendingSMS = sendingSMS;
    }

    public String getNumContract() {
        return numContract;
    }

    public void setNumContract(String numContract) {
        this.numContract = numContract;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public void setPromocion(Promociones promocion) {
        this.promocion = promocion;
    }

    public Promociones[] getPromociones() {
        return promociones;
    }

    public void setPromociones(Promociones[] promociones) {
        this.promociones = promociones;
    }

    public String getPM() {
        return PM;
    }

    public void setPM(String PM) {
        this.PM = PM;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

    }

    public void process(String parser) throws IOException, ParsingException, JSONException {
        JSONObject obj = new JSONObject(parser);

        setPersonalSolution((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_PERSONAL_SOL)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_PERSONAL_SOL) : "");
        setNumContract((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_NUM_CONTRACT)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_NUM_CONTRACT) : "");
        setAmount((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_AMOUNT)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_AMOUNT) : "");
        setRate((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_RATE)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_RATE) : "");
        setSendingEmail((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_SENDING_EMAIL)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_SENDING_EMAIL) : "");
        setSendingSMS((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_SENDING_SMS)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_SENDING_SMS) : "");

        PM = obj.getJSONObject("response").getString("pm");

        try {
            if (PM.equals("SI")) {
                JSONObject jsonObjectPromociones = new JSONObject(obj.getJSONObject("response").getString("lp"));
                JSONArray arrPromocionesJson = new JSONArray(jsonObjectPromociones.getString("campaings"));
                ArrayList<Promociones> arrPromociones = new ArrayList<Promociones>();
                for (int i = 0; i < arrPromocionesJson.length(); i++) {

                    JSONObject jsonPromocion = (JSONObject) arrPromocionesJson.get(i);
                    Promociones promo = new Promociones();

                    promo.setCveCamp(jsonPromocion.getString(ConstantsIL.TAG_CVE_CAMP));
                    promo.setDesOferta(jsonPromocion.getString(ConstantsIL.TAG_DESOFERTA));
                    promo.setMonto(jsonPromocion.getString(ConstantsIL.TAG_AMOUNT));

                    arrPromociones.add(promo);
                }

                this.promociones = arrPromociones
                        .toArray(new Promociones[arrPromociones.size()]);
            }else{
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            if(Server.ALLOW_LOG) e.printStackTrace();
        }
    }

    public void processTransfers(String parser) throws IOException, ParsingException, JSONException {

        JSONObject obj = new JSONObject(parser);

        setDateContract((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_DATE_CONTRACT)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_DATE_CONTRACT) : "");
        setFolio((!obj.getJSONObject("response").isNull(ConstantsIL.TAG_FOLIO)) ? obj.getJSONObject("response").getString(ConstantsIL.TAG_FOLIO) : "");
    }
}
