package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.desactivada;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
//import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.ConstanstMenuOpcionesHamburguesa;
//import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.IndicadorHamburguesa;
//import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.desactivada.ConsultaEstatusAplicacionDesactivadaDelegate;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.mbanking.desactivada.R;
import com.bancomer.mbanking.desactivada.SuiteAppDesac;

public class ConsultaEstatusAplicacionDesactivadaViewController extends LinearLayout implements OnClickListener {
	/**
	 * The delegate reference
	 */
	private ConsultaEstatusAplicacionDesactivadaDelegate delegate;
	
	/**
	 * 
	 */
	private BmovilViewsController parentManager;
	
	/**
	 * The flip back button
	 */
	private Button mFlipBackButton;
	
	 /**
	  * The button which fires the hamburguesa process
	  */
	 private ImageButton mHamburguesaButton;
	
	/**
	 * The button which fires the login process
	 */
	private Button mAccessButton;
	
	/**
	 * Campo de texto para el numero de celular.
	 */
	private EditText tbNumeroCelular;
	
	/**
	 * Link para ayuda de contratación.
	 */
	private TextView linkReactivar;
	//ARR
		private static String nombreUsuario;
	
	/**
	 * Reference to the parent controller, used to perform the flip back action
	 */
	private BaseViewControllerCommons menuViewController;
	
	public ConsultaEstatusAplicacionDesactivadaViewController(BaseViewControllerCommons menuSuiteViewController, BmovilViewsController parentViewsController) {
		super(menuSuiteViewController);
		
		parentManager = parentViewsController;
		menuViewController = menuSuiteViewController;
		//parentManager.setCurrentActivityApp(menuViewController);
		
	    LayoutInflater inflater = LayoutInflater.from(menuSuiteViewController);
	    LinearLayout viewLayout = (LinearLayout)inflater.inflate(SuiteAppDesac.getResourceId("layout_bmovil_aplicacion_desactivada_des", "layout"), this, true);
	    
	    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1,-1); // Fill/Match the parent.
	    viewLayout.setLayoutParams(params);

	    init();
	}
	
	public void onBtnFlipClick(View sender) {
		menuViewController.plegarOpcionAplicacionDesactivada();
	}
	
	@Override
	public void onClick(View v) {
		if(mFlipBackButton == v) {
			onBtnFlipClick(v);
		} else if(mAccessButton == v) {
			onBtnContinuarClick(v);
		} else if(linkReactivar == v) {
			onLinkComoContratarClick(v);
		}else if(mHamburguesaButton == v) 
		{
			//onBtnHamburguesaClick(v);
		 }
	}
	
	/*private void onBtnHamburguesaClick(View v)
	{
		Session.bmovilApp = null;
		IndicadorHamburguesa.muestraIndicadorActividad(menuViewController,menuViewController.getString(R.string.alert_operation),ConstanstMenuOpcionesHamburguesa.CONECTANDO_CON_EL_SERVER);
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				MenuHamburguesaViewsControllers.initValuesRequest(menuViewController, ConstanstMenuOpcionesHamburguesa.VALUE_NOMBRE_APP);
				parentManager.showMenuHamburguesa(new String[]{ConstanstMenuOpcionesHamburguesa.NAME_NOMBRE_APP,ConstanstMenuOpcionesHamburguesa.BAND_NAME_SESSION}, new String[]{ConstanstMenuOpcionesHamburguesa.VALUE_NOMBRE_APP,ConstanstMenuOpcionesHamburguesa.SIN_SESSION}, menuViewController);
			}
		}).start();
	}*/

	private void init() {
		delegate = (ConsultaEstatusAplicacionDesactivadaDelegate)parentManager.getBaseDelegateForKey(ConsultaEstatusAplicacionDesactivadaDelegate.CONSULTA_APLICACION_DESACTIVADA_DELEGATE_ID);
		if(null == delegate) {
			delegate = new ConsultaEstatusAplicacionDesactivadaDelegate();
			parentManager.addDelegateToHashMap(ConsultaEstatusAplicacionDesactivadaDelegate.CONSULTA_APLICACION_DESACTIVADA_DELEGATE_ID, delegate);
		}
		//delegate.setOwnerController(menuViewController);
		
		findViews();
		scaleToCurrentScreen();
		
		linkReactivar.setOnClickListener(this);
		mAccessButton.setOnClickListener(this);
		mFlipBackButton.setOnClickListener(this);
		mHamburguesaButton.setOnClickListener(this);
		String numberPhone=	delegate.buscarNumero();
		if(numberPhone!=null){
			tbNumeroCelular.setText(numberPhone);
		}
		
	}
	
	private void findViews() {
		tbNumeroCelular = (EditText)findViewById(SuiteAppDesac.getResourceId("login_user_input", "id"));
		mFlipBackButton = (Button)findViewById(SuiteAppDesac.getResourceId("login_flip_button", "id"));
		linkReactivar = (TextView)findViewById(SuiteAppDesac.getResourceId("login_account_reactivation", "id"));
		mAccessButton = (Button)findViewById(SuiteAppDesac.getResourceId("login_access_button", "id"));
		mHamburguesaButton = (ImageButton)findViewById(SuiteAppDesac.getResourceId("bmovil_menu_hamburguesa", "id"));
	}
	
	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(menuViewController.getWindowManager());
		
		guiTools.scale(findViewById(SuiteAppDesac.getResourceId("inputsLayout", "id")));
		guiTools.scale(findViewById(SuiteAppDesac.getResourceId("loginTitleLayout", "id")));
		guiTools.scale(findViewById(SuiteAppDesac.getResourceId("login_title", "id")), true);
		guiTools.scale(findViewById(SuiteAppDesac.getResourceId("login_flip_button", "id")));
		guiTools.scale(findViewById(SuiteAppDesac.getResourceId("loginNumeroCelularLayout", "id")));
		guiTools.scale(findViewById(SuiteAppDesac.getResourceId("login_user_label", "id")), true);
		guiTools.scale(findViewById(SuiteAppDesac.getResourceId("login_user_input", "id")), true);
		guiTools.scale(findViewById(SuiteAppDesac.getResourceId("ingresarLayout", "id")));
		guiTools.scale(findViewById(SuiteAppDesac.getResourceId("login_account_reactivation", "id")), true);
		guiTools.scale(findViewById(SuiteAppDesac.getResourceId("login_access_button", "id")));
	}

	public void setPhoneNumber(String number) {
		this.tbNumeroCelular.setText(number);
	}
	
	public void onBtnContinuarClick(View sender) {
		delegate.validarDatos(tbNumeroCelular.getText().toString(),true);
		//ARR
				if(delegate.res)
				{
					nombreUsuario = tbNumeroCelular.getText().toString(); 
				}
	}
	//ARR
		public static String getNombreUsuario()
		{
			return nombreUsuario;
		}
	
	public void onLinkComoContratarClick(View sener) {
		//SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController().showAyudaContratacion();
	}
}
