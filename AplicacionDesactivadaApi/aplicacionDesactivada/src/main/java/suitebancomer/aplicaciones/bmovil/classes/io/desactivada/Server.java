/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomer.aplicaciones.bmovil.classes.io.desactivada;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;




/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receive the network operation
 * requests and builds a network operation suitable for HttpInvoker class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponse object, so that the application logic can
 * perform high level checks and processes.
 * 
 * @author Stefanini IT Solutions.
 */
public class Server extends ServerCommons{
	


	/**
	 * Activation step 1 operation.
	 */
	public static final int ACTIVATION_STEP1_OPERATION = 1;

	/**
	 * Acivation step 2 operation.
	 */
	public static final int ACTIVATION_STEP2_OPERATION = 2;

	/**
	 * Login oepration.
	 */
	public static final int LOGIN_OPERATION = 3;

	/**
	 * Movements operation.
	 */
	public static final int MOVEMENTS_OPERATION = 4;

	/**
	 * Transfer money operation between the user's accounts.
	 */
	public static final int SELF_TRANSFER_OPERATION = 5;

	/**
	 * Transfer money operation to someone else's account in Bancomer.
	 */
	public static final int BANCOMER_TRANSFER_OPERATION = 6;

	/**
	 * External transfer operation.
	 */
	public static final int EXTERNAL_TRANSFER_OPERATION = 7;

	/**
	 * Close session operation.
	 */
	public static final int CLOSE_SESSION_OPERATION = 11;

	/**
	 * Service payment.
	 */
	public static final int SERVICE_PAYMENT_OPERATION = 15;

	/**
	 * Alta de operaciones sin tarjeta
	 */
	public static final int ALTA_OPSINTARJETA = 23;

	/**
	 * Consulta status del servicio
	 */
	public static final int CONSULTA_ESTATUSSERVICIO = 26;

	/**
	 * Cambia perfil del cliente
	 */
	public static final int CAMBIA_PERFIL = 27;

	/**
	 * Compra de tiempo aire
	 */
	public static final int COMPRA_TIEMPO_AIRE = 31;

	/**
	 * Consulta de estatus mantenimiento.
	 */
	public static final int CONSULTA_MANTENIMIENTO = 45;

	/**
	 * Quitar suspension
	 */
	public static final int QUITAR_SUSPENSION = 47;

	/**
	 * Reactivacion
	 */
	public static final int OP_ACTIVACION = 48;

	public static final int OP_ENVIO_CLAVE_ACTIVACION = 51;

	/** Activacion softtoken - Autenticacion */
	public static final int AUTENTICACION_ST = 57;

	/** Solicitar Alertas */
	
	public static final int OP_SOLICITAR_ALERTAS = 66;

		public static final int OP_CONSULTA_INTERBANCARIOS = 87;
		
		/**
		 * Request frequent service payment operation list for BBVA accounts.
		 */
		public static final int FAVORITE_PAYMENT_OPERATION_BBVA = 88;

		
		//Depositos Movil
		public static final int CONSULTA_DEPOSITOS_CHEQUES = 89;//87

		public static final int CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS = 96;//94
		public static final int CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO = 97;//95


		/** consulta importes tarjeta de crÃ©dito **/
		public static final int OP_CONSULTA_TDC = 100;
		/** consulta alta de retiro sin tarjeta**/
		public static final int OP_RETIRO_SIN_TAR = 101;
		public static final int CONSULTA_SIN_TARJETA = 102;
		

		/** consulta alta de retiro sin tarjeta**/
		public static final int OP_RETIRO_SIN_TAR_12_DIGITOS = 103;
		/** sincronizaExportaToken **/
		public static final int OP_SINC_EXP_TOKEN = 104;



	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_VALUE = "BAN2O01";
	
	/**
	 * Operation code parameter for RECORTADO.
	 */
	public static final String OPERATION_CODE_VALUE_RECORTADO = "BREC001";

	/**
	 * Operation code parameter for BMOVIL MEJORADO.
	 */
	public static final String OPERATION_CODE_VALUE_BMOVIL_MEJORADO = "MJRS001";
	
	/**
	 * Operation code parameter for DEPOSITOS
	 */
	public static final String JSON_OPERATION_CODE_VALUE_DEPOSITOS = "O300001";

	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE = "BAN2O05";
	

	//one CLick
		/**
		 * Operation code parameter for new ops.
		 */
		public static final String JSON_OPERATION_CODE_VALUE_ONECLICK = "ONEC001";
		/**
		 * 
		 */
		
		//one CLick
		/**
		 * Operation code parameter for new ops.
		 */
		public static final String JSON_OPERATION_CODE_VALUE_CONSUMO = "ONEC002";
		/**
		 * 
		 */
	
	/**
	 * Operation code parameter for new ops.
	 */
//	public static final String JSON_OPERATION_SOLICITAR_ALERTAS_CODE_VALUE = "BREC001";
	public static final String JSON_OPERATION_RECORTADO_CODE_VALUE = "BREC001";

	/**
	 * Operation code for mejoras
	 */
	public static final String JSON_OPERATION_MEJORAS_CODE_VALUE = "MEJO001";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";




	// ///////////////////////////////////////////////////////////////////////////
	// Parameter definition for application interface //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * The username.
	 */
	public static final String USERNAME_PARAM = "username";

	/**
	 * The password.
	 */
	public static final String PASSWORD_PARAM = "password";
	
	/**
	 * The IUM. SPEI
	 */
	public static final String IUM_PARAM = "ium";

	/**
	 * The version of catalog 1.
	 */
	public static final String VERSION_C1_PARAM = "version_c1";

	/**
	 * The version of catalog 4.
	 */
	public static final String VERSION_C4_PARAM = "version_c4";

	/**
	 * The version of catalog 5.
	 */
	public static final String VERSION_C5_PARAM = "version_c5";

	/**
	 * The version of catalog 8.
	 */
	public static final String VERSION_C8_PARAM = "version_c8";

	/**
	 * The version of catalog Tiempo aire.
	 */
	public static final String VERSION_TA_PARAM = "version_ta";

	/**
	 * The version of catalog Dinero movil.
	 */
	public static final String VERSION_DM_PARAM = "version_dm";
	
	
	/**
	 * The version of catalog Servicios.
	 */
	public static final String VERSION_SV_PARAM = "version_sv";

	public static final String VERSION_AU_PARAM = "version_au";
	//SPEI revisar


	/**
	 * Activation code.
	 */
	public static final String ACTIVATION_CODE_PARAM = "activation";

	/**
	 * User identifier.
	 */
	public static final String USER_ID_PARAM = "userid";

	/**
	 * Marca y modelo que se envÃ­a en Login, se concatenan las cadenas.
	 */
	public static final String MARCA_MODELO = "Marca Modelo";

	public static final String CAT_PARAM = "verCatAutenticacion";
	public static final String TELEFONICAS_PARAM = "verCatTelefonicas";

	/**
	 * Tags JSON*/
	public static final String J_AUT = "cadenaAutenticacion";

		//termina SPEI

	public static String JSON_OPERATION_CONSULTA_INTERBANCARIOS="BXCO001";




	// protected Context mContext;

	/**
	 * Default constructor.
	 */
	public Server() {

	}



//


			
}