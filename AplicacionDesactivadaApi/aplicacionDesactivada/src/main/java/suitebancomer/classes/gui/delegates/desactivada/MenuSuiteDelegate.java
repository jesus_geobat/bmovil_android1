package suitebancomer.classes.gui.delegates.desactivada;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.desactivada.Server;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomer.classes.gui.controllers.desactivada.MenuSuiteViewController;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;

import com.bancomer.mbanking.desactivada.R;
import com.bancomer.mbanking.desactivada.SuiteAppDesac;

public class MenuSuiteDelegate extends BaseDelegate {

    private final String TAG = MenuSuiteDelegate.class.getSimpleName();

	public final static long MENU_SUITE_DELEGATE_ID = 0x3329474451002cd6L; 
	
	private MenuSuiteViewController menuSuiteViewController;
	
	private boolean isCallActive;
	private boolean bMovilSelected;
	
	public void setbMovilSelected(final boolean bMovilSelected) {
		this.bMovilSelected = bMovilSelected;
	}
	
	public boolean isbMovilSelected() {
		return bMovilSelected;
	}
	
	public MenuSuiteDelegate() {
	}
	
	public boolean isCallActive() {
		return isCallActive;
	}
	
	public void setCallActive(final boolean isCallActive) {
		this.isCallActive = isCallActive;
	}
	
	public boolean isDisconnected(){
		SuiteAppDesac suiteApp = SuiteAppDesac.getInstance();
		
		 ConnectivityManager connectivity = (ConnectivityManager) 
				 suiteApp.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		 
         if (connectivity != null){
             NetworkInfo[] info = connectivity.getAllNetworkInfo();
             if (info != null){
                 for (int i = 0; i < info.length; i++){
                     if (info[i].getState() == NetworkInfo.State.CONNECTED){
                         return false;
                     }
                 }
             }
         }
         return true;
	}
	
	public void startBmovilApp() {
		if (SuiteAppDesac.getInstance().getBmovilApplication() == null) {
			SuiteAppDesac.getInstance().startBmovilApp();
		}
	}
	
	public int getBmovilAppStatus(final SuiteAppDesac suiteApp) {
		return suiteApp.getBmovilApplication().getApplicationStatus();
	}
	
	public MenuSuiteViewController getMenuSuiteViewController() {
		return menuSuiteViewController;
	}

	public void setMenuSuiteViewController(final MenuSuiteViewController menuSuiteViewController) {
		this.menuSuiteViewController = menuSuiteViewController;
	}
	
	public void llamarLineaBancomer(final String numeroTel) {
		try {
			isCallActive = true;
	        Intent callIntent = new Intent(Intent.ACTION_CALL);
	        callIntent.setData(Uri.parse(Constants.TEL_URI+numeroTel));
	        menuSuiteViewController.startActivity(callIntent);
	    } catch (ActivityNotFoundException e) {
	    	menuSuiteViewController.showErrorMessage(menuSuiteViewController.getString(R.string.menuSuite_callErrorMessage));
	    }
	}

	
	public void leerContratacionST() {
			
	}

	/**
	 * Metodo para cargar datos implementacion P026 BConnect EA#9, EA#10, EA#11, EA#12,EA#13
	 */
	public void cargaTelSeedKeystore() {
		// Recoger sesion del contexto
		Session session = Session.getInstance(SuiteAppDesac.appContext);

		// Recoger KeyStoreManager
		KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();

		// Inicializa Telefono y seed
		String telefono = null;
		String seed = null;

		try {
			telefono = kswrapper.getUserName(); 
			seed = kswrapper.getSeed();

			if(Server.ALLOW_LOG)
                Log.i("Key", "BConnect MenuSuiteDelegate Username: " + telefono);

			if(Server.ALLOW_LOG)
                Log.i("Key", "BConnect MenuSuiteDelegate Seed: " + seed);
		}
        catch (Exception e1) {
			if(Server.ALLOW_LOG)
                Log.i(TAG, "Error e2 cargaTelSeedKeystore: " + e1.toString());
		}
		
		// Comprobar Bmovil en estatusAplicaciones
		if (PropertiesManager.getCurrent().getBmovilActivated()) {
			// Si bmovil activado, validar telefono e IUM en KeyStore
			if (!Tools.validaSeed(seed) || !Tools.validaTelefono(telefono)) {
				// Si no son validos se desactiva la app
				PropertiesManager.getCurrent().setBmovilActivated(false);
			}
		}
        else {
			// Si Bmovil desactivado o no existe estatusAplicaciones
			// Validar telefono y seed EA#12
			if (Tools.validaSeed(seed) && Tools.validaTelefono(telefono)) {
				// Si los datos son validos borra datos en keychain
				try {
					//Integración KeyChainAPI
					kswrapper.setUserName(" ");
					kswrapper.setSeed(" ");
					kswrapper.storeValueForKey(Constants.CENTRO, " ");
				}
				catch (Exception e2) {
					if(Server.ALLOW_LOG)
                        Log.i(TAG, "Error e2 cargaTelSeedKeystore: " + e2.toString());
				}
			}
		}
	}
}
