package bancomer.api.apiventatdc.commons;

import java.util.Date;

import suitebancomer.aplicaciones.bmovil.classes.model.Account;

public interface VentaTDCSession {

    public String getClientNumber();
    public void setAccounts(Account[] accounts);
    public Date getServerDate();
}
