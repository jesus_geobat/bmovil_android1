package bancomer.api.apiventatdc.gui.delegates;

import android.util.Log;

import java.util.Hashtable;


import bancomer.api.apiventatdc.gui.controllers.DetalleViewController;
import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.apiventatdc.io.Server;
import bancomer.api.apiventatdc.models.AceptaOfertaTDC;
import bancomer.api.apiventatdc.models.OfertaTDC;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by OOROZCO on 8/28/15.
 */
public class DetalleDelegate implements BaseDelegate {

    public static final long DETALLE_DELEGATE_VENTA_TDC = 110909334566765662L;


    private DetalleViewController controller;
    OfertaTDC ofertaTDC;
    Promociones promocion;
    String fechaCatVisual;
    InitVentaTDC init = InitVentaTDC.getInstance();

    public DetalleDelegate(OfertaTDC oferta)
    {
        this.ofertaTDC = oferta;
        this.promocion = init.getOfertaVentaTDC();
    }

    public OfertaTDC getOfertaTDC() {
        return ofertaTDC;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public String getFechaCatVisual() {
        return fechaCatVisual;
    }

    public void formatoFechaCatMostrar(){
        try{
            String[] meses = { "enero", "febrero", "marzo", "abril", "mayo",
                    "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };
            String fechaCatM= ofertaTDC.getFechaCat();
            int mesS=Integer.parseInt(fechaCatM.substring(5,7));
            String dia= fechaCatM.substring(fechaCatM.length()-2);
            String anio= fechaCatM.substring(0,4);

            fechaCatVisual= dia+" de "+meses[mesS-1].toString()+ " de "+anio;

        }catch(Exception ex){
            if(Server.ALLOW_LOG) Log.e("", "error formato" + ex);
        }
    }

    public void realizaOpe(){

        Log.e("realizaOpe", "1");
        ICommonSession session = init.getSession();

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put("operacion", "noAceptacionBMovil");
        params.put("numeroCelular",session.getUsername());
        params.put("cveCamp", promocion.getCveCamp());
        params.put("descripcionMensaje", "10");
        params.put("folioUG", ofertaTDC.getTddAsociada());//ofertaConsumo.getFolioUG()); // definir de donde se obtendra
        params.put("IUM", session.getIum());

        doNetworkOperation(Server.ME_INETERESA_OFERTA_TDC, params, init.getBaseViewController(), true, new AceptaOfertaTDC());
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {

    }

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, final boolean isJson,final ParsingHandler handler) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false, isJson, handler);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (operationId == Server.ME_INETERESA_OFERTA_TDC){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                init.getParentManager().setActivityChanging(true);
                init.showClausulas(getOfertaTDC(), init.getOfertaVentaTDC());
            }else if (response.getStatus() == ServerResponse.OPERATION_ERROR){
                init.getBaseViewController().showErrorMessage(controller,response.getMessageText());
            }
        }
    }

    @Override
    public void performAction(Object obj) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
