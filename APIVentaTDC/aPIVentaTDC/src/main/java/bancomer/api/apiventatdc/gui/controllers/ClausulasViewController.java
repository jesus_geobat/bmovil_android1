package bancomer.api.apiventatdc.gui.controllers;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.adobe.mobile.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bancomer.api.apiventatdc.R;
import bancomer.api.apiventatdc.commons.ConstantsVentaTDC;
import bancomer.api.apiventatdc.commons.TrackerVentaTDC;
import bancomer.api.apiventatdc.gui.delegates.ClausulasDelegate;
import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.apiventatdc.models.Colonia;
import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ControlEventos;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;

/**
 * Created by OOROZCO on 8/31/15.
 */
public class ClausulasViewController extends Activity implements View.OnClickListener, AdapterView.OnItemSelectedListener {


    private BaseViewController baseViewController;
    private InitVentaTDC init = InitVentaTDC.getInstance();
    private ClausulasDelegate delegate;
    public BaseViewsController parentManager;



    /** View Elements **/


    // layout checkbox buro de credito
    private LinearLayout linearChecKBox1;
    private TextView clausula1;
    private CheckBox cbClausula1;

    // layout checkbox aceptacion de tarjeta
    private LinearLayout linearChecKBox2;
    private TextView clausula2;
    private CheckBox cbClausula2;

    // layout checkbox recepcion de tarjeta
    private LinearLayout linearRBEnvioTarjeta;
    /*private TextView txtSucursal;
    private RadioButton rbSucursal;*/

    // layout checkbox domiciliacion
    private LinearLayout linearClausula4;
    private TextView clausula4;
    private CheckBox cbClausula4;
    private TextView clausula4_aviso;
    private LinearLayout linearClausula4_aviso;
    private LinearLayout layoutInfoClausula4;

    // layout checkbox terminos y condiciones
    private LinearLayout linearClausula5;
    private TextView linkClausula5;
    private CheckBox cbClausula5;

    private LinearLayout layout_avisotoken;
    private LinearLayout contenedorContrasena, contenedorNIP,contenedorASM,contenedorCVV,contenedorCampoTarjeta;
    private TextView campoContrasena,campoNIP,campoASM,campoCVV,campoTarjeta;
    private EditText contrasena,nip,asm,cvv,tarjeta;
    private TextView instruccionesContrasena,instruccionesNIP,instruccionesASM,instruccionesCVV,instruccionesTarjeta;

    //Layout Checkbox Domicilio
    private LinearLayout linearClausula6;
    private TextView clausula6;
    private CheckBox cbClausula6;

    private LinearLayout layout_rbDireccion;
    private TextView txtMiDomicilio;
    private RadioButton rbMiDomicilio;
    private TextView txt_miDomicilio;
    private TextView txtSucursal;
    private RadioButton rbSucursal;
    private TextView txtLeyendaSucursal;
    private TextView txtOtroDomicilio;
    private RadioButton rbOtroDomicilio;
    private LinearLayout layout_datos_domicilio;
    private EditText edtCP;
    private EditText edtEstado;
    private EditText edtDelegacion;
    private EditText edtCalle;
    private EditText edtNumExt;
    private EditText edtNumInt;
    private Spinner spinner;

    private LinearLayout vista;
    private ImageButton btnConfirmacion;

    //variables para reglas de negocio
    private boolean isBuro=true;
    private boolean isAceptacionTarjeta=true;
    //private boolean isAnualidad=true;
    private boolean isDomiciliacion=true;
    private boolean isDireccion=true;
    private boolean isOtroDomicilio=false;
    //private boolean isAdicionaTarjeta = true;
    private boolean isSelectedClauses=false;

    private String soloCompras="";
    private String domiciliacion ="";
    private String opcionDomicilio="";

    String textoSucursal;
    String domSucursal;
    int gris1;
    int verde;
    int azul;

    // Datos Domicilio
    String codPostal = "";
    String entidad = "";
    String delegacionMunicipio = "";
    String colonia = "";
    String calle = "";
    String numExt = "";
    String numInt = "";
    String opDomicilio = "";

    //Adobe
    String screen = "";
    ArrayList<String> list= new ArrayList<String>();
    Map<String,Object> operacionMap = new HashMap<String, Object>();


    public String getOpcionDomicilio() {
        return opcionDomicilio;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate=(ClausulasDelegate) parentManager.getBaseDelegateForKey(ClausulasDelegate.CLAUSULAS_VENTA_TDC_DELEGATE);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_venta_tdc_clausulas);
        baseViewController.setTitle(this, R.string.promocion_title_ventaTDC, R.drawable.an_ic_venta_tdc);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        findViews();
        setSecurityComponents();
        scaleForCurrentScreen();

        //Adobe
        Config.setContext(this.getApplicationContext());

        screen = "clausulas venta tdc";
        list.add(screen);
        TrackerVentaTDC.trackState(list);

        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackerVentaTDC.trackEntrada(eventoEntrada);



    }


    @Override
    protected void onPause() {
        super.onPause();
        /*Config.collectLifecycleData();
        if(!init.getParentManager().isActivityChanging())
        {
           // TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }*/

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (DialogInterface.BUTTON_POSITIVE == which) {
                        init.getParentManager().setActivityChanging(true);
                        //init.getParentManager().showMenuInicial();
                        ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                        init.getParentManager().resetRootViewController();
                        init.resetInstance();
                    }else{
                        dialog.dismiss();
                    }

                }
            };

            init.getBaseViewController().showYesNoAlert(this, R.string.promocion_ventatdc_rechazo, listener);

        }
        return true;
    }

    @Override
    public void onUserInteraction() {
        //super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }


    @Override
    protected void onResume() {
        super.onResume();
        Config.pauseCollectingLifecycleData();
        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);

    }



    @Override
    public void onClick(View v) {

        if(v==btnConfirmacion){
            if(isSelectedClauses){
                if(delegate.enviaPeticionOperacion())
                {
                    init.getParentManager().setActivityChanging(true);
                }

            }else
                init.getBaseViewController().showInformationAlert(ClausulasViewController.this,getResources().getString(R.string.clausulas_ventatdc_alerta_clausulas));

        }
        else if(v == linkClausula5){
            init.getParentManager().setActivityChanging(true);
            delegate.realizaOperacionTerminosYCondiciones();
        }
    }


    public void onCheckboxClick(View v){


        if(v==cbClausula1){
            if(cbClausula1.isChecked()){
                cbClausula1.setClickable(false);
            }
        }else if(v==cbClausula2){
            if(!cbClausula1.isClickable()){

                cbClausula2.setClickable(false);
                cbClausula2.setBackgroundResource(R.drawable.an_ic_check_on);
                //linearRBEnvioTarjeta.setVisibility(View.VISIBLE);
                setOpcionDomicilio("sucursal");
                setSoloCompras("NO");
            }else{
                if(isBuro){
                    init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.clausulas_ventatdc_alerta_buro));
                    cbClausula2.setBackgroundResource(R.drawable.an_ic_check_off);
                }
            }
        }
        else if(v == cbClausula4){
            if(!cbClausula1.isClickable() && !cbClausula2.isClickable()){
                linearRBEnvioTarjeta.setVisibility(View.GONE);
                cbClausula4.setBackgroundResource(R.drawable.an_ic_check_on);
                cbClausula4.setClickable(false);
                setDomiciliacion("minimo");
                linearClausula4_aviso.setVisibility(View.VISIBLE);
                layoutInfoClausula4.setVisibility(View.VISIBLE);
            }
            else{
                if(isAceptacionTarjeta){
                    init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.clausulas_ventatdc_alerta_aceptacionTarjeta));
                    cbClausula4.setBackgroundResource(R.drawable.an_ic_check_off);
                }
            }
        }
        else if(v == cbClausula6){
            if(!cbClausula1.isClickable() && !cbClausula2.isClickable() && !cbClausula4.isClickable()){
                String domicilio = String.valueOf(delegate.getOferta().getDomicilio());
                Log.e("Mi domicilio", String.valueOf(delegate.getOferta().getDomicilio()));

                cbClausula6.setBackgroundResource(R.drawable.an_ic_check_on);
                cbClausula6.setClickable(false);
                linearClausula4_aviso.setVisibility(View.GONE);
                layoutInfoClausula4.setVisibility(View.GONE);
                layout_rbDireccion.setVisibility(View.VISIBLE);
                txt_miDomicilio.setVisibility(View.VISIBLE);
                txt_miDomicilio.setText(domicilio);
                opDomicilio = ConstantsVentaTDC.OPCION_MIDOMICILIO_VALUE;
                rbMiDomicilio.setChecked(true);
            }
            else{
                if(isDireccion){
                    init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.clausulas_ventatdc_alerta_direccion));
                    cbClausula6.setBackgroundResource(R.drawable.an_ic_check_off);
                }
            }
        }
        else if(v == rbMiDomicilio){
            rbMiDomicilio.setChecked(true);
            rbSucursal.setChecked(false);
            rbOtroDomicilio.setChecked(false);
            layout_datos_domicilio.setVisibility(View.GONE);
            txtLeyendaSucursal.setVisibility(View.GONE);
            txt_miDomicilio.setVisibility(View.VISIBLE);
            txt_miDomicilio.setText(String.valueOf(delegate.getOferta().getDomicilio()));
            opDomicilio = ConstantsVentaTDC.OPCION_MIDOMICILIO_VALUE;
            isOtroDomicilio=false;
        }
        else if(v == rbOtroDomicilio){
            rbMiDomicilio.setChecked(false);
            rbSucursal.setChecked(false);
            rbOtroDomicilio.setChecked(true);
            txt_miDomicilio.setVisibility(View.GONE);
            txtLeyendaSucursal.setVisibility(View.GONE);
            layout_datos_domicilio.setVisibility(View.VISIBLE);
            edtCP.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onTextChanged(CharSequence s, int a, int b, int c) {
                    // TODO Auto-generated method stub
                    Log.e("CP:", s.toString());
                    if (s.toString().length() == 5) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edtCP.getWindowToken(), 0);
                        delegate.realizaOperacionColonias(edtCP.getText().toString());
                    }
                }
            });
            opDomicilio = ConstantsVentaTDC.OPCION_MIDOMICILIO_VALUE;
            isOtroDomicilio=true;
        }
        else if(v == rbSucursal){
            rbMiDomicilio.setChecked(false);
            rbSucursal.setChecked(true);
            rbOtroDomicilio.setChecked(false);
            layout_datos_domicilio.setVisibility(View.GONE);
            txt_miDomicilio.setVisibility(View.GONE);
            txtLeyendaSucursal.setVisibility(View.VISIBLE);
            opDomicilio = ConstantsVentaTDC.OPCION_DOMICILIO_VALUE;
            isOtroDomicilio=false;
        }
        else if(v == cbClausula5){
            if(!cbClausula1.isClickable() && !cbClausula2.isClickable() && !cbClausula4.isClickable() && !cbClausula6.isClickable()){
                if(isOtroDomicilio == true){
                    if(edtCP.getText().length()!=0 && edtCalle.getText().length()!=0 && edtNumExt.getText().length()!=0){
                        calle = edtCalle.getText().toString();
                        numExt = edtNumExt.getText().toString();
                        numInt = edtNumInt.getText().toString();

                        cbClausula5.setBackgroundResource(R.drawable.an_ic_check_on);
                        cbClausula5.setClickable(false);
                        layout_rbDireccion.setVisibility(View.GONE);
                        isSelectedClauses = true;
                    }else if(edtCP.getText().length() == 0){
                        init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.clausulas_ventatdc_alerta_cpVacio));
                        cbClausula5.setBackgroundResource(R.drawable.an_ic_check_off);
                    }else if(edtCalle.getText().length() == 0){
                        init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.clausulas_ventatdc_alerta_calle));
                        cbClausula5.setBackgroundResource(R.drawable.an_ic_check_off);
                    }else{
                        init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.clausulas_ventatdc_alerta_numExtVacio));
                        cbClausula5.setBackgroundResource(R.drawable.an_ic_check_off);
                    }
                }else{
                    cbClausula5.setBackgroundResource(R.drawable.an_ic_check_on);
                    cbClausula5.setClickable(false);
                    layout_rbDireccion.setVisibility(View.GONE);
                    isSelectedClauses = true;

                    /*linearClausula4_aviso.setVisibility(View.GONE);
                    layoutInfoClausula4.setVisibility(View.GONE);*/
                }
            }
            else{
                if(isDomiciliacion){
                    init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.clausulas_ventatdc_alerta_domiciliacion));
                    //init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.clausulas_ventatdc_alerta_direccion));
                    cbClausula5.setBackgroundResource(R.drawable.an_ic_check_off);
                }
            }
        }
    }

    public void insertDatosCP(){
        Log.e("DELEGACION", String.valueOf(delegate.getColonia().getDelegacion()));
        Log.e("Entidad", String.valueOf(delegate.getColonia().getEntidadFed()));

        edtEstado.setText(delegate.getColonia().getEntidadFed());
        edtDelegacion.setText(delegate.getColonia().getDelegacion());
        entidad = String.valueOf(edtEstado.getText());
        delegacionMunicipio = String.valueOf(edtDelegacion.getText());
        codPostal = String.valueOf(edtCP.getText());

        Colonia[] arrColonias = delegate.getColonia().getColonias();
        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.spColonia);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        // Spinner Drop down elements
        List<String> colonias = new ArrayList<String>();

        for(int i=0; i<arrColonias.length; i++){
            colonias.add(arrColonias[i].getNombreColonia());
            Log.e("Colonias NOM", String.valueOf(arrColonias[i].getNombreColonia()));
        }
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, colonias);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        // Showing selected spinner item
        //Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
        colonia = item;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void findViews(){

        gris1 = getResources().getColor(R.color.gris_texto_1);
        verde = getResources().getColor(R.color.verde);
        azul  = getResources().getColor(R.color.primer_azul);

        //buro de credito
        linearChecKBox1 = (LinearLayout) findViewById(R.id.linearChecKBox1);
        clausula1 = (TextView) findViewById(R.id.clausula1);
        cbClausula1 = (CheckBox) findViewById(R.id.cbClausula1);

        //aceptacion de tarjeta
        linearChecKBox2 = (LinearLayout) findViewById(R.id.linearChecKBox2);

        clausula2 = (TextView) findViewById(R.id.clausula2);
        String txtAceptaTarjeta= getString(R.string.clausulas_ventatdc_txt3_aceptaTarjeta);
        String tipoTarjeta= delegate.getOferta().getProducto();
        
        String importe;
        if (!ServerCommons.ARQSERVICE) {
            importe = Tools.formatAmount(delegate.getPromocion().getMonto(),false);
        } else {
            importe = Tools.formatAmountCarruselArq(delegate.getPromocion().getMonto(), false);
        }
        SpannableString textoImporte = new SpannableString(importe);
        textoImporte.setSpan(new UnderlineSpan(), 0, textoImporte.length(), 0);
        clausula2.setText(Html.fromHtml("<font color='" + gris1 + "'>" + txtAceptaTarjeta + "</font>"
                                        + "<b><font color='" + verde + "'>" + " " + importe + " " + "</font></b>"), TextView.BufferType.SPANNABLE);
        
        cbClausula2 = (CheckBox) findViewById(R.id.cbClausula2);



        //recepcion de tarjeta
        linearRBEnvioTarjeta = (LinearLayout) findViewById(R.id.linearRBEnvioTarjeta);

        /*txtSucursal = (TextView) findViewById(R.id.txtSucursal);
        textoSucursal = getResources().getString(R.string.bmovil_clausulas_ventatdc_txt3_2);
        domSucursal = delegate.getOferta().getDomicilioSucursal();
        txtSucursal.setText(Html.fromHtml("<font color='"+azul+"'>"+textoSucursal+"</font><br/>"
                + "<font color='"+gris1+"'>"+domSucursal+"</font>"), TextView.BufferType.SPANNABLE);*/



        linearClausula4 = (LinearLayout) findViewById(R.id.linearClausula4);
        clausula4 = (TextView) findViewById(R.id.clausula4);
        String txtDomiciliacion = getResources().getString(R.string.clausulas_ventatdc_txt5);
        String tipoPago = getResources().getString(R.string.clausulas_ventatdc_txt5_1);
        clausula4.setText(Html.fromHtml("<font color='"+gris1+"'>"+txtDomiciliacion+"</font>"
                + "<b><font color='"+verde+"'>"+" "+tipoPago+" "+"</font></b>"), TextView.BufferType.SPANNABLE);
        cbClausula4 = (CheckBox) findViewById(R.id.cbClausula4);

        linearClausula4_aviso = (LinearLayout) findViewById(R.id.linearClausula4_aviso);
        clausula4_aviso = (TextView) findViewById(R.id.clausula4_aviso);
        String txtAvisoDom = getResources().getString(R.string.clausulas_ventatdc_txt5_Domiciliacion);


        String teminacionTarjeta  = init.getCuentaOneClick().substring(init.getCuentaOneClick().length()-5,init.getCuentaOneClick().length());

        clausula4_aviso.setText(txtAvisoDom+" *"+teminacionTarjeta);
        layoutInfoClausula4 = (LinearLayout) findViewById(R.id.layoutInfoClausula4);

        //layout checkbox terminos y condiciones
        linearClausula5 = (LinearLayout) findViewById(R.id.linearClausula5);

        linkClausula5 = (TextView) findViewById(R.id.linkClausula5);
        String txtClausula6 = getString(R.string.clausulas_ventatdc_terminosycondiciones);
        SpannableString textolinkClausula6 = new SpannableString(txtClausula6);
       //MAPE textolinkClausula6.setSpan(new UnderlineSpan(), 0, txtClausula6.length(), 0);
        linkClausula5.setText(textolinkClausula6);
        linkClausula5.setOnClickListener(this);
        cbClausula5 = (CheckBox) findViewById(R.id.cbClausula5);

        //Layout CheckBox Domicilio
        linearClausula6 = (LinearLayout) findViewById(R.id.linearClausula6);
        clausula6 = (TextView) findViewById(R.id.clausula6);
        cbClausula6 = (CheckBox) findViewById(R.id.cbClausula6);

        layout_rbDireccion = (LinearLayout) findViewById(R.id.layout_rbDireccion);
        txtMiDomicilio = (TextView) findViewById(R.id.txtMiDomicilio);
        rbMiDomicilio = (RadioButton) findViewById(R.id.rbMiDomicilio);
        txt_miDomicilio = (TextView) findViewById(R.id.txt_miDomicilio);
        txtSucursal = (TextView) findViewById(R.id.txtSucursal);
        rbSucursal = (RadioButton) findViewById(R.id.rbSucursal);
        txtLeyendaSucursal = (TextView) findViewById(R.id.txtLeyendaSucursal);
        txtOtroDomicilio = (TextView) findViewById(R.id.txtOtroDomicilio);
        rbOtroDomicilio = (RadioButton) findViewById(R.id.rbOtroDomicilio);
        layout_datos_domicilio = (LinearLayout) findViewById(R.id.layout_datos_domicilio);
        edtCP = (EditText) findViewById(R.id.edtCP);
        edtEstado = (EditText) findViewById(R.id.edtEstado);
        edtDelegacion = (EditText) findViewById(R.id.edtDelegacion);
        spinner = (Spinner) findViewById(R.id.spColonia);
        edtCalle = (EditText) findViewById(R.id.edtCalle);
        edtNumExt = (EditText) findViewById(R.id.edtNumExt);
        edtNumInt = (EditText) findViewById(R.id.edtNumInt);

        //credenciales autenticacion
        layout_avisotoken	= (LinearLayout)findViewById(R.id.layout_avisotoken);
        LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);

        contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_confirmacion_contrasena_layout);
        contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_confirmacion_nip_layout);
        contenedorASM 			= (LinearLayout)findViewById(R.id.campo_confirmacion_asm_layout);
        contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_confirmacion_cvv_layout);

        contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
        nip 					= (EditText)contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
        asm						= (EditText)contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
        cvv						= (EditText)contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);

        campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
        campoNIP				= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_label);
        campoASM				= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_label);
        campoCVV				= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_label);

        instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
        instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
        instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
        instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);

        contenedorCampoTarjeta  = (LinearLayout)findViewById(R.id.campo_confirmacion_campotarjeta_layout);
        tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
        campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
        instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);

        setSecurityComponents();
        //END credenciales autenticacion

        //boton confirmar
        btnConfirmacion = (ImageButton)findViewById(R.id.confirmacion_confirmar_button);
        btnConfirmacion.setOnClickListener(this);
    }


    private void setSecurityComponents()
    {
        mostrarContrasena(delegate.mostrarContrasenia());
        mostrarNIP(delegate.mostrarNIP());
        mostrarASM(delegate.tokenAMostrar());
        mostrarCVV(delegate.mostrarCVV());

        LinearLayout parentContainer = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);

        if (contenedorContrasena.getVisibility() == View.GONE &&
                contenedorNIP.getVisibility() == View.GONE &&
                contenedorASM.getVisibility() == View.GONE &&
                contenedorCVV.getVisibility() == View.GONE &&
                contenedorCampoTarjeta.getVisibility() == View.GONE) {
            parentContainer.setBackgroundColor(0);
            parentContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        }

        contrasena.addTextChangedListener(listener);
        nip.addTextChangedListener(listener);
        asm.addTextChangedListener(listener);
        cvv.addTextChangedListener(listener);
        tarjeta.addTextChangedListener(listener);

    }



    public void mostrarContrasena(boolean visibility){
        contenedorContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        campoContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
            layout_avisotoken.setVisibility(View.VISIBLE);
            campoContrasena.setText(delegate.getEtiquetaCampoContrasenia());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
            contrasena.setFilters(userFilterArray);
            contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
        instruccionesContrasena.setVisibility(View.GONE);
    }


    public void mostrarNIP(boolean visibility){
        contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
        campoNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
        nip.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
            layout_avisotoken.setVisibility(View.VISIBLE);
            campoNIP.setText(delegate.getEtiquetaCampoNip());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
            nip.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(tarjeta);
            nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = delegate.getTextoAyudaNIP();
            if (instrucciones.equals("")) {
                instruccionesNIP.setVisibility(View.GONE);
            } else {
                instruccionesNIP.setVisibility(View.VISIBLE);
                instruccionesNIP.setText(instrucciones);
            }
        } else {
            nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }


    public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){
        switch(tipoOTP) {
            case ninguno:
                contenedorASM.setVisibility(View.GONE);
                campoASM.setVisibility(View.GONE);
                asm.setVisibility(View.GONE);
                asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
                break;
            case codigo:
            case registro:
                layout_avisotoken.setVisibility(View.VISIBLE);
                contenedorASM.setVisibility(View.VISIBLE);
                campoASM.setVisibility(View.VISIBLE);
                asm.setVisibility(View.VISIBLE);
                InputFilter[] userFilterArray = new InputFilter[1];
                userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
                asm.setFilters(userFilterArray);
                cambiarAccionTexto(contrasena);
                cambiarAccionTexto(tarjeta);
                cambiarAccionTexto(nip);
                asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
                break;
        }

        Constants.TipoInstrumento tipoInstrumento = delegate.getTipoInstrumentoSeguridad();

        switch (tipoInstrumento) {
            case OCRA:
                campoASM.setText(delegate.getEtiquetaCampoOCRA());
                asm.setTransformationMethod(null);
                break;
            case DP270:
                campoASM.setText(delegate.getEtiquetaCampoDP270());
                asm.setTransformationMethod(null);
                break;
            case SoftToken:
                if(init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
                    asm.setText(Constants.DUMMY_OTP);
                    asm.setEnabled(false);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
                } else {
                    asm.setText("");
                    asm.setEnabled(true);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
                    asm.setTransformationMethod(null);
                }

                break;
            default:
                break;
        }
        String instrucciones = delegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento,(init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()),delegate.tokenAMostrar());
        if (instrucciones.equals("")) {
            instruccionesASM.setVisibility(View.GONE);
        } else {
            instruccionesASM.setVisibility(View.VISIBLE);
            instruccionesASM.setText(instrucciones);
        }
    }



    public void mostrarCVV(boolean visibility){
        contenedorCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
        campoCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
        cvv.setVisibility(visibility ? View.VISIBLE:View.GONE);
        instruccionesCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);

        if (visibility) {
            layout_avisotoken.setVisibility(View.VISIBLE);
            campoCVV.setText(delegate.getEtiquetaCampoCVV());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
            cvv.setFilters(userFilterArray);
            String instrucciones = delegate.getTextoAyudaCVV();
            instruccionesCVV.setText(instrucciones);
            instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(nip);
            cambiarAccionTexto(asm);
            cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    private void cambiarAccionTexto(EditText campo) {
        if (campo.getVisibility() == View.VISIBLE) {
            campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        }
    }



    public String pideContrasena() {
        if (contrasena.getVisibility() == View.GONE) {
            return "";
        } else {
            return contrasena.getText().toString();
        }
    }

    public String pideNIP() {
        if (nip.getVisibility() == View.GONE) {
            return "";
        } else {
            return nip.getText().toString();
        }
    }

    public String pideASM() {
        if (asm.getVisibility() == View.GONE) {
            return "";
        } else {
            return asm.getText().toString();
        }
    }

    public String pideCVV() {
        if (cvv.getVisibility() == View.GONE) {
            return "";
        } else {
            return cvv.getText().toString();
        }
    }

    public String getCodPostal() {
        return codPostal;
    }

    public String getEntidad() {
        return entidad;
    }

    public String getDelegacionMunicipio() {
        return delegacionMunicipio;
    }

    public String getColonia() {
        return colonia;
    }

    public String getCalle() {
        return calle;
    }

    public String getNumExt() {
        return numExt;
    }

    public String getNumInt() {
        return numInt;
    }

    public String getOpDomicilio() {
        return opDomicilio;
    }

    private TextWatcher listener = new TextWatcher() {

        public void afterTextChanged(Editable s) {
            ClausulasViewController.this.onUserInteraction();

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        public void onTextChanged(CharSequence s, int start, int before, int count) {}
    };

    private void scaleForCurrentScreen() {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(findViewById(R.id.lblTextoInfoClausulas), true);
        gTools.scale(findViewById(R.id.clausula1), true);
        gTools.scale(findViewById(R.id.cbClausula1));
        gTools.scale(findViewById(R.id.clausula2), true);
        gTools.scale(findViewById(R.id.cbClausula2));
        gTools.scale(findViewById(R.id.txtEnvioTarjeta),true);
        gTools.scale(findViewById(R.id.clausula4), true);
        gTools.scale(findViewById(R.id.cbClausula4));
        gTools.scale(findViewById(R.id.clausula4_aviso), true);
        gTools.scale(findViewById(R.id.imgInfoClausula4));
        gTools.scale(findViewById(R.id.txtInfoClausula4), true);
        gTools.scale(findViewById(R.id.linkClausula5), true);
        gTools.scale(findViewById(R.id.txtClausula5), true);
        gTools.scale(findViewById(R.id.cbClausula5));
        gTools.scale(findViewById(R.id.imgaviso_token));
        gTools.scale(findViewById(R.id.txtaviso_token), true);
        gTools.scale(findViewById(R.id.confirmacion_contrasena_edittext), true);
        gTools.scale(findViewById(R.id.confirmacion_nip_edittext), true);
        gTools.scale(findViewById(R.id.confirmacion_asm_edittext), true);
        gTools.scale(findViewById(R.id.confirmacion_cvv_edittext), true);
        gTools.scale(findViewById(R.id.confirmacion_campotarjeta_edittext), true);
        gTools.scale(findViewById(R.id.confirmacion_contrasena_label), true);
        gTools.scale(findViewById(R.id.confirmacion_nip_label), true);
        gTools.scale(findViewById(R.id.confirmacion_asm_label), true);
        gTools.scale(findViewById(R.id.confirmacion_cvv_label), true);
        gTools.scale(findViewById(R.id.confirmacion_campotarjeta_label), true);
        gTools.scale(findViewById(R.id.confirmacion_contrasena_instrucciones_label), true);
        gTools.scale(findViewById(R.id.confirmacion_nip_instrucciones_label), true);
        gTools.scale(findViewById(R.id.confirmacion_asm_instrucciones_label), true);
        gTools.scale(findViewById(R.id.confirmacion_cvv_instrucciones_label), true);
        gTools.scale(findViewById(R.id.confirmacion_campotarjeta_instrucciones_label), true);
        gTools.scale(btnConfirmacion);
        gTools.scale(linearClausula6);
        gTools.scale(clausula6);
        gTools.scale(cbClausula6);

        gTools.scale(layout_rbDireccion);
        gTools.scale(txtMiDomicilio);
        gTools.scale(rbMiDomicilio);
        gTools.scale(txt_miDomicilio);
        gTools.scale(txtSucursal);
        gTools.scale(rbSucursal);
        gTools.scale(txtLeyendaSucursal);
        gTools.scale(txtOtroDomicilio);
        gTools.scale(rbOtroDomicilio);

    }


    public void setSoloCompras(String soloCompras) {
        this.soloCompras = soloCompras;
    }

    public String getDomiciliacion() {
        return domiciliacion;
    }

    public void setDomiciliacion(String domiciliacion) {
        this.domiciliacion = domiciliacion;
    }

    public void setOpcionDomicilio(String opcionDomicilio) {
        this.opcionDomicilio = opcionDomicilio;
    }

    @Override
    protected void onStop() {
        if(ControlEventos.isAppIsInBackground(this.getBaseContext())){ //Valida el estado de la app si es change Activity no hace nada
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "La app se fue a Background");
            }
            TimerController.getInstance().initTimer(this, TimerController.getInstance().getClase());

        }
        super.onStop();
    }

}
