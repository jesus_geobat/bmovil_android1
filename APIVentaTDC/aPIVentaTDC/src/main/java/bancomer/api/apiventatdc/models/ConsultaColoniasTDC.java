package bancomer.api.apiventatdc.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.apiventatdc.io.Server;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by SDIAZ on 14/03/16.
 */
public class ConsultaColoniasTDC implements ParsingHandler {

    String delegacion;
    String entidadFed;
    Colonia[] colonias;

    public ConsultaColoniasTDC() {
        super();
    }

    public ConsultaColoniasTDC(String delegacion, String entidadFed, Colonia[] colonias) {
        this.delegacion = delegacion;
        this.entidadFed = entidadFed;
        this.colonias = colonias;
    }

    public String getDelegacion() {
        return delegacion;
    }

    public void setDelegacion(String delegacion) {
        this.delegacion = delegacion;
    }

    public String getEntidadFed() {
        return entidadFed;
    }

    public void setEntidadFed(String entidadFed) {
        this.entidadFed = entidadFed;
    }

    public Colonia[] getColonias() {
        return colonias;
    }

    public void setColonias(Colonia[] colonias) {
        this.colonias = colonias;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        Log.e("modelo: process(ParserJSON parser)", "OK");

        delegacion = parser.hasValue("delegacion")?parser.parseNextValue("delegacion"):"";
        entidadFed = parser.hasValue("entidadFed")?parser.parseNextValue("entidadFed"):"";

        try {

                //JSONObject jsonObjectColonias = new JSONObject(parser.parseNextValue("colonias"));
                //JSONArray arrPromocionesJson = jsonObjectPromociones.getJSONArray("colonias");
                JSONArray arrColoniasJson = new JSONArray(parser.parseNextValue("colonias"));
                ArrayList<Colonia> arrColonias = new ArrayList<Colonia>();
                for (int i = 0; i < arrColoniasJson.length(); i++) {
                    JSONObject jsonPromocion = (JSONObject) arrColoniasJson.get(i);
                    Colonia col = new Colonia();
                    col.setCveColonia(jsonPromocion.getString("cveColonia"));
                    col.setNombreColonia(jsonPromocion.getString("nombreColonia"));

                    arrColonias.add(col);
                }

                this.colonias = arrColonias.toArray(new Colonia[arrColonias.size()]);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            if(Server.ALLOW_LOG) e.printStackTrace();
        }
    }
}
