package bancomer.api.apiventatdc.models;

/**
 * Created by SDIAZ on 14/03/16.
 */
public class Colonia {
    String cveColonia;
    String nombreColonia;

    public Colonia(){
        super();
    }

    public String getCveColonia() {
        return cveColonia;
    }

    public void setCveColonia(String cveColonia) {
        this.cveColonia = cveColonia;
    }

    public String getNombreColonia() {
        return nombreColonia;
    }

    public void setNombreColonia(String nombreColonia) {
        this.nombreColonia = nombreColonia;
    }

}
