package bancomer.api.apiventatdc.models;

import java.io.IOException;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by OOROZCO on 9/1/15.
 */
public class FormatosFinalesVentaTDC implements ParsingHandler{
    /**
     * Formatos Finales en formato HTML.
     */
    private String formatoHTML;


    /**
     * @return Formatos Finales en formato HTML.
     */
    public String getFormatoHTML() {
        return formatoHTML;
    }

    /**
     * @param terminosHtml Terminos de uso en formato HTML.
     */
    public void setFormatoHTML(String formatoHTML) {
        this.formatoHTML = formatoHTML;
    }


    public FormatosFinalesVentaTDC() {
        formatoHTML = null;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        formatoHTML = null;
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        formatoHTML = parser.parseNextValue("txtHTML1").replace("\u0093", "\"").replace("\u0094", "\"");


    }
}

