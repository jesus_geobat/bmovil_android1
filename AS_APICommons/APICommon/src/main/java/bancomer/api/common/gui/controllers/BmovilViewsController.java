package bancomer.api.common.gui.controllers;


import android.accounts.Account;


public interface BmovilViewsController {

	
	public void cierraViewsController();

	public void cerrarSesionBackground();


	public void showMenuPrincipal();

	public void showMenuPrincipal(boolean inverted);

	public void showLogin();

	public void showPantallaActivacion();

	public void showBorrarDatos();

	public void showCambioPerfil();

	// Alberto
	//public BmovilApp getBmovilApp();

	// Alberto
	//public void setBmovilApp(BmovilApp bmovilApp);

	public void showMisCuentasViewController();

	public void showConsultarMovimientos();
	
	public void showConsultarDepositosRecibidosCuenta(Account cuenta);
	
	public void showConsultarDepositosRecibidos();
	
	public void showConsultarObtenerComprobante();


	/**
	 * Muestra la ventana de consultar envios de dinero movil.
	 */
	public void showConsultarDineroMovil();

	public void showCuentaOrigenListViewController();

	public void showDetalleMovimientosViewController();

/*	@Override
	public void onUserInteraction();

	@Override
	public boolean consumeAccionesDeReinicio();

	@Override
	public boolean consumeAccionesDePausa();

	@Override
	public boolean consumeAccionesDeAlto();*/

	/*
	 * Muestra el menu de administrar
	 */
	public void showMenuAdministrar();

	/*
	 * Muestra la pantalla de acerca de...
	 */
	public void showAcercaDe();

	/*
	 * Muestra la pantalla de Novedades
	 */
	public void showNovedades();
	
	public void showNovedadesLogin();
	
	/*
	 * muestra la pantalla para cambiar password
	 */
	public void showCambiarPassword();

	/*
	 * Muestra la pantalla con el resultado del cambio de password
	 */
	public void showCambiarPasswordResultado();

//	/**
//	 * Muestra la pantalla de confirmacion autenticacion
//	 */
//	public void showConfirmacionAutenticacionViewController(
//			DelegateBaseAutenticacion autenticacionDelegate, int resIcon,
//			int resTitle, int resSubtitle);
//
//	/**
//	 * Muestra la pantalla de confirmacion autenticacion
//	 */
//	public void showConfirmacionAutenticacionViewController(
//			DelegateBaseAutenticacion autenticacionDelegate, int resIcon,
//			int resTitle, int resSubtitle, int resTitleColor);
//
//	/**
//	 * Muestra la pantalla de resultados
//	 */
//	public void showResultadosViewController(
//			DelegateBaseOperacion delegateBaseOperacion, int resIcon,
//			int resTitle);
//	
//	public void showResultadosViewControllerWithoutFreq(
//			DelegateBaseOperacion delegateBaseOperacion, int resIcon,
//			int resTitle);
//
//	public void showOpcionesTransferir(String tipoOperacion, int resIcon,
//			int resTitle, int resSubtitle);
//
//	/**
//	 * Muestra la pantalla de transferencia mis cuentas
//	 */
//	public void showTransferirMisCuentas();
//
//	/**
//	 * Muestra la pantalla de transferencia entre mis cuentas detalle
//	 */
//	public void showTransferirMisCuentasDetalle();
//
//	/**
//	 * Muestra la pantalla de confirmacion para transferencias.
//	 * 
//	 * @param delegateBaseOperacion
//	 *            EL DelegateBaseOperacion que manda a llamar la pantalla de
//	 *            confirmaci�n.
//	 */
//	public void showConfirmacion(DelegateBaseOperacion delegateBaseOperacion);
//
//	/*
//	 * Muestra la pantalla para alta de frecuente
//	 */
//	public void showAltaFrecuente(DelegateBaseOperacion delegateBaseOperacion);
//
//	/*
//	 * Muestra la pantalla de transferViewController gen�rica para transferir
//	 * comprar pagar consultar
//	 */
//	public void showTransferViewController(String tipoOperacion,
//			boolean isExpress, boolean isTDC);
//
//	/*
//	 * Muestra la pantalla de transferViewController gen�rica para transferir
//	 * comprar pagar consultar
//	 */
//	public void showSelectTDCViewController(String tipoOperacion,
//			boolean isExpress, boolean isTDC);
//	
//	/*
//	 * Muestra la pantalla de InterbancariosViewController
//	 */
//	public void showInterbancariosViewController(Object obj);
//
//	public void showOpcionesPagar();
//
//	public void showTipoConsultaFrecuentes();
//
//	/*
//	 * Muestra la pantalla de PagoServiciosViewController
//	 */
//	public void showPagosViewController(Constants.Operacion tipoOperacion);
//
//	/*
//	 * 
//	 */
//	public void showConsultaFrecuentes(String tipoFrecuente);
//
//	/*
//	 * Muestra la pantalla del menu consultar
//	 */
//	public void showMenuConsultar();
//
//	/*
//	 * Muestra la pantalla de PagoServiciosViewController
//	 */
//	public void showPagoServiciosViewController(PagoServicio pagoServicio);
//
//	public void showPagoServiciosAyudaViewController(String[] llaves,
//			Object[] valores);
//
//	public void showLecturaCodigoViewController(int codigoActividad);
//
//	/**
//	 * Muestra la pantalla de tranferencia de dinero m�vil.
//	 */
//	public void showTransferirDineroMovil(TransferenciaDineroMovil frecuente,
//			boolean bajaDM);
//
//	/**
//	 * 
//	 * @param rapido
//	 */
//	public void showTransferirDineroMovil(Rapido rapido);
//
//	/*
//	 * Muestra la pantalla del Menu Comprar
//	 */
//	public void showComprarViewController();
//
//	public void showTiempoAireViewController(Rapido rapido);
//
//	/*
//	 * Muestra la pantalla de Tiempo Aire
//	 */
//	public void showTiempoAireViewController(Constants.Operacion tipoOpracion);
//
//	/*
//	 * Muestra la segunda pantalla de Tiempo Aire
//	 */
//	public void showTiempoAireDetalleViewController();
//
//	/*
//	 * Muestra la pantalla de Transferencias terceros
//	 */
//	public void showOtrosBBVAViewController(Object obj, boolean esExpress,
//			boolean esTDC);
//
//	public void showOtrosBBVAViewController(Rapido rapido);
//	
//	/**
//	 * Muestra la pantalla de detalles de un movimiento de Obtener Comprobante.
//	 */
//	public void showDetalleObtenerComprobante();
//	
//	/**
//	 * Muestra la pantalla de detalles de un movimiento de Depositos Recibidos.
//	 */
//	public void showDetalleDepositosRecibidos();
//
//
//	/**
//	 * Muestra la pantalla de detalles de un movimiento de dinero movil.
//	 */
//	public void showDetalleDineroMovil();
//
//	@Override
//	public void removeDelegateFromHashMap(long key);
//
//	public void showListaBancos(int codigoActividad);
//
//	/**
//	 * Muestra la pantalla para cambio de telefono asociado
//	 */
//	public void showCambioDeTelefono();
//
//	/**
//	 * Muestra la pantalla para cambio de cuenta asociado
//	 */
//	public void showCambioCuentaAsociada();
//
//	/**
//	 * Muestra la pantalla del menu Suspender / Cancelar
//	 */
//	public void showMenuSuspenderCancelar();
//
//	/**
//	 * Muestra la pantalla de ingresar datos de contratación.
//	 */
//	public void showContratacion(ConsultaEstatus consultaEstatus, String estatusServicio, boolean deleteData);
//
//	/**
//	 * Ejecuta flujo
//	 * 
//	 */
//	public void showContratacionEP11(BaseViewController ownerController, ConsultaEstatus consultaEstatus,
//			String estatusServicio, boolean deleteData);
//
//
//	/**
//	 * Muestra la pantalla de ingresar datos de contratación.
//	 */
//	public void showContratacion();
//
//	/**
//	 * Muestra la pantalla de reactivación.
//	 */
//	public void showReactivacion(ConsultaEstatus consultaEstatus);
//
//	/**
//	 * Muestra la pantalla de reactivación.
//	 */
//	public void showReactivacion(Reactivacion reactivacion);
//
//	public void showReactivacion(ConsultaEstatus consultaEstatus,
//			String password);
//
//	/**
//	 * Muestra la pantalla de definir contraseña la contratación.
//	 */
//	public void showDefinirPassword();
//
//	/**
//	 * Muestra la pantalla de ayuda para agregar una operación rápida.
//	 */
//	public void showAyudaAgregarRapido();
//
//	/**
//	 * Muestra la pantalla de configurar montos con los limites elegidos por el
//	 * usuario.
//	 */
//	public void showConfigurarMontos(ConfigurarMontos cm);
//
//	/**
//	 * Muestra la pantalla de resultados
//	 */
//	public void showResultadosAutenticacionViewController(
//			DelegateBaseOperacion delegateBaseOperacion, int resIcon,
//			int resTitle);
//
//	/**
//	 * Muestra la pantalla para desbloqueo
//	 * 
//	 * @param ConsultaEstatus
//	 */
//	public void showDesbloqueo(ConsultaEstatus ce);
//
//	/**
//	 * Muestra la confirmacion para quitar la suspension
//	 * 
//	 * @param ConsultaEstatus
//	 */
//	public void showQuitarSuspension(ConsultaEstatus ce);
//
//	/**
//	 * Muestra la pantalla de configurar alertas.
//	 */
//	public void showConfigurarAlertas();
//
//	/**
//	 * Muestra la pantalla de aplicación desactivada.
//	 */
//	public void showConsultaEstatusAplicacionDesactivada();
//
//	/**
//	 * Muestra la pantalla de ayuda para la contratacion.
//	 */
//	public void showAyudaContratacion();
//
//	/**
//	 * Muestra la pantalla de informaci�n contratacion con y sin Token.
//	 */
//	public void showAyudaContratacionTokens();
//
//	/**
//	 * Muestra la pantalla de confirmaci�n autenticación para el estatus PS.
//	 */
//	public void showContratacionAutenticacion();
//
//	/**
//	 * Muestra la pantalla de confirmacion autenticacion
//	 */
//	public void showContratacionAutenticacion(DelegateBaseAutenticacion autenticacionDelegate);
//
//	/**
//	 * Muestra los terminos y condiciones de uso.
//	 * 
//	 * @param terminosDeUso
//	 *            Terminos de uso.
//	 */
//	public void showTerminosDeUso(String terminosDeUso);
//
//	public void showActivacion(final ConsultaEstatus ce, final String password,
//			final boolean appActivada);
//
//	public void showActivacion(ConsultaEstatus ce, String password);
//
//	public void showActivacion(Reactivacion reactivacion);
//
//	/**
//	 * Muestra la pantalla para consultar rápidos.
//	 */
//	public void showConsultarRapidas();
//
//	/**
//	 * Muestra la pantalla de configurrar correo.
//	 */
//	public void showConfigurarCorreo();
//	
//	//SPEI
//
//		/**
//		 * Shows the account association screen.
//		 */
//		public void showAsociacionCuentaTelefono();
//		
//		/**
//		 * Shows the screen to associate an account with a phone.
//		 */
//		public void showDetailAsociacionCuentaTelefono();
//
//	
//	//Termina SPEI
//	
//	
//	
//
//	/**
//	 * Muestra la pantalla de enviar correo
//	 */
//	public void showEnviarCorreo(ResultadosDelegate resultadosDelegate);
//	
//	/**
//	 * Muestra la pantalla de enviar correo
//	 */
//	public void showEnviarCorreo(DepositosRecibidosDelegate depositosDelegate);
//	
//	/**
//	 * Muestra la pantalla de enviar correo
//	 */
//	public void showEnviarCorreo(ObtenerComprobantesDelegate obtenerComprobantesDelegate);
//
//	/**
//	 * Muestra la pantalla de confirmacion para transferencias.
//	 * 
//	 * @param delegateBaseOperacion
//	 *            EL DelegateBaseOperacion que manda a llamar la pantalla de
//	 *            confirmaci�n.
//	 */
//	public void showConfirmacionRegistro(
//			DelegateBaseOperacion delegateBaseOperacion);
//
//	/**
//	 * 
//	 * @param delegateOp
//	 */
//	public void showRegistrarOperacion(DelegateBaseAutenticacion delegateOp);
//	public void showRegistrarOperacion(DelegateBaseAutenticacion delegateOp, boolean showHelpImage);
//	
//	/**
//	 * Shows the confirmation screen for the SPEI maintenance operations. 
//	 * @param operationDelegate The delegate of the current operation. 
//	 */
//	public void showSpeiConfirmation(DelegateBaseAutenticacion operationDelegate, boolean showHelpImage);
//	
//	
//	
//	/**
//	 * Shows the terms and conditions screen.
//	 * @param terminosDeUso Terms and conditions HTML.
//	 * @param title The title resource identifier.
//	 * @param icon The icon recource identifier.
//	 */
//	public void showTermsAndConditions(String termsHtml, int title, int icon);
//
//	public void showOtrosBBVASpeiViewController(Object obj, boolean esExpress, boolean esTDC);
//	
//	public void touchMenu();
//	
//	public void touchAtras();
//
//	//One click
//	
//	/*
//	 * Muestra la pantalla de detalle oferta ILC
//	 */
//	public void showDetalleILC(OfertaILC ofertaILC, Promociones promocion );
//	
//	/*
//	 * Muestra la pantalla de exito oferta ILC
//	 */
//	public void showExitoILC(AceptaOfertaILC aceptaOfertaILC, OfertaILC ofertaILC );
//	
//	/*
//	 * Muestra la pantalla de detalle oferta ILC
//	 */
//	public void showDetalleEFI(OfertaEFI ofertaEFI, Promociones promocion );
//	
//	/*
//	 * Muestra la pantalla de exito oferta ILC
//	 */
//	public void showExitoEFI(AceptaOfertaEFI aceptaOfertaEFI, OfertaEFI ofertaEFI);
//	
//	/*
//	 * Muestra la pantalla de importe a modificar EFI
//	 */
//	public void showModificaImporteEFI(OfertaEFI ofertaEFI);
//	
//	/*
//	 * Muestra la pantalla de detalle oferta consumo 
//	 */
//	public void showDetalleConsumo(OfertaConsumo ofertaConsumo, Promociones promocion );
//	
//	/*
//	 * Muestra la pantalla de contrato oferta consumo 
//	 */
//	public void showContratoConsumo(OfertaConsumo ofertaConsumo, Promociones promocion);
//	
//	/**
//	 * Muestra la poliza consumo.
//	 * 
//	 * @param terminosDeUso
//	 *            Terminos de uso.
//	 */
//	public void showPoliza(String poliza);
//	
//	/**
//	 * Muestra formato domiciliacion
//	 * 
//	 * @param terminosDeUso
//	 *            Terminos de uso.
//	 */
//	
//	public void showformatodomiciliacion(String terminos);
//	
//	/**
//	 * Muestra contrato consumo
//	 * 
//	 * @param terminosDeUso
//	 *            Terminos de uso.
//	 */
//	
//	public void showContrato(String contrato);
//	
//	/**
//	 * Muestra terminos y condiciones consumo.
//	 * 
//	 * @param terminosDeUso
//	 *            Terminos de uso.
//	 */
//	
//	public void showTerminosConsumo(String terminos);
//	
//	/*
//	 * Muestra la pantalla de exito  oferta consumo 
//	 */
//	public void showExitoConsumo(AceptaOfertaConsumo aceptaOfertaConsumo, OfertaConsumo ofertaConsumo);
//	/**
//	 * Muestra la pantalla para consultar Interbancarios.
//	 */
//	public void showConsultarInterbancarios();
//
//	/**
//	 * Muestra la pantalla detalle interbancaria
//	 */
//	public void showDetalleInterbancaria(ConsultaInterbancariosDelegate delegate);
//
//	public void showPagoTDCCuenta(Account cuenta);
//
//	public void showPagoTDCCuenta(Account cuenta, Account cuentaOrigen);
//
//	
//	/**
//	 * Muestra la pantalla otros creditos
//	 */
//	public void showOtrosCreditos();
	
}
