package bancomer.api.common.callback;

import bancomer.api.common.io.ServerResponse;

public interface CallBackModule {

	public void returnToPrincipal();
	
	public void returnDataFromModule(String operation, ServerResponse response);
}
