package bancomer.api.common.timer;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import bancomer.api.common.commons.ServerConstants;
import bancomer.api.common.session.CommonSession;
import bancomer.api.common.session.NotificacionesApp;


public class TimerController {

	private static TimerController timerController;
	/**
	 * Message for the handler, indicates that session must be expired.
	 */
	private static final int LOGOUT_MESSAGE = 0;

    Calendar calendar = Calendar.getInstance();
	
	private boolean cerrandoSesion = false;
	
	private int timeout;

    public boolean isAlert=Boolean.FALSE;
    public boolean sendAlert=Boolean.FALSE;

    public void setSendAlert(boolean sendAlert) {
        this.sendAlert = sendAlert;
    }

    /**
	 * The local session validity timer.
	 */
	private Timer sessionTimer = null;
	
	/**
	 * Task that manages session logout
	 */
	public SessionLogoutTask logoutTask;
    /**
     * contexto
     */
	public Context context;
    /**
     * clase
     */
    public Class clase;


	public SessionCloser sessionCloser;
	
	
	/**
	 * @return the sessionCloser
	 */
	public SessionCloser getSessionCloser() {
		return sessionCloser;
	}


	/**
	 * @param sessionCloser the sessionCloser to set
	 */
	public void setSessionCloser(SessionCloser sessionCloser) {
		this.sessionCloser = sessionCloser;
	}

	private static boolean isHomeKeyPressed = true;

	public static boolean isHomeKeyPressed() {
		return isHomeKeyPressed;
	}

	public static void setIsHomeKeyPressed(boolean isHomeKeyPressed) {
		TimerController.isHomeKeyPressed = isHomeKeyPressed;
	}

	public static TimerController getInstance(){
		if(timerController == null){
			timerController = new TimerController();
		}
		
		return timerController;
	}
	
	public void initTimer() {
        if(sessionTimer!=null){
            sessionTimer.cancel();
        }
		sessionTimer = new Timer(true);
		logoutTask = new SessionLogoutTask();
	}

	public void initTimer(Context context, Class clase) {
        this.isAlert=Boolean.FALSE;
        this.context = context;
        this.clase = clase;

            if (sessionTimer != null) {
                sessionTimer.cancel();
            }
            sessionTimer = new Timer(true);
            logoutTask = new SessionLogoutTask();
            setCerrandoSesion(false);
            if (CommonSession.getInstance().getCommonSessionService().getAllowLog())
                Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Timer Controller] InitTimer ->  Valor Cierre Sesion " + cerrandoSesion);
            if (sessionTimer != null) {
                logoutTask = new SessionLogoutTask();
                //if(Session.getInstance(suiteApp.getApplicationContext()).getValidity() == Session.VALID_STATUS){
                if (CommonSession.getInstance().getCommonSessionService().getAllowLog())
                    Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Timer Controller] ResetTimer ->  LogoutTask Launch " + logoutTask);
                if (CommonSession.getInstance().getCommonSessionService().getAllowLog())
                    Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Timer Controller] ResetTimer ->  LogoutTask Timeout " + timeout);
                timeout = ServerConstants.TIME_OUT_GENERAL;
                long timeoutMilis = timeout;
                Log.e("Nuevotimer schedule", "" + sessionTimer);
                sessionTimer.cancel();
                sessionTimer = new Timer(true);
                sessionTimer.schedule(logoutTask, timeoutMilis);
                //}
            }

	}

    public boolean masCincoSegundos() {

        long segundos = System.currentTimeMillis()-calendar.getTimeInMillis();
        if((int)(segundos / 1000)<ServerConstants.TIME_OUT_RESTART){
            return false;
        }else{
            calendar.setTimeInMillis(System.currentTimeMillis());
            return true;

        }
    }


    /**
	 * @return the sessionTimer
	 */
	public Timer getSessionTimer() {
		return sessionTimer;
	}


	/**
	 * @param sessionTimer the sessionTimer to set
	 */
	public void setSessionTimer(Timer sessionTimer) {
		this.sessionTimer = sessionTimer;
	}


	/**
	 * Called whenever the user performs an action or touches the screen
	 */
	public void resetTimer() {
        this.isAlert=Boolean.TRUE;
		if(CommonSession.getInstance().getCommonSessionService().getAllowLog()) Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Timer Controller] ResetTimer ->  ResetTimer");

            if (logoutTask != null) {
                logoutTask.cancel();
                if (CommonSession.getInstance().getCommonSessionService().getAllowLog())
                    Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Timer Controller] ResetTimer ->  LogoutTask Cancel " + logoutTask);
            }
            if (sessionTimer != null) {
                logoutTask = new SessionLogoutTask();
                //if(Session.getInstance(suiteApp.getApplicationContext()).getValidity() == Session.VALID_STATUS){
                if (CommonSession.getInstance().getCommonSessionService().getAllowLog())
                    Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Timer Controller] ResetTimer ->  LogoutTask Launch " + logoutTask);
                if (CommonSession.getInstance().getCommonSessionService().getAllowLog())
                    Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Timer Controller] ResetTimer ->  LogoutTask Timeout " + timeout);
                timeout = ServerConstants.TIME_OUT_GENERAL;
                long timeoutMilis = timeout;
                sessionTimer.cancel();
                sessionTimer = new Timer(true);
                sessionTimer.schedule(logoutTask, timeoutMilis);
                //}
            }

	}

	/**
	 * @return the timeout
	 */
	public int getTimeout() {
		return timeout;
	}


	/**
	 * @param timeout the timeout to set
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public boolean isCerrandoSesion() {
		return cerrandoSesion;
	}


	public void setCerrandoSesion(boolean cerrandoSesion) {
		this.cerrandoSesion = cerrandoSesion;
	}

    public Class getClase() {
        return clase;
    }

    /**
	 * We must use a TimerTask class to execute logout actions after the
	 * timer runs out.
	 */
	public class SessionLogoutTask extends TimerTask {

		@Override
		public void run() {
			//applicationLogged = false;
			Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Timer Controller] ResetTimer ->  LogoutTask " + this);
			Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Timer Controller] ResetTimer ->  LogoutTask Timeout " + timeout);
			Message msg = new Message();
			msg.what = LOGOUT_MESSAGE;
			mApplicationHandler.sendMessage(msg);
		}

	}

	/**
	 * Use handler to respond to logout messages given by the timer.
	 */
	private Handler mApplicationHandler = new Handler() {

		public void handleMessage(Message msg) {

			/*if(ServerCommons.ARQSERVICE){
				try{
					JSONObject jo=new JSONObject(response.getResponse().toString());
					if(200!=jo.getJSONObject("status").getInt("code")){
						muestra error comunicaciones
						return;
					}
				}catch (Exception e){
					if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "No se pudo cerrar sesión"+e.getMessage());
					muestra error comunicaciones
					return;
				}
			}*/

			if(msg.what == LOGOUT_MESSAGE){
				if(CommonSession.getInstance().getCommonSessionService().getAllowLog()) Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Timer Controller] TimerController ->  Valor Cierre Sesion " + cerrandoSesion);
				setCerrandoSesion(true);
				isHomeKeyPressed = false;
				sessionCloser.cerrarSesion();
				if(!isAlert){
                    NotificacionesApp.nuevaNotificacion(context, clase);
                }else{
                    sendAlert=Boolean.TRUE;
                }
				//logoutApp(false);
			}
		}
	};


	/**
	 * @return the timerController
	 */
	public static TimerController getTimerController() {
		return timerController;
	}


	/**
	 * @param timerController the timerController to set
	 */
	public static void setTimerController(TimerController timerController) {
		TimerController.timerController = timerController;
	}


	/**
	 * @return the logoutTask
	 */
	public SessionLogoutTask getLogoutTask() {
		return logoutTask;
	}


	/**
	 * @param logoutTask the logoutTask to set
	 */
	public void setLogoutTask(SessionLogoutTask logoutTask) {
		this.logoutTask = logoutTask;
	}
	
}
