package bancomer.api.common.model;

import android.content.res.Resources;

/**
 * Created by OOROZCO on 9/8/15.
 */
public interface HostAccounts {


    public double getBalance();

    public String getNumber();

    public String getType();

    public String getAlias();

    public String getPublicName(Resources res, boolean isCuentaOrigen);

    public String getNombreTipoCuenta(Resources res);

    public boolean isVisible();


}
