package bancomer.api.common.commons;

/**
 * Created by OOROZCO on 8/27/15.
 */
public interface IAutenticacion {

    boolean mostrarContrasena(Constants.Operacion tipoOperacion, Constants.Perfil perfil, double importe);

    boolean mostrarNIP(Constants.Operacion tipoOperacion, Constants.Perfil perfil);

    Constants.TipoOtpAutenticacion tokenAMostrar(Constants.Operacion tipoOperacion, Constants.Perfil perfil, double importe);

    boolean mostrarCVV(Constants.Operacion tipoOperacion, Constants.Perfil perfil, double importe);

    String getCadenaAutenticacion(Constants.Operacion tipoOperacion, Constants.Perfil perfil);

    /** Modified 28/08/2015*/
    boolean mostrarContrasena(Constants.Operacion tipoOperacion, Constants.Perfil perfil);

    Constants.TipoOtpAutenticacion tokenAMostrar(Constants.Operacion tipoOperacion, Constants.Perfil perfil);

    boolean mostrarCVV(Constants.Operacion tipoOperacion, Constants.Perfil perfil);

}
