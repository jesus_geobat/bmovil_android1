package bancomer.api.common.gui.controllers;

import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import bancomer.api.common.commons.GuiTools;

public class ListaDatosComboViewController extends LinearLayout {

    private BaseViewsController parentManager;
    private ListView listview;
    private ArrayList<Object> lista;
    private int numeroCeldas;
    private int numeroFilas;
    private LlenarListadoAdapter adapter;
    private LinearLayout.LayoutParams params;
    private float anchoColumna;
    private LayoutInflater inflater;
    private String title;
    private TextView titulo;
    private ArrayList<String> tablaDatosAColorear;
    //O3
    private ArrayList<String> tablaTitulosAColorear;
    private ArrayList<String> tablaDatosACambiarTamaño;
    private ArrayList<String> nuevaTablaDatosACambiarTamaño;

    private ArrayList<Spinner> listaSpinners;

    private Context context;

    private AdapterView.OnItemSelectedListener listener;

    public AdapterView.OnItemSelectedListener getListener() {
        return listener;
    }

    public ListaDatosComboViewController setListener(AdapterView.OnItemSelectedListener listener) {
        this.listener = listener;
        return this;
    }

    public Spinner getSpinner(Integer index) {
        Spinner sp = null;
        if((listaSpinners != null)&&(!listaSpinners.isEmpty())){
            sp = listaSpinners.get(index);
        }
        return sp;
    }

    public ListaDatosComboViewController(Context context,LinearLayout.LayoutParams layoutParams, BaseViewsController parentManager)
    {
        super(context);
        inflater = LayoutInflater.from(context);
        LinearLayout viewLayout = (LinearLayout) inflater.inflate(bancomer.api.common.R.layout.api_common_layout_lista_datos_view, this, true);
        viewLayout.setLayoutParams(layoutParams);
        this.parentManager = parentManager;

        this.context = context;

        listview = (ListView) findViewById(bancomer.api.common.R.id.layout_lista_Datos);
        tablaDatosAColorear = new ArrayList<String>();
        tablaDatosAColorear.add(context.getString(bancomer.api.common.R.string.transferir_interbancario_tabla_resultados_importe));
        tablaDatosAColorear.add(context.getString(bancomer.api.common.R.string.transferir_dineromovil_resultados_codigo));
        tablaDatosAColorear.add(context.getString(bancomer.api.common.R.string.administrar_cambiarpassword_folioOperacionLabel));

        tablaDatosAColorear.add(context.getString(bancomer.api.common.R.string.bmovil_consultar_interbancario_estatus_liquidada));
        tablaDatosAColorear.add(context.getString(bancomer.api.common.R.string.bmovil_consultar_interbancario_estatus_devuelta));
        tablaDatosAColorear.add(context.getString(bancomer.api.common.R.string.bmovil_consultar_interbancario_estatus_proceso));
        tablaDatosAColorear.add(context.getString(bancomer.api.common.R.string.bmovil_consultar_interbancario_estatus_validacion));
        tablaDatosAColorear.add(context.getString(bancomer.api.common.R.string.bmovil_consultar_interbancario_estatus_enviada));
        tablaDatosAColorear.add(context.getString(bancomer.api.common.R.string.bmovil_menu_consultar_detalle_otroscreditos_recibo));
        tablaDatosAColorear.add(context.getString(bancomer.api.common.R.string.bmovil_menu_consultar_detalle_otroscreditos_recibosVencidos));
        tablaDatosAColorear.add(context.getString(bancomer.api.common.R.string.bmovil_menu_consultar_detalle_otroscreditos_totalPagar));
        //O3
        tablaTitulosAColorear = new ArrayList<String>();
        tablaTitulosAColorear.add(context.getString(bancomer.api.common.R.string.bmovil_consultar_obtenercomprobante_detalles_datos_titulo_datodelenvio));
        tablaTitulosAColorear.add(context.getString(bancomer.api.common.R.string.bmovil_consultar_obtenercomprobante_detalles_datos_titulo_datodeenvio));
        tablaTitulosAColorear.add(context.getString(bancomer.api.common.R.string.bmovil_consultar_depositosrecibidos_detalles_datos_titulo_datosuc));

        tablaDatosACambiarTamaño = new ArrayList<String>();
        //tablaDatosACambiarTamaño.add(context.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_tipoop));
        /******/
        nuevaTablaDatosACambiarTamaño = new ArrayList<String>();
        nuevaTablaDatosACambiarTamaño.add(context.getString(bancomer.api.common.R.string.bmovil_consultar_depositosrecibidos_detalles_datos_deporecibido));
        nuevaTablaDatosACambiarTamaño.add(context.getString(bancomer.api.common.R.string.bmovil_consultar_depositosrecibidos_detalles_datos_titulo_datosuc));
        nuevaTablaDatosACambiarTamaño.add(context.getString(bancomer.api.common.R.string.bmovil_consultar_depositosrecibidos_detalles_datos_referencianum));


        listaSpinners = new ArrayList<Spinner>();
    }

    public void setNumeroFilas(int numeroFilas) {
        this.numeroFilas = numeroFilas;
    }

    public void setTitulo(int resTitle) {
        this.title = context.getString(resTitle);
    }

    public void showLista()
    {
        if (title != null) {
            titulo = (TextView) findViewById(bancomer.api.common.R.id.lista_datos_titulo);
            titulo.setText(title);
            titulo.setVisibility(View.VISIBLE);

            GuiTools.getCurrent().scale(titulo, true);
        }
        adapter = new LlenarListadoAdapter();
        listview.setAdapter(adapter);
        listview.setEnabled(false);

        ListAdapter listAdapter = listview.getAdapter();
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listview);
            listItem.measure(0, 0);

            int htemp = listItem.getMeasuredHeight()*numeroFilas;
            if(totalHeight == 0){
                totalHeight = htemp;
            }else if(htemp <= totalHeight){
                totalHeight = htemp;
            }
        }

        ViewGroup.LayoutParams params = listview.getLayoutParams();
        params.height = totalHeight	+ (listview.getDividerHeight() * (listview.getCount() - 1));
        listview.setLayoutParams(params);
        listview.requestLayout();

    }

    public ArrayList<Object> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Object> lista) {
        this.lista = lista;
    }

    public int getNumeroCeldas() {
        return numeroCeldas;
    }

    public void setNumeroCeldas(int numeroCeldas) {
        this.numeroCeldas = numeroCeldas;
    }

    private class LlenarListadoAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return lista.size();
        }

        @Override
        public Object getItem(int position) {
            return lista.get(position);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            GuiTools guiTools = GuiTools.getCurrent();


            if (convertView == null) {
                convertView = inflater.inflate(bancomer.api.common.R.layout.api_common_list_item_objects, null);
            }

            anchoColumna = 1.0f/getNumeroCeldas();

            if (getNumeroCeldas() == 2) {
                params = new LayoutParams(0,LayoutParams.WRAP_CONTENT,0.47f);
                ArrayList<Object> registro = (ArrayList<Object>) lista.get(position);

                TextView celda1 = (TextView)convertView.findViewById(bancomer.api.common.R.id.lista_celda_1);
                celda1.setText((String) registro.get(0));

                if(registro.get(1) instanceof ArrayAdapter){
                    LinearLayout.LayoutParams params2 = new LayoutParams(0,LayoutParams.WRAP_CONTENT,0.67f);
                    celda1.setLayoutParams(params);
                }else{
                    celda1.setLayoutParams(params);
                }
                celda1.setGravity(Gravity.LEFT);
                celda1.setMaxLines(1);
                //celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
                celda1.setSingleLine(true);
                celda1.setSelected(true);
                /*****O3*/
                //celda1.setEllipsize(null);
                if (tablaTitulosAColorear.contains((String) registro.get(0))) {
                    celda1.setTextColor(getResources().getColor(bancomer.api.common.R.color.primer_azul));
                }
                if (nuevaTablaDatosACambiarTamaño.contains((String) registro.get(0)))
                {//celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10.0f);
                    celda1.setEllipsize(null);
                    celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 12.0f);
                }
                else
                    celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
                guiTools.tryScaleText(celda1);


//	            ((TextView)convertView.findViewById(R.id.lista_celda_1)).setText((String) registro.get(0));
//	            ((TextView)convertView.findViewById(R.id.lista_celda_1)).setLayoutParams(params);
//	            ((TextView)convertView.findViewById(R.id.lista_celda_1)).setGravity(Gravity.LEFT);

                ((Spinner)convertView.findViewById(bancomer.api.common.R.id.spinner)).setVisibility(View.GONE);

                params = new LayoutParams(0,LayoutParams.WRAP_CONTENT,0.53f);

                TextView celda2 = (TextView)convertView.findViewById(bancomer.api.common.R.id.lista_celda_2);

                if(registro.get(1) instanceof String){

                    celda2.setText((String) registro.get(1));

                    celda2.setLayoutParams(params);
                    celda2.setGravity(Gravity.RIGHT);
                    celda2.setMaxLines(1);
                    //celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
                    if (tablaDatosACambiarTamaño.contains((String) registro.get(0))) {
                        celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10.0f);

                    }else{
                        celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
                    }
                    celda2.setSingleLine(true);
                    celda2.setSelected(true);
                    guiTools.tryScaleText(celda2);

                    ((TextView)convertView.findViewById(bancomer.api.common.R.id.lista_celda_3)).setVisibility(View.GONE);
                    ((TextView)convertView.findViewById(bancomer.api.common.R.id.lista_celda_4)).setVisibility(View.GONE);
                    ((ImageButton)convertView.findViewById(bancomer.api.common.R.id.lista_celda_check)).setVisibility(View.GONE);

                }else if(registro.get(1) instanceof ArrayAdapter){

//                    LinearLayout layoutspinner = (LinearLayout)convertView.findViewById(R.id.spinnerLayout);
//                    layoutspinner.setVisibility(View.VISIBLE);
//                    layoutspinner.setLayoutParams(params);
//                    guiTools.scale(layoutspinner);

                    ((TextView)convertView.findViewById(bancomer.api.common.R.id.lista_celda_2)).setVisibility(View.GONE);

                    Spinner spinner = (Spinner)convertView.findViewById(bancomer.api.common.R.id.spinner);
                    spinner.setVisibility(View.VISIBLE);

                    spinner.setAdapter((ArrayAdapter) registro.get(1));

                    params = new LayoutParams(0,LayoutParams.MATCH_PARENT,0.45f);
                    spinner.setLayoutParams(params);
                    guiTools.scale(spinner);
                    if(listener != null){
                        spinner.setOnItemSelectedListener(listener);
                    }
                    listaSpinners.add(spinner);

                    ((TextView)convertView.findViewById(bancomer.api.common.R.id.lista_celda_3)).setVisibility(View.GONE);
                    ((TextView)convertView.findViewById(bancomer.api.common.R.id.lista_celda_4)).setVisibility(View.VISIBLE);
                    ((ImageButton)convertView.findViewById(bancomer.api.common.R.id.lista_celda_check)).setVisibility(View.GONE);

                }





//	            ((TextView)convertView.findViewById(R.id.lista_celda_2)).setText((String) registro.get(1));
//	            ((TextView)convertView.findViewById(R.id.lista_celda_2)).setLayoutParams(params);
//	            ((TextView)convertView.findViewById(R.id.lista_celda_2)).setGravity(Gravity.RIGHT);

            }

            return convertView;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }


    }

}

