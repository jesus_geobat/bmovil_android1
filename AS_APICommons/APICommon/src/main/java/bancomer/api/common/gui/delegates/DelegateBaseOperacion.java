package bancomer.api.common.gui.delegates;

import bancomer.api.common.commons.Constants;

/**
 * Created by OOROZCO on 8/26/15.
 */
public interface DelegateBaseOperacion {

    public String getNumeroCuentaParaRegistroOperacion();

    public String loadOtpFromSofttoken(Constants.TipoOtpAutenticacion tipoOTP, DelegateBaseOperacion delegate);

}
