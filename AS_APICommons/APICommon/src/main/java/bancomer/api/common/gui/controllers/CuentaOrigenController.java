package bancomer.api.common.gui.controllers;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bancomer.api.common.R;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.delegates.CuentaOrigenDelegate;
import bancomer.api.common.model.HostAccounts;

/**
 * Created by OOROZCO on 9/8/15.
 */
public class CuentaOrigenController extends LinearLayout implements View.OnClickListener {

    private CuentaOrigenDelegate cuentaOrigenDelegate;
    private ImageButton imgIzquierda;
    private ImageButton imgDerecha;
    private TextView vistaCtaOrigen;
    private TextView tituloComponenteCtaOrigen;
    private LinearLayout componenteCtaOrigen;
    private ArrayList<HostAccounts> listaCuetasAMostrar;
    private int indiceCuentaSeleccionada;
    private String saldo;
    private boolean seleccionado;

    public CuentaOrigenController(Context context, LayoutParams layoutParams) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        LinearLayout viewLayout = (LinearLayout) inflater.inflate(R.layout.api_common_layout_cuenta_origen_view, this, true);
        viewLayout.setLayoutParams(layoutParams);


        setTituloComponenteCtaOrigen((TextView) findViewById(R.id.componente_cuenta_origen_title));
        setImgIzquierda((ImageButton) findViewById(R.id.img_izquierda));
        setImgDerecha((ImageButton) findViewById(R.id.img_derecha));
        setVistaCtaOrigen((TextView) findViewById(R.id.vista_cta_origen));
        componenteCtaOrigen = (LinearLayout) findViewById(R.id.componente_cuenta_origen);

        getImgIzquierda().setOnClickListener(this);
        getImgDerecha().setOnClickListener(this);
        getVistaCtaOrigen().setOnClickListener(this);
        setSeleccionado(false);

        // Resize to screen.
        scaleForCurrentScreen();
    }

    public void init() {

        HostAccounts cuenta = listaCuetasAMostrar.get(indiceCuentaSeleccionada);
        cuentaOrigenDelegate = new CuentaOrigenDelegate(cuenta);
        cuentaOrigenDelegate.setCuentaOrigenViewController(this);

        if (getListaCuetasAMostrar().size() < 2) {
            desactivarCuentaSiguientePrevia();
            Log.e("lista < 2", "OK");
        } else if (getListaCuetasAMostrar().size() > 1) {
            Log.e("lista > 1", "OK");
            activarCuentaSiguientePrevia();
        }
        cuentaOrigenDelegate.setListaCuentasOrigen(getListaCuetasAMostrar());
        cuentaOrigenDelegate.indiceCuenta = getIndiceCuentaSeleccionada();
        actualizaComponente(false);

    }

    @Override
    public void onClick(View v) {
        Log.e("onclick", "OK");
        if (v == getImgIzquierda()) {
            Log.e("Izquierda", "OK");
            muestraCuentaPrevia();
        } else if (v == getImgDerecha()) {
            Log.e("Derecha", "OK");
            muestraCuentaSiguiente();
        }/* else if (v == getVistaCtaOrigen() && !parentManager.isActivityChanging()) {
            muestraListaCuentasView();
        }*/
    }

    /*public void muestraListaCuentasView() {
        SuiteApp suiteApp = (SuiteApp)parentManager.getCurrentViewControllerApp().getApplication();
        suiteApp.getBmovilApplication().getBmovilViewsController().showCuentaOrigenListViewController();
        suiteApp.getBmovilApplication().getBmovilViewsController().addDelegateToHashMap(CuentaOrigenDelegate.CUENTA_ORIGEN_DELEGATE_ID, cuentaOrigenDelegate);
        setSeleccionado(true);
    }*/

    public void muestraCuentaSiguiente() {
        Log.e("CuentaOrigen", "Presionaste flecha Derecha");
        getVistaCtaOrigen().setEnabled(false);
        setSeleccionado(true);
        //getImgDerecha().setEnabled(false);
        //getImgIzquierda().setEnabled(false);
        cuentaOrigenDelegate.setCuentaSiguiente();
        if (getIndiceCuentaSeleccionada() == getListaCuetasAMostrar().size() - 1) {
            setIndiceCuentaSeleccionada(0);
        } else {
            setIndiceCuentaSeleccionada(getIndiceCuentaSeleccionada() + 1);
        }

        actualizaComponente(false);
    }

    public void muestraCuentaPrevia() {
        Log.e("CuentaOrigen", "Presionaste flecha Izquierda");
        //getImgDerecha().setEnabled(false);
        //getImgIzquierda().setEnabled(false);
        getVistaCtaOrigen().setEnabled(false);
        setSeleccionado(true);
        cuentaOrigenDelegate.setCuentaPrevia();
        if (getIndiceCuentaSeleccionada() == 0) {
            setIndiceCuentaSeleccionada(getListaCuetasAMostrar().size() - 1);
        } else {
            setIndiceCuentaSeleccionada(getIndiceCuentaSeleccionada() - 1);
        }
        actualizaComponente(false);
    }

    @SuppressWarnings("deprecation")
    public void desactivarCuentaSiguientePrevia() {
        componenteCtaOrigen
                .setBackgroundResource(R.drawable.cuenta_origen_sin_flechas);
        getImgDerecha().setVisibility(View.INVISIBLE);
        getImgIzquierda().setVisibility(View.INVISIBLE);
        getVistaCtaOrigen().setEnabled(false);
    }

    @SuppressWarnings("deprecation")
    public void activarCuentaSiguientePrevia() {
        componenteCtaOrigen
                .setBackgroundResource(R.drawable.cuenta_origen_con_flechas);
        getImgDerecha().setVisibility(View.VISIBLE);
        getImgIzquierda().setVisibility(View.VISIBLE);
        getVistaCtaOrigen().setEnabled(true);
    }

    public CuentaOrigenDelegate getCuentaOrigenDelegate() {
        return cuentaOrigenDelegate;
    }

    public void setCuentaOrigenDelegate(CuentaOrigenDelegate cuentaOrigenDelegate) {
        this.cuentaOrigenDelegate = cuentaOrigenDelegate;
    }

    public void dejarDeMostrarLista() {
        actualizaComponente(false);
        // parentManager.getCurrentViewControllerApp().goBack();
    }

    //public void actualizaComponente(){
    public void actualizaComponente(boolean mostrarAlias) {
        cuentaOrigenDelegate.indiceCuenta = getIndiceCuentaSeleccionada();
        cuentaOrigenDelegate.actualizarCuentaSeleccionada();
        saldo = Tools.convertDoubleToBigDecimalAndReturnString(cuentaOrigenDelegate.getCuentaSeleccionada().getBalance());
        getVistaCtaOrigen().setText(cuentaOrigenDelegate.getCuentaSeleccionada().getPublicName(getResources(), true) +
                "\n" +
                Tools.formatAmount(saldo, false));
        //One CLick
        if (mostrarAlias) {
            if (cuentaOrigenDelegate.getCuentaSeleccionada().getAlias().compareTo("") != 0) {
                getVistaCtaOrigen().setText(cuentaOrigenDelegate.getCuentaSeleccionada().getAlias() +
                        "\n" +
                        Tools.formatAmount(saldo, false));
            } else {
                getVistaCtaOrigen().setText(cuentaOrigenDelegate.getCuentaSeleccionada().getPublicName(getResources(), true) +
                        "\n" +
                        Tools.formatAmount(saldo, false));
            }

        } else {
            getVistaCtaOrigen().setText(cuentaOrigenDelegate.getCuentaSeleccionada().getPublicName(getResources(), true) +
                    "\n" +
                    Tools.formatAmount(saldo, false));
        }

    }

    private void scaleForCurrentScreen() {
        GuiTools guiTools = GuiTools.getCurrent();

        guiTools.scale(getTituloComponenteCtaOrigen(), true);
        guiTools.scale(componenteCtaOrigen);
        guiTools.scale(getImgIzquierda());
        guiTools.scale(getVistaCtaOrigen(), true);
        guiTools.scale(getImgDerecha());
    }

    public ImageButton getImgDerecha() {
        return imgDerecha;
    }

    public void setImgDerecha(ImageButton imgDerecha) {
        this.imgDerecha = imgDerecha;
    }

    public ImageButton getImgIzquierda() {
        return imgIzquierda;
    }

    public void setImgIzquierda(ImageButton imgIzquierda) {
        this.imgIzquierda = imgIzquierda;
    }

    public TextView getVistaCtaOrigen() {
        return vistaCtaOrigen;
    }

    public void setVistaCtaOrigen(TextView vistaCtaOrigen) {
        this.vistaCtaOrigen = vistaCtaOrigen;
    }

    public ArrayList<HostAccounts> getListaCuetasAMostrar() {
        return listaCuetasAMostrar;
    }

    public void setListaCuetasAMostrar(ArrayList<HostAccounts> listaCuetasAMostrar) {
        this.listaCuetasAMostrar = listaCuetasAMostrar;
    }

    public TextView getTituloComponenteCtaOrigen() {
        return tituloComponenteCtaOrigen;
    }

    public void setTituloComponenteCtaOrigen(TextView tituloComponenteCtaOrigen) {
        this.tituloComponenteCtaOrigen = tituloComponenteCtaOrigen;
    }

    public int getIndiceCuentaSeleccionada() {
        return indiceCuentaSeleccionada;
    }

    public void setIndiceCuentaSeleccionada(int indiceCuentaSeleccionada) {
        if (indiceCuentaSeleccionada < 0)
            this.indiceCuentaSeleccionada = 0;
        else
            this.indiceCuentaSeleccionada = indiceCuentaSeleccionada;
    }

    public boolean isSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }

}
