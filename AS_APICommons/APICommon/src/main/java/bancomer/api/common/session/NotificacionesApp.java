package bancomer.api.common.session;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import bancomer.api.common.R;
import bancomer.api.common.commons.ServerConstants;


/**
 * Created by mcamarillo on 20/10/16.
 */
public final class NotificacionesApp {

    /**
     * constructor
     */
    private NotificacionesApp() {

    }

    /**
     *
     * @param activityActual
     * @param clase
     */
    public static void nuevaNotificacion(final Context activityActual, Class clase){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                activityActual).setAutoCancel(true)
                .setContentTitle(ServerConstants.TITULO_NOTIFICACION)
                .setSmallIcon(R.drawable.icc_launcher)
                .setLargeIcon((((BitmapDrawable) activityActual.getResources()
                        .getDrawable(R.drawable.ic_launcherr)).getBitmap())).setContentText(ServerConstants.MENSAJE_NOTIFICACION);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(ServerConstants.MENSAJE_NOTIFICACION);
        bigText.setBigContentTitle(ServerConstants.TITULO_NOTIFICACION);
        mBuilder.setStyle(bigText);
        mBuilder.setPriority(NotificationCompat.PRIORITY_MAX);
        mBuilder.setTicker(ServerConstants.MENSAJE_NOTIFICACION);

        Intent notIntent =new Intent(activityActual, clase);
        PendingIntent contIntent =  PendingIntent.getActivity(activityActual, 0, notIntent, 0);
        mBuilder.setContentIntent(contIntent);
        mBuilder.setVibrate(new long[]{1000, 1000 });
        NotificationManager mNotificationManager =
                (NotificationManager) activityActual.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(1, mBuilder.build());
        Log.e("NotificacionesApp ", "si se mando notificacion");
    }
}
