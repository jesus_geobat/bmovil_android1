package bancomer.api.consultaotroscreditos.models;

import android.util.Log;

import com.bancomer.base.callback.CallBackSession;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.consultaotroscreditos.io.Server;

public class ConsultaOtrosCreditosSendData {
	
	private String IUM;
	
	private String username;
	
	private String cadenaAutenticacion;
	
	private String cveAcceso;
	
	private String tarjeta5Dig;
	
	private String codigoNip;
	
	private String codigoCvv2;
	
	private String codigoOtp;
	
	private Boolean simulation;
	
	private Boolean development;
	
	private Boolean emulator;

	private CallBackModule callBackModule;
	private CallBackSession callBackSession;

	public ConsultaOtrosCreditosSendData() {
		super();
		this.IUM = "";
		this.username = "";
		this.cadenaAutenticacion = "";
		this.cveAcceso = "";
		this.tarjeta5Dig = "";
		this.codigoNip = "";
		this.codigoCvv2 = "";
		this.codigoOtp = "";
		this.development = true;
		this.simulation = true;
		this.emulator = false;
	}
	
	public void setServerParams(Boolean dev, Boolean sim, Boolean em){
		this.development = dev;
		this.simulation = sim;
		this.emulator = em;
		
		// Seteo de parametros de servidor
		Server.DEVELOPMENT = dev;
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitConsultaOtrosCreditos] Se inicializa development -> "+Server.DEVELOPMENT);
		Server.SIMULATION = sim;
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitConsultaOtrosCreditos] Se inicializa simulation -> "+Server.SIMULATION);
		Server.EMULATOR = em;
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitConsultaOtrosCreditos] Se inicializa emulator -> "+Server.EMULATOR);

		Server.setServer();
		Server.setAllowLog();
	}
	
	public CallBackModule getCallBackModule() {
		return callBackModule;
	}

	public void setCallBackModule(CallBackModule callBackModule) {
		this.callBackModule = callBackModule;
	}

	public CallBackSession getCallBackSession() {
		return callBackSession;
	}

	public void setCallBackSession(CallBackSession callBackSession) {
		this.callBackSession = callBackSession;
	}

	public Boolean getSimulation() {
		return simulation;
	}

	public void setSimulation(Boolean simulation) {
		this.simulation = simulation;
	}

	public Boolean getDevelopment() {
		return development;
	}

	public void setDevelopment(Boolean development) {
		this.development = development;
	}

	public Boolean getEmulator() {
		return emulator;
	}

	public void setEmulator(Boolean emulator) {
		this.emulator = emulator;
	}

	public String getIUM() {
		return IUM;
	}

	public void setIUM(String iUM) {
		IUM = iUM;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCadenaAutenticacion() {
		return cadenaAutenticacion;
	}

	public void setCadenaAutenticacion(String cadenaAutenticacion) {
		this.cadenaAutenticacion = cadenaAutenticacion;
	}

	public String getCveAcceso() {
		return cveAcceso;
	}

	public void setCveAcceso(String cveAcceso) {
		this.cveAcceso = cveAcceso;
	}

	public String getTarjeta5Dig() {
		return tarjeta5Dig;
	}

	public void setTarjeta5Dig(String tarjeta5Dig) {
		this.tarjeta5Dig = tarjeta5Dig;
	}

	public String getCodigoNip() {
		return codigoNip;
	}

	public void setCodigoNip(String codigoNip) {
		this.codigoNip = codigoNip;
	}

	public String getCodigoCvv2() {
		return codigoCvv2;
	}

	public void setCodigoCvv2(String codigoCvv2) {
		this.codigoCvv2 = codigoCvv2;
	}

	public String getCodigoOtp() {
		return codigoOtp;
	}

	public void setCodigoOtp(String codigoOtp) {
		this.codigoOtp = codigoOtp;
	}
	
	

}
