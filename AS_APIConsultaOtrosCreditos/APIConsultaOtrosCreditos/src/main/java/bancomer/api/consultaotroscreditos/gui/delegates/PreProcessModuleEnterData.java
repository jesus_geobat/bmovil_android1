package bancomer.api.consultaotroscreditos.gui.delegates;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.io.ServerResponse;

public class PreProcessModuleEnterData {
	// Status de la peticion
	public int status;
	
	// Variable para conocer si la petición ha acabado. Variable de espera para el callback
	private Boolean finishedCall;
	
	public PreProcessModuleEnterData() {
		super();
		this.finishedCall = false;
	}

	// Respuesta de la peticion. Se lee al ejecutar el callback
	private ServerResponse response;

	public ServerResponse getResponse() {
		return response;
	}

	public void setResponse(ServerResponse response) {
		this.response = response;
	}

	public int getStatus() {
		return response.getStatus();
	}

	public Boolean getFinishedCall() {
		return finishedCall;
	}

	public void setFinishedCall(Boolean finishedCall) {
		this.finishedCall = finishedCall;
	}
		
}
