package bancomer.api.domiciliaciontdc.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.domiciliaciontdc.commons.ConstantsDomicilia;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by WGUADARRAMA on 27/08/15.
 */
public class OfertaDomiciliacionTDC implements ParsingHandler {

    String cuentaCargo;
    String tdcDomiciliada;
    String envioCorreoElectronico;
    String envioSMS;
    String PM;
    private ArrayList<Promociones> promociones=null;
    private Promociones[] arrayPromociones=null;

    public String getCuentaCargo() { return cuentaCargo; }

    public void setCuentaCargo(String cuentaCargo) { this.cuentaCargo = cuentaCargo; }

    public String getTdcDomiciliada() {
        return tdcDomiciliada;
    }

    public void setTdcDomiciliada(String tdcDomiciliada) {
        this.tdcDomiciliada = tdcDomiciliada;
    }

    public String getEnvioCorreoElectronico() {
        return envioCorreoElectronico;
    }

    public void setEnvioCorreoElectronico(String envioCorreoElectronico) {
        this.envioCorreoElectronico = envioCorreoElectronico;
    }

    public String getEnvioSMS() {
        return envioSMS;
    }

    public void setEnvioSMS(String envioSMS) {
        this.envioSMS = envioSMS;
    }

    public String getPM() {
      return PM;
  }

    public void setPM(String PM) { this.PM = PM; }

    public Promociones[] getArrayPromociones() {
        return arrayPromociones;
    }

    public void setArrayPromociones(Promociones[] arrayPromociones) {
        this.arrayPromociones = arrayPromociones;
    }

    public ArrayList<Promociones> getPromociones() {
        return promociones;
    }

    public void setPromociones(ArrayList<Promociones> promociones) {
        this.promociones = promociones;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

            cuentaCargo = (parser.hasValue(ConstantsDomicilia.CUENTA_CARGO)) ? parser.parseNextValue(ConstantsDomicilia.CUENTA_CARGO):"";
            tdcDomiciliada = (parser.hasValue(ConstantsDomicilia.TDC_DOMICILIADA)) ? parser.parseNextValue(ConstantsDomicilia.TDC_DOMICILIADA):"";
            envioCorreoElectronico = (parser.hasValue(ConstantsDomicilia.ENVIO_CORREO)) ? parser.parseNextValue(ConstantsDomicilia.ENVIO_CORREO):"";
            envioSMS = (parser.hasValue(ConstantsDomicilia.ENVIO_SMS)) ? parser.parseNextValue(ConstantsDomicilia.ENVIO_SMS):"";
            PM = parser.hasValue(ConstantsDomicilia.PM) ? parser.parseNextValue(ConstantsDomicilia.PM) : "";

            //Log.e(ConstantsDomicilia.CUENTA_CARGO, cuentaCargo);
            //Log.e(ConstantsDomicilia.TDC_DOMICILIADA, tdcDomiciliada);
            //Log.e(ConstantsDomicilia.ENVIO_CORREO, envioCorreoElectronico != null? envioCorreoElectronico:"No hay dato");
            //Log.e(ConstantsDomicilia.ENVIO_SMS, envioSMS !=null? envioSMS:"No hay dato");
            //Log.e(ConstantsDomicilia.PM, PM !=null?PM:"No hay dato");

            if (PM.equalsIgnoreCase(ConstantsDomicilia.SI)) {
                try {
                    JSONObject json = new JSONObject(parser.parseNextValue(ConstantsDomicilia.LP));
                    JSONArray array = json.getJSONArray(ConstantsDomicilia.CAMPANIAS);
                    promociones = new ArrayList<Promociones>();
                    Promociones promocion;

                    for (int i = 0; i < array.length(); i++) {
                        promocion = new Promociones();
                        JSONObject auxiliar = array.getJSONObject(i);
                        promocion.setCveCamp(auxiliar.getString(ConstantsDomicilia.CVE_CAMP));
                        promocion.setDesOferta(auxiliar.getString(ConstantsDomicilia.DES_OFERTA));
                        promocion.setMonto(auxiliar.getString(ConstantsDomicilia.MONTO));
                        promocion.setCarrusel(auxiliar.getString(ConstantsDomicilia.CARRUSEL));
                        promociones.add(promocion);
                    }

                    arrayPromociones = new Promociones[promociones.size()];

                    for (int i = 0; i < promociones.size(); i++) {
                        arrayPromociones[i] = promociones.get(i);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
    }
}
