package bancomer.api.domiciliaciontdc.implementations;

import android.app.Activity;
import android.util.Log;

import org.apache.http.impl.client.DefaultHttpClient;


import java.util.ArrayList;

import bancomer.api.common.gui.delegates.MainViewHostDelegate;
import bancomer.api.common.model.HostAccounts;
import bancomer.api.domiciliaciontdc.gui.controllers.DetalleDomiciliaTDCViewController;

import bancomer.api.domiciliaciontdc.gui.controllers.ExitoDomiciliaTDCViewController;
import bancomer.api.domiciliaciontdc.gui.delegates.ConsultaDomiciliacionTDCDelegate;
import bancomer.api.domiciliaciontdc.gui.delegates.DetalleDomiciliaTDCDelegate;
import bancomer.api.domiciliaciontdc.gui.delegates.ExitoDomiciliaTDCDelegate;
import bancomer.api.domiciliaciontdc.io.BaseSubapplication;
import bancomer.api.domiciliaciontdc.io.Server;
import bancomer.api.common.commons.IAutenticacion;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.BaseViewsControllerImpl;
import bancomer.api.common.gui.delegates.DelegateBaseOperacion;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import bancomer.api.domiciliaciontdc.models.OfertaDomiciliacionTDC;


public class InitDomiciliacionTDC {


    /*Components for APIDOMICILIACIONTDC working*/

    private static InitDomiciliacionTDC mInstance = null;

    private BaseViewController baseViewController;

    private static BaseViewsController parentManager;

    private BaseSubapplication baseSubApp;

    private static DefaultHttpClient client;

    private Promociones ofertaDomiciliacion;

    private DelegateBaseOperacion delegateOTP;

    private ICommonSession session;

    private IAutenticacion autenticacion;

    private boolean sofTokenStatus;

    private MainViewHostDelegate hostInstance;

    private HostAccounts[] listaCuentas;

    /* End Region */

    /////////////  Getter's & Setter's /////////////////


    public ICommonSession getSession()
    {
        return session;
    }

    public DelegateBaseOperacion getDelegateOTP() {
        return delegateOTP;
    }

    public Promociones getOfertaDomiciliacion()
    {
        return ofertaDomiciliacion;
    }

    public BaseViewController getBaseViewController() {
        return baseViewController;
    }

    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }

    public BaseViewsController getParentManager() {
        return parentManager;
    }

    public BaseSubapplication getBaseSubApp() {
        return baseSubApp;
    }

    public IAutenticacion getAutenticacion() {  return autenticacion;  }

    public boolean getSofTokenStatus(){ return sofTokenStatus; }

    public static DefaultHttpClient getClient() {
        return client;
    }

    public HostAccounts[] getListaCuentas() {
        return listaCuentas;
    }

    ///////////// End Region Getter's & Setter's /////////////////

    public void resetInstance() { mInstance = null; }

    public void updateHostActivityChangingState()
    {
        hostInstance.updateActivityChangingState();
    }

    public static InitDomiciliacionTDC getInstance(Activity activity, Promociones promocion,DelegateBaseOperacion delegateOTP, ICommonSession session,IAutenticacion autenticacion,boolean softTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance, HostAccounts[] listaCuentas, boolean dev, boolean sim, boolean emulator, boolean log)
    {
        if(mInstance == null)
        {
            //Log.d("[CGI-Configuracion-Obligatorio] >> ", "[InitTDCAdicional] Se inicializa la instancia del singleton de datos");
            mInstance = new InitDomiciliacionTDC(activity,promocion,delegateOTP,session,autenticacion,softTokenStatus,client, hostInstance, listaCuentas, dev,  sim,  emulator, log);
        }
        else
            parentManager.setCurrentActivityApp(activity);
        return mInstance;
    }

    public static InitDomiciliacionTDC getInstance()
    {
        return mInstance;
    }

    private InitDomiciliacionTDC(Activity activity, Promociones promocion, DelegateBaseOperacion delegateOTP, ICommonSession session, IAutenticacion autenticacion, boolean sofTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance, HostAccounts[] listaCuentas,boolean dev, boolean sim, boolean emulator, boolean log)
    {
        setServerParams(dev,  sim,  emulator,  log);
        Server.setEnviroment();
        ofertaDomiciliacion = promocion;
        this.delegateOTP=delegateOTP;
        baseViewController = new BaseViewControllerImpl();
        this.client = client;
        parentManager = new BaseViewsControllerImpl();
        parentManager.setCurrentActivityApp(activity);
        baseSubApp = new BaseSubapplicationImpl(activity, client);
        this.session=session;
        this.autenticacion=autenticacion;
        this.sofTokenStatus = sofTokenStatus;
        this.hostInstance = hostInstance;
        this.listaCuentas = listaCuentas;

    }

    public void setServerParams(boolean dev, boolean sim, boolean emulator, boolean log)
    {
        Server.DEVELOPMENT = dev;
        Server.SIMULATION = sim;
        Server.EMULATOR = emulator;
        Server.ALLOW_LOG = log;

    }

    public void ejecutaAPIDomiciliacion()
    {
        new ConsultaDomiciliacionTDCDelegate().performAction(null);
    }


    public void showFormalizaDomiciliacion(OfertaDomiciliacionTDC data){
        ExitoDomiciliaTDCDelegate delegate = (ExitoDomiciliaTDCDelegate) parentManager.getBaseDelegateForKey(ExitoDomiciliaTDCDelegate.EXITO_DOMICILIA_DELEGATE);
        if(delegate == null)
        {
            delegate = new ExitoDomiciliaTDCDelegate(data);
            parentManager.addDelegateToHashMap(ExitoDomiciliaTDCDelegate.EXITO_DOMICILIA_DELEGATE,delegate);
        }
        parentManager.showViewController(ExitoDomiciliaTDCViewController.class,0,false,null,null,parentManager.getCurrentViewController());

    }

    public void showDetalleDomiciliacion(OfertaDomiciliacionTDC data){
        DetalleDomiciliaTDCDelegate delegate = (DetalleDomiciliaTDCDelegate) parentManager.getBaseDelegateForKey(DetalleDomiciliaTDCDelegate.DETALLE_DOMICILIACION_DELEGATE);
        if(delegate == null)
        {
            delegate = new DetalleDomiciliaTDCDelegate(data);
            parentManager.addDelegateToHashMap(DetalleDomiciliaTDCDelegate.DETALLE_DOMICILIACION_DELEGATE,delegate);
        }
        parentManager.showViewController(DetalleDomiciliaTDCViewController.class,0,false,null,null,parentManager.getCurrentViewController());

    }



}
