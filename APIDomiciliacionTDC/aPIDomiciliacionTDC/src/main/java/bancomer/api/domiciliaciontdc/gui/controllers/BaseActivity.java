package bancomer.api.domiciliaciontdc.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.adobe.mobile.Config;

import bancomer.api.common.timer.TimerController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ControlEventos;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by WGUADARRAMA on 27/08/15.
 */
public class BaseActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Config.setContext(this.getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        Config.collectLifecycleData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Config.pauseCollectingLifecycleData();
    }

    @Override
    protected void onStop() {
        if(ControlEventos.isAppIsInBackground(this.getBaseContext())){ //Valida el estado de la app si es change Activity no hace nada
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "La app se fue a Background");
            }
            TimerController.getInstance().initTimer(this, TimerController.getInstance().getClase());

        }
        super.onStop();
    }
}
