package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.reactivacion.R;
import com.bancomer.mbanking.reactivacion.SuiteAppApiReactivacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.reactivacion.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ConfirmacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ReactivacionDelegate;
import suitebancomer.classes.common.reactivacion.GuiTools;
import suitebancomer.classes.gui.controllers.reactivacion.BaseViewController;
import suitebancomer.classes.gui.views.reactivacion.ListaDatosViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import tracking.reactivacion.TrackingHelper;
public class ConfirmacionAutenticacionViewController extends BaseViewController
		implements View.OnClickListener {

	private ImageButton confirmarButton;

	private LinearLayout contenedorPrincipal;
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;

	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;
	private EditText contrasena;
	private EditText nip;
	private EditText asm;
	private EditText cvv;
	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;

	// Nuevo Campo
	private TextView campoTarjeta;
	private LinearLayout contenedorCampoTarjeta;
	private EditText tarjeta;
	private TextView instruccionesTarjeta;

	private ConfirmacionAutenticacionDelegate confirmacionAutenticacionDelegate;
	//AMZ
		public BmovilViewsController parentManager;
		public String titulo;
		//AMZ

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,
				SuiteAppApiReactivacion.getResourceId("layout_bmovil_confirmacion_api_rec","layout"));
		
		setParentViewsController(SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController());

		setDelegate(getParentViewsController()
				.getBaseDelegateForKey(
						ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID));
		setTitulo();

		confirmacionAutenticacionDelegate = (ConfirmacionAutenticacionDelegate) getDelegate();
		confirmacionAutenticacionDelegate
				.setConfirmacionAutenticacionViewController(this);
		getParentViewsController().setCurrentActivityApp(this);
		findViews();
		scaleToScreenSize();

		
		confirmacionAutenticacionDelegate.consultaDatosLista();

		
		configuraPantalla();
		
		moverScroll();
		
		this.statusdesactivado = !((confirmacionAutenticacionDelegate
				.getOperationDelegate() instanceof ReactivacionDelegate));

		contrasena.addTextChangedListener(new BmovilTextWatcher(this));
		nip.addTextChangedListener(new BmovilTextWatcher(this));
		cvv.addTextChangedListener(new BmovilTextWatcher(this));
		asm.addTextChangedListener(new BmovilTextWatcher(this));
		tarjeta.addTextChangedListener(new BmovilTextWatcher(this));
		//AMZ
				parentManager = SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController();
			
				if(!(confirmacionAutenticacionDelegate.getOperationDelegate() instanceof ReactivacionDelegate)){
				//TrackingHelper.trackState("reactivacion", parentManager.estados);
				//}else{
				TrackingHelper.trackState("confautent", parentManager.estados);
				}
	}
	@Override
	protected void onDestroy() {
		parentViewsController.removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		habilitarBtnContinuar();
		if (statusdesactivado) {

		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
		//parentViewsController.consumeAccionesDePausa();
		statusdesactivado = true;
	}

	private void configuraPantalla() {
		mostrarContrasena(confirmacionAutenticacionDelegate
				.consultaDebePedirContrasena());
		mostrarCampoTarjeta(confirmacionAutenticacionDelegate
				.mostrarCampoTarjeta());
		mostrarNIP(confirmacionAutenticacionDelegate.consultaDebePedirNIP());
		mostrarASM(confirmacionAutenticacionDelegate
				.consultaInstrumentoSeguridad());
		mostrarCVV(confirmacionAutenticacionDelegate.consultaDebePedirCVV());

		final LinearLayout contenedorPadre = (LinearLayout) findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_campos_layout","id"));

		if (contenedorContrasena.getVisibility() == View.GONE
				&& contenedorNIP.getVisibility() == View.GONE
				&& contenedorASM.getVisibility() == View.GONE
				&& contenedorCVV.getVisibility() == View.GONE
				&& contenedorCampoTarjeta.getVisibility() == View.GONE) {
			// contenedorPadre.setVisibility(View.GONE);
			contenedorPadre.setBackgroundColor(0);
		}

		contenedorPadre.measure(MeasureSpec.UNSPECIFIED,
				MeasureSpec.UNSPECIFIED);
		final float camposHeight = contenedorPadre.getMeasuredHeight();
		// contenedorPadre.measure(MeasureSpec.UNSPECIFIED,
		// MeasureSpec.UNSPECIFIED);
		// float camposHeight = contenedorPadre.getMeasuredHeight();
		//
		// LinearLayout layoutListaDatos =
		// (LinearLayout)findViewById(R.id.confirmacion_lista_datos);
		// layoutListaDatos.measure(MeasureSpec.UNSPECIFIED,
		// MeasureSpec.UNSPECIFIED);
		// float listaHeight = layoutListaDatos.getMeasuredHeight();
		//
		// ViewGroup contenido =
		// (ViewGroup)this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
		// contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		// float contentHeight = contenido.getMeasuredHeight();
		//
		// System.out.println("Los valores " + camposHeight + " y " +
		// contentHeight + " y " + listaHeight);
		//
		// float margin =
		// getResources().getDimension(R.dimen.confirmacion_fields_initial_margin);

		final LinearLayout layoutListaDatos = (LinearLayout) findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_lista_datos","id"));
		layoutListaDatos.measure(MeasureSpec.UNSPECIFIED,
				MeasureSpec.UNSPECIFIED);
		final float listaHeight = layoutListaDatos.getMeasuredHeight();
		// float maximumSize = (contentHeight * 4) / 5;
		// System.out.println("Altura maxima " +maximumSize);
		// float elementsSize = listaHeight + camposHeight;
		// System.out.println("Altura mixta " +elementsSize);
		// float heightParaValidar = (contentHeight*3)/4;
		// System.out.println("heightParaValidar " +contentHeight);
		//
		// if (elementsSize >= contentHeight) {
		// RelativeLayout.LayoutParams camposLayout =
		// (RelativeLayout.LayoutParams)contenedorPadre.getLayoutParams();
		// camposLayout.addRule(RelativeLayout.BELOW,
		// R.id.confirmacion_lista_datos);
		// }

		final ViewGroup contenido = (ViewGroup) this.findViewById(
				android.R.id.content).getRootView();// findViewById(android.R.id.content);
		contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		final float contentHeight = contenido.getMeasuredHeight();

		//System.out.println("Los valores " + camposHeight + " y "
		//	+ contentHeight + " y " + listaHeight);

		final float margin = getResources().getDimension(
				R.dimen.confirmacion_fields_initial_margin);

		final float maximumSize = (contentHeight * 4) / 5;
		//System.out.println("Altura maxima " + maximumSize);
		final float elementsSize = listaHeight + camposHeight;
		//System.out.println("Altura mixta " + elementsSize);
		final float heightParaValidar = (contentHeight * 3) / 4;
		//System.out.println("heightParaValidar " + contentHeight);

		if (elementsSize >= contentHeight) {
			// RelativeLayout.LayoutParams camposLayout =
			// (RelativeLayout.LayoutParams)contenedorPadre.getLayoutParams();
			// camposLayout.addRule(RelativeLayout.BELOW,
			// R.id.confirmacion_lista_datos);
		}

		confirmarButton.setOnClickListener(this);
	}

	public void setTitulo() {

		final ConfirmacionAutenticacionDelegate confirmacionAutenticacionDelegate = (ConfirmacionAutenticacionDelegate) getDelegate();

		setTitle(confirmacionAutenticacionDelegate.consultaOperationsDelegate()
				.getTextoEncabezado(), confirmacionAutenticacionDelegate
				.consultaOperationsDelegate().getNombreImagenEncabezado());
	}

	@SuppressWarnings("deprecation")
	public void setListaDatos(final ArrayList<Object> datos) {
		final LinearLayout.LayoutParams params = new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		// params.topMargin =
		// getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
		// params.leftMargin =
		// getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
		// params.rightMargin =
		// getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);

		final ListaDatosViewController listaDatos = new ListaDatosViewController(
				this, params, parentViewsController);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.setTitulo(SuiteAppApiReactivacion.getResourceId("confirmation.subtitulo", "string"));
		listaDatos.showLista();
		final LinearLayout layoutListaDatos = (LinearLayout) findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_lista_datos","id"));
		layoutListaDatos.addView(listaDatos);
	}

	public void pideContrasenia() {
		findViewById(SuiteAppApiReactivacion.getResourceId("campo_confirmacion_contrasena_layout","id")).setVisibility(
				View.GONE);
	}

	public void pideClaveSeguridad() {
		findViewById(SuiteAppApiReactivacion.getResourceId("campo_confirmacion_asm_layout","id")).setVisibility(
				View.GONE);
	}

	/*
	* 
	*/
	public void mostrarContrasena(final boolean visibility) {
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE
				: View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
		if (visibility) {
			campoContrasena.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoContrasenia());
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}

	/*
	* 
	*/
	public void mostrarNIP(final boolean visibility) {
		contenedorNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE : View.GONE);
		if (visibility) {
			campoNIP.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoNip());
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			final String instrucciones = confirmacionAutenticacionDelegate
					.getTextoAyudaNIP();
			if (instrucciones.equals("")) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}

	/*
	* 
	*/
	public void mostrarASM(final TipoOtpAutenticacion tipoOTP) {
		if (tipoOTP == TipoOtpAutenticacion.ninguno) {
			contenedorASM.setVisibility(View.GONE);
			campoASM.setVisibility(View.GONE);
			asm.setVisibility(View.GONE);
			asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
		} else if (tipoOTP == TipoOtpAutenticacion.codigo
				|| tipoOTP == TipoOtpAutenticacion.registro) {
			contenedorASM.setVisibility(View.VISIBLE);
			campoASM.setVisibility(View.VISIBLE);
			asm.setVisibility(View.VISIBLE);
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.ASM_LENGTH);
			asm.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			cambiarAccionTexto(nip);
			//asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
		}

		final TipoInstrumento tipoInstrumento = confirmacionAutenticacionDelegate
				.consultaTipoInstrumentoSeguridad();

		switch (tipoInstrumento) {
		case OCRA:
			campoASM.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoOCRA());
			//asm.setTransformationMethod(null);
			break;
		case DP270:
			campoASM.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoDP270());
			//asm.setTransformationMethod(null);
			break;
		case SoftToken:
			if (SuiteAppApiReactivacion.getSofttokenStatus()) {
				asm.setText(Constants.DUMMY_OTP);
				asm.setEnabled(false);
				campoASM.setText(confirmacionAutenticacionDelegate
						.getEtiquetaCampoSoftokenActivado());
			} else {
				asm.setText("");
				asm.setEnabled(true);
				campoASM.setText(confirmacionAutenticacionDelegate
						.getEtiquetaCampoSoftokenDesactivado());
				//asm.setTransformationMethod(null);
			}
			// String otp = GeneraOTPSTDelegate.generarOtpTiempo();
			// if(null != otp) {
			// asm.setText(otp);
			// asm.setEnabled(false);
			// }
			// campoASM.setText(confirmacionAutenticacionDelegate.getOperationDelegate().getEtiquetaCampoSoftokenActivado());
			// break;
		default:
			break;
		}
		final String instrucciones = confirmacionAutenticacionDelegate
				.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
		if ("".equals(instrucciones)) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}
	}

	private void cambiarAccionTexto(final EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}

	public String pideContrasena() {
		if (contrasena.getVisibility() == View.GONE) {
			return "";
		} else {
			return contrasena.getText().toString();
		}
	}

	public String pideNIP() {
		if (nip.getVisibility() == View.GONE) {
			return "";
		} else {
			return nip.getText().toString();
		}
	}

	public String pideASM() {
		if (asm.getVisibility() == View.GONE) {
			return "";
		} else {
			return asm.getText().toString();
		}
	}

	public String pideCVV() {
		if (cvv.getVisibility() == View.GONE) {
			return "";
		} else {
			return cvv.getText().toString();
		}
	}

	/*
	* 
	*/
	public void mostrarCVV(final boolean visibility) {
		contenedorCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE : View.GONE);
		instruccionesCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);

		if (visibility) {
			campoCVV.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoCVV());
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			final String instrucciones = confirmacionAutenticacionDelegate
					.getTextoAyudaCVV();
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility("".equals(instrucciones) ? View.GONE
					: View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}

	@Override
	public void onClick(final View v) {
		confirmarButton.setEnabled(false);
		if (v == confirmarButton && !parentViewsController.isActivityChanging()) {
			botonConfirmarClick();
		}
	}

	public void botonConfirmarClick() {
		confirmacionAutenticacionDelegate.enviaPeticionOperacion();
		if(confirmacionAutenticacionDelegate.res == true){
			//AMZ inicio
			titulo = getString(confirmacionAutenticacionDelegate.getOperationDelegate().getTextoEncabezado());

			final Map<String,Object> OperacionRealizadaMap = new HashMap<String, Object>();
				if(titulo == getString(R.string.bmovil_consultar_dineromovil_titulo)){
					//AMZ
					OperacionRealizadaMap.put("evento_realizada","event52");
					OperacionRealizadaMap.put("&&products","operaciones;cancelar dinero movil");
					OperacionRealizadaMap.put("eVar12","operacion_cancelada");
					TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);

				}else if(titulo == getString(R.string.bmovil_cambio_cuenta_title)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+cambio cuenta");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == getString(R.string.bmovil_configurar_correo_titulo)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+configura correo");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == getString(R.string.bmovil_cambio_telefono_title)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+cambio celular");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == getString(R.string.bmovil_suspender_title)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+suspender o cancelar");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == getString(R.string.bmovil_cancelar_title)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+suspender o cancelar");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == getString(R.string.bmovil_configurar_montos_title)){
					//AMZ
					OperacionRealizadaMap.put("evento_realizada","event52");
					OperacionRealizadaMap.put("&&products","operaciones;admin+configura montos");
					OperacionRealizadaMap.put("eVar12","operacion realizada");
					TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}
			}
	}

	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {

		if(SuiteAppApiReactivacion.getInstance().getBmovilApplication().getViewsController().getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID) !=null	){
			confirmacionAutenticacionDelegate =
					(ConfirmacionAutenticacionDelegate)SuiteAppApiReactivacion.getInstance().getBmovilApplication().getViewsController().getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		}
			confirmacionAutenticacionDelegate
				.analyzeResponse(operationId, response);
	}

	private void findViews() {
		
		
		contenedorPrincipal = (LinearLayout) findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_lista_datos","id"));
		contenedorContrasena = (LinearLayout) findViewById(SuiteAppApiReactivacion.getResourceId("campo_confirmacion_contrasena_layout","id"));
		contenedorNIP = (LinearLayout) findViewById(SuiteAppApiReactivacion.getResourceId("campo_confirmacion_nip_layout","id"));
		contenedorASM = (LinearLayout) findViewById(SuiteAppApiReactivacion.getResourceId("campo_confirmacion_asm_layout","id"));
		contenedorCVV = (LinearLayout) findViewById(SuiteAppApiReactivacion.getResourceId("campo_confirmacion_cvv_layout","id"));

		contrasena = (EditText) contenedorContrasena
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_contrasena_edittext","id"));
		nip = (EditText) contenedorNIP
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_nip_edittext","id"));
		asm = (EditText) contenedorASM
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_asm_edittext","id"));
		cvv = (EditText) contenedorCVV
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_cvv_edittext","id"));

		campoContrasena = (TextView) contenedorContrasena
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_contrasena_label","id"));
		campoNIP = (TextView) contenedorNIP
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_nip_label","id"));
		campoASM = (TextView) contenedorASM
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_asm_label","id"));
		campoCVV = (TextView) contenedorCVV
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_cvv_label","id"));

		instruccionesContrasena = (TextView) contenedorContrasena
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_contrasena_instrucciones_label","id"));
		instruccionesNIP = (TextView) contenedorNIP
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_nip_instrucciones_label","id"));
		instruccionesASM = (TextView) contenedorASM
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_asm_instrucciones_label","id"));
		instruccionesCVV = (TextView) contenedorCVV
				.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_cvv_instrucciones_label","id"));

		contenedorCampoTarjeta = (LinearLayout) findViewById(SuiteAppApiReactivacion.getResourceId("campo_confirmacion_campotarjeta_layout","id"));
		tarjeta = (EditText) contenedorCampoTarjeta.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_campotarjeta_edittext","id"));
		campoTarjeta = (TextView) contenedorCampoTarjeta.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_campotarjeta_label","id"));
		instruccionesTarjeta = (TextView) contenedorCampoTarjeta.findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_campotarjeta_instrucciones_label","id"));



		confirmarButton = (ImageButton) findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_confirmar_button","id"));
	}

	private void scaleToScreenSize() {

		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		// guiTools.scaleAll(contenedorPrincipal);

		// guiTools.scaleAll((LinearLayout)findViewById(R.id.confirmacion_parent_parent_view));

		guiTools.scale(contenedorPrincipal);
		guiTools.scale(findViewById(SuiteAppApiReactivacion.getResourceId("confirmacion_campos_layout","id")));

		guiTools.scale(contenedorContrasena);
		guiTools.scale(contenedorNIP);
		guiTools.scale(contenedorASM);
		guiTools.scale(contenedorCVV);

		guiTools.scale(contrasena, true);
		guiTools.scale(nip, true);
		guiTools.scale(asm, true);
		guiTools.scale(cvv, true);

		guiTools.scale(campoContrasena, true);
		guiTools.scale(campoNIP, true);
		guiTools.scale(campoASM, true);
		guiTools.scale(campoCVV, true);

		guiTools.scale(instruccionesContrasena, true);
		guiTools.scale(instruccionesNIP, true);
		guiTools.scale(instruccionesASM, true);
		guiTools.scale(instruccionesCVV, true);

		// nuevo campo
		guiTools.scale(contenedorCampoTarjeta);
		guiTools.scale(tarjeta, true);
		guiTools.scale(campoTarjeta, true);
		guiTools.scale(instruccionesTarjeta, true);

		guiTools.scale(confirmarButton);

	}

	public void limpiarCampos() {
		contrasena.setText("");
		nip.setText("");
		asm.setText("");
		cvv.setText("");
		tarjeta.setText("");
	}

	public void habilitarBtnContinuar() {
		confirmarButton.setEnabled(true);
	}

	public String pideTarjeta() {
		if (tarjeta.getVisibility() == View.GONE) {
			return "";
		} else {
			return tarjeta.getText().toString();
		}
	}

	private void mostrarCampoTarjeta(boolean visibility) {
		contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE
				: View.GONE);
		campoTarjeta.setVisibility(visibility ? View.VISIBLE : View.GONE);
		tarjeta.setVisibility(visibility ? View.VISIBLE : View.GONE);
		if (visibility) {
			campoTarjeta.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoTarjeta());
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(5);
			tarjeta.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
			final String instrucciones = confirmacionAutenticacionDelegate
					.getTextoAyudaTarjeta();
			if ("".equals(instrucciones)) {
				instruccionesTarjeta.setVisibility(View.GONE);
			} else {
				instruccionesTarjeta.setVisibility(View.VISIBLE);
				instruccionesTarjeta.setText(instrucciones);
			}
		} else {
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}

	}

}
