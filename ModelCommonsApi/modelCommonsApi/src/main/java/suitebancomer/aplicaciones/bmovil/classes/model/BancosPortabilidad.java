package suitebancomer.aplicaciones.bmovil.classes.model;

/**
 * Created by root on 08/07/16.
 */

import org.json.JSONObject;

import java.io.Serializable;

public class BancosPortabilidad implements Serializable{

    private String id;
    private String name;
    private JSONObject array;

    public BancosPortabilidad(final String id, final String name) {
        this.id = emptyIfNull(id);
        this.name = emptyIfNull(name);
    }

    public BancosPortabilidad(final String id, final JSONObject arrCuentas) {
        this.id = emptyIfNull(id);
        this.array = arrCuentas;
        this.name = "*ARRAY*";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONObject getArray() {
        return array;
    }

    public void setArray(JSONObject array) {
        this.array = array;
    }

    private static String emptyIfNull(final String value) {
        return (value != null) ? value : "";
    }
}
