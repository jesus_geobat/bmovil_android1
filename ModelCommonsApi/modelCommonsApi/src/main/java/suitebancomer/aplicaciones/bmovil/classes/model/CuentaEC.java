package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.Serializable;

public class CuentaEC implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String numeroCuenta;
	private String estadoEnvio;
	private boolean flag;
	private String fecha;
	
	public boolean isFlag() {
	    return flag;
	}
	public void setFlag(final boolean flag) {
	    this.flag = flag;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(final String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getEstadoEnvio() {
		return estadoEnvio;
	}
	public void setEstadoEnvio(final String estadoEnvio) {
		this.estadoEnvio = estadoEnvio;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(final String fecha) {
		this.fecha = fecha;
	}	
	
}
