/**
 *
 */
package suitebancomercoms.aplicaciones.bmovil.classes.common;

import android.content.Context;

import org.spongycastle.util.encoders.Base64;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.ToolsCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Clase que permite realizar la encriptacion de los datos sencibles
 *
 * @author evaltierrah
 * @version 1.0
 */
public class Encripcion extends EncripcionHelper {

    /**
     * Cadena que contine las comillas
     */
    private static final String COMILLAS = "\"";
    /**
     * Cadena que contine los dos puntos
     */
    private static final String DOS_PUNTOS = ":";
    /**
     * Constante para el mensaje del log
     */
    private static final String MENSAJE_LOG = "Cadena encriptada ->";
    /**
     * Cadena que contine los coma
     */
    private static final String COMA = ",";

    /**
     * Inicializa el contexto
     *
     * @param contextParam Contexto de la aplicacion
     */
    public static void setContext(final Context contextParam) {
        context = contextParam;
        loadProperties();
    }

    /**
     * Metodo que recibe el map con los parametros y obtiene solo los sencibles,
     * los elimina y los pasa a formato JSON para su encriptacion
     *
     * @param params              Map con los parametros que se envian al servidor
     * @param parametrosEncriptar Lista de parametros a encriptar
     */
    public static void encriptaCadenaAutenticacion(
            final Map<String, String> params, final List<String> parametrosEncriptar) {
        if (EncriptarConstants.encriptar) {
            final StringBuilder json = new StringBuilder();
            String cadenaEncriptada = Constants.EMPTY_STRING;
            for (final String llave : parametrosEncriptar) {
                json.append(COMILLAS);
                json.append(llave);
                json.append(COMILLAS);
                json.append(DOS_PUNTOS);
                json.append(COMILLAS);
                String valor = params.get(llave);
                if (valor != null && valor.startsWith(COMILLAS) && valor.endsWith(COMILLAS)) {
                    valor = valor.replaceAll(COMILLAS, Constants.EMPTY_STRING);
                }
                json.append(valor);
                json.append(COMILLAS);
                json.append(COMA);
                params.remove(llave);
            }
            if (json.length() > 0) {
                json.setLength(json.length() - 1);
                cadenaEncriptada = encripta(json.toString());
            }
            params.remove(ServerConstantsCom.PARAMS_TEXTO_EN);
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), MENSAJE_LOG + cadenaEncriptada, ServerCommons.ALLOW_LOG);
            params.put(ServerConstantsCom.PARAMS_TEXTO_EN,
                    EncriptarConstants.IZQ_ENCRIPTAR + cadenaEncriptada +
                            EncriptarConstants.DER_ENCRIPTAR);
        }
    }

    /**
     * Metodo que recibe el map con los parametros y obtiene solo los sencibles,
     * los elimina si la vandera lo indica en caso contrario lo deja vacios y los pasa a formato JSON para su encriptacion
     *
     * @param params              Map con los parametros que se envian al servidor
     * @param parametrosEncriptar Lista de parametros a encriptar
     */
    public static void encriptaCadenaAutenticacion(
            final Map<String, String> params, final List<String> parametrosEncriptar, final Boolean removeParam) {
        if (EncriptarConstants.encriptar) {
            final StringBuilder json = new StringBuilder();
            String cadenaEncriptada = Constants.EMPTY_STRING;
            for (final String llave : parametrosEncriptar) {
                json.append(COMILLAS);
                json.append(llave);
                json.append(COMILLAS);
                json.append(DOS_PUNTOS);
                json.append(COMILLAS);
                String valor = params.get(llave);
                if (valor != null && valor.startsWith(COMILLAS) && valor.endsWith(COMILLAS)) {
                    valor = valor.replaceAll(COMILLAS, Constants.EMPTY_STRING);
                }
                json.append(valor);
                json.append(COMILLAS);
                json.append(COMA);
                if (removeParam) {
                    params.remove(llave);
                } else {
                    params.put(llave, Constants.EMPTY_STRING);
                }
            }
            if (json.length() > 0) {
                json.setLength(json.length() - 1);
                cadenaEncriptada = encripta(json.toString());
            }
            params.remove(ServerConstantsCom.PARAMS_TEXTO_EN);
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), MENSAJE_LOG + cadenaEncriptada, ServerCommons.ALLOW_LOG);
            params.put(ServerConstantsCom.PARAMS_TEXTO_EN,
                    EncriptarConstants.IZQ_ENCRIPTAR + cadenaEncriptada +
                            EncriptarConstants.DER_ENCRIPTAR);
        }
    }


    public static void encriptarPeticionString(
            final Map<String, String> params, final List<String> parametrosEncriptar,
            final String separador, final String reemplazo, final Boolean removeParam) {
        String valor = Constants.EMPTY_STRING;
        String cadenaAEncriptar = Constants.EMPTY_STRING;
        if (EncriptarConstants.encriptar) {
            for (final String llave : parametrosEncriptar) {
                valor = params.get(llave);
                if (valor != null && valor.startsWith(COMILLAS) && valor.endsWith(COMILLAS)) {
                    valor = valor.replaceAll(COMILLAS, Constants.EMPTY_STRING);
                }
                if (removeParam) {
                    params.remove(llave);
                } else {
                    params.put(llave, valor.replaceAll(".", reemplazo));
                }
                cadenaAEncriptar += llave + valor + separador;
            }
            if (cadenaAEncriptar.length() > 0) {
                cadenaAEncriptar = cadenaAEncriptar.substring(0, cadenaAEncriptar.length() - 1);
                cadenaAEncriptar = reemplazo + cadenaAEncriptar + reemplazo;
            }
            params.remove(ServerConstantsCom.PARAMS_TEXTO_EN);
            final String cadenaEncriptada = encripta(cadenaAEncriptar);
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), MENSAJE_LOG + cadenaEncriptada, ServerCommons.ALLOW_LOG);
            params.put(ServerConstantsCom.PARAMS_TEXTO_EN, cadenaEncriptada);
        }
    }

    /**
     * Metodo que realiza la encripcion de datos cuando la peticion es por medio
     * de string
     *
     * @param params              Map con los parametros que se envian al servidor
     * @param parametrosEncriptar Lista de parametros a encriptar
     * @param separador           Indica el separador que se ocupara para separar los caracteres
     * @param reemplazo           El caracter a reemplazar
     */
    public static void encriptarPeticionString(
            final Map<String, String> params, final List<String> parametrosEncriptar,
            final String separador, final String reemplazo) {
        String valor = Constants.EMPTY_STRING;
        String cadenaAEncriptar = Constants.EMPTY_STRING;
        if (EncriptarConstants.encriptar) {
            for (final String llave : parametrosEncriptar) {
                valor = params.get(llave);
                params.remove(llave);
                params.put(llave, valor.replaceAll(".", reemplazo));
                cadenaAEncriptar += llave + valor + separador;
            }
            if (cadenaAEncriptar.length() > 0) {
                cadenaAEncriptar = cadenaAEncriptar.substring(0, cadenaAEncriptar.length() - 1);
            }
            params.remove(ServerConstantsCom.PARAMS_TEXTO_EN);
            final String cadenaEncriptada = encripta(cadenaAEncriptar);
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), MENSAJE_LOG + cadenaEncriptada, ServerCommons.ALLOW_LOG);
            params.put(ServerConstantsCom.PARAMS_TEXTO_EN, cadenaEncriptada);
        }
    }

    /**
     * Metodo que realiza la encripcion de datos cuando la peticion es por medio
     * de string
     *
     * @param params              Map con los parametros que se envian al servidor
     * @param parametrosEncriptar Lista de parametros a encriptar
     * @param separador           Indica el separador que se ocupara para separar los caracteres
     * @param reemplazo           El caracter a reemplazar
     * @param init                El caracter inicial y final de la cadena a encriptar
     */
    public static void encriptarPeticionString(
            final Map<String, String> params, final List<String> parametrosEncriptar,
            final String separador, final String reemplazo, final String init) {
        String valor = Constants.EMPTY_STRING;
        String cadenaAEncriptar = Constants.EMPTY_STRING;
        if (EncriptarConstants.encriptar) {
            for (final String llave : parametrosEncriptar) {
                valor = params.get(llave);
                params.remove(llave);
                params.put(llave, valor.replaceAll(".", reemplazo));
                cadenaAEncriptar += llave + valor + separador;
            }
            if (cadenaAEncriptar.length() > 0) {
                cadenaAEncriptar = cadenaAEncriptar.substring(0, cadenaAEncriptar.length() - 1);
            }
            params.remove(ServerConstantsCom.PARAMS_TEXTO_EN);
            cadenaAEncriptar = init + cadenaAEncriptar + init;
            final String cadenaEncriptada = encripta(cadenaAEncriptar);
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), MENSAJE_LOG + cadenaEncriptada, ServerCommons.ALLOW_LOG);
            params.put(ServerConstantsCom.PARAMS_TEXTO_EN, cadenaEncriptada);
        }
    }

    /**
     * Metodo que encripta una cadena
     *
     * @param cadena Cadena a encriptar
     * @return Regresa la cadena encriptada
     */
    public static String encripta(final String cadena) {
        final byte[] byteEncriptado = rsaEncrypt(cadena);
        String cadenaEncriptada = Constants.EMPTY_STRING;
        if (byteEncriptado != null) {
            cadenaEncriptada = Base64.toBase64String(byteEncriptado);
        }
        return urlencode(cadenaEncriptada);
    }

    /**
     * generates a RSA public key with given modulus and public exponent
     *
     * @return PublicKey Regresa la llave publica
     */
    private static PublicKey generateRsaPublicKey() {
        try {
            final BigInteger modulus = new BigInteger(publicModulus, 16);
            final BigInteger exponent = new BigInteger(publicExponent, 16);
            final RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(
                    modulus, exponent);
            final KeyFactory fact = KeyFactory
                    .getInstance(EncriptarConstants.TIPO_ENCRIPCION);
            return fact.generatePublic(rsaPublicKeySpec);

        } catch (NoSuchAlgorithmException e) {
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        } catch (InvalidKeySpecException e) {
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        }
        return null;
    }

    /**
     * RSA encrypt function (RSA / ECB / PKCS1-Padding)
     *
     * @param data Cadena a encriptar
     * @return Regresa un arreglo de Bytes con la cadena encriptada
     */
    private static byte[] rsaEncrypt(final String data) {
        final byte[] dataToEncrypt = data.getBytes();
        byte[] encryptedData = null;
        try {
            final PublicKey pubKey = generateRsaPublicKey();
            final Cipher cipher = Cipher
                    .getInstance(EncriptarConstants.INSTANCIA_CIPHER);
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            encryptedData = cipher.doFinal(dataToEncrypt);
        } catch (NoSuchAlgorithmException e) {
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        } catch (NoSuchPaddingException e) {
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        } catch (InvalidKeyException e) {
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        } catch (IllegalBlockSizeException e) {
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        } catch (BadPaddingException e) {
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        }
        return encryptedData;
    }

}
