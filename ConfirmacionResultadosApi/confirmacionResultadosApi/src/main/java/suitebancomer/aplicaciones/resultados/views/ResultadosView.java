package suitebancomer.aplicaciones.resultados.views;

import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.to.ResultadosViewTo;

/**
 * 
 * @author lbermejo
 *
 */
public interface ResultadosView extends IBaseView{

	void onSuccess();
	void configuraPantalla();
	ResultadosViewTo getResultadosViewTo();
	void setResultadosViewTo(final ResultadosViewTo viewTo);
	void setListaDatos(final ArrayList<Object> datos);
	void setListaClave(final ArrayList<Object> datos, final Boolean isConsultaInterbancario);
	void setOpcionesMenu(final ResultadosViewTo to);
}
