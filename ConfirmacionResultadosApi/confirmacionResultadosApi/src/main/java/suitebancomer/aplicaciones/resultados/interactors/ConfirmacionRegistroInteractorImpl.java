/**
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.resultados.listeners.OnConfirmRegistroFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionRegistroServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import android.os.Handler;
import android.text.TextUtils;

/**
 * @author amgonzalez
 *
 */
public class ConfirmacionRegistroInteractorImpl implements ConfirmacionRegistroInteractor {
	
	private final IConfirmacionRegistroServiceProxy proxy;
	
	public ConfirmacionRegistroInteractorImpl(final IConfirmacionRegistroServiceProxy proxy) {
		this.proxy = proxy;
	}
	
	/**
	 * @return the proxy
	 */
	public IConfirmacionRegistroServiceProxy getProxy() {
		return proxy;
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.interactors.ConfirmacionInteractor#getDatos(suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener)
	 */
	@Override
	public void getListaDatos(final OnConfirmRegistroFinishedListener listener) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedListaDatos((ArrayList<Object>) 
						proxy.getListaDatos());
			}
		}
		);
	}
	
	//getDatosRegistroExitoso()
	@Override
	public void getDatosRegistroExitoso(final OnConfirmRegistroFinishedListener listener) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedDatosRegistroOk( (ArrayList<Object>) 
						proxy.getListaDatosRegistroExitoso() );
			}
		}
		);
	}
	
	
	@Override
	public void getShowFields(final OnConfirmRegistroFinishedListener listener) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedValidShowFields(
						proxy.showFields());
			}
		}
		);
	}
	
	@Override
	public Integer consultaOperationsIdTextoEncabezado() {
		return proxy.consultaOperationsIdTextoEncabezado();
	}
	
	//TODO
	public void doConfirmacionOperacion(final ConfirmacionViewTo viewTo,
							final OnConfirmRegistroFinishedListener listener){

		if( viewTo.getShowContrasena() ){
			if (TextUtils.isEmpty(viewTo.getContrasena() )){
                listener.onErrorContrasena(false);
                return;
            }else if(viewTo.getContrasena().trim().length() 
            		!= Constants.PASSWORD_LENGTH){
            	listener.onErrorContrasena(true);
            	return;
            }
		}
		
		if( viewTo.getShowNip() ){
			if (TextUtils.isEmpty(viewTo.getNip() )){
                listener.onErrorNip(false);
                return;
            }else if(viewTo.getNip().trim().length() 
            		!= Constants.NIP_LENGTH ){
            	listener.onErrorNip(true);
            	return;
            }
		}
		
		if( viewTo.getTokenAMostrar()
				!= Constants.TipoOtpAutenticacion.ninguno){
			
			if (TextUtils.isEmpty(viewTo.getAsm() )){
				final int msg = proxy.getMessageAsmError( viewTo.getInstrumentoSeguridad());
				/*
				 * en el proxy 
				 * switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				 */
                listener.onErrorAsm(msg, false);
                return;
            }else if(viewTo.getAsm().trim().length() 
            		!= Constants.ASM_LENGTH ){
				final int msg = proxy.getMessageAsmError(viewTo.getInstrumentoSeguridad());
            	listener.onErrorAsm(msg, true);
            	return;
            }
		}
		
		if( viewTo.getShowCvv() ){
			if (TextUtils.isEmpty(viewTo.getCvv() )){
                listener.onErrorCvv(false);
                return;
            }else if(viewTo.getCvv().trim().length() 
            		!= Constants.CVV_LENGTH ){
            	listener.onErrorCvv(true);
            	return;
            }
		}
		
		// operacion
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				 //TODO enviar Activity
				listener.onFinishedConfirmacionOperacion(
						proxy.doOperation(viewTo));
			}
		}
		);
		
	}

}
