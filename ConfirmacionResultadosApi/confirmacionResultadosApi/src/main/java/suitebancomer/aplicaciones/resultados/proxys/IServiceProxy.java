/**
 * 
 */
package suitebancomer.aplicaciones.resultados.proxys;

import java.io.Serializable;

import suitebancomer.aplicaciones.resultados.to.ParamTo;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * @author lbermejo
 *
 */
public interface IServiceProxy extends Serializable {
	
	BaseDelegateCommons getDelegate();
	Boolean doOperation(final ParamTo to); //BaseViewControllerCommons view,
		
}
