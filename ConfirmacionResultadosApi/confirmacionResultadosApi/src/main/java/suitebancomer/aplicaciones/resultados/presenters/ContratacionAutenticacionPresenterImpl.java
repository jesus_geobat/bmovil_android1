/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.interactors.ContratacionAutenticacionInteractor;
import suitebancomer.aplicaciones.resultados.interactors.ContratacionAutenticacionInteractorImpl;
import suitebancomer.aplicaciones.resultados.listeners.OnContrataAutoFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IContratacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.views.ContratacionAutenticacionView;
import android.content.Intent;

/**
 * @author amgonzalez
 *
 */
public class ContratacionAutenticacionPresenterImpl 
implements ContratacionAutenticacionPresenter, OnContrataAutoFinishedListener {
	
	private final ContratacionAutenticacionView cview;
	private final ContratacionAutenticacionInteractor interactor;

	public ContratacionAutenticacionPresenterImpl(final ContratacionAutenticacionView view, final Intent intent){
		this.cview = view;
		this.interactor = new ContratacionAutenticacionInteractorImpl(
				getServiceProxy(intent)); 
	}
	
	
	@Override 
	public void onResume() {
        //mainView.showProgress();
        //findItemsInteractor.findItems(this);
    }
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.presenters.ConfirmacionPresenter#show()
	 */
	@Override
	public void show() {
		// TODO Auto-generated method stub
		/*
		 *  mainView.showProgress();
        findItemsInteractor.findItems(this);
		 */
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener#onError()
	 */
	
	@Override
	public void onError() {
		// TODO Auto-generated method stub
	}
	
	private IContratacionAutenticacionServiceProxy getServiceProxy(final Intent intent){
		final IContratacionAutenticacionServiceProxy p = (IContratacionAutenticacionServiceProxy)
				SuiteAppCRApi.getInstance().getProxy();
		return p;
	}
	
	public void limpiarCampos(){
		cview.reset();
	}

	@Override
	public void getListaDatos() {
		interactor.getListaDatos(this);
	}

	@Override
	public void onFinishedListaDatos(final ArrayList<Object> list) {
		cview.setListaDatos(list);
	}

	@Override
	public void getShowFields() {
		interactor.getShowFields(this);
	}

	@Override
	public void onFinishedValidShowFields(final ConfirmacionViewTo fields) {
		cview.setViewTo(fields);
	}

	@Override
	public void confirmarClick(final ConfirmacionViewTo viewTo) {
		interactor.doConfirmacionOperacion(viewTo, this);
	}

	@Override
	public void onFinishedConfirmacionOperacion(final Boolean resp) {
		if ( resp.booleanValue() ){
			final Constants.Perfil clienteperfil = interactor.consultaClienteProfile(this);
			//Session.getInstance(SuiteAppContratacion.appContext).getClientProfile())
			cview.onSuccess(clienteperfil);
		}else{
			cview.showMessage("Error!");
		}

	}

	@Override
	public void onErrorContrasena(final boolean lenght) {
		cview.showMensajePideContrasena(lenght);
	}

	@Override
	public void onErrorNip(final boolean lenght) {
		cview.showMensajePideNip(lenght);
	}

	@Override
	public void onErrorCvv(final boolean lenght) {
		cview.showMensajePideCVV(lenght);
	}


	@Override
	public void onErrorAsm(final int msg, final boolean lenght) {
		cview.showMensajePideToken(msg, lenght);
	}

	@Override
	public void onErrorTarjeta(final boolean lenght) {
		cview.showMensajePideTarjeta(lenght);
		
	}
	
	public void onErrorTerminos(){
		cview.showMensajeErrorTerminos();
	}
	
	public void consultarTerminosDeUso(){
		interactor.consultarTerminosDeUso();
	}
}
