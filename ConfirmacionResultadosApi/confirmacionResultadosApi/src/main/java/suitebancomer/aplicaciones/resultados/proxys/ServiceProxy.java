/**
 * 
 */
package suitebancomer.aplicaciones.resultados.proxys;

import java.io.Serializable;

/**
 * @author lbermejo
 *
 */
public abstract class ServiceProxy implements IServiceProxy, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3186034731888093719L;
	//private IServiceProxy realProxy;
	
	//private ServiceProxy(){}
	//public ServiceProxy(IServiceProxy realProxy) {
	//	this.realProxy = realProxy;
	//}
	
	//@Override
	//public BaseDelegateCommons getDelegate() {
		// TODO Auto-generated method stub
	//	return null;
	//}
	
	/*
	@Override
	public Boolean doOperation(ParamTo to) {
		// TODO Auto-generated method stub
		return null;
	}*/
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.proxys.IServiceProxy#doOperation(java.lang.Object)
	 * /
	@Override
	public void doOperation(ParamTo to) {
		realProxy.doOperation(to);
	}*/

}
