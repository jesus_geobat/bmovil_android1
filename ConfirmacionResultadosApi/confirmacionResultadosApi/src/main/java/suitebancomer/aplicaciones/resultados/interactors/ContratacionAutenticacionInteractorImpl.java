/**
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import java.util.ArrayList;


import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.listeners.OnContrataAutoFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IContratacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtp;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtpBmovil;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.bancomer.base.SuiteApp;

/**
 * @author amgonzalez
 *
 */
public class ContratacionAutenticacionInteractorImpl implements ContratacionAutenticacionInteractor {
	
	private final IContratacionAutenticacionServiceProxy proxy;
	
	public ContratacionAutenticacionInteractorImpl(final IContratacionAutenticacionServiceProxy proxy) {
		this.proxy = proxy;
	}

	@Override
	public void getListaDatos(final OnContrataAutoFinishedListener listener) {
		//new Handler().post(new Runnable() {
		//	@Override
		//	public void run() {
				listener.onFinishedListaDatos((ArrayList<Object>) 
						proxy.getListaDatos());
		//	}
		//}
		//);
	}

	@Override
	public void getShowFields(final OnContrataAutoFinishedListener listener) {
		//new Handler().post(new Runnable() {
		//	@Override
		//	public void run() {
				listener.onFinishedValidShowFields(
						proxy.showFields());

				/* en el proxy 
				//mostrarContrasena(confirmacionDelegate.consultaDebePedirContrasena());
				//mostrarNIP(confirmacionDelegate.consultaDebePedirNIP());
				//mostrarASM(confirmacionDelegate.consultaInstrumentoSeguridad());
				//mostrarCVV(confirmacionDelegate.consultaDebePedirCVV());
				//mostrarCampoTarjeta(confirmacionDelegate.mostrarCampoTarjeta());
				 */
		//	}
	//	}
	//	);
		
	}

	@Override
	public void doConfirmacionOperacion(final ConfirmacionViewTo viewTo,
			final OnContrataAutoFinishedListener listener) {
		
		
		if( viewTo.getShowContrasena() ){
			if (TextUtils.isEmpty(viewTo.getContrasena() )){
                listener.onErrorContrasena(false);
                return;
            }else if(viewTo.getContrasena().trim().length() 
            		!= Constants.PASSWORD_LENGTH){
            	listener.onErrorContrasena(true);
            	return;
            }
		}
		
		if( viewTo.getShowTarjeta() ){
			if (TextUtils.isEmpty(viewTo.getTarjeta() )){
                listener.onErrorTarjeta(false);
                return;
            }else if(viewTo.getTarjeta().trim().length() 
            		!= ApiConstants.TARJETA_LENGTH  ){
            	listener.onErrorTarjeta(true);
            	return;
            }
		}
		
		if( viewTo.getShowNip() ){
			if (TextUtils.isEmpty(viewTo.getNip() )){
                listener.onErrorNip(false);
                return;
            }else if(viewTo.getNip().trim().length() 
            		!= Constants.NIP_LENGTH ){
            	listener.onErrorNip(true);
            	return;
            }
		}
		
		if( viewTo.getTokenAMostrar()
				!= Constants.TipoOtpAutenticacion.ninguno){
			
			if (TextUtils.isEmpty(viewTo.getAsm() )){
				final int msg = proxy.getMessageAsmError( viewTo.getInstrumentoSeguridad());
				/*
				 * en el proxy 
				 * switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				 */
                listener.onErrorAsm(msg, false);
                return;
            }else if(viewTo.getAsm().trim().length() 
            		!= Constants.ASM_LENGTH ){
				final int msg = proxy.getMessageAsmError(viewTo.getInstrumentoSeguridad());
            	listener.onErrorAsm(msg, true);
            	return;
            }
		}
		
		if( viewTo.getShowCvv() ){
			if (TextUtils.isEmpty(viewTo.getCvv() )){
                listener.onErrorCvv(false);
                return;
            }else if(viewTo.getCvv().trim().length() 
            		!= Constants.CVV_LENGTH ){
            	listener.onErrorCvv(true);
            	return;
            }
		}
		
		if(!viewTo.getOkTerminos() ) {
			listener.onErrorTerminos();
			return;
		}
		
		String newToken = null;
		if ( viewTo.getTokenAMostrar() != TipoOtpAutenticacion.ninguno
				&& viewTo.getInstrumentoSeguridad() ==  TipoInstrumento.SoftToken
				&& (SuiteAppCRApi.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) ){
			if(SuiteAppCRApi.getSofttokenStatus()) {
				if(ServerCommons.ALLOW_LOG){
					Log.d("APP", "softToken local");
				}
				newToken = proxy.loadOtpFromSofttoken(viewTo.getTokenAMostrar());

				this.finaliceOP(newToken, viewTo, listener);
			}
			else if(!SuiteAppCRApi.getSofttokenStatus() && PropertiesManager.getCurrent().getSofttokenService()) {
				if(ServerCommons.ALLOW_LOG){
					Log.d("APP","softoken compartido");
				}
				//suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST validacionEstatusST = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST();
				//validacionEstatusST.validaEstatusSofttoken(SuiteApp.appContext);
				GetOtpBmovil otp = new GetOtpBmovil(new GetOtp() {
					/**
					 *
					 * @param otp
					 */
					@Override
					public void setOtpRegistro(String otp) {

					}

					/**
					 *
					 * @param otp
					 */
					@Override
					public void setOtpCodigo(String otp) {
						if(ServerCommons.ALLOW_LOG) {
							Log.d("APP", "la otp en callback: " + otp);
						}
						finaliceOP(otp, viewTo, listener);
					}

					/**
					 *
					 * @param otp
					 */
					@Override
					public void setOtpCodigoQR(String otp) {

					}
				}, SuiteApp.appContext);

				otp.generateOtpCodigo();
			}
		}
		else if(viewTo.getTokenAMostrar() != TipoOtpAutenticacion.ninguno ) {
			finaliceOP(viewTo.getAsm(),viewTo, listener);
		}
		else if(viewTo.getTokenAMostrar() == TipoOtpAutenticacion.ninguno){
			finaliceOP(null, viewTo,listener);
		}


	}
	private void finaliceOP(String newToken, final ConfirmacionViewTo viewTo,
							final OnContrataAutoFinishedListener listener){
		if(null != newToken){
			viewTo.setAsm(newToken);
		}

		Log.d("APP", "el newToken 9 " + newToken);
		// operacion
		new Handler().post(new Runnable() {
							   @Override
							   public void run() {
								   //TODO enviar Activity
								   listener.onFinishedConfirmacionOperacion(
										   proxy.doOperation(viewTo));
							   }
						   }
		);
	}

	@Override
	public Constants.Perfil consultaClienteProfile(final OnContrataAutoFinishedListener listener) {
		return proxy.consultaClienteProfile();
	}
	
	
	public void consultarTerminosDeUso(){
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				 //TODO enviar Activity
				proxy.consultarTerminosDeUso();
			}
		}
		);
	}
	
}
