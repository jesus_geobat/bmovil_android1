/**
 * 
 */
package suitebancomer.aplicaciones.resultados.listeners;

import suitebancomer.aplicaciones.resultados.to.ResultadosViewTo;


/**
 * @author lbermejo
 *
 */
public interface OnResultadosFinishedListener {

	void onError();
	void onFinishedListaDatos(final ResultadosViewTo lists);
	void onFinishedValidShowFields(final ResultadosViewTo fields);
	void onFinishedConfirmacionOperacion();
	void onFinishedGetPrepareOptionsMenu(final ResultadosViewTo lists);
	
}
