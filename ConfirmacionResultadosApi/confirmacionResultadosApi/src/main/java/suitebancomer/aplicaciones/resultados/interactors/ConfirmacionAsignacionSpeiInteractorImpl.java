/**
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import android.os.Handler;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.resultados.R;
import suitebancomer.aplicaciones.resultados.listeners.OnConfirmAsingSpeiFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionAsignacionSpeiServiceProxy;
import suitebancomer.aplicaciones.resultados.proxys.IServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionAsignacionSpeiViewTo;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;

/**
 * @author amgonzalez
 *
 */
public class ConfirmacionAsignacionSpeiInteractorImpl 
implements ConfirmacionAsignacionSpeiInteractor {
	
	private final IConfirmacionAsignacionSpeiServiceProxy proxy;
	
	public ConfirmacionAsignacionSpeiInteractorImpl(final IConfirmacionAsignacionSpeiServiceProxy proxy) {
		this.proxy = proxy;
	}

	/**
	 * @return the proxy
	 */
	public IServiceProxy getProxy() {
		return proxy;
	}


	@Override
	public void getListaDatos(final OnConfirmAsingSpeiFinishedListener listener) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedListaDatos((ArrayList<Object>) 
						proxy.getListaDatos());
			}
		}
		);
		
	}

	@Override
	public void getShowFields(final OnConfirmAsingSpeiFinishedListener listener) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedValidShowFields(
						proxy.showFields());
			}
		}
		);
		
	}


	@Override
	public void doConfirmacionOperacion(
			final ConfirmacionAsignacionSpeiViewTo viewTo,
			final OnConfirmAsingSpeiFinishedListener listener) {
		
		int iderrorMessage = 0;
		
		if(viewTo.getTokenAMostrar() != TipoOtpAutenticacion.ninguno) {
			final String fieldName = viewTo.getOtpItem().getLabel();
			if(Tools.isEmptyOrNull(viewTo.getOtp())) {
				iderrorMessage = R.string.bmovil_confirmacion_asignacion_spei_error_missing_otp;
				listener.onErrorOtp(iderrorMessage, fieldName);
                return;
			} else if(Constants.ASM_LENGTH != viewTo.getOtp().length()) {
				iderrorMessage = R.string.bmovil_confirmacion_asignacion_spei_error_length_otp;
				listener.onErrorOtp(iderrorMessage, fieldName);
                return;
			}
		}
		
		if(viewTo.getCvvItem().getVisible()) {
			if(Tools.isEmptyOrNull(viewTo.getCvv()))
				iderrorMessage = R.string.bmovil_confirmacion_asignacion_spei_error_missing_cvv;
			else if(Constants.CVV_LENGTH != viewTo.getCvv().length())
				iderrorMessage = R.string.bmovil_confirmacion_asignacion_spei_error_length_cvv;
		}
		
		if(viewTo.getPwdItem().getVisible()) {
			if(Tools.isEmptyOrNull(viewTo.getContrasena()))
				iderrorMessage = R.string.bmovil_confirmacion_asignacion_spei_error_missing_password;
			else if(Constants.PASSWORD_LENGTH != viewTo.getContrasena().length())
				iderrorMessage = R.string.bmovil_confirmacion_asignacion_spei_error_length_password;
		}
		
		if(viewTo.getNipItem().getVisible()) {
			if(Tools.isEmptyOrNull(viewTo.getNip()))
				iderrorMessage = R.string.bmovil_confirmacion_asignacion_spei_error_missing_nip;
			else if(Constants.NIP_LENGTH != viewTo.getNip().length())
				iderrorMessage = R.string.bmovil_confirmacion_asignacion_spei_error_length_nip;	
		}
		
		if(viewTo.getCardItem().getVisible()) {
			if(Tools.isEmptyOrNull(viewTo.getTarjeta()))
				iderrorMessage = R.string.bmovil_confirmacion_asignacion_spei_error_missing_card;
			else if(Constants.CARD_LAST_DIGITS_LENGTH != viewTo.getTarjeta().length())
				iderrorMessage = R.string.bmovil_confirmacion_asignacion_spei_error_length_card;
		}
		
		if(!viewTo.getOkTerminos())
			iderrorMessage = R.string.bmovil_confirmacion_asignacion_spei_error_terms;
		
		// If the message value is greater than 0 then an error message was set and must be showed. 
		if(iderrorMessage != 0) {
			
			listener.onErrorValue(iderrorMessage);
			//ownerController.showInformationAlert(errorMessage);
			
		} else {
			
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					listener.onFinishedConfirmacionOperacion(
							proxy.doOperation(viewTo));
				}
			}
			);
			
			//((MantenimientoSpeiMovilDelegate)operationDelegate).
			//	realizeOperation(ownerController, pwd, nip, otp, cvv, card);
		}
	}


	@Override
	public Integer consultaOperationsIdTextoEncabezado() {
		return proxy.consultaOperationsIdTextoEncabezado();
	}

	@Override
	public void requestSpeiTermsAndConditions(final OnConfirmAsingSpeiFinishedListener listener) {
		
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedTerminosCondiciones(
						proxy.requestSpeiTermsAndConditions());
			}
		}
		);
	}
	
	
}
