/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;



import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.interactors.RegistroOperacionInteractor;
import suitebancomer.aplicaciones.resultados.interactors.RegistroOperacionInteractorImpl;
import suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.views.RegistroOperacionView;
import android.content.Intent;

/**
 * @author amgonzalez
 *
 */
public class RegistroOperacionPresenterImpl implements RegistroOperacionPresenter, OnConfirmacionFinishedListener {
	
	private final RegistroOperacionView cview;
	private final RegistroOperacionInteractor interactor;

	public RegistroOperacionPresenterImpl(final RegistroOperacionView view, final Intent intent){
		this.cview = view;
		this.interactor = new RegistroOperacionInteractorImpl(
				getServiceProxy(intent)); 
	}
	
	
	@Override 
	public void onResume() {
        //mainView.showProgress();
        //findItemsInteractor.findItems(this);
    }
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.presenters.ConfirmacionPresenter#show()
	 */
	@Override
	public void show() {
		// TODO Auto-generated method stub
		/*
		 *  mainView.showProgress();
        findItemsInteractor.findItems(this);
		 */
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener#onError()
	 */
	@Override
	public void onError() {
		// TODO Auto-generated method stub
		//loginView.setUsernameError();
		// en el view username.setError(getString(R.string.username_error));
	}

	/*
	@Override public void validateCredentials(String username, String password) {
        loginView.showProgress();
        loginInteractor.login(username, password, this);
    }*/
	
	private IServiceProxy getServiceProxy(final Intent intent){
		final IServiceProxy p = (IServiceProxy)
				SuiteAppCRApi.getInstance().getProxy();
		return p;
	}

	public void limpiarCampos(){
		//cview.limpiarCampos();
	}


	@Override
	public void onFinishedListaDatos(final ArrayList<Object> list) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onFinishedValidShowFields(final ConfirmacionViewTo fields) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onFinishedConfirmacionOperacion(final Boolean resp) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onErrorContrasena(final boolean lenght) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onErrorNip(final boolean lenght) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onErrorCvv(final boolean lenght) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onErrorAsm(final int msg, final boolean lenght) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onErrorTarjeta(final boolean lenght) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void getListaDatos() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void getShowFields() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void confirmarClick(final ConfirmacionViewTo viewVo) {
		// TODO Auto-generated method stub
		
	}

}
