/**, 
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import suitebancomer.aplicaciones.resultados.listeners.OnResultadosFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IResultadosServiceProxy;

/**
 * @author lbermejo
 *
 */
public interface ResultadosInteractor {
	
	IResultadosServiceProxy getProxy();
	void getListaDatos(final OnResultadosFinishedListener listener);
	void getShowFields(final OnResultadosFinishedListener listener);
	void getOnPrepareOptionsMenu(final OnResultadosFinishedListener listener);
	void botonMenuClick();
	
	void setParamStateParentManager();
	boolean optionsItemSelected(final int idMenu);
}
