package suitebancomer.aplicaciones.resultados.views;

/**
 * 
 * @author lbermejo
 *
 */
public interface IBaseView {
	
	void onErrorMsg();	
	void showMessage(final String message);
	
}
