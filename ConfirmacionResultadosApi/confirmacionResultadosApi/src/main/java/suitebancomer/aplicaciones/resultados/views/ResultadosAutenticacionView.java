package suitebancomer.aplicaciones.resultados.views;

import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.to.ResultadosViewTo;

/**
 * @author amgonzalez
 *
 */
public interface ResultadosAutenticacionView extends IBaseView{

	void onSuccess();
	void configuraPantalla();
	ResultadosViewTo getResultadosViewTo();
	void setResultadosViewTo(final ResultadosViewTo viewTo);
	void setListaDatos(final ArrayList<Object> datos);
	//void setListaClave(ArrayList<Object> datos, Boolean isConsultaInterbancario);
	void setOpcionesMenu(final ResultadosViewTo to);
}
