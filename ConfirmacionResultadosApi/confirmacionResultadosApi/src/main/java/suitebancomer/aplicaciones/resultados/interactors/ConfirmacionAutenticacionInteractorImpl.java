/**
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtp;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtpBmovil;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.bancomer.base.SuiteApp;

/**
 * @author lbermejo
 *
 */
public class ConfirmacionAutenticacionInteractorImpl implements ConfirmacionAutenticacionInteractor {
	
	private final IConfirmacionAutenticacionServiceProxy proxy;
	
	public ConfirmacionAutenticacionInteractorImpl(final IConfirmacionAutenticacionServiceProxy proxy) {
		this.proxy = proxy;
	}
	
	/**
	 * @return the proxy
	 */
	public IConfirmacionAutenticacionServiceProxy getProxy() {
		return proxy;
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.interactors.ConfirmacionInteractor#getDatos(suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener)
	 */
	@Override
	public void getListaDatos(final OnConfirmacionFinishedListener listener) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedListaDatos((ArrayList<Object>) 
						proxy.getListaDatos());
			}
		}
		);
	}
	
	@Override
	public void getShowFields(final OnConfirmacionFinishedListener listener) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedValidShowFields(
						proxy.showFields());
				/* en el proxy 
				//mostrarContrasena(confirmacionDelegate.consultaDebePedirContrasena());
				//mostrarNIP(confirmacionDelegate.consultaDebePedirNIP());
				//mostrarASM(confirmacionDelegate.consultaInstrumentoSeguridad());
				//mostrarCVV(confirmacionDelegate.consultaDebePedirCVV());
				//mostrarCampoTarjeta(confirmacionDelegate.mostrarCampoTarjeta());
				 */
			}
		}
		);
	}
	
	@Override
	public Integer consultaOperationsIdTextoEncabezado() {
		return proxy.consultaOperationsIdTextoEncabezado();
	}
	
	//TODO
	public void doConfirmacionOperacion(final ConfirmacionViewTo viewTo,
							final OnConfirmacionFinishedListener listener){

		if( viewTo.getShowContrasena() ){
			if (TextUtils.isEmpty(viewTo.getContrasena() )){
                listener.onErrorContrasena(false);
                return;
            }else if(viewTo.getContrasena().trim().length() 
            		!= Constants.PASSWORD_LENGTH){
            	listener.onErrorContrasena(true);
            	return;
            }
		}
		
		if( viewTo.getShowTarjeta() ){
			if (TextUtils.isEmpty(viewTo.getTarjeta() )){
                listener.onErrorTarjeta(false);
                return;
            }else if(viewTo.getTarjeta().trim().length() 
            		!= ApiConstants.TARJETA_LENGTH  ){
            	listener.onErrorTarjeta(true);
            	return;
            }
		}
		
		if( viewTo.getShowNip() ){
			if (TextUtils.isEmpty(viewTo.getNip() )){
                listener.onErrorNip(false);
                return;
            }else if(viewTo.getNip().trim().length() 
            		!= Constants.NIP_LENGTH ){
            	listener.onErrorNip(true);
            	return;
            }
		}
		
		if( viewTo.getTokenAMostrar()
				!= Constants.TipoOtpAutenticacion.ninguno){
			
			if (TextUtils.isEmpty(viewTo.getAsm() )){
				final int msg = proxy.getMessageAsmError( viewTo.getInstrumentoSeguridad());
				/*
				 * en el proxy 
				 * switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				 */
                listener.onErrorAsm(msg, false);
                return;
            }else if(viewTo.getAsm().trim().length() 
            		!= Constants.ASM_LENGTH ){
				final int msg = proxy.getMessageAsmError(viewTo.getInstrumentoSeguridad());
            	listener.onErrorAsm(msg, true);
            	return;
            }
		}
		
		if( viewTo.getShowCvv() ){
			if (TextUtils.isEmpty(viewTo.getCvv() )){
                listener.onErrorCvv(false);
                return;
            }else if(viewTo.getCvv().trim().length() 
            		!= Constants.CVV_LENGTH ){
            	listener.onErrorCvv(true);
            	return;
            }
		}

		String newToken = null;
		suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST validacionEstatusST = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST();
		validacionEstatusST.validaEstatusSofttoken(SuiteApp.appContext);
		if(ServerCommons.ALLOW_LOG){
			Log.d("APP", "el estatus del token es: " + PropertiesManager.getCurrent().getSofttokenService());
		}
		if ( viewTo.getTokenAMostrar() != TipoOtpAutenticacion.ninguno
				&& viewTo.getInstrumentoSeguridad() ==  TipoInstrumento.SoftToken
				&& (SuiteAppCRApi.getSofttokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) ){
			if(SuiteAppCRApi.getSofttokenStatus()) {
				if(ServerCommons.ALLOW_LOG){
					Log.d("APP","softToken local");

				}
				newToken = proxy.loadOtpFromSofttoken(viewTo.getTokenAMostrar());

				this.getOtp(newToken, viewTo, listener);
			}
			else if(!SuiteAppCRApi.getSofttokenStatus() && PropertiesManager.getCurrent().getSofttokenService()) {
				if(ServerCommons.ALLOW_LOG){
					Log.d("APP","softoken compartido");
				}
				//suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST validacionEstatusST = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ValidacionEstatusST();
				//validacionEstatusST.validaEstatusSofttoken(SuiteApp.appContext);
				GetOtpBmovil otp = new GetOtpBmovil(new GetOtp() {
					/**
					 *
					 * @param otp
					 */
					@Override
					public void setOtpRegistro(String otp) {

					}

					/**
					 *
					 * @param otp
					 */
					@Override
					public void setOtpCodigo(String otp) {
						if(ServerCommons.ALLOW_LOG) {
							Log.d("APP", "la otp en callback: " + otp);
						}
						getOtp(otp, viewTo, listener);
					}

					/**
					 *
					 * @param otp
					 */
					@Override
					public void setOtpCodigoQR(String otp) {

					}
				}, SuiteApp.appContext);

				otp.generateOtpCodigo();
			}
		}
		else if(viewTo.getTokenAMostrar() != TipoOtpAutenticacion.ninguno && (viewTo.getInstrumentoSeguridad() == TipoInstrumento.DP270 || viewTo.getInstrumentoSeguridad() == TipoInstrumento.OCRA)) {
			getOtp(viewTo.getAsm(), viewTo, listener);
		}
		else if(viewTo.getTokenAMostrar() == TipoOtpAutenticacion.ninguno){
			getOtp(null, viewTo,listener);
		}

	}

	private void getOtp(String otp, final ConfirmacionViewTo viewTo, final OnConfirmacionFinishedListener listener) {
		if(null != otp){
			viewTo.setAsm(otp);
		}

		Log.d("APP", "el newToken 7 " + otp);
		// operacion
		new Handler().post(new Runnable() {
							   @Override
							   public void run() {
								   //TODO enviar Activity
								   listener.onFinishedConfirmacionOperacion(
										   proxy.doOperation(viewTo));
							   }
						   }
		);
		
	}

}
