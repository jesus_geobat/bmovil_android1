/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;

import java.util.ArrayList;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.interactors.ConfirmacionRegistroInteractor;
import suitebancomer.aplicaciones.resultados.interactors.ConfirmacionRegistroInteractorImpl;
import suitebancomer.aplicaciones.resultados.listeners.OnConfirmRegistroFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionRegistroServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.views.ConfirmacionRegistroView;
import android.content.Intent;

/**
 * @author amgonzalez
 *
 */
public class ConfirmacionRegistroPresenterImpl implements ConfirmacionRegistroPresenter, OnConfirmRegistroFinishedListener {
	
	private final ConfirmacionRegistroView cview;
	private final ConfirmacionRegistroInteractor interactor;

	public ConfirmacionRegistroPresenterImpl(final ConfirmacionRegistroView view, final Intent intent){
		this.cview = view;
		this.interactor = new ConfirmacionRegistroInteractorImpl(
				getServiceProxy(intent)); 
	}
	
	private IConfirmacionRegistroServiceProxy getServiceProxy(final Intent intent){
		final IConfirmacionRegistroServiceProxy p = (IConfirmacionRegistroServiceProxy)
				SuiteAppCRApi.getInstance().getProxy();
		return p;
	}

	@Override 
	public void onResume() {
        //mainView.showProgress();
       
    }
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.presenters.ConfirmacionPresenter#show()
	 */
	@Override
	public void show() {
		/*
		 *  mainView.showProgress();
		 */
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener#onError()
	 */
	@Override
	public void onError() {
		//View.setUsernameError();
		// en el view username.setError(getString(R.string.username_error));
	}
	
	@Override
	public void limpiarCampos(){
		cview.reset();
	}

	@Override
	public void getListaDatos() {
		interactor.getListaDatos(this);
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener#onFinished()
	 */
	@Override
	public void onFinishedListaDatos( final ArrayList<Object> list ) {
		cview.setListaDatos(list);
	}

	@Override
	public void getShowFields() {
		interactor.getShowFields(this);
	}
	
	@Override
	public void onFinishedValidShowFields(final ConfirmacionViewTo fields) {
		cview.setViewTo(fields);
		cview.mostrarContrasena(fields.getShowContrasena());
		cview.mostrarNIP(fields.getShowNip());
		cview.mostrarASM(fields);
		cview.mostrarCVV(fields.getShowCvv());
	}

	@Override
	public void getDatosRegistroExitoso() {
		interactor.getShowFields(this);
	}	
	
	@Override
	public void onFinishedDatosRegistroOk(final ArrayList<Object> list) {
		cview.mostrarDatosRegistro(list);
	}
	
	
	@Override
	public void confirmarClick(final ConfirmacionViewTo viewTo) {
		
		interactor.doConfirmacionOperacion(viewTo, this);
	}
	
	@Override
	public void onFinishedConfirmacionOperacion(final Boolean resp){
		
		if ( resp.booleanValue() ){
			final int idTextoEncabezado = interactor.consultaOperationsIdTextoEncabezado();
			cview.onSuccess(idTextoEncabezado);
		}
	}

	@Override
	public void onErrorContrasena(final boolean lenght) {
		cview.showMensajePideContrasena(lenght);
	}

	@Override
	public void onErrorNip(final boolean lenght) {
		cview.showMensajePideNip(lenght);
	}

	@Override
	public void onErrorCvv(final boolean lenght) {
		cview.showMensajePideCVV(lenght);
	}

	@Override
	public void onErrorAsm(final int msg, final boolean lenght) {
		cview.showMensajePideToken(msg, lenght);
	}

	@Override
	public void onErrorTarjeta(final boolean lenght) {
	}

}
