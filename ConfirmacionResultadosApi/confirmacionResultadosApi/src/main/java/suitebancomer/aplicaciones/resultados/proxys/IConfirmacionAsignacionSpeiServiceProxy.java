/**
 * 
 */
package suitebancomer.aplicaciones.resultados.proxys;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionAsignacionSpeiViewTo;

/**
 * @author lbermejo
 *
 */
public interface IConfirmacionAsignacionSpeiServiceProxy extends IServiceProxy {
	
	ArrayList<Object> getListaDatos();
	ConfirmacionAsignacionSpeiViewTo showFields();
	Integer getMessageAsmError(	final Constants.TipoInstrumento tipoInstrumento);
	String loadOtpFromSofttoken(  final Constants.TipoOtpAutenticacion tipoOtpAutenticacion);
	Integer consultaOperationsIdTextoEncabezado();
	Boolean requestSpeiTermsAndConditions();

}
