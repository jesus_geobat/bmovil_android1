package suitebancomer.aplicaciones.keystore;

import android.content.Context;
import android.os.Environment;

import com.bancomer.keygenerator.ToolsKey;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;

import javax.crypto.spec.SecretKeySpec;

public class KeyStoreManager {
	static {
		Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);
	}
	/**
	 * Password por defecto del keystore.
	 */
	private static final String DEFAULT_KEY_STORE_PASSWORD = "passssword";
    private static final String TAG = KeyStoreManager.class.getSimpleName();

	private final String mKeyStoreName;
    private final File keyStoreFile;

    private String mKeyStorePassword = DEFAULT_KEY_STORE_PASSWORD;

	private KeyStore mKeystore = null;
	private Context context;

    private static final String KEYSTORE_PATH = Environment.getExternalStorageDirectory() + "/.bancomer";
    private static final String OLD_KEYSTORE_PATH = "//data/data/com.bancomer.mbanking";
    private static final String KEYSTORE_NAME = "keyChain.bks";
	
	/**
	 * 
	 * @param keyStoreFilePath directorio del keyStore
	 * @param keyStoreFileName nombre del keyStore
     * @param context contexto de la implementacion
	 * @throws KeyStoreException
	 * @throws IOException 
	 */
	protected KeyStoreManager(final File keyStoreFilePath, final String keyStoreFileName, final Context context) throws KeyStoreException, IOException {
		this.setContext(context);

		this.mKeyStoreName = keyStoreFileName;
		this.mKeyStorePassword = ToolsKey.generateKey(context);
		
		// Creamos el fichero
		keyStoreFile = new File(keyStoreFilePath, mKeyStoreName);

		ToolsKeyChain.writeLogd(TAG, "Clave para keystore " + this.mKeyStorePassword, SecurityConstants.allowLog);
		ToolsKeyChain.writeLogd(TAG, "Iniciando keystore manager con fichero: " + keyStoreFile.getAbsolutePath(), SecurityConstants.allowLog);
		
		this.openOrCreate();
	}
	
	protected Context getContext() {
		return this.context;
	}

	protected void setContext(final Context context) {
		this.context = context;
	}

	/**
	 * Crea el keystore
	 * @throws KeyStoreException
	 */
	private void openOrCreate() throws KeyStoreException {
		try {
			// Creamos la instancia del keystore
			this.mKeystore = KeyStore.getInstance(KeyStore.getDefaultType());
			
			// Comprobamos si existe el fichero.
			if (!this.keyStoreFile.exists() || 0 == keyStoreFile.length()) {
				
				// No existe, iniciamos el keystore
				this.mKeystore.load(null, null);

				Runtime.getRuntime().exec("chmod 777 " + keyStoreFile.getAbsolutePath());
				ToolsKeyChain.writeLogd(TAG, "Keystore creado OK", SecurityConstants.allowLog);
			}
            else {
                try {
                    // Existe, intenta cargar  el keystore con el contenido del fichero.
                    ToolsKeyChain.writeLogd(TAG, "Accediendo a keystore " + this.mKeyStorePassword, SecurityConstants.allowLog);
                    final FileInputStream fist = new FileInputStream(keyStoreFile);
                    this.mKeystore.load(fist, this.mKeyStorePassword.toCharArray());
                    ToolsKeyChain.writeLogd(TAG, "Keystore inicializado OK", SecurityConstants.allowLog);
                    fist.close();
                }
                catch (Exception e){//error al cargar el keychain
                    try {
                        ToolsKeyChain.writeLogd(TAG, "Error al cargar keystore nuevo intenta con el antiguo", SecurityConstants.allowLog);
                        //si el keystore no se puede cargar intenta cargar el antiguo para saver si no hay problema con el atiguo
                        copyOldKeyStore();
                        final FileInputStream  fist = new FileInputStream(keyStoreFile);
                        this.mKeystore.load(fist, this.mKeyStorePassword.toCharArray());
                        ToolsKeyChain.writeLogd(TAG, "Keystore inicializado OK2", SecurityConstants.allowLog);
                    }
                    catch (Exception e1){
                        //si el keystore no se puede cargar ni el nuevo ni el viejo se crea uno nuevo pero antes se elimina el archivo
                        final File oldKeyStore = new File(OLD_KEYSTORE_PATH, KEYSTORE_NAME);
                        keyStoreFile.delete();
                        oldKeyStore.delete();
                        // No existe, iniciamos el keystore
                        this.mKeystore.load(null, null);
                        //permisos
                        Runtime.getRuntime().exec("chmod 777 " + keyStoreFile.getAbsolutePath());
                        ToolsKeyChain.writeLogd(TAG, "Keystore creado OK2", SecurityConstants.allowLog);
                    }
                }
			}
		}
        catch (NoSuchAlgorithmException e) {
            ToolsKeyChain.writeLogd(TAG, "NoSuchAlgorithmException => openOrCreate: ", SecurityConstants.allowLog);
			throw new KeyStoreException(e);
		}
        catch (CertificateException e) {
            ToolsKeyChain.writeLogd(TAG, "CertificateException => openOrCreate: ", SecurityConstants.allowLog);
			throw new KeyStoreException(e);
		}
        catch (IOException e) {
            ToolsKeyChain.writeLogd(TAG, "IOException => openOrCreate: ", SecurityConstants.allowLog);
			throw new KeyStoreException(e);
		}
        catch (KeyStoreException e) {
            ToolsKeyChain.writeLogd(TAG, "KeyStoreException => openOrCreate: ", SecurityConstants.allowLog);
			throw new KeyStoreException(e);
		}
        catch (Exception e) {
            ToolsKeyChain.writeLogd(TAG, "Exception => openOrCreate: ", SecurityConstants.allowLog);
            throw new KeyStoreException(e);
        }
	}
	
	/**
	 * Almacena en el keystore una nueva entrada, informando si se ha relizado correctamente o no la escritura. Si se produce
	 * un error lanzara la exception KeyManagerStoreException
	 * @param alias clave
	 * @param secretKey valor
	 * @return true si ha escrito correctamente, false en caso contrario
	 */
	protected synchronized boolean setEntry(final String alias, final String secretKey) throws KeyManagerStoreException {

	    boolean keyStoreEntryWritten = false;

	    if (mKeystore != null && secretKey != null) {
	        // store something in the key store
	        final SecretKeySpec sks = new SecretKeySpec(secretKey.getBytes(), "MD5");
	        final KeyStore.SecretKeyEntry ske = new KeyStore.SecretKeyEntry(sks);
	        final KeyStore.ProtectionParameter pp = new KeyStore.PasswordProtection(null);

	        try {
				ToolsKeyChain.writeLogi("Introducir dato en keystore", alias + " " + secretKey, SecurityConstants.allowLog);
	        	this.mKeystore.setEntry(alias, ske, pp);

	            // Salvamos el keystore con la nueva clave
	            final boolean success = save();

	            if (success) {
	                keyStoreEntryWritten = true;
	            }
	        }
            catch (KeyStoreException ex) {
				ToolsKeyChain.writeLoge(TAG,"Error KeyStoreException: setEntry " + mKeyStoreName, SecurityConstants.allowLog);
	            throw new KeyManagerStoreException(ex);
	        }
            catch (Exception ex) {
                ToolsKeyChain.writeLoge(TAG,"Error Exception: setEntry " + mKeyStoreName, SecurityConstants.allowLog);
                throw new KeyManagerStoreException(ex);
            }
	    }
	    return keyStoreEntryWritten;
	}
	
	/**
	 * Retorna el valor de la clave almacenada en el keystore.
	 * @param alias key Alias
	 * @return secretStr
	 */
	protected synchronized String getEntry(final String alias) throws KeyManagerStoreException {
	    String secretStr = null;
	    byte[] secret;

	    if (this.mKeystore != null) {
	        try {
	            if (!this.mKeystore.containsAlias(alias)) {
					ToolsKeyChain.writeLoge(TAG, "Keystore " + this.mKeyStoreName + " no contiene clave " + alias, SecurityConstants.allowLog);
	                return null;
	            }

		        // get my entry from the key store
		        final KeyStore.ProtectionParameter pp = new KeyStore.PasswordProtection(null);
				KeyStore.SecretKeyEntry ske = (KeyStore.SecretKeyEntry) this.mKeystore.getEntry(alias, pp);

                if (ske != null) {
					final SecretKeySpec sks = (SecretKeySpec) ske.getSecretKey();
		            secret = sks.getEncoded();

		            if (secret != null) {
		                secretStr = new String(secret);
						//secretStr = new String();
		            }
					else {
						ToolsKeyChain.writeLoge(TAG, "La lectura ha sido vacia" + alias, SecurityConstants.allowLog);
					}
		        }
				else {
					ToolsKeyChain.writeLoge(TAG, "Fallo al leer del keystore la clave " + alias, SecurityConstants.allowLog);
		        }
	        }
			catch (Exception ex) {
				ToolsKeyChain.writeLoge(TAG, "Error Exception: Al leer del keystore la clave " + alias, SecurityConstants.allowLog);
	        	throw new KeyManagerStoreException(ex);
	        }
	    }
	    
	    return secretStr;
	}
	
	/**
	 * Nos permitira almacenar el keystore en un fichero.
	 * @return keyStoreSaved
	 */
	private synchronized boolean save() {
	    FileOutputStream fos = null;
	    boolean keyStoreSaved = true;

	    try {
	    	fos = new FileOutputStream(keyStoreFile);
	        this.mKeystore.store(fos, this.mKeyStorePassword.toCharArray());
	        
	    }
        catch (Exception ex) {
	        keyStoreSaved = false;
			ToolsKeyChain.writeLoge(TAG, "Error Exception: Ha fallado al salvar el keystore " + this.mKeyStoreName, SecurityConstants.allowLog);

	    }
        finally {
	        if (fos != null) {
	            try {
	                fos.close();
	            }
                catch (IOException ex) {
	                keyStoreSaved = false;
					ToolsKeyChain.writeLoge(TAG, "Error finally-IOException: Fallo al cerrar FileOutputStream", SecurityConstants.allowLog);
	            }
	        }
	    }

	    return keyStoreSaved;
	}

    /**
     * copia el antiguo keystore a la ubicacion nueva
     * (utilizado cuando no se puede cargar el keystore de la ubicacion nueva)
     */
    private static  void copyOldKeyStore() {
        final File oldKeyStore = new File(OLD_KEYSTORE_PATH, KEYSTORE_NAME);
        final File newKeyStore = new File(KEYSTORE_PATH, KEYSTORE_NAME);
        final File carpetakey = new File(KEYSTORE_PATH);
        if(!carpetakey.exists()) {
            carpetakey.mkdir();
        }
        //solo se copia a la nueva ubicacion si el antiguo archivo existe y el nuevo archivo no
        if (oldKeyStore.exists() ) {
            FileChannel inChannel = null;
            FileChannel outChannel = null;
            try {
                inChannel = new FileInputStream(oldKeyStore).getChannel();
                outChannel = new FileOutputStream(newKeyStore).getChannel();
                inChannel.transferTo(0, inChannel.size(), outChannel);
                //oldKeyStore.delete();
            }
            catch (FileNotFoundException e) {
                ToolsKeyChain.writeLoge(TAG, "Error: checkKeyStore FileNotFoundException => " + e.getMessage(), Boolean.TRUE);
            }
            catch (IOException e) {
                ToolsKeyChain.writeLoge(TAG, "Error: checkKeyStore IOException => " + e.getMessage(), Boolean.TRUE);
            }
            catch (Exception e) {
                ToolsKeyChain.writeLoge(TAG, "Error: checkKeyStore Exception => " + e.getMessage(), Boolean.TRUE);
            }
            finally {
                try {
                    if (inChannel != null) {
                        inChannel.close();
                    }
                    if (outChannel != null) {
                        outChannel.close();
                    }
                }
                catch (IOException e) {
                    ToolsKeyChain.writeLoge(TAG, "Error: checkKeyStore finally-IOException => " + e.getMessage(), Boolean.TRUE);
                }
                catch (Exception e) {
                    ToolsKeyChain.writeLoge(TAG, "Error: checkKeyStore finally-Exception => " + e.getMessage(), Boolean.TRUE);
                }
            }
        }
    }
}
