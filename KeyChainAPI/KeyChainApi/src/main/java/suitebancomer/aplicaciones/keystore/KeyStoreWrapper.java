/**
 * 
 */
package suitebancomer.aplicaciones.keystore;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.security.KeyStoreException;

/**
 * @author IDS Comercial
 *
 */
public final class KeyStoreWrapper {

    private static final String TAG = KeyStoreWrapper.class.getSimpleName();
	
	private static final String USRNAME_KEY = "username";
	private static final String SEED_KEY = "seed";
    private static final String PASSWD_KEY = "keychain.passwd";
    private static final String OTP_KEY = "keychain.otp";
	private static final String CELLPHONE_KEY = "keychain.nphone";
	private static final String KEYSTORE_NAME = "keyChain.bks";
	private static final String DEL_KEY_VALUE = " ";
	private static final String EMPTY_STRING = "";
	private static final String KEYSTORE_PATH = Environment.getExternalStorageDirectory() + "/.bancomer";
	//private static final String OLD_KEYSTORE_PATH = "//data/data/com.bancomer.mbanking";//ruta vieja, se duplica para que ya no
	// exista el keyChain en el paquete local de bmovil.
	private static final String OLD_KEYSTORE_PATH = Environment.getExternalStorageDirectory() + "/.bancomer";

	private static Context context = null;
	private static KeyStoreManager keyStoreManager = null;
	
	/**
	 * Object for singleton synchronization
	 */

	private static final KeyStoreWrapper INSTANCE = new KeyStoreWrapper();
	private KeyStoreWrapper(){}
	
	public static KeyStoreWrapper getInstance(final Context contexto) {
		context = contexto;
        checkKeyStore();
		instanceKeyStoreManager();
		return INSTANCE;
	}

	private static void instanceKeyStoreManager() {
			synchronized (KeyStoreWrapper.class) {
				try {
						final File f = new File(KEYSTORE_PATH);
						keyStoreManager = new KeyStoreManager(f, KEYSTORE_NAME, context);
				}
                catch (KeyStoreException e) {
					if(SecurityConstants.allowLog)
						Log.e(TAG, "Error instanceKeyStoreManager: KeyStoreException: " + e.toString());
				}
                catch (IOException e) {
					if(SecurityConstants.allowLog)
						Log.e(TAG, "Error instanceKeyStoreManager: IOException" + e.toString());
				}
				catch (Exception e) {
					if(SecurityConstants.allowLog)
						Log.e(TAG, "Error instanceKeyStoreManager: Exception" + e.toString());
				}
			}
	}
	
	public void setUserName(final String value) {
		storeValueForKey(USRNAME_KEY, value);
	}
	
	public void setSeed(final String value) {
		storeValueForKey(SEED_KEY, value);
	}

    public void setPasswd(final String value) {
        storeValueForKey(PASSWD_KEY, value);
    }

	public void setOtp(final String value) {
		storeValueForKey(OTP_KEY, value);
	}
	
	public void setPhone(final String value) {
		storeValueForKey(CELLPHONE_KEY, value);
	}
	
	public void storeValueForKey(final String key, String value) {
		try {
			//si el valor es nulo o vacio se cambia a " "
			if(value == null || EMPTY_STRING.equals(value)) {
				value = DEL_KEY_VALUE;
			}

			//si la clave es nula o vacia no se guarda
			if(key == null || EMPTY_STRING.equals(key)) {
				return;
			}

			if(SecurityConstants.allowLog)
				Log.e(TAG, "storeValueForKey: Es nulo keyStoreManager? (" + key + ") " + (keyStoreManager == null));

			keyStoreManager.setEntry(key, value);
			//synckKeyStore();
		}
		catch (KeyManagerStoreException e) {
			if(SecurityConstants.allowLog)
				Log.e(TAG, "Error: storeValueForKey KeyManagerStoreException => " + e.toString());
		}
		catch (Exception e) {
			if(SecurityConstants.allowLog)
				Log.e(TAG, "Error: storeValueForKey Exception => " + e.toString());
		}
	}
	
	public String getUserName(){
		return fetchValueForKey(USRNAME_KEY);
	}
	
	public String getSeed(){
		return fetchValueForKey(SEED_KEY);
	}

	public String getPasswd(){
		return fetchValueForKey(PASSWD_KEY);
	}

	public String getOtp() {
		return fetchValueForKey(OTP_KEY);
	}
	
	public String getPhone() {
		return fetchValueForKey(CELLPHONE_KEY);
	}
	
	public String fetchValueForKey(final String key) {
		String value = null;

		try {
			if(SecurityConstants.allowLog)
				Log.e(TAG, "fetchValueForKey: Es nulo keyStoreManager? (" + key + ") " + (keyStoreManager == null));

			value = keyStoreManager.getEntry(key);
		}
		catch (KeyManagerStoreException e) {
			if(SecurityConstants.allowLog)
				Log.e(TAG, "Error: fetchValueForKey KeyManagerStoreException => " + e.toString());
		}
		catch (Exception e) {
			if(SecurityConstants.allowLog)
				Log.e(TAG, "Error: fetchValueForKey Exception => " + e.toString());
		}
		
		return value != null ? value : "";
	}

    private static void checkKeyStore() {
        final File oldKeyStore = new File(OLD_KEYSTORE_PATH, KEYSTORE_NAME);
		final File newKeyStore = new File(KEYSTORE_PATH, KEYSTORE_NAME);
		final File carpetakey = new File(KEYSTORE_PATH);

		if(!carpetakey.exists()) {
            carpetakey.mkdir();
        }

		//solo se copia a la nueva ubicacion si el antiguo archivo existe y el nuevo archivo no
        if (oldKeyStore.exists() && !newKeyStore.exists()) {
            FileChannel inChannel = null;
            FileChannel outChannel = null;

            try {
                inChannel = new FileInputStream(oldKeyStore).getChannel();
                outChannel = new FileOutputStream(newKeyStore).getChannel();
                inChannel.transferTo(0, inChannel.size(), outChannel);
            }
			catch (FileNotFoundException e) {
                ToolsKeyChain.writeLoge(TAG, "Error: checkKeyStore FileNotFoundException => " + e.getMessage(), Boolean.TRUE);
            }
			catch (IOException e) {
                ToolsKeyChain.writeLoge(TAG, "Error: checkKeyStore IOException => " + e.getMessage(), Boolean.TRUE);
            }
			catch (Exception e) {
				ToolsKeyChain.writeLoge(TAG, "Error: checkKeyStore Exception => " + e.getMessage(), Boolean.TRUE);
			}
			finally {
                try {
                    if (inChannel != null) {
                        inChannel.close();
                    }
                    if (outChannel != null) {
                        outChannel.close();
                    }
                }
				catch (IOException e) {
                    ToolsKeyChain.writeLoge(TAG, "Error: checkKeyStore finally-IOException => " + e.getMessage(), Boolean.TRUE);
                }
				catch (Exception e) {
					ToolsKeyChain.writeLoge(TAG, "Error: checkKeyStore finally-Exception => " + e.getMessage(), Boolean.TRUE);
				}
            }
        }
        else if(newKeyStore.exists()) {
			/*
				si el viejo key ya no existe en la antigua ubiacion se restaura para que
				las apis no actualizadas encuentren el archivo
			*/
			//synckKeyStore();
		}
    }

	/**
	 * Metodo que mantendra actualizado el keychain de la antigua ubicacion.
	 */
	private static void synckKeyStore() {
        final File oldKeyStore = new File(OLD_KEYSTORE_PATH, KEYSTORE_NAME);
        final File newKeyStore = new File(KEYSTORE_PATH, KEYSTORE_NAME);

        FileChannel inChannel = null;
        FileChannel outChannel = null;

        try {
            inChannel = new FileInputStream(newKeyStore).getChannel();
            outChannel = new FileOutputStream(oldKeyStore).getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
            Runtime.getRuntime().exec("chmod 777 /data/data/com.bancomer.mbanking/keyChain.bks");
        }
        catch (FileNotFoundException e) {
            ToolsKeyChain.writeLoge(TAG, "Error: synckKeyStore FileNotFoundException => " + e.getMessage(), Boolean.TRUE);
        }
        catch (IOException e) {
            ToolsKeyChain.writeLoge(TAG, "Error: synckKeyStore IOException => " + e.getMessage(), Boolean.TRUE);
        }
        catch (Exception e) {
            ToolsKeyChain.writeLoge(TAG, "Error: synckKeyStore Exception => " + e.getMessage(), Boolean.TRUE);
        }
        finally {
            try {
                if (inChannel != null) {
                    inChannel.close();
                }
                if (outChannel != null) {
                    outChannel.close();
                }
            }
            catch (IOException e) {
                ToolsKeyChain.writeLoge(TAG, "Error: synckKeyStore finally-IOException => " + e.getMessage(), Boolean.TRUE);
            }
            catch (Exception e) {
                ToolsKeyChain.writeLoge(TAG, "Error: synckKeyStore finally-Exception => " + e.getMessage(), Boolean.TRUE);
            }
        }
    }
}
