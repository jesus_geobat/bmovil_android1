package com.bancomer.mbanking.otrasaplicaciones;

import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import android.content.Context;

public class SuiteAppOtrasApps extends com.bancomer.base.SuiteApp {
	public static boolean isSubAppRunning;
	public static Context appContext;
	private static SuiteAppOtrasApps me;
	private BaseViewControllerCommons baseViewControllerToReturn;

	public BaseViewControllerCommons getBaseViewControllerToReturn() {
		return baseViewControllerToReturn;
	}

	public void setBaseViewControllerToReturn(
			final BaseViewControllerCommons baseViewControllerReturn) {
		this.baseViewControllerToReturn = baseViewControllerReturn;
	}

	public void onCreate(final Context context) {
		isSubAppRunning = false;
		appContext = context;
		me = this;
	}

	public void cierraAplicacionSuiteApi() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}
	public static SuiteAppOtrasApps getInstanceApi() {
		return me;
	}



	public static boolean getBmovilStatusApi() {
		return PropertiesManager.getCurrent().getBmovilActivated();
	}


	public static int getResourceId(final String nombre, final String tipo) {
		return appContext.getResources().getIdentifier(nombre, tipo,
				appContext.getPackageName());
	}

	public Context getApplicationContext() {
		return appContext;
	}
}
