package com.bancomer.mbanking.apipush.adapters;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.mbanking.apipush.MainActivity;
import com.bancomer.mbanking.apipush.R;
import com.bancomer.mbanking.apipush.UtilsGCM;
import com.bancomer.mbanking.apipush.models.NotificacionesPush;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jinclan on 11/11/15.
 */
public class NotificacionesAdapter extends BaseAdapter {
    protected Activity activity;
    protected List<NotificacionesPush> items;

    private static CallBackBConnect showLogin;

    public static CallBackBConnect getShowLogin() {
        return showLogin;
    }

    public static void setShowLogin(CallBackBConnect showLogin) {
        NotificacionesAdapter.showLogin = showLogin;
    }

    public NotificacionesAdapter(final Activity activity, final List<NotificacionesPush> items) {
        this.activity = activity;
        this.items = items;
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(final int position) {
        return items.get(position);
    }

    public long getItemId(final int position) {
        return position;
    }

    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View vi=convertView;

        if(convertView == null) {
            final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.notifications_custom_adapter, null);
        }

        final NotificacionesPush item = items.get(position);

        final TextView titleCardNotificacion = (TextView) vi.findViewById(R.id.title_card_notificacion);

        final SimpleDateFormat formatDate= new SimpleDateFormat("EEE, MMM dd", new Locale("es_ES"));
        final SimpleDateFormat formatHour = new SimpleDateFormat("hh:mm", Locale.getDefault());
        final TextView fecha = (TextView) vi.findViewById(R.id.fechaPush);
        fecha.setText(formatDate.format(item.getFecha()).toUpperCase());
        final TextView hora = (TextView) vi.findViewById(R.id.horaPush);
        hora.setText(formatHour.format(item.getFecha()).toUpperCase());
        final TextView menuTexto = (TextView) vi.findViewById(R.id.textoNotificacion);
        menuTexto .setText(item.getMensaje());
        final ImageView icon = (ImageView) vi.findViewById(R.id.imagenNotificacion);
        final ImageView indicator = (ImageView) vi.findViewById(R.id.indicador);

        Pattern p = Pattern.compile("==");
        Matcher m = p.matcher(item.getMensaje());
        vi.setOnClickListener(null);
        indicator.setVisibility(View.GONE);
        if (m.find() && item.getTitulo().equalsIgnoreCase(UtilsGCM.pushAviso)) {
            indicator.setVisibility(View.VISIBLE);
            vi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivity.setIdCampania(item.getMensaje().substring(item.getMensaje().indexOf("=")+2,
                            item.getMensaje().indexOf("=")+19));
                    if (showLogin != null) {
                        showLogin.returnToPrincipal();
                    } else {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setComponent(new ComponentName("com.bancomer.mbanking", "com.bancomer.mbanking.SplashViewController"));
                        activity.startActivity(intent);
                        activity.finishAffinity();
                    }
                }
            });
            menuTexto.setText(item.getMensaje().replace(item.getMensaje().substring(item.getMensaje().indexOf("=")),""));
        }

        if(item.getTitulo().equalsIgnoreCase(UtilsGCM.pushCargo)){
            titleCardNotificacion.setBackgroundColor(vi.getResources().getColor(R.color.texto_cargos));
            titleCardNotificacion.setText(R.string.cargos);
            //icon.setImageResource(R.mipmap.an_ic_cargos);
        }else{
            if(item.getTitulo().equalsIgnoreCase(UtilsGCM.pushAbono)){
                titleCardNotificacion.setBackgroundColor(vi.getResources().getColor(R.color.texto_abonos));
                titleCardNotificacion.setText(R.string.abonos);
                //icon.setImageResource(R.mipmap.an_ic_abonos);
            }else{
                titleCardNotificacion.setBackgroundColor(vi.getResources().getColor(R.color.texto_aviso));
                titleCardNotificacion.setText(R.string.avisos);
                //titleCardNotificacion.setText();
                ///icon.setImageResource(R.mipmap.an_ic_todos);
            }
        }
        return vi;
    }

    @Override
    public void notifyDataSetChanged(){
        super.notifyDataSetChanged();
        Log.d("NotificacionesAdapter", "notifyDataSetChanged");
    }

    public List<NotificacionesPush> getItems() {
        return items;
    }

    public void setItems(final List<NotificacionesPush> menuItems) {
        this.items = menuItems;
    }

}
