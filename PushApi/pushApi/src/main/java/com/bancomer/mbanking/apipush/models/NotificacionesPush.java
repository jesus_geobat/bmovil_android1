package com.bancomer.mbanking.apipush.models;

import com.bancomer.mbanking.apipush.UtilsGCM;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by jinclan on 18/09/15.
 */
public class NotificacionesPush implements Serializable {

    private static final long serialVersionUID = 1L;

    private String mensaje;
    private String titulo;
    private Date fecha;

    public NotificacionesPush(final String mensaje, final String titulo, final Date fecha) {
        this.mensaje = mensaje;
        if(titulo==null){
            this.titulo = UtilsGCM.gcmTitulo;
        }else{
            this.titulo = titulo;
        }

        this.fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(final String mensaje) {
        this.mensaje = mensaje;
    }

    public String getTitulo() {
        if(titulo == null){
            titulo=UtilsGCM.gcmTitulo;
        }
        return titulo;
    }

    public void setTitulo(final String titulo) {
        this.titulo = titulo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(final Date fecha) {
        this.fecha = fecha;
    }
}
