package com.bancomer.mbanking.apipush;

import com.bancomer.mbanking.apipush.exeption.PushException;

/**
 * Created by jinclan on 17/09/15.
 */
public final class ApiPushData {

    private String celular;
    private String cliente;
    private static ApiPushData apiPushData;
    private ApiPushData(final String celular, final String cliente){
        this.celular=celular;
        this.cliente=cliente;
    }


    public static ApiPushData getInstance(final String celular, final String cliente){
        if(UtilsGCM.isNull(apiPushData)){
            apiPushData= new ApiPushData(celular,cliente);
        }
        return  apiPushData;

    }

    public static ApiPushData getInstance() throws PushException {
        if(UtilsGCM.isNull(apiPushData)){
            throw  new PushException("No se ha inicializado API PUSH");
        }
        return  apiPushData;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(final String celular) {
        this.celular = celular;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(final String cliente) {
        this.cliente = cliente;
    }
}