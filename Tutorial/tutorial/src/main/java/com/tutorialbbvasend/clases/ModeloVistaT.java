package com.tutorialbbvasend.clases;

public class ModeloVistaT {
    private int idVista;
    private String mensaje;
    private int [] coords;

    public ModeloVistaT(int idVista, String mensaje, int[] coords) {
        this.idVista = idVista;
        this.mensaje = mensaje;
        this.coords = coords;
    }

    public int getIdVista() {
        return idVista;
    }

    public String getMensaje() {
        return mensaje;
    }

    public int[] getCoords() {
        return coords;
    }
}
