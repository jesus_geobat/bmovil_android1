package com.tutorialbbvasend.clases;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.Date;

import static android.graphics.Bitmap.createBitmap;

public class FondoTutorial extends View {

    private static Paint p = new Paint();
    private static Paint transparentPaint = new Paint();
    private static Paint marcoT = new Paint();
    private static Paint paint = new Paint();
    private int addX1 = 0;
    private int addX2 = 0;
    private int addY1 = 0;
    private int addY2 = 0;
    private static  int pasos = 15;
    private int contadorT = 1;
    private int x1, x2;
    private int y1, y2;
    private int xa1, xa2;
    private int ya1, ya2;
    private int dx1, dx2;
    private int dy1, dy2;



    private int alfa;
    //variables para calcular la velocidad del dispositivo
    Long tiempo;
    Double velocidad;
    long tiempoFinal = 1000;

    private static Paint negroOscuro = new Paint();

    /**
     * recive el tiempo que se desea tarde la animacion
     * @param tiempoFinal
     */
    public void setTiempoFinal(long tiempoFinal) {
        this.tiempoFinal = tiempoFinal;
        velocidad = null;
        tiempo = null;
    }

    public FondoTutorial(Context context) {
        super(context);
        init();
    }

    public FondoTutorial(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        alfa = 255;

        xa1 = 0;
        xa2 = 0;
        ya1 = 0;
        ya2 = 0;
        dx1 = 0;
        dx2 = 0;
        dy1 = 0;
        dy2 = 0;

        contadorT = pasos;
        paint.setColor(getResources().getColor(R.color.color_fondos_transparente));
        transparentPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_ATOP));
        transparentPaint.setColor(getResources().getColor(R.color.color_fondos_transparente));
        transparentPaint.setAntiAlias(true);

        marcoT.setStyle(Paint.Style.STROKE);
        marcoT.setColor(getResources().getColor(R.color.color_blanco_transparente));
        marcoT.setAntiAlias(true);

        negroOscuro.setStyle(Paint.Style.FILL);
        negroOscuro.setColor(getResources().getColor(R.color.negro));
        negroOscuro.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //calcula la velocidad del dispositivo (solo lo hace 1 vez)
        if(velocidad == null && tiempo!=null){
            long diffTime=  new Date().getTime()-tiempo;
            velocidad= Double.valueOf(diffTime);
            pasos = Double.valueOf(tiempoFinal / velocidad).intValue();
        }
        //inicia conteo para calcular la velocidad
        if(tiempo == null){
            tiempo = new Date().getTime();
        }

        if (getWidth() == 0 || getHeight() == 0) {
            return;
        }

        int cocienteIntX1 = (contadorT * dx1)/pasos;
        int cocienteIntX2 = (contadorT * dx2)/pasos;
        int cocienteIntY1 = (contadorT * dy1)/pasos;
        int cocienteIntY2 = (contadorT * dy2)/pasos;

        addX1 = Math.abs(addX1) < Math.abs(cocienteIntX1) ? cocienteIntX1 : addX1;
        addX2 = Math.abs(addX2) < Math.abs(cocienteIntX2) ? cocienteIntX2 : addX2;
        addY1 = Math.abs(addY1) < Math.abs(cocienteIntY1) ? cocienteIntY1 : addY1;
        addY2 = Math.abs(addY2) < Math.abs(cocienteIntY2) ? cocienteIntY2 : addY2;

        Bitmap bitmap = createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas temp = new Canvas(bitmap);

        temp.drawRect(0, 0, temp.getWidth(), temp.getHeight(), paint);
        temp.drawRect(0, 0, temp.getWidth(), temp.getHeight(), paint);
        transparentPaint.setAlpha(alfa);

        RectF r = new RectF(xa1 + addX1,
                ya1 + addY1,
                xa2 + addX2,
                ya2 + addY2);

        temp.drawRoundRect(r, 10, 10, marcoT);
        temp.drawRoundRect(r, 10, 10, transparentPaint);


        if(contadorT < pasos){
            ++contadorT;
        }
        /*else{
            ImageView imageView = (ImageView) ((RelativeLayout)this.getParent()).findViewById(R.id.img_flecha_ind2);

            if(imageView.getVisibility()==View.VISIBLE) {
                int [] coordenadas = new int[2];
                imageView.getLocationInWindow(coordenadas);
                RectF r2 = new RectF(0, coordenadas[1]-50, canvas.getWidth(), ya2 + addY2 + (canvas.getWidth()/4) );
                temp.drawRoundRect(r2, 0, 0, negroOscuro);
            }
            else
            {
                RectF r2 = new RectF(0, ya1 + addY1-100, canvas.getWidth(), ya2 + addY2 + (canvas.getWidth()/4) );
                temp.drawRoundRect(r2, 0, 0, negroOscuro);
            }
        //___
            //temp.drawRect(0, 0, temp.getWidth(), temp.getHeight(), paint);
            transparentPaint.setAlpha(alfa);
            temp.drawRoundRect(r, 10, 10, marcoT);
            temp.drawRoundRect(r, 10, 10, transparentPaint);
        }*/
        canvas.drawBitmap(bitmap, 0, 0, p);
        invalidate();
    }

    public void changeArea(int marco, int a, int x1, int y1, int x2, int y2){
        marcoT.setStrokeWidth(marco);

        this.alfa = a;

        dx1 = x1 - this.x1;
        dx2 = x2 - this.x2;
        dy1 = y1 - this.y1;
        dy2 = y2 - this.y2;

        addX1 = 0;
        addX2 = 0;
        addY1 = 0;
        addY2 = 0;

        xa1 = this.x1;
        xa2 = this.x2;
        ya1 = this.y1;
        ya2 = this.y2;

        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;

        contadorT = 1;

        invalidate();
    }

    public void finalizaAnim(){
        alfa = 255;
        marcoT.setStrokeWidth(0);

        x1 = 0;
        y1 = 0;
        x2 = 0;
        y2 = 0;
        xa1 = 0;
        xa2 = 0;
        ya1 = 0;
        ya2 = 0;
        dx1 = 0;
        dx2 = 0;
        dy1 = 0;
        dy2 = 0;

        contadorT = pasos;
    }
}

