package com.tutorialbbvasend.clases;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TutorialBBVAView {

    public RelativeLayout relativeLayout;
    private TutorialDelegate tutorialDelegate;
    private RelativeLayout fondoTutorial;
    private FondoTutorial fondoAnimado;
    private ViewGroup pantallaPrincipal;
    Context context;

    public TextView txtDescripcionPaso;
    public ImageView imgFlecha;
    public ImageView imgMano;

    ImageButton close_Button;

    RelativeLayout bar_help;
    public EditText editText;
    public Activity activity;

    public String temp_text;
    public int temp_gravity;

    private Typeface style;

    ImageView image;

    int popup[];

    boolean aux_bandera;

    /**
     * Constructor para la carga del fondo del tutorial a la pantalla princial
     * @param pantallaPrincipal Es el layout principal donde contiene todas las vistas (Views)
     *                          que señalara y explicara su función.
     *                          Puede ser cualquier layot que extienda del ViewGroup, por ejemplo
     *                          RelativeLayout, FrameLayout, CoordinateLayout.
     */
    @SuppressLint("InflateParams")
    public TutorialBBVAView(Activity activity, ViewGroup pantallaPrincipal) {
       this.activity=activity;
        this.pantallaPrincipal = pantallaPrincipal;
        this.context = activity.getApplicationContext();
        LayoutInflater inflater = (LayoutInflater)   context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fondoTutorial = (RelativeLayout) inflater.inflate(R.layout.layout_tutorial_bbva_tutorial, null);
        pantallaPrincipal.addView(fondoTutorial);

        relativeLayout = (RelativeLayout)fondoTutorial.findViewById(R.id.container);

        txtDescripcionPaso =  (TextView) fondoTutorial.findViewById(R.id.txt_descripcion_tutorial);
        imgFlecha =  (ImageView) fondoTutorial.findViewById(R.id.img_flecha_ind);
        imgMano =  (ImageView) fondoTutorial.findViewById(R.id.img_hand_tuto);

        fondoAnimado = (FondoTutorial) fondoTutorial.findViewById(R.id.fondo_animacion);
        //pone el layout a typo hardware para tener la aceleracion por hardware
        this.fondoAnimado.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        bar_help = (RelativeLayout) pantallaPrincipal.findViewById(R.id.bar_help);
        close_Button=(ImageButton) pantallaPrincipal.findViewById(R.id.close_bar);
        aux_bandera = true;
    }

    /**coordenadas[0]
     * Metodo inicializador, recupera parametros necesarios para mostrar animación
     * @param id_vista Arreglo de componentes que recibiran la transparencia
     * @param texto_precardado Texto que aparecera precargado sobre el componente
     * @param texto_ayuda Texto de ayuda que aparecera en la parte inferior de el/los componentes
     * @param id_header Id del componente que actuara como header
     */
    public void initTutorial(final int[] id_vista, final int id_vista_mano ,final String texto_precardado,final String texto_ayuda,final int id_header, final int image, final boolean posImagen, final int posMano, final boolean posTexto){
        if(aux_bandera) {
            aux_bandera = false;
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if(activity.getCurrentFocus()!=null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
            final PopUpModel model[] = {
                    new PopUpModel(id_vista,id_vista_mano, id_header, texto_precardado, texto_ayuda, null, image, posImagen, posMano, posTexto)
            };
            acomodarBarra(id_header, image);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    popup = id_vista;
                    if(pantallaPrincipal.findViewById(popup[id_vista.length-1]) instanceof EditText) {
                        editText = (EditText) pantallaPrincipal.findViewById(popup[1]);

                        temp_text = editText.getText().toString();

                        editText.setText(texto_precardado);
                        temp_gravity = editText.getGravity();
                        editText.setGravity(Gravity.CENTER);
                        style = editText.getTypeface();

                        editText.setTypeface(null, Typeface.BOLD);
                        editText.setCursorVisible(false);
                    }
                    iniValores(model, 1000);
                    iniciarTutorial();
                }
            }, 250);
        }
    }

    /**
     * Metodo inicializador, el cual agrega la pantalla en tiempo de ejecucion.
     * @param model Arreglo tipo PopUpModel, el cual contiene los atributos de cada vista
     */
    public void iniValores(PopUpModel model[],long limit){
        fondoAnimado.setTiempoFinal(limit);
        tutorialDelegate = new TutorialDelegate(context,pantallaPrincipal, model);
        close_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close_Animation();
            }
        });
        }


    public void close_Animation()
    {
        tutorialDelegate.nextStep();
        tutorialDelegate=null;
        if(editText!=null){
        editText.setText("");
        editText.append(temp_text);
        editText.setCursorVisible(true);
        editText.setGravity(temp_gravity);
        editText.setTypeface(style);
        txtDescripcionPaso.setText("");
        }
        aux_bandera = true;
    }
    /**
     * Metodo que da inicio a la animacion del tutorial, si no se tiene una vista que dispare dicho evento,
     * y quiere iniciarlo al cargar la pantalla, se debe de poner en el onWindowFocusChanged
     */
    public void iniciarTutorial(){
        tutorialDelegate.iniciaTuto(new View[]{relativeLayout,txtDescripcionPaso, imgFlecha, imgMano}, fondoTutorial, fondoAnimado);
    }

    public void acomodarBarra(int id_header,int bImage){
        RelativeLayout bar_help = (RelativeLayout) pantallaPrincipal.findViewById(R.id.bar_help);
        if(id_header != 0) {
            View menu_bar = ((View) pantallaPrincipal.getRootView()).findViewById(id_header);
            int alturaBar = menu_bar.getHeight()-20;
            setMargins(bar_help, alturaBar);
        }
        else{
            setMargins(bar_help, 10);
        }

     /*   View menu_bar = ((View) pantallaPrincipal.getRootView()).findViewById(id_header);
        int alturaBar = menu_bar.getHeight()-20;
        RelativeLayout bar_help = (RelativeLayout) pantallaPrincipal.findViewById(R.id.bar_help);
        setMargins(bar_help, alturaBar);*/
    }

    private static void setMargins (View v, int altura) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(0, altura, 0, 0);
            v.requestLayout();
        }
    }

}
