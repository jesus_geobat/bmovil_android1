package com.tutorialbbvasend.clases;

/**
 * Created by terry0022 on 06/07/16.
 */
public class PopUpModel {

    private int[] idView;
    private int idHeader;
    private String textPreloading;
    private String helpText;
    private int[] coords;
    private int image;
    private boolean posImage;
    private int posHand;
    private boolean posText;
    private int id_vista_mano;

    public PopUpModel(int[] idView, int id_vista_mano, int idHeader, String textPreloading, String helpText, int[] coords, int image, boolean posImage, int posHand, boolean posText) {
        this.idView = idView;
        this.idHeader = idHeader;
        this.textPreloading = textPreloading;
        this.helpText = helpText;
        this.coords = coords;
        this.image = image;
        this.posImage = posImage;
        this.posHand = posHand;
        this.posText = posText;
        this.id_vista_mano=id_vista_mano;
    }
    public int[] getIdView() {
        return idView;
    }

    public int getIdVistaMano(){ return id_vista_mano;}

    public int getIdHeader() {
        return idHeader;
    }

    public String getTextPreloading() {
        return textPreloading;
    }

    public String getHelpText() {
        return helpText;
    }

    public int[] getCoords() {
        return coords;
    }

    public int getImage() {
        return image;
    }

    public boolean getPosImage() {
        return posImage;
    }

    public int getPosHand() {
        return posHand;
    }

    public boolean getPosText() {
        return posText;
    }
}
