package com.tutorialbbvasend.clases;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TutorialDelegate {
    private static final String TAG = "TutorialDelegate";
    private View pantallaPrincipal;
    private ImageView imgFlecha;
    private RelativeLayout fondoTutorial;
    private FondoTutorial fondoAnimado;
    private PopUpModel vistasTuto[];
    private int totalNumPasos, pasoActual;
    private AnimationSet animInOut;
    private RelativeLayout relativeLayout;
    private int anchoFlecha, altoFlecha;
    private int maxAncho;
    private int alturaBar;
    private Context context;
    private int marco = 30;

    private TextView txtDescripcionPaso;
    private ImageView imgMano;
    private ImageView image;

    public TutorialDelegate(Context context, View pantallaPrincipal, PopUpModel vistasTuto[]){

        this.pantallaPrincipal = pantallaPrincipal;
        this.vistasTuto = vistasTuto;
        this.context = context;

        totalNumPasos = vistasTuto.length - 1;
        pasoActual = 0;

        animInOut = new AnimationSet(false);

        Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(2000);

        animInOut.addAnimation(fadeIn);
    }

    public void iniciaTuto(View view[], RelativeLayout fondoTutorial, FondoTutorial fondoAnimado){
        this.fondoTutorial = fondoTutorial;
        this.fondoAnimado = fondoAnimado;
        fondoTutorial.setVisibility(View.VISIBLE);
        relativeLayout = (RelativeLayout)view[0];

        txtDescripcionPaso = (TextView)view[1];
        imgFlecha = (ImageView)view[2];
        imgMano = (ImageView)view[3];

        image = (ImageView) pantallaPrincipal.findViewById(R.id.img_flecha_ind2);

        anchoFlecha = converDpiToPx(pantallaPrincipal.getContext(), 8);
        altoFlecha = converDpiToPx(pantallaPrincipal.getContext(), 5);
        marco = converDpiToPx(pantallaPrincipal.getContext(),0);
        maxAncho = pantallaPrincipal.getWidth() - anchoFlecha;
        int coordsMain[] = new int[2];
        pantallaPrincipal.getLocationOnScreen(coordsMain);
        alturaBar = coordsMain[1];

        pasoSiguiente(0);
    }

    /**
     * Metodo para ejecutar paso numero n del tutorial
     *
     * @param paso El numero de paso a ejecutar
     */
    public void pasoSiguiente(int paso){
        pasoActual = paso;
        txtDescripcionPaso.setText(vistasTuto[paso].getHelpText());

        View vistaObjetivo = null;
        View vistaObjetivo2 = null;
        View vistaObjetivo3 = null;
        boolean esVistaEspecial = vistasTuto[paso].getCoords() != null;

        if(!esVistaEspecial){
            vistaObjetivo = pantallaPrincipal.findViewById(vistasTuto[paso].getIdView()[0]);
            vistaObjetivo2 = pantallaPrincipal.findViewById(vistasTuto[paso].getIdView()[vistasTuto[paso].getIdView().length-1]);
            vistaObjetivo3 = pantallaPrincipal.findViewById(vistasTuto[paso].getIdVistaMano());
            if(vistaObjetivo.getId()==vistaObjetivo2.getId()){
                // es un elemento
                vistaObjetivo2=null;
            }
        }

        if(vistaObjetivo != null){
            int [] coordenadas = new int[2];
            int [] coordenadas2 = new int[2];
            int [] coordenadas3 = new int[2];

            vistaObjetivo.getLocationInWindow(coordenadas);


            int [] coordFlecha;
            imgFlecha.setImageResource(R.drawable.mano_tuto);

            if(vistaObjetivo2!= null) {
                vistaObjetivo2.getLocationInWindow(coordenadas2);
                fondoAnimado.changeArea(marco, 0,
                        (coordenadas[0] < coordenadas2[0] ? coordenadas[0] - 15 : coordenadas2[0] - 15),
                        (coordenadas[1] - alturaBar - 20),
                        coordenadas2[0] + vistaObjetivo2.getWidth() + 15,
                        (coordenadas2[1] + vistaObjetivo2.getHeight() - alturaBar + 20));


                coordFlecha = getPosTexto(coordenadas2, vistaObjetivo2, this.vistasTuto[0].getPosText());
                setMarginsText(txtDescripcionPaso, getLeftTxt(coordFlecha[0]), coordFlecha[1] , true);

                vistaObjetivo3.getLocationInWindow(coordenadas3);
                coordFlecha=getPosFlecha2(coordenadas3, vistaObjetivo3, this.vistasTuto[0].getPosHand());

                //coordFlecha=getPosFlecha2(coordenadas2, vistaObjetivo2, this.vistasTuto[0].getPosHand());
                setPositionImage(coordFlecha, imgFlecha);

                if(vistasTuto[0].getImage() != 0) {
                    image.setImageResource(vistasTuto[0].getImage());
                    coordFlecha = getPosImage(coordenadas2, vistaObjetivo2, this.vistasTuto[0].getPosImage());
                    setPositionImage(coordFlecha, image);
                }
            }else{
                fondoAnimado.changeArea(marco, 0,
                        coordenadas[0],
                        (coordenadas[1] - alturaBar),
                        (coordenadas[0] + vistaObjetivo.getWidth()),
                        (coordenadas[1] + vistaObjetivo.getHeight() - alturaBar));


                coordFlecha = getPosTexto(coordenadas, vistaObjetivo, this.vistasTuto[0].getPosText());//Texto
                setMarginsText(txtDescripcionPaso, getLeftTxt(coordFlecha[0]), coordFlecha[1], true);
                coordFlecha=getPosFlecha2(coordenadas, vistaObjetivo, this.vistasTuto[0].getPosHand());//Mano
                setPositionImage(coordFlecha, imgFlecha);

                if(vistasTuto[0].getImage() != 0) {
                    image.setImageResource(vistasTuto[0].getImage());
                    coordFlecha = getPosImage(coordenadas, vistaObjetivo, this.vistasTuto[0].getPosImage());
                    setPositionImage(coordFlecha, image);
                }
            }
            /*setMargins(imgFlecha, coordFlecha[0], coordFlecha[1], 0);
            imgFlecha.setVisibility(View.VISIBLE);
            imgFlecha.startAnimation(animInOut);*/

            if(estaArriba(coordenadas[1])){
                coordFlecha = getPosFlecha(coordenadas, vistaObjetivo, true);
            }else{
                coordFlecha = getPosFlecha(coordenadas, vistaObjetivo, false);
            }
            setMargins(relativeLayout, coordFlecha[0], coordFlecha[1], 0);

        }else if(esVistaEspecial){
            int [] coordFlecha;

            fondoAnimado.changeArea(marco,0, vistasTuto[paso].getCoords()[0],
                    (vistasTuto[paso].getCoords()[1]),
                    (vistasTuto[paso].getCoords()[2]),
                    (vistasTuto[paso].getCoords()[3]));

            if(estaArriba(vistasTuto[paso].getCoords()[1])){
                coordFlecha = getPosFlecha(vistasTuto[paso].getCoords(), true);
            }else{
                coordFlecha = getPosFlecha(vistasTuto[paso].getCoords(), false);
            }
            setMargins(relativeLayout, coordFlecha[0], coordFlecha[1], 0);
        }else{
            Log.i(TAG, "pasoSiguiente: ");
        }

        LayoutInflater inflater = LayoutInflater.from(context);
        relativeLayout.removeAllViews();
        txtDescripcionPaso.setVisibility(View.VISIBLE);
        txtDescripcionPaso.startAnimation(animInOut);
    }

    /**
     * Metodo que se manda a llamar desde el OnClick de la pantalla del tutorial, el cual
     * hace que pase inmediatamente al siguiente paso.
     */
    public synchronized void nextStep(){
        if(pasoActual < totalNumPasos){
            relativeLayout.clearAnimation();
            relativeLayout.setVisibility(View.INVISIBLE);
            pasoSiguiente(pasoActual + 1);
        }else{
            finalizaTutorial();
        }
    }

    private void finalizaTutorial(){
        fondoAnimado.finalizaAnim();
        relativeLayout.setVisibility(View.INVISIBLE);
        fondoTutorial.setVisibility(View.INVISIBLE);
        if(image.getVisibility() == View.VISIBLE){
            image.setVisibility(View.INVISIBLE);
        }
        pasoActual = 0;
        // limpia el layout
        this.fondoAnimado.setLayerType(View.LAYER_TYPE_HARDWARE, null);
    }

    private int [] getPosFlecha(int[] coordVista, View vista, boolean estaArriba){
        int [] coord = new int[2];

        coord[0] = 70;

        if(estaArriba){
            coord[1] = coordVista[1] + vista.getHeight() - alturaBar + 30;
        }else{
            //coord[1] = coordVista[1] - ((vista.getHeight() + alturaBar) * 2) - 80;

            coord[1] = coordVista[1] - (vista.getHeight()+alturaBar)*2;
        }
        return coord;
    }

    private int [] getPosTexto(int[] coordVista, View vista, boolean estaArriba){
        int [] coord = new int[2];
        txtDescripcionPaso.getLayoutParams().width= ((pantallaPrincipal.getWidth()/10)*9);
        txtDescripcionPaso.requestLayout();


        coord[0] = 40;

        if(estaArriba){
            coord[1] = coordVista[1] + vista.getHeight() - alturaBar + 30;
        }else{
            //coord[1] = coordVista[1] - ((vista.getHeight() + alturaBar) * 2) - 80;
            int numeroLineas=((TextView)txtDescripcionPaso).getText().toString().split("\n").length;
            numeroLineas=numeroLineas-1;
            if(numeroLineas==0 || numeroLineas==1){
                numeroLineas=2;
            }
            coord[1] = (coordVista[1] - vista.getHeight()*numeroLineas)-20;
        }

        /*String[] temp_texto_mensaje=((TextView)txtDescripcionPaso).getText().toString().split("\n");
        String temp_mensaje="";
        StringBuilder temp_mensaje = new StringBuilder();
        for(String temp: temp_texto_mensaje){
            //temp_mensaje+=temp;
            temp_mensaje.append(temp);
        }
        txtDescripcionPaso.setText(temp_mensaje);*/
        return coord;
    }

    private int [] getPosFlecha(int[] coordVista, boolean estaArriba){
        int [] coord = new int[2];

        if(coordVista[0] > maxAncho){
            coord[0] = maxAncho;
        }else {
            if((coordVista[2] - coordVista[0])> anchoFlecha)
                coord[0] = coordVista[0] + ( coordVista[2] - coordVista[0] - anchoFlecha )/2;
            else{
                coord[0] = coordVista[0];
            }
        }

        if(estaArriba){
            coord[1] = coordVista[3];
        }else{
            coord[1] = coordVista[1] - altoFlecha;
        }

        return coord;
    }

    private int [] getPosFlecha2(int[] coordVista, View vista, int posHand){
        int [] coord = new int[2];
        if(coordVista[0] > maxAncho){
            coord[0] = maxAncho;
        }else {
            if(vista.getWidth() > anchoFlecha) {
                if(posHand == 1){
                    coord[0] = coordVista[0] +25;
                }else if(posHand == 2) {
                    coord[0] = coordVista[0] + (vista.getWidth()/2)-20;
                }else if(posHand == 3){
                    coord[0] = coordVista[0] + vista.getWidth() - (vista.getWidth()/5);
                }
            }else{
                coord[0] = coordVista[0];
            }
        }
        coord[1] = (coordVista[1] + (vista.getHeight()/2) - alturaBar)-20;
        return coord;
    }

    private static void setMargins (View v, int l, int t, int b) {

        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, 0, b);
            v.requestLayout();
        }
    }

    private boolean estaArriba(int altura){
        return pantallaPrincipal.getHeight() / 2 > altura;
    }

    private int converDpiToPx(Context context, int dps){
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
    }
    private void setMarginsText(View v, int l, int t, boolean estaArriba){
        RelativeLayout.LayoutParams layout = (RelativeLayout.LayoutParams) v.getLayoutParams();
        if(estaArriba){
            layout.addRule(RelativeLayout.ABOVE, 0);
            setMargins(v, l, t, 0);        }else{
            layout.addRule(RelativeLayout.ABOVE, imgFlecha.getId());
            setMargins(v, l, 0, (0 - t));
        }
    }

    private int getLeftTxt(int marginLeft){
        int margenMax = (int) (pantallaPrincipal.getWidth() * 0.6);
        if(marginLeft < 20)
            return 0;
        return marginLeft > margenMax ? margenMax : marginLeft - 20;
    }

    private int[] getPosImage(int[] coordVista, View vista, boolean posImage){
        int [] coord = new int[2];
        int [] coordBar = new int[2];

        coord[0] = coordVista[0]/6;

        RelativeLayout bar = (RelativeLayout) pantallaPrincipal.findViewById(R.id.bar_help);
        bar.getLocationInWindow(coordBar);

        if(posImage){
            coord[1] = coordBar[1] + bar.getHeight();
            int tamanioImage = (coordVista[1] - (coordBar[1]+bar.getHeight())) - vista.getHeight();
            image.getLayoutParams().height = (tamanioImage*3)/4;
            image.requestLayout();
        }else{
            if (vistasTuto[0].getPosText()){
                coord[1] = coordVista[1] + (vista.getHeight() + alturaBar) + txtDescripcionPaso.getHeight();
            }else{
                coord[1] = coordVista[1] + (vista.getHeight() + alturaBar);
            }
        }

        return coord;
    }

    private void setPositionImage(int[] coordFlecha, ImageView imgFlecha){
        if(this.vistasTuto[0].getPosHand()!=4) {
            setMargins(imgFlecha, coordFlecha[0], coordFlecha[1], 0);
            imgFlecha.setVisibility(View.VISIBLE);
            imgFlecha.startAnimation(animInOut);
        }
    }
}
