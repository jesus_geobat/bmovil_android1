package com.bancomer.oneclickseguros.commons;

/**
 * Created by DMEJIA on 30/09/15.
 */
public class ConstantsSeguros {

    public static String EXITO_SEGUROS = "exito seguros";
    public static String DETALLE_SEGUROS = "detalle seguros";

    public static String CONTRATO_HTML_SEGUROS = "ContratoHTMLSeguros";
    public static String VALOR_ID_SEGUROS = "PBSEGGFU02";
    public static String VALOR_IDE_SEGUROS_DOMICILIACION = "PBCCMDOM01";

    public static String CUENTA_CARGO = "cuentaCargo";

    public static String TAG_CUENTA_SUGERIDA = "cuentaSugerida";
    public static String TAG_PRODUCTO = "producto";
    public static String TAG_IMPORTE_PRIMA = "importePrima";
    public static String TAG_COBERTURA_SEGURO = "coberturaSeguro";
    public static String TAG_COBERTURAS = "coberturas";
    public static String TAG_DESCOBERTURA = "desCobertura";
    public static String TAG_IMPORTE_SUMA_ASEGURADA = "importeSumaAsegurada";
    public static String TAG_FECHA_ALTA = "fechaAlta";
    public static String TAG_FECHA_VENCIMIENTO = "fechaVencimiento";
    public static String TAG_NUMERO_POLIZA = "numeroPoliza";
    public static String TAG_ENVIO_CORREO_ELECTRONICO = "envioCorreoElectronico";
    public static String TAG_ENVIO_SMS = "envioSMS";
    public static String TAG_PM = "PM";
    public static String TAG_LP = "LP";
    public static String TAG_CAMPANIAS = "campanias";
    public static String TAG_CVE_CAMP = "cveCamp";
    public static String TAG_DESOFERTA = "desOferta";
    public static String TAG_MONTO = "monto";
    public static String TAG_CARRUSEL = "carrusel";

    public static String OP_TERMINOS = "TERMINOS";
    public static String OP_DOMICILIA = "DOMICILIA";

    public static final String OPERACION_TAG="operacion";
    public static final String NUMERO_CELULAR_TAG="numeroCelular";
    public static final String CVE_CAMP_TAG="cveCamp";
    public static final String IUM_TAG="IUM";
    public static final String OPCION_TAG="opcion";


    public static final String OPCION_DETALLE_VALUE="D";
    public static final String OPCION_FORMALIZACION_VALUE="F";

    public static final String CODIGO_OTP_TAG="codigoOTP";
    public static final String CADENA_AUTENTICACION_TAG="cadenaAutenticacion";
    public static final String CODIGO_NIP_TAG="codigoNip";
    public static final String CODIGO_CVV2_TAG="codigoCVV2";
    public static final String TARJETA_5IG_TAG="Tarjeta5ig";

    public static final String ID_PRODUCTO_TAG="idProducto";
}
