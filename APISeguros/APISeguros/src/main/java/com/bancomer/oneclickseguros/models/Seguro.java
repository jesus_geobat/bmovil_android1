package com.bancomer.oneclickseguros.models;

/**
 * Created by DMEJIA on 13/10/15.
 */
public class Seguro {
    private String coberturaSeguro;
    private String importeSumaAsegurada;

    public String getCoberturaSeguro() {
        return coberturaSeguro;
    }

    public void setCoberturaSeguro(String coberturaSeguro) {
        this.coberturaSeguro = coberturaSeguro;
    }

    public String getImporteSumaAsegurada() {
        return importeSumaAsegurada;
    }

    public void setImporteSumaAsegurada(String importeSumaAsegurada) {
        this.importeSumaAsegurada = importeSumaAsegurada;
    }
}
