package com.bancomer.oneclickseguros.delegates;

import android.content.DialogInterface;
import android.util.Log;

import com.bancomer.base.SuiteApp;
import com.bancomer.oneclickseguros.commons.ConstantsSeguros;
import com.bancomer.oneclickseguros.controllers.DetalleViewController;
import com.bancomer.oneclickseguros.controllers.R;
import com.bancomer.oneclickseguros.implementations.InitOneClickSeguros;
import com.bancomer.oneclickseguros.io.Server;
import com.bancomer.oneclickseguros.models.ConsultaTerminosYCondiciones;
import com.bancomer.oneclickseguros.models.DetalleCarrousel;
import com.bancomer.oneclickseguros.models.Formalizacion;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.logging.Handler;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.gui.delegates.SecurityComponentDelegate;
import bancomer.api.common.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
//import com.bancomer.oneclickseguros.commons.AccountSeguros;
import bancomer.api.common.model.HostAccounts;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtp;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GetOtpBmovil;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;

public class DetalleDelegate extends SecurityComponentDelegate implements BaseDelegate{

	private Constants.Operacion tipoOperacion;
	private Constants.TipoInstrumento tipoInstrumentoSeguridad;
	private DetalleViewController controller;
	private DetalleCarrousel detalles;
	private BaseViewController baseViewController;
	private Promociones promocion;
	private InitOneClickSeguros init;
	public static final long SEGUROS_DETALLE_DELEGATE = 10602003111230813L;

	private String token;
	private String contrasena;
	private String nip;
	private String cvv;
	private String tarjeta;

	private String montoPrima;

	private boolean terminos;
	private boolean domiciliar;

	private String cuentaCargo;
	private String tipoCuenta;
	private String opWeb;

	private Account[] cuentas;

	public DetalleDelegate(DetalleCarrousel detalleCarrousel){
		init = InitOneClickSeguros.getInstance().getInstance();
		tipoOperacion = Constants.Operacion.oneClicSeguros;
		if(detalleCarrousel != null) {
			detalles = detalleCarrousel.getSeguros();
			montoPrima = detalles.getImportePrima();
		}

		String instrumento = init.getSession().getSecurityInstrument();

		if (instrumento.equals(Constants.IS_TYPE_DP270)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
		} else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
		} else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)||instrumento.equals(Constants.TYPE_SOFTOKEN.S2.value)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
		} else {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
		}

	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public void setCuentaCargo(String cuentaCargo) {
		this.cuentaCargo = cuentaCargo;
	}

	public String getMontoPrima(){
		return montoPrima;
	}

	public boolean isDomiciliar() {
		return domiciliar;
	}

	public void setDomiciliar(boolean domiciliar) {
		this.domiciliar = domiciliar;
	}

	public boolean isTerminos() {
		return terminos;
	}

	public void setTerminos(boolean terminos) {
		this.terminos = terminos;
	}

	public Constants.TipoInstrumento getTipoInstrumentoSeguridad() {
		return tipoInstrumentoSeguridad;
	}

	public DetalleCarrousel cargarServicios(DetalleViewController controller){
		this.controller = controller;
		setContext(this.controller);
		return detalles;

	}

	public void realizaOperacionTerminosYCondiciones() {
		opWeb = "terminos";
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ConstantsSeguros.OPERACION_TAG, ConstantsSeguros.CONTRATO_HTML_SEGUROS);
		params.put(ConstantsSeguros.ID_PRODUCTO_TAG, ConstantsSeguros.VALOR_ID_SEGUROS);
		params.put(ConstantsSeguros.NUMERO_CELULAR_TAG,init.getSession().getUsername() );
		params.put(ConstantsSeguros.IUM_TAG, init.getSession().getIum());

		doNetworkOperation(ApiConstants.CONSULTA_TERMINOS_CONDICIONES_SEGUROS, params, init.getBaseViewController(),true,new ConsultaTerminosYCondiciones());

	}

	public void realizaOperacionDomiciliacion(){
		opWeb = "domicilia";
		isTerminos();
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ConstantsSeguros.OPERACION_TAG, ConstantsSeguros.CONTRATO_HTML_SEGUROS);
		params.put(ConstantsSeguros.ID_PRODUCTO_TAG, ConstantsSeguros.VALOR_IDE_SEGUROS_DOMICILIACION);
		params.put(ConstantsSeguros.NUMERO_CELULAR_TAG,init.getSession().getUsername() );
		params.put(ConstantsSeguros.IUM_TAG, init.getSession().getIum());

		doNetworkOperation(ApiConstants.CONSULTA_TERMINOS_CONDICIONES_SEGUROS, params, init.getBaseViewController(),true,new ConsultaTerminosYCondiciones());

	}

	public void formalizacion(){
		String cadAutenticacion = init.getAutenticacion().getCadenaAutenticacion(this.tipoOperacion,init.getSession().getClientProfile());

		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ConstantsSeguros.NUMERO_CELULAR_TAG,init.getSession().getUsername());
		params.put(ConstantsSeguros.CVE_CAMP_TAG,init.getOfertaSeguros().getCveCamp());
		params.put(ConstantsSeguros.IUM_TAG, init.getSession().getIum());
		params.put(ConstantsSeguros.CUENTA_CARGO, this.cuentaCargo);

		//Log.e("esta es la tarjeta donde se hara el cobro---", this.cuentaCargo);
		//params.put(Constants.TIPO_CUENTA, this.tipoCuenta);
		//Log.e("TIPO CUENTA---",this.tipoCuenta);
		//String tipo="";
		if(!this.tipoCuenta.equals("TC")){
			//tipo="TD";
			params.put(Constants.TIPO_CUENTA, "TD");
		}else{
			params.put(Constants.TIPO_CUENTA, this.tipoCuenta);
			//tipo=this.tipoCuenta;
		}
		//Log.e("se manda TIPO---", tipo);
		params.put(ConstantsSeguros.OPCION_TAG, ConstantsSeguros.OPCION_FORMALIZACION_VALUE);
		params.put(ConstantsSeguros.CODIGO_OTP_TAG, (token != null)? token:"");
		params.put(ConstantsSeguros.CADENA_AUTENTICACION_TAG, cadAutenticacion);
		params.put(ConstantsSeguros.CODIGO_NIP_TAG, (nip != null)? nip:"");
		params.put(ConstantsSeguros.CODIGO_CVV2_TAG, (cvv != null)? cvv:"");
		params.put(ConstantsSeguros.TARJETA_5IG_TAG, (tarjeta != null) ? tarjeta : "");

		doNetworkOperation(ApiConstants.CONSULTA_FORMALIZACION_GF, params, InitOneClickSeguros.getInstance().getBaseViewController(),true, new Formalizacion());
	}

	@Override
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
		init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false);
	}

	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller,Boolean isJson,ParsingHandler handler) {
		init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false,isJson,handler);
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {


		if (operationId == ApiConstants.CONSULTA_TERMINOS_CONDICIONES_SEGUROS){

			ConsultaTerminosYCondiciones terminos = (ConsultaTerminosYCondiciones)response.getResponse();
			if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if(opWeb.equalsIgnoreCase(ConstantsSeguros.OP_TERMINOS)) {
					init.showTerminosYCondiciones(terminos.getFormatoHTML());
				}else if(opWeb.equalsIgnoreCase(ConstantsSeguros.OP_DOMICILIA)){
					init.showFormatoDomiciliacion(terminos.getFormatoHTML());
				}
				opWeb = "";
			}else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(
									DialogInterface dialog,
									int which) {
							}
						});
			}
		}else if (operationId == ApiConstants.CONSULTA_FORMALIZACION_GF){
			terminos = false;
			domiciliar = false;
			Formalizacion formalizacion = (Formalizacion)response.getResponse();
			Log.e("STATUS SEGUROS", String.valueOf(response.getStatus()));
			if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if(formalizacion != null) {
					if (formalizacion.getPm().equalsIgnoreCase("SI")) {
						init.getSession().setPromocion(formalizacion.getArrayPromociones());
					}
					init.showResultados(formalizacion);
				}
			}else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				Log.e("STATUS SEGUROS", "ERROR");
				//init.resetInstance();
				try{
					if(formalizacion.getPm().equalsIgnoreCase("SI")){
						Log.e("STATUS SEGUROS", "ERROR 1");

						init.getSession().setPromocion(formalizacion.getArrayPromociones());
						DialogInterface.OnClickListener clickListener=new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								init.getParentManager().setActivityChanging(true);
								init.getParentManager().showMenuInicial();
							}
						};
						init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(
											DialogInterface dialog,
											int which) {
									}
								});
					}else {
						Log.e("STATUS SEGUROS", "ERROR 2");
						init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(
											DialogInterface dialog,
											int which) {
									}
								});
					}
				}catch (NullPointerException e){
					Log.e("STATUS SEGUROS", "ERROR 3");
					init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
								}
							});
				}
			}else{
				init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(
									DialogInterface dialog,
									int which) {
							}
						});
			}
		}if(operationId == ApiConstants.CONSULTA_DETALLE_SEGUROS){
			DetalleCarrousel detalleCarrousel = (DetalleCarrousel)response.getResponse();

			if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)
				init.showDetalleSeguros(detalleCarrousel);
			else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				try {
					if (detalleCarrousel.getSeguros().getPm().equalsIgnoreCase("SI")) {
						init.getSession().setPromocion(detalleCarrousel.getSeguros().getArrayPromociones());
						DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								init.getParentManager().setActivityChanging(true);
								init.getParentManager().showMenuInicial();
							}
						};
						init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(
											DialogInterface dialog,
											int which) {
									}
								});
					} else
						init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(
											DialogInterface dialog,
											int which) {
									}
								});
				}catch (NullPointerException e){
					init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
								}
							});
				}
			}else{
				init.getBaseViewController().showInformationAlert(controller, "Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(
									DialogInterface dialog,
									int which) {
							}
						});
			}
		}
	}

	@Override
	public void performAction(Object obj) {
		baseViewController = init.getBaseViewController();
		baseViewController.setActivity(init.getParentManager().getCurrentViewController());
		baseViewController.setDelegate(this);
		init.setBaseViewController(baseViewController);

		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ConstantsSeguros.NUMERO_CELULAR_TAG,init.getSession().getUsername());
		params.put(ConstantsSeguros.CVE_CAMP_TAG,init.getOfertaSeguros().getCveCamp());
		params.put(ConstantsSeguros.IUM_TAG, init.getSession().getIum());
		params.put(ConstantsSeguros.OPCION_TAG, ConstantsSeguros.OPCION_DETALLE_VALUE);

		doNetworkOperation(ApiConstants.CONSULTA_DETALLE_SEGUROS, params, InitOneClickSeguros.getInstance().getBaseViewController(),true,new DetalleCarrousel());


	}

	//obtiene las cuentas de tipo AH, LI y CH
	public ArrayList<HostAccounts> obtieneCuentas(){
		ArrayList<HostAccounts> accountsArray = new ArrayList<HostAccounts>();
		HostAccounts[] accounts = cuentaOrigen();

		for(HostAccounts acc : accounts) {
				accountsArray.add(acc);
		}
		return accountsArray;
	}

	/** ordena las cuentas y pone en la posicion 0 la cuenta cargo que viene por default
	 * en la oferta de domiciliacion, en caso de no tener en el arreglo de cuentas la cuenta cargo
	 * se obtiene la cuenta eje y es la que se setea en la posicion 0
	 */

	public ArrayList<HostAccounts> cargaCuentasOrigen(){
		ArrayList<HostAccounts> accountsArray = obtieneCuentas();
		ArrayList<HostAccounts> accArrayOrdenado = new ArrayList<HostAccounts>();
		HostAccounts accountCargo = null;
		HostAccounts accountEje = null;

		accountEje = Tools.obtenerCuentaEje(cuentaOrigen());

		for(HostAccounts acc : accountsArray) {
			if(acc.getNumber().equals(detalles.getCuentaSugerida())) {
				accountCargo = acc;
			}
			else if(accountEje.getNumber().equals(acc.getNumber())){
			}
			else{
				accArrayOrdenado.add(acc);
			}
		}

		if(accountCargo != null) {
			accArrayOrdenado.add(0, accountCargo);

			if(accountEje.equals(accountCargo)){
			}
			else if(accountEje != null)
				accArrayOrdenado.add(accountEje);

		} else
			accArrayOrdenado.add(0, accountEje);

        /*
        Log.e("accArrayOrdenado", "accArrayOrdenado");
        for(HostAccounts acc : accArrayOrdenado){
            Log.e("cuenta", acc.getAlias());
            Log.e("cuenta", acc.getNumber());
            Log.e("cuenta", String.valueOf(acc.getBalance()));
            Log.e("cuenta", acc.getType());
        }*/

		return accArrayOrdenado;
	}

	public HostAccounts[] cuentaOrigen(){
		return init.getListaCuentas();
	}

	@Override
	public long getDelegateIdentifier() {
		return 0;
	}

	public boolean mostrarContrasenia() {
		return init.getAutenticacion().mostrarContrasena(this.tipoOperacion,
				init.getSession().getClientProfile());
	}

	public boolean mostrarNIP() {
		return init.getAutenticacion().mostrarNIP(this.tipoOperacion,
				init.getSession().getClientProfile());
	}

	public boolean mostrarCampoTarjeta(){
		return false;
	}

	public Constants.TipoOtpAutenticacion tokenAMostrar() {
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = init.getAutenticacion().tokenAMostrar(this.tipoOperacion,
					init.getSession().getClientProfile());

		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
					"Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}

		return tipoOTP;
	}

	public boolean enviaPeticionOperacion() {
        /*
        String contrasena = null;
        String nip = null;
        String asm = null;
        String cvv = null;
        */
		String asm = null;
		if(controller instanceof DetalleViewController){
			if (mostrarContrasenia()) {
				contrasena = controller.pideContrasena();
				if (contrasena.equals("")) {
					String mensaje = controller.getString(R.string.confirmation_valorVacio);
					mensaje += " ";
					mensaje += controller.getString(R.string.confirmation_componenteContrasena);
					mensaje += ".";
					init.getBaseViewController().showInformationAlert(controller, mensaje);
					return false;
				} else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
					String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
					mensaje += " ";
					mensaje += Constants.PASSWORD_LENGTH;
					mensaje += " ";
					mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
					mensaje += " ";
					mensaje += controller.getString(R.string.confirmation_componenteContrasena);
					mensaje += ".";
					init.getBaseViewController().showInformationAlert(controller, mensaje);
					return false;
				}
			}

			if(mostrarCampoTarjeta()){
				tarjeta = controller.pideTarjeta();
				String mensaje = "";
				if(tarjeta.equals("")){
					mensaje = controller.getString(R.string.confirmation_componente_digitos_tarjeta_alerta);
					init.getBaseViewController().showInformationAlert(controller,mensaje);
					return false;
				}else if(tarjeta.length() != 5){
					mensaje =  controller.getString(R.string.confirmation_componente_digitos_tarjeta_alerta);
					init.getBaseViewController().showInformationAlert(controller, mensaje);
					return false;
				}
			}

			if (mostrarNIP()) {
				nip = controller.pideNIP();
				if (nip.equals("")) {
					String mensaje = controller.getString(R.string.confirmation_valorVacio);
					mensaje += " ";
					mensaje += controller.getString(R.string.confirmation_componenteNip);
					mensaje += ".";
					init.getBaseViewController().showInformationAlert(controller, mensaje);
					return false;
				} else if (nip.length() != Constants.NIP_LENGTH) {
					String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
					mensaje += " ";
					mensaje += Constants.NIP_LENGTH;
					mensaje += " ";
					mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
					mensaje += " ";
					mensaje += controller.getString(R.string.confirmation_componenteNip);
					mensaje += ".";
					init.getBaseViewController().showInformationAlert(controller, mensaje);
					return false;
				}
			}
			if (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) {
				asm = controller.pideASM();
				if (asm.equals("")) {
					String mensaje = controller.getString(R.string.confirmation_valorVacio);
					mensaje += " ";
					switch (tipoInstrumentoSeguridad) {
						case OCRA:
							mensaje += getEtiquetaCampoOCRA();
							break;
						case DP270:
							mensaje += getEtiquetaCampoDP270();
							break;
						case SoftToken:
							if (init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
								mensaje += getEtiquetaCampoSoftokenActivado();
							} else {
								mensaje += getEtiquetaCampoSoftokenDesactivado();
							}
							break;
						default:
							break;
					}
					mensaje += ".";
					init.getBaseViewController().showInformationAlert(controller, mensaje);
					return false;
				} else if (asm.length() != Constants.ASM_LENGTH) {
					String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
					mensaje += " ";
					mensaje += Constants.ASM_LENGTH;
					mensaje += " ";
					mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
					mensaje += " ";
					switch (tipoInstrumentoSeguridad) {
						case OCRA:
							mensaje += getEtiquetaCampoOCRA();
							break;
						case DP270:
							mensaje += getEtiquetaCampoDP270();
							break;
						case SoftToken:
							if (init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
								mensaje += getEtiquetaCampoSoftokenActivado();
							} else {
								mensaje += getEtiquetaCampoSoftokenDesactivado();
							}
							break;
						default:
							break;
					}
					mensaje += ".";
					init.getBaseViewController().showInformationAlert(controller, mensaje);
					return false;
				}
			}
			if (mostrarCVV()) {
				cvv = controller.pideCVV();
				if (cvv.equals("")) {
					String mensaje = controller.getString(R.string.confirmation_valorVacio);
					mensaje += " ";
					mensaje += controller.getString(R.string.confirmation_componenteCvv);
					mensaje += ".";
					init.getBaseViewController().showInformationAlert(controller, mensaje);
					return false;
				} else if (cvv.length() != Constants.CVV_LENGTH) {
					String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
					mensaje += " ";
					mensaje += Constants.CVV_LENGTH;
					mensaje += " ";
					mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
					mensaje += " ";
					mensaje += controller.getString(R.string.confirmation_componenteCvv);
					mensaje += ".";
					init.getBaseViewController().showInformationAlert(controller, mensaje);
					return false;
				}
			}

			String newToken = null;
			if(tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == Constants.TipoInstrumento.SoftToken && (init.getSofTokenStatus()  || PropertiesManager.getCurrent().getSofttokenService())) {
				if (init.getSofTokenStatus()) {
					newToken = init.getDelegateOTP().loadOtpFromSofttoken(tokenAMostrar(), init.getDelegateOTP());
					if (null != newToken)
						asm = newToken;
					token = asm;
					formalizacion();
				} else if (!init.getSofTokenStatus() && PropertiesManager.getCurrent().getSofttokenService()) {

					GetOtpBmovil otpG = new GetOtpBmovil(new GetOtp() {
						/**
						 * @param otp
						 */
						@Override
						public void setOtpRegistro(String otp) {

						}

						/**
						 * @param otp
						 */
						@Override
						public void setOtpCodigo(String otp) {
							if (ServerCommons.ALLOW_LOG) {
								Log.d("APP", "la otp en callback: " + otp);
							}
							token = String.valueOf(otp);
							formalizacion();
						}

						/**
						 * @param otp
						 */
						@Override
						public void setOtpCodigoQR(String otp) {

						}
					}, SuiteApp.appContext);

					otpG.generateOtpCodigo();
				}
			}
			else if(tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno ) {
				token = asm;
				formalizacion();
			}
			else if(tokenAMostrar() == Constants.TipoOtpAutenticacion.ninguno){
				token = asm;
				formalizacion();
			}
			return true;
		}
		return false;
	}

	public boolean mostrarCVV() {
		return init.getAutenticacion().mostrarCVV(this.tipoOperacion,
				init.getSession().getClientProfile());
	}
}
