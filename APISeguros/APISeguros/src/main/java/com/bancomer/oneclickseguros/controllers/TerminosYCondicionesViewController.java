package com.bancomer.oneclickseguros.controllers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.oneclickseguros.implementations.InitOneClickSeguros;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicBoolean;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.timer.TimerController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ControlEventos;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

import android.view.View.OnClickListener;

public class TerminosYCondicionesViewController extends Activity{

    private WebView wvpoliza;
    private GestureDetector gestureDetector;
    private String termino;
    private String domicilia;
    private BaseViewController baseViewController;
    private InitOneClickSeguros init = InitOneClickSeguros.getInstance();

    private AtomicBoolean mPreventAction = new AtomicBoolean(false);
    private LinearLayout header;
    private TextView txtTitleTerms;
    private ImageView imgflecha;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init.getParentManager().setCurrentActivityApp(this);

        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_seguros_terminos_condiciones);
        baseViewController.setTitle(this, R.string.seguro_gastos_funerarios, 0);
        init.setBaseViewController(baseViewController);


        termino = (String)this.getIntent().getExtras().get(Constants.TERMINOS_DE_USO_CONSUMO);
        domicilia = (String)this.getIntent().getExtras().get(Constants.DOMICILIACION_CONSUMO);
        /******/
        ViewGroup scroll = (ViewGroup) findViewById(R.id.body_layout);
        ViewGroup parent = (ViewGroup) scroll.getParent();
        parent.removeView(scroll);

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.api_seguros_terminos_condiciones, parent, true);
        findViews();
        scaleToScreenSize();
        /******/
        if(termino != null) {
            showTerminos(termino);
            txtTitleTerms.setText(getString(R.string.terminos_titulo));
        }else if (domicilia != null){
            showTerminos(domicilia);
            txtTitleTerms.setText(getString(R.string.formato_titulo));
        }


    }

    private void showTerminos(String html){
        html =leerArchivo(html);
        String html1;
        wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        wvpoliza.getSettings().setBuiltInZoomControls(true);
        double factor = GuiTools.getScaleFactor();
        wvpoliza.setInitialScale((int) (45 * factor));
        html1= html.replace("FECHADIA","________").replace("FECHAMES","______").replace("FECANO","____").replace("MONTOMAX","______").replace("FECVENCI","\"\"];").replace("BSCPAGAR","_____________").replace("<u>CONTRATO</u>","________").replace("<u>PERIODICIDAD</u"," ________").replace("FECEXIGI","________").replace("CVEBANCO","________").replace("RECA","______").replace("BBVA BANCOMER S.A. GRUPO FINANCIERO BBVA BANCOMER","_______________________________");
        wvpoliza.loadData(html1, "text/html", "utf-8");

        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                mPreventAction.set(true);
                return true;
            }
            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                mPreventAction.set(true);
                return true;
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                mPreventAction.set(true); // this is the key! this would block double tap to zoom to fire
                return false;
            }

        });
        gestureDetector.setIsLongpressEnabled(true);
        wvpoliza.setOnTouchListener(new View.OnTouchListener() {
            @TargetApi(Build.VERSION_CODES.FROYO)
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int pointId = 0;
                if (Build.VERSION.SDK_INT > 7) {
                    int index = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                    pointId = event.getPointerId(index);
                }
                if (pointId == 0) {
                    gestureDetector.onTouchEvent(event);
                    if (mPreventAction.get()) {
                        mPreventAction.set(false);
                        return true;
                    }
                    return wvpoliza.onTouchEvent(event);
                } else return true;
            }
        });
    }

    private String leerArchivo(String url){
        String leerHTML = "";
        try {
            FileInputStream fIn = new FileInputStream(url);
            InputStreamReader archivo = new InputStreamReader(fIn);
            BufferedReader br = new BufferedReader(archivo);
            String linea = br.readLine();
            while (linea != null) {
                leerHTML += linea + "";
                linea = br.readLine();
            }
            br.close();
            archivo.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return leerHTML;
    }

    /**
     * Busca las referencias a las vistas.
     */
    private void findViews() {
        wvpoliza = (WebView)findViewById(R.id.poliza);
        header = (LinearLayout) findViewById(R.id.frame_encabezadoTerminos);
        header.setVisibility(View.VISIBLE);
        txtTitleTerms = (TextView) findViewById(R.id.txtTitleTerms);
        imgflecha = (ImageView) findViewById(R.id.img_flecha_terminos);
        imgflecha.setOnClickListener(listener);
    }

    /**
     * Escala las vistas para acomodarse a la pantalla actual.
     */
    private void scaleToScreenSize() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.layoutBaseContainer));
        guiTools.scale(header);
        guiTools.scale(wvpoliza);
        guiTools.scale(findViewById(R.id.title_layout));
        guiTools.scale(imgflecha);
        guiTools.scale(findViewById(R.id.img_Logo));
        guiTools.scale(findViewById(R.id.txtTerminosTitle), true);
        guiTools.scale(txtTitleTerms , true);



    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        float touchX = ev.getX();
        float touchY = ev.getY();
        int[] webViewPos = new int[2];

        wvpoliza.getLocationOnScreen(webViewPos);

        float listaSeleccionX2 = webViewPos[0] + wvpoliza.getMeasuredWidth();
        float listaSeleccionY2 = webViewPos[1] + wvpoliza.getMeasuredHeight();

        if ((touchX >= webViewPos[0] && touchX <= listaSeleccionX2) &&
                (touchY >= webViewPos[1] && touchY <= listaSeleccionY2)) {
            wvpoliza.getParent().requestDisallowInterceptTouchEvent(true);
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onStop() {
        if (ServerCommons.ALLOW_LOG) {
            Log.e(getClass().getSimpleName(), "OnStop");
        }
        if(ControlEventos.isAppIsInBackground(this.getBaseContext())){ //Valida el estado de la app si es change Activity no hace nada
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "La app se fue a Background");
            }
            TimerController.getInstance().initTimer(this, TimerController.getInstance().getClase());

        }
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
       /* if(!init.getParentManager().isActivityChanging())
        {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }*/

    }

    @Override
    public void onBackPressed() {
        init.getParentManager().setActivityChanging(true);
        init.showDetalleSeguros();
        finish();

    }

    @Override
    public void onUserInteraction() {
       // super.onUserInteraction();
       // init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    OnClickListener listener = new OnClickListener()
    {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };





}
