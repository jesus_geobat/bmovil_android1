/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package com.bancomer.oneclickseguros.io;

import com.bancomer.oneclickseguros.commons.ConstantsSeguros;
import com.bancomer.oneclickseguros.models.ConsultaTerminosYCondiciones;
import com.bancomer.oneclickseguros.models.DetalleCarrousel;
import com.bancomer.oneclickseguros.models.Formalizacion;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.NameValuePair;
import bancomer.api.common.io.ParsingException;


/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receives the network operation
 * requests and builds a network operation suitable for ClienteHttp class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponseImpl object, so that the application logic can
 * perform high level checks and processes.
 * 
 * @author GoNet.
 */

public class Server {

	/**
	 * Defines if the application show logs
	 */
	public static boolean ALLOW_LOG = true;

	/**
	 * Defines if the application is running on the emulator or in the device.
	 */
	public static boolean EMULATOR = false;

	/**
	 * Indicates simulation usage.
	 */
	public static boolean SIMULATION = false;

	/**
	 * Indicates if the base URL points to development server or production
	 */
	public static boolean DEVELOPMENT = true;
	// public static boolean DEVELOPMENT = true; //TODO remove for TEST

	/**
	 * Indicates simulation usage.
	 */
	public static final long SIMULATION_RESPONSE_TIME = 5;


    public static final String JSON_OPERATION_SEGUROS = "SGRS001";

	/**
	 * Bancomer Infrastructure.
	 */
	public static final int BANCOMER = 0;

	public static int PROVIDER = BANCOMER; // TODO remove for TEST


	// ///////////////////////////////////////////////////////////////////////////
	// Operations identifiers //
	// ///////////////////////////////////////////////////////////////////////////


	/**ONE CLICK
	 * Seguros*/
	public static final int CONSULTA_DETALLE_SEGUROS=158;
	public static final int CONSULTA_TERMINOS_CONDICIONES_SEGUROS=159;
	public static final int CONSULTA_FORMALIZACION_GF =160;


	/**
	 * Operation codes.
	 */
	public static final String[] OPERATION_CODES = { "00","oneClicSeguros", "ContratoHTMLSeguros", "oneClicSeguros"};


	/**
	 * Base URL for providers.
	 */
	public static String[] BASE_URL;

	/**
	 * Path table for provider/operation URLs.
	 */
	public static String[][] OPERATIONS_PATH;

	/**
	 * Operation URLs map.
	 */
	public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

	/**
	 * Operation providers map.
	 */
	public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";


	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";



	public static void setEnviroment() {
		if (DEVELOPMENT) {
			setupDevelopmentServer();
		} else {
			setupProductionServer();
		}

		setupOperations(PROVIDER);
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupProductionServer() {
		BASE_URL = new String[] { "https://www.bancomermovil.com",
				// "https://172.17.100.173",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Produccion"
						: "" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "", "/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP1: Consulta Detalle ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP4: Formatos finales ventaTDC *ONE CLICK*

				},
				new String[] { "", "/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP1: Consulta Detalle ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP4: Formatos finales ventaTDC *ONE CLICK*


				} };
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupDevelopmentServer() {
		BASE_URL = new String[] {
				"https://www.bancomermovil.net:11443",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Desarrollo"
						: "http://189.254.77.54:8080/servermobile/Servidor/Desarrollo" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "", "/eexd_mx_web/servlet/ServletOperacionWeb",  // OP1: Consulta Detalle ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP4: Formatos finales ventaTDC *ONE CLICK*

				},
				new String[] { "", "/eexd_mx_web/servlet/ServletOperacionWeb",  // OP1: Consulta Detalle ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP4: Formatos finales ventaTDC *ONE CLICK*


				} };
	}

	/**
	 * Setup operations for a provider.
	 *
	 * @param provider
	 *            the provider
	 */
	private static void setupOperations(int provider) {

		setupOperation(CONSULTA_DETALLE_SEGUROS, provider); // Detalle
		setupOperation(CONSULTA_TERMINOS_CONDICIONES_SEGUROS, provider); //Terminos y Condiciones Seguros
		setupOperation(CONSULTA_FORMALIZACION_GF, provider);

	}

	/**
	 * Setup operation for a provider.
	 * @param operation the operation
	 * @param provider the provider
	 */
	private static void setupOperation(int operation, int provider) {

		int op=0;
		switch (operation){
			case CONSULTA_DETALLE_SEGUROS:
				op=1;
				break;
			case CONSULTA_TERMINOS_CONDICIONES_SEGUROS:
				op=2;
				break;
			case CONSULTA_FORMALIZACION_GF:
				op=3;
				break;
		}
		String code = OPERATION_CODES[op];
		OPERATIONS_MAP.put(
				code,
				new StringBuffer(BASE_URL[provider]).append(
						OPERATIONS_PATH[provider][op]).toString());
		OPERATIONS_PROVIDER.put(code, new Integer(provider));
	}

	/**
	 * Get the operation URL.
	 * @param operation the operation id
	 * @return the operation URL
	 */
	public static String getOperationUrl(String operation) {
		String url = (String) OPERATIONS_MAP.get(operation);
		return url;
	}

	/**
	 * Referencia a ClienteHttp, en lugar de HttpInvoker.
	 */
	private ClienteHttp clienteHttp = null;


	// protected Context mContext;

	public ClienteHttp getClienteHttp() {
		return clienteHttp;
	}

	public void setClienteHttp(ClienteHttp clienteHttp) {
		this.clienteHttp = clienteHttp;
	}



	public Server() {
		clienteHttp = new ClienteHttp();
	}

	/**
	 * perform the network operation identifier by operationId with the
	 * parameters passed.
	 * @param operationId the identifier of the operation
	 * @param params Hashtable with the parameters as key-value pairs.
	 * @return the response from the server
	 * @throws IOException exception
	 * @throws ParsingException exception
	 */
	public ServerResponseImpl doNetworkOperation(int operationId, Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = null;
		long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
		Integer provider = (Integer) OPERATIONS_PROVIDER.get(OPERATION_CODES[operationId]);
		if (provider != null) {
			PROVIDER = provider.intValue();
		}

		switch (operationId) {

			case CONSULTA_DETALLE_SEGUROS: // ONE CLICK venta TDC
				response = detalleOfertaSeguros(params);
				break;

			case CONSULTA_TERMINOS_CONDICIONES_SEGUROS:
				response = consultaTerminosCondicionesSeguros(params);
				break;

			case CONSULTA_FORMALIZACION_GF:
				response = formalizacionSeguros(params);
				break;
		}
		//Termina SPEI
		if (sleep > 0) {
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
			}
		}
		return response;
	}


	/**
	 * Cancel the ongoing network operation.
	 */
	public void cancelNetworkOperations() {
		clienteHttp.abort();
	}












	private ServerResponseImpl detalleOfertaSeguros(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		DetalleCarrousel detalleCarrousel = new DetalleCarrousel();
		ServerResponseImpl response = new ServerResponseImpl(detalleCarrousel);


		String numeroCelular = (String) params.get(ConstantsSeguros.NUMERO_CELULAR_TAG);
		String cveCamp = (String) params.get(ConstantsSeguros.CVE_CAMP_TAG);
		String IUM = (String) params.get(ConstantsSeguros.IUM_TAG);
		String opcion = (String) params.get(ConstantsSeguros.OPCION_TAG);


		NameValuePair[] parameters = new NameValuePair[4];
		parameters[0] = new NameValuePair(ConstantsSeguros.NUMERO_CELULAR_TAG, numeroCelular);
		parameters[1] = new NameValuePair(ConstantsSeguros.CVE_CAMP_TAG, cveCamp);
		parameters[2] = new NameValuePair(ConstantsSeguros.IUM_TAG, IUM);
		parameters[3] = new NameValuePair(ConstantsSeguros.OPCION_TAG, opcion);

		if (SIMULATION) {
			//traza OK
			this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DETALLE_SEGUROS], parameters, response,"{\"estado\":\"OK\",\"tipoCuenta\":\"AH\",\"cuentaCargo\":\"00740010000178659087\",\"producto\":\"56\",\"importePrima\":\"80\",\"coberturaSeguro\":{\"coberturas\":[{\"desCobertura\":\"Fallecimiento\",\"importeSumaAsegurada\":\"100000\"},{\"desCobertura\":\"MuerteAccidental\",\"importeSumaAsegurada\":\"100000\"},{\"desCobertura\":\"Hospitalización\",\"importeSumaAsegurada\":\"100000\"}]}}", true);

			//traza error
			//this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_FORMALIZACION_GF], parameters, response,"{\"estado\":\"ERROR\",\"codigoMensaje\":\"KZE0485\",\"descripcionMensaje\":\"SERVICIO NO DISPONIBLE.\" , \"PM\":\"SI\", \"LP\":{\"campanias\":[{\"cveCamp\":\"0130ABCDEGAS123\",\"desOferta\":\"Incremento de línea de credito\",\"monto\":\"50000\",\"carrusel\":\"SI\"},{\"cveCamp\":\"1234CAMP343435356\",\"desOferta\":\"VentaTDC\",\"monto\":\"30000\",\"carrusel\":\"SI\"},{\"cveCamp\":\"0377POIOIU23434\",\"desOferta\":\"Efectivoinmediato\",\"monto\":\"3000\",\"carrusel\":\"SI\"},{\"cveCamp\":\"0060CAM9123459864\",\"desOferta\":\"PIDE\",\"monto\":\"900000\",\"carrusel\":\"SI\"}]}}", true);


		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DETALLE_SEGUROS], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponseImpl consultaTerminosCondicionesSeguros(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		ConsultaTerminosYCondiciones terminosResponse = new ConsultaTerminosYCondiciones();
		ServerResponseImpl response = new ServerResponseImpl(terminosResponse);

		String idProducto = (String) params.get(ConstantsSeguros.ID_PRODUCTO_TAG);
		String numeroCelular = (String) params.get(ConstantsSeguros.NUMERO_CELULAR_TAG);
		String IUM = (String) params.get(ConstantsSeguros.IUM_TAG);

		NameValuePair[] parameters = new NameValuePair[3];
		parameters[0] = new NameValuePair(ConstantsSeguros.ID_PRODUCTO_TAG, idProducto);
		parameters[1] = new NameValuePair(ConstantsSeguros.NUMERO_CELULAR_TAG, numeroCelular);
		parameters[2] = new NameValuePair(ConstantsSeguros.IUM_TAG,IUM);


		if (SIMULATION) {

			//traza OK
			this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_TERMINOS_CONDICIONES_SEGUROS], parameters, response,"{\"estado\":\"OK\",\"formatoHTML\":\"<html>Detalle contrato</html>\"}", true);

		}else{

			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_TERMINOS_CONDICIONES_SEGUROS], parameters, response, false, true);
		}
		return response;
	}


	private ServerResponseImpl formalizacionSeguros(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		Formalizacion formalizacion = new Formalizacion();
		ServerResponseImpl response = new ServerResponseImpl(formalizacion);

		String numeroCelular = (String) params.get(ConstantsSeguros.NUMERO_CELULAR_TAG);
		String cveCamp = (String) params.get(ConstantsSeguros.CVE_CAMP_TAG);
		String IUM = (String) params.get(ConstantsSeguros.IUM_TAG);
		String opcion = (String) params.get(ConstantsSeguros.OPCION_TAG);
		String cuentaCargo = (String) params.get(ConstantsSeguros.CUENTA_CARGO);
		String tipoCuenta = (String) params.get(Constants.TIPO_CUENTA);
		String codigoOTP = (String) params.get(ConstantsSeguros.CODIGO_OTP_TAG);
		String cadenaAutenticacion = (String) params.get(ConstantsSeguros.CADENA_AUTENTICACION_TAG);
		String codigoNip = (String) params.get(ConstantsSeguros.CODIGO_NIP_TAG);
		String codigoCVV2 = (String) params.get(ConstantsSeguros.CODIGO_CVV2_TAG);
		String Tarjeta5ig = (String) params.get(ConstantsSeguros.TARJETA_5IG_TAG);

		NameValuePair[] parameters = new NameValuePair[11];
		parameters[0] = new NameValuePair(ConstantsSeguros.NUMERO_CELULAR_TAG, numeroCelular);
		parameters[1] = new NameValuePair(ConstantsSeguros.CVE_CAMP_TAG, cveCamp);
		parameters[2] = new NameValuePair(ConstantsSeguros.IUM_TAG, IUM);
		parameters[3] = new NameValuePair(ConstantsSeguros.OPCION_TAG, opcion);
		parameters[4] = new NameValuePair(ConstantsSeguros.CUENTA_CARGO, cuentaCargo);
		parameters[5] = new NameValuePair(Constants.TIPO_CUENTA, tipoCuenta);
		parameters[6] = new NameValuePair(ConstantsSeguros.CODIGO_OTP_TAG, codigoOTP);
		parameters[7] = new NameValuePair(ConstantsSeguros.CADENA_AUTENTICACION_TAG, cadenaAutenticacion);
		parameters[8] = new NameValuePair(ConstantsSeguros.CODIGO_NIP_TAG, codigoNip);
		parameters[9] = new NameValuePair(ConstantsSeguros.CODIGO_CVV2_TAG, codigoCVV2);
		parameters[10] = new NameValuePair(ConstantsSeguros.TARJETA_5IG_TAG, Tarjeta5ig);



		if (SIMULATION) {

			//traza OK
			this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_FORMALIZACION_GF], parameters, response,"{\"estado\": \"OK\",\"producto\": \"56\",\"fechaAlta\": \"2015-08-25\",\"fechaVencimiento\":\"2016-08-25\",\"importePrima\": \"80\",\"numeroPoliza\": \"87985453\",\"envioCorreoElectronico\":\"SI\",\"envioSMS\":\"SI\",\"PM\":\"SI\",\"LP\":{\"campanias\":[{\"cveCamp\":\"0130ABCDEGAS123\",\"desOferta\":\"Incrementodelíneadecredito\",\"monto\": \"50000\",\"carrusel\":\"SI\"},{\"cveCamp\": \"1234CAMP343435356\",\"desOferta\":\"VentaTDC\",\"monto\": \"30000\",\"carrusel\":\"SI\"},{\"cveCamp\": \"0377POIOIU23434\",\"desOferta\": \"Efectivoinmediato\",\"monto\":\"3000\",\"carrusel\":\"SI\"},{\"cveCamp\":\"0060CAM9123459864\",\"desOferta\": \"PIDE\",\"monto\": \"900000\",\"carrusel\":\"SI\"}]}}", true);

			//traza error
			//this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_FORMALIZACION_GF], parameters, response,"{\"estado\":\"ERROR\",\"codigoMensaje\":\"KZE0485\",\"descripcionMensaje\":\"SERVICIO NO DISPONIBLE.\" , \"PM\":\"SI\", \"LP\":{\"campanias\":[{\"cveCamp\":\"0130ABCDEGAS123\",\"desOferta\":\"Incremento de línea de credito\",\"monto\":\"50000\",\"carrusel\":\"SI\"},{\"cveCamp\":\"1234CAMP343435356\",\"desOferta\":\"VentaTDC\",\"monto\":\"30000\",\"carrusel\":\"SI\"},{\"cveCamp\":\"0377POIOIU23434\",\"desOferta\":\"Efectivoinmediato\",\"monto\":\"3000\",\"carrusel\":\"SI\"},{\"cveCamp\":\"0060CAM9123459864\",\"desOferta\":\"PIDE\",\"monto\":\"900000\",\"carrusel\":\"SI\"}]}}", true);

		}else{

			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_FORMALIZACION_GF], parameters, response, false, true);
		}
		return response;
	}

}