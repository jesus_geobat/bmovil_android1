package com.bancomer.oneclickseguros.controllers;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adobe.mobile.Config;
import com.bancomer.oneclickseguros.commons.ConstantsSeguros;
import com.bancomer.oneclickseguros.commons.Tracker;
import com.bancomer.oneclickseguros.delegates.ExitoDelegate;
import com.bancomer.oneclickseguros.implementations.InitOneClickSeguros;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.common.callback.CallBackModule;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ControlEventos;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class ExitoViewController extends Activity implements View.OnClickListener{
	
	private TextView txtMonto;
	private TextView txtTexto9;
	private TextView txtTexto10;
	private TextView txtEmail;
	private TextView txtPhone;
	private TextView txtPoliza;
	private TextView texto11;

	private LinearLayout email;
	private LinearLayout sms;

	private Button btnMenu;

	private String importePrima;
	private String numPoliza;

	private ExitoDelegate delegate;
	private BaseViewController baseViewController;
	public BaseViewsController parentManager;

	private InitOneClickSeguros init = InitOneClickSeguros.getInstance();

	private LinearLayout headerDetalle, headerExito;


	//Adobe
	ArrayList<String> list= new ArrayList<String>();
	Map<String,Object> operacionMap = new HashMap<String, Object>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		baseViewController = init.getBaseViewController();

		parentManager = init.getParentManager();
		parentManager.setCurrentActivityApp(this);

		delegate = (ExitoDelegate) parentManager.getBaseDelegateForKey(ExitoDelegate.SEGUROS_RESULTADOS_DELEGATE);

		baseViewController.setActivity(this);
		baseViewController.onCreate(this, 0, R.layout.api_seguros_exito);
		baseViewController.setDelegate(delegate);
		init.setBaseViewController(baseViewController);

		//Adobe
		Config.setContext(this.getApplicationContext());

		list.add(ConstantsSeguros.EXITO_SEGUROS);
		Tracker.trackState(list);

		Map<String,Object> eventoEntrada = new HashMap<String, Object>();
		eventoEntrada.put("evento_entrar", "event22");
		Tracker.trackEntrada(eventoEntrada);

		init();
	}
	
	
	private void init(){
		findViews();
		scaleViews();
		Spanned msj = Html.fromHtml(getString(R.string.texto_sms));
		txtTexto10.setText(msj);

		/*String textClausula = getString(R.string.api_domiciliacion_clausula);
		String textDom = getString(R.string.api_domiciliacion);*/
		int startTextDom= getString(R.string.leyenda_exito).indexOf(getString(R.string.www_seguros));
		SpannableString clausulaDom = new SpannableString(getString(R.string.leyenda_exito));
		clausulaDom.setSpan(new ForegroundColorSpan(Color.rgb(9, 79, 164)), startTextDom, startTextDom + getString(R.string.www_seguros).length(), 0);
		texto11.setText(clausulaDom);

		delegate.setController(this);
		delegate.cargarValores();
	}
	
	private void findViews(){
		txtEmail = (TextView)findViewById(R.id.txtCorreo);
		txtPhone = (TextView)findViewById(R.id.txtTelefono);
		txtPoliza = (TextView)findViewById(R.id.txtPoliza);
		txtMonto = (TextView)findViewById(R.id.txtMontoPrima);
		txtTexto9 = (TextView)findViewById(R.id.texto9);
		txtTexto10 = (TextView)findViewById(R.id.texto10);
		texto11 =  (TextView)findViewById(R.id.texto11);
		btnMenu = (Button)findViewById(R.id.btnMenu);

		email = (LinearLayout)findViewById(R.id.linearEmail);
		sms = (LinearLayout)findViewById(R.id.linearSms);

		email.setVisibility(View.GONE);
		sms.setVisibility(View.GONE);

		btnMenu.setOnClickListener(this);


		headerDetalle = (LinearLayout) findViewById(R.id.frame_encabezadoDetalle);
		headerExito = (LinearLayout) findViewById(R.id.frame_encabezadoExito);

		headerDetalle.setVisibility(View.GONE);
		headerExito.setVisibility(View.VISIBLE);

	}
	
	private void scaleViews(){
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.layout_contenido));
		guiTools.scale(findViewById(R.id.layout_cuerpo));
		guiTools.scale(findViewById(R.id.encabezadoResultados));
		guiTools.scale(findViewById(R.id.layout_contenido));
		guiTools.scale(findViewById(R.id.texto1), true);
		guiTools.scale(findViewById(R.id.texto11), true);
		guiTools.scale(findViewById(R.id.texto12), true);
		guiTools.scale(findViewById(R.id.texto13), true);
		guiTools.scale(findViewById(R.id.texto2), true);
		guiTools.scale(findViewById(R.id.texto3), true);
		guiTools.scale(findViewById(R.id.texto4), true);
		guiTools.scale(findViewById(R.id.texto6), true);
		guiTools.scale(findViewById(R.id.texto9bold), true);
		guiTools.scale(findViewById(R.id.txtAlMes), true);
		guiTools.scale(findViewById(R.id.txtSegurosTitles), true);
		guiTools.scale(findViewById(R.id.txtVive), true);
		//guiTools.scale(findViewById(R.id.imgaviso), true);
		guiTools.scale(txtEmail, true);
		guiTools.scale(txtMonto, true);
		guiTools.scale(txtPhone, true);
		guiTools.scale(txtPoliza, true);
		guiTools.scale(txtTexto10, true);
		guiTools.scale(txtTexto9, true);
		guiTools.scale(headerExito);
		
		guiTools.scale(findViewById(R.id.img_Logo));
		
		guiTools.scale(btnMenu);
	}

	@Override
	public void onClick(View v) {
		if(v == btnMenu){
			init.getParentManager().setActivityChanging(true);
			//init.getParentManager().showMenuInicial();
			((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
			init.getParentManager().resetRootViewController();
			init.resetInstance();
		}
	}

	public void setTxtMonto(String montoPrima){
		txtMonto.setText("$" + Tools.formatUserAmount(montoPrima));
	}

	public void setTxtPoliza(String numPoliza){
		txtPoliza.setText(numPoliza);
	}

	public void showEmail(String email){
		this.email.setVisibility(View.VISIBLE);
		txtEmail.setText(email);
	}

	public void showSms(String sms){
		this.sms.setVisibility(View.VISIBLE);
		txtPhone.setText(sms);
	}

	@Override
	public void onBackPressed() {

	}


	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		init.getParentManager().onUserInteraction();
		TimerController.getInstance().resetTimer();
	}

	@Override
	protected void onPause() {
		super.onPause();
		/*if(!init.getParentManager().isActivityChanging())
		{
			TimerController.getInstance().getSessionCloser().cerrarSesion();
			init.getParentManager().resetRootViewController();
			init.resetInstance();
		}*/

	}

    @Override
    protected void onStop() {
        if (ServerCommons.ALLOW_LOG) {
            Log.e(getClass().getSimpleName(), "OnStop");
        }
        if(ControlEventos.isAppIsInBackground(this.getBaseContext())){ //Valida el estado de la app si es change Activity no hace nada
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "La app se fue a Background");
            }
            TimerController.getInstance().initTimer(this, TimerController.getInstance().getClase());

        }
        super.onStop();
    }


}
