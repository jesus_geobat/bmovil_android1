package com.bancomer.oneclickseguros.models;

/**
 * Created by DMEJIA on 01/10/15.
 */
public class Campania {

    private String claveCampania;
    private String descripcionOferta;
    private String monto;
    private String carrusel;

    public Campania(){}

    public Campania(String claveCampania, String descripcionOferta, String monto, String carrusel) {
        this.claveCampania = claveCampania;
        this.descripcionOferta = descripcionOferta;
        this.monto = monto;
        this.carrusel = carrusel;
    }

    public String getClaveCampania() {
        return claveCampania;
    }

    public void setClaveCampania(String claveCampania) {
        this.claveCampania = claveCampania;
    }

    public String getDescripcionOferta() {
        return descripcionOferta;
    }

    public void setDescripcionOferta(String descripcionOferta) {
        this.descripcionOferta = descripcionOferta;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getCarrusel() {
        return carrusel;
    }

    public void setCarrusel(String carrusel) {
        this.carrusel = carrusel;
    }
}
