/**
 * 
 */
package com.bancomer.oneclickseguros.commons;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

import com.bancomer.oneclickseguros.implementations.InitOneClickSeguros;

/**
 * Implementation of the App power manager for api7 or greater.
 */
public final class EclairPowerManager extends AbstractSuitePowerManager {
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.common.AbstractSuitePowerManager#isScreenOn()
	 */
	@Override
	public boolean isScreenOn() {
		try {
			PowerManager pm = (PowerManager) InitOneClickSeguros.appContext.getSystemService(Context.POWER_SERVICE);
			return pm.isScreenOn();
		}catch(Exception ex) {
			Log.wtf("EclairPowerManager", "Error while getting the state of the screen.", ex);
			return false;
		}
	}

}
