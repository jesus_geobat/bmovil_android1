package com.bancomer.oneclickseguros.implementations;

import android.app.Activity;
import android.content.Context;

import com.bancomer.oneclickseguros.commons.AccountSeguros;
import com.bancomer.oneclickseguros.delegates.DetalleDelegate;
import com.bancomer.oneclickseguros.delegates.ExitoDelegate;
import com.bancomer.oneclickseguros.controllers.ExitoViewController;
import com.bancomer.oneclickseguros.controllers.DetalleViewController;
import com.bancomer.oneclickseguros.controllers.TerminosYCondicionesViewController;
import com.bancomer.oneclickseguros.io.BaseSubapplication;
import com.bancomer.oneclickseguros.io.Server;
import com.bancomer.oneclickseguros.models.DetalleCarrousel;
import com.bancomer.oneclickseguros.models.Formalizacion;

import org.apache.http.impl.client.DefaultHttpClient;

import bancomer.api.common.commons.IAutenticacion;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.BaseViewsControllerImpl;
import bancomer.api.common.gui.delegates.DelegateBaseOperacion;
import bancomer.api.common.gui.delegates.MainViewHostDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
//import com.bancomer.oneclickseguros.commons.AccountSeguros;
import bancomer.api.common.model.HostAccounts;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by DKMG on 8/20/15.
 */
public class InitOneClickSeguros {


    /*Components for APIVENTATDC working*/

    private static InitOneClickSeguros mInstance = null;

    private BaseViewController baseViewController;

    private static BaseViewsController parentManager;

    private BaseSubapplication baseSubApp;

    public static DefaultHttpClient client;

    private Promociones ofertaSeguros;

    private DelegateBaseOperacion delegateOTP;

    private ICommonSession session;

    private IAutenticacion autenticacion;

    private boolean sofTokenStatus;

    private String cuentaOneClick;

    private MainViewHostDelegate hostInstance;

    private AccountSeguros[] listaCuentas;

    private DetalleCarrousel detalleCarrousel;

    public static Context appContext;

    /* End Region */

    /////////////  Getter's & Setter's /////////////////


    public ICommonSession getSession()
    {
        return session;
    }

    public DelegateBaseOperacion getDelegateOTP() {
        return delegateOTP;
    }

    public Promociones getOfertaSeguros()
    {
        return ofertaSeguros;
    }

    public BaseViewController getBaseViewController() {
        return baseViewController;
    }

    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }

    public BaseViewsController getParentManager() {
        return parentManager;
    }

    public BaseSubapplication getBaseSubApp() {
        return baseSubApp;
    }

    public IAutenticacion getAutenticacion() {  return autenticacion;  }

    public boolean getSofTokenStatus(){ return sofTokenStatus; }

    public String getCuentaOneClick() {
        return cuentaOneClick;
    }

    public AccountSeguros[] getListaCuentas() {
        return listaCuentas;
    }

///////////// End Region Getter's & Setter's /////////////////

    public static InitOneClickSeguros getInstance(Activity activity, Promociones promocion, DelegateBaseOperacion delegateOTP, ICommonSession session, IAutenticacion autenticacion, boolean softTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance, HostAccounts[] listaCuentas,boolean dev, boolean sim, boolean emulator, boolean log, Context appContext) {
        if(mInstance == null)
            mInstance = new InitOneClickSeguros(activity, promocion, delegateOTP, session, autenticacion, softTokenStatus, client, hostInstance, listaCuentas,dev, sim, emulator, log, appContext);
         else
            parentManager.setCurrentActivityApp(activity);

        return mInstance;
    }

    public static InitOneClickSeguros getInstance() {
        return mInstance;
    }

    private InitOneClickSeguros(Activity activity, Promociones promocion, DelegateBaseOperacion delegateOTP, ICommonSession session, IAutenticacion autenticacion, boolean sofTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance, HostAccounts[] listaCuentas,boolean dev, boolean sim, boolean emulator, boolean log, Context appContext) {
        this.setServerParams(dev, sim, emulator, log);
        this.appContext = appContext;
        Server.setEnviroment();
        this.ofertaSeguros = promocion;
        this.delegateOTP = delegateOTP;
        this.baseViewController = new BaseViewControllerImpl();
        this.client = client;
        parentManager = new BaseViewsControllerImpl();
        parentManager.setCurrentActivityApp(activity);
        this.baseSubApp = new BaseSubapplicationImpl(activity, this.client);
        this.session = session;
        this.autenticacion = autenticacion;
        this.sofTokenStatus = sofTokenStatus;
        this.hostInstance = hostInstance;
        //this.listaCuentas = listaCuentas;
        setAcounts(listaCuentas);
    }



    public void setServerParams(boolean dev, boolean sim, boolean emulator, boolean log)
    {
        Server.DEVELOPMENT = dev;
        Server.SIMULATION = sim;
        Server.EMULATOR = emulator;
        Server.ALLOW_LOG = log;

    }



    public void showTerminosYCondiciones(String poliza) {
        parentManager.showViewController(TerminosYCondicionesViewController.class, 0, false,
                new String[]{Constants.TERMINOS_DE_USO_CONSUMO},
                new Object[]{poliza}
                ,parentManager.getCurrentViewController());
    }

    public void showFormatoDomiciliacion(String poliza) {
        parentManager.showViewController(TerminosYCondicionesViewController.class, 0, false,
                new String[]{Constants.DOMICILIACION_CONSUMO},
                new Object[]{poliza}
                ,parentManager.getCurrentViewController());
    }

    public void showResultados(Formalizacion formalizacion) {

        ExitoDelegate delegate = (ExitoDelegate) parentManager.getBaseDelegateForKey(ExitoDelegate.SEGUROS_RESULTADOS_DELEGATE);

        if(delegate == null){
            delegate = new ExitoDelegate(formalizacion);
            parentManager.addDelegateToHashMap(ExitoDelegate.SEGUROS_RESULTADOS_DELEGATE,delegate);
        }

        //parentManager.showViewController(ExitoViewController.class);
        parentManager.showViewController(ExitoViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }

   public void showDetalleSeguros(DetalleCarrousel detalleCarrousel){
        DetalleDelegate delegate = (DetalleDelegate) parentManager.getBaseDelegateForKey(DetalleDelegate.SEGUROS_DETALLE_DELEGATE);

       this.detalleCarrousel = detalleCarrousel;
        if(delegate == null){
            delegate = new DetalleDelegate(detalleCarrousel);
            parentManager.addDelegateToHashMap(DetalleDelegate.SEGUROS_DETALLE_DELEGATE,delegate);
        }
       parentManager.showViewController(DetalleViewController.class,0,false,null,null,parentManager.getCurrentViewController());
      // parentManager.showViewController(DetalleViewController.class);
    }

    public void showDetalleSeguros(){
        DetalleDelegate delegate = (DetalleDelegate) parentManager.getBaseDelegateForKey(DetalleDelegate.SEGUROS_DETALLE_DELEGATE);

        if(delegate == null){
            delegate = new DetalleDelegate(detalleCarrousel);
            parentManager.addDelegateToHashMap(DetalleDelegate.SEGUROS_DETALLE_DELEGATE,delegate);
        }

        //parentManager.showViewController(DetalleViewController.class);
        parentManager.showViewController(DetalleViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }

    public void resetInstance()
    {   mInstance = null; }

    public void startSeguros()
    {   DetalleCarrousel detalleCarrousel = null;
        new DetalleDelegate(detalleCarrousel).performAction(null);}

    public void setAcounts(HostAccounts[] accounts) {
        AccountSeguros[] acc = new AccountSeguros[accounts.length];

        if (accounts != null){
            for (int i=0; i<accounts.length; i++){
                AccountSeguros aux = new AccountSeguros();

                aux.setBalance(accounts[i].getBalance());
                aux.setNumber(accounts[i].getNumber());
                aux.setDate("");
                aux.setVisible(accounts[i].isVisible());
                aux.setCurrency("");
                aux.setType(accounts[i].getType());
                aux.setAlias(accounts[i].getAlias());
                aux.setConcept("");
                aux.setCelularAsociado("");
                aux.setCodigoCompania("");
                aux.setDescripcionCompania("");
                aux.setFechaUltimaModificacion("");
                aux.setIndicadorSPEI("");
                acc[i]=aux;
            }
        }
        this.listaCuentas = acc;

    }
}
