package com.bancomer.oneclickseguros.controllers;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adobe.mobile.Config;
import com.bancomer.oneclickseguros.delegates.DetalleDelegate;
import com.bancomer.oneclickseguros.carrousel.Carrousel;
import com.bancomer.oneclickseguros.carrousel.CarrouselAdapter;
import com.bancomer.oneclickseguros.carrousel.CarrouselAdapter.OnItemClickListener;
import com.bancomer.oneclickseguros.commons.ConstantsSeguros;
import com.bancomer.oneclickseguros.commons.Tracker;
import com.bancomer.oneclickseguros.implementations.InitOneClickSeguros;
import com.bancomer.oneclickseguros.models.DetalleCarrousel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.CuentaOrigenController;
import bancomer.api.common.model.HostAccounts;
import bancomer.api.common.timer.TimerController;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ControlEventos;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;

public class DetalleViewController extends Activity implements View.OnClickListener{
	
	private TextView txtSeguro;
	private TextView txtProtege;
	private TextView txtTerminos;
	private TextView txtAcepto;
	private TextView txtAutorizo;
	private TextView txtDomiciliacionLink;
	private TextView txtavisoLink;

	private TextView txtPrima;

	private CheckBox cbTerminos;
	private CheckBox cbAutorizo;

	private Button btnAceptar;
	
	//private ImageView imgIzq;
	//private ImageView imgDer;
	
	private ImageView imgFlecha;
	private ImageView imgLogo;
	
	private String cuentas[];
	private LinearLayout vistaCtaOrigen;

	//private PageIndicator mIndicator;
	//private ViewPager mPager;
	private Carrousel carrousel;
	
	private DetalleDelegate delegate;
	private BaseViewController baseViewController;
	private InitOneClickSeguros init = InitOneClickSeguros.getInstance();
	public BaseViewsController parentManager;

	private Constants.Operacion tipoOperacion;
	public CuentaOrigenController componenteCtaOrigen;
	private LinearLayout headerDetalle;


	/**
	 * TOKEN
	 */
	private LinearLayout layout_avisotoken;
	private ImageButton imgaviso_token;
	private TextView    txtaviso_token;
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;
	private LinearLayout contenedorCampoTarjeta;

	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;
	private TextView campoTarjeta;

	private EditText contrasena;
	private EditText nip;
	private EditText asm;
	private EditText cvv;
	private EditText tarjeta;

	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;
	private TextView instruccionesTarjeta;


	//Adobe
	ArrayList<String> list= new ArrayList<String>();
	Map<String,Object> operacionMap = new HashMap<String, Object>();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		baseViewController = init.getBaseViewController();

		parentManager = init.getParentManager();
		parentManager.setCurrentActivityApp(this);

		delegate = (DetalleDelegate) parentManager.getBaseDelegateForKey(DetalleDelegate.SEGUROS_DETALLE_DELEGATE);
		baseViewController.setActivity(this);
		baseViewController.onCreate(this, 0, R.layout.api_seguros_detalle);
		baseViewController.setDelegate(delegate);
		init.setBaseViewController(baseViewController);

		//Adobe
		Config.setContext(this.getApplicationContext());

		list.add(ConstantsSeguros.DETALLE_SEGUROS);
		Tracker.trackState(list);

		Map<String,Object> eventoEntrada = new HashMap<String, Object>();
		eventoEntrada.put("evento_entrar", "event22");
		Tracker.trackEntrada(eventoEntrada);

		init();		
	}

	private void scaleViews(){
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.layout_cuerpo));
		guiTools.scale(findViewById(R.id.carousel));
		guiTools.scale(findViewById(R.id.layout_cuentas));
		guiTools.scale(txtAcepto, true);
		guiTools.scale(txtAutorizo, true);
		guiTools.scale(txtProtege, true);
		guiTools.scale(txtSeguro, true);
		guiTools.scale(txtTerminos, true);
		guiTools.scale(txtDomiciliacionLink, true);
		guiTools.scale(findViewById(R.id.txtSegurosTitle), true);
		guiTools.scale(btnAceptar);
		guiTools.scale(findViewById(R.id.encabezado));
		guiTools.scale(findViewById(R.id.title_layout));
		guiTools.scale(findViewById(R.id.title_layout_precio));
		guiTools.scale(txtPrima);
		guiTools.scale(findViewById(R.id.txtmes));
		guiTools.scale(findViewById(R.id.carousel));
		guiTools.scale(findViewById(R.id.layout_contenido));
		guiTools.scale(headerDetalle);
		guiTools.scale(txtavisoLink, true);

		//Token
		guiTools.scale(imgaviso_token);
		guiTools.scale(txtaviso_token,true);

		guiTools.scale(contenedorContrasena);
		guiTools.scale(contenedorNIP);
		guiTools.scale(contenedorASM);
		guiTools.scale(contenedorCVV);

		guiTools.scale(contrasena, true);
		guiTools.scale(nip, true);
		guiTools.scale(asm, true);
		guiTools.scale(cvv, true);

		guiTools.scale(campoContrasena, true);
		guiTools.scale(campoNIP, true);
		guiTools.scale(campoASM, true);
		guiTools.scale(campoCVV, true);

		guiTools.scale(instruccionesContrasena, true);
		guiTools.scale(instruccionesNIP, true);
		guiTools.scale(instruccionesASM, true);
		guiTools.scale(instruccionesCVV, true);

		guiTools.scale(contenedorCampoTarjeta);
		guiTools.scale(tarjeta, true);
		guiTools.scale(campoTarjeta, true);
		guiTools.scale(instruccionesTarjeta, true);
	}
	
	private void findViews(){

		vistaCtaOrigen = (LinearLayout)findViewById(R.id.ofertaILC_cuenta_origen_view);

		txtAcepto = (TextView) findViewById(R.id.txtAceptoTyC);
		txtAutorizo = (TextView) findViewById(R.id.txtDomiciliar);
		txtProtege = (TextView) findViewById(R.id.txtProtege);
		txtSeguro = (TextView) findViewById(R.id.txtSeguro);
		txtTerminos = (TextView) findViewById(R.id.txtTerminos);
		txtDomiciliacionLink = (TextView) findViewById(R.id.txtLinkdomiciliar);
		txtPrima = (TextView)findViewById(R.id.txtPrecio);
		txtavisoLink= (TextView) findViewById(R.id.txtLinkaviso);


		cbAutorizo = (CheckBox) findViewById(R.id.cbDomiciliar);
		cbTerminos = (CheckBox) findViewById(R.id.cbTerminos);

		btnAceptar = (Button)findViewById(R.id.btnAceptar);
		btnAceptar.setOnClickListener(this);
		//imgDer = (ImageView) findViewById(R.id.img_derecha);
		//imgIzq = (ImageView) findViewById(R.id.img_izquierda);
		
		imgFlecha = (ImageView) findViewById(R.id.img_flecha);
        imgFlecha.setOnClickListener(this);
		imgLogo = (ImageView) findViewById(R.id.img_Logo);
		carrousel = (Carrousel)findViewById(R.id.carousel);

		final Carrousel c = carrousel;
        carrousel.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(CarrouselAdapter<?> parent, View view,
									int position, long id) {
				c.setSelection(position);
			}

		});

		txtTerminos.setOnClickListener(this);
		txtDomiciliacionLink.setOnClickListener(this);
		txtavisoLink.setOnClickListener(this);


		headerDetalle = (LinearLayout) findViewById(R.id.frame_encabezadoDetalle);
		headerDetalle.setVisibility(View.VISIBLE);

		/** Token **/
		layout_avisotoken	= (LinearLayout)findViewById(R.id.layout_avisotoken);
		txtaviso_token = (TextView) findViewById(R.id.txtaviso_token);
		imgaviso_token = (ImageButton) findViewById(R.id.imgaviso_token);

		contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_confirmacion_contrasena_layout);
		contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_confirmacion_nip_layout);
		contenedorASM 			= (LinearLayout)findViewById(R.id.campo_confirmacion_asm_layout);
		contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_confirmacion_cvv_layout);

		contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
		nip 					= (EditText)contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
		asm						= (EditText)contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
		cvv						= (EditText)contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);

		campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
		campoNIP				= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_label);
		campoASM				= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_label);
		campoCVV				= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_label);

		instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
		instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
		instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
		instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);

		contenedorCampoTarjeta  = (LinearLayout) findViewById(R.id.campo_confirmacion_campotarjeta_layout);
		tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
		campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
		instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);

		layout_avisotoken.setVisibility(View.GONE);
	}

	public void datosCuentaCargo(){
		delegate.setCuentaCargo( componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getNumber());
		delegate.setTipoCuenta(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getType());
	}
	
	private void init(){
		findViews();
		scaleViews();
		cargarCuentas();

		if(delegate.isDomiciliar()){
			cbAutorizo.setClickable(false);
			cbAutorizo.setChecked(true);
		}

		if(delegate.isTerminos()){
			cbTerminos.setClickable(false);
			cbTerminos.setChecked(true);
		}

		txtPrima.setText("$ " + delegate.getMontoPrima());

		setSecurityComponents();

	}

	public DetalleCarrousel cargarServicios(){
		return delegate.cargarServicios(this);
	}
	
	@Override
	public void onClick(View v) {
		if(v == btnAceptar){
            if(aceptarSeguros())
				datosCuentaCargo();
			    if(delegate.enviaPeticionOperacion()) {
				    init.getParentManager().setActivityChanging(true);
			    }
		}else if(v == txtTerminos){
			init.getParentManager().setActivityChanging(true);
			delegate.realizaOperacionTerminosYCondiciones();
		}else if(v == txtDomiciliacionLink){
			init.getParentManager().setActivityChanging(true);
			delegate.realizaOperacionDomiciliacion();
		}else if(v == imgFlecha)
            onBackPressed();
		else if(v == txtavisoLink){
			init.getBaseViewController().showInformationAlert(this,R.string.texto_avisoprivacidad);

		}

	}

	public boolean aceptarSeguros(){

		if(!cbTerminos.isChecked()){
			init.getBaseViewController().showInformationAlert(this, R.string.aceptar_terminos);
            return false;
		}else if(!cbAutorizo.isChecked()){
			init.getBaseViewController().showInformationAlert(this, R.string.aceptar_domiciliacion);
            return false;
		}/*else {
            delegate.formalizacion();
		}*/
        return true;
	}

	@Override
	public void onBackPressed() {
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (DialogInterface.BUTTON_POSITIVE == which) {
					init.getParentManager().setActivityChanging(true);
					//init.getParentManager().showMenuInicial();
					((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
					init.getParentManager().resetRootViewController();
					init.resetInstance();
				}else{
					dialog.dismiss();
				}

			}
		};

		init.getBaseViewController().showYesNoAlert(this, R.string.salir_seguros, listener);
	}

	@SuppressWarnings("deprecation")
	public void cargarCuentas(){
		ArrayList<HostAccounts> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

		componenteCtaOrigen = new CuentaOrigenController(this, params);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText("");
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
		componenteCtaOrigen.init();
		componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.oneClicSeguros);
		vistaCtaOrigen.addView(componenteCtaOrigen);

	}

	@Override
	public void onUserInteraction() {
		//super.onUserInteraction();
		//init.getParentManager().onUserInteraction();
		TimerController.getInstance().resetTimer();
	}

	@Override
	protected void onPause() {
		super.onPause();
		/*if(!init.getParentManager().isActivityChanging())
		{
			TimerController.getInstance().getSessionCloser().cerrarSesion();
			init.getParentManager().resetRootViewController();
			init.resetInstance();
		}*/

	}

    @Override
    protected void onStop() {
        if (ServerCommons.ALLOW_LOG) {
            Log.e(getClass().getSimpleName(), "OnStop");
        }
        if(ControlEventos.isAppIsInBackground(this.getBaseContext())){ //Valida el estado de la app si es change Activity no hace nada
            if(ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "La app se fue a Background");
            }
            TimerController.getInstance().initTimer(this, TimerController.getInstance().getClase());

        }
        super.onStop();
    }

	public void onCheckboxClick(View v) {


		if (v == cbAutorizo) {
			if(cbTerminos.isChecked()) {
				if (cbAutorizo.isChecked()) {
					cbAutorizo.setClickable(false);
					delegate.setDomiciliar(true);
				}
			}else{
				init.getBaseViewController().showInformationAlert(this, R.string.aceptar_terminos);
				cbAutorizo.setChecked(false);
			}
		} else if (v == cbTerminos) {
				if (cbTerminos.isChecked()) {
					cbTerminos.setClickable(false);
					delegate.setTerminos(true);
				}
		}
	}



	/**   Inherited Methods From Base Code**/



	private void setSecurityComponents() {
		mostrarContrasena(delegate.mostrarContrasenia());
		mostrarNIP(delegate.mostrarNIP());
		mostrarASM(delegate.tokenAMostrar());
		mostrarCVV(delegate.mostrarCVV());
		mostrarCampoTarjeta(delegate.mostrarCampoTarjeta());

		LinearLayout parentContainer = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);

		if (contenedorContrasena.getVisibility() == View.GONE &&
				contenedorNIP.getVisibility() == View.GONE &&
				contenedorASM.getVisibility() == View.GONE &&
				contenedorCVV.getVisibility() == View.GONE &&
				contenedorCampoTarjeta.getVisibility() == View.GONE) {
			parentContainer.setBackgroundColor(0);
			parentContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		}

		android.util.Log.e("dora", "contenedorContrasena.getVisibility() " +contenedorContrasena.getVisibility());
		android.util.Log.e("dora", "contenedorNIP.getVisibility() " +contenedorNIP.getVisibility());
		android.util.Log.e("dora", "contenedorASM.getVisibility() " +contenedorASM.getVisibility());
		android.util.Log.e("dora", "contenedorCVV.getVisibility() " +contenedorCVV.getVisibility());
		android.util.Log.e("dora", "contenedorCampoTarjeta.getVisibility() " +contenedorCampoTarjeta.getVisibility());

		if (contenedorContrasena.getVisibility() == View.VISIBLE ||
				contenedorNIP.getVisibility() == View.VISIBLE ||
				contenedorASM.getVisibility() == View.VISIBLE ||
				contenedorCVV.getVisibility() == View.VISIBLE ||
				contenedorCampoTarjeta.getVisibility() == View.VISIBLE) {
			android.util.Log.e("dora" ,"no debe verse");
			layout_avisotoken.setVisibility(View.VISIBLE);
		}

	}


	public void mostrarCVV(boolean visibility){
		contenedorCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE : View.GONE);
		instruccionesCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);

		if (visibility) {
			campoCVV.setText(delegate.getEtiquetaCampoCVV());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			String instrucciones = delegate.getTextoAyudaCVV();
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}

	public void mostrarContrasena(boolean visibility){
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
		if (visibility) {
			campoContrasena.setText(delegate.getEtiquetaCampoContrasenia());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}


	public void mostrarNIP(boolean visibility){
		contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoNIP.setText(delegate.getEtiquetaCampoNip());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			String instrucciones = delegate.getTextoAyudaNIP();
			if (instrucciones.equals("")) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}


	public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){
		switch(tipoOTP) {
			case ninguno:
				contenedorASM.setVisibility(View.GONE);
				campoASM.setVisibility(View.GONE);
				asm.setVisibility(View.GONE);
				asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
				break;
			case codigo:
			case registro:
				contenedorASM.setVisibility(View.VISIBLE);
				campoASM.setVisibility(View.VISIBLE);
				asm.setVisibility(View.VISIBLE);
				InputFilter[] userFilterArray = new InputFilter[1];
				userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
				asm.setFilters(userFilterArray);
				cambiarAccionTexto(contrasena);
				cambiarAccionTexto(tarjeta);
				cambiarAccionTexto(nip);
				asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
		}

		Constants.TipoInstrumento tipoInstrumento = delegate.getTipoInstrumentoSeguridad();

		switch (tipoInstrumento) {
			case OCRA:
				campoASM.setText(delegate.getEtiquetaCampoOCRA());
				asm.setTransformationMethod(null);
				break;
			case DP270:
				campoASM.setText(delegate.getEtiquetaCampoDP270());
				asm.setTransformationMethod(null);
				break;
			case SoftToken:
				if(init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()) {
					asm.setText(Constants.DUMMY_OTP);
					asm.setEnabled(false);
					campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
				} else {
					asm.setText("");
					asm.setEnabled(true);
					campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
					asm.setTransformationMethod(null);
				}

				break;
			default:
				break;
		}
		String instrucciones = delegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento,(init.getSofTokenStatus() || PropertiesManager.getCurrent().getSofttokenService()),delegate.tokenAMostrar());

		if (instrucciones.equals("")) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}
	}


	private void mostrarCampoTarjeta(boolean visibility) {
		contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		tarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			layout_avisotoken.setVisibility(View.VISIBLE);
			campoTarjeta.setText(delegate.getEtiquetaCampoTarjeta());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(5);
			tarjeta.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
			String instrucciones = delegate.getTextoAyudaTarjeta();
			if (instrucciones.equals("")) {
				instruccionesTarjeta.setVisibility(View.GONE);
			} else {
				instruccionesTarjeta.setVisibility(View.VISIBLE);
				instruccionesTarjeta.setText(instrucciones);
			}
		} else {
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}

	}



	private void cambiarAccionTexto(EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}



	public String pideContrasena() {
		if (contrasena.getVisibility() == View.GONE) {
			return "";
		} else {
			return contrasena.getText().toString();
		}
	}

	public String pideNIP() {
		if (nip.getVisibility() == View.GONE) {
			return "";
		} else {
			return nip.getText().toString();
		}
	}

	public String pideASM() {
		if (asm.getVisibility() == View.GONE) {
			return "";
		} else {
			return asm.getText().toString();
		}
	}

	public String pideCVV() {
		if (cvv.getVisibility() == View.GONE) {
			return "";
		} else {
			return cvv.getText().toString();
		}
	}

	public String pideTarjeta(){
		if (tarjeta.getVisibility() == View.GONE) {
			return "";
		}else
			return tarjeta.getText().toString();
	}


}
