package com.bancomer.oneclickseguros.delegates;

import com.bancomer.oneclickseguros.controllers.ExitoViewController;
import com.bancomer.oneclickseguros.implementations.InitOneClickSeguros;
import com.bancomer.oneclickseguros.models.Formalizacion;

import java.util.Hashtable;

import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;

public class ExitoDelegate implements BaseDelegate{


    public static final long SEGUROS_RESULTADOS_DELEGATE = 10602003105230813L;
    private Formalizacion formalizacion;
    private ExitoViewController controller;
    private InitOneClickSeguros init;

    public void setController(ExitoViewController controller){
        this.controller = controller;
    }

    public ExitoDelegate(Formalizacion formalizacion){
        this.formalizacion = formalizacion;
        init = InitOneClickSeguros.getInstance();
    }

    public void cargarValores(){
        controller.setTxtMonto(formalizacion.getImportePrima());
        controller.setTxtPoliza(formalizacion.getNumeroPoliza());

        if(formalizacion.getEnvioSMS().equalsIgnoreCase("SI")){
            controller.showEmail(init.getSession().getEmail());
        }

        if(formalizacion.getEnvioCorreoElectronico().equalsIgnoreCase("SI")){
            controller.showSms(Tools.hideUsername(init.getSession().getUsername()));
        }
    }


    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {

    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

    }

    @Override
    public void performAction(Object obj) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
