package com.bancomer.oneclickseguros.models;

import com.bancomer.oneclickseguros.commons.ConstantsSeguros;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;


public class DetalleCarrousel implements ParsingHandler {

    private String cuentaSugerida;
    private String producto;
    private String importePrima;
    private ArrayList<Seguro> seguroInfo;
    private ArrayList<Promociones> promociones;
    private Promociones[] arrayPromociones;
    private String pm;

    private DetalleCarrousel seguros;

    public DetalleCarrousel getSeguros(){
        return seguros;
    }

    public String getCuentaSugerida() {
        return cuentaSugerida;
    }

    public void setCuentaSugerida(String cuentaSugerida) {
        this.cuentaSugerida = cuentaSugerida;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getImportePrima() {
        return importePrima;
    }

    public void setImportePrima(String importePrima) {
        this.importePrima = importePrima;
    }

    public ArrayList<Seguro> getSeguroInfo(){
        return seguroInfo;
    }

    public void setSeguroInfo(ArrayList<Seguro> seguroInfo){
        this.seguroInfo = seguroInfo;
    }

    public String getPm() {
        return pm;
    }

    public void setPm(String pm) {
        this.pm = pm;
    }

    public Promociones[] getArrayPromociones() {
        return arrayPromociones;
    }

    public void setArrayPromociones(Promociones[] arrayPromociones) {
        this.arrayPromociones = arrayPromociones;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        android.util.Log.e("dora", "parser detalle: ");

        seguros = new DetalleCarrousel();
        seguroInfo = new ArrayList<Seguro>();
        if(parser.hasValue(ConstantsSeguros.TAG_CUENTA_SUGERIDA))
            seguros.setCuentaSugerida(parser.parseNextValue(ConstantsSeguros.TAG_CUENTA_SUGERIDA));
        if(parser.hasValue(ConstantsSeguros.TAG_PRODUCTO))
            seguros.setProducto(parser.parseNextValue(ConstantsSeguros.TAG_PRODUCTO));
        if(parser.hasValue(ConstantsSeguros.TAG_IMPORTE_PRIMA))
            seguros.setImportePrima((parser.parseNextValue(ConstantsSeguros.TAG_IMPORTE_PRIMA)));
        JSONObject coberturaSeguro = null;
        if(parser.hasValue(ConstantsSeguros.TAG_COBERTURA_SEGURO))
            coberturaSeguro = parser.parserNextObject(ConstantsSeguros.TAG_COBERTURA_SEGURO, false);

        try {
            JSONArray coberturas = null;
            if(parser.hasValue(ConstantsSeguros.TAG_COBERTURA_SEGURO)) {
                coberturas = coberturaSeguro.getJSONArray(ConstantsSeguros.TAG_COBERTURAS);


                for (int i = 0; i < coberturas.length(); i++) {
                    Seguro seguro = new Seguro();
                    JSONObject cobertura = coberturas.getJSONObject(i);
                    seguro.setCoberturaSeguro(cobertura.getString(ConstantsSeguros.TAG_DESCOBERTURA));
                    seguro.setImporteSumaAsegurada((com.bancomer.oneclickseguros.commons.Tools.formatPositiveIntegerAmount(cobertura.getString(ConstantsSeguros.TAG_IMPORTE_SUMA_ASEGURADA))));
                    seguroInfo.add(seguro);
                }
                seguros.setSeguroInfo(seguroInfo);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(parser.hasValue(ConstantsSeguros.TAG_PM)) {
            pm = parser.parseNextValue(ConstantsSeguros.TAG_PM);
            seguros.setPm(pm);

            if (pm.equalsIgnoreCase("SI")) {
                JSONObject lp = parser.parserNextObject(ConstantsSeguros.TAG_LP);
                try {
                    JSONArray campaniasJSONArray = lp.getJSONArray(ConstantsSeguros.TAG_CAMPANIAS);

                    promociones = new ArrayList<Promociones>();

                    for (int i = 0; i < campaniasJSONArray.length(); i++) {
                        JSONObject campaniaObject;
                        Promociones promocion = new Promociones();
                        campaniaObject = campaniasJSONArray.getJSONObject(i);
                        promocion.setCveCamp(campaniaObject.getString(ConstantsSeguros.TAG_CVE_CAMP));
                        promocion.setDesOferta(campaniaObject.getString(ConstantsSeguros.TAG_DESOFERTA));
                        promocion.setMonto(campaniaObject.getString(ConstantsSeguros.TAG_MONTO));
                        promocion.setCarrusel(campaniaObject.getString(ConstantsSeguros.TAG_CARRUSEL));
                        promociones.add(promocion);

                    }

                    arrayPromociones = new Promociones[promociones.size()];

                    for (int i = 0; i < promociones.size(); i++) {
                        arrayPromociones[i] = promociones.get(i);
                    }
                    seguros.setArrayPromociones(arrayPromociones);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
