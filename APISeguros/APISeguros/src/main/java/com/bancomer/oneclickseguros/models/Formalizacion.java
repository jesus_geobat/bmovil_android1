package com.bancomer.oneclickseguros.models;

import com.bancomer.oneclickseguros.commons.ConstantsSeguros;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by DMEJIA on 01/10/15.
 */
public class Formalizacion implements ParsingHandler {

    private String producto;
    private String fechaAlta;
    private String fechaVencimiento;
    private String importePrima;
    private String numeroPoliza;
    private String envioCorreoElectronico;
    private String envioSMS;
    private String pm;
    private ArrayList<Promociones> promociones;
    private Promociones[] arrayPromociones;

    public Formalizacion(){
        promociones = null;
        arrayPromociones = null;
    }

    public String getProducto() {
        return producto;
    }

    public String getFechaAlta() {
        return fechaAlta;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public String getImportePrima() {
        return importePrima;
    }

    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    public String getEnvioCorreoElectronico() {
        return envioCorreoElectronico;
    }

    public String getEnvioSMS() {
        return envioSMS;
    }

    public String getPm() {
        return pm;
    }


    public Promociones[] getArrayPromociones() {
        return arrayPromociones;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        android.util.Log.e("dora", "parser formalizacion: ");

        if(parser.hasValue(ConstantsSeguros.TAG_PRODUCTO))
            producto = parser.parseNextValue(ConstantsSeguros.TAG_PRODUCTO);
        if(parser.hasValue(ConstantsSeguros.TAG_FECHA_ALTA))
            fechaAlta = parser.parseNextValue(ConstantsSeguros.TAG_FECHA_ALTA);
        if(parser.hasValue(ConstantsSeguros.TAG_FECHA_VENCIMIENTO))
            fechaVencimiento = parser.parseNextValue(ConstantsSeguros.TAG_FECHA_VENCIMIENTO);
        if(parser.hasValue(ConstantsSeguros.TAG_IMPORTE_PRIMA))
            importePrima = parser.parseNextValue(ConstantsSeguros.TAG_IMPORTE_PRIMA);
        if(parser.hasValue(ConstantsSeguros.TAG_NUMERO_POLIZA))
            numeroPoliza = parser.parseNextValue(ConstantsSeguros.TAG_NUMERO_POLIZA);
        if(parser.hasValue(ConstantsSeguros.TAG_ENVIO_CORREO_ELECTRONICO))
            envioCorreoElectronico = parser.parseNextValue(ConstantsSeguros.TAG_ENVIO_CORREO_ELECTRONICO);
        if(parser.hasValue(ConstantsSeguros.TAG_ENVIO_SMS))
            envioSMS = parser.parseNextValue(ConstantsSeguros.TAG_ENVIO_SMS);
        if(parser.hasValue(ConstantsSeguros.TAG_PM)) {
            pm = parser.parseNextValue(ConstantsSeguros.TAG_PM);

            if (pm.equalsIgnoreCase("SI")) {

                JSONObject lp = parser.parserNextObject(ConstantsSeguros.TAG_LP);
                try {
                    JSONArray campaniasJSONArray = lp.getJSONArray(ConstantsSeguros.TAG_CAMPANIAS);

                    promociones = new ArrayList<Promociones>();

                    for (int i = 0; i < campaniasJSONArray.length(); i++) {
                        JSONObject campaniaObject;
                        Promociones promocion = new Promociones();
                        campaniaObject = campaniasJSONArray.getJSONObject(i);
                        promocion.setCveCamp(campaniaObject.getString(ConstantsSeguros.TAG_CVE_CAMP));
                        promocion.setDesOferta(campaniaObject.getString(ConstantsSeguros.TAG_DESOFERTA));
                        promocion.setMonto(campaniaObject.getString(ConstantsSeguros.TAG_MONTO));
                        promocion.setCarrusel(campaniaObject.getString(ConstantsSeguros.TAG_CARRUSEL));
                        promociones.add(promocion);
                    }

                    arrayPromociones = new Promociones[promociones.size()];

                    for (int i = 0; i < promociones.size(); i++) {
                        arrayPromociones[i] = promociones.get(i);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
