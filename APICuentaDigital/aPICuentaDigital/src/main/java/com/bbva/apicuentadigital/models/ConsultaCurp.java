package com.bbva.apicuentadigital.models;

/**
 * Created by Karina on 07/12/2015.
 */
public class ConsultaCurp {

    private String code;
    private String description;
    private String curpValido;
    private String nombres;
    private String aPaterno;
    private String aMaterno;
    private String sexo;
    private String fechNac;
    private String cveEntidadNac;
    private String curp;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurpValido() {
        return curpValido;
    }

    public void setCurpValido(String curpValido) {
        this.curpValido = curpValido;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getaPaterno() {
        return aPaterno;
    }

    public void setaPaterno(String aPaterno) {
        this.aPaterno = aPaterno;
    }

    public String getaMaterno() {
        return aMaterno;
    }

    public void setaMaterno(String aMaterno) {
        this.aMaterno = aMaterno;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getFechNac() {
        return fechNac;
    }

    public void setFechNac(String fechNac) {
        this.fechNac = fechNac;
    }

    public String getCveEntidadNac() {
        return cveEntidadNac;
    }

    public void setCveEntidadNac(String cveEntidadNac) {
        this.cveEntidadNac = cveEntidadNac;
    }

    public void setCurp(String aCurp){
        this.curp = aCurp;
    }

    public String getCurp(){
        return curp;
    }


}
