package com.bbva.apicuentadigital.controllers;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.APICuentaDigital;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.delegates.ClausulasDelegate;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import tracking.TrackingHelperCuentaDigital;

/**
 * Created by Karina on 07/12/2015.
 */
public class ClausulasViewController extends Fragment implements View.OnClickListener {

    private View rootView;

    private CheckBox cbTerms;
    private CheckBox cbTermsThree;
    private CheckBox cbTermsFour;

    private EditText edtPass;
    private EditText edtPassConfirm;

    private ImageButton imbContinue;

    private TextView lbl_accept_terms;
    private TextView lbl_term_three;
    //private TextView lbl_term_four;
    private TextView lbl_alerta_no_medios;

    private GuiTools guiTools;
    private LinearLayout ly_alerta_no_medios;

    private CreaCuentaDelegate creaCuentaDelegate;
    private ClausulasDelegate clausulasDelegate;

    private boolean okPass;
    private boolean okPassConfirm;
    private APICuentaDigitalSharePreferences sharePreferences; //MAPE1

    public ClausulasViewController(GuiTools guiTools, CreaCuentaDelegate creaCuentaDelegate) {
        this.guiTools = guiTools;
        this.creaCuentaDelegate = creaCuentaDelegate;
        clausulasDelegate = new ClausulasDelegate(this, creaCuentaDelegate);
        okPass = false;
        okPassConfirm = false;
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();
        Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
        alert.putExtra(Constants.MENSAJE, creaCuentaDelegate.getCreaCuentaView().getString(R.string.alert_clausulas));
        creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);//MAPE1
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_clausulas, container, false);

        init();
        return rootView;
    }

    private void init() {
        findViewById();
        clickListener();
        scaleView();
        etiquetado();
    }

    private void etiquetado() {
        ArrayList<String> list = new ArrayList<String>();
        list.add(Constants.QUIERO_CUENTA);
        list.add(Constants.CREA_CUENTA);
        list.add(Constants.CLAUSULAS_CUENTA);
        TrackingHelperCuentaDigital.trackState(list);
        Map<String, Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackingHelperCuentaDigital.trackEntrada(eventoEntrada);
        //TrackingHelper.trackState("clausulas", new ArrayList<String>());
    }

    private void findViewById() {
        cbTerms = (CheckBox) rootView.findViewById(R.id.cb_terms);
        cbTermsThree = (CheckBox) rootView.findViewById(R.id.cb_terms_three);
        //cbTermsFour = (CheckBox) rootView.findViewById(R.id.cb_terms_four);

        edtPass = (EditText) rootView.findViewById(R.id.edt_pass);
        edtPassConfirm = (EditText) rootView.findViewById(R.id.edt_pass_confirm);
        imbContinue = (ImageButton) rootView.findViewById(R.id.imb_continue);


        lbl_accept_terms = (TextView) rootView.findViewById(R.id.lbl_accept_terms);
        lbl_term_three = (TextView) rootView.findViewById(R.id.lbl_term_three);
        //lbl_term_four = (TextView) rootView.findViewById(R.id.lbl_term_four);
        lbl_alerta_no_medios = (TextView) rootView.findViewById(R.id.txt_no_medios_electronicos);
        ly_alerta_no_medios = (LinearLayout) rootView.findViewById(R.id.ly_aviso_no_medios_electronicos);
        /*String text = "";
        text = lbl_accept_terms.getText().toString();
        SpannableString text1 = new SpannableString(text);
        text1.setSpan(new StyleSpan(Typeface.BOLD), 22, 44, 0);
        text1.setSpan(new UnderlineSpan(), 22, 44, 0);
        text1.setSpan(new ForegroundColorSpan(Color.rgb(135, 200, 46)), text.length() - 13, text.length(), 0);
        lbl_accept_terms.setText(text1);*/

        String text = "";
        text = lbl_accept_terms.getText().toString();
        SpannableString text1 = new SpannableString(text);
        text1.setSpan(new StyleSpan(Typeface.BOLD), 22, 44, 0);
        text1.setSpan(new UnderlineSpan(), 22, 44, 0);
        text1.setSpan(new ForegroundColorSpan(Color.rgb(135, 200, 46)), text.length() - 21, text.length(), 0);
        lbl_accept_terms.setText(text1);

        text = lbl_term_three.getText().toString();
        SpannableString text2 = new SpannableString(text);
        text2.setSpan(new StyleSpan(Typeface.BOLD), 22, 44, 0);
        text2.setSpan(new UnderlineSpan(), 22, 44, 0);
        text2.setSpan(new ForegroundColorSpan(Color.rgb(135, 200, 46)), text.length() - 17, text.length(), 0);
        lbl_term_three.setText(text2);

        /*text = lbl_term_four.getText().toString();
        SpannableString text3 = new SpannableString(text);
        text3.setSpan(new StyleSpan(Typeface.BOLD), 22, 44, 0);
        text3.setSpan(new UnderlineSpan(), 22, 44, 0);
        text3.setSpan(new ForegroundColorSpan(Color.rgb(135, 200, 46)), text.length() - 14, text.length(), 0);
        lbl_term_four.setText(text3);*/

    }

    private void clickListener() {
        cbTerms.setOnClickListener(this);
        cbTermsThree.setOnClickListener(this);
        //cbTermsFour.setOnClickListener(this);

        imbContinue.setOnClickListener(this);

        lbl_accept_terms.setOnClickListener(this);
        lbl_term_three.setOnClickListener(this);
        //lbl_term_four.setOnClickListener(this);

        listenerPass();
        listenerPassConfirm();
    }

    private void listenerPass() {
        edtPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 6) {
                    if (clausulasDelegate.validatePasswordPolicy1(edtPass.getText().toString().trim())) {
                        if (clausulasDelegate.validatePasswordPolicy2(edtPass.getText().toString().trim())) {
                            okPass = true;
                            if (edtPass.getText().toString().equals(edtPassConfirm.getText().toString())) {
                                okPassConfirm = true;
                            } else {
                                okPassConfirm = false;
                            }
                        } else {
                            Intent alert = new Intent(getActivity(), PopUpGeneral.class);
                            alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.contrata_message_pass_error_numeros_repetidos));
                            getActivity().startActivityForResult(alert, 1234);

                        }
                    } else {
                        Intent alert = new Intent(getActivity(), PopUpGeneral.class);
                        alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.contrata_message_pass_error_numeros_consecutivos));
                        getActivity().startActivityForResult(alert, 1234);
                    }
                } else {
                    if (okPass) {
                        okPass = false;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void listenerPassConfirm() {
        edtPassConfirm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 6) {
                    if (edtPass.getText().toString().equals(edtPassConfirm.getText().toString())) {
                        okPassConfirm = true;
                    } else {
                        Intent alert = new Intent();
                        alert.setClass(getActivity(), PopUpGeneral.class);
                        alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.contrasena_no_coincide));
                        getActivity().startActivityForResult(alert, 1234);
                        edtPassConfirm.setText(Constants.EMPTY_STRING);
                    }
                } else {
                    okPassConfirm = false;
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void scaleView() {
        guiTools.scale(rootView.findViewById(R.id.lbl_term_three), true);
        //guiTools.scale(rootView.findViewById(R.id.lbl_term_four), true);
        guiTools.scale(rootView.findViewById(R.id.txt_contrata), true);
        guiTools.scale(rootView.findViewById(R.id.txt_cuenta_contrata), true);
        guiTools.scale(edtPass);
        guiTools.scale(edtPassConfirm);
        guiTools.scale(lbl_accept_terms);
        guiTools.scale(rootView.findViewById(R.id.txt_no_medios_electronicos),true);
    }

    @Override
    public void onClick(View v) {

        if (v == imbContinue) {
            validatePass();
        } else if (cbTermsThree == v) {
            if (!cbTerms.isChecked()) {
                cbTermsThree.setChecked(false);
                Intent alert = new Intent(getActivity(), PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.contrata_alerta_no_datos));
                getActivity().startActivityForResult(alert, 1234);
            } else{
                cbTermsThree.setChecked(true);
                lbl_alerta_no_medios.setVisibility(View.GONE);
                ly_alerta_no_medios.setVisibility(View.GONE);
            }
        } else if (cbTermsFour == v) {
            if (!cbTermsThree.isChecked()) {
                cbTermsFour.setChecked(false);
                Intent alert = new Intent(getActivity(), PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.contrata_alerta_no_vincular));
                getActivity().startActivityForResult(alert, 1234);
            } else {
                cbTermsFour.setChecked(true);
            }
        } else if (lbl_accept_terms == v) {
            clausulasDelegate.showTerminos("cuenta");
        } else if (cbTerms == v) {
            if (cbTerms.isChecked()) {
                lbl_alerta_no_medios.setVisibility(View.VISIBLE);
                ly_alerta_no_medios.setVisibility(View.VISIBLE);
                cbTerms.setChecked(true);
            } else
                cbTerms.setChecked(true);
        } else if (lbl_term_three == v) {
            clausulasDelegate.showTerminos("alertas");
        } /*else if (lbl_term_four == v) {
            clausulasDelegate.showTerminos("canal");
        }*/
    }

    private void validatePass() {
        if (cbTerms.isChecked()/* && cbTermsTwo.isChecked() */ && cbTermsThree.isChecked() /*&& cbTermsFour.isChecked()*/) {
            if (okPass) {
                if (okPassConfirm) {
                    suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(getActivity()).saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CONTRATAR_CUENTA_DIGITAL, true, true);
                    sharePreferences.setStringData(Constants.TAG_ACCESSCODE, edtPassConfirm.getText().toString()); //MAPE1
                    KeyStoreWrapper.getInstance(APICuentaDigital.appContext).setUserName(APICuentaDigitalSharePreferences.getInstance().getUser());
                    clausulasDelegate.showConfirmacion();

                } else if (edtPassConfirm.getText().toString().equals("")) {

                    Intent alert = new Intent(getActivity(), PopUpGeneral.class);
                    alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.contrasena_confirmacion_vacia));
                    getActivity().startActivityForResult(alert, 1234);
                } else if (!edtPassConfirm.getText().toString().equals(edtPass.getText().toString())) {
                    //creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrasena_no_coincide);
                    Intent alert = new Intent();
                    alert.setClass(getActivity(), PopUpGeneral.class);
                    alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.contrasena_no_coincide));
                    getActivity().startActivityForResult(alert, 1234);
                }

            } else if (edtPass.getText().toString().equals("") || edtPass.getText().toString() == null) {
                //creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrasena_vacia);
                Intent alert = new Intent(getActivity(), PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.contrasena_vacia));
                getActivity().startActivityForResult(alert, 1234);
            } else if (edtPass.getText().toString().length() < 6) {
                //creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrasena_menor);
                Intent alert = new Intent(getActivity(), PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.contrasena_menor));
                getActivity().startActivityForResult(alert, 1234);
            }
        } else {
            Intent alert = new Intent(getActivity(), PopUpGeneral.class);
            alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.contrata_alerta_clausulas));
            getActivity().startActivityForResult(alert, 1234);
        }


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        setUserVisibleHint(true);
    }


}
