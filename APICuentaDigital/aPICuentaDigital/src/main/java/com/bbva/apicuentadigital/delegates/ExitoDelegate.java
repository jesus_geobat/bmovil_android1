package com.bbva.apicuentadigital.delegates;

import android.content.Intent;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.controllers.ExitoViewController;
import com.bbva.apicuentadigital.controllers.PopUpGeneral;
import com.bbva.apicuentadigital.controllers.WebViewController;
import com.bbva.apicuentadigital.io.ConstantsServer;
import com.bbva.apicuentadigital.io.Server;
import com.bbva.apicuentadigital.io.ServerResponse;
import com.bbva.apicuentadigital.models.ContratoHTML;
import com.bbva.apicuentadigital.models.ValidaOTP;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

import java.util.Hashtable;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;

/**
 * Created by Karina on 09/12/2015.
 */
public class ExitoDelegate extends BaseDelegate{

    private CreaCuentaDelegate creaCuentaDelegate;
    private ExitoViewController exitoViewController;
    private APICuentaDigitalSharePreferences sharePreferences;

    public ExitoDelegate(CreaCuentaDelegate creaCuentaDelegate, ExitoViewController exitoViewController){
        this.creaCuentaDelegate = creaCuentaDelegate;
        this.exitoViewController = exitoViewController;
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();
    }

    public void realizaOperacion(){
        creaCuentaDelegate.getCreaCuentaView().showActivityIndicator();

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(Constants.TAG_NUMUERO_CELULAR,"5515024046");
        params.put(Constants.TAG_ID_PRODUCTO,"PBXXXXXX01");
        params.put(Constants.TAG_IND_VACIO,Constants.TAG_FALSE);
        params.put(Constants.TAG_CONTRATO,"PBXXXXXX01");

        Server server = Server.getInstance(this);
        server.doNetWorkOperation(ApiConstants.CONSULTA_CONTRATO, params);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        creaCuentaDelegate.getCreaCuentaView().hideActivityIndicator();
       if(response.getCODE()!=null) {
           if (response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
               ContratoHTML contratoHTML = (ContratoHTML) response.getResponse();
               showTerminos(contratoHTML.getFormatoHTML());
           } else {
               int asFinal;
               asFinal = response.getMESSAGE().indexOf("**");
               if (asFinal > -1) {
                   //mesagge= response.getMESSAGE().substring(0,asFinal);
                   Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
                   alert.putExtra(Constants.MENSAJE, response.getMESSAGE().substring(0, asFinal));
                   creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);

               } else {
                   //mesagge= response.getMESSAGE();
                   Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
                   alert.putExtra(Constants.MENSAJE, response.getMESSAGE());
                   creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
               }
           }
       }else{
           Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
           alert.putExtra(Constants.MENSAJE,"Error de comunicaciones");
           creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
       }
    }

    public void obtenerDatos(){
        //ValidaOTP validaOTP = creaCuentaDelegate.getValidaOTP();
        String cuentaClabe="";
        if(sharePreferences.getStringData(Constants.TAG_CUENTA_CLABE).equalsIgnoreCase("null"))
            cuentaClabe= "No disponible";
        else
            cuentaClabe=sharePreferences.getStringData(Constants.TAG_CUENTA_CLABE);

        exitoViewController.mostarDatos(sharePreferences.getStringData(Constants.TAG_TITULAR), sharePreferences.getStringData(Constants.TAG_CUENTA), sharePreferences.getStringData(Constants.TAG_TARJETA), cuentaClabe, sharePreferences.getStringData(Constants.CELULAR), sharePreferences.getStringData(Constants.TAG_MAIL));
    }

    public boolean isCanal(){
        //ValidaOTP validaOTP = creaCuentaDelegate.getValidaOTP();

        return sharePreferences.getBooleanData(Constants.TAG_CONTRATACION_CANAL);
    }

    public void showTerminos(String url){
        Intent intent = new Intent(exitoViewController.getActivity(), WebViewController.class);
        intent.putExtra(Constants.OPERATION, Constants.TERMINOS);
        intent.putExtra(Constants.URL, url);
        exitoViewController.getActivity().startActivity(intent);
    }

    public void dataKeyChain(){
        /*
        KeyStoreWrapper.getInstance(APICuentaDigital.appContext).setSeed(String.valueOf(APICuentaDigitalSharePreferences.getInstance().getSeed()));
        KeyStoreWrapper.getInstance(APICuentaDigital.appContext).setUserName(APICuentaDigitalSharePreferences.getInstance().getUser());
        android.util.Log.e("Key", "seed" + KeyStoreWrapper.getInstance(APICuentaDigital.appContext).getSeed());
        android.util.Log.e("Key", "user" + KeyStoreWrapper.getInstance(APICuentaDigital.appContext).getUserName());
        */
    }
}
