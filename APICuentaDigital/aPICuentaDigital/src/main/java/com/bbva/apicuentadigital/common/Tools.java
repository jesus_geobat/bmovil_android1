package com.bbva.apicuentadigital.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.bancomer.base.SuiteApp;
import com.bbva.apicuentadigital.encoder.Digest;
import com.bbva.apicuentadigital.encoder.Hex;
import com.bbva.apicuentadigital.encoder.MD5Digest;
import com.bbva.apicuentadigital.io.ConstantsServer;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

/**
 * Created by Karina on 15/12/2015.
 */
public class Tools {

    public static String[] getCompanias(){
        String [] companies = {"Selecciona","TELCEL", "MOVISTAR", "UNEFON","IUSACELL", "NEXTEL", "VIRGIN MOBILE"};
        return companies;
    }

    public static int getPositionCompany(String company){
        String [] companies = getCompanias();
        int i = 0;
        for(String e:companies){
            if(e.equalsIgnoreCase(company)){
                return i;
            }
            i++;
        }
        return i;
    }

    public static String fortmatFecha(String fecha){
        return fecha.replace("-","/");
    }

    public static String fortmatFechaPeticion(String fecha){
        return fecha.replace("/","-");
    }

    public static String[] getLugarN(){
        String[]lugares = {"Selecciona", Constants.AS, Constants.BC, Constants.BS, Constants.CC, Constants.CL, Constants.CM, Constants.CS, Constants.CH, Constants.DF,
                Constants.DG, Constants.GT, Constants.GR, Constants.HG, Constants.JC, Constants.MC, Constants.MN, Constants.MS,
                Constants.NT, Constants.NL, Constants.OC,Constants.PL, Constants.QT, Constants.QR, Constants.SP, Constants.SL,
                Constants.SR, Constants.TC, Constants.TS, Constants.TL, Constants.VZ, Constants.YO, Constants.ZS, Constants.NE};
        return lugares;
    }

    public static String getEstado(String clave){
        String estado = "";
        switch (clave){
            case "AG":
                estado = Constants.AS;
                break;
            case "BN":
                estado = Constants.BC;
                break;
            case "BS":
                estado = Constants.BS;
                break;
            case "CA":
                estado = Constants.CC;
                break;
            case "CU":
                estado = Constants.CL;
                break;
            case "CO":
                estado = Constants.CM;
                break;
            case "CS":
                estado = Constants.CS;
                break;
            case "CH":
                estado = Constants.CH;
                break;
            case "DF":
                estado = Constants.DF;
                break;
            case "DU":
                estado = Constants.DG;
                break;
            case "GU":
                estado = Constants.GT;
                break;
            case "GO":
                estado = Constants.GR;
                break;
            case "HI":
                estado = Constants.HG;
                break;
            case "JA":
                estado = Constants.JC;
                break;
            case "EM":
                estado = Constants.MC;
                break;
            case "MI":
                estado = Constants.MN;
                break;
            case "MO":
                estado = Constants.MS;
                break;
            case "NA":
                estado = Constants.NT;
                break;
            case "NL":
                estado = Constants.NL;
                break;
            case "OA":
                estado = Constants.OC;
                break;
            case "PU":
                estado = Constants.PL;
                break;
            case "QU":
                estado = Constants.QT;
                break;
            case "QR":
                estado = Constants.QR;
                break;
            case "SL":
                estado = Constants.SP;
                break;
            case "SI":
                estado = Constants.SL;
                break;
            case "SO":
                estado = Constants.SR;
                break;
            case "TA":
                estado = Constants.TC;
                break;
            case "TM":
                estado = Constants.TS;
                break;
            case "TL":
                estado = Constants.TL;
                break;
            case "VE":
                estado = Constants.VZ;
                break;
            case "YU":
                estado = Constants.YO;
                break;
            case "ZA":
                estado = Constants.ZS;
                break;
            case "NE":
                estado = Constants.NE;
                break;
        }

        return estado;
    }

    public static String cveEstado(String estado){
        switch (estado){
            case Constants.AS:
                estado = "AS";
                break;
            case Constants.BC:
                estado = "BC";
                break;
            case Constants.BS:
                estado = "BS";
                break;
            case Constants.CC:
                estado = "CC";
                break;
            case Constants.CL:
                estado = "CL";
                break;
            case Constants.CM:
                estado = "CM";
                break;
            case Constants.CS:
                estado = "CS";
                break;
            case Constants.CH:
                estado = "CH";
                break;
            case Constants.DF:
                estado = "DF";
                break;
            case Constants.DG:
                estado = "DG";
                break;
            case Constants.GT:
                estado = "GT";
                break;
            case Constants.GR:
                estado = "GR";
                break;
            case Constants.HG:
                estado = "HG";
                break;
            case Constants.JC:
                estado = "JC";
                break;
            case Constants.MC:
                estado = "MC";
                break;
            case Constants.MN:
                estado = "MN";
                break;
            case Constants.MS:
                estado = "MS";
                break;
            case Constants.NT:
                estado = "NT";
                break;
            case Constants.NL:
                estado = "NL";
                break;
            case Constants.OC:
                estado = "OC";
                break;
            case Constants.PL:
                estado = "PL";
                break;
            case Constants.QT:
                estado = "QT";
                break;
            case Constants.QR:
                estado = "QR";
                break;
            case Constants.SP:
                estado = "SP";
                break;
            case Constants.SL:
                estado = "SL";
                break;
            case Constants.SR:
                estado = "SR";
                break;
            case Constants.TC:
                estado = "TC";
                break;
            case Constants.TS:
                estado = "TS";
                break;
            case Constants.TL:
                estado = "TL";
                break;
            case Constants.VZ:
                estado = "VZ";
                break;
            case Constants.YO:
                estado = "YN";
                break;
            case Constants.ZS:
                estado = "ZS";
                break;
            case Constants.NE:
                estado = "NE";
                break;
        }

        return estado;
    }

    public static String getCveSexo(String genero){

        switch (genero){
            case "Femenino":
                genero = "M";
                break;
            case "Masculino":
                genero = "H";
                break;
        }
        return genero;
    }

    public static String buildIUM(String username) {
        Context applicationContext = APICuentaDigital.appContext;
        APICuentaDigitalSharePreferences sharePreferences = APICuentaDigitalSharePreferences.getInstance();

        long seed = sharePreferences.getLongData(Constants.SEED);
        if(ConstantsServer.ALLOW_LOG)android.util.Log.e("Dora", "sedd: " + seed);
        if( seed == 0){
            seed = System.currentTimeMillis();
            sharePreferences.setLongData(Constants.SEED, seed);
        }

        StringBuffer sb;

        if(username != null)
            sb = new StringBuffer(username);
        else
            sb = new StringBuffer();

        sb.append(seed);
        String imei = getImei(applicationContext);
        if (imei != null) {
            sb.append(imei);
        }
        String imsi = getImsi(applicationContext);
        if (imsi != null) {
            sb.append(imsi);
        }

        String props = getSystemProperties();
        if (props != null) {
            sb.append(props);
        }

        String input = sb.toString();

        Digest digest = new MD5Digest();
        byte[]  resBuf = new byte[digest.getDigestSize()];
        byte[]  bytes = input.getBytes();
        digest.update(bytes, 0, bytes.length);
        digest.doFinal(resBuf, 0);

        String output = new String(Hex.encode(resBuf)).toUpperCase();

        return output;

    }


    // TODO: PENDING, Must check in real device -- folvera
    /**
     * Try to obtain the IMEI from the telephone.
     * @param applicationContext application context to retrieve phone data
     * @return the IMEI from the telephone, or null if it cannot be obtained
     */
    private static String getImei(Context applicationContext) {
        TelephonyManager telephonyManager =
                (TelephonyManager) applicationContext.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = telephonyManager.getDeviceId();
        if (TextUtils.isEmpty(imei)) {
            imei = "";
        }
        return imei;
    }

    // TODO: PENDING, Must check in real device -- folvera
    /**
     * Try to obtain the IMSI from the telephone SIM card.
     * @param applicationContext application context to retrieve phone data
     * @return the IMSI from the telephone SIM card, or null if it cannot be obtained
     */
    private static String getImsi(Context applicationContext)  {
        TelephonyManager telephonyManager =
                (TelephonyManager) applicationContext.getSystemService(Context.TELEPHONY_SERVICE);
        String imsi = telephonyManager.getSubscriberId();
        if (imsi != null) {
            return "misdn:" + imsi;
        } else {
            return null;
        }
    }

    /**
     * .
     * @return SOMETHING.
     */
    private static String getSystemProperties() {
        StringBuffer result = new StringBuffer();
        try {
            result.append(System.getProperty("java.runtime.name"));
        } catch (Throwable th) { };
        try {
            result.append(System.getProperty("os.version"));
        } catch (Throwable th) { };
        try {
            result.append(System.getProperty("java.vm.name"));
        } catch (Throwable th) { };
        try {
            result.append(System.getProperty("java.runtime.version"));
        } catch (Throwable th) { };
        try {
            result.append(System.getProperty("java.vm.version"));
        } catch (Throwable th) { };


        // System.out.println("System Properties = " + result.toString());

        return result.toString();
    }

    /**
     * Determines if a string is empty or null
     * @param text the string object to evaluate
     * @return true if the string object has no real value
     */
    public static boolean isEmptyOrNull(String text){
        boolean b = text==null || text.trim().length() == 0;
        return text==null || text.trim().length() == 0;
    }

    public static String putFirstSlash(String date) {
        String day = date.substring(0, 2);
        return day + "/" + date.substring(2, date.length());
    }

    public static String removeFirstSlash(String date) {
        return date.substring(0, 2);
    }

    public static String putSecondSlash(String date) {
        String day = date.substring(0, 5);
        return day + "/" + date.substring(5, date.length());
    }

    public static String removeSecondSlash(String date) {
        return date.substring(0, 5);
    }

    public static String getName(){
        APICuentaDigitalSharePreferences preferences = APICuentaDigitalSharePreferences.getInstance();
        return preferences.getStringData(Constants.TAG_NOMBRE) + Constants.SPACING_STRING + preferences.getStringData(Constants.TAG_APATERNO) + Constants.SPACING_STRING +preferences.getStringData(Constants.TAG_AMATERNO);
    }

    public static String cveEstadoEntidad(String estado){
        switch (estado){
            case Constants.AS:
                estado = "AG";
                break;
            case Constants.BC:
                estado = "BN";
                break;
            case Constants.BS:
                estado = "BS";
                break;
            case Constants.CC:
                estado = "CA";
                break;
            case Constants.CL:
                estado = "CU";
                break;
            case Constants.CM:
                estado = "CO";
                break;
            case Constants.CS:
                estado = "CS";
                break;
            case Constants.CH:
                estado = "CH";
                break;
            case Constants.DF:
                estado = "DF";
                break;
            case Constants.DG:
                estado = "DU";
                break;
            case Constants.GT:
                estado = "GU";
                break;
            case Constants.GR:
                estado = "GO";
                break;
            case Constants.HG:
                estado = "HI";
                break;
            case Constants.JC:
                estado = "JA";
                break;
            case Constants.MC:
                estado = "EM";
                break;
            case Constants.MN:
                estado = "MI";
                break;
            case Constants.MS:
                estado = "MO";
                break;
            case Constants.NT:
                estado = "NA";
                break;
            case Constants.NL:
                estado = "NL";
                break;
            case Constants.OC:
                estado = "OA";
                break;
            case Constants.PL:
                estado = "PU";
                break;
            case Constants.QT:
                estado = "QU";
                break;
            case Constants.QR:
                estado = "QR";
                break;
            case Constants.SP:
                estado = "SL";
                break;
            case Constants.SL:
                estado = "SI";
                break;
            case Constants.SR:
                estado = "SO";
                break;
            case Constants.TC:
                estado = "TA";
                break;
            case Constants.TS:
                estado = "TM";
                break;
            case Constants.TL:
                estado = "TL";
                break;
            case Constants.VZ:
                estado = "VE";
                break;
            case Constants.YO:
                estado = "YU";
                break;
            case Constants.ZS:
                estado = "ZA";
                break;
            case Constants.NE:
                estado = "NE";
                break;
        }

        return estado;
    }

    /**
     * Método para verificar si el dispositivo cuenta con conexión inalambrica.
     * @return
     */
    public static boolean isDisconnected(){
        SuiteApp suiteApp = SuiteApp.getInstance();
        ConnectivityManager connectivity = (ConnectivityManager)
                suiteApp.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null){
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null){
                for (int i = 0; i < info.length; i++){
                    if (info[i].getState() == NetworkInfo.State.CONNECTED){
                        return false;
                    }
                }
            }
        }
        return true;
    }



}