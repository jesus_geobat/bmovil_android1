package com.bbva.apicuentadigital.models;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.io.ParserJSON;
import com.bbva.apicuentadigital.io.ParsingException;
import com.bbva.apicuentadigital.io.ParsingHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by erodriguez on 06/09/16.
 */
public class Signing implements ParsingHandler {

    public int code = 0;
    public String msg = "";
    public String errorCode = "";

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {


        try {
            JSONObject json = parser.parserNextObject(Constants.TAG_RESPONSE);

            if(json.length() > 0) {
                code = 200;
                msg = json.getString("status");
            }
//                code = Integer.valueOf(parser.parseNextValue("status"));
//                msg = parser.parseNextValue("description");
//                errorCode = parser.parseNextValue("errorCode");
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
