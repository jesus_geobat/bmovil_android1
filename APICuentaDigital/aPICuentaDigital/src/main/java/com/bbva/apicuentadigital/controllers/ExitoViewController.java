package com.bbva.apicuentadigital.controllers;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bancomer.mbanking.softtoken.SuiteAppApi;
import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.delegates.ExitoDelegate;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.ReturnAlertCuentaDigital;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import tracking.TrackingHelperCuentaDigital;

/**
 * Created by Karina on 09/12/2015.
 */
public class ExitoViewController extends Fragment implements View.OnClickListener, ReturnAlertCuentaDigital {


    private GuiTools guiTools;
    private CreaCuentaDelegate creaCuentaDelegate;
    private ExitoDelegate exitoDelegate;
    private View rootView;
    private EditText clave;
    private ImageButton imbSalir;
    private TextView txtContrato;
    private TextView txtTitular;
    private TextView txtCuenta;
    private TextView txtTarjeta;
    private TextView txtClabe;
    private TextView txtServicios;
    private TextView txtCelular;
    private TextView txtEmail;
    private TextView txtClave;
    private TextView lblTitular;
    private TextView lblCuenta;
    private TextView lblTarjeta;
    private TextView lblClabe;
    private TextView lblCelular;
    private TextView lblEmail;
    private boolean nextActivaded;
    private TextView txt_contcuenta;
    private TextView txt_contalertas;
    private TextView txt_contcanal;


    public ExitoViewController(GuiTools guiTools, CreaCuentaDelegate creaCuentaDelegate, boolean nextActivaded) {
        this.creaCuentaDelegate = creaCuentaDelegate;
        this.guiTools = guiTools;
        exitoDelegate = new ExitoDelegate(creaCuentaDelegate, this);
        this.nextActivaded = nextActivaded;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_exito, container, false);
        init();
        return rootView;
    }

    private void init() {
        findViewById();
        scaleView();
        exitoDelegate.obtenerDatos();
        exitoDelegate.dataKeyChain();
        etiquetado();
    }

    private void etiquetado() {
        ArrayList<String> list = new ArrayList<String>();
        list.add(Constants.QUIERO_CUENTA);
        list.add(Constants.CREA_CUENTA);
        list.add(Constants.CLAUSULAS_CUENTA);
        list.add(Constants.CONFIRMA_CUENTA);
        list.add(Constants.EXITO_CUENTA);
        TrackingHelperCuentaDigital.trackState(list);
        Map<String, Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackingHelperCuentaDigital.trackEntrada(eventoEntrada);
        //TrackingHelper.trackState("exito", new ArrayList<String>());
    }

    private void findViewById() {
        imbSalir = (ImageButton) rootView.findViewById(R.id.imb_salir);
        txtContrato = (TextView) rootView.findViewById(R.id.txt_contrato);
        txt_contcanal = (TextView) rootView.findViewById(R.id.txt_contcanal);
        txt_contcuenta = (TextView) rootView.findViewById(R.id.txt_contcuenta);
        //txt_contalertas = (TextView) rootView.findViewById(R.id.txt_contalertas);
        txtCelular = (TextView) rootView.findViewById(R.id.txt_celular);
        txtClabe = (TextView) rootView.findViewById(R.id.txt_cuenta_clabe);
        txtCuenta = (TextView) rootView.findViewById(R.id.txt_nombre_cuenta);
        txtEmail = (TextView) rootView.findViewById(R.id.txt_email);
        txtTitular = (TextView) rootView.findViewById(R.id.txt_nombre_titular);
        txtServicios = (TextView) rootView.findViewById(R.id.txt_servicios);
        txtTarjeta = (TextView) rootView.findViewById(R.id.txt_tarjeta);
        clave = (EditText) rootView.findViewById(R.id.edt_code_mail);
        lblTitular = (TextView) rootView.findViewById(R.id.lbl_nombre_titular);
        lblCuenta = (TextView) rootView.findViewById(R.id.lbl_nombre_cuenta);
        lblTarjeta = (TextView) rootView.findViewById(R.id.lbl_tarjeta);
        lblClabe = (TextView) rootView.findViewById(R.id.lbl_cuenta_clabe);
        lblCelular = (TextView) rootView.findViewById(R.id.lbl_celular);
        lblEmail = (TextView) rootView.findViewById(R.id.lbl_email);
        if (!nextActivaded) {
            imbSalir.setImageResource(R.drawable.an_btn_salir);
            Session session = Session.getInstance(getActivity());
            clave.setVisibility(View.GONE);
            txtClave = (TextView) rootView.findViewById(R.id.txt_ingresa_cod);
            txtClave.setVisibility(View.GONE);
            session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CONTRATAR_CUENTA_DIGITAL, false, true);
            session = null;
        }

        if (!exitoDelegate.isCanal()) {
            txt_contcanal.setVisibility(View.GONE);
        }

        imbSalir.setOnClickListener(this);
        txt_contcuenta.setOnClickListener(this);
        //txt_contalertas.setOnClickListener(this);
        txt_contcanal.setOnClickListener(this);
        setSelected();
    }


    private void setSelected() {
        txtTitular.setSelected(true);
        txtCuenta.setSelected(true);
        txtTarjeta.setSelected(true);
        txtClabe.setSelected(true);
        txtCelular.setSelected(true);
        txtEmail.setSelected(true);

        lblTitular.setSelected(true);
        lblCuenta.setSelected(true);
        lblTarjeta.setSelected(true);
        lblClabe.setSelected(true);
        lblCelular.setSelected(true);
        lblEmail.setSelected(true);
    }

    private void scaleView() {
        guiTools.scale(rootView.findViewById(R.id.img_aviso));
        guiTools.scale(rootView.findViewById(R.id.txt_mesage));
        guiTools.scale(rootView.findViewById(R.id.txt_tu_cuenta), true);
        guiTools.scale(lblTitular, true);
        guiTools.scale(txtClave, true);
        guiTools.scale(txtTitular, true);
        guiTools.scale(lblCuenta, true);
        guiTools.scale(txtCuenta, true);
        guiTools.scale(lblTarjeta, true);
        guiTools.scale(txtTarjeta, true);
        guiTools.scale(lblClabe, true);
        guiTools.scale(txtClabe, true);
        guiTools.scale(rootView.findViewById(R.id.lbl_servicios), true);
        guiTools.scale(txtServicios, true);
        guiTools.scale(lblCelular, true);
        guiTools.scale(txtCelular, true);
        guiTools.scale(lblEmail, true);
        guiTools.scale(txtEmail, true);
        guiTools.scale(txtContrato, true);
        guiTools.scale(txt_contcanal, true);
        //guiTools.scale(txt_contalertas, true);
        guiTools.scale(txt_contcuenta, true);
        guiTools.scale(rootView.findViewById(R.id.txt_exito), true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        if (v == txt_contcuenta) {

            exitoDelegate.showTerminos("cuenta");
        }
        if (v == imbSalir) {
            if (!nextActivaded) {
                SuiteAppApi.getIntentToReturn().returnObjectFromApi(null);
            } else {
                SuiteAppApi sftoken = new SuiteAppApi();
                sftoken.onCreate(getActivity());
                ContratacionSTDelegate delegate = (ContratacionSTDelegate) sftoken.getSuiteViewsControllerApi().getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
                if (null == delegate) {
                    delegate = new ContratacionSTDelegate();
                    sftoken.getSuiteViewsControllerApi().addDelegateToHashMap(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID, delegate);
                    sftoken.getSuiteViewsControllerApi().addDelegateToHashMap(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID, delegate.generaTokendelegate);
                }
                delegate.softToken.setNumeroSerie(APICuentaDigitalSharePreferences.getInstance().getStringData(Constants.TAG_NUMERO_SERIE));
                delegate.softToken.setNumeroTelefono(txtCelular.getText().toString());
                if (APICuentaDigitalSharePreferences.getInstance().getStringData(Constants.COMPANIA).equals(Constants.COMPANIA_VIRGIN_MOBILE)) {
                    delegate.softToken.setCompanniaCelular(Constants.COMPANIA_MOVISTAR);
                } else {
                    delegate.softToken.setCompanniaCelular(APICuentaDigitalSharePreferences.getInstance().getStringData(Constants.COMPANIA));
                }
                delegate.softToken.setNumeroTarjeta(txtTarjeta.getText().toString());
                delegate.softToken.setNombreToken(APICuentaDigitalSharePreferences.getInstance().getStringData(Constants.TAG_NOMBRE_TOKEN));
                delegate.isCuentaDigital = Boolean.TRUE;
                delegate.setInterfaceRetorno(this);
                delegate.activacionST(clave.getText().toString());

            }

        } else if (v == txt_contalertas) {
            exitoDelegate.showTerminos("alertas");
        } else if (v == txt_contcanal) {
            exitoDelegate.showTerminos("canal");
        }
    }

    public void mostarDatos(String titular, String cuenta, String tarjeta, String clabe, String celular, String email) {
        txtTitular.setText(titular);
        txtCuenta.setText(cuenta);
        txtTarjeta.setText(tarjeta);
        txtClabe.setText(clabe);
        txtCelular.setText(celular);
        txtEmail.setText(email);
    }

    public void finish() {
        getActivity().finish();
    }


    @Override
    public void returnCuentaDigital(String alert) {
        if (alert.equals("aviso")) {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent alert = new Intent(getActivity(), PopUpGeneral.class);
                    alert.putExtra(Constants.MENSAJE, "Error de comunicaciones");
                    alert.putExtra(Constants.FINAL, Constants.FINAL);
                    getActivity().startActivityForResult(alert, 1234);

                }
            });

        } else if (alert.equals("final")) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), PopUpFinal.class);
                    startActivity(intent);

                }
            });
        } else if (alert.equalsIgnoreCase("codigo")) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(getActivity(), PopUpGeneral.class).putExtra(Constants.MENSAJE, "Tu clave es incorrecta, intenta nuevamente."));
                }
            });
        } else if (alert.equalsIgnoreCase("blocked")) {
            startActivity(new Intent(getActivity(), PopUpGeneral.class).putExtra(Constants.MENSAJE, "Excediste el número de intentos.").putExtra(Constants.FINAL, Constants.FINAL));
        } else {
            startActivity(new Intent(getActivity(), PopUpGeneral.class).putExtra(Constants.MENSAJE, alert).putExtra(Constants.FINAL, Constants.FINAL));
        }
    }
}
