package com.bbva.apicuentadigital.delegates;

import com.bbva.apicuentadigital.common.BaseActivity;
import com.bbva.apicuentadigital.io.ServerResponse;

import java.util.Hashtable;

/**
 * Created by Karina on 07/12/2015.
 */
public class BaseDelegate {


    public void doNetworkOperation(final int operationId, final Hashtable<String,?> params, final BaseActivity caller) {}

    public void analyzeResponse(int operationId, ServerResponse response) {}

    public void performAction(Object obj) {}

    public long getDelegateIdentifier() { return 0; }
}
