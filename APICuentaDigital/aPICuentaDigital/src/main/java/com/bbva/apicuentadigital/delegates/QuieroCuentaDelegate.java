package com.bbva.apicuentadigital.delegates;

import android.content.Intent;
import android.util.Log;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.controllers.CreaCuentaViewController;
import com.bbva.apicuentadigital.controllers.PopUpGeneral;
import com.bbva.apicuentadigital.controllers.QuieroCuentaViewController;
import com.bbva.apicuentadigital.io.Server;
import com.bbva.apicuentadigital.io.ServerResponse;
import com.bbva.apicuentadigital.models.ConsultaColoniasCurp;
import com.bbva.apicuentadigital.models.ConsultaColoniasNueva;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;
import com.google.gson.Gson;

import java.util.Hashtable;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;

/**
 * Created by Karina on 06/12/2015.
 */
public class QuieroCuentaDelegate extends BaseDelegate{

    private QuieroCuentaViewController quieroCuentaView;
    private String celular;
    private String compania;
    private int peticion;
    private String cp;
    private String curp;
    private ConsultaColoniasNueva consultaColonias;
    private boolean siguientePantalla=Boolean.FALSE;

    public QuieroCuentaDelegate(QuieroCuentaViewController quieroCuentaView, String celular, String compania){
        this.quieroCuentaView = quieroCuentaView;
        this.celular = celular;
        this.compania = compania;
    }

    public boolean validaCurp(String curp){
       /* if (curp == null){
            return false;
        }*/
        if ((curp.length()==0) || !(curp.matches(Constants.EXPRESION_REGULAR_CURP))) {
            return false;

        }else {
            return true;
        }
    }

    public boolean validaCP(String cp){
       if(cp.length() == 5){
            return true;
        }else{
            return false;
        }
    }
    public boolean validaCPvacio(String cp){
        if(cp==null){
            return true;
        }else if (cp.length() == 0){
            return true;
        } else {
            return false;
        }
    }

    public void realizaOperacion(final String curp, final String cp){

        //quieroCuentaView.showActivityIndicator(quieroCuentaView.getResources().getString(R.string.alert_operation),
          //      quieroCuentaView.getResources().getString(R.string.alert_connecting),quieroCuentaView );
        this.curp=curp;
        this.cp = cp;
        quieroCuentaView.showActivityIndicator();
        consultaColonias(cp);



    }

    private void consultaColonias(final String codigoPostal) {

        final Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(Constants.TAG_CODIGO_POSTAL, codigoPostal);
        Server server = Server.getInstance(this);
        peticion=ApiConstants.CONSULTA_COLONIAS;
        server.doNetWorkOperation(ApiConstants.CONSULTA_COLONIAS, params);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        quieroCuentaView.hideActivityIndicator();

        if(response.getCODE()!=null) {
            if (response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
                if (peticion == ApiConstants.CONSULTA_COLONIAS) {
                    consultaColonias = (ConsultaColoniasNueva) response.getResponse();

                    APICuentaDigitalSharePreferences.getInstance().setStringData(Constants.TAG_DELEGACION, consultaColonias.getConsultaColonias().getDelegacion());
                    APICuentaDigitalSharePreferences.getInstance().setStringData(Constants.TAG_ENTIDAD, consultaColonias.getConsultaColonias().getEntidad());
                    String idRenapo = "FALSE";

                    if (!curp.equalsIgnoreCase("") && curp.length() == 18)
                        idRenapo = "CURP";

                    if (idRenapo.equalsIgnoreCase("CURP")) {
                        Hashtable<String, String> params = new Hashtable<String, String>();
                        params.put(Constants.TAG_IND_CP, "TRUE");
                        params.put(Constants.TAG_IND_RENAPO, idRenapo);
                        params.put(Constants.TAG_CODIGO_POSTAL, cp);
                        params.put(Constants.TAG_CURP, curp);
                        params.put(Constants.TAG_NOMBRES, "");
                        params.put(Constants.TAG_APATERNO, "");
                        params.put(Constants.TAG_AMATERNO, "");
                        params.put(Constants.TAG_SEXO, "");
                        params.put(Constants.TAG_FECHANAC, "");
                        params.put(Constants.TAG_CVEENTIDADNAC, "");
                        peticion = ApiConstants.CONSULTA_CURP;
                        Server server = Server.getInstance(this);
                        siguientePantalla= Boolean.TRUE;
                        server.doNetWorkOperation(ApiConstants.CONSULTA_CURP, params);
                    } else {
                        showCrearCuentaCP(consultaColonias);
                    }
                    // }else{

                    //  }

                } else {
                    ConsultaColoniasCurp consultaColoniasCurp = (ConsultaColoniasCurp) response.getResponse();
                    if (consultaColoniasCurp.getConsultaCurp().getCode().equalsIgnoreCase(response.CODE_OK)) {
                        showCrearCuenta(consultaColoniasCurp, consultaColonias);
                        APICuentaDigitalSharePreferences.getInstance().setStringData(Constants.TAG_CURP, curp);
                    } else {
                        mensajeErrorServer(response, 22);
                    }
                }
                //if(ConstantsServer.ALLOW_LOG)android.util.Log.e("Dora", "analyzeResponse" + consultaColoniasCurp.getConsultaColonias().getCpValido());
            } else {
                //showCrearCuentaCP(consultaColonias);
                if(siguientePantalla){
                    siguientePantalla=Boolean.FALSE;
                    mensajeErrorServer(response, 22);
                }else{
                    mensajeErrorServer(response, 1234);
                }

            }
        }else{
            Intent alert = new Intent(quieroCuentaView, PopUpGeneral.class);
            alert.putExtra(Constants.MENSAJE, "Error de comunicaciones");
            quieroCuentaView.startActivityForResult(alert, 1234);
        }
    }

    private void mensajeErrorServer(ServerResponse response, int requestCode) {
        quieroCuentaView.limpiaCampos();
        Log.e("Dora", "analyzeResponse errorQuieroCuenta");

        int asFinal;
        asFinal = response.getMESSAGE().indexOf("**");
        if (asFinal > -1) {
            Intent alert = new Intent(quieroCuentaView, PopUpGeneral.class);
            alert.putExtra(Constants.MENSAJE, response.getMESSAGE().substring(0, asFinal));
            quieroCuentaView.startActivityForResult(alert, requestCode);
        } else {
            Intent alert = new Intent(quieroCuentaView, PopUpGeneral.class);
            alert.putExtra(Constants.MENSAJE, response.getMESSAGE());
            quieroCuentaView.startActivityForResult(alert, requestCode);
        }
    }

    private void showCrearCuenta(final ConsultaColoniasCurp consultaColoniasCurp, final ConsultaColoniasNueva colonias){
        Gson gson = new Gson();
        Intent intent = new Intent(quieroCuentaView, CreaCuentaViewController.class);
        intent.putExtra(Constants.TAG_CODIGO_POSTAL, cp);
        intent.putExtra(Constants.COMPANIA,compania);
        intent.putExtra(Constants.CELULAR,celular);
        intent.putExtra(Constants.CONSULTA_COLONIA_CURP, gson.toJson(consultaColoniasCurp));
        intent.putExtra(Constants.CONSULTA_COLONIA, gson.toJson(colonias));
        quieroCuentaView.startActivity(intent);
        quieroCuentaView.finish();
    }

    private void showCrearCuentaCP(final ConsultaColoniasNueva colonias){
        Gson gson = new Gson();
        Intent intent = new Intent(quieroCuentaView, CreaCuentaViewController.class);
        intent.putExtra(Constants.TAG_CODIGO_POSTAL, cp);
        intent.putExtra(Constants.COMPANIA,compania);
        intent.putExtra(Constants.CELULAR,celular);
        intent.putExtra(Constants.CONSULTA_COLONIA, gson.toJson(colonias));
        quieroCuentaView.startActivity(intent);
        quieroCuentaView.finish();
    }

    public void next () {
        showCrearCuentaCP(consultaColonias);
    }


}
