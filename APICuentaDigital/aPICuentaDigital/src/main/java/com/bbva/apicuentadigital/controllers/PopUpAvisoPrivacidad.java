package com.bbva.apicuentadigital.controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.APICuentaDigital;

/**
 * Created by mcamarillo on 09/07/16.
 */
public class PopUpAvisoPrivacidad extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_popup_aviso_privacidad);

        TextView txtMessage = (TextView) findViewById(R.id.lblMessageAvisoPrivacidad);
        txtMessage.setText(Html.fromHtml("<font>BBVA Bancomer, S.A., Av. Paseo de la Reforma 510, Colonia Juárez, Delegación Cuauhtémoc, Código Postal 06600, Distrito Federal, recaba sus datos para verificar su identidad. El Aviso de Privacidad Integral actualizado está en cualquier sucursal y en </font><b><font color=\"#009EE5\">http://www.bancomer.com/personas/aviso-privacidad.jsp</font>"));
        Button btnLate = (Button) findViewById(R.id.btnAceptarAvisoPrivacidad);
        btnLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent();
                i.putExtra("regresar", false);
                setResult(RESULT_OK, i);
                finish();
            }
        });



    }


    @Override
    protected void onResume() {
        super.onResume();
        APICuentaDigital.appContext=this;
    }

    @Override
    public void onBackPressed() {
    }
}
