package com.bbva.apicuentadigital.io;

import android.util.Log;

import java.io.IOException;

/**
 * Created by Karina on 07/12/2015.
 */
public class ServerResponse implements ParsingHandler{

    private Object handler = null;

    public String CODE_OK = "200";

    public String CODE;

    public String MESSAGE;

    public ServerResponse(){

    }
    public ServerResponse(Object handler){
        this.handler = handler;
    }

    public Object getResponse(){
        return handler;
    }

    public String getCODE() {
        return CODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
            Log.e("Aqui", "Parse recibido");

        if(parser != null) {
            Result result = parser.parseResult();
            Log.e("result",result.getCode()+ result.getMessage());
            if (result != null) {
                CODE = result.getCode();

                if (CODE.equalsIgnoreCase(CODE_OK)) {
                    if ((handler != null) && (handler instanceof ParsingHandler)) {
                        ((ParsingHandler) handler).process(parser);
                        }
                }else {
                    CODE = result.getCode();
                    MESSAGE = result.getMessage();
                }
            }else {
                CODE = "";
                MESSAGE = "";
            }
        }else{
            CODE = "";
             MESSAGE = "";
        }
    }
}
