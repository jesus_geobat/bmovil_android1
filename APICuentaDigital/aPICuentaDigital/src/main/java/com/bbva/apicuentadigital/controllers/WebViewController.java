package com.bbva.apicuentadigital.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import bancomer.api.common.timer.TimerController;
import tracking.TrackingHelperCuentaDigital;

/**
 * Created by DMEJIA on 17/12/15.
 */
public class WebViewController extends Activity{

    private WebView wvpoliza;
    private TextView lblTitulo;

    private String operation;
    private String url;

    private AtomicBoolean mPreventAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_web_view);
        operation = (String)this.getIntent().getExtras().get(Constants.OPERATION);
        url = (String)this.getIntent().getExtras().get(Constants.URL);
        init();
    }

    private void init(){
        mPreventAction = new AtomicBoolean(false);
        findViewById();
        scaleView();
        selectOperation();
        etiquetado();
    }

    private void etiquetado(){
        ArrayList<String> list= new ArrayList<String>();
        list.add(Constants.CONTRATO_CUENTA);
        TrackingHelperCuentaDigital.trackState(list);
        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackingHelperCuentaDigital.trackEntrada(eventoEntrada);
    }

    private void findViewById(){
        lblTitulo = (TextView)findViewById(R.id.title_text);
        wvpoliza = (WebView)findViewById(R.id.poliza);
    }

    private void scaleView(){
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(wvpoliza);
        guiTools.scale(lblTitulo, true);
    }

    private void selectOperation(){
        switch (operation){
            case Constants.TERMINOS:
                lblTitulo.setText(getResources().getString(R.string.consulta_terminos));
                break;
            case Constants.CONTRATO:
                lblTitulo.setText(getResources().getString(R.string.consulta_terminos));
                break;
        }
        showWebView(url);
    }

    private String leerArchivo(String url){
        String leerHTML = "";
        try {
            FileInputStream fIn = new FileInputStream(url);
            InputStreamReader archivo = new InputStreamReader(fIn);
            BufferedReader br = new BufferedReader(archivo);
            String linea = br.readLine();
            while (linea != null) {
                leerHTML += linea + "";
                linea = br.readLine();
            }
            br.close();
            archivo.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return leerHTML;
    }

    private void showWebView(String html){
        String URLPDF="";
        Log.d("MANRIQ---",html);
        wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        if(html.equals("cuenta")){
             URLPDF ="http://www.bancomer.com/fbin/contrato-cuenta-multinivel_tcm1344-487163.pdf";
        }else if(html.equals("alertas")){
             URLPDF ="http://www.bancomer.com/fbin/Contrato_Alertas_10_marzo_2016_tcm1344-487793.pdf";
        }else if(html.equals("canal")){
            URLPDF ="http://www.bancomer.com/fbin/cud_tcm1344-591310.pdf";
        }else if(html.equals("web_view_firma")){
            URLPDF = "http://www.bancomer.com/fbin/cud_tcm1344-591310.pdf";
        }

        wvpoliza = (WebView) findViewById(R.id.poliza);
        wvpoliza.getSettings().setJavaScriptEnabled(true);
        wvpoliza.getSettings().setDefaultTextEncodingName("utf-8");
        wvpoliza.getSettings().setBuiltInZoomControls(true);
        wvpoliza.getSettings().setSupportZoom(true);
        wvpoliza.getSettings().setLoadWithOverviewMode(true);
        wvpoliza.getSettings().setUseWideViewPort(true);
        wvpoliza.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        wvpoliza.setWebViewClient(new Callback());
        wvpoliza.loadUrl("http://docs.google.com/gview?embedded=true&url=" + URLPDF);
        wvpoliza.setInitialScale(15);

    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }
}
