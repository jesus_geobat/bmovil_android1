package com.bbva.apicuentadigital.models;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.io.ParserJSON;
import com.bbva.apicuentadigital.io.ParsingException;
import com.bbva.apicuentadigital.io.ParsingHandler;

import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Karina on 08/12/2015.
 */
public class ValidaOTP implements ParsingHandler {

    private String titular;
    private String tarjeta;
    private String cuenta;
    private String cuentaClabe;
    private String numeroSerie;
    private String nombreToken;
    private boolean contratacionCuenta;
    private boolean contratacionCanal;
    private boolean solicitudToken;
    private boolean autenticacionToken;

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getNombreToken() {
        return nombreToken;
    }

    public void setNombreToken(String nombreToken) {
        this.nombreToken = nombreToken;
    }

    public boolean isContratacionCuenta() {
        return contratacionCuenta;
    }

    public void setContratacionCuenta(boolean contratacionCuenta) {
        this.contratacionCuenta = contratacionCuenta;
    }

    public boolean isContratacionCanal() {
        return contratacionCanal;
    }

    public void setContratacionCanal(boolean contratacionCanal) {
        this.contratacionCanal = contratacionCanal;
    }

    public boolean isSolicitudToken() {
        return solicitudToken;
    }

    public void setSolicitudToken(boolean solicitudToken) {
        this.solicitudToken = solicitudToken;
    }

    public boolean isAutenticacionToken() {
        return autenticacionToken;
    }

    public void setAutenticacionToken(boolean autenticacionToken) {
        this.autenticacionToken = autenticacionToken;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getCuentaClabe() {
        return cuentaClabe;
    }

    public void setCuentaClabe(String cuentaClabe) {
        this.cuentaClabe = cuentaClabe;
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        if(parser.hasValue(Constants.TAG_RESPONSE))
            parser = new ParserJSON(parser.parserNextObject(Constants.TAG_RESPONSE).toString());

        if(parser.hasValue(Constants.TAG_TITULAR))
            titular = parser.parseNextValue(Constants.TAG_TITULAR);

        if(parser.hasValue(Constants.TAG_TARJETA))
            tarjeta = parser.parseNextValue(Constants.TAG_TARJETA);

        if(parser.hasValue(Constants.TAG_CUENTA))
            cuenta = parser.parseNextValue(Constants.TAG_CUENTA);

        if(parser.hasValue(Constants.TAG_CUENTA_CLABE))
            cuentaClabe = parser.parseNextValue(Constants.TAG_CUENTA_CLABE);

        if(parser.hasValue(Constants.TAG_NUMERO_SERIE))
            numeroSerie = parser.parseNextValue(Constants.TAG_NUMERO_SERIE);

        if(parser.hasValue(Constants.TAG_NOMBRE_TOKEN))
            nombreToken = parser.parseNextValue(Constants.TAG_NOMBRE_TOKEN);

        if(parser.hasValue(Constants.TAG_CONTRATACION_CUENTA))
            contratacionCuenta = Boolean.parseBoolean(parser.parseNextValue(Constants.TAG_CONTRATACION_CUENTA).toString());

        if(parser.hasValue(Constants.TAG_CONTRATACION_CANAL))
            contratacionCanal = Boolean.parseBoolean(parser.parseNextValue(Constants.TAG_CONTRATACION_CANAL).toString());

        if(parser.hasValue(Constants.TAG_SOLICITUD_TOKEN))
            solicitudToken = Boolean.parseBoolean(parser.parseNextValue(Constants.TAG_SOLICITUD_TOKEN).toString());

        if(parser.hasValue(Constants.TAG_AUTENTICACION_TOKEN))
            autenticacionToken = Boolean.parseBoolean(parser.parseNextValue(Constants.TAG_AUTENTICACION_TOKEN).toString());


    }
}
