package com.bbva.apicuentadigital.delegates;

import android.content.Intent;
import android.util.Log;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.controllers.PopUpGeneral;
import com.bbva.apicuentadigital.io.ConstantsServer;
import com.bbva.apicuentadigital.io.Server;
import com.bbva.apicuentadigital.io.ServerResponse;
import com.bbva.apicuentadigital.models.OTP;
import com.bbva.apicuentadigital.models.ValidaOTP;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

import java.util.Hashtable;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.aplicaciones.softtoken.classes.model.token.SoftToken;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;

/**
 * Created by Karina on 09/12/2015.
 */
public class ConfirmacionDelegate extends BaseDelegate{

    private CreaCuentaDelegate creaCuentaDelegate;
    private APICuentaDigitalSharePreferences sharePreferences;

    public ConfirmacionDelegate(CreaCuentaDelegate creaCuentaDelegate){
        this.creaCuentaDelegate = creaCuentaDelegate;
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();
    }

    public void realizaOperacion(String otpSMS,String otpMail){

        String newOtpSms = otpSMS;

        creaCuentaDelegate.getCreaCuentaView().showActivityIndicator();

        //String ium = Tools.buildIUM(sharePreferences.getStringData(Constants.CELULAR));
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(Constants.TAG_NUMUERO_CELULAR,sharePreferences.getUser());
        params.put(Constants.TAG_NOMBRES,sharePreferences.getStringData(Constants.TAG_NOMBRE));
        params.put(Constants.TAG_APATERNO,sharePreferences.getStringData(Constants.TAG_APATERNO));
        params.put(Constants.TAG_AMATERNO,sharePreferences.getStringData(Constants.TAG_AMATERNO));//JACT
        if (sharePreferences.getStringData(Constants.TAG_COMPANIA).equals(Constants.COMPANIA_VIRGIN_MOBILE)){
            params.put(Constants.TAG_COMPANIA, Constants.COMPANIA_MOVISTAR);
        } else {
            params.put(Constants.TAG_COMPANIA, sharePreferences.getStringData(Constants.COMPANIA).toUpperCase());
        }
        params.put(Constants.TAG_MAIL, sharePreferences.getStringData(Constants.TAG_MAIL));
        params.put(Constants.TAG_CALLE, sharePreferences.getStringData(Constants.TAG_CALLE));
        params.put(Constants.TAG_COLONIA, sharePreferences.getStringData(Constants.TAG_COLONIA));
        params.put(Constants.TAG_NUMEXT, sharePreferences.getStringData(Constants.TAG_NUMEXT));
        params.put(Constants.TAG_NUMINT, sharePreferences.getStringData(Constants.TAG_NUMINT));
        params.put(Constants.TAG_CODIGO_POSTAL,sharePreferences.getStringData(Constants.TAG_CODIGO_POSTAL));
        params.put(Constants.TAG_DELEGACION,sharePreferences.getStringData(Constants.TAG_DELEGACION));
        params.put(Constants.TAG_ESTADO,sharePreferences.getStringData(Constants.TAG_ENTIDAD));
        params.put(Constants.TAG_PAIS,Constants.MEXICO);
        params.put(Constants.TAG_CURP,sharePreferences.getStringData(Constants.TAG_CURP));
        params.put(Constants.TAG_NUMEROID, sharePreferences.getStringData(Constants.TAG_CREDENTIAL));
        params.put(Constants.TAG_OTPSMS, newOtpSms);
        params.put(Constants.TAG_IUM, Constants.EMPTY_STRING);
        params.put(Constants.TAG_ACCESSCODE, sharePreferences.getStringData(Constants.TAG_ACCESSCODE)); //MAPE1

        int n=sharePreferences.getIntData(Constants.CONT);
        n=n+1;
        Log.e("MAPE","inicial"+n);

        sharePreferences.setIntData(Constants.CONT, n++);

        Log.e("MAPE","Despues de... cont" + sharePreferences.getIntData(Constants.CONT));

        if(sharePreferences.getIntData(Constants.CONT)==4){
            //creaCuentaDelegate.getCreaCuentaView().showInformationAlert("Superado el numero de intentos");
            Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
            alert.putExtra(Constants.MENSAJE, "Superado el numero de intentos");
            alert.putExtra(Constants.FINAL,Constants.FINAL);
            sharePreferences.setBooleanData("CONFIRMAOTP", false);
            creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
            //creaCuentaDelegate.quieroCuenta();

        }else{
            Server server = Server.getInstance(this);
            server.doNetWorkOperation(ApiConstants.CONSULTA_VALIDA_OTP, params);
        }
    }

    public void generaOTP(){
        realizaOperacion(sharePreferences.getStringData(Constants.CELULAR), sharePreferences.getStringData(Constants.COMPANIA), sharePreferences.getStringData(Constants.TAG_MAIL));
    }

    public void realizaOperacion(String numCelular, String compania, String mail){

        creaCuentaDelegate.getCreaCuentaView().showActivityIndicator();
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(Constants.TAG_NUMUERO_CELULAR,numCelular);
        if (APICuentaDigitalSharePreferences.getInstance().getStringData(Constants.COMPANIA).equals(Constants.COMPANIA_VIRGIN_MOBILE)){
            params.put(Constants.TAG_COMPANIA, Constants.COMPANIA_MOVISTAR);
        } else {
            params.put(Constants.TAG_COMPANIA, compania);
        }
        params.put(Constants.TAG_MAIL,mail);
        params.put(Constants.TAG_CURP, sharePreferences.getStringData(Constants.TAG_CURP));
        params.put(Constants.TAG_CODIGO_POSTAL,sharePreferences.getStringData(Constants.TAG_CODIGO_POSTAL));
        params.put(Constants.TAG_NOMBRE,sharePreferences.getStringData(Constants.TAG_NOMBRE));
        params.put(Constants.TAG_PRIMER_APELLIDO,sharePreferences.getStringData(Constants.TAG_APATERNO));
        params.put(Constants.TAG_SEGUNDO_APELLIDO,sharePreferences.getStringData(Constants.TAG_AMATERNO));
        params.put(Constants.TAG_FECHA_NACIEMIENTO,sharePreferences.getStringData(Constants.TAG_FECHANAC));

       /* params.put(Constants.TAG_NUMUERO_CELULAR,"5561437523");
        params.put(Constants.TAG_COMPANIA, "TELCEL");
        params.put(Constants.TAG_MAIL,"erika.manriquep@gmail.com");
        params.put(Constants.TAG_CURP,"mape900605mdfnlr01");
        params.put(Constants.TAG_NOMBRE,"Erika");
        params.put(Constants.TAG_PRIMER_APELLIDO,"manrique");
        params.put(Constants.TAG_SEGUNDO_APELLIDO,"plascencia");
        params.put(Constants.TAG_FECHA_NACIEMIENTO,"1990-06-05");*/

        int n=sharePreferences.getIntData(Constants.CONTGOTP);
        n=n+1;
        Log.e("MAPE", "inicialOTP"+n);

        sharePreferences.setIntData(Constants.CONTGOTP, n++);

        Log.e("MAPE","Despues de... contOTP" + sharePreferences.getIntData(Constants.CONTGOTP));

        if(sharePreferences.getIntData(Constants.CONTGOTP)==4){
            //creaCuentaDelegate.getCreaCuentaView().showInformationAlert("Superado el numero de intentos");
            Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
            alert.putExtra(Constants.MENSAJE, "Superado el numero de intentos");
            alert.putExtra(Constants.FINAL,Constants.FINAL);
            creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
            //creaCuentaDelegate.quieroCuenta();
        }else {
            Server server = Server.getInstance(this);
            server.doNetWorkOperation(ApiConstants.CONSULTA_GENERA_OTP, params);
        }
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        creaCuentaDelegate.getCreaCuentaView().hideActivityIndicator();
        if(operationId == ApiConstants.CONSULTA_VALIDA_OTP) {
            if(response.getCODE()!=null) {
                if (response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
                    if (ConstantsServer.ALLOW_LOG) android.util.Log.e("Dora", "analyzeResponse");
                    sharePreferences.setIntData(Constants.CONT, 0);
                    ValidaOTP validaOTP = (ValidaOTP) response.getResponse();
                    //guardarBanderas();
                    creaCuentaDelegate.setValidaOTP(validaOTP);
                    sharePreferences.setBooleanData(Constants.CONFIRMA, false);
                    sharePreferences.setStringData(Constants.TAG_NUMERO_SERIE, validaOTP.getNumeroSerie());
                    sharePreferences.setStringData(Constants.TAG_NOMBRE_TOKEN, validaOTP.getNombreToken());
                    sharePreferences.setBooleanData(Constants.TAG_CONTRATACION_CANAL, validaOTP.isContratacionCanal());
                    sharePreferences.setStringData(Constants.TAG_TITULAR, validaOTP.getTitular());
                    sharePreferences.setStringData(Constants.TAG_CUENTA, validaOTP.getCuenta());
                    sharePreferences.setStringData(Constants.TAG_CUENTA_CLABE, validaOTP.getCuentaClabe());
                    sharePreferences.setStringData(Constants.TAG_TARJETA, validaOTP.getTarjeta());
                    Session session = Session.getInstance(creaCuentaDelegate.getCreaCuentaView());
                    session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_FIRMA_DIGITAL, true, true);
                    if (validaOTP.isContratacionCanal() && validaOTP.isAutenticacionToken() && validaOTP.isSolicitudToken()) {
                        session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CONTRATAR, false, true);
                        // session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CONTRATAR2x1, true, true);
                        addVariableKeyChain(bancomer.api.common.commons.Constants.ACTIVA_TOKEN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CUENTA_DIGITAL, session);
                        SofttokenActivationBackupManager.getCurrent().initPropertiesFile();
                        addVariableKeyChain(suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CONTRATACION_BT, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CUENTA_DIGITAL, session);
                        addVariableKeyChain(bancomer.api.common.commons.Constants.TIPO_TOKEN,"S1", session);
                        creaCuentaDelegate.getCreaCuentaView().firma(true);
                    } else if (validaOTP.isContratacionCanal() && !validaOTP.isAutenticacionToken() && validaOTP.isSolicitudToken()) {
                        session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CONTRATAR, false, true);
                        // session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CONTRATAR2x1, true, true);
                        addVariableKeyChain(bancomer.api.common.commons.Constants.ACTIVA_TOKEN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CUENTA_DIGITAL, session);
                        addVariableKeyChain(suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CONTRATACION_BT, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CUENTA_DIGITAL, session);
                        creaCuentaDelegate.getCreaCuentaView().firma(false);
                    } else if (validaOTP.isContratacionCanal() && !validaOTP.isSolicitudToken()) {
                        session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CONTRATAR, false, true);
                        addVariableKeyChain(bancomer.api.common.commons.Constants.ACTIVA_TOKEN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CUENTA_DIGITAL, session);
                        addVariableKeyChain(suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CONTRATACION_BT, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CUENTA_DIGITAL, session);
                        creaCuentaDelegate.getCreaCuentaView().firma(false);
                    } else if (validaOTP.isContratacionCanal()) {
                        session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CONTRATAR, true, true);
                        addVariableKeyChain(bancomer.api.common.commons.Constants.ACTIVA_TOKEN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CUENTA_DIGITAL, session);
                        addVariableKeyChain(suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CONTRATACION_BT, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.CUENTA_DIGITAL, session);
                        creaCuentaDelegate.getCreaCuentaView().firma(false);
                    } else {
                        creaCuentaDelegate.getCreaCuentaView().firma(false);
                    }


                } else {
                    //String mesagge="";
                    int asFinal;
                    asFinal = response.getMESSAGE().indexOf("**");
                    if (asFinal > -1) {
                        //mesagge= response.getMESSAGE().substring(0,asFinal);
                        Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
                        alert.putExtra(Constants.MENSAJE, response.getMESSAGE().substring(0, asFinal));
                        creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
                    } else {
                        //mesagge = response.getMESSAGE();
                        Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
                        alert.putExtra(Constants.MENSAJE, response.getMESSAGE());
                        creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
                        if (ConstantsServer.ALLOW_LOG)
                            android.util.Log.e("Dora", "analyzeResponse error");
                    }
                }
            }else{
                Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, "Error de comunicaciones");
                creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
            }
        }else if(operationId == ApiConstants.CONSULTA_GENERA_OTP){
            if(response.getCODE()!=null) {
                if (response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
                    if (ConstantsServer.ALLOW_LOG) android.util.Log.e("Dora", "analyzeResponse");
                    sharePreferences.setIntData(Constants.CONTGOTP, 0);
                    OTP otp = (OTP) response.getResponse();
                    Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
                    alert.putExtra(Constants.MENSAJE, creaCuentaDelegate.getCreaCuentaView().getString(R.string.confirmacion_message_no_code));
                    creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
                } else {
                    String mesagge = "";
                    int asFinal;
                    asFinal = response.getMESSAGE().indexOf("**");
                    if (asFinal > -1) {
                        mesagge = response.getMESSAGE().substring(0, asFinal);
                    } else {
                        mesagge = response.getMESSAGE();
                    }
                    //creaCuentaDelegate.getCreaCuentaView().showInformationAlert("Status "+response.getCODE()+"\n"+mesagge);
                    Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
                    alert.putExtra(Constants.MENSAJE, "Status " + response.getCODE() + "\n" + mesagge);
                    creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
                    if (ConstantsServer.ALLOW_LOG)
                        android.util.Log.e("Dora", "analyzeResponse error");
                }
            }else{
                Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, "Error de comunicaciones");
                creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
            }
        }
    }



    private void addVariableKeyChain(final String variable, final String valor, Session session){

        // Intergracion KeyChainAPI
        // Recoger KeyStoreManager
        final KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();
        try{
            kswrapper.storeValueForKey(variable, valor);
            //----
        }catch(Exception e){
            if(suitebancomer.aplicaciones.bmovil.classes.io.token.Server.ALLOW_LOG) e.printStackTrace();
        }
    }

}
