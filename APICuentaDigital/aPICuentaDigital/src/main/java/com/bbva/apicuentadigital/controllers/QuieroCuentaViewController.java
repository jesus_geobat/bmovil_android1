package com.bbva.apicuentadigital.controllers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.InputFilter;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.APICuentaDigital;
import com.bbva.apicuentadigital.common.BaseActivity;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.delegates.QuieroCuentaDelegate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.timer.TimerController;
import tracking.TrackingHelperCuentaDigital;

/**
 * Created by Karina on 06/12/2015.
 */
public class QuieroCuentaViewController extends BaseActivity {

    private CreaCuentaDelegate creaCuentaDelegate;


    private Bundle bundle;

    private String celular;
    private String compania;

    private EditText edtCURP;
    private EditText edtCP;

    private ImageButton imbRegistrar;
    private ImageButton imbWhat;
    private ImageButton imbWhere;

    private ImageView imgClose;
    private ImageView imgDesc;
    private TextView txtDesc;
    private TextView linkVerAvisoPrivacidad;

    private GuiTools guiTools;

    private QuieroCuentaDelegate delegate;

    private LinearLayout lnBotones;
    private RelativeLayout rlDescBoton;

    public boolean atras = true;
    ArrayList<String> list;
    private ImageButton ayuda;

    private int day, month, years, calYear = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_quiero_cuenta_digital);
        bundle = getIntent().getExtras();
        init();
    }

    private void init(){
        findViewById();
        scaleView();
        getDataInten();
        etiquetado();
    }

    public void getDataInten(){
        celular = bundle.getString(Constants.CELULAR);
        compania = bundle.getString(Constants.COMPANIA);
        delegate = new QuieroCuentaDelegate(this, celular, compania);
    }

    private void findViewById(){
        edtCURP = (EditText)findViewById(R.id.edt_curp);
        edtCP = (EditText)findViewById(R.id.edt_cp);

        imbRegistrar = (ImageButton)findViewById(R.id.imb_registrar);
        imbWhat = (ImageButton)findViewById(R.id.imb_what);
        imbWhere = (ImageButton)findViewById(R.id.imb_where);

        imgClose =(ImageView)findViewById(R.id.img_close);
        imgDesc = (ImageView)findViewById(R.id.img_desc);

        txtDesc = (TextView)findViewById(R.id.txt_desc);
        linkVerAvisoPrivacidad = (TextView)findViewById(R.id.txt_verAvisoPrivacidad);
        linkVerAvisoPrivacidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnVerAviso();
            }
        });
        lnBotones =(LinearLayout)findViewById(R.id.ln_botones);
        rlDescBoton = (RelativeLayout)findViewById(R.id.rl_desc);
        ayuda = (ImageButton) findViewById(R.id.imb_ayuda);
        final Activity activity = this;
        ayuda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent alert = new Intent(activity, PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, Constants.AYUDA_CURP);
                activity.startActivityForResult(alert, 1234);
            }
        });
        edtCURP.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(18)});

    }

    private void scaleView(){
        guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(findViewById(R.id.lbl_welcome), true);
        guiTools.scale(txtDesc, true);
        guiTools.scale(edtCP);
        guiTools.scale(edtCURP);
        guiTools.scale(linkVerAvisoPrivacidad);

    }

    private void etiquetado(){
        list= new ArrayList<String>();
        list.add(Constants.QUIERO_CUENTA);
        TrackingHelperCuentaDigital.trackState(list);
        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackingHelperCuentaDigital.trackEntrada(eventoEntrada);
       // TrackingHelper.trackState("quierocuenta", new ArrayList<String>());
    }

    public void btnVerAviso(){

        Intent intent = new Intent();
        intent.setClass(this, PopUpAvisoPrivacidad.class);
        this.startActivityForResult(intent, 1234);

    }

    /**
     * Este método se ejecuta al presionar el botón registrar.
     * @param v
     */
    public void btnRegistrar(View v){
        boolean validaCP = delegate.validaCP(edtCP.getText().toString());
        boolean validaCPvacio = delegate.validaCPvacio(edtCP.getText().toString());
        boolean validaCurp = delegate.validaCurp(edtCURP.getText().toString());
        final Calendar calendar = Calendar.getInstance();
        if (validaCurp){
            month = Integer.parseInt(edtCURP.getText().toString().substring(6,8));
            day = Integer.parseInt(edtCURP.getText().toString().substring(8,10));
            calYear= Integer.parseInt(edtCURP.getText().toString().substring(4, 6));
            if(calYear>=25){
                years = Integer.parseInt(edtCURP.getText().toString().substring(4,6))+ 1900;
            }else{
                years = Integer.parseInt(edtCURP.getText().toString().substring(4,6))+ 2000;
            }
        }
        if (!validaCPvacio){
            if (validaCP){

                if(edtCURP.getText().toString().length() > 0 && !edtCURP.getText().toString().matches(Constants.EXPRESION_REGULAR_CURP)){
                    Intent alert = new Intent(this, PopUpGeneral.class);
                    alert.putExtra(Constants.MENSAJE, Constants.CURP_INCORRECTA);
                    edtCURP.setText(Constants.EMPTY_STRING);
                    this.startActivity(alert);
                }else {
                    if (validaCurp) {
                        if ((years > calendar.get(Calendar.YEAR) - 18) ||
                                ((years >= calendar.get(Calendar.YEAR) - 18) && (month > calendar.get(Calendar.MONTH) + 1)) ||
                                ((years >= calendar.get(Calendar.YEAR) - 18) && (month >= calendar.get(Calendar.MONTH) + 1) &&
                                        (day > calendar.get(Calendar.DAY_OF_MONTH)))) {
                            Intent alert = new Intent(this, PopUpGeneral.class);
                            alert.putExtra(Constants.MENSAJE, Constants.MAYOR_EDAD);
                            edtCURP.setText(Constants.EMPTY_STRING);
                            this.startActivity(alert);
                        } else {
                            delegate.realizaOperacion(edtCURP.getText().toString(), edtCP.getText().toString());
                        }
                    }else {
                        delegate.realizaOperacion(edtCURP.getText().toString(), edtCP.getText().toString());
                    }
                    }
            } else {
                Intent alert = new Intent(this, PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, Constants.CP_INCORRECTO);
                this.startActivityForResult(alert, 1234);
                edtCP.setBackgroundResource(R.drawable.an_textbox_magenta);
                edtCP.setTextColor(getResources().getColor(R.color.pink_dark));
                edtCP.setHintTextColor(getResources().getColor(R.color.pink_dark));
            }
        } else {
            Intent alert = new Intent(this, PopUpGeneral.class);
            alert.putExtra(Constants.MENSAJE, Constants.CP_VACIO);
            this.startActivity(alert);
        }

    }

    public void btnRegresar(View v){
        onBackPressed();
    }

    public void btnQueEs(View v) {
        lnBotones.setVisibility(View.GONE);
        rlDescBoton.setVisibility(View.VISIBLE);
        imgDesc.setImageResource(R.drawable.img_quees);
        txtDesc.setText(getResources().getString(R.string.que_es));
    }

    public void btnDondeUsarla(View v) {
        lnBotones.setVisibility(View.GONE);
        rlDescBoton.setVisibility(View.VISIBLE);
        imgDesc.setImageResource(R.drawable.img_donde);
        txtDesc.setText(getResources().getString(R.string.donde_puedo_usarla));
    }

    public void btnDescubreVentajas(View v) {
        lnBotones.setVisibility(View.GONE);
        rlDescBoton.setVisibility(View.VISIBLE);
        imgDesc.setImageResource(R.drawable.img_ventajas);
        txtDesc.setText(getResources().getString(R.string.descrubre_ventajas));
    }

    public void btnBeneficios(View v){
        lnBotones.setVisibility(View.GONE);
        rlDescBoton.setVisibility(View.VISIBLE);
        imgDesc.setImageResource(R.drawable.img_beneficios);
        txtDesc.setText(getResources().getString(R.string.beneficios));
    }

    public void btnCerrar(View v){
        lnBotones.setVisibility(View.VISIBLE);
        rlDescBoton.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {

        if(atras) {
            Intent intent = new Intent();
            intent.setClass(this, PopUpSalir.class);
            this.startActivityForResult(intent, 1234);
        }else{
            super.onBackPressed();
        }
    }

    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        if(data.getExtras().getBoolean("regresar")){
            atras = false;
            onBackPressed();
        }else if(requestCode==22){
            delegate.next();
        }
    }

    public void limpiaCampos(){
        edtCURP.setText("");
        edtCP.setText("");
        edtCP.setBackgroundResource(R.drawable.an_textbox);
        edtCP.setTextColor(getResources().getColor(R.color.black));
        edtCP.setHintTextColor(getResources().getColor(R.color.blueA));
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        APICuentaDigital.appContext=this;
    }

}