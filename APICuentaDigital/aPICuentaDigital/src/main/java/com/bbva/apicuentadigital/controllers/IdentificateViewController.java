package com.bbva.apicuentadigital.controllers;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.common.Tools;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.delegates.IdentificateDelegate;
import com.bbva.apicuentadigital.models.ConsultaColonias;
import com.bbva.apicuentadigital.models.ConsultaColoniasNueva;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelperCuentaDigital;

/**
 * Created by Karina on 07/12/2015.
 */
public class IdentificateViewController extends Fragment implements View.OnClickListener {

    public TextView txtNombre;
    public TextView txtApaterno;
    public TextView txtAmaterno;
    public TextView txtFechaN;
    public TextView txtLugarN;
    public TextView txtCredencialE;
    public TextView txtCP;
    public TextView txtEstado;
    public TextView txtName;
    public TextView txtGenero;
    public TextView txtMunicipios;
    public EditText edtNombre;
    public EditText edtApaterno;
    public EditText edtAmaterno;
    public EditText edtFechaN;
    public EditText edtCredencialE;
    public ImageView img_credencial;
    public EditText edtCell;
    public EditText edtEmail;
    public EditText edtEmailConfirm;
    public EditText edtCalle;
    public EditText edtNumExt;
    public EditText edtNumInt;
    public Spinner spnLugarN;
    public Spinner spnCompany;
    public Spinner spnEstado;
    private View v;
    private GuiTools guiTools;
    private CreaCuentaDelegate creaCuentaDelegate;
    private IdentificateDelegate identificateDelegate;
    private APICuentaDigitalSharePreferences sharePreferences;
    private ImageButton imbContinuar;
    private int day, month, year = 0;
    private Spinner spnColonias;
    private RadioButton cbMasculino;
    private RadioButton cbFemenino;

    private ImageView imgDatos;
    private ImageView imgContacto;
    private ImageView imgDomicilio;


    private LinearLayout lnDatos;
    private LinearLayout lnContacto;
    private LinearLayout lnDomicilio;
    private LinearLayout lnActual;
    private LinearLayout lnTelefono;

    private boolean datos;
    private boolean contacto;
    private boolean domicilio;
    private String json;

    private boolean inshowpicker = false;


    public IdentificateViewController(GuiTools guiTools, CreaCuentaDelegate creaCuentaDelegate, boolean isVisible, String consultaColonias) {
        this.guiTools = guiTools;
        this.creaCuentaDelegate = creaCuentaDelegate;
        identificateDelegate = new IdentificateDelegate(creaCuentaDelegate, this);
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();
        json = consultaColonias;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.layout_identificate, container, false);
        init();
        return v;
    }

    private void init() {
        findViewById();
        scaleView();
        listenerDate();
        setData();
        validaCurp();
        etiquetado();
    }

    private void etiquetado() {
        ArrayList<String> list = new ArrayList<String>();
        list.add(Constants.QUIERO_CUENTA);
        list.add(Constants.CREA_CUENTA);
        TrackingHelperCuentaDigital.trackState(list);
        Map<String, Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackingHelperCuentaDigital.trackEntrada(eventoEntrada);
        // TrackingHelper.trackState("identificate", new ArrayList<String>());
    }

    private void findViewById() {

        txtNombre = (TextView) v.findViewById(R.id.lbl_name);
        txtApaterno = (TextView) v.findViewById(R.id.lbl_aPaterno);
        txtAmaterno = (TextView) v.findViewById(R.id.lbl_aMaterno);
        txtFechaN = (TextView) v.findViewById(R.id.lbl_fecha);
        txtLugarN = (TextView) v.findViewById(R.id.lbl_lugar);
        txtCredencialE = (TextView) v.findViewById(R.id.lbl_credential);
        txtName = (TextView) v.findViewById(R.id.txt_name);
        txtGenero = (TextView) v.findViewById(R.id.txt_genero);
        edtNombre = (EditText) v.findViewById(R.id.edt_name);
        edtApaterno = (EditText) v.findViewById(R.id.edt_aPaterno);
        edtAmaterno = (EditText) v.findViewById(R.id.edt_aMaterno);
        edtFechaN = (EditText) v.findViewById(R.id.edt_fecha);
        edtCredencialE = (EditText) v.findViewById(R.id.edt_credential);
        edtCell = (EditText) v.findViewById(R.id.edt_cell);
        edtEmail = (EditText) v.findViewById(R.id.edt_email);
        edtEmailConfirm = (EditText) v.findViewById(R.id.edt_email_confirm);

        spnCompany = (Spinner) v.findViewById(R.id.spn_company);
        spnLugarN = (Spinner) v.findViewById(R.id.sp_lugar);

        cbMasculino = (RadioButton) v.findViewById(R.id.cb_masculino);
        cbFemenino = (RadioButton) v.findViewById(R.id.cb_femenino);

        txtCP = (TextView) v.findViewById(R.id.txt_cp);
        txtEstado = (TextView) v.findViewById(R.id.txt_state);

        edtCalle = (EditText) v.findViewById(R.id.edt_street);
        edtNumExt = (EditText) v.findViewById(R.id.edt_num_ext);
        edtNumInt = (EditText) v.findViewById(R.id.edt_num_int);

        txtMunicipios = (TextView) v.findViewById(R.id.txt_municipaly);
        spnColonias = (Spinner) v.findViewById(R.id.spn_colony);

        spnEstado = (Spinner) v.findViewById(R.id.spn_state);

        imbContinuar = (ImageButton) v.findViewById(R.id.imb_continue);

        imgContacto = (ImageView) v.findViewById(R.id.img_contacto);
        imgDomicilio = (ImageView) v.findViewById(R.id.img_domicilio);
        imgDatos = (ImageView) v.findViewById(R.id.img_datos);

        lnContacto = (LinearLayout) v.findViewById(R.id.datosContacto);
        lnDatos = (LinearLayout) v.findViewById(R.id.datos);
        lnDomicilio = (LinearLayout) v.findViewById(R.id.domicilio);
        lnTelefono = (LinearLayout) v.findViewById(R.id.ln_telefono);

        imbContinuar.setOnClickListener(this);

        imgDatos.setOnClickListener(this);
        imgDomicilio.setOnClickListener(this);
        imgContacto.setOnClickListener(this);

        lnActual = lnDatos;

        img_credencial = ((ImageView) v.findViewById(R.id.img_credencial));
        img_credencial.setOnClickListener(this);


        edtFechaN.setInputType(InputType.TYPE_NULL);
        edtFechaN.setOnTouchListener(new View.OnTouchListener() {

            @Override

            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    showPicker();
                } else if (MotionEvent.ACTION_UP == event.getAction()) {
                    inshowpicker = true;
                }
                return true;
            }
        });
        txtMunicipios.setText(sharePreferences.getStringData(Constants.TAG_DELEGACION));
        spnEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                spnEstado.setBackgroundResource(R.drawable.an_listboxcorto_magenta);
            }
        });
    }

    private void scaleView() {
        guiTools.scale(v.findViewById(R.id.txt_mesage), true);
        guiTools.scale(txtGenero, true);
        guiTools.scale(v.findViewById(R.id.lbl_cell), true);
        guiTools.scale(v.findViewById(R.id.lbl_company), true);
        guiTools.scale(v.findViewById(R.id.lbl_email), true);
        guiTools.scale(v.findViewById(R.id.lbl_email_confirm), true);
        guiTools.scale(v.findViewById(R.id.lbl_cp), true);
        guiTools.scale(v.findViewById(R.id.lbl_municipaly), true);
        guiTools.scale(v.findViewById(R.id.lbl_colony), true);
        guiTools.scale(v.findViewById(R.id.lbl_street), true);
        guiTools.scale(v.findViewById(R.id.lbl_num_int), true);
        guiTools.scale(v.findViewById(R.id.lbl_num_ext), true);
        guiTools.scale(txtNombre, true);
        guiTools.scale(txtAmaterno, true);
        guiTools.scale(txtApaterno, true);
        guiTools.scale(txtCP, true);
        guiTools.scale(txtCredencialE, true);
        guiTools.scale(txtEstado, true);
        guiTools.scale(txtFechaN, true);
        guiTools.scale(txtLugarN, true);
        guiTools.scale(txtName, true);
        guiTools.scale(edtAmaterno);
        guiTools.scale(edtApaterno);
        guiTools.scale(edtCalle);
        guiTools.scale(edtCell);
        guiTools.scale(edtCredencialE);
        guiTools.scale(edtEmail);
        guiTools.scale(edtEmailConfirm);
        guiTools.scale(edtFechaN);
        guiTools.scale(edtNombre);
        guiTools.scale(edtNumExt);
        guiTools.scale(edtNumInt);
        guiTools.scale(spnColonias);
        guiTools.scale(spnCompany);
        guiTools.scale(spnLugarN);
        guiTools.scale(spnEstado);
        guiTools.scale(txtMunicipios, true);

        guiTools.scale(v.findViewById(R.id.img_aviso));
        guiTools.scale(imgContacto);
        guiTools.scale(imgDatos);
        guiTools.scale(imgDomicilio);
        guiTools.scale(imbContinuar);
        guiTools.scale(img_credencial);
    }

    private void setData() {
        txtCP.setText(sharePreferences.getStringData(Constants.TAG_CODIGO_POSTAL));
        loadCompanies();
        loadStates();
    }

    private void loadColoniasNuew() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, identificateDelegate.getColonias());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnColonias.setAdapter(adapter);
    }

    private void loadCompanies() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Tools.getCompanias());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCompany.setAdapter(adapter);
    }

    private void loadStates() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Tools.getLugarN());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Gson gson = new Gson();
        ConsultaColoniasNueva colonias = gson.fromJson(json, ConsultaColoniasNueva.class);
        if (colonias == null) {
            spnEstado.setAdapter(adapter);
        } else {
            ConsultaColonias consultaColonias = colonias.getConsultaColonias();
            String[] estado = {Tools.getEstado(consultaColonias.getEntidad())};
            ArrayAdapter<String> adapterEstado = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, estado);
            adapterEstado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnEstado.setAdapter(adapterEstado);
            desSpinner();
            ArrayAdapter<String> adapterCol = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, consultaColonias.getColonias());
            adapterCol.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnColonias.setAdapter(adapterCol);
        }

        spnLugarN.setAdapter(adapter);

    }

    private void ocultarVista(LinearLayout ln) {
        ImageView img = null;
        ln.setVisibility(View.GONE);
        if (ln == lnDatos) {
            img = imgDatos;
            img.setImageResource(R.drawable.img_datospersonales_2);
            datos = true;
        } else if (ln == lnDomicilio) {
            img = imgDomicilio;
            img.setImageResource(R.drawable.img_domicilio_3);
            domicilio = true;
        } else if (ln == lnContacto) {
            img = imgContacto;
            img.setImageResource(R.drawable.img_datoscontacto_3);
            contacto = true;
        }
    }

    private void mostarVista(LinearLayout ln) {
        ImageView img = null;
        ln.setVisibility(View.VISIBLE);
        if (ln == lnDatos) {
            img = imgDatos;
            img.setImageResource(R.drawable.img_datospersonales_1);
            datos = false;
        } else if (ln == lnDomicilio) {
            img = imgDomicilio;
            domicilio = false;
            img.setImageResource(R.drawable.img_domicilio_1);

        } else if (ln == lnContacto) {
            img = imgContacto;
            contacto = false;
            img.setImageResource(R.drawable.img_datoscontacto_1);
        }
    }

    private void seleccionarItem(LinearLayout ln) {
        if (ln == lnDatos) {
            if (ln.isShown()) {
                ocultarVista(ln);
            } else
                mostarVista(ln);
        } else if (ln == lnContacto) {
            if (ln.isShown())
                ocultarVista(ln);
            else
                mostarVista(ln);
        } else if (ln == lnDomicilio) {
            if (ln.isShown())
                ocultarVista(ln);
            else
                mostarVista(ln);
        }
    }

    private void itemActual(LinearLayout lnClick) {
        boolean ok = false;
        if (lnActual == lnDatos) {
            if (datos)
                ok = true;
            else
                ok = validarDatosPersonales();
        } else if (lnActual == lnContacto) {
            if (contacto)
                ok = true;
            else
                ok = validarDatosContacto();
        } else if (lnActual == lnDomicilio) {
            if (domicilio)
                ok = true;
            else
                ok = validarDomicilio();
        }

        if (ok) {
            if (lnActual == lnClick) {
                seleccionarItem(lnClick);
            } else {
                ocultarVista(lnActual);
                mostarVista(lnClick);
            }
            lnActual = lnClick;
        } else {
            if (lnActual != lnClick) {
                if (!identificateDelegate.validaCorreo(edtEmailConfirm.getText().toString(), edtEmail.getText().toString()) && lnTelefono.isShown() && edtEmail.getText().toString().length()!=0 && edtEmailConfirm.getText().toString().length()!=0) {
                    if(!android.util.Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString()).matches()){
                        Intent alert = new Intent();
                        alert.setClass(getActivity(), PopUpGeneral.class);
                        alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.correo_valido));
                        getActivity().startActivityForResult(alert, 1234);
                    }else if(!edtEmail.getText().toString().equals(edtEmailConfirm.getText().toString())){
                        Intent alert = new Intent();
                        alert.setClass(getActivity(), PopUpGeneral.class);
                        alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.correo_no_coincide));
                        getActivity().startActivityForResult(alert, 1234);
                    }

                } else {
                    Intent alert = new Intent();
                    alert.setClass(getActivity(), PopUpGeneral.class);
                    alert.putExtra(Constants.MENSAJE, "Favor de informar todos los campos");
                    getActivity().startActivityForResult(alert, 1234);
                }
            }
        }
    }

    private void validarContinuar() {
        if (!identificateDelegate.validaCorreo(edtEmailConfirm.getText().toString(), edtEmail.getText().toString()) && lnTelefono.isShown() && edtEmail.getText().toString().length()!=0 && edtEmailConfirm.getText().toString().length()!=0) {
            if(!android.util.Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString()).matches()){
                Intent alert = new Intent();
                alert.setClass(getActivity(), PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.correo_valido));
                getActivity().startActivityForResult(alert, 1234);
            }else if(!edtEmail.getText().toString().equals(edtEmailConfirm.getText().toString())){
                Intent alert = new Intent();
                alert.setClass(getActivity(), PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.correo_no_coincide));
                getActivity().startActivityForResult(alert, 1234);
            }
        } else {
            if (activarBoton())
                alert();
            else {
                Intent alert = new Intent();
                alert.setClass(getActivity(), PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, "Favor de informar todos los campos");
                getActivity().startActivityForResult(alert, 1234);
            }
        }
    }


    @Override
    public void onClick(View v) {
        if (v == imbContinuar) {
            if (domicilioCompleto()) {
                validarContinuar();
            }
        } else if (imgDatos == v) {
            if (lnActual == lnContacto) {
                if (datosContactoCompletos()) {
                    itemActual(lnDatos);
                }
            } else if (lnActual == lnDomicilio) {
                if (domicilioCompleto()) {
                    itemActual(lnDatos);
                }
            } else {
                itemActual(lnDatos);
            }
        } else if (imgContacto == v) {
            if (lnActual == lnDomicilio) {
                if (domicilioCompleto()) {
                    itemActual(lnDatos);
                }
            } else if (datosPersonalesCompletos()) {
                itemActual(lnContacto);
            }
        } else if (imgDomicilio == v) {
            if (datosContactoCompletos()) {
                itemActual(lnDomicilio);
            }
        } else if (v == img_credencial) {
            creaCuentaDelegate.getCreaCuentaView().alertDialogAyudaCredencial();
        }
    }

    private boolean datosContactoCompletos() {
        Boolean retorno = Boolean.TRUE;
        if (edtCell.getText().toString().equals(Constants.EMPTY_STRING) && edtCell.isShown()) {
            edtCell.setBackgroundResource(R.drawable.an_textbox_magenta);
            listenerDateTwo();
            retorno = false;
        } else {
            edtCell.setBackgroundResource(R.drawable.an_textbox);
        }
        if (edtEmail.getText().toString().equals(Constants.EMPTY_STRING) && edtEmail.isShown()) {
            edtEmail.setBackgroundResource(R.drawable.an_textbox_magenta);
            listenerDateTwo();
            retorno = false;
        } else {
            edtEmail.setBackgroundResource(R.drawable.an_textbox);
        }
        if (edtEmailConfirm.getText().toString().equals(Constants.EMPTY_STRING) && edtEmailConfirm.isShown()) {
            edtEmailConfirm.setBackgroundResource(R.drawable.an_textbox_magenta);
            listenerDateTwo();
            retorno = false;
        } else {
            edtEmailConfirm.setBackgroundResource(R.drawable.an_textbox);
        }
        return retorno;

    }

    private boolean domicilioCompleto() {
        Boolean retorno = Boolean.TRUE;
        if (edtCalle.getText().toString().equals(Constants.EMPTY_STRING) && edtCalle.isShown()) {
            edtCalle.setBackgroundResource(R.drawable.an_textbox_magenta);
            listenerDateThree();
            retorno = false;
        } else {
            edtCalle.setBackgroundResource(R.drawable.an_textbox);
        }
        if (edtNumExt.getText().toString().equals(Constants.EMPTY_STRING) && edtNumExt.isShown()) {
            edtNumExt.setBackgroundResource(R.drawable.an_textbox_magenta);
            listenerDateThree();
            retorno = false;
        } else {
            edtNumExt.setBackgroundResource(R.drawable.an_textbox);
        }
        return retorno;
    }

    private boolean datosPersonalesCompletos() {
        Boolean retorno = Boolean.TRUE;
        if (edtNombre.getText().toString().equals(Constants.EMPTY_STRING) && edtNombre.isShown()) {
            edtNombre.setBackgroundResource(R.drawable.an_textbox_magenta);
            listenerDate();
            retorno = false;
        } else {
            edtNombre.setBackgroundResource(R.drawable.an_textbox);
        }
        if (edtApaterno.getText().toString().equals(Constants.EMPTY_STRING) && edtApaterno.isShown()) {
            edtApaterno.setBackgroundResource(R.drawable.an_textbox_magenta);
            listenerDate();
            retorno = false;
        } else {
            edtApaterno.setBackgroundResource(R.drawable.an_textbox);
        }
        if (edtAmaterno.getText().toString().equals(Constants.EMPTY_STRING) && edtAmaterno.isShown()) {
            edtAmaterno.setBackgroundResource(R.drawable.an_textbox_magenta);
            listenerDate();
            retorno = false;
        } else {
            edtAmaterno.setBackgroundResource(R.drawable.an_textbox);
        }
        if (edtFechaN.getText().toString().equals(Constants.EMPTY_STRING) && edtFechaN.isShown()) {
            edtFechaN.setBackgroundResource(R.drawable.an_textbox_magenta);
            listenerDate();
            retorno = false;
        } else {
            edtFechaN.setBackgroundResource(R.drawable.an_textbox);
        }
        if (edtCredencialE.getText().toString().equals(Constants.EMPTY_STRING) && edtCredencialE.isShown()) {
            edtCredencialE.setBackgroundResource(R.drawable.an_textbox_magenta);
            listenerDate();
            retorno = false;
        } else {
            edtCredencialE.setBackgroundResource(R.drawable.an_textbox);
        }

        return retorno;
    }

    private boolean validarDatosPersonales() {
        try {
            if (txtName.isShown()) {
                if (!edtCredencialE.getText().toString().equals("") && edtCredencialE.getText().toString().length() == 13) {
                    identificateDelegate.setCredencial(edtCredencialE.getText().toString());
                    datos = true;
                    return true;
                } else {
                    datos = false;
                    return false;

                }

            } else if (identificateDelegate.validaDatos(edtNombre.getText().toString(), edtApaterno.getText().toString(), edtAmaterno.getText().toString(), edtFechaN.getText().toString(), edtCredencialE.getText().toString(), validaSexo(), spnLugarN.getSelectedItem().toString())) {
                return true;
            } else {
                datos = false;
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private String validaSexo() {
        if (cbFemenino.isChecked())
            return Constants.FEMENINO;
        else if (cbMasculino.isChecked())
            return Constants.MASCULINO;
        else
            return Constants.EMPTY_STRING;
    }

    private boolean validarDatosContacto() {
        boolean n = false;
        if (lnTelefono.isShown()) {
            if (identificateDelegate.validaContacto(edtCell.getText().toString(), edtEmail.getText().toString(), edtEmailConfirm.getText().toString(), spnCompany.getSelectedItem().toString()))
                n = true;

        } else {
            if (identificateDelegate.validaCorreo(edtEmailConfirm.getText().toString(), edtEmail.getText().toString()))
                n = true;
        }
        return n;
    }

    private boolean validarDomicilio() {
        if (identificateDelegate.validaDomicilio(edtCalle.getText().toString(), edtNumExt.getText().toString(), edtNumInt.getText().toString(), spnColonias.getSelectedItem().toString())) {
            return true;
        } else {
            return false;
        }
    }


    private void validaCurp() {
        if (APICuentaDigitalSharePreferences.getInstance().getBooleanData(Constants.OK_CONSULTA_CURP)) {
            txtApaterno.setVisibility(View.GONE);
            txtAmaterno.setVisibility(View.GONE);
            txtFechaN.setVisibility(View.GONE);
            txtLugarN.setVisibility(View.GONE);
            txtGenero.setVisibility(View.GONE);
            cbMasculino.setVisibility(View.GONE);
            cbFemenino.setVisibility(View.GONE);
            edtAmaterno.setVisibility(View.GONE);
            edtApaterno.setVisibility(View.GONE);
            edtNombre.setVisibility(View.GONE);
            edtFechaN.setVisibility(View.GONE);
            spnLugarN.setVisibility(View.GONE);

            txtName.setVisibility(View.VISIBLE);
            txtName.setText(Tools.getName());
        }
    }

    private void alert() {
        Intent intent = new Intent();
        intent.setClass(getActivity(), PopUpConfirmacion.class);
        intent.putExtra("celular", sharePreferences.getStringData(Constants.CELULAR));
        intent.putExtra("compania", sharePreferences.getStringData(Constants.COMPANIA));
        this.startActivityForResult(intent, 1234);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data.getExtras().getBoolean("aceptar")) {
            aceptar();
        } else if (data.getExtras().getBoolean("modificar")) {
            modificar();
        }
    }

    public void desSpinner() {
        spnEstado.setEnabled(false);

    }

    public void generaOTP() {//JACT

        String com = "";
        String tel = "";

        if (sharePreferences.getStringData(Constants.TAG_COMPANIA) == null &&
                sharePreferences.getStringData(Constants.CELULAR) == null) {
            com = spnCompany.getSelectedItem().toString().toUpperCase();
            tel = edtCell.getText().toString();
        } else {
            com = sharePreferences.getStringData(Constants.TAG_COMPANIA);
            tel = sharePreferences.getStringData(Constants.CELULAR);
        }
        identificateDelegate.realizaOperacion(tel,
                com, edtEmail.getText().toString());
    }

    public void modificar() {
        lnTelefono.setVisibility(View.VISIBLE);
        edtCell.setText(sharePreferences.getStringData(Constants.CELULAR));
        findCompany();
        mostarVista(lnContacto);
        ocultarVista(lnDatos);
        ocultarVista(lnDomicilio);
        lnActual = lnContacto;
    }

    public void aceptar() {
        if (sharePreferences.getBooleanData(Constants.OK_CONSULTA_CURP)) {
            generaOTP();
        } else {
            identificateDelegate.realizaOperacion(edtNombre.getText().toString(),
                    edtApaterno.getText().toString(), edtAmaterno.getText().toString(), edtFechaN.getText().toString(),
                    spnLugarN.getSelectedItem().toString(), cbFemenino.isChecked() ? Constants.FEMENINO : Constants.MASCULINO);
        }
    }

    public void irSetColoniasEstado(String colonias) {

        Gson gson = new Gson();
        identificateDelegate = new IdentificateDelegate(creaCuentaDelegate, this);
        identificateDelegate.obtenerInformacionColonias(gson.fromJson(colonias, ConsultaColoniasNueva.class));

        txtMunicipios.setText(sharePreferences.getStringData(Constants.TAG_DELEGACION));

        loadColoniasNuew();
    }

    private void findCompany() {

        spnCompany.setSelection(Tools.getPositionCompany(sharePreferences.getStringData(Constants.COMPANIA)));
    }

    private void listenerDate() {
        edtNombre.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    edtNombre.setBackgroundResource(R.drawable.an_textbox);
                    edtNombre.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtNombre.setBackgroundResource(R.drawable.an_textbox_magenta);
                    edtNombre.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtNombre.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtApaterno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    edtApaterno.setBackgroundResource(R.drawable.an_textboxcorto);
                    edtApaterno.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtApaterno.setBackgroundResource(R.drawable.an_textboxcorto_magenta);
                    edtApaterno.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtApaterno.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtAmaterno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    edtAmaterno.setBackgroundResource(R.drawable.an_textboxcorto);
                    edtAmaterno.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtAmaterno.setBackgroundResource(R.drawable.an_textboxcorto_magenta);
                    edtAmaterno.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtAmaterno.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtFechaN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    edtFechaN.setBackgroundResource(R.drawable.an_textboxcorto);
                    edtFechaN.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtFechaN.setBackgroundResource(R.drawable.an_textboxcorto_magenta);
                    edtFechaN.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtFechaN.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtCredencialE.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    edtCredencialE.setBackgroundResource(R.drawable.an_textbox);
                    edtCredencialE.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtCredencialE.setBackgroundResource(R.drawable.an_textbox_magenta);
                    edtCredencialE.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtCredencialE.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

    }

    private void listenerDateTwo() {
        edtCell.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    edtCell.setBackgroundResource(R.drawable.an_textbox);
                    edtCell.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtCell.setBackgroundResource(R.drawable.an_textbox_magenta);
                    edtCell.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtCell.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    edtEmail.setBackgroundResource(R.drawable.an_textbox);
                    edtEmail.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtEmail.setBackgroundResource(R.drawable.an_textbox_magenta);
                    edtEmail.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtEmail.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtEmailConfirm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    edtEmailConfirm.setBackgroundResource(R.drawable.an_textbox);
                    edtEmailConfirm.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtEmailConfirm.setBackgroundResource(R.drawable.an_textbox_magenta);
                    edtEmailConfirm.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtEmailConfirm.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

    }

    private void listenerDateThree() {


        edtCalle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    edtCalle.setBackgroundResource(R.drawable.an_textbox);
                    edtCalle.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtCalle.setBackgroundResource(R.drawable.an_textbox_magenta);
                    edtCalle.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtCalle.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtNumExt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    edtNumExt.setBackgroundResource(R.drawable.an_textbox);
                    edtNumExt.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtNumExt.setBackgroundResource(R.drawable.an_textbox_magenta);
                    edtNumExt.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtNumExt.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtNumInt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    edtNumInt.setBackgroundResource(R.drawable.an_textbox);
                    edtNumInt.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtNumInt.setBackgroundResource(R.drawable.an_textbox_magenta);
                    edtNumInt.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtNumInt.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });


    }

    public void validateDate(String date) {
        int lenght = date.length();
        switch (lenght) {
            case 3: {
                if (Character.isDigit(date.charAt(2))) {
                    edtFechaN.setText(Tools.putFirstSlash(date));
                } else {
                    edtFechaN.setText(Tools.removeFirstSlash(date));
                }
            }
            break;
            case 6: {
                if (Character.isDigit(date.charAt(5))) {
                    edtFechaN.setText(Tools.putSecondSlash(date));
                } else {
                    edtFechaN.setText(Tools.removeSecondSlash(date));
                }
            }
            break;
        }
        edtFechaN.setSelection(edtFechaN.getText().toString().length());
    }

    private boolean activarBoton() {
        boolean allData = false;
        if (lnContacto.isShown()) {
            if (validarDatosContacto() && datos && domicilio) {
                allData = true;
            }
        } else if (lnDatos.isShown()) {
            if (validarDatosPersonales() && contacto && domicilio) {
                allData = true;
            }
        } else if (lnDomicilio.isShown()) {
            if (validarDomicilio() && datos && contacto) {
                allData = true;
            }
        } else if (datos && contacto && domicilio) {
            allData = true;
        }
        return allData;
    }


    private void showPicker() {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        final int menos18 = 18;
        if (inshowpicker) {
            return;
        }
        /**
         * There's a problem when using "lollipop" with DatePickerDialog, so it's necessary to check which SDK level the device has.
         */
        final Calendar calendar = Calendar.getInstance();
        final View dialogView = View.inflate(getActivity(), R.layout.api_cuenta_digital_date_picker, null);
        final DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.datePicker);

        datePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -18);
        datePicker.setMaxDate(cal.getTime().getTime());

        if (sdk >= 21) {

            if (year == 0) {
                datePicker.init(calendar.get(Calendar.YEAR) - menos18, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);

            } else {
                datePicker.init(year, month - 1, day, null);
            }

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.bmovil_popup_button_accept), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int arg1) {

                    year = datePicker.getYear();
                    month = datePicker.getMonth() + 1;
                    day = datePicker.getDayOfMonth();
                    if (year > calendar.get(Calendar.YEAR) - 18) {
                        datePicker.init(calendar.get(Calendar.YEAR) - menos18, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);
                        year = datePicker.getYear();
                        month = datePicker.getMonth() + 1;
                        day = datePicker.getDayOfMonth();
                    } else if (year == calendar.get(Calendar.YEAR) - 18 && month > calendar.get(Calendar.MONTH) + 1) {
                        datePicker.init(calendar.get(Calendar.YEAR) - menos18, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);
                        year = datePicker.getYear();
                        month = datePicker.getMonth() + 1;
                        day = datePicker.getDayOfMonth();
                    } else if (year == calendar.get(Calendar.YEAR) - 18 && month == calendar.get(Calendar.MONTH) + 1 && day > calendar.get(Calendar.DAY_OF_MONTH)) {
                        datePicker.init(calendar.get(Calendar.YEAR) - menos18, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);
                        year = datePicker.getYear();
                        month = datePicker.getMonth() + 1;
                        day = datePicker.getDayOfMonth();
                    }

                    setDate();
                    dialog.dismiss();

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.setView(dialogView);
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    inshowpicker = false;
                }
            });
            alertDialog.show();


        } else {
            final DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0, int y, int m, int d) {
                    year = y;
                    month = m + 1;
                    day = d;
                    inshowpicker = false;
                    if (year > calendar.get(Calendar.YEAR) - 18) {
                        datePicker.init(calendar.get(Calendar.YEAR) - menos18, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);
                        year = datePicker.getYear();
                        month = datePicker.getMonth() + 1;
                        day = datePicker.getDayOfMonth();
                    } else if (year == calendar.get(Calendar.YEAR) - 18 && month > calendar.get(Calendar.MONTH) + 1) {
                        datePicker.init(calendar.get(Calendar.YEAR) - menos18, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);
                        year = datePicker.getYear();
                        month = datePicker.getMonth() + 1;
                        day = datePicker.getDayOfMonth();
                    } else if (year == calendar.get(Calendar.YEAR) - 18 && month == calendar.get(Calendar.MONTH) + 1 && day > calendar.get(Calendar.DAY_OF_MONTH)) {
                        datePicker.init(calendar.get(Calendar.YEAR) - menos18, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);
                        year = datePicker.getYear();
                        month = datePicker.getMonth() + 1;
                        day = datePicker.getDayOfMonth();
                    }
                    setDate();
                }
            };


            if (year == 0) {

                year = calendar.get(Calendar.YEAR) - menos18;
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog f = new DatePickerDialog(getActivity(), pickerListener, year, month, day);
                f.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        inshowpicker = false;
                    }
                });
                f.show();

            } else {
                DatePickerDialog f = new DatePickerDialog(getActivity(), pickerListener, year, month - 1, day);
                f.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        inshowpicker = false;
                    }
                });
                f.show();

            }

        }
    }

    private void setDate() {
        if (day < 10 && month < 10)
            edtFechaN.setText(new StringBuilder().append("0" + day).append("/").append("0" + month).append("/").append(year));
        else if (month < 10)
            edtFechaN.setText(new StringBuilder().append(day).append("/").append("0" + month).append("/").append(year));
        else if (day < 10)
            edtFechaN.setText(new StringBuilder().append("0" + day).append("/").append(month).append("/").append(year));
        else
            edtFechaN.setText(new StringBuilder().append(day).append("/").append(month).append("/").append(year));


    }

}


