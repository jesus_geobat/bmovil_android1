package com.bbva.apicuentadigital.controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bancomer.mbanking.softtoken.SuiteAppApi;
import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;

/**
 * Created by mcamarillo on 24/08/16.
 */
public class PopUpGeneral extends Activity {
    /**
     * mensaje
     */
    TextView mensaje;
    /**
     * boton
     */
    Button aceptar;
    /**
     * extras
     */
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_popup_general);
        extras = getIntent().getExtras();
        findViews();
    }

    private void findViews() {
        final Session session = Session.getInstance(this);
        mensaje = (TextView)findViewById(R.id.mensaje);
        mensaje.setText(extras.getString(Constants.MENSAJE));
        aceptar = (Button) findViewById(R.id.aceptar);
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(extras.getString(Constants.FINAL)!=null){
                    session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CONTRATAR_CUENTA_DIGITAL, false, true);
                    SuiteAppApi.getIntentToReturn().returnObjectFromApi(null);
                }else {
                    Intent i = new Intent();
                    i.putExtra("regresar", false);
                    i.putExtra("aceptar", false);
                    i.putExtra("modificar", false);
                    setResult(RESULT_OK, i);
                    finish();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
    }
}
