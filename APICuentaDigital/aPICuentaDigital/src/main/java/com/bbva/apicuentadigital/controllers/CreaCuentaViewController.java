package com.bbva.apicuentadigital.controllers;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.APICuentaDigital;
import com.bbva.apicuentadigital.common.BaseActivity;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.models.ConsultaColoniasCurp;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;
import com.google.gson.Gson;

import bancomer.api.common.timer.TimerController;

/**
 * Created by Karina on 07/12/2015.
 */
public class CreaCuentaViewController extends BaseActivity {

    public CreaCuentaDelegate creaCuentaDelegate;
    private Fragment fragment;
    private FragmentManager fm;
    private GuiTools guiTools;
    private String json;
    private String cp;
    private String compania;
    private String celular;
    private Boolean confirmacion;

    private boolean isVisible;
    private Bundle extras;
    private ImageView imgTitle;
    private Constants.VISTA actual;
    private ImageButton imbBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_crea_cuenta_digital);
        extras = getIntent().getExtras();
        json = extras.getString(Constants.CONSULTA_COLONIA_CURP);
        cp = extras.getString(Constants.TAG_CODIGO_POSTAL);
        compania = extras.getString(Constants.COMPANIA);
        celular = extras.getString(Constants.CELULAR);
        confirmacion = extras.getBoolean("confirmacion");

        init();
    }


    private void init() {
        fm = getFragmentManager();
        findViewById();
        scaleView();
        validarCelular();
        //Mario IDS
        if (extras.getBoolean("appDesacExito")) {
            creaCuentaDelegate = new CreaCuentaDelegate(this, cp, compania, celular);
            exito(true);
        } else if (extras.getString("firma") == null) {
            validarConfirmacion();
        } else if (extras.getString("firma").equals("si")) {
            creaCuentaDelegate = new CreaCuentaDelegate(this, cp, compania, celular);
            firma(true);
        } else {
            creaCuentaDelegate = new CreaCuentaDelegate(this, cp, compania, celular);
            firma(false);
        }
    }

    private void findViewById() {
        imgTitle = (ImageView) findViewById(R.id.img_title);
        imbBack = (ImageButton) findViewById(R.id.imb_back);
    }

    private void scaleView() {
        guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
    }

    public void identificate() {
        actual = Constants.VISTA.Identificate;
        Fragment fragment = new IdentificateViewController(guiTools, creaCuentaDelegate, isVisible, extras.getString(Constants.CONSULTA_COLONIA));
        fm.beginTransaction().replace(R.id.frame, fragment).commit();
    }

    public void clausulas() {
        actual = Constants.VISTA.Clausulas;
        imgTitle.setImageResource(R.drawable.img_estado_2);
        this.fragment = new ClausulasViewController(guiTools, creaCuentaDelegate);
        fm.beginTransaction().replace(R.id.frame, fragment).commit();
    }

    public void confirmacion() {
        actual = Constants.VISTA.Confirmacion;
        Fragment fragment = new ConfirmacionViewController(guiTools, creaCuentaDelegate);
        fm.beginTransaction().replace(R.id.frame, fragment).commitAllowingStateLoss();
    }

    public void datosConfirmados() {
        actual = Constants.VISTA.DatosConfirmados;
        imbBack.setVisibility(View.GONE);
        Fragment fragment = new DatosConfirmadosViewController(guiTools, creaCuentaDelegate);
        fm.beginTransaction().replace(R.id.frame, fragment).commit();
    }

    public void exito(final boolean nextActivaded) {
        actual = Constants.VISTA.Exito;
        imbBack.setVisibility(View.GONE);
        imgTitle.setImageResource(R.drawable.img_estado_3);
        Fragment fragment = new ExitoViewController(guiTools, creaCuentaDelegate, nextActivaded);
        fm.beginTransaction().replace(R.id.frame, fragment).commit();
    }

    public void firma(final boolean nextActivaded) {
        actual = Constants.VISTA.Firma;
        imbBack.setVisibility(View.GONE);
        Fragment fragment = new FirmaCDViewController(guiTools, creaCuentaDelegate, nextActivaded);
        fm.beginTransaction().replace(R.id.frame, fragment).commit();
    }

    public void cargarFragment() {
        actual = Constants.VISTA.Clausulas;
        imbBack.setVisibility(View.VISIBLE);
        if (this.fragment == null) {
            Fragment fragment = new ClausulasViewController(guiTools, creaCuentaDelegate);
            fm.beginTransaction().replace(R.id.frame, fragment).commit();
        } else {
            fm.beginTransaction().replace(R.id.frame, this.fragment).commit();
        }
    }

    private void cargarDatos() {
        Gson gson = new Gson();
        creaCuentaDelegate = new CreaCuentaDelegate(this, cp, compania, celular);
        if (json == null) {

            APICuentaDigitalSharePreferences.getInstance().setBooleanData(Constants.OK_CONSULTA_CURP, false);

        } else {
            creaCuentaDelegate.obtenerInformacion(gson.fromJson(json, ConsultaColoniasCurp.class));
        }
        identificate();
    }

    private void validarConfirmacion() {
        if (confirmacion) {
            creaCuentaDelegate = new CreaCuentaDelegate(this, cp, compania, celular);
            confirmacion();
        } else
            cargarDatos();
    }

    private void validarCelular() {
        if (celular != null && !celular.equals("")) {
            isVisible = true;
        }
    }

    @Override
    public void onBackPressed() {

        switch (actual) {
            case Identificate:
                vistaIdentificate();
                break;
            case Clausulas:
                datosConfirmados();
                break;
            case Confirmacion:
                break;
            case Firma:
                break;
            case Exito:
                break;
        }
    }


    public void onClick(View v) {
        onBackPressed();
    }

    private void vistaIdentificate() {
        APICuentaDigitalSharePreferences.getInstance().deleteAll();
        Intent intent = new Intent();
        intent.setClass(this, PopUpSalir.class);
        this.startActivityForResult(intent, 1234);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data.getExtras() != null) {
            if (data.getExtras().getBoolean("regresar")) {
                Intent intent = new Intent(this, QuieroCuentaViewController.class);
                intent.putExtra(Constants.CELULAR, APICuentaDigitalSharePreferences.getInstance().getStringData(Constants.CELULAR));
                intent.putExtra(Constants.COMPANIA, APICuentaDigitalSharePreferences.getInstance().getStringData(Constants.COMPANIA));
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        APICuentaDigital.appContext = this;
    }
}
