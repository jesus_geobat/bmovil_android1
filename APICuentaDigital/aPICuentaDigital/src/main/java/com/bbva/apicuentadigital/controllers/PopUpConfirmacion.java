package com.bbva.apicuentadigital.controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.APICuentaDigital;

/**
 * Created by mcamarillo on 09/07/16.
 */
public class PopUpConfirmacion extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_popup_confirmacion);
        Bundle extras = getIntent().getExtras();
        TextView txtMessage = (TextView) findViewById(R.id.lblMessage);
        /*txtMessage.setText(Html.fromHtml("<font>Te enviaremos un código de seguridad por SMS al número celular: </font> <b><font color='" + getResources().getColor(R.color.colorBlueDark) +"'>"+celular+"</font></b>" +
                "<font> de la compañia <b><font color='" + getResources().getColor(R.color.colorBlueDark) +"'>"+compania+"</font><font>  al correo electrónico </font></b><b><font color='" + getResources().getColor(R.color.colorBlueDark) +"'>"+correo+"</font></b>"));*/
        txtMessage.setText("Por favor confirma tu número celular donde enviaremos tu código para activar tu cuenta:");

        Button btnNow = (Button) findViewById(R.id.btnLate);
        btnNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent();
                i.putExtra("modificar", true);
                i.putExtra("aceptar", false);
                setResult(RESULT_OK, i);
                finish();

            }
        });

        Button btnLate = (Button) findViewById(R.id.btnNow);
        btnLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent();
                i.putExtra("aceptar", true);
                i.putExtra("modificar", false);
                setResult(RESULT_OK, i);
                finish();

            }
        });

        TextView edtCel = (TextView) findViewById(R.id.edtCel);
        edtCel.setText(extras.getString("celular"));

        TextView edtCompania = (TextView) findViewById(R.id.edtCompania);
        edtCompania.setText(extras.getString("compania"));

    }


    @Override
    protected void onResume() {
        super.onResume();
        APICuentaDigital.appContext=this;
    }

    @Override
    public void onBackPressed() {
    }
}