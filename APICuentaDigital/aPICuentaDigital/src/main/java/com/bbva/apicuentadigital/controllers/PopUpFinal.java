package com.bbva.apicuentadigital.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bancomer.mbanking.softtoken.SuiteAppApi;
import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.APICuentaDigital;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;

/**
 * Created by leleyvah on 24/06/2016.
 */


public class PopUpFinal extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_bmovil_activado);

        ImageView ConfirmarCuenta = (ImageView)findViewById(R.id.imagen_confirmar);
        ConfirmarCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
               goAppDesac();

            }
        });
    }

    private void goAppDesac() {
        SuiteAppApi.getIntentToReturn().returnObjectFromApi(null);
    }


    @Override
    protected void onResume() {
        super.onResume();
        APICuentaDigital.appContext=this;
    }
    
}