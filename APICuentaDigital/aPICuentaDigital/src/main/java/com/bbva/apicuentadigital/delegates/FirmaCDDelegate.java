package com.bbva.apicuentadigital.delegates;

import android.content.Intent;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.controllers.FirmaCDViewController;
import com.bbva.apicuentadigital.controllers.PopUpGeneral;
import com.bbva.apicuentadigital.controllers.WebViewController;
import com.bbva.apicuentadigital.io.Server;
import com.bbva.apicuentadigital.io.ServerResponse;
import com.bbva.apicuentadigital.models.Signing;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

import java.util.Hashtable;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;


/**
 * Created by mcamarillo on 05/09/16.
 */
public class FirmaCDDelegate extends BaseDelegate {
    /**
     * delegate base
     */
    private CreaCuentaDelegate creaCuentaDelegate;
    /**
     * controller
     */
    private FirmaCDViewController firmaController;

    public FirmaCDDelegate(CreaCuentaDelegate creaCuentaDelegate, FirmaCDViewController firmaCDViewController){
        this.creaCuentaDelegate = creaCuentaDelegate;
        this.firmaController = firmaCDViewController;
    }

    public void signingRequest(final String sign,final String ium){
        APICuentaDigitalSharePreferences sharePreferences = APICuentaDigitalSharePreferences.getInstance();
        Hashtable<String,String> params = new Hashtable<>();
        params.put(Constants.TELEFONO_PARAM,sharePreferences.getStringData(Constants.CELULAR));
        params.put(Constants.FIRMA_PARAM,sign);
        Server server = Server.getInstance(this);
        server.doNetWorkOperation(ApiConstants.OP_SIGNING, params);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        super.analyzeResponse(operationId, response);
        if(response.getCODE()!=null) {
            if (response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
                //Signing signObject = (Signing) response.getResponse();
                    Session session = Session.getInstance(creaCuentaDelegate.getCreaCuentaView());
                    session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_FIRMA_DIGITAL, false, true);
                    firmaController.exito();
            } else {
                //String mesagge="";
                int asFinal;
                asFinal = response.getMESSAGE().indexOf("**");
                if (asFinal > -1) {
                    Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
                    alert.putExtra(Constants.MENSAJE, response.getMESSAGE().substring(0, asFinal));
                    creaCuentaDelegate.getCreaCuentaView().hideActivityIndicator();
                    creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
                } else {
                    Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
                    alert.putExtra(Constants.MENSAJE, response.getMESSAGE());
                    creaCuentaDelegate.getCreaCuentaView().hideActivityIndicator();
                    creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
                }
            }
        } else {
            Intent alert = new Intent(creaCuentaDelegate.getCreaCuentaView(), PopUpGeneral.class);
            alert.putExtra(Constants.MENSAJE, "Error de comunicaciones");
            creaCuentaDelegate.getCreaCuentaView().hideActivityIndicator();
            creaCuentaDelegate.getCreaCuentaView().startActivityForResult(alert, 1234);
        }
    }
    public void showTerminosFirma(String urlFirma){
        Intent intent = new Intent(firmaController.getActivity(), WebViewController.class);
        intent.putExtra(Constants.OPERATION, Constants.TERMINOS);
        intent.putExtra(Constants.URL, urlFirma);
        firmaController.getActivity().startActivity(intent);
    }

}
