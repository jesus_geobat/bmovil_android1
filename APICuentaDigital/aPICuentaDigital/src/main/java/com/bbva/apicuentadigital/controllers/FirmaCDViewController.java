package com.bbva.apicuentadigital.controllers;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.delegates.FirmaCDDelegate;

import suitebancomercoms.classes.gui.delegates.FirmaDelegate;

/**
 * Created by mcamarillo on 05/09/16.
 */
public class FirmaCDViewController extends Fragment implements View.OnClickListener {

    /**
     * codigo exito firma
     */
    int FIRMA_CODE = 123;
    /**
     * base 64 de imagen
     */
    String imageBase64 = Constants.EMPTY_STRING;
    /**
     * firma delegate
     */
    FirmaDelegate firmaD;
    /**
     * tools para ajustar los componentes en la pantalla
     */
    private GuiTools guiTools;
    /**
     * delegate
     */
    private CreaCuentaDelegate creaCuentaDelegate;
    /**
     * delegate general
     */
    private FirmaCDDelegate firmaCDDelegate;
    /**
     * vista
     */
    private View rootView;

    //private ImageView imageFirma;
    /**
     * terminos
     */
    private TextView terminos;
    /**
     * check terminos
     */
    private CheckBox checkTerminos;
    /**
     * firma
     */
    private LinearLayout firma;
    /**
     * boton aceptar
     */
    private ImageButton aceptar;
    /**
     * booleano exito activado
     */
    private Boolean exitoActivado;

    /**
     * constructor
     *
     * @param guiTools
     * @param creaCuentaDelegate
     */
    public FirmaCDViewController(final GuiTools guiTools, final CreaCuentaDelegate creaCuentaDelegate, final boolean exitoActivado) {
        this.creaCuentaDelegate = creaCuentaDelegate;
        this.guiTools = guiTools;
        this.exitoActivado = exitoActivado;
        firmaCDDelegate = new FirmaCDDelegate(creaCuentaDelegate, this);
    }

    /**
     * Metodo donde se crea la vista
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_firma, container, false);
        findViewById();

        creaCuentaDelegate.getCreaCuentaView().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        View focusedView = getActivity().getCurrentFocus();
        if (focusedView != null) {
            InputMethodManager input = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            input.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }
        firmaD = new FirmaDelegate(getActivity(), this);
        firmaD.generateViewSignature(firma, FIRMA_CODE);
        //etiquetado();
        return rootView;
    }

   /* private void etiquetado() {
        TrackingHelper.trackState("firma", new ArrayList<String>());
    }*/

    /**
     * asociar vista
     */
    private void findViewById() {
        terminos = (TextView) rootView.findViewById(R.id.terminos);
        checkTerminos = (CheckBox) rootView.findViewById(R.id.check_firma);
        firma = (LinearLayout) rootView.findViewById(R.id.firma);
        aceptar = (ImageButton) rootView.findViewById(R.id.continuar);
        String text = "";
        text = terminos.getText().toString();
        SpannableString txtTermino = new SpannableString(text);
        txtTermino.setSpan(new StyleSpan(Typeface.BOLD), 22, 44, 0);
        txtTermino.setSpan(new UnderlineSpan(), 22, 44, 0);
        txtTermino.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.azul_3)), 22, text.length() - 12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        terminos.setText(txtTermino);
        firma.setOnClickListener(this);
        aceptar.setOnClickListener(this);
        terminos.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        if (terminos == v) {
            firmaCDDelegate.showTerminosFirma("web_view_firma");
        } else if (v.equals(aceptar)) {
            if (imageBase64.equals(Constants.EMPTY_STRING)) {
                Intent alert = new Intent(getActivity(), PopUpGeneral.class);
                alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.sin_ingresa_firma));
                getActivity().startActivityForResult(alert, 1234);
            } else {
                if (!checkTerminos.isChecked()) {
                    Intent alert = new Intent(getActivity(), PopUpGeneral.class);
                    alert.putExtra(Constants.MENSAJE, getActivity().getString(R.string.contrata_alerta_no_terminos));
                    getActivity().startActivityForResult(alert, 1234);
                } else {
                    creaCuentaDelegate.getCreaCuentaView().showActivityIndicator();
                    firmaCDDelegate.signingRequest(imageBase64, Constants.EMPTY_STRING);
                }
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == FIRMA_CODE) {
                imageBase64 = firmaD.returnViewSignature(data.getExtras());
                firma.setEnabled(Boolean.FALSE);
                Log.i("onActivityResult: ", imageBase64);
            }
        }

    }

    public void exito() {
        creaCuentaDelegate.getCreaCuentaView().hideActivityIndicator();
        creaCuentaDelegate.getCreaCuentaView().exito(exitoActivado);
    }

}
