package com.bbva.apicuentadigital.common;

/**
 * Created by Karina on 07/12/2015.
 */
public class Constants {

    public static String TAG_OPERACIONES = "Operaciones";
    public static String TAG_CONSULTACOLONIAS = "consultaColonias";
    public static String TAG_CODE = "code";
    public static String TAG_DESCRIPTION = "description";
    public static String TAG_CPVALIDO = "cpValido";
    public static String TAG_ENTIDAD = "entidad";
    public static String TAG_DELEGACION = "delegacion";
    public static String TAG_COLONIAS = "colonias";
    public static String TAG_CONSULTACURP = "consultaCurp";
    public static String TAG_CURPVALIDO = "curpValido";
    public static String TAG_NOMBRES = "nombres";
    public static String TAG_APATERNO = "aPaterno";
    public static String TAG_AMATERNO = "aMaterno";
    public static String TAG_SEXO = "sexo";
    public static String TAG_FECHANAC = "fechNac";
    public static String TAG_CVEENTIDADNAC = "cveEntidadNac";
    public static String TAG_VALIDACURP = "validaCurp";
    public static String TAG_ENCONTROCURP = "encontroCURP";
    public static String TAG_CURP = "curp";
    public static String TAG_CVE_CURP = "cveCURP";
    public static String TAG_CURPSTATUSB = "curpStatusB";
    public static String TAG_STATUS = "status";
    public static String TAG_RESPONSE = "response";
    public static String TAG_OTP_ENVIADAS = "otpEnviadas";
    public static String TAG_TITULAR = "titular";
    public static String TAG_TARJETA = "tarjeta";
    public static String TAG_CUENTA = "cuenta";
    public static String TAG_CUENTA_CLABE = "cuentaClabe";
    public static String TAG_LUGARN = "lugar_nacimiento";
    public static String OK_VALIDA_CURP = "VALIDA_CURP";
    public static String OK_CONSULTA_CURP = "VALIDA_CURP";
    public static String TAG_CREDENTIAL = "credential";
    public static String TAG_NUMERO_SERIE = "numeroSerie";
    public static String TAG_NOMBRE_TOKEN = "numeroToken";
    public static String TAG_CONTRATACION_CUENTA = "contratacionCuenta";
    public static String TAG_CONTRATACION_CANAL = "contratacionCanal";
    public static String TAG_SOLICITUD_TOKEN = "solicitudToken";
    public static String TAG_AUTENTICACION_TOKEN = "autenticacionToken";

    public static final String TAG_IND_CP = "indCP";
    public static final String TAG_IND_RENAPO = "indRENAPO";
    public static final String TAG_CODIGO_POSTAL = "codigoPostal";


    public static final String TAG_NUMUERO_CELULAR = "numeroCelular";
    public static final String TAG_ID_PRODUCTO = "idProducto";
    public static final String TAG_IND_VACIO = "indVacio";
    public static final String TAG_CONTRATO = "contrato";

    public static final String COMPANIA_VIRGIN_MOBILE = "VIRGIN MOBILE";
    public static final String COMPANIA_MOVISTAR = "MOVISTAR";
    public static final String TAG_COMPANIA = "compania";
    public static final String TAG_MAIL = "mail";
    public static final String TAG_NOMBRE = "nombre";
    public static final String TAG_PRIMER_APELLIDO = "primerApellido";
    public static final String TAG_SEGUNDO_APELLIDO = "segundoApellido";
    public static final String TAG_FECHA_NACIEMIENTO = "fechaNacimiento";

    public static final String TAG_CALLE = "calle";
    public static final String TAG_COLONIA = "colonia";
    public static final String TAG_NUMEXT = "numExt";
    public static final String TAG_NUMINT = "numInt";
    public static final String TAG_ESTADO = "estado";
    public static final String TAG_PAIS = "pais";
    public static final String TAG_NUMEROID = "numeroId";
    public static final String TAG_OTPSMS = "otpCuentaDigital";
    public static final String TAG_OTPMAIL = "otpMail";
    public static final String TAG_IUM = "ium";

    public static final String CONSULTA_COLONIA_CURP = "consultaColoniasCurp";
    public static final String CONSULTA_COLONIA = "consultaColonia";

    public static final String TAG_ACCESSCODE= "accessCode";


    public static final String CODE_OK = "200";

    public static enum VISTA{
        Identificate,
        Clausulas,
        Confirmacion,
        Firma,
        Exito,
        DatosConfirmados
    }

    public static final String AS = "Aguascalientes";
    public static final String BC = "Baja California";
    public static final String BS = "Baja California Sur";
    public static final String CC = "Campeche";
    public static final String CL = "Coahulila";
    public static final String CM = "Colima";
    public static final String CS = "Chiapas";
    public static final String CH = "Chihuahua";
    public static final String DF = "Distrito Federal";
    public static final String DG = "Durango";
    public static final String GT = "Guanajuato";
    public static final String GR = "Guerrero";
    public static final String HG = "Hidalgo";
    public static final String JC = "Jalisco";
    public static final String MC = "México";
    public static final String MN = "Michoacan";
    public static final String MS = "Morelos";
    public static final String NT = "Nayarit";
    public static final String NL = "Nuevo León";
    public static final String OC = "Oaxaca";
    public static final String PL = "Puebla";
    public static final String QT = "Querétaro";
    public static final String QR = "Quintana Roo";
    public static final String SP = "San Luis Potosí";
    public static final String SL = "Sinaloa";
    public static final String SR = "Sonora";
    public static final String TC = "Tabasco";
    public static final String TS = "Tamaulipas";
    public static final String TL = "Tlaxcala";
    public static final String VZ = "Veracruz";
    public static final String YO = "Yucatán";
    public static final String ZS = "Zacatecas";
    public static final String NE = "Nacido en el Extranjero";

    public static final String CELULAR = "celular";
    public static final String COMPANIA = "compania";
    public static final String OPERATION = "operation";
    public static final String TERMINOS = "terminos";
    public static final String CONTRATO = "contrato";
    public static final String URL = "url";

    public static final String FORMATO_HTML = "formatoHTML";
    public static final String NOMBRE_ARCHIVO = "terminos";
    public static final String RUTA = "//data/data/com.bancomer.mbanking/";
    public static final String RUTA_BANDERAS = "//data/data/com.bancomer.mbanking/files/";

    public static final String FEMENINO = "Femenino";
    public static final String MASCULINO = "Masculino";

    public static final String SHARED_NAME = "shared";
    public static final String EMPTY_STRING = "";
    public static final String SPACING_STRING = " ";
    public static final String SEED = "seed";
    public static final String CURP_VALIDO = "curpValido";
    public static final String MEX = "MEX";
    public static final String MEXICO = "MEXICO";


    /** Maquetado**/
    public static final String QUIERO_CUENTA = "quiero cuenta digital";
    public static final String CREA_CUENTA = "crea tu cuenta digital";
    public static final String CLAUSULAS_CUENTA = "clausulas cuenta digital";
    public static final String CONTRATO_CUENTA = "contrato cuenta digital";
    public static final String CONFIRMA_CUENTA = "confirma cuenta digital";
    public static final String EXITO_CUENTA = "exito cuenta digital";

    public static final String TAG_TRUE = "true";
    public static final String TAG_FALSE = "false";

    /**guardar estado confirmacion*/

    public static final String CONFIRMA = "CONFIRMAOTP";
    public static final String CONT = "CONT";
    public static final String CONTGOTP = "CONTGOTP";
    /**
     *
     */
    public static final String CP_INCORRECTO = "Código postal incorrecto";
    public static final String CP_VACIO = "Ingrese código postal";
    public static final String CURP_INCORRECTO = "Ingresa CURP con 18 caracteres";
    public static final String NETWORK_DISABLED = "Para ingresar a Bancomer móvil requieres una conexión a internet.";

    /**
     * Mensaje para alert CD
     */
    public static final String MENSAJE = "mensaje";
    /**
     * alert final
     */
    public static final String FINAL = "final";
    /**
     * firma cadena
     */
    public static final String TAG_FIRMA_CADENA = "firmaCadena";
    /**
     * parametro para peticion firma
     */
    public static final String TELEFONO_PARAM = "cellphoneNumber";
    /**
     * parametro para peticion firma
     */
    public static final String FIRMA_PARAM = "digitalSignatureImage";
    /**
     * mensaje ayuda Curp
     */
    public static final String AYUDA_CURP = "Para facilitarte tu registro te recomendamos ingresar tu CURP (Clave Única de Registro de Poblacion).";
    /**
     * expresion regular Curp
     */
    public static final String EXPRESION_REGULAR_CURP = "[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$";
    /**
     * Curp incorrecta
     */
    public static final String CURP_INCORRECTA = "Ingresa una CURP válida.\n" +
            "Nota: Campo opcional";

    public static final String MAYOR_EDAD = "Lo sentimos, el servicio es para mayores de edad";

}