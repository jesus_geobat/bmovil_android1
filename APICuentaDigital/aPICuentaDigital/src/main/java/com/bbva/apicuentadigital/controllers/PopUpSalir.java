package com.bbva.apicuentadigital.controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.APICuentaDigital;

/**
 * Created by mcamarillo on 09/07/16.
 */
public class PopUpSalir extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_popup_salir);


        TextView txtMessage = (TextView) findViewById(R.id.lblMessage);
        txtMessage.setText(getResources().getString(R.string.alert_abandonar));


        Button btnLate = (Button) findViewById(R.id.btnLate);
        btnLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent();
                i.putExtra("regresar", false);
                setResult(RESULT_OK, i);
                finish();
            }
        });

        Button btnNow = (Button) findViewById(R.id.btnNow);
        btnNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent();
                i.putExtra("regresar", true);
                setResult(RESULT_OK, i);
                //controller.creaCuentaDelegate.quieroCuenta();
                finish();

            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        APICuentaDigital.appContext=this;
    }

    @Override
    public void onBackPressed() {
    }
}
